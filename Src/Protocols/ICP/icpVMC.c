/*--------------------------------------------------------------------------------------*\
	ICPVMC.c
\*--------------------------------------------------------------------------------------*/

#include <intrinsics.h>


#include "ORION.H"
#include "icpProt.h"
#include "icpVMCProt.h"
#include "icpCHGProt.h"
#include "icpBILLProt.h"
#include "icpCPCProt.h"
#include "icpUSDProt.h"
#include "ICPUSDHook.h"


#if (_IcpMaster_ == true)

CreditValue nPrevCashLimitAmount;

bool ICPProt_VMCIsCmdBufferBusy(uint8_t nSlaveType) {
	bool fBusy = true;

	switch(nSlaveType) {
#if (_IcpSlave_ & ICP_CHANGER)
		case ICP_CHG_ADDR:
			if(ICPCHGInfo.nCmdType == ICP_VMCCMD_NONE) {
				fBusy = false;
			} else {

				switch(ICPCHGInfo.nCmdState) {
					case ICP_VMCCMDSTATE_INQUEUE:
					case ICP_VMCCMDSTATE_RUNNING:
						break;
					case ICP_VMCCMDSTATE_CHG_INITDONE:
						if(ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_RESET) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CHG_COINTYPEDONE:
						if((ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_ENABLE) ||
							(ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_ENABLECOININTUBE) ||
							(ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_INHIBIT)) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CHG_PAYOUT2DONE:
						if((ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_DISPENSE) ||
							(ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_PAYOUT2)) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CHG_PAYOUT3DONE:
						if(ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_PAYOUT3) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CHG_DIAGDONE:
						if(ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_DIAG) {
							fBusy = false;
						}
						break;
/*						
					case ICP_VMCCMDSTATE_CHG_FILLDONE:
						if(ICPCHGInfo.nCmdType == ICP_VMCCMD_CHG_FILL) {
							fBusy = false;
						}
						break;
*/						
					case ICP_VMCCMDSTATE_JUSTRESET:
					case ICP_VMCCMDSTATE_REJECTED:
					default:
						fBusy = false;
						break;
				}
			}
			if(!fBusy) {
				ICPCHGInfo.nCmdState = ICP_VMCCMDSTATE_INQUEUE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
		case ICP_BILL_ADDR:
			if(ICPBILLInfo.nCmdType == ICP_VMCCMD_NONE) {
				fBusy = false;
			} else {

				switch(ICPBILLInfo.nCmdState) {
					case ICP_VMCCMDSTATE_INQUEUE:
					case ICP_VMCCMDSTATE_RUNNING:
						break;
					case ICP_VMCCMDSTATE_BILL_INITDONE:
						if(ICPBILLInfo.nCmdType == ICP_VMCCMD_BILL_RESET) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_BILL_BILLTYPEDONE:
						if((ICPBILLInfo.nCmdType == ICP_VMCCMD_BILL_ENABLE) ||
							(ICPBILLInfo.nCmdType == ICP_VMCCMD_BILL_INHIBIT)) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_BILL_DIAGDONE:
						if(ICPBILLInfo.nCmdType == ICP_VMCCMD_BILL_DIAG) {
							fBusy = false;
						}
						break;
					/*
					case ICP_VMCCMDSTATE_BILL_RETURNED:
					case ICP_VMCCMDSTATE_BILL_STACKED:
						if(ICPBILLInfo.nCmdType == ICP_VMCCMD_BILL_ESCROW) {
							fBusy = false;
						}
						break;
					*/
					case ICP_VMCCMDSTATE_JUSTRESET:
					case ICP_VMCCMDSTATE_REJECTED:
					default:
						fBusy = false;
						break;
				}
			}
			if(!fBusy) {
				ICPBILLInfo.nCmdState = ICP_VMCCMDSTATE_INQUEUE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
		case ICP_CPC_ADDR:
			if(ICPCPCInfo.nCmdType == ICP_VMCCMD_NONE) {
				fBusy = false;
			} else {

				switch(ICPCPCInfo.nCmdState) {
					case ICP_VMCCMDSTATE_INQUEUE:
					case ICP_VMCCMDSTATE_RUNNING:
						break;
					case ICP_VMCCMDSTATE_CPC_INITDONE:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_RESET) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_DISABLED:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_DISABLE) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_ENABLED:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_ENABLE) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_CANCELLED:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_CANCEL) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_ENDSESSION:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_SESSCOMP) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_CASHSALEDONE:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_CASHSALE) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_VENDDENY:
						if((ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_VENDREQ) ||
							(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_NEGVENDREQ) ||
							(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_VENDCANCEL)) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_VENDAPP:
						if((ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_VENDREQ) ||
							(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_NEGVENDREQ) ||
							(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_VENDCANCEL)) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_VENDSUCC:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_VENDSUCC) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_VENDFAIL:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_VENDFAIL) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_REVALUEAPP:
					case ICP_VMCCMDSTATE_CPC_REVALUEDENY:
					case ICP_VMCCMDSTATE_CPC_REVALUELIMIT:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_REVALUELIMIT) {
							fBusy = false;
						}
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_REVALUEREQ) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_CPC_DIAGDONE:
						if(ICPCPCInfo.nCmdType == ICP_VMCCMD_CPC_DIAG) {
							fBusy = false;
						}
						break;
					case ICP_VMCCMDSTATE_JUSTRESET:
					case ICP_VMCCMDSTATE_REJECTED:
					default:
						fBusy = false;
						break;
				}
			}
			if(!fBusy) {
				ICPCPCInfo.nCmdState = ICP_VMCCMDSTATE_INQUEUE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD1)
		case ICP_USD1_ADDR:
			if(ICPUSD1Info.nCmdType == ICP_VMCCMD_NONE) {
				fBusy = false;
			} else {
				switch(ICPUSD1Info.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					break;
				case ICP_VMCCMDSTATE_USD_INITDONE:
				case ICP_VMCCMDSTATE_USD_DISABLED:
				case ICP_VMCCMDSTATE_USD_ENABLED:
				case ICP_VMCCMDSTATE_USD_VENDAPPROVED:
				case ICP_VMCCMDSTATE_USD_VENDDENIED:
				case ICP_VMCCMDSTATE_USD_VENDSEL:
				case ICP_VMCCMDSTATE_USD_VENDHOMESEL:
				case ICP_VMCCMDSTATE_USD_VENDSELSTATUS:
				case ICP_VMCCMDSTATE_USD_VENDSUCC:
				case ICP_VMCCMDSTATE_USD_VENDFAIL:
				case ICP_VMCCMDSTATE_USD_FUNDSCREDIT:
				case ICP_VMCCMDSTATE_USD_FUNDSSELPRICE:
				case ICP_VMCCMDSTATE_USD_DIAGDONE:
					ICPUSD1Info.nCmdType = ICP_VMCCMD_NONE;
					fBusy = false;
					break;
				case ICP_VMCCMDSTATE_JUSTRESET:
				case ICP_VMCCMDSTATE_REJECTED:
				default:
					fBusy = false;
					break;
				}
			}
			if(!fBusy) {
				ICPUSD1Info.nCmdState = ICP_VMCCMDSTATE_INQUEUE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD2)
		case ICP_USD2_ADDR:
			if(ICPUSD2Info.nCmdType == ICP_VMCCMD_NONE) {
				fBusy = false;
			} else {
				switch(ICPUSD2Info.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					break;
				case ICP_VMCCMDSTATE_USD_INITDONE:
				case ICP_VMCCMDSTATE_USD_DISABLED:
				case ICP_VMCCMDSTATE_USD_ENABLED:
				case ICP_VMCCMDSTATE_USD_VENDAPPROVED:
				case ICP_VMCCMDSTATE_USD_VENDDENIED:
				case ICP_VMCCMDSTATE_USD_VENDSEL:
				case ICP_VMCCMDSTATE_USD_VENDHOMESEL:
				case ICP_VMCCMDSTATE_USD_VENDSELSTATUS:
				case ICP_VMCCMDSTATE_USD_VENDSUCC:
				case ICP_VMCCMDSTATE_USD_VENDFAIL:
				case ICP_VMCCMDSTATE_USD_FUNDSCREDIT:
				case ICP_VMCCMDSTATE_USD_FUNDSSELPRICE:
				case ICP_VMCCMDSTATE_USD_DIAGDONE:
					ICPUSD2Info.nCmdType = ICP_VMCCMD_NONE;
					fBusy = false;
					break;
				case ICP_VMCCMDSTATE_JUSTRESET:
				case ICP_VMCCMDSTATE_REJECTED:
				default:
					fBusy = false;
					break;
				}
			}
			if(!fBusy) {
				ICPUSD2Info.nCmdState = ICP_VMCCMDSTATE_INQUEUE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD3)
		case ICP_USD3_ADDR:
			if(ICPUSD3Info.nCmdType == ICP_VMCCMD_NONE) {
				fBusy = false;
			} else {
				switch(ICPUSD3Info.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					break;
				case ICP_VMCCMDSTATE_USD_INITDONE:
				case ICP_VMCCMDSTATE_USD_DISABLED:
				case ICP_VMCCMDSTATE_USD_ENABLED:
				case ICP_VMCCMDSTATE_USD_VENDAPPROVED:
				case ICP_VMCCMDSTATE_USD_VENDDENIED:
				case ICP_VMCCMDSTATE_USD_VENDSEL:
				case ICP_VMCCMDSTATE_USD_VENDHOMESEL:
				case ICP_VMCCMDSTATE_USD_VENDSELSTATUS:
				case ICP_VMCCMDSTATE_USD_VENDSUCC:
				case ICP_VMCCMDSTATE_USD_VENDFAIL:
				case ICP_VMCCMDSTATE_USD_FUNDSCREDIT:
				case ICP_VMCCMDSTATE_USD_FUNDSSELPRICE:
				case ICP_VMCCMDSTATE_USD_DIAGDONE:
					ICPUSD3Info.nCmdType = ICP_VMCCMD_NONE;
					fBusy = false;
					break;
				case ICP_VMCCMDSTATE_JUSTRESET:
				case ICP_VMCCMDSTATE_REJECTED:
				default:
					fBusy = false;
					break;
				}
			}
			if(!fBusy) {
				ICPUSD3Info.nCmdState = ICP_VMCCMDSTATE_INQUEUE;
			}
			break;
#endif
		default:
			break;
	}

	return fBusy;
}

void ICPProt_VMCCmdRejected(uint8_t nSlaveType) {
	switch(nSlaveType) {
#if (_IcpSlave_ & ICP_CHANGER)
		case ICP_CHG_ADDR:
			ICPCHGInfo.nCmdState = ICP_VMCCMDSTATE_REJECTED;
			ICPCHGInfo.nCmdType = ICP_VMCCMD_NONE;
			break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
		case ICP_BILL_ADDR:
			ICPBILLInfo.nCmdState = ICP_VMCCMDSTATE_REJECTED;
			ICPBILLInfo.nCmdType = ICP_VMCCMD_NONE;
			break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
		case ICP_CPC_ADDR:
			ICPCPCInfo.nCmdState = ICP_VMCCMDSTATE_REJECTED;
			ICPCPCInfo.nCmdType = ICP_VMCCMD_NONE;
			break;
#endif
#if (_IcpSlave_ & ICP_USD1)
		case ICP_USD1_ADDR:
			ICPUSD1Info.nCmdState = ICP_VMCCMDSTATE_REJECTED;
			ICPUSD1Info.nCmdType = ICP_VMCCMD_NONE;
			break;
#endif
#if (_IcpSlave_ & ICP_USD2)
		case ICP_USD2_ADDR:
			ICPUSD2Info.nCmdState = ICP_VMCCMDSTATE_REJECTED;
			ICPUSD2Info.nCmdType = ICP_VMCCMD_NONE;
			break;
#endif
#if (_IcpSlave_ & ICP_USD3)
		case ICP_USD3_ADDR:
			ICPUSD3Info.nCmdState = ICP_VMCCMDSTATE_REJECTED;
			ICPUSD3Info.nCmdType = ICP_VMCCMD_NONE;
			break;
#endif
		default:
			break;
	}
}

// ------------------------------------------------------------------
ET_VMCCMDS ICPProt_VMCGetCmd(uint8_t nSlaveType) {
	ET_VMCCMDS nCmdType = ICP_VMCCMD_NONE;

	switch(nSlaveType) {
#if (_IcpSlave_ & ICP_CHANGER)
		case ICP_CHG_ADDR:
			switch(ICPCHGInfo.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					nCmdType = (ET_VMCCMDS)ICPCHGInfo.nCmdType;
					break;
				default:
					// Controllare stato ICP_CHG_LINK
					nCmdType = ICPProt_VMCCheckTubesFull();
					if(nCmdType == ICP_VMCCMD_NONE) {
						nCmdType = ICPProt_VMCCheckExactChange();
					}
					break;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
		case ICP_BILL_ADDR:
			switch(ICPBILLInfo.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					nCmdType = (ET_VMCCMDS)ICPBILLInfo.nCmdType;
					break;
				default:
					// check ICP_BILL_LINK state!!!!!!
					nCmdType = ICPProt_VMCCheckStackerFull();
					break;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
		case ICP_CPC_ADDR:
			switch(ICPCPCInfo.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					nCmdType = (ET_VMCCMDS)ICPCPCInfo.nCmdType;
					break;
				default:
					nCmdType = ICPProt_VMCCheckEscrowReturn();
					break;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD1)
		case ICP_USD1_ADDR:
			switch(ICPUSD1Info.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					nCmdType = (ET_VMCCMDS)ICPUSD1Info.nCmdType;
					break;
				default:
					break;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD2)
		case ICP_USD2_ADDR:
			switch(ICPUSD2Info.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					nCmdType = (ET_VMCCMDS)ICPUSD2Info.nCmdType;
					break;
				default:
					break;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD3)
		case ICP_USD3_ADDR:
			switch(ICPUSD3Info.nCmdState) {
				case ICP_VMCCMDSTATE_INQUEUE:
				case ICP_VMCCMDSTATE_RUNNING:
					nCmdType = (ET_VMCCMDS)ICPUSD3Info.nCmdType;
					break;
				default:
					break;
			}
			break;
#endif
		default:
			break;
	}

	return nCmdType;
}

// ------------------------------------------------------------------
uint8_t *ICPProt_VMCGetPar(uint8_t nSlaveType) {
	uint8_t *ptCmdPar = NULL;

	switch(nSlaveType) {
#if (_IcpSlave_ & ICP_CHANGER)
		case ICP_CHG_ADDR:
			ptCmdPar = ICPCHGInfo.aCmdParameters;
			ICPCHGInfo.nCmdState = ICP_VMCCMDSTATE_RUNNING;
			break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
		case ICP_BILL_ADDR:
			ptCmdPar = ICPBILLInfo.aCmdParameters;
			ICPBILLInfo.nCmdState = ICP_VMCCMDSTATE_RUNNING;
			break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
		case ICP_CPC_ADDR:
			ptCmdPar = ICPCPCInfo.aCmdParameters;
			ICPCPCInfo.nCmdState = ICP_VMCCMDSTATE_RUNNING;
			break;
#endif
#if (_IcpSlave_ & ICP_USD1)
		case ICP_USD1_ADDR:
			ptCmdPar = ICPUSD1Info.aCmdParameters;
			ICPUSD1Info.nCmdState = ICP_VMCCMDSTATE_RUNNING;
			break;
#endif
#if (_IcpSlave_ & ICP_USD2)
		case ICP_USD2_ADDR:
			ptCmdPar = ICPUSD2Info.aCmdParameters;
			ICPUSD2Info.nCmdState = ICP_VMCCMDSTATE_RUNNING;
			break;
#endif
#if (_IcpSlave_ & ICP_USD3)
		case ICP_USD3_ADDR:
			ptCmdPar = ICPUSD3Info.aCmdParameters;
			ICPUSD3Info.nCmdState = ICP_VMCCMDSTATE_RUNNING;
			break;
#endif
		default:
			break;
	}

	return ptCmdPar;
}

void ICPProt_VMCCmdDone(uint8_t nSlaveType, uint8_t nCmdResult) {
	switch(nSlaveType) {
#if (_IcpSlave_ & ICP_CHANGER)
		case ICP_CHG_ADDR:
			if(ICPCHGInfo.nCmdState == ICP_VMCCMDSTATE_RUNNING) {
				ICPCHGInfo.nCmdState = nCmdResult;
				ICPCHGInfo.nCmdType = ICP_VMCCMD_NONE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
		case ICP_BILL_ADDR:
			if(ICPBILLInfo.nCmdState == ICP_VMCCMDSTATE_RUNNING) {
				ICPBILLInfo.nCmdState = nCmdResult;
				ICPBILLInfo.nCmdType = ICP_VMCCMD_NONE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
		case ICP_CPC_ADDR:
			if(ICPCPCInfo.nCmdState == ICP_VMCCMDSTATE_RUNNING) {
				ICPCPCInfo.nCmdState = nCmdResult;
				ICPCPCInfo.nCmdType = ICP_VMCCMD_NONE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD1)
		case ICP_USD1_ADDR:
			if(ICPUSD1Info.nCmdState == ICP_VMCCMDSTATE_RUNNING) {
				ICPUSD1Info.nCmdState = nCmdResult;
				ICPUSD1Info.nCmdType = ICP_VMCCMD_NONE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD2)
		case ICP_USD2_ADDR:
			if(ICPUSD2Info.nCmdState == ICP_VMCCMDSTATE_RUNNING) {
				ICPUSD2Info.nCmdState = nCmdResult;
				ICPUSD2Info.nCmdType = ICP_VMCCMD_NONE;
			}
			break;
#endif
#if (_IcpSlave_ & ICP_USD3)
		case ICP_USD3_ADDR:
			if(ICPUSD3Info.nCmdState == ICP_VMCCMDSTATE_RUNNING) {
				ICPUSD3Info.nCmdState = nCmdResult;
				ICPUSD3Info.nCmdType = ICP_VMCCMD_NONE;
			}
			break;
#endif
		default:
			break;
	}
}


//-----------------------------------------------//
/* CHANGEGIVER FUNCTIONS*/

bool ICPCHGCoinsInTubeGet(uint8_t nCoinType, uint8_t *nCoins) {
	bool fInTube = false;
#if (_IcpSlave_ & ICP_CHANGER)
	if(nCoins) {
		uint16_t nMask;
		nMask = 1 << nCoinType;
		if(ICPCHGInfo.Status.nCoinTypeInTubes & nMask) 
			fInTube = true;
		*nCoins = ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType];
	}
#endif
	return fInTube;
}


void ICPCHGSetCoinEnableMask(uint16_t nMask)
{
#if (_IcpSlave_ & ICP_CHANGER)
	ICPCHGInfo.CoinTypeState.nCoinEnable = nMask;
#endif
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPCHGGetCoinValue
	Rende il valore della moneta selezionata dal parametro "nCoinType". 

   IN:  - nCoinType
  OUT:  - Valore moneta selezionata
\*------------------------------------------------------------------------------------------*/
CreditCoinValue ICPCHGGetCoinValue(uint8_t nCoinType) {
	
#if (_IcpSlave_ & ICP_CHANGER)
	if(nCoinType >= ICP_MAX_COINTYPES) return 0;
	return (CreditCoinValue)(ICPCHGInfo.Status.aCoinTypeVal[nCoinType] * ICPCHGInfo.Status.nUSF);
#else
	return 0;
#endif
}

ET_VMCCMDRES ICPCHGGetCmdResult(void)
{
	ET_VMCCMDRES nCmdRes = ICP_VMCCMDRES_NONE;

#if (_IcpSlave_ & ICP_CHANGER)

	if(ICPCHGInfo.nCmdType != ICP_VMCCMD_NONE) {

		switch(ICPCHGInfo.nCmdState) {
			case ICP_VMCCMDSTATE_JUSTRESET:
				nCmdRes = ICP_VMCCMDRES_JUSTRESET;
				break;
			case ICP_VMCCMDSTATE_REJECTED:
				nCmdRes = ICP_VMCCMDRES_REJECTED;
				break;
			case ICP_VMCCMDSTATE_INQUEUE:
				nCmdRes = ICP_VMCCMDRES_INQUEUE;
				break;
			case ICP_VMCCMDSTATE_RUNNING:
				nCmdRes = ICP_VMCCMDRES_RUNNING;
				break;
			default:
				nCmdRes = ICP_VMCCMDRES_DONE;
				break;
		}
	}
#endif

	return nCmdRes;
}

bool ICPCHGReset(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if(!fFail) {
		ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_RESET;
	}
#endif

	return fFail;
}


bool ICPCHGEnableCoinInTube(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)

	//MR19 uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if(!fFail) {
		ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_ENABLECOININTUBE;
	}
#endif

	return fFail;
}


bool ICPCHGEnable(CreditValue nCashLimitAmount)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)

	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if(!fFail) {

		ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_ENABLE;

		ptParam = (uint8_t *)ICPCHGInfo.aCmdParameters;
		*((CreditValue *)ptParam) = nCashLimitAmount;
		nPrevCashLimitAmount = nCashLimitAmount;
	}
#endif

	return fFail;
}


// --------------------------------------------------------------------
bool ICPCHGInhibit(void) {
	
	bool fFail = true;
#if (_IcpSlave_ & ICP_CHANGER)

	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if (!fFail) {
		ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_INHIBIT;
		ptParam = (uint8_t *)ICPCHGInfo.aCmdParameters;
		*((CreditValue *)ptParam) = 0;
	}
#endif
	return fFail;
}

// --------------------------------------------------------------------
//	Per risolvere il problema "non da' resto a livello 2" il cmd 
//	ICPCHGDispense viene rifiutato perche' c'e' in corso il cmd 
//	di ICPCHGPayout del VMC
static bool ICPCHGPayoutDispense(uint8_t nCoinType, uint8_t nCoins) {
	
	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)
	ICPCHGInfo.nCmdType = ICP_VMCCMD_NONE;
	fFail = ICPCHGDispense(nCoinType, nCoins);
#endif

	return fFail;
}

// --------------------------------------------------------------------
bool ICPCHGDispense(uint8_t nCoinType, uint8_t nCoins)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)
	uint8_t *ptParam;
	uint16_t nMask;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if(!fFail) {

		nMask = 1 << nCoinType;
		if(ICPCHGInfo.Status.nCoinTypeInTubes & nMask) {

			ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_DISPENSE;

			ptParam = (uint8_t *)ICPCHGInfo.aCmdParameters;
			*((uint8_t *)ptParam) = nCoinType;
			*((uint8_t *)(ptParam + sizeof(uint8_t))) = nCoins;
		}
	}
#endif

	return fFail;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPCHGPayout

   IN:  - 
  OUT:  - TRUE se parte la richiesta di Payout, altrimenti FALSE
\*------------------------------------------------------------------------------------------*/
bool ICPCHGPayout(CreditCoinValue nPayoutVal) {

	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)
	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if (!fFail) {
		if ((ICPCHGInfo.Status.nFLevel < 3) || !(ICPCHGInfo.nFOptions & ICP_CHGOPT3_PAYOUT)) {
			ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_PAYOUT2;
		} else {
			ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_PAYOUT3;
		}
		ptParam = (uint8_t *)ICPCHGInfo.aCmdParameters;
		*((CreditCoinValue *)ptParam) = nPayoutVal;
	}
#endif

	return fFail;
}


// --------------------------------------------------------------------
bool ICPCHGDiag(uint8_t *ptData, uint8_t nDataLen)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)
	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if(!fFail) {

		ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_DIAG;

		ptParam = (uint8_t *)ICPCHGInfo.aCmdParameters;
		*((uint8_t *)ptParam) = nDataLen;
		*((uint8_t **)(ptParam + sizeof(uint8_t))) = ptData;
	}
#endif

	return fFail;
}



bool ICPCHG_fill_report(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CHANGER)
	//MR19 uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CHG_ADDR);
	if(!fFail) {

//		ICPCHGInfo.nCmdType = ICP_VMCCMD_CHG_FILL;

		//MR19 ptParam = (uint8_t *)ICPCHGInfo.aCmdParameters;

	}
#endif

	return fFail;
}




//-----------------------------------------------//
/* BILL VALIDATOR FUNCTIONS*/

void ICPBILLSetBillEnableMask(uint16_t nMask)
{
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	ICPBILLInfo.BillTypeState.nBillEnable = nMask;
#endif
}

CreditCoinValue ICPBILLGetBillValue(uint8_t nBillType)
{
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	if(nBillType >= ICP_MAX_BILLTYPES)
		return 0;

	return (CreditCoinValue)(ICPBILLInfo.Status.aBillTypeVal[nBillType] * ICPBILLInfo.Status.nUSF);
#else
	return 0;
#endif
}

ET_VMCCMDRES ICPBILLGetCmdResult(void)
{
	ET_VMCCMDRES nCmdRes = ICP_VMCCMDRES_NONE;

#if (_IcpSlave_ & ICP_BILLVALIDATOR)

	if(ICPBILLInfo.nCmdType != ICP_VMCCMD_NONE) {

		switch(ICPBILLInfo.nCmdState) {
			case ICP_VMCCMDSTATE_JUSTRESET:
				nCmdRes = ICP_VMCCMDRES_JUSTRESET;
				break;
			case ICP_VMCCMDSTATE_REJECTED:
				nCmdRes = ICP_VMCCMDRES_REJECTED;
				break;
			case ICP_VMCCMDSTATE_INQUEUE:
				nCmdRes = ICP_VMCCMDRES_INQUEUE;
				break;
			case ICP_VMCCMDSTATE_RUNNING:
				nCmdRes = ICP_VMCCMDRES_RUNNING;
				break;
			default:
				nCmdRes = ICP_VMCCMDRES_DONE;
				break;
		}
	}
#endif

	return nCmdRes;
}

bool ICPBILLReset(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_BILLVALIDATOR)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_BILL_ADDR);
	if(!fFail) {
		ICPBILLInfo.nCmdType = ICP_VMCCMD_BILL_RESET;
	}
#endif

	return fFail;
}


bool ICPBILLEnable(CreditValue nCashLimitAmount)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_BILLVALIDATOR)

	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_BILL_ADDR);
	if(!fFail) {

		ICPBILLInfo.nCmdType = ICP_VMCCMD_BILL_ENABLE;

		ptParam = (uint8_t *)ICPBILLInfo.aCmdParameters;
		*((CreditValue *)ptParam) = nCashLimitAmount;
	}
#endif

	return fFail;
}


bool ICPBILLInhibit(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_BILLVALIDATOR)

	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_BILL_ADDR);
	if(!fFail) {

		ICPBILLInfo.nCmdType = ICP_VMCCMD_BILL_INHIBIT;

		ptParam = (uint8_t *)ICPBILLInfo.aCmdParameters;
		*((CreditValue *)ptParam) = 0;
	}
#endif

	return fFail;
}

/*
bool ICPBILLEscrow(bool fEscrow)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_BILLVALIDATOR)

	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_BILL_ADDR);
	if(!fFail && ICPBILLInfo.Status.fEscrow && ICPBILLInfo.BillTypeState.nEscrowEnable) {

		ICPBILLInfo.nCmdType = ICP_VMCCMD_BILL_ESCROW;

		ptParam = (uint8_t *)ICPBILLInfo.aCmdParameters;
		*((uint8_t *)ptParam) = fEscrow;
	}

#endif

	return fFail;
}
*/

bool ICPBILLDiag(uint8_t *ptData, uint8_t nDataLen)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_BILL_ADDR);
	if(!fFail) {

		ICPBILLInfo.nCmdType = ICP_VMCCMD_BILL_DIAG;

		ptParam = (uint8_t *)ICPBILLInfo.aCmdParameters;
		*((uint8_t *)ptParam) = nDataLen;
		*((uint8_t **)(ptParam + sizeof(uint8_t))) = ptData;
	}
#endif

	return fFail;
}


//-----------------------------------------------//
/* CASHLESS FUNCTIONS*/

ET_VMCCMDRES ICPCPCGetCmdResult(void)
{
	ET_VMCCMDRES nCmdRes = ICP_VMCCMDRES_NONE;

#if (_IcpSlave_ & ICP_CARDREADER)

	if(ICPCPCInfo.nCmdType != ICP_VMCCMD_NONE) {

		switch(ICPCPCInfo.nCmdState) {
			case ICP_VMCCMDSTATE_JUSTRESET:
				nCmdRes = ICP_VMCCMDRES_JUSTRESET;
				break;
			case ICP_VMCCMDSTATE_REJECTED:
				nCmdRes = ICP_VMCCMDRES_REJECTED;
				break;
			case ICP_VMCCMDSTATE_INQUEUE:
				nCmdRes = ICP_VMCCMDRES_INQUEUE;
				break;
			case ICP_VMCCMDSTATE_RUNNING:
				nCmdRes = ICP_VMCCMDRES_RUNNING;
				break;
			default:
				nCmdRes = ICP_VMCCMDRES_DONE;
				break;
		}
	}
#endif

	return nCmdRes;
}

bool ICPCPCReset(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_RESET;
	}
#endif

	return fFail;
}


bool ICPCPCVendReq(CreditCpcValue nSelPrice, uint16_t nSelNumber, uint16_t nCurrencyCode)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)
	uint8_t *ptParam;
	uint16_t nCC = 0;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {

		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_VENDREQ;

		ptParam = (uint8_t *)ICPCPCInfo.aCmdParameters;
		*((CreditCpcValue *)ptParam) = (CreditCpcValue)nSelPrice;
		*((uint16_t *)(ptParam + sizeof(CreditCpcValue))) = nSelNumber;
		if(ICPCPCInfo.Setup00.nFLevel >= 3)
			nCC = nCurrencyCode;
		*((uint16_t *)(ptParam + sizeof(CreditCpcValue) + sizeof(uint16_t))) = nCC;
	}
#endif

	return fFail;
}


bool ICPCPCNegVendReq(CreditCpcValue nSelPrice, uint16_t nSelNumber, uint16_t nCurrencyCode)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)
	uint8_t *ptParam;

	if(ICPCPCInfo.Setup00.nFLevel < 3)
		return fFail;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {

		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_NEGVENDREQ;

		ptParam = (uint8_t *)ICPCPCInfo.aCmdParameters;
		*((CreditCpcValue *)ptParam) = (CreditCpcValue)nSelPrice;
		*((uint16_t *)(ptParam + sizeof(CreditCpcValue))) = nSelNumber;
		*((uint16_t *)(ptParam + sizeof(CreditCpcValue) + sizeof(uint16_t))) = nCurrencyCode;
	}
#endif

	return fFail;
}


bool ICPCPCVendSuccess(uint16_t nSelNumber)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)
	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {

		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_VENDSUCC;

		ptParam = (uint8_t *)ICPCPCInfo.aCmdParameters;
		*((uint16_t *)ptParam) = nSelNumber;
	}
#endif

	return fFail;
}


bool ICPCPCVendFailure(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_VENDFAIL;
	}
#endif

	return fFail;
}


bool ICPCPCVendCancel(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_VENDCANCEL;
	}
#endif

	return fFail;
}


bool ICPCPCVendCashSale(CreditCpcValue nSelPrice, uint16_t nSelNumber, uint16_t nCurrencyCode)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)
	uint8_t *ptParam;
	uint16_t nCC = 0xFFFF;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {

		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_CASHSALE;

		ptParam = (uint8_t *)ICPCPCInfo.aCmdParameters;
		*((CreditCpcValue *)ptParam) = (CreditCpcValue)nSelPrice;
		*((uint16_t *)(ptParam + sizeof(CreditCpcValue))) = nSelNumber;
		if(ICPCPCInfo.Setup00.nFLevel >= 3)
			nCC = nCurrencyCode;
		*((uint16_t *)(ptParam + sizeof(CreditCpcValue) + sizeof(uint16_t))) = nCC;
	}
#endif

	return fFail;
}


bool ICPCPCPullMedia(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_SESSCOMP;
	}
#endif

	return fFail;
}


bool ICPCPCEnable(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_ENABLE;
	}
#endif

	return fFail;
}


bool ICPCPCDisable(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_DISABLE;
	}
#endif

	return fFail;
}


bool ICPCPCCancel(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_CANCEL;
	}
#endif

	return fFail;
}


bool ICPCPCRevalueRequest(CreditCpcValue nRevalueAmount, uint16_t nCurrencyCode)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)
	uint8_t *ptParam;
	uint16_t nCC = 0;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {

		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_REVALUEREQ;

		ptParam = (uint8_t *)ICPCPCInfo.aCmdParameters;
		*((CreditCpcValue *)ptParam) = (CreditCpcValue)nRevalueAmount;
		if(ICPCPCInfo.Setup00.nFLevel >= 3)
			nCC = nCurrencyCode;
		*((uint16_t *)(ptParam + sizeof(CreditCpcValue))) = nCC;
	}
#endif

	return fFail;
}


bool ICPCPCRevalueLimit(void)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {
		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_REVALUELIMIT;
	}
#endif

	return fFail;
}


bool ICPCPCDiag(uint8_t *ptData, uint8_t nDataLen)
{
	bool fFail = true;

#if (_IcpSlave_ & ICP_CARDREADER)
	uint8_t *ptParam;

	fFail = ICPProt_VMCIsCmdBufferBusy(ICP_CPC_ADDR);
	if(!fFail) {

		ICPCPCInfo.nCmdType = ICP_VMCCMD_CPC_DIAG;

		ptParam = (uint8_t *)ICPCPCInfo.aCmdParameters;
		*((uint8_t *)ptParam) = nDataLen;
		*((uint8_t **)(ptParam + sizeof(uint8_t))) = ptData;
	}
#endif

	return fFail;
}


//-----------------------------------------------//

T_ICP_USDINFO *ICPUSDGetInfo(uint8_t usdSlave, bool *fCmdRejected) {
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	if(fCmdRejected) {
		*fCmdRejected = ICPProt_VMCIsCmdBufferBusy(usdSlave);
		if(*fCmdRejected) {
			return nil;
		}
	}
	switch(usdSlave) {
	case ICP_USD1_ADDR:
		return &ICPUSD1Info;
	case ICP_USD2_ADDR:
		return &ICPUSD2Info;
	case ICP_USD3_ADDR:
		return &ICPUSD3Info;
	}
#endif
	return nil;
}

ET_VMCCMDRES ICPUSDGetCmdResult(uint8_t usdSlave) {
	ET_VMCCMDRES nCmdRes = ICP_VMCCMDRES_NONE;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, nil);
	if(pUSDInfo->nCmdType != ICP_VMCCMD_NONE) {
		switch(pUSDInfo->nCmdState) {
			case ICP_VMCCMDSTATE_JUSTRESET:
				nCmdRes = ICP_VMCCMDRES_JUSTRESET;
				break;
			case ICP_VMCCMDSTATE_REJECTED:
				nCmdRes = ICP_VMCCMDRES_REJECTED;
				break;
			case ICP_VMCCMDSTATE_INQUEUE:
				nCmdRes = ICP_VMCCMDRES_INQUEUE;
				break;
			case ICP_VMCCMDSTATE_RUNNING:
				nCmdRes = ICP_VMCCMDRES_RUNNING;
				break;
			default:
				nCmdRes = ICP_VMCCMDRES_DONE;
				break;
		}
	}
#endif
	return nCmdRes;
}

bool ICPUSDReset(uint8_t usdSlave) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) pUSDInfo->nCmdType = ICP_VMCCMD_USD_RESET;
#endif
	return fFail;
}

bool ICPUSDDisable(uint8_t usdSlave) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) pUSDInfo->nCmdType = ICP_VMCCMD_USD_DISABLE;
#endif
	return fFail;
}

bool ICPUSDEnable(uint8_t usdSlave) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) pUSDInfo->nCmdType = ICP_VMCCMD_USD_ENABLE;
#endif
	return fFail;
}

bool ICPUSDVendApproved(uint8_t usdSlave) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) pUSDInfo->nCmdType = ICP_VMCCMD_USD_VENDAPPROVED;
#endif
	return fFail;
}

bool ICPUSDVendDenied(uint8_t usdSlave) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) pUSDInfo->nCmdType = ICP_VMCCMD_USD_VENDDENIED;
#endif
	return fFail;
}

bool ICPUSDVendRequest(uint8_t usdSlave, uint8_t cmdType, uint8_t nSelRow, uint8_t nSelColumn) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	uint8_t *ptParam;
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) {
		pUSDInfo->nCmdType = cmdType;
		ptParam = (uint8_t *)pUSDInfo->aCmdParameters;
		*((uint8_t *)ptParam) = nSelRow;
		*((uint8_t *)(ptParam + sizeof(uint8_t))) = nSelColumn;
	}
#endif
	return fFail;
}

bool ICPUSDVendSel(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	fFail = ICPUSDVendRequest(usdSlave, ICP_VMCCMD_USD_VENDSEL, nSelRow, nSelColumn);
#endif
	return fFail;
}

bool ICPUSDVendHomeSel(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	fFail = ICPUSDVendRequest(usdSlave, ICP_VMCCMD_USD_VENDHOMESEL, nSelRow, nSelColumn);
#endif
	return fFail;
}

bool ICPUSDVendSelStatus(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	fFail = ICPUSDVendRequest(usdSlave, ICP_VMCCMD_USD_VENDSELSTATUS, nSelRow, nSelColumn);
#endif
	return fFail;
}

bool ICPUSDFundsCredit(uint8_t usdSlave, CreditCpcValue creditAmount) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	uint8_t *ptParam;
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) {
		pUSDInfo->nCmdType = ICP_VMCCMD_USD_FUNDSCREDIT;
		ptParam = (uint8_t *)pUSDInfo->aCmdParameters;
		*((CreditCpcValue *)ptParam) = (CreditCpcValue)creditAmount;
	}
#endif
	return fFail;
}

bool ICPUSDFundsSelPrice(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn, CreditCpcValue nSelPrice, uint16_t nSelID) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	uint8_t *ptParam;
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) {
		pUSDInfo->nCmdType = ICP_VMCCMD_USD_FUNDSSELPRICE;
		ptParam = (uint8_t *)pUSDInfo->aCmdParameters;
		*((uint8_t *)ptParam) = nSelRow;
		*((uint8_t *)(ptParam + sizeof(uint8_t))) = nSelColumn;
		*((CreditCpcValue *)(ptParam + sizeof(uint8_t) + sizeof(uint8_t))) = nSelPrice;
		*((uint16_t *)(ptParam + sizeof(uint8_t) + sizeof(uint8_t) + sizeof(CreditCpcValue))) = nSelID;
	}
#endif
	return fFail;
}

bool ICPUSDDiag(uint8_t usdSlave, uint8_t *ptData, uint8_t nDataLen) {
	bool fFail = true;
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	uint8_t *ptParam;
	T_ICP_USDINFO *pUSDInfo = ICPUSDGetInfo(usdSlave, &fFail);
	if(pUSDInfo) {
		pUSDInfo->nCmdType = ICP_VMCCMD_USD_DIAG;
		ptParam = (uint8_t *)pUSDInfo->aCmdParameters;
		*((uint8_t *)ptParam) = nDataLen;
		*((uint8_t **)(ptParam + sizeof(uint8_t))) = ptData;
	}
#endif
	return fFail;
}


//-----------------------------------------------//

#define _IcpChgEuroExch_ false

#if ((_IcpSlave_ & ICP_CHANGER) && (_IcpChgEuroExch_)) 
	const uint8_t tbEuroCoinVal[] = {
//		5,
		10,
//		20,
		50,
		100,
		200
	};
#endif

#ifdef ICPProt_VMCCheckExactChange
#undef ICPProt_VMCCheckExactChange
#endif

ET_VMCCMDS ICPProt_VMCCheckExactChange(void) {

	ET_VMCCMDS nCmdType = ICP_VMCCMD_NONE;

#ifndef ICP_ORION		

#if (_IcpSlave_ & ICP_CHANGER)
	uint8_t *ptParam;
	bool fExChange, fEuroExch = false;															  //ABELE cambiato nome alla variabile locale per exact change
	int16 nCoinType, n;
	uint16_t nMask, nCoinTypeInTubes;
	uint8_t nCoinTypeVal;
	CreditValue nCashLimitAmount;

	if(!(ICPInfo.nSlaveReadyMask & ICP_CHG_LINK))
		return nCmdType;

	#if (_IcpChgEuroExch_)
		fEuroExch = true;
	#endif

	if(!fEuroExch) {
		nCoinTypeInTubes = ICPCHGInfo.Status.nCoinTypeInTubes;
		if (!nCoinTypeInTubes)																	// Senza monete nei tubi nCoinTypeInTubes=0xffff per segnalare Ex-Ch
			nCoinTypeInTubes = 0xFFFF;
	} else {
		nCoinTypeInTubes = 0;
		#if (_IcpChgEuroExch_)
			for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {

				nMask = 0;
				nCoinTypeVal = ICPCHGInfo.Status.aCoinTypeVal[nCoinType];
				for(n = 0; n < sizeof(tbEuroCoinVal); n++) {
					if(nCoinTypeVal == tbEuroCoinVal[n]) {
						nMask = 1 << nCoinType;
						break;
					}
				}

				nCoinTypeInTubes |= nMask;
			}
			nCoinTypeInTubes &= ICPCHGInfo.Status.nCoinTypeInTubes;
		#endif
	}

	fExChange = false;
	for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {

		nMask = 1 << nCoinType;
		if(nCoinTypeInTubes & nMask) {
			if((ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType] < ICP_CHG_NCOINSEXCH) && !(ICPCHGInfo.TubeStatus.nCoinTypeTubeFull & nMask)) {
				fExChange = true;
				break;
			}
		}
	}

	if(fExactChange) {
		if(!(ICPCHGInfo.nInfoMask & ICP_CHGINFO_EXCH)) {
			ICPCHGInfo.nInfoMask |= ICP_CHGINFO_EXCH;
			ICPCHGHook_SetExactChange(fExactChange);
			nCashLimitAmount = 0;
			if(nCoinTypeInTubes) {
				for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {
					nMask = 1 << nCoinType;
					if(nMask & nCoinTypeInTubes) {
						if(ICPCHGInfo.Status.aCoinTypeVal[nCoinType] > nCashLimitAmount)
							nCashLimitAmount = ICPCHGInfo.Status.aCoinTypeVal[nCoinType];
					}
				}
				nCashLimitAmount *= ICPCHGInfo.Status.nUSF;
				ICPCHGEnable(nCashLimitAmount);
			}
		}
	} else {
		if(ICPCHGInfo.nInfoMask & ICP_CHGINFO_EXCH) {
			ICPCHGInfo.nInfoMask &= ~ICP_CHGINFO_EXCH;
			ICPCHGHook_SetExactChange(fExactChange);
			ICPCHGEnable(nPrevCashLimitAmount);
		}
	}
#endif

#endif   /*END IF ORION*/	
	return nCmdType;
	
}


ET_VMCCMDS ICPProt_VMCCheckTubesFull(void)
{
	ET_VMCCMDS nCmdType = ICP_VMCCMD_NONE;

#if (_IcpSlave_ & ICP_CHANGER)
	//MR19 uint8_t *ptParam;

	if((ICPCHGInfo.TubeStatus.nCoinTypeTubeFull & ICPCHGInfo.Status.nCoinTypeInTubes) == ICPCHGInfo.Status.nCoinTypeInTubes) {
		ICPCHGHook_SetTubesFull(true);
	}
#endif

	return nCmdType;
}

ET_VMCCMDS ICPProt_VMCCheckStackerFull(void)
{
	ET_VMCCMDS nCmdType = ICP_VMCCMD_NONE;

#if (_IcpSlave_ & ICP_BILLVALIDATOR)
//	uint8_t *ptParam;

	if(!(ICPBILLInfo.nInfoMask & ICP_BILLINFO_STACKERFULLINH)) {

		if(ICPBILLInfo.nInfoMask & ICP_BILLINFO_STACKERFULL) {
			ICPBILLInfo.nInfoMask |= ICP_BILLINFO_STACKERFULLINH;
			ICPBILLHook_SetStackerFull(true);
		}
	} else {

		if(!(ICPBILLInfo.nInfoMask & ICP_BILLINFO_STACKERFULL)) {
			ICPBILLInfo.nInfoMask &= ~ICP_BILLINFO_STACKERFULLINH;
			ICPBILLHook_SetStackerFull(false);
		}
	}
#endif

	return nCmdType;

}


ET_VMCCMDS ICPProt_VMCCheckEscrowReturn(void)
{
	ET_VMCCMDS nCmdType = ICP_VMCCMD_NONE;
	uint8_t nPeriphID;
	bool fFail;

/*test: NRIEuro
if(ICPCHGInfo.nInfoMask & ICP_CHGINFO_ESCROW) {
	ICPCHGInfo.nInfoMask &= ~ICP_CHGINFO_ESCROW;
	ICPCHGPayout(20);
}
*/

#if ((_IcpSlave_ & ICP_CHANGER) && (_IcpSlave_ & ICP_CARDREADER))

	if(ICPCHGInfo.nInfoMask & ICP_CHGINFO_ESCROW) {

		fFail = true;
		nPeriphID = ICPInfo.nCurrPeriph;
		switch(ICPInfo.aPeriphInfo[nPeriphID].nState) {
			case ICP_CPC_ENABLED:
				fFail = ICPCPCCancel();
				break;
			case ICP_CPC_SESSIDLE:
				fFail = ICPCPCPullMedia();
				break;
			case ICP_CPC_VEND:
				fFail = ICPCPCVendCancel();
				break;
			default:
				fFail = false;
				break;
		}

		if(!fFail)
			ICPCHGInfo.nInfoMask &= ~ICP_CHGINFO_ESCROW;
	}
#endif

	return nCmdType;
}
/*------------------------------------------------------------------------------------------*\
 Method: ICPProt_VMCDispense
	Calcola la quantita' di monete per ogni CoinType da rendere con RR a Livello 2 con
	comando DISPENSE (0x0D).
	numero di attivazioni e tubo sono memorizzate nel buffer "ICPInfo.freeuse".

   IN:  - nPayoutVal con valore del resto da rendere
  OUT:  - ICPInfo.freeuse con coppie "numero_tubo e pezzi da rendere"
\*------------------------------------------------------------------------------------------*/
bool ICPProt_VMCDispense(void)
{
	bool fDispense = false;

#if (_IcpSlave_ & ICP_CHANGER)
	uint8_t 		*ptCmdPar;
	CreditCoinValue	nPayoutVal;
	int16 			nCoinType, nCoins;
	uint16_t 		nMask, CoinVal_16bit;

	ptCmdPar = ICPProt_VMCGetPar(ICP_CHG_ADDR);
	if(!ptCmdPar)
		return fDispense;
	nPayoutVal = *((CreditCoinValue *)ptCmdPar);
	if(!nPayoutVal)
		return fDispense;

	Cvt16ToBigEndian(nPayoutVal);
	nPayoutVal = nPayoutVal / ICPCHGInfo.Status.nUSF;

	for(nCoinType = ICP_MAX_COINTYPES - 1; nCoinType >= 0; nCoinType--) {

		ICPInfo.freeuse[nCoinType] = 0;

		nMask = 1 << nCoinType;
		if(ICPCHGInfo.Status.nCoinTypeInTubes & nMask)
		{
			// --  Errore rilevato il 11.04.2020: con RR a livello 2 e con comando --
		  	// --  DISPENSE (0x0D), la RR rende un credito molto maggiore del corretto --
		  	// Il motivo era la divisione di nPayoutVal, moltiplicato per 256 dalla 
		  	// Cvt16ToBigEndian, col valore della moneta puntata da nCoinType
			
		  	//MR20 !!!!!! nCoins = nPayoutVal / ICPCHGInfo.Status.aCoinTypeVal[nCoinType];	!!!!!!
		  	CoinVal_16bit = ICPCHGInfo.Status.aCoinTypeVal[nCoinType];
		  	Cvt16ToBigEndian(CoinVal_16bit);
		  	nCoins = nPayoutVal / CoinVal_16bit;
			
			if(!nCoins)
				continue;

//			if(ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType] < ICP_CHG_NCOINSEXCH)
//				continue;

			if(nCoins > ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType])
				nCoins = ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType];

			ICPInfo.freeuse[nCoinType] = nCoins;
			//MR20nPayoutVal -= nCoins * ICPCHGInfo.Status.aCoinTypeVal[nCoinType];
			nPayoutVal -= nCoins * CoinVal_16bit;
			if(!fDispense && nCoins)
				fDispense = true;
		}
	}
#endif

	return fDispense;
}

bool ICPProt_VMCTxDispense(void)
{
#if (_IcpSlave_ & ICP_CHANGER)
	int16 nCoinType;

	for(nCoinType = ICP_MAX_COINTYPES - 1; nCoinType >= 0; nCoinType--) {

		if(ICPInfo.freeuse[nCoinType]) {
			// Per risolvere problema "non da' resto a livello 2"
			/*ICPCHGDispense(nCoinType, ICPInfo.freeuse[nCoinType]);*/
			ICPCHGPayoutDispense(nCoinType, ICPInfo.freeuse[nCoinType]);
			ICPInfo.freeuse[nCoinType] = 0;
			return true;
		}
	}

	for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {
		ICPInfo.freeuse[nCoinType] = ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType];
	}
#endif

	return false;
}

void ICPProt_VMCEndDispense(void)
{
#if (_IcpSlave_ & ICP_CHANGER)
	uint16_t nCoinType;
	int16 nCoins;
	uint16_t nMask;
	CreditCoinValue nPaidoutVal;

	nPaidoutVal = 0;
	for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {

		nMask = 1 << nCoinType;
		if(ICPCHGInfo.Status.nCoinTypeInTubes & nMask) {

			nCoins = ICPInfo.freeuse[nCoinType] - ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType];
			if(nCoins > 0)
				nPaidoutVal += nCoins * ICPCHGInfo.Status.aCoinTypeVal[nCoinType];
		}
	}

	nPaidoutVal = nPaidoutVal * ICPCHGInfo.Status.nUSF;

	Cvt16ToBigEndian(&nPaidoutVal);
	ICPCHGHook_SetPayoutComplete(nPaidoutVal);
#endif
}



void ICPProt_VMCGetSetupInfo(T_VMC_SETUP00 *ptInfo) {
	ptInfo->nFLevel = _IcpVmcLevel_;
	ptInfo->nDispCol = _IcpVmcDisplayCol_;
	ptInfo->nDispRow = _IcpVmcDisplayRow_;
	ptInfo->nDispType = _IcpVmcDisplayType_;
}

void ICPProt_VMCUsdGetSetupInfo(T_VMC_SETUP *ptInfo) {
	ptInfo->nFLevel = _IcpVmcLevel_;
	ptInfo->nUSF = ICPVMCHook_GetScalingFactor();
	//MR20 Cvt16ToBigEndian(&ptInfo->nUSF);
	ptInfo->nUSF = __REV16(ptInfo->nUSF);
	ptInfo->nDPP = ICPVMCHook_GetDecimalPoint();
	ptInfo->nMaxTmApproveDeny = 0xFF;
}

#endif
