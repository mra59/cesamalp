/****************************************************************************************
File:
    AuditRevalue.c

Description:
    Funzioni Audit Revalue
    

History:
    Date       Aut  Note
    Set 2012	MR   

*****************************************************************************************/

#include "ORION.H"
#include "AuditRevalue.h"
#include "AuditExt.h"
#include "IICEEP.h"
#include "Funzioni.h"
#include "Power.h"



#include "Dummy.h"

/*---------------------------------------------------------------------------------------------*\
Method: ResRevalue
	Azzera variabili Reinstate.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static void ResRevalue(void) {
	
	PSHProcessingData();
	TipoRevalue = NO_REVALUE;
	StatoRevalue = 0;
	RechargeFlags = 0;
	CashRecharged = 0;
	PSHCheckPowerDown();
}

/*---------------------------------------------------------------------------------------------*\
Method: RevalueKRCash
	Aggiorna gli ID Audit relativi al Revalue carta effettuato tramite Cash da Selettore o
	da Bill Validator (anche in MDB).

	IN:	  - StatoRevalue e valore caricato in CashRecharged.
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static void RevalueKRCash(void) {

	uint16_t	valore;
	
	if ((RechargeFlags & WaitEndCashWrite)) return;												// In attesa fine scrittura carta
	valore = CashRecharged;
	switch(StatoRevalue) {

		case 1 :
			AuditUlongAdd(IICEEPAuditAddr(LRCardCre), valore);         							// DA402
			StatoRevalue++;
			PSHCheckPowerDown();

		case 2 :
			AuditUlongAdd(IICEEPAuditAddr(INCardCre), valore);       							// DA401
			StatoRevalue++;
			PSHCheckPowerDown();

		case 3 :
			if (RechargeFlags & CaricamentoDubbio) {											// C'e' accredito dubbio, quindi eseguo i successivi case
				StatoRevalue++;
				break;
			} else {
				AuditExtRevalueKRCash(valore);													// Non c'e' accredito dubbio, quindi Audit Estesa Record N. 4
				ResRevalue();
				PSHCheckPowerDown();
				break;
			}

		// --- Registro evento EA2*EK3 e AuditEstesa N. 13 solo se accredito dubbio ---	
    	case 4 :
    		AuditUintInc(IICEEPAuditAddr(LREvent3));                      						// EA-202_3	EA2*EK_03 accredito dubbio LR
    		StatoRevalue++;
    		PSHCheckPowerDown();

    	case 5 :
    		AuditUlongInc(IICEEPAuditAddr(INEvent3));                 							// EA-203_3 Numb EA2*EK_03 accredito dubbio IN
    		StatoRevalue++;
    		PSHCheckPowerDown();

    	case 6 :
    		AuditUlongAdd(IICEEPAuditAddr(LREv3Val), CashRecharged);       						// EA-204_3 Numb EA2*EK_03 accredito dubbio : valore azzerato LR
    		StatoRevalue++;
    		PSHCheckPowerDown();

    	case 7 :
    		AuditExtCardWriteFail(TipoRevalue, valore);											// Audit Estesa Record N. 13
    		StatoRevalue++;

		default:
			ResRevalue();
  }
}

/*---------------------------------------------------------------------------------------------*\
Method: RevalueKRCaricamentoBlocchi
	Aggiorna gli ID Audit relativi al Revalue carta effettuato tramite carta caricamento a
	blocchi. E' sempre caricato un blocco alla volta.

	IN:	  - StatoRevalue
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static void RevalueKRCaricamentoBlocchi(void) {

	uint16_t  valore, blocchi;

	//blocchi = (CashCaricamentoBlocchi / ValoreBloccoCaricamento);
	blocchi = 1;
	valore = CashRecharged;
	switch(StatoRevalue) {

		case 1 :
			AuditUlongAdd(IICEEPAuditAddr(LRCardCre), valore);    								// DA402 - Value credited to Cashless 1 - LR
			StatoRevalue++;
			PSHCheckPowerDown();

		case 2 :
			AuditUlongAdd(IICEEPAuditAddr(INCardCre), valore);    								// DA401 - Value credited to Cashless 1 - IN
			StatoRevalue++;
			PSHCheckPowerDown();

		case 3 :
			AuditUlongAdd(IICEEPAuditAddr(LRManCard), valore);    								// DA303 - User Defined Field
			StatoRevalue++;
			PSHCheckPowerDown();

		case 4 :
			AuditUintAdd(IICEEPAuditAddr(LREvent8), blocchi);                      				// EA2*EK_04" "Caricamento a Blocchi"
			StatoRevalue++;
			PSHCheckPowerDown();

		case 5 :
			AuditUlongAdd(IICEEPAuditAddr(INEvent8), blocchi);                 					// EA2*EK_04" "Caricamento a Blocchi"
			StatoRevalue++;
			PSHCheckPowerDown();

		case 6 :
			AuditUlongAdd(IICEEPAuditAddr(LREv8Val), valore);  									// EA2*EK_04" "Caricamento a Blocchi"
			StatoRevalue++;
			PSHCheckPowerDown();
			
		case 7 :
			if (RechargeFlags & CaricamentoDubbio) {											// C'e' accredito dubbio, quindi eseguo i successivi case
				StatoRevalue++;
				break;
			} else {
				AuditExtRevalueCartaCaricamentoBlocchi(valore);									// Non c'e' accredito dubbio, quindi Audit Estesa Record N. 12
				ResRevalue();
				PSHCheckPowerDown();
				break;
			}

		// --- Registro evento EA2*EK3 e AuditEstesa N. 13 solo se accredito dubbio ---	
		case 8 :
    		AuditUintInc(IICEEPAuditAddr(LREvent3));                      						// EA2*EK_03 accredito dubbio LR
    		StatoRevalue++;
    		PSHCheckPowerDown();

    	case 9 :
    		AuditUlongInc(IICEEPAuditAddr(INEvent3));                 							// EA2*EK_03 accredito dubbio IN
    		StatoRevalue++;
    		PSHCheckPowerDown();

    	case 10 :
    		AuditUlongAdd(IICEEPAuditAddr(LREv3Val), CashRecharged);       						// EA2*EK_03 accredito dubbio : valore azzerato LR
    		StatoRevalue++;
    		PSHCheckPowerDown();

    	case 11 :
    		AuditExtCardWriteFail(TipoRevalue, valore);											// Audit Estesa Record N. 13
    		StatoRevalue++;

		default:
			ResRevalue();
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: RevalueKRReinstate
	Aggiorna gli ID Audit relativi al Revalue carta effettuato tramite reinstate da vendita
	fallita.
	Esistono 2 refund: uno effettuato al termine della selezione fallita quando la carta e'
	ancora nel lettore, il secondo effettuato al successivo inserimento della carta che non
	era piu' nel lettore al termine della vendita fallita.

	IN:	  - StatoRevalue
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static void RevalueKRReinstate(uint8_t RefundType) {

	AuditExtRefundKR(RefundType);																// RechargeFlags usata come flag in "Power.c" per finire la memorizzazione in caso di pwdown
	ResRevalue();																				// Clr solo variabili per revalue ma non le altre che serviranno all'Audit vendita
}

/*---------------------------------------------------------------------------------------------*\
Method: GestioneRevalue
	Aggiorna gli ID Audit relativi al tipo di Revalue effettuato sulla carta.

	IN:	  - TipoRevalue
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void GestioneRevalue(void) {

	switch (TipoRevalue) {

	case REVALUE_KR_CASH :
		RevalueKRCash();
		break;

	case REVALUE_KR_CARICAMENTO_BLOCCHI :
		RevalueKRCaricamentoBlocchi();
		break;

	case REFUND_IMMEDIATE_KR :
		RevalueKRReinstate(0x00);     
		break;

	case REFUND_DELAYED_KR :
		RevalueKRReinstate(0x01);     
		break;

	default:
		ResRevalue();
  }
}

/*--------------------------------------------------------------------------------------*\
Method:	RevalueKR
	Predispone variabili per generazione Audit dopo una rivalutazione della carta.

\*--------------------------------------------------------------------------------------*/
void RevalueKR(uint8_t Tipo, uint32_t valore, uint8_t flag) {
	
	PSHProcessingData();
	TipoRevalue 	= Tipo;
	StatoRevalue 	= 1;
	CashRecharged 	= valore;  
	RechargeFlags 	= flag;
	NewCreditKR 	= (uint16_t) gOrionKeyVars.Credit;						// Credito carta dopo il revalue
	NewFreeCreditKR = (uint16_t) gOrionKeyVars.FreeCredit;					// Free Credit carta
	NewFreeVendKR	= (uint16_t) gOrionKeyVars.FreeVend;						// Free Vend carta
	CodiceUtenteKR 	= (uint16_t) gOrionKeyVars.CodiceUtente;					// Codice Utente carta
	PSHCheckPowerDown();
}

