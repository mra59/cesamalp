/* ***************************************************************************************
File:
    Sicur.c

Description:
    Funzioni di sicurezza

History:
    Date       Aut  Note
    Ott 2016	MR   

 *****************************************************************************************/

#include "ORION.H" 
#include "Sicur.h"
#include "IICEEP.h"



/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void decipher(uint32_t*, const uint32_t*);
extern	void Read_UniqueID(uint32_t *DestCodes);
extern	bool WriteEEPI2C(uint16_t AddrEEP, uint8_t *AddrSource, uint16_t size);


/*--------------------------------------------------------------------------------------*\
Private types and variables
\*--------------------------------------------------------------------------------------*/

const uint32_t  T[] = {0x2B678235, 0x56CEA94F, 0x59A7D065, 0xEE41BD3F};							// Chiave di calcolo cifratura

/*---------------------------------------------------------------------------------------------*\
Method: Test_uP
	Controlla se la scheda CPU contiene i codici di sicurezza.
 
	IN:	  - 
	OUT:  - True = OK; False = False;
\*----------------------------------------------------------------------------------------------*/
bool  Test_uP(uint8_t *codici)
{
	uint32_t	Secure0, Secure1, cryptomsg[2];
	uint32_t	SNumH, SNumMH, SNumML, SNumL, SerNum[3];
	
	Read_UniqueID(&SerNum[0]);
	SNumH = FIXED_CODE;
	SNumMH = SerNum[2];
	SNumML = SerNum[1];
	SNumL = SerNum[0];
	
	// Decrypto i primi 8 bytes
	cryptomsg[0] = ((*(codici+3)<<24) | (*(codici+2)<<16) | (*(codici+1)<<8) | *codici);
	cryptomsg[1] = ((*(codici+7)<<24) | (*(codici+6)<<16) | (*(codici+5)<<8) | *(codici+4));
	Secure0 = cryptomsg[0];
	Secure1 = cryptomsg[1];
	decipher(cryptomsg, T);

	if ((cryptomsg[0] != SNumH) || (cryptomsg[1] != SNumMH))
	{
	  	return false;
	}

	// Decrypto i secondi 8 bytes
	cryptomsg[0] = ((*(codici+11)<<24) | (*(codici+10)<<16) | (*(codici+9)<<8) | *(codici+8));
	cryptomsg[1] = ((*(codici+15)<<24) | (*(codici+14)<<16) | (*(codici+13)<<8) | *(codici+12));
	decipher(cryptomsg, T);
	
	// XOR con i primi 8 bytes criptati
	cryptomsg[0] ^= Secure0;
	cryptomsg[1] ^= Secure1;

	if ((cryptomsg[0] != SNumML) || (cryptomsg[1] != SNumL))
	{
	 	return false;
	}
	else
	{
		return true;
	}
}

/*------------------------------------------------------------------------------------------*\
Method: encipher
  Cifra un messaggio di 64 bit (8 Bytes) con la chiave k di 128 bits (16 Bytes).
  La funzione dura circa 22,5 uSec @ 50MHz.
  
  IN:  - pointer al messaggio da cifrare e pointer alla chiave di cifratura.
 OUT:  - messaggio cifrato
\*------------------------------------------------------------------------------------------*/
#if CALC_SICUR_CODES
static void Encipher(uint32_t* cryptomsg, const uint32_t* k) {

	uint8_t		 num_rounds = 32;
    uint32_t	 v0=cryptomsg[0], v1=cryptomsg[1], i;
    uint32_t	 sum=0, delta=0x9E3779B9;

    for(i=0; i<num_rounds; i++) {
       v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + k[sum & 3]);
        sum += delta;
        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + k[(sum>>11) & 3]);
    }
    cryptomsg[0]=v0; cryptomsg[1]=v1;
}
#endif

/*------------------------------------------------------------------------------------------*\
Method: encipher

  IN:  - pointer al messaggio da cifrare e pointer alla chiave di cifratura.
 OUT:  - messaggio cifrato
\*------------------------------------------------------------------------------------------*/

#if CALC_SICUR_CODES

void cifra(void)
{
  	uint32_t	cryptomsg[2];
	uint8_t		Turing[16];
	uint16_t	ee_addr;
	uint32_t	SNumH, SNumMH, SNumML, SNumL, SerNum[3];
	
	Read_UniqueID(&SerNum[0]);
	SNumH = FIXED_CODE;
	SNumMH = SerNum[2];
	SNumML = SerNum[1];
	SNumL = SerNum[0];

	// Crypto i primi 8 bytes in cryptomsg
	cryptomsg[0] = SNumH;
	cryptomsg[1] = SNumMH;
	Encipher(cryptomsg, T);

	Turing[0] = cryptomsg[0];
	Turing[1] = cryptomsg[0]>>8;
	Turing[2] = cryptomsg[0]>>16;
	Turing[3] = cryptomsg[0]>>24;
	Turing[4] = cryptomsg[1];
	Turing[5] = cryptomsg[1]>>8;
	Turing[6] = cryptomsg[1]>>16;
	Turing[7] = cryptomsg[1]>>24;

	// Crypto i secondi 8 bytes dopo aver eseguito l'Xor con i primi 8 bytes criptati
	cryptomsg[0] ^= SNumML;
	cryptomsg[1] ^= SNumL;
	Encipher(cryptomsg, T);

	Turing[8]  = cryptomsg[0];
	Turing[9]  = cryptomsg[0]>>8;
	Turing[10] = cryptomsg[0]>>16;
	Turing[11] = cryptomsg[0]>>24;
	Turing[12] = cryptomsg[1];
	Turing[13] = cryptomsg[1]>>8;
	Turing[14] = cryptomsg[1]>>16;
	Turing[15] = cryptomsg[1]>>24;
	
	ee_addr = AlTur(Data[0]);
	WriteEEPI2C(ee_addr, (uint8_t *)(&Turing[0]), sizeof(Turing));
	
}
#endif	// End CALC_SICUR_CODES










