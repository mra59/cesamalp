/**
  ******************************************************************************
  * @file    file_operations.c 
  * @author  Abe - (C) COPYRIGHT AbTech september 2019
  * @brief   USB file functions
  ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include "main.h"
#include "ff.h"
#include "ff_gen_drv.h"
#include "usbh_diskio_dma.h"
#include "ORION.H"
#include "file_operations.h"
#include "IICEEP.h"

#include "DDCMPHook.h"
#include "DDCMPDTSHook.h"
#include "AuditDataDescr.h"
#include "Audit.h"
#include "Monitor.h"
#include "VMC_CPU_LCD.h"



/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void  RedLedCPU(uint8_t On_Off);
extern	void  RedLedCPU_Toggle(void);
extern	void  GreenLedCPU(uint8_t On_Off);
extern	void  Buz_start(uint32_t time, uint32_t tono1, uint32_t tono2);

extern	const char FileNameRxConf_String[];
extern	const char FileNameTxConf_String[];
extern	const char FileNameAudit_String[];
extern	const char FileNameLOG_String[];

extern	const  char mKeyOK[];
extern	const  char mFileErr[];	
extern	const  char mNoFileCfw[];
extern	const  char mNoDIR[];	
extern	const  char mPSWErr[];	
extern	const  char mDirFull[];	
extern	const  char mAudDenied[];
extern	const  char mNoLogData[];
extern	const  char mErrEEProm[];

extern	const  char mKeyOK_2x16[];
extern	const  char mFileErr_2x16[];	
extern	const  char mNoFileCfw_2x16[];
extern	const  char mNoDIR_2x16[];	
extern	const  char mPSWErr_2x16[];	
extern	const  char mDirFull_2x16[];	
extern	const  char mAudDenied_2x16[];
extern	const  char mNoLogData_2x16[];
extern	const  char mErrEEProm_2x16[];

extern 	bool  EEPROM_Write (uint16_t device, uint16_t MemAddr, uint8_t* EEBuff, uint8_t size);
extern 	bool  EEPROM_Read (uint16_t device, uint16_t EE_Address, uint8_t *DestBuff, uint8_t size);
extern	bool  WriteEEPI2C(uint16_t AddrEEP, uint8_t *AddrSource, uint16_t size);
extern	bool  ReadEEPI2C(uint16_t AddrEEP, uint8_t *AddrDest, uint16_t size);
extern	void  Rd_EEBuffStartAddr_And_BankSelect(uint8_t);
extern	void  LeggiOrologio(void);
extern	void  Set_DDCMP_EnableAuditClear(void);
extern	void  SetBlockTypeZero(void);

extern	void  BankSelect(uint8_t bank);
extern	void  VMC_USB_KEY_Msg(void);
extern	void  Message_In_Message(const char* pntMsgIn, uint8_t RowNum);
extern	bool  NewCPU, NewConfigFromKeyUSB;


#if AWS_CONFIG
	extern	USB_Key_Err Import_AWS_Conf(void);
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
FATFS USBH_fatfs;
FIL MyFile;
FRESULT res;
uint32_t bytesWritten;


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint16_t tx_file_baseadd;
uint8_t tx_file_bank;
uint16_t rx_file_baseadd;
uint8_t rx_file_bank;
uint16_t aud_file_baseadd;
uint8_t aud_file_bank;
uint16_t log_file_baseadd;
uint8_t log_file_bank;

//char app_fw_rev[4];

char fw_namefile[32+2+3];
char cfrx_namefile[32+2+3];
char cftx_namefile[32+2+3];
char audit_namefile[32+2+3];
char log_namefile[32+2+3];

char usb_name[40];   //nome del file da scrivere su chiave USB --> usato per inc_nome

uint8_t len_cfrx_namefile;
uint8_t len_cftx_namefile;
uint8_t len_audit_namefile;
uint8_t len_log_namefile;

uint8_t len_usb;

uint32_t add_eeprom;
uint8_t  rowlen;
uint32_t endadd;
uint8_t  row_pointer;
uint8_t  fase;

uint32_t err_fs = 0;
uint8_t error = 0;

char volname[16];

/* Private constant ---------------------------------------------------------*/

const char UnderScore[]  = {"_"};

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------*\
Method: Import_Conf
	Legge la configurazione dalla chiave USB e la memorizza nella EEProm

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
static USB_Key_Err Import_Conf(void)
{
    USB_Key_Err		resp;
  	DIR 			dir;
    FILINFO 		fno;
    char 			conf_path[] = "";

	CompFileName 	ResComp;
    uint8_t 		maxcp=255;					//default security --> max files checked into root, to find default "OrionPK3_FW"
	static uint8_t 	i;
	static uint8_t 	result;
	FRESULT 		returnCode;                 // Result code
	FIL     		fp;                         // File pointer
	uint32_t  		size;
	uint8_t  		buffer[BUFFER_LENGTH];      // Buffer for parse
	uint8_t 		valid_pw;
	
	//-- Fino a 3 tentativi di verificare se la chiave USB ha il file system --
	for (i=0; i < 3; i++)
	{
    	returnCode = f_opendir(&dir, conf_path);
		if (returnCode != FR_NO_FILESYSTEM) break;
    }
	//-- Predispongo nomefile .cfw nella stringa cfrx_namefile --
	memset(&cfrx_namefile, 0, sizeof(cfrx_namefile));
	len_cfrx_namefile = strlen(FileNameTxConf_String);
	memcpy(&cfrx_namefile[0], FileNameTxConf_String, len_cfrx_namefile);						// Risultato: "FileName_"
	
	// -- Merge della password di sistema letta dalla EE nel nomefile dopo il chr "_" --
	// -- Se e' diversa da zero accetto solo filenames con password identica, alrimenti--
	// -- accetto qualsiasi file di configurazione: serve ad inizializzare --
	// -- automaticamente la password al primo uso del prodotto --
	valid_pw = false;																			//  Accetto file config con qualsiasi password
	if (AccPsw)
	{
		valid_pw = true;																		// Accetto solo file config con password esatta
	}
	sprintf(&cfrx_namefile[len_cfrx_namefile], "%05d.cfw", AccPsw);								// Aggiungo psw e creo la stringa "FileName_xxxxx.cfw\0"
	len_cfrx_namefile = strlen(cfrx_namefile);
	
	//-- Determino address EE dove salvare la nuova configurazione --
	//-- Predispone anche il Bank della EE --
	Rd_EEBuffStartAddr_And_BankSelect(BUF_RXCONFIG);											// In MemoryBytes[] indirizzo di inizio del Buffer RxConfig
	rx_file_baseadd = 0;				
	rx_file_baseadd += MemoryBytes[1];															// LSB
	rx_file_baseadd += (MemoryBytes[0]<<8);														// MSB

	//-- Lettura nomefiles presenti su chiave USB e comparazione con cfrx_namefile--
	//-- La comparazione e' case-sensitive --
	for (i=0; i<maxcp; i++)
	{
		returnCode = f_readdir(&dir, &fno);
        if (returnCode != FR_OK || fno.fname[0] == 0)
		{
			//f_close(&fplog);  
			resp = ERR_NO_CONFIG_FILE;															// Non ci sono files .cfw di configurazione sulla chiave
        	goto EndImportConf;
		}
        i = 14;
        if (valid_pw == false)i = 9;		         											// If VMC PSW = 0, get configuration without check. Any file .cfw is valid!!!
        ResComp = (CompFileName)memcmp(cfrx_namefile, fno.fname, i);	 						// Compare filename with default configuration file suffix "FileName_00000"
        if(ResComp == FILENAME_COMPARE_OK)
		{
        	if ((strstr(fno.fname, "cfw"))) break;												// check if filename with correct suffix terminates with "cfw" string
        	if (strstr(fno.fname, "lnn"))														// check if filename with correct default suffix terminates with "lnn" string
			{
				// Determino address inizio Buffer EEProm dove memorizzare il file generico				
				Rd_EEBuffStartAddr_And_BankSelect(BUF_GENERIC_FILE);							// In MemoryBytes[] StartAddress del buffer e bank della EE
				rx_file_baseadd = 0;				
				rx_file_baseadd += MemoryBytes[1];												// LSB
				rx_file_baseadd += (MemoryBytes[0]<<8);											// MSB
        	}
        }
    }
	returnCode = f_open(&fp, fno.fname, FA_READ);
	if(returnCode == FR_NO_FILESYSTEM)
	{
		resp = ERR_RW_FILE;
        goto EndImportConf;
	}
	if (returnCode != FR_OK)
	{
		resp = ERR_RW_FILE;
        goto EndImportConf;
	}
    else 
    {
    	add_eeprom = rx_file_baseadd;
		SetLed(LED_VERDE, false);
		SetLed(LED_ROSSO, true);
		resp = USB_ERR_OK;
		// --  Loop trasferimento file di configurazione in EEprom ---
		while(true)
		{
			//-- Get BUFFER_LENGTH bytes and parse for programming --
			returnCode = f_read(&fp, buffer, BUFFER_LENGTH, &size);								// La f_read rende il buffer e il suo size
			if (returnCode)
			{
			  	buffer[0]= NULL;
				WriteEEPI2C(rx_file_baseadd, &buffer[0], 1U);									// Azzero il primo byte del buffer EE per disattivarne l'uso
				resp = ERR_RW_FILE;
				break;
			}
			if(size == 0)
			{
				//-- Trasferimento completato, aggiungo chr di chiusura buffer EE --
			  	buffer[0]= NULL;
				WriteEEPI2C(add_eeprom, &buffer[0], 1U);
				break;
			}
			else
			{	
				//-- Save buffer from file to EE --
			  	result = EE_Block(buffer, size);												// parse and flash an array to EEprom memory 
				if(result == false) 
				{
			  		buffer[0]= NULL;
					WriteEEPI2C(rx_file_baseadd, &buffer[0], 1U);								// Write FAIL, azzero il primo byte del buffer EE per disattivarne l'uso
					resp = ERR_EE_WRITE;
					break;
				}
			}
		}
    }
	returnCode = f_close(&fp);  

EndImportConf:	
	SetLed(LED_VERDE, true);
	SetLed(LED_ROSSO, false);
	return resp;
}


/*---------------------------------------------------------------------------------------------*\
Method: Export_conf
	Legge la configurazione dalla EEProm e crea il file "FileName.cfr" sulla chiave USB.
	Se il nome volume della chiave inserita e' "NomeVolumeCOPY", contestualmente al file .cfr
	nella directory "Config" viene creato un file .cfw della directory radice.

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
static USB_Key_Err Export_conf(void)
{
const char Conf_psw_Msg[] = {"Conf_%05d.pwd"};

    USB_Key_Err	resp;
	uint8_t 	i;
    uint32_t  	datalen;
    char 		conf_path[] = "Config";			// Directory da ricercare sulla chiave USB
    char 		conf_pw[16];
    char 		cpname[] = "ALPCOPY";			// Nome Volume per creare una chiave USB di copia configurazione
    
    FRESULT     returnCode;						// Result code
    FIL         fp;								// File pointer
    FIL			fp1;    
    uint32_t    size;
    uint8_t     buffer[BUFFER_LENGTH];			// Buffer for parse
    bool        copia;
    uint32_t    sn;
    
    // controllo presenza directory "Config"
    returnCode = f_chdir(conf_path);
	if(returnCode)
	{
		resp = ERR_NO_DIR;																		// Chiave senza richiesta di Export-Config
		goto EndExportConf;
    }
    
    //-- Inserisco password di sistema al nome file per autorizzazione prelievo configurazione --
	memset(&conf_pw, 0, sizeof(conf_pw));
	datalen = (strlen(Conf_psw_Msg) + 2);
	snprintf((char *)&conf_pw[0], datalen, Conf_psw_Msg, AccPsw);								// Risultato: "Conf_xxxxx.pwd"
    
    // verifico se presente file con superpassword
    returnCode = f_open(&fp, "Cf_ffffffff", FA_READ);
    f_close(&fp);
    if(returnCode)
	{
		// verifico se presente file con password corretta
		returnCode = f_open(&fp, conf_pw, FA_READ);
		f_close(&fp);
		if(returnCode)
		{
			resp = ERR_PSW;																		// Exit error
			goto EndExportConf;
		} 
	}
	
    // se il nome del volume = ALPCOPY set flag fare copia del file di configurazione letto.
    returnCode = f_getlabel("", volname, (DWORD*)&sn );
    for (i=0; i < sizeof(cpname); i++)
	{
    	copia = true;
    	if (volname[i] != cpname[i])
		{
    		copia = false;
    		break;
    	}
    }
    
	//**************************************************************************************
	//******  Crea  Chiave USB Copia Configurazione creando file .cfw               ********
	//**************************************************************************************
	if (copia == true)
	{
	  	//-- Crea chiave copia configurazione creando file *.cfw --
		len_cfrx_namefile = strlen(FileNameTxConf_String);
		memcpy(&cfrx_namefile[0], FileNameTxConf_String, len_cfrx_namefile);					// Risultato: "NomeFile_"
		sprintf(&cfrx_namefile[len_cfrx_namefile], "%05d.cfw", AccPsw);							// Aggiungo psw e creo la stringa "NomeFile_xxxxx.cfw\0"
		len_cfrx_namefile = strlen(cfrx_namefile);
		returnCode = f_chdir("/");
    	returnCode = f_open(&fp1, cfrx_namefile, FA_CREATE_NEW | FA_WRITE);
    	if (returnCode != FR_OK)
		{
    		copia = false;
    		returnCode = f_close(&fp1);
			resp = ERR_RW_FILE;																	// Exit error
			goto EndExportConf;
    	}
        returnCode = f_chdir(conf_path);
	}
	//**************************************************************************************
	//-- Predispongo nomefile .cfr nella stringa cftx_namefile --
	len_cftx_namefile = strlen(FileNameRxConf_String);
	memcpy(&cftx_namefile[0], FileNameRxConf_String, len_cftx_namefile);						// Risultato: "NomeFile.cfr\0"
	cftx_namefile[len_cftx_namefile + 1] = NULL;												// Chr di chiusura stringa

	copyname(cftx_namefile, usb_name, len_cftx_namefile);
 	//-- Loop eventuale aggiunta indice (x) al nomefile su chiave USB--
	while ((returnCode = f_open(&fp, usb_name, FA_CREATE_NEW | FA_WRITE)) == FR_EXIST)
	{
		if (!(inc_nome(usb_name, &len_usb)))
		{
		 	break;
		}
	}
	if (!returnCode)
	{
		//-- Determino address EE da dove prelevare il file di configurazione --
		//-- Predispone anche il Bank della EE --
		Rd_EEBuffStartAddr_And_BankSelect(BUF_TXCONFIG);										// In MemoryBytes[] indirizzo di inizio del Buffer TxConfig
		tx_file_baseadd = 0;				
		tx_file_baseadd += MemoryBytes[1];														// LSB
		tx_file_baseadd += (MemoryBytes[0]<<8);													// MSB
		
		add_eeprom = tx_file_baseadd;
		SetLed(LED_VERDE, false);
		resp = USB_ERR_OK;
		//-- Loop lettura Configurazione da EEprom per trasferimento su chiave USB --
		while ((datalen = EE_cfg_read(&buffer[0])) > 0)
        {
			RedLedCPU_Toggle();
			returnCode = f_write(&fp, buffer, datalen ,&size); 
        	if (returnCode != FR_OK)
			{
        		returnCode = f_close(&fp);
				resp = ERR_RW_FILE;																// Exit error
				goto EndExportConf;
        	}
			if (copia == true)
			{
            	returnCode = f_chdir("/");
        		returnCode = f_write(&fp1, buffer, datalen ,&size);
            	if (returnCode != FR_OK)
				{
            		returnCode = f_close(&fp);
					resp = ERR_RW_FILE;															// Exit error
					goto EndExportConf;
            	}
        	}
        }
		SetLed(LED_ROSSO, false); 
    }
	else
	{
		resp = ERR_FULL_DIR;																	// Exit error
		goto EndExportConf;
	}
	if (copia == true)
	{
    	returnCode = f_close(&fp1);
        res = set_timestamp((char*)&cfrx_namefile);
    	returnCode = f_chdir(conf_path);														// Ripristino directory "Config" altrimenti fallisce registrazione data/ora file .cfr
	}
	returnCode = f_close(&fp);
	res = set_timestamp((char*)&usb_name);
	if (res != FR_OK)
	{
		err_fs++;
	}

EndExportConf:
	returnCode = f_chdir("/");																	// Set directory prinicpale
  	SetLed(LED_VERDE, true);
	SetLed(LED_ROSSO, false);
	return resp;
}


/*---------------------------------------------------------------------------------------------*\
Method: Export_Audit
	Genera l'Audit in formato EVA-DTS e crea il file "NomeFileAud_xxxxxxxx.txt" sulla chiave USB.
	*** ATTENZIONE *** Il prelievo Audit azzera gli ID LR.

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
static USB_Key_Err Export_Audit(void)
{
const char Aud_psw_Msg[]   	= {"Audit_%05d.pwd"};
const char Closing_Chars[] 	= {"DXE*1*1\r\n\r\n"};
const char audit_path[]		= "Audit";

    USB_Key_Err		resp;
	uint16_t		datalen;
	char 			audit_pwd[16];
	char			*bufpnt;

	FRESULT 		returnCode;                 // Result code
	FIL     		fp;                         // File pointer
	uint32_t  		size;
	uint8_t  		buffer[BUFFER_LENGTH];      // Buffer for parse

	// controllo presenza directory "Audit"
	returnCode = f_chdir(audit_path);
	if(returnCode)
	{
		resp = ERR_NO_DIR;																		// Chiave senza richiesta di Export-Audit
		goto EndExportAudit;
	}
	if (AuditTransferAllowed() == false)
	{
		// -- Prelievo Audit non disponibile --
		resp = ERR_AUDIT_DENIED;																// Probabile credito presente nel sistema che impedisce il prelievo
		goto EndExportAudit;
	}
	
	//-- Aggiungo password Audit al nome file per autorizzazione prelievo audit --
	memset(&audit_pwd, 0, sizeof(audit_pwd));
	datalen = (strlen(Aud_psw_Msg) + 2);
	snprintf((char *)&audit_pwd[0], datalen, Aud_psw_Msg, AudPsw);								// Risultato: "Audit_xxxxx.pwd"
	
	//-- Aggiungo codice macchina al nome file Audit Destinazione --
	memset(&audit_namefile, 0, sizeof(audit_namefile));
	len_audit_namefile = strlen(FileNameAudit_String);
	memcpy(&audit_namefile[0], FileNameAudit_String, len_audit_namefile);						// Risultato: "NomeFileAud_"

#if (CodMacch_10Cifre == false)
	sprintf(&audit_namefile[len_audit_namefile], "%08d.txt", CodiceMacchina);					// Aggiungo Codice Macchina e creo la stringa "NomeFileAud_xxxxxxxx.txt\0"
	len_audit_namefile = strlen(audit_namefile);
#endif
	
	// verifico se presente file con superpassword
	returnCode = f_open(&fp, "Au_ffffffff", FA_READ);
	f_close(&fp);
	if(returnCode)
	{
		// verifico se presente file con password corretta
		returnCode = f_open(&fp, audit_pwd, FA_READ);
		f_close(&fp);
		if(returnCode)
		{
			resp = ERR_PSW;																		// Exit error
			goto EndExportAudit;
		}
	}
	copyname(audit_namefile, usb_name, len_audit_namefile);
	//-- Loop eventuale aggiunta indice (x) al nomefile su chiave USB--
	while ((returnCode = f_open(&fp, usb_name, FA_CREATE_NEW | FA_WRITE)) == FR_EXIST)
	{
		if (!(inc_nome(usb_name, &len_usb)))
		{
			break;
		}
	}
	if (returnCode)
	{
		returnCode = f_close(&fp);
		resp = ERR_FULL_DIR;																	// Exit error
		goto EndExportAudit;
	}
	else
	{
		resp = USB_ERR_OK;
		memset(&buffer, 0, sizeof(buffer));
		SetLed(LED_VERDE, false);
		
		//**************************************************************************************
		//******  Creazione Audit EVA-DTS e scrittura file  NomeFileAud_xxxxxxxx.txt    ********
		//**************************************************************************************
		
		if (DDCMPDTSHookReadDataReqInd(AUDIT_LIST, 0, 0, 0) == true)
		{																							// Lista Audit set e init di tutte le variabili, orologio compreso
			bufpnt = (char*)&buffer[0];
			Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));											// Init Buffer destinazione Audit and counters
			// Dal loop puo' uscire in 2 modi:
			// 1) AuditLoopBody() == false perche' ha finito la scansione degli ID: 
			//    trasferire Buffer su chiave USB aggiungendo DXE di chiusura
			// 2) uData.audit.localBufferSize != 0 perche' il buffer e' pieno e l'ultimo dato letto non ci sta:
			//    trasferire su chiave USB il buffer audit e copiare successivamente l'ultimo dato letto nel buffer
			
			do {
				//  ********   Audit   Standard    ************
				RedLedCPU_Toggle();																	// Toggle Led Rosso
				if (AuditLoopBody() == false)														// Audit Standard END
				{
					if (gOrionConfVars.AuditExt == true)											// Trasmettere anche l'Audit Estesa
					{
						//  ********   Audit   Estesa    ************
						do {
								if (Extended_Audit_LoopBody() == false)
								{
									break;															// Audit Estesa END
								}
								if (Get_localBufferSize() != 0)										// Buffer destinazione Audit pieno e l'ultimo ID appena letto non ci sta
								{
									//--  Trasferimento intermedio Buffer Audit  --
									datalen = Get_dataBuffTransmitted();							// Lunghezza del buffer da trasferire su USB
									returnCode = f_write(&fp, buffer, datalen, &size);
									if (returnCode != FR_OK)
									{
										returnCode = f_close(&fp);
										resp = ERR_RW_FILE;											// Exit error
										goto EndExportAudit;
									}
									else
									{
										memset(&buffer, 0, sizeof(buffer));
										//bufpnt = (char*)&buffer[0];
										Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));				// Init Buffer destinazione Audit and counters
									}
									DDCMPMngrHookCopyLocalBuffer();									// Trasferisco ultimo ID appena letto nel buffer audit
								}
					  	} while(true);
					}
				  
					//***********************************************				  
				  	//--  Fine File Audit, aggiungo DXE e termino --
					//***********************************************				  
					
					datalen = Get_dataBuffTransmitted();											// Lunghezza del buffer da trasferire su USB
					if (Get_dataBuffFree() < sizeof(Closing_Chars))
					{
						returnCode = f_write(&fp, buffer, datalen, &size);							// L'ID chiusura non ci sta nel buffer: copio il buffer in EEPROM e poi aggiungo DXE
						if (returnCode != FR_OK)
						{
							returnCode = f_close(&fp);
							resp = ERR_RW_FILE;														// Exit error
							goto EndExportAudit;
						}
						else
						{
							bufpnt = (char*)&buffer[0];
							datalen = 0;
							Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));							// Init Buffer destinazione Audit and counters
						}
					}
					snprintf((char *)&buffer[datalen], sizeof(Closing_Chars), Closing_Chars);		// "DXE*1*1\r\n\r\n\0" a chiusura del file Audit
					datalen += (sizeof(Closing_Chars) -1); 
					returnCode = f_write(&fp, buffer, datalen, &size);								// Completo scrittura file su USB
					if (returnCode != FR_OK)
					{
						returnCode = f_close(&fp);
						resp = ERR_RW_FILE;															// Exit error
						goto EndExportAudit;
					}
					else
					{
						break;
					}
				}
				if (Get_localBufferSize() != 0)														// Buffer destinazione Audit pieno e l'ultimo ID appena letto non ci sta
				{
					//--  Trasferimento intermedio Buffer Audit  --
				  	datalen = Get_dataBuffTransmitted();											// Lunghezza del buffer da trasferire su USB
					returnCode = f_write(&fp, buffer, datalen, &size);
					if (returnCode != FR_OK)
					{
						returnCode = f_close(&fp);
						resp = ERR_RW_FILE;															// Exit error
						goto EndExportAudit;
		    		}
					else
					{
					  	bufpnt = (char*)&buffer[0];
						Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));								// Init Buffer destinazione Audit and counters
					}
					DDCMPMngrHookCopyLocalBuffer();													// Trasferisco ultimo ID appena letto nel buffer audit
				}
			} while(true);
		}
		else
		{
			// -- Prelievo Audit non disponibile --
		  	returnCode = f_close(&fp);																// Probabile credito presente nel sistema che impedisce il prelievo
			resp = ERR_AUDIT_DENIED;																// Exit error
			goto EndExportAudit;
		}
	}
	returnCode = f_close(&fp);
	res = set_timestamp((char*)&usb_name);															// Aggiungo data/ora al file Audit
	Set_DDCMP_EnableAuditClear();																	// Registro data/ora prelievo attuale e marca per abilitare azzeramento Audit al successivo Main
	buffer[0] = AUDIT_USB_READY;
	WriteEEPI2C(AuditFileReady, (byte *)(&buffer), 1U);												// Scrittura marca Buffer Audit Ready per successivo azzeramento dati parziali UP
	Clear_fDDCMPTransferInProgress;																	// Riabilita sistemi di pagamento


EndExportAudit:
	returnCode = f_chdir("/");																		// Set directory prinicpale
	SetLed(LED_VERDE, true);
	SetLed(LED_ROSSO, false);
	return resp;
}


/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : Export_Log
* Returned Value : none
* Comments       : Read Log from EEPROM, convert in ascii and write it into USB stick
*     
*
*END*--------------------------------------------------------------------*/
static USB_Key_Err Export_Log(void)
{
const char Log_psw_Msg[]   	= {"Log_%05d.pwd"};
const char log_path[]		= {"Logdir"};					// Directory da ricercare sulla chiave USB

    USB_Key_Err	resp;
	uint16_t  	datalen;
	uint32_t	loglen = 0;
	uint32_t	logsize = 0;
	char 		log_pwd[16];
	
	FRESULT     returnCode;					// Result code
    FIL         fp;							// File pointer
    uint32_t    size;
    uint8_t     buffer[BUFFER_LENGTH];		// Buffer for parse

	//-- Controllo presenza directory "Logdir" --
	returnCode = f_chdir(log_path);
	if(returnCode)
	{
		resp = ERR_NO_DIR;																		// Chiave senza richiesta di Export-LOG
		goto EndExportLog;
	}
	//-- Aggiungo password di sistema al nome file per autorizzazione prelievo log --
	memset(&log_pwd, 0, sizeof(log_pwd));
	datalen = (strlen(Log_psw_Msg) + 2);
	snprintf((char *)&log_pwd[0], datalen, Log_psw_Msg, AccPsw);								// Risultato: "Log_xxxxx.pwd"
	
	//-- Aggiungo codice macchina al nome file Log Destinazione --
	memset(&log_namefile, 0, sizeof(log_namefile));
	len_log_namefile = strlen(FileNameLOG_String);
	memcpy(&log_namefile[0], FileNameLOG_String, len_log_namefile);								// Risultato: "NomeFileLog_"

#if (CodMacch_10Cifre == false)
	sprintf(&log_namefile[len_log_namefile], "%08d.log", CodiceMacchina);						// Aggiungo Codice Macchina e creo la stringa "NomeFileLog_xxxxxxxx.txt\0"
	len_log_namefile = strlen(log_namefile);
#endif
	
	// verifico se presente file con password corretta
	returnCode = f_open(&fp, log_pwd, FA_READ);
	f_close(&fp);
	if(returnCode)
	{
		resp = ERR_PSW;																			// Exit error
		goto EndExportLog;
	}
	loglen = ReadLogFileContentLen();															// read log file lengh
    if (loglen == 0)
	{
		resp = ERR_NO_DATA_LOG;																	// Exit error: log file lenght = 0 --> nothing to do
		goto EndExportLog;
    }
	ReadEEPI2C(LogFileSize, (uint8_t *)(&MemoryBytes[0]), 3U);									// In logsize dimensioni del Log Buffer in EEProm
	logsize = MemoryBytes[0];																	// LSB
	logsize += (MemoryBytes[1]<<8);																// Medium
	logsize += (MemoryBytes[2]<<16);															// MSB
	ReadEEPI2C(LogFileSize, &rowlen, 1U);														// read row len
    if (loglen > logsize) loglen = logsize;
		
	copyname(log_namefile, usb_name, len_log_namefile);
	//-- Loop eventuale aggiunta indice (x) al nomefile su chiave USB--
	while ((returnCode = f_open(&fp, usb_name, FA_CREATE_NEW | FA_WRITE)) == FR_EXIST)
	{
		f_close(&fp);  
        if (!(inc_nome(usb_name, &len_usb)))
		{
			break;
		}
	}
	if (returnCode)
	{
		returnCode = f_close(&fp);
		resp = ERR_FULL_DIR;																	// Exit error
		goto EndExportLog;
	}
	else
	{
		//-- Determino address EE da dove prelevare il file Log --
		//-- Predispone anche il Bank della EE --
		Rd_EEBuffStartAddr_And_BankSelect(BUF_LOGFILE);											// In MemoryBytes[] indirizzo di inizio del Buffer log
		tx_file_baseadd = 0;				
		tx_file_baseadd += MemoryBytes[1];														// LSB
		tx_file_baseadd += (MemoryBytes[0]<<8);													// MSB
		
		add_eeprom = tx_file_baseadd;
		endadd = add_eeprom + loglen;
		SetLed(LED_VERDE, false);
		resp = USB_ERR_OK;
		row_pointer = 0;
		
		//-- Loop lettura LOG File da EEprom per trasferimento su chiave USB --
		while ((datalen = EE_log_read(&buffer[0])) > 0)
		{
			RedLedCPU_Toggle();
			returnCode = f_write(&fp, buffer, datalen ,&size); 
			if (returnCode != FR_OK)
			{
				returnCode = f_close(&fp);
				resp = ERR_RW_FILE;																// Exit error
				goto EndExportLog;
			}
		}
		SetLed(LED_ROSSO, false); 
	}
	returnCode = f_close(&fp);
	res = set_timestamp((char*)&usb_name);														// Aggiungo data/ora al file Log
	
#if (NO_CLEAR_LOG == 0)
	memset(&log_pwd, 0, 4U);																	// Array usato per azzerare il LogFile counter
	WriteEEPI2C(LogFileContentLen, (uint8_t *)(&log_pwd[0]), 4U);								// Azzero LogFile counter
#endif

EndExportLog:
  	SetLed(LED_VERDE, true);
	SetLed(LED_ROSSO, false);
	return resp;
}

/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : copynome
* Returned Value : true, false
* Comments       : copy name file into usb name buffer
*     
*
*END*--------------------------------------------------------------------*/
static void copyname (char *src, char * dst, uint8_t size)
{
	len_usb = size;
	memcpy (dst, src, size+2);
}

/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : inc_nome
* Returned Value : true, false
* Comments       : increment file name index
*     
*
*END*--------------------------------------------------------------------*/
static bool inc_nome(char nf[], uint8_t *size)
{
	uint8_t i,s;
	uint8_t len = *size;
	
	for (i=0;i<len;i++){
		if((nf[i]=='.') || (nf[i] == '(')){
			switch (nf[i]){
			case '.':
				for (s=len;s>=i;s--){
					nf[3+s] = nf[s];
					}
					nf[i] = '(';
					i++;
					nf[i] = '1';
					i++;
					nf[i] = ')';
					i++;
					nf[i+5] = 0xaa;
							
					*size = i+3;
					
					return true;

			case '(':
				s=i+1;
			}
			break;
			}
		}
	if (i==len)return false;

	if ((nf[s]++) > '9')return false; 

return true;

}

/*------------------------------------------------------------------------------------------*\
 Method: EE_Block
	Scrittura e rilettura di verifica di un blocco dati nella EEprom.
	Se la rilettura conferma la correttezza dei dati memorizzati, si incrementa l'address
	EEprom.

   IN:  - eeprom bank, source buffer pointer, buffer size
  OUT:  - address eeprom incrementato del block size se scrittura OK
\*------------------------------------------------------------------------------------------*/
static uint8_t EE_Block(uint8_t *arr, uint32_t size)
{	
	uint16_t 	EEAddr, i;
	uint8_t		result;
	
	EEAddr = add_eeprom;																		// Param predisposto prima della chiamata funzione
	result = false;
	
	if (WriteEEPI2C(EEAddr, arr, (uint16_t)size) == true)										// write data from file buffer to EEprom
	{
		result = true;
		ReadEEPI2C(EEAddr, (uint8_t*)&gVars.ddcmpbuff, (uint16_t)size);							// data verify --> read and compare
		for (i=0; i < size; i++)
		{
			if (*(arr + i) != gVars.ddcmpbuff[i])
			{
				result = false;
				break;
			}
		}
	}
	if (result == true)
	{
		add_eeprom += size;																		// Write OK, aggiorno address EEprom
	}
	return result;
}


/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : EE_cfg_read
* Returned Value : number of read char
* Comments       : read bytes from EE until byte 0 found or BUFFER_LENGTH
*     				reached
*
*END*--------------------------------------------------------------------*/
static uint32_t EE_cfg_read(uint8_t* buf)
{
	uint32_t 	i;
	
	for (i=0; i < BUFFER_LENGTH; i++)
	{
		ReadEEPI2C((uint16_t)add_eeprom, (buf+i), 1U);
		if (*(buf+i) == 0)
		{
			break;
		}
		add_eeprom++;
	}	  
	return i;
}


/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : EE_log_read
* Returned Value : number of char readed
* Comments       : get from file buffer into EEprom and place in USB stick file
*     
*
*END*--------------------------------------------------------------------*/
static uint16_t EE_log_read(uint8_t* buf)
{
	uint8_t		data;
	uint16_t 	numchar = 0;
	
	do
	{
		ReadEEPI2C((uint16_t)add_eeprom, (&data), 1U);											// Legge un chr hex dalla EEprom
		*(buf + numchar++) = hex_to_ascii((data >> 4));											// Trasforma il nibble HIGH in un chr ASCII e lo memorizza nel buffer destinazione, incrementa il counter dati inseriti nel buffer
		row_pointer ++;																			// Incrementa pointer byte inseriti nel buffer prima di un NewLine
		if (numchar == BUFFER_LENGTH) break;

		*(buf + numchar++) = hex_to_ascii(data);												// Trasforma il nibble LOW in un chr ASCII e lo memorizza nel buffer destinazione, incrementa il counter dati inseriti nel buffer
		add_eeprom++;
		row_pointer ++;																			// Incrementa pointer byte inseriti nel buffer prima di un NewLine
		if (numchar == BUFFER_LENGTH) break;
		if (row_pointer == (rowlen*2))
		{
			row_pointer = 0;																	// Ogni rowlen caratteri inserisco NL
			*(buf + numchar++) = ASCII_CR;
			if (numchar == BUFFER_LENGTH) break;
			*(buf + numchar++) = ASCII_LF;
			if (numchar == BUFFER_LENGTH) break;
		}
		if (add_eeprom == endadd) break;
	} while(true);
	
	return numchar;
}
	
/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : SetLed
* Returned Value : None
* Comments       : Set LED state true = led on; false = led off
*     
*END*--------------------------------------------------------------------*/
static void SetLed(uint32_t output, bool state)
{
	if (output == LED_ROSSO) RedLedCPU(state);
	if (output == LED_VERDE) GreenLedCPU(state);
}

/*---------------------------------------------------------------------------------------------*\
Method: set_timestamp
	Legge i dati dall'orologio interno del uP e li aggiunge al file salvato sulla chiave USB
	Non so per quale motivo la funzione "f_utime" aggiunge 60 anni alla data passata come
	parametro.

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
static FRESULT set_timestamp (char *obj)
{
    FILINFO fno;

	LeggiOrologio();
	if (DataOra.Year > 60) DataOra.Year -= 60;
    fno.fdate = (WORD)(((DataOra.Year) << 9) | DataOra.Month << 5 | DataOra.Day);
    fno.ftime = (WORD)(DataOra.Hour <<11 | DataOra.Minute <<5 | DataOra.Second/2 );
    return f_utime(obj, &fno);
}

/*------------------------------------------------------------------------------------------*\
 Method: Select_Message
	Seleziona il messagio da visualizzare su dislay LCD

   IN:  -  numero messaggio da visualizzare
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void Select_Message(USB_Key_Err MsgNum, uint8_t RowNum)
{
	if (gOrionConfVars.LCD_Type == LCD_4x20)
	{
		switch(MsgNum)
		{

#if AWS_CONFIG
			case USB_AWS_ERR_OK:
				Message_In_Message(&mKeyOK[0], RowNum);
				break;
#endif
			case USB_ERR_OK:
				Message_In_Message(&mKeyOK[0], RowNum);
				break;
			  
			case ERR_NO_CONFIG_FILE:
				Message_In_Message(&mNoFileCfw[0], RowNum);
				break;
		  
			case ERR_RW_FILE:
				Message_In_Message(&mFileErr[0], RowNum);
				break;
			  
			case ERR_NO_DIR:
				//Message_In_Message(&mNoDIR[0], RowNum);											// Lascio i trattini nel messaggio perche' il servizio non e' richiesto da questa chiave
				break;
		  
			case ERR_PSW:
				Message_In_Message(&mPSWErr[0], RowNum);
				break;
		  
			case ERR_FULL_DIR:
				Message_In_Message(&mDirFull[0], RowNum);
				break;

			case ERR_AUDIT_DENIED:
				Message_In_Message(&mAudDenied[0], RowNum);
				break;
		
			case ERR_NO_DATA_LOG:
				Message_In_Message(&mNoLogData[0], RowNum);
				break;
		
			case ERR_EE_WRITE:
				Message_In_Message(&mErrEEProm[0], RowNum);
				break;
		}
	}	
	else
	{
		switch(MsgNum)
		{

#if AWS_CONFIG
			case USB_AWS_ERR_OK:
				Message_In_Message(&mKeyOK_2x16[0], RowNum);
				break;
#endif
			case USB_ERR_OK:
				Message_In_Message(&mKeyOK_2x16[0], RowNum);
				break;
			  
			case ERR_NO_CONFIG_FILE:
				Message_In_Message(&mNoFileCfw_2x16[0], RowNum);
				break;
		  
			case ERR_RW_FILE:
				Message_In_Message(&mFileErr_2x16[0], RowNum);
				break;
			  
			case ERR_NO_DIR:
				//Message_In_Message(&mNoDIR[0], RowNum);											// Lascio i trattini nel messaggio perche' il servizio non e' richiesto da questa chiave
				break;
		  
			case ERR_PSW:
				Message_In_Message(&mPSWErr_2x16[0], RowNum);
				break;
		  
			case ERR_FULL_DIR:
				Message_In_Message(&mDirFull_2x16[0], RowNum);
				break;

			case ERR_AUDIT_DENIED:
				Message_In_Message(&mAudDenied_2x16[0], RowNum);
				break;
		
			case ERR_NO_DATA_LOG:
				Message_In_Message(&mNoLogData_2x16[0], RowNum);
				break;
		
			case ERR_EE_WRITE:
				Message_In_Message(&mErrEEProm_2x16[0], RowNum);
				break;
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: MSC_File_Operations
	Chiama le funzioni di elaborazione chiave USB:
	1) "Import_Conf"   		per ricevere una nuova configurazione da chiave USB
	2) "Export_conf"   		per inviare a chiave USB l'attuale configurazione nella EEProm
	3) "Export_Audit"  		per creare il file Audit sulla chiave
	3) "Export_Log"    		per creare il file Log sulla chiave
	4) "Import_AWS_Conf"   	per ricevere una nuova configurazione AWS da chiave USB

   IN:  - 
  OUT:  - true se ricevuta New Config, altrimenti false
\*------------------------------------------------------------------------------------------*/
bool MSC_File_Operations(void)
{
	USB_Key_Err	result;
	bool	resp = false;
	
	VMC_USB_KEY_Msg();																			// Messaggi USB su LCD
	HAL_Delay(OneSec);
	
#if AWS_CONFIG
	BankSelect(BANCOZERO);
	result = Import_AWS_Conf();																	// Read config file from USB stick and write it to EEPROM
	if (result == USB_AWS_ERR_OK)
	{
		resp = true;																			// Ricevuta new config: restart VMC dopo estrazione chiave
		Select_Message(result, LCD_ROW_1);
		return resp;
	}
#endif
	
	BankSelect(BANCOZERO);
	result = Import_Conf();																		// Read config file from USB stick and write it to EEPROM
	if (result == USB_ERR_OK) resp = true;														// Ricevuta new config: restart VMC dopo estrazione chiave
	Select_Message(result, LCD_ROW_1);
	NewConfigFromKeyUSB = resp;
	
	BankSelect(BANCOZERO);
	result = Export_conf();																		// Read configuration from EEprom and write to file
	Select_Message(result, LCD_ROW_2);

	BankSelect(BANCOZERO);
	result = Export_Audit();																	// Read Audit file and write it into USB stick
	Select_Message(result, LCD_ROW_3);
	
	BankSelect(BANCOZERO);
	if (NewCPU) return resp;																// In collaudo esco qui
	result = Export_Log();																		// Read Log from EEPROM, convert in ascii and write it into USB stick
	Select_Message(result, LCD_ROW_4);
	BankSelect(BANCOZERO);
	return resp;
}
  











