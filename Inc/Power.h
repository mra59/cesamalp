/****************************************************************************************
File:
    Power.h

Description:
    Include file con definizioni e strutture per le funzioni di Backup.

History:
    Date       Aut  Note
    Apr 2019	MR   

 *****************************************************************************************/

#ifndef __Power__H
#define __Power__H

void		 SaveBackupData(void);
void		 ReloadAndCkeck_BackupRAM(void);
void  		 EraseBackupData(void);
static bool  IsFailCivetta(void);
static void  SetCivetta(void);
void 		 PSHCheckPowerDown(void);
void 		 PSHProcessingData(void);
void  		 Restore(void);


#define	SemeBackupRAM 0x2406;


#endif