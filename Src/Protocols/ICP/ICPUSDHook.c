/****************************************************************************************
File:
    ICPUSDHook.c

Description:
    Funzioni di aggancio all'applicazione per protocollo ICP USD

History:
    Date       Aut  Note
    Set 2017 	MR

*****************************************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrinsics.h>


#include "ORION.H"
#include "icpVMCProt.h"
#include "icpUSDProt.h"
#include "ICP.h"
#include "OrionCredit.h"
#include "VMC_GestSelez.h"
#include "ICPUSDHook.h"

#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
Global  References 
\*--------------------------------------------------------------------------------------*/
void ICPUSDHook_GetVendFailed(uint8_t usdType, T_USD_VENDFAILED *ptResp);
	// Called to get Vend Failed Info... Error Code of current "selRow/selColumn" selection
void ICPUSDHook_GetErrCode(uint8_t usdType, T_USD_ERRCODE *ptResp);
	// Called to get the USD failure code
void ICPUSDHook_GetSelStatus(uint8_t usdType, T_USD_SELSTATUS *ptResp);
	// Called to get the Status of current "selRow/selColumn" selection


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	const char		AUDIT_ID1_1Manif[];
extern	const char		gSWRevCode[];
extern	T_ICP_USDINFO	ICPUSD1Info;
extern	bool			ConnessioneDDCMP_IrDA(void);

/*--------------------------------------------------------------------------------------*\
Method: USD_Task_MasterMode
	Main Task USD: chiamata continuamente dal Main in Master Mode.
	Trasmette la nuova visualizzazione del credito all'USD Slave quando cambia valore. 

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  USD_Task_MasterMode(void) {
	
	if (IsUSD1Connected()) {
		if (VMC_CreditoDaVisualizzare != Get_USD1_CreditoAttuale()) {
			if (ICPUSDFundsCredit(ICP_USD1_ADDR, VMC_CreditoDaVisualizzare) == false) {			// Visualizzazione nuovo credito predisposta con successo
				Set_USD1_NewCredit(VMC_CreditoDaVisualizzare);
			}
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_SetMaxPrice

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_SetMaxPrice(uint8_t usdType, CreditCpcValue MaxPrice) {
	
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_SetRowColumn

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_SetRowColumn(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn) {
	
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_VendReq
 	 Master Mode - La Vend Req dell'USD esterno e' rifiutata se:
 	 1) VMC Master inibito (dovrebbero essere inibite tutte le fonti di credito)
 	 2) VMC Master gia' in selezione (dovrebbe comunque essere inibito anche l'USD)
 	 3) VMC Master in trasmissione DDCMP
	 4)	VMC Master programmato in Gratuito perche' i sistemi di pagamento sono inibiti
	    o non esistono e si rifiuta la VendReq se con prezzo != 0.
	    Si rifiuta anche la VendReq dall'USD se il VMC Master e' in gratuito solo per la
	    selezione corrente perche' e' una modalita' di selezione di prova del Master e quindi
	    non gestisco la selezione da USD.
	 Non si utilizza nSelRow o nSelColumn per avere il numero di selezione perche' non si sa
	 che valori possono essere inviati da altri DA: si inserisce fittiziamente il numero
	 USD_FixedSelNum in NumVMCSelez al solo scopo di registrare l'Audit vendite.
	    
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_VendReq(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn, CreditCpcValue nSelPrice) {
	
	if (gVMCState.SelezReq || fVMC_Inibito || ConnessioneDDCMP_IrDA()) {
		ICPUSDVendDenied(ICP_USD1_ADDR);
	} else if (gVMC_ConfVars.Gratuito && (nSelPrice != 0)) {
		ICPUSDVendDenied(ICP_USD1_ADDR);
	} else {
		gVMCState.USD_Ext_SelezReq = true;														// Flag per indicare selezione su USD esterno
		gVMCState.SelezReq 	= true;
		NumVMCSelez 			= USD_FixedSelNum;												// 09.17 Per ora numero selezione fisso da USD da registrare nell'Audit
		gOrionKeyVars.VendReq 	= nSelPrice;
		gOrionKeyVars.VendSel 	= NumVMCSelez;
		gOrionKeyVars.VendNeg 	= false;
		VMC_ValoreSelezione		= nSelPrice; 
		VMC_NumeroSelezione 	= NumVMCSelez;
		fSelezioneUSD = true;																	// Flag per Audit Vendita
		return;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_VendApproved
	Comando inviato in Master Mode allo USD Slave. 

   IN:  - 
  OUT:  - false se Vend Approved predisposto per l'invio, true se errore
\*------------------------------------------------------------------------------------------*/
bool ICPUSDHook_VendApproved(uint8_t usdType) {

	if (ICPUSDVendApproved(ICP_USD1_ADDR)== false) {
		gVMCState.USD_Ext_In_Selezione = true;
		return false;
	}
	return true;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_VendDenied
	Comando inviato in Master Mode allo USD Slave. 

   IN:  - 
  OUT:  - false se Vend Denied predisposto per l'invio, true se errore
\*------------------------------------------------------------------------------------------*/
bool ICPUSDHook_VendDenied(uint8_t usdType) {

	if (ICPUSDVendDenied(ICP_USD1_ADDR)== false) {
		gVMCState.SelezReq 			= false;
		gVMCState.USD_Ext_In_Selezione = false;
		return false;
	}
	return true;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_VendSucc

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_VendSucc(uint8_t usdType) {
	
	gVMCState.EsitoFineVend 		= VEND_OK;
	gVMCState.USD_Ext_In_Selezione = false;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_VendFailed

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_VendFailed(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn, uint16_t ErrCode) {
	
	gVMCState.EsitoFineVend 		= VEND_FAIL;
	gVMCState.USD_Ext_In_Selezione = false;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_PriceReq

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_PriceReq(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn) {
	
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPVMCHook_GetScalingFactor
 	 Restituisce l'USF programmato nel VMC per comunicare con l'USD.
 	 Ad oggi (04.09.2017) non essendoci una programmazione dell'USF restituisce 1.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
uint16_t ICPVMCHook_GetScalingFactor(void) {
	
	return 0x01;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPVMCHook_GetDecimalPoint
 	 Restituisce il DPP programmato nel VMC.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
uint8_t ICPVMCHook_GetDecimalPoint(void) {
	return DecimalPointPosition;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPVMCHook_ResetUSD
	Resetta la periferica USD1.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPVMCHook_ResetUSD(void) {

	ICPUSDReset(ICP_USD1_ADDR);
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_SetReady
	Return true se chiamato come USD1
	
   IN:  - tipo USD
  OUT:  - true se USD1 ready e available
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_SetReady(uint8_t usdType, bool fReady) {

	if (usdType == gVMC_ConfVars.USD_Addr) {
	}
}



//========================================================================================================
//========================================================================================================
//===============    U  S  D         S  L  A  V  E     C  o  d  e            =============================
//========================================================================================================
//========================================================================================================

/*--------------------------------------------------------------------------------------*\
Method: USD_Task_SlaveMode
	Main Task USD: chiamata continuamente dal Main in Slave Mode.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  USD_Task_SlaveMode(void) {
	
}



/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_Init
	Funzione chiamata quando si riceve il comando Reset del Master.
	Reset delle flag della eventuale selezione in sospeso.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_Init(uint8_t slaveType) {

//	if (usdType == gVMC_ConfVars.USD_Addr) {
	gVMCState.SelezReq	= false;
	gVMCState.Fase 	= NoSelez;																// Riporto state selezione in NoSelez
	NumVMCSelez 		= 0;

//	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_IsAvailable
	Return true se il DA e' in USD Slave Mode
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
bool ICPUSDHook_IsAvailable(void) {
	
	if (gVMC_ConfVars.ProtMDB && gVMC_ConfVars.ProtMDBSLV) {
		return true;
	}
	return false;  
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_IsEnabled
	Return true se chiamato come USD1
	
   IN:  - tipo USD
  OUT:  - true se chiamato USD1
\*------------------------------------------------------------------------------------------*/
bool ICPUSDHook_IsEnabled(uint8_t usdType) {
	
  if (usdType == gVMC_ConfVars.USD_Addr) {
	  return true;
  }
  return false;
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_ControlEnable
	Comando dal Master: enable/disable the USD.
	Quando disabilitato l'USD non effettua selezioni e risponde Vend Fail.
	
   IN:  - tipo USD e flag enable(true)/inhibit(false)
  OUT:  - true se USD1 ready e available
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_ControlEnable(uint8_t usdType, bool enabled) {
	
	if (usdType == gVMC_ConfVars.USD_Addr) {
		if (enabled) {
			gVMCState.USD_Loc_Disabled = false;												// Clear Disable 
		} else {
			gVMCState.USD_Loc_Disabled = true;													// Set Disable 
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetManufactCode
	Ricevo dal Master i suoi parametri quali VMC Feature Level, USF, DPP e VMC maximum
	approved/denied time 
	
   IN:  - VMC Feature Level, USF, DPP e VMC maximum approved/denied time 
  OUT:  - 
\*---------------------------------------------------------------------------------------*/
void ICPUSDHook_VMCSetupInfo(T_VMC_SETUP *ptInfo) {
	
	//MR20 Cvt16ToBigEndian(&ptInfo->nUSF);
	ptInfo->nUSF = __REV16(ptInfo->nUSF);

	UnitScalingFactor = ptInfo->nUSF;
	DecimalPointPosition = ptInfo->nDPP;
	//xxx = ptInfo->nMaxTmApproveDeny;
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetManufactCode
	Comando dal Master per richiedere il Manifacturer Code dell'USD.
	
   IN:  - tipo USD
  OUT:  - Manifacturer Code ID (3 bytes)
\*---------------------------------------------------------------------------------------*/
void ICPUSDHook_GetManufactCode(uint8_t *ptData, uint8_t dataLen) {

	uint8_t	j;
	
	for (j=0; j < dataLen; j++) {
		*(ptData+j) = AUDIT_ID1_1Manif[1+j];
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetSerialNumber
	Comando dal Master per richiedere il Serial Number dell'USD.
	
   IN:  - tipo USD
  OUT:  - DA Serial Number ID (12 bytes)
\*---------------------------------------------------------------------------------------*/
void ICPUSDHook_GetSerialNumber(uint8_t *ptData, uint8_t nDataLen) {
	
	uint8_t	i;
	uint8_t	LocalBuff[16];
	
	sprintf((char*)&LocalBuff[0],"%012lu", SerialNumber);
	for (i=0; i<nDataLen; i++) {
		*(ptData+i) = LocalBuff[i];
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetModelNumber
	Comando dal Master per richiedere il Model Number dell'USD.
	
   IN:  - 
  OUT:  - Model Number ID (12 bytes)
\*---------------------------------------------------------------------------------------*/
void ICPUSDHook_GetModelNumber(uint8_t *ptData, uint8_t dataLen) {
	
	memset(ptData, ' ', dataLen);
	if(dataLen>sizeof(MachineModelNumber)) dataLen= sizeof(MachineModelNumber);
	strncpy((char *) (ptData), &MachineModelNumber[0], dataLen);
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetSWVersion
	Comando dal Master per richiedere la Rev SW dell'USD.
	
   IN:  - 
  OUT:  - SW Revision 
\*---------------------------------------------------------------------------------------*/
uint16_t ICPUSDHook_GetSWVersion(void) {
	
	return (uint16_t) kORIONVersion;
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetFeatureLevel
	Comando dal Master per richiedere il livello dell'USD.
	
   IN:  - 
  OUT:  - Feature Level USD 
\*---------------------------------------------------------------------------------------*/
uint8_t ICPUSDHook_GetFeatureLevel(uint8_t usdType) {
	
	return _IcpUsdLevel_;
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetMaxPrice
	Comando dal Master per richiedere il prezzo piu' alto dell'USD.
	
   IN:  - 
  OUT:  - Max Price USD 
\*---------------------------------------------------------------------------------------*/
CreditCpcValue ICPUSDHook_GetMaxPrice(uint8_t usdType) {
	
	return MaxPrice;
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_GetRowColumn
	Comando dal Master per richiedere il righe e colonne dell'USD.
	
   IN:  - 
  OUT:  - Row and Column
\*---------------------------------------------------------------------------------------*/
void ICPUSDHook_GetRowColumn(uint8_t usdType, uint8_t *selRow, uint8_t *selColumn) {
	
//	if (usdType == gVMC_ConfVars.USD_Addr) {
		*selRow 	= 0;
		*selColumn 	= 0;
//	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPUSDHook_FundsCredit
	Comando dal Master per aggiornare il credito su display.
	
   IN:  - 
  OUT:  - Row and Column
\*---------------------------------------------------------------------------------------*/
void ICPUSDHook_FundsCredit(uint8_t usdType, CreditCpcValue creditVal) {
	
//	if (usdType == gVMC_ConfVars.USD_Addr) {
		VMC_CreditoDaVisualizzare = creditVal;
//	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPUSDHook_VendRequest
	Determino costo/numero selezione e, se parametri leciti, predispongo la richiesta di
	VendRequest al Master MDB.

   IN:  -  Selezione richiesta
  OUT:  -  true e Vend Request predisposto se selezione lecita, altrimenti false.
\*--------------------------------------------------------------------------------------*/
bool ICPUSDHook_VendRequest(uint8_t numtasto) {
	
	//MR19 CreditValue	ValSelez;		//Never referenced
	
	if ((numtasto == 0) || (gVMCState.USD_Loc_Disabled)) {										// Numero selezione zero o USD Slave disabled
		return false;
	}
	//MR19 ValSelez = MRPPrices.Prices99.MRPPricesList99[numtasto-1];		Never referenced
	if (SetPrice_e_Selnum(0, numtasto) == false) {
		return false;
	}
	ICPProt_USDSetEvtMask(gVMC_ConfVars.USD_Addr, ICP_USDINFO_VENDREQ);							// Predispongo per rispondere al prossimo POLL con Vend Request
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPUSDHook_GetVendRequest
	Rispondo al Poll del Master con la Vend Request.
	I parametri sono stati predisposti in precedenza dalla ICPUSDHook_VendRequest.

   IN:  -  
  OUT:  -  
\*--------------------------------------------------------------------------------------*/
void ICPUSDHook_GetVendRequest(uint8_t usdType, uint8_t *selRow, uint8_t *selColumn, CreditCpcValue *selPrice) {
	
	*selRow 	= 0;
	*selColumn	= gOrionKeyVars.VendSel;
	*selPrice	= gOrionKeyVars.VendReq;
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_SlaveVendApproved
	Comando dal Master per eseguire la selezione richiesta da questo USD.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_SlaveVendApproved(uint8_t usdType) {

//	if (usdType == gVMC_ConfVars.USD_Addr) {
		VendAuthorResult(true);																	// Vend Approved alla Gestione Selezione                                               	
		if (gOrionConfVars.RestoSubito) {														// Resto Subito = Y
			ICPUSDHook_SlaveVendSuccess();														// Simula vendita completata e risponde subito VEND OK
			ICPUSD1Info.waitingRealVendEnd = true;
		} else {
			ICPUSD1Info.waitingRealVendEnd = false;												// Attendere fine selezione
		}
//	}
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPUSDHook_SlaveVendDenied
	Comando dal Master per negare la selezione richiesta da questo USD.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPUSDHook_SlaveVendDenied(uint8_t usdType) {

//	if (usdType == gVMC_ConfVars.USD_Addr) {
		VendAuthorResult(false);															// Vend Denied alla Gestione Selezione                                               	
//	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPUSDHook_SlaveVendFailed
	Predispongo la risposta Vend Fail al Master MDB.

   IN:  -  Errore Selezione
  OUT:  -  
\*--------------------------------------------------------------------------------------*/
void ICPUSDHook_SlaveVendFailed(uint16_t Errore) {
	
	if (ICPUSD1Info.waitingRealVendEnd == false) {
		gVMCState.USDSelez_Error = Errore;
		ICPProt_USDSetEvtMask(gVMC_ConfVars.USD_Addr, ICP_USDINFO_VENDFAILED);					// Predispongo per rispondere al prossimo POLL con Vend Fail
	}
	ICPUSD1Info.waitingRealVendEnd = false;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPUSDHook_SlaveVendSuccess
	Predispongo la risposta Vend Success al Master MDB.

   IN:  -  
  OUT:  -  
\*--------------------------------------------------------------------------------------*/
void ICPUSDHook_SlaveVendSuccess(void) {
	
	if (ICPUSD1Info.waitingRealVendEnd == false) {
		ICPProt_USDSetEvtMask(gVMC_ConfVars.USD_Addr, ICP_USDINFO_VENDSUCC);					// Predispongo per rispondere al prossimo POLL con Vend Success
	}
	ICPUSD1Info.waitingRealVendEnd = false;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPUSDHook_GetVendFailed
	Rispondo al Poll del Master con il motivo del Vend Fail inviato in precedenza.

   IN:  -  
  OUT:  -  
\*--------------------------------------------------------------------------------------*/
void ICPUSDHook_GetVendFailed(uint8_t usdType, T_USD_VENDFAILED *ptResp)
{
//	if (usdType == gVMC_ConfVars.USD_Addr) {
		ptResp->nSelRow 	= 0;
		ptResp->nSelColumn	= gOrionKeyVars.VendSel;
		ptResp->nErrCode	= gVMCState.USDSelez_Error;
//	}
}






//MR19  Aggiunto return 0 per evitare warning
uint16_t ICPUSDHook_VendSelStatus(uint8_t usdType, uint8_t selRow, uint8_t selColumn) {return 0;}


void ICPUSDHook_GetPriceRequest(uint8_t usdType, uint8_t *selRow, uint8_t *selColumn) {}
void ICPUSDHook_GetErrCode(uint8_t usdType, T_USD_ERRCODE *ptResp)
{
	ptResp->nErrCode = 0;			//MR20200412 Aggiunto per debug, e' una funzione vuota
}
void ICPUSDHook_GetSelStatus(uint8_t usdType, T_USD_SELSTATUS *ptResp)
{
	ptResp->nSelRow		= 4;		//MR20200412 Aggiunti per debug, e' una funzione vuota
	ptResp->nSelColumn	= 4;
	ptResp->nSelStatus	= 0;
}

void ICPUSDHook_VendSel(uint8_t usdType, uint8_t selRow, uint8_t selColumn) {}
void ICPUSDHook_VendHomeSel(uint8_t usdType, uint8_t selRow, uint8_t selColumn) {}
void ICPUSDHook_FundsSelPrice(uint8_t usdType, uint8_t selRow, uint8_t selColumn, CreditCpcValue selPrice, uint16_t selID) {}

void ICPUSDHook_SetManufactCode(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen) {}
void ICPUSDHook_SetSerialNumber(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen) {}




// =======================================================================================================================
// =======================================================================================================================
// =======================================================================================================================

void ICPUSDHook_ErrCode(uint8_t usdType, uint16_t errCode) {}
void ICPUSDHook_Data(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen) {}
void ICPUSDHook_DataBlock(uint8_t usdType, uint8_t nBlock, uint8_t *ptData, uint8_t nDataLen) {}
void ICPUSDHook_DataBlockReq(uint8_t usdType, uint8_t nBlock) {}
void ICPUSDHook_SetModelNumber(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen) {}
void ICPUSDHook_SetSWVersion(uint8_t usdType, uint16_t nSwVersion) {}
bool ICPUSDHook_SetDiagData(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen) {return false;}					//MR19  Chiamata dal "icpUSD"
void ICPUSDHook_SelStatus(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn, uint16_t nSelStatus) {}
void ICPUSDHook_SendDataReq(uint8_t usdType, uint8_t nBlock) {}













