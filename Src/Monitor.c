/****************************************************************************************
File:
    Monitor.c

Description:
    Funzioni interne di Monitor.

History:
    Date       Aut  Note
    Giu 2019	MR   

 *****************************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
   
#include "Orion.h"
#include "Monitor.h"
#include "IICEEP.h"
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"
#include "General_utility.h"
#include "power.h"
#include "Funzioni.h"

#include "Dummy.h"

#if USB_CDC_PC
	#include "usbd_def.h"
#endif

/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

static 	uint8_t  	cr = 0x0d, lf = 0x0a, ENTER = 0x0d, ESC = 0x1b, Back = 0x08;
static 	uint8_t  	Space = 0x20, QuestionMark = 0x3F, Canc = 0x7F, Bel = 0x07;
		uint16_t	cmdval, MemAddr, NumBytes, ValToWrite, esito;
		uint32_t  	AbsoluteAddress, StartAddress, EndAddress, TimeMonitor;
		uint16_t 	*pnt;
		uint8_t  	*Rxpnt;															// Pointer al buffer di ricezione dal PC
		uint8_t  	*RAMLogBuffPnt;													// Pointer al Log buffer in RAM prima di trasferirlo in EE
static 	bool		DisplayASCII;

#if USB_CDC_PC
	#define	USB_RX_BUFF_SIZE	128
	#define	BUF_THRESHOLD		128																// Ogni BUF_THRESHOLD caratteri memorizzo in EE
	#define	USB_RX_BUFF_LIMIT	(BUF_THRESHOLD*12)												// Scambio pointer al buffer di ricezione ogni USB_RX_BUFF_LIMIT bytes
	#define	TOUT_ATTESA_CHR		TwoSec

	uint8_t  	usb_char[USB_RX_BUFF_SIZE];
	uint8_t		usb_char_cnt_IN = 0, usb_char_cnt_OUT = 0;
	uint8_t		*RxConfPnt;
	static	uint32_t	NumCharIN_1, NumCharIN_2, NumCharOUT_1, NumCharOUT_2, CharCntOut;
	//static	uint32_t	StoreCnt_1, StoreCnt_2, , DEB_Blk_FULL, DEB_Blk_LESS;				// DEBUG
	static	bool		ChrResidui, SecondBuff;
#endif
		
Mon SysMonitor;
//MR19 uint32_t	result;


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void   	FillEE_LogBuffer(uint8_t FillVal);
extern	bool	ConnessioneDDCMP_IrDA(void);
extern	void	AuditExtClearData(bool);
extern	void	LeggiOrologio(void);
extern	void  	SetPC_Comm(uint32_t BaudRate);
extern	uint8_t	SendConfToEEprom(uint8_t);

extern	USB_STATE_ENUM USB_state;
extern	uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);


/*--------------------------------------------------------------------------------------*\
Global Defines
\*--------------------------------------------------------------------------------------*/

#define	TxOffs		512

#if USB_CDC_PC	
	#define	SCI_SendBlock(Buff, Size)   	CDC_Transmit_FS(Buff, Size);
	#define	SCI_ReceiveBlock(Size)			gVars.Dummy = false;								// Dummy
	#define	SCI_ClearFlagTx					gVars.DummyFLagTx = true;							// Dummy
	#define	SCI_FlagTx						gVars.DummyFLagTx									// Dummy
#else							
	#define	SCI_SendBlock(Buff, Size)   	RS485_Comm_SendBlock(Buff, Size);
	#define	SCI_ReceiveBlock(Size)			RS485_Comm_Enable_Rx_Chr();
	#define	SCI_ClearFlagTx					gVars.DummyFLagTx = false;
	#define	SCI_FlagTx						gVars.DummyFLagTx
#endif


/*  Comandi disponibili     */
//-----------------------------------1--------------------------------------------------------------------70-------80
const char Menu_Utente[] 		 = {"\r\n3: Read/Write Date\r\n"
									"4: Read/Write Time\r\n"
									"5: Debug Mifare\r\n"
									"10: Vend Req and NumSel\r\n"
									"11: Echo EXEC SLAVE /DDCMP\r\n"
									"12: Echo MDB MASTER\r\n"
									"13: Clear Backup EEPROM\r\n"};


const char Menu_Cliente[] 		 = {"\r\n1: Read eeprom\r\n"
									"3: Read/Write Date\r\n"
        							"4: Read/Write Time\r\n"
        							"5: Debug Mifare\r\n"
        							"7: Clear Audit\r\n"
									"10: Vend Req and NumSel\r\n"
									"11: Echo Executive Communication\r\n"
									"12: Echo MDB Communication\r\n"};

const char Menu_Completo[] 		 = {"\r\n1-21: Read eeprom (21 in ASCII)\r\n"
									"2: Write eeprom\r\n"
		                            "3: Read/Write Date\r\n"
		                            "4: Read/Write Time\r\n"
		                            "5: Debug Mifare\r\n"
		                            "6: Fill EEprom(s)\r\n"
		                            "7: Clear Audit\r\n"
//									"8: Set Development Config\r\n"
        							"9: Reset USB Area\r\n"
									"10: Vend Req and NumSel\r\n"
									"11: Echo EXEC SLAVE /DDCMP\r\n"
									"12: Echo MDB MASTER\r\n"
									"13: Clear Backup EEPROM\r\n"
									"14: Send Config File\r\n"
									"15: DDCMP Log State\r\n"
									"16: Send Log Buffer\r\n"
									"17: Get Config File from PC\r\n"
									"18: Echo Executive Master\r\n"};

const char CmdPerListaComandi[]	= {"\r\nType '?' to get Command List, Escape to Terminate Command or to Quit Monitor"};
const char Nuova_Data[] 		= {"\r\nNew Date (gg.mm.yyyy-DayofTheWeek [0=Sunday - 6=Saturday] ): "};
const char Nuova_Ora[] 			= {"\r\nNew Time (hh:mm:ss): "};
const char CardErrors[] 		= {"\r\n\r\nShow Card Errors\r\n"};
const char Pass[]				= {"\r\n\r\nInsert Password (5 digit): "};
const char NumComando[] 		= {"\r\nCommand Number "};
const char UserMenu[] 			= {"(User):  "};
const char MHDMenu[] 			= {"(MHD):  "};
const char RBTMenu[] 			= {"(RBT):  "};
const char SicuroEEprom[]		= {"\r\n\r\nAre you sure to Fill Entire EEPROM (Y/N) ? "};
const char SicuroAudit[] 		= {"\r\n\r\nAre you sure to Clr Entire Audit (Y/N) ? "};
const char SicuroBackupEE[]		= {"\r\n\r\nAre you sure to Clr Backup EEprom (Y/N) ? "};
const char AuditErased[] 		= {"\r\n\r\nAudit cleared\r\n"};
const char BackupErased[] 		= {"\r\n\r\nBackup EEprom cleared\r\n"};
const char Erase_EEPROM[] 		= {"\r\n\r\nFilling EEPROM: Wait Restart after erasing"};
const char Fill_EEPROM[] 		= {"\r\n\r\nFilling EEPROM: Wait...."};
//const char SicuroClrAreaUSB[]	= {"\r\n\r\nAre you sure to Erase USB Area (Y/N) ? "};
//const char AreaUSBErased[] 		= {"\r\n\r\nUSB Area Erased\r\n"};
const char WaitWDTReset[] 		= {"\r\nWait Restart\r\n"};
//const char SicuroConfig[] 		= {"\r\nAre you sure to Set Default Config (Y/N) ? "};
//const char ConfigSet[] 			= {"\r\nDefault Config Set: Wait Restart"};
const char MonitorEndSession[]	= {"\r\nMonitor End Session\r\n"};
const char NumBytesToRead[]		= {"\r\nNumber of Bytes to Read (1-65535): "};
const char ValueToWrite[]		= {"\r\nValue to Write (0x or ASCII):  "};
const char MemoryAddress[]		= {"\r\nEE Address (0x0-0x2FFFF): "};
const char EE_StartAddress[]	= {"\r\nEE Start Address (0x0-0x2FFFF): 0x"};
const char EE_EndAddress[]		= {"\r\nEE End Address (0x0-0x2FFFF): 0x"};
const char SelecPrice[] 		= {"\r\n\r\nShow Vend Price"};
const char SelecNumber[] 		= {"\r\n\r\nShow Selection Number (Price Holding)"};
const char ExecProtComms[] 		= {"\r\n\r\nShow Executive Communications\r\n"};
const char MDBProtComms[] 		= {"\r\n\r\nShow MDB Communications\r\n"};
const char PauseMSG[] 			= {"\r\n  *** Pause  ***\r\n"};
const char SelecPriceNumMDB[]	= {"\r\n\r\nShow Vend Price and Sel Number"};
const char VendDen_TipoKey[]	= {"Wrong Key Type\r\n"};
const char VendDen_Cash[]		= {"Cash Present\r\n"};
const char VendDenReadErrMsg[]	= {"Key Read Err\r\n"};
const char VendDenWriteErrMsg[]	= {"Key Write Err\r\n"};
const char VendDenNoKeyMsg[]	= {"No Key Err\r\n"};
const char VendDenNoIDLEMsg[]	= {"No IDLE State\r\n"};
const char VendDenNoPriceMsg[]	= {"No Price\r\n"};
const char VendPendingMsg[]		= {"Vend Pending\r\n"};
const char VendDenSysOff_DDCMP_AudExt[] = {"System OFF/DDCMP Conn/ExtAud Full\r\n"};
const char CreditoInsuffMsg[]   = {"Insufficient Counter\r\n"};
const char AltreCauseMsg[]   	= {"Undefined\r\n"};
const char RegMsg[]				= {"\r\nRegister Value = "};
const char VendDen_OFF_DDCMP_ExtAudFull[]	= {"System OFF - DDCMP Active - ExtAudFull\r\n"};
const char PressAnyKey[]		= {"\r\n\r\nPress Any Key to Start\r\n"};
const char ActualDDCMPLogState[]= {"\r\nActual Log State: "};
const char SetLogDDCMP[]		= {"Set Log (0=Log disabled 1=Prot 3=Prot+Time 5=Prot+Flags): "};
const char NoLogData[] 			= {"\r\n\r\nNo Log Data\r\n"};
const char Rear_or_Write[]		= {"\r\nRead (r/R) or Write (w/W): "};
const char PN512Address[]		= {"\r\nPN512 Address (0x0-0x3F): 0x"};
const char StartSendingConf[]	= {"\r\n\r\nStart Sending Config File\r\n"};
const char RxConf_OK[]			= {"\r\nConfig File OK: Received %d bytes\r\n"};
const char NoConfigData[]		= {"\r\nNo Data Received\r\n"};
const char EEWriteFail[]		= {"\r\nEEprom Write Fail\r\n"};

const char CmdUnknown[] 		= {"\r\nCmd_Unknown"};
const char InvalidAddress[] 	= {"\r\nInvalid Address"};
const char InvalidParameter[] 	= {"\r\nInvalid Parameter/s"};
const char DataErrata[] 		= {"\r\nInvalid Date"};
const char OraErrata[]	 		= {"\r\nInvalid Time"};

const char ValidChars[] 		= {'.', ':', '-', '_', '?', 'r', 'R', 'w', 'W', '*', ' ', '+', 0x00};


//	Lista comandi disponibili all'Utente.
// **** Aggiornare lunghezza UserCmdList_NumElem *****
const uint8_t UserCmdList[] = {01,03,04,05,07,10,11,12};
#define	 UserCmdList_NumElem	8						// Numero di elementi della lista comandi User
#define	 CMD1_RigaASCIILen		70						// Numero di chr per riga quando visualizzo la memoria come stringa ASCII

/*---------------------------------------------------------------------------------------------*\
Method: SystemMonitor
	Contiene varie routines di debug del sistema.
	
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  SystemMonitor(void)
{
	
#define ddcmpOffs  144
	
// Dichiarazione Variabili Locali
uint16_t	r, i, k, Dummy, RowBlocks;
//MR19 uint32_t  pos;
static 	uint32_t	LogBuffCnt, LogBuffSize, LogBuffStartAddr, FirstByteFreeAddr, SpaceAvailable;
static  uint8_t		Reg1;
//MR19 phcsBfl_Status_t status;

#if USB_CDC_PC	
	if (USB_state == USB_MODE_DEVICE)
	{
		if (usb_char_cnt_IN != usb_char_cnt_OUT)
		{
		 	if (SysMonitor.state == WaitPC_Call)
		  	{
				if (usb_char[usb_char_cnt_OUT++] == ENTER)
				{
					SysMonitor.PC_Call = true;													// Attivo Monitor
				}
			}
			else 
			{
				do
			  	{
			  		MonitorRxData(usb_char[usb_char_cnt_OUT++]);
					usb_char_cnt_OUT %= USB_RX_BUFF_SIZE;
			  	} while (usb_char_cnt_IN != usb_char_cnt_OUT);
			}
		}
	}

#else
	
	// Controllo se e' stata richiesta una connessione DDCMP da PC
	if (SysMonitor.Call_DDCMP_PC) {
		if (SysMonitor.Set_DDCMP_PC) {																// Sono in connessione DDCMP da PC: normalmente al termine
			if (Touts.tmMon != 0) {																	// la "kDDCMPPhaseFinished" chiama la "Start_IrDA" che azzera
				return;																				// le flags monitor e riattiva l'irq Sense
			} else {																				// Timeout scaduto senza connessione completata
				Start_IrDA();																		// Riabilito IRQ da SCI_Sense, Clr flags PC e set UART
			}
		} else {
			if (Touts.tmMon == 0) {																	// Attendo timeout per perdere tutto il primo START
				//MR19 Start_DDCMP_da_PC();																// Init UART e HC125 per comunicazioni con PC
				SysMonitor.Set_DDCMP_PC = true;
				Touts.tmMon = OneMin;																// Carico un Timeout di sicurezza
			}
		}
	}
	// Controllo Timeout Monitor: se non e' in uno degli stati elencati e non riceve caratteri dal PC per tmMon tempo, esco dal Monitor
	if (SysMonitor.state != ShowCardError && SysMonitor.state != WaitPC_Call && SysMonitor.state != WaitMonitorEndSession
			&& SysMonitor.state != ShowVendReqData && SysMonitor.state != ShowExecProtComm && SysMonitor.state != PauseShowExecProtComm &&
			SysMonitor.state != ShowMDBProtComm && SysMonitor.state != PauseShowMDBProtComm) {
		if (Touts.tmMon == 0) {
			SCI_RxOFF();																			// Disattivo ricezione seriale
			SysMonitor.state = Quit_Monitor;														// Timeout: termino sessione Monitor
		}
	}
#endif
	
	switch (SysMonitor.state) {

	case WaitPC_Call:
#if USB_CDC_PC
		if (SysMonitor.PC_Call) {
			SysMonitor.PC_Call = false;
		    Rxpnt = (uint8_t *)calloc(32, 32);														// Riservo spazio RAM per RxBuff
		    if (SysMonitor.LogProtocol != 0) {
			    SysMonitor.ProtMonitor = true;
			    if (SysMonitor.LogProtocol == TypeMDB)
				{
					if (SysMonitor.LogMDBPowerUp == true)
					{																				// Log attivo gia' al PowerUp, salto allo stato "ShowMDBProtComm" per non resettare
						SysMonitor.state = ShowMDBProtComm;											// counters GenericCnt e non perdere i dati gia' ricevuti
					}
					else
					{
						SysMonitor.state = DebugMDBProtocol;
					}
			    } else if (SysMonitor.LogProtocol == TypeExec){
					SysMonitor.state = DebugExecutiveProtocol;
			    } else if (SysMonitor.LogProtocol == TypeMasterExec) {
					SysMonitor.state = DebugExecMasterProtocol;
			    }
			    SysMonitor.LogProtocol = 0;			//10.2019 Tolta per far registrare Log nel buffer anche se non attiva la comunicazione col terminal
				SysMonitor.MonFeatures = MonCompleto;
		    } else {
				SysMonitor.state = InviaMsg;
		    }
		}
		break;
#else		
		if (SysMonitor.ProtMonitor) {
			SysMonitor.state = Quit_Monitor;														// ProtMonitor non puo' essere true nello stato di WaitPC_Call
			break;
		}
		if (SysMonitor.PC_Call) {
			SysMonitor.PC_Call = false;
			if (ConnessioneDDCMP_IrDA()) break;														// Connessione IrDA: non e' il PC
			SetPC_Comm(57600);
		    Rxpnt = (uint8_t *)calloc(32, 32);														// Riservo spazio RAM per RxBuff
		    if (SysMonitor.LogProtocol != 0) {
			    SysMonitor.ProtMonitor = true;
			    if (SysMonitor.LogProtocol == TypeMDB) {
					SysMonitor.state = DebugMDBProtocol;
			    } else {
					SysMonitor.state = DebugExecutiveProtocol;
			    }
			    SysMonitor.LogProtocol = 0;
				SysMonitor.MonFeatures = MonCompleto;
			    //MR19 result = SCI_ReceiveBlock(1U);					       							// Start reception of one character
			    SCI_ReceiveBlock(1U);					       											// Start reception of one character
		    } else {
				SysMonitor.state = InviaMsg;
		    }
		}
		break;
#endif
		
	case InviaMsg:
		// Messaggio di inizio sessione Monitor
	    SysMonitor.ProtMonitor = true;

#if (CESAM == true)
	    sprintf((char*)&gVars.ddcmpbuff[0], "\r\ns/n %010u\r\n ALP  Machines", SerialNumber);
#endif
	    
	    r = strlen((char const*)&gVars.ddcmpbuff[0]);
		sprintf((char*)&gVars.ddcmpbuff[r], " - Fw Rev. ");
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		strncpy((char*)&gVars.ddcmpbuff[r], kProductSWVersionStr, sizeof(kProductSWVersionStr));	// Visualizzo Revisione Firmware
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
#if USB_CDC_PC
	    sprintf((char*)&gVars.ddcmpbuff[r], "\r\n%1s", Pass);
		r = strlen((char*)&gVars.ddcmpbuff[0]);
#endif		
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		SysMonitor.state = InsertPassword;
		break;
	
	case InsertPassword:
		// Visualizza la richiesta di inserire una password
	    if (TestTxMsgInCorso()) break;

#if (USB_CDC_PC == false)
	    sprintf((char*)&gVars.ddcmpbuff[0], "%1s", Pass);
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SCI_ClearFlagTx;
		//MR19 result = SCI_ReceiveBlock(1U);					             						// Start reception of one character
		SCI_ReceiveBlock(1U);					             										// Start reception of one character
#else
		SCI_ClearFlagTx;
#endif
		SysMonitor.DontTxChrBack = 0;																// Abilito ritrasmissione dei caratteri al PC
		SysMonitor.state = WaitPassword;
		break;

	case WaitPassword:
		if (!SCI_FlagTx) break;
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SCI_RxOFF();																			// Disattivo ricezione seriale
			SysMonitor.state = Quit_Monitor;														// Premuto "Escape": termino sessione Monitor
			gVars.ddcmpbuff[0] = 0;
			break;
		}
		if (!GetNumero(5)) {
			PrintError(InvalidParameter);															// Segnalo errore al PC
			SysMonitor.state = InviaMsg;
			break;
		}
		SysMonitor.state = SendComandoListaComandi;													// Predispongo prossimo menu
		switch (NumBytes) {
		case (PswMia):
			SysMonitor.MonFeatures = MonCompleto;
			break;
		case (PswMHD):
			SysMonitor.MonFeatures = MonCliente;
			break;
		case (0):
			SysMonitor.MonFeatures = MonUtente;
			break;
		default:
			PrintError(InvalidParameter);															// Segnalo errore al PC
			SysMonitor.state = InviaMsg;
		}
		break;
	
	case SendMenuComandi:
	    // Visualizza i comandi disponibili
		if (TestTxMsgInCorso()) break;
		switch (SysMonitor.MonFeatures) {
		case (MonCompleto):
			sprintf((char*)&gVars.ddcmpbuff[0], "\r\n%1s", Menu_Completo);
			break;
		case (MonCliente):
			sprintf((char*)&gVars.ddcmpbuff[0], "\r\n%1s", Menu_Cliente);
			//sprintf((char*)&gVars.ddcmpbuff[0], "\r\n%1s", Menu_Completo);					// 26.07.14 Fino ad ora autorizzo tutte le funzioni anche per MHD
			break;
		default:
			sprintf((char*)&gVars.ddcmpbuff[0], "\r\n%1s", Menu_Utente);
			break;
		}
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		SysMonitor.state = SendSceltaComando;
		break;
		
	case SendComandoListaComandi:
	    // Visualizza "Type ? per vedere lista comandi"
		if (TestTxMsgInCorso()) break;
		sprintf((char*)&gVars.ddcmpbuff[0], "\r\n%1s", CmdPerListaComandi);
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		SysMonitor.state = SendSceltaComando;
		break;
		
	case SendSceltaComando:
		// Visualizza la richiesta di inserire un comando
	    if (TestTxMsgInCorso()) break;
		sprintf((char*)&gVars.ddcmpbuff[0], "%1s", NumComando);
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		switch (SysMonitor.MonFeatures) {
		case (MonCompleto):
			sprintf((char*)&gVars.ddcmpbuff[r], "%1s", RBTMenu);
			break;
		case (MonCliente):
			sprintf((char*)&gVars.ddcmpbuff[r], "%1s", MHDMenu);
			break;
		default:
			sprintf((char*)&gVars.ddcmpbuff[r], "%1s", UserMenu);
		}
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SCI_ClearFlagTx;																			// Attendo trasmissione ultimo carattere del buffer
		SysMonitor.state = WaitInvioSceltaComando;
		break;
	
	case WaitInvioSceltaComando:
		// Attesa ricezione comando dal PC
		if (!SCI_FlagTx)  break;
		memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));									// Azzero buffer ddcmp
		memset(&gVars.tempRTx1[0], 0, sizeof(gVars.tempRTx1));										// Azzero buffer tempRTx1
		SysMonitor.RxCount = 0;
		//MR19 result = SCI_ReceiveBlock(1U);					             						// Start reception of one character
		SCI_ReceiveBlock(1U);					             										// Start reception of one character
	    SysMonitor.DontTxChrBack = 0;																// Abilito ritrasmissione dei caratteri al PC
		SysMonitor.state = SceltaComando;
		break;
	
	case SceltaComando:
		if (TestTxMsgInCorso()) break;
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (!ElaboraNumeroComando()) {																// Funzione elaborazione comando ricevuto
			SysMonitor.state = SendSceltaComando;													// Comando non valido: invio nuovamente msg di richiesta comando
		}
		break;

// ========================================================================
// ========    Read / Write EEPROM      CMD1 - CMD2    ====================
// ========================================================================

	case Ask_Address:
		// Scelto comando di scrittura o lettura EEPROM: chiedo Address
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&MemoryAddress[0]);
		SysMonitor.state = WaitAddress;
		break;
	
	case WaitAddress:
		if (TestTxMsgInCorso()) break;
		if (!SysMonitor.RxCMD) 	break;																	// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;														// Premuto "Escape": termino comando
			break;
		}
		if (!ElaboraAddress()) {
			SysMonitor.state = Ask_Address;
			break;
		}
		if ((cmdval == read_ee) || (cmdval == read_ee_ASCII)) {
			SysMonitor.state = AskNumByteToRead;
			break;
		}
		if (cmdval == write_ee) {
			SysMonitor.state = AskParametroWrite;
		}
		break;

	case AskNumByteToRead:
		// Scelto comando di lettura EEPROM: chiedo numero bytes da leggere
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&NumBytesToRead[0]);
		SysMonitor.state = WaitNumByteToRead;
		break;
	
	case WaitNumByteToRead:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		SysMonitor.state = readeeprom;
		break;

	case AskParametroWrite:
		// Scelto comando di scrittura EEPROM: chiedo parametro
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&ValueToWrite[0]);
		SysMonitor.state = WaitParameterWrite;
		break;
	
	case WaitParameterWrite:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		SysMonitor.state = writeeeprom;
		break;

	case readeeprom:
		if (!GetNumBytes(5)) {
			PrintError(InvalidParameter);															// Segnalo errore al PC
			SysMonitor.state = AskNumByteToRead;
			break;
		}
		Dummy = AbsoluteAddress / 0x10000;
		BankSelect((uint8_t) Dummy);
		MemAddr = (uint16_t)AbsoluteAddress;
		LogBuffCnt = 0;																				// Uso LogBuffCnt come flag per leggere anche l'ultimo byte di EE
		if (NumBytes == 0xffff) {																	// quando e' richiesta la lettura di 65535 bytes. Essendo NumBytes a 16 bit
			LogBuffCnt = 1;																			// non e' possibile impostare 0x10000, pertanto uso LogBuffCnt come flag
		}
		Reg1 = 0;
		do {
			WDT_Clear(gVars.devWDT);  																// Trigger WDT
			if (NumBytes > 256) {																	// Leggo al massimo 256 bytes per volta
				k = 256;
			} else {
				k = NumBytes;																		// Ultimo blocco dati da leggere
				if (LogBuffCnt) {
					k++;
					NumBytes++;
				}
			}
			ReadEEPI2C(MemAddr, (uint8_t *)(&gVars.tempRTx1[0]), k);									// Leggo il numero di bytes richiesti in gVars.tempRTx1
			i = 0;																					// Pointer buffer destinazione
			gVars.ddcmpbuff[i++] = cr;																// NewLine
			gVars.ddcmpbuff[i++] = lf; 
			RowBlocks = NumBlocksPerRaw;
			for (r=0; r < k; r++) {
				if (DisplayASCII == FALSE) {
					// --- Visualizza su PC come chr Hex ------
					HexByteToAsciiInt(gVars.tempRTx1[r],&Dummy, NULL);								// Li converto in ASCII e li memorizzo in gVars.ddcmpbuff
					if (r%4 == 0 && r != 0) {														// Ogni 4 valori Hex (8 caratteri) aggiungo uno spazio e vado a capo
						if (RowBlocks == 0) {														// ogni "NumBlocksPerRaw" blocchi
							gVars.ddcmpbuff[i++] = cr;												// inserisco un NL e passo alla riga successiva
							gVars.ddcmpbuff[i++] = lf;
							RowBlocks = NumBlocksPerRaw;
						} else {
							gVars.ddcmpbuff[i++] = Space;
							RowBlocks--;
						}
					}
					gVars.ddcmpbuff[i++] = Dummy>>8;
					gVars.ddcmpbuff[i++] = Dummy;
				} else {
					// --- Visualizza su PC come stringa ASCII ------
					if ((gVars.tempRTx1[r] > 0x1f) && (gVars.tempRTx1[r] < 0x7f)) {				// Chr ASCII stampabile
						gVars.ddcmpbuff[i++] = gVars.tempRTx1[r];
						Reg1++;
					} else {
						HexByteToAsciiInt(gVars.tempRTx1[r],&Dummy, NULL);						// Chr ASCII NON stampabile, lo visualizzo come chr Hex
						gVars.ddcmpbuff[i++] = Dummy>>8;
						gVars.ddcmpbuff[i++] = Dummy;
						Reg1 +=2;
					}
					if (Reg1 > CMD1_RigaASCIILen) {												// Ogni CMD1_RigaASCIILen chr aggiungo un NL
						gVars.ddcmpbuff[i++] = cr;
						gVars.ddcmpbuff[i++] = lf;
						Reg1 = 0;
					}
				}
			}
			SendBuffer(&gVars.ddcmpbuff[0], i);													// Trasmetto il risultato al PC
			TimeMonitor = gVars.gKernelFreeCounter + Milli100;									// Delay tra una trasmissione al PC e la successiva			
			while (TimeMonitor > gVars.gKernelFreeCounter) {									// Attendo fine invio al PC
				if (FindEscape()) {
					SysMonitor.RxCMD = false;
					SysMonitor.state = SendSceltaComando;										// Premuto "Escape": termino comando
					*(Rxpnt) = NULL;															// Cancello ESC dal buffer altrimenti causa l'uscita dal Monitor
				}
			}
			if (SysMonitor.state == SendSceltaComando) {										// Se nella while e' stato premuto ESC termino
				NumBytes = 0;
			} else {
			    NumBytes -= k;
			    MemAddr += k;
			}
		} while (NumBytes > 0);
		BankSelect(0x00);																		// Punto di default al bank 0
		SysMonitor.state = SendSceltaComando;
		break;

	case writeeeprom:
		Dummy = AbsoluteAddress / 0x10000;
		BankSelect((uint8_t) Dummy);
		MemAddr = (uint16_t)AbsoluteAddress;
		NumBytes = strlen((char const*)Rxpnt);
		if ((*(Rxpnt) == ASCII_ZERO) && (*(Rxpnt+1) == ASCII_x)) {							// Dato Hex
			// --- Dato Hex ----
			memset(&gVars.ddcmpbuff[0], 0x0, 64U);											// Fill zero 64 bytes buffer ddcmp per far funzionare la strlen
			for (r=0,k=2; k<NumBytes; r++,k+=2) {
				gVars.ddcmpbuff[r] = (uint8_t)(AsciiToHex((char*)(Rxpnt+k), 2U));
			}
			WriteEEPI2C(MemAddr, (uint8_t *)(&gVars.ddcmpbuff[0]), r);
		} else {
			// --- Dato ASCII ----
			if ((*(Rxpnt+(NumBytes-1)) == ASCII_PIU)) {										// Ultimo chr = '+': aggiungo 0x0d e 0x0a alla stringa ASCII
				NumBytes--;																	// Sottraggo un chr per non memorizzare il chr '+'
				WriteEEPI2C(MemAddr, (uint8_t *)(Rxpnt), NumBytes);							
				MemAddr += (NumBytes);
				gVars.ddcmpbuff[0] = cr;													// NewLine
				gVars.ddcmpbuff[1] = lf; 
				WriteEEPI2C(MemAddr, (uint8_t *)(&gVars.ddcmpbuff[0]), 2U);
			} else {
					WriteEEPI2C(MemAddr, (uint8_t *)(Rxpnt), NumBytes);
			}
		}
		BankSelect(0x00);
		SysMonitor.state = SendSceltaComando;
		break;
		

// ========================================================================
// ========    Read / Write Date / Time    CMD3 - CMD4    =================
// ========================================================================
		
	case Read_date:
		LeggiOrologio();
		r = 0;
		gVars.ddcmpbuff[r++] = cr;
		gVars.ddcmpbuff[r++] = lf;
		sprintf((char*)&gVars.ddcmpbuff[r], "%2u", DataOra.Day);										// Giorno
		if (gVars.ddcmpbuff[r] == Space) gVars.ddcmpbuff[r] = '0';
		if (gVars.ddcmpbuff[r+1] == Space) gVars.ddcmpbuff[r+1] = '0';
		r +=2;
		gVars.ddcmpbuff[r++] = '.';
		sprintf((char*)&gVars.ddcmpbuff[r], "%2u", DataOra.Month);										// Mese
		if (gVars.ddcmpbuff[r] == Space) gVars.ddcmpbuff[r] = '0';
		if (gVars.ddcmpbuff[r+1] == Space) gVars.ddcmpbuff[r+1] = '0';
		r +=2;
		gVars.ddcmpbuff[r++] = '.';
		sprintf((char*)&gVars.ddcmpbuff[r], "%2u", DataOra.Year);										// Anno
		r +=4;
		gVars.ddcmpbuff[r++] = '-';
		sprintf((char*)&gVars.ddcmpbuff[r], "%1u", DataOra.DayOfWeek);									// Giorno della settimana
		if (gVars.ddcmpbuff[r] == Space) gVars.ddcmpbuff[r] = '0';
		//if (gVars.ddcmpbuff[r+1] == Space) gVars.ddcmpbuff[r+1] = '0';
		//r +=2;
		r++;
		sprintf((char*)&gVars.ddcmpbuff[r],"%1s", Nuova_Data);											// Memorizza una stringa ASCII + crlf + NULL
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		SysMonitor.state = Write_date;
		break;

	case Write_date:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		r = SysMonitor.RxCount;																		// Copio numero caratteri digitati in r
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		if (r < 10) {																				// Data troppo corta
			PrintError(DataErrata);																	// Data Errata: segnalo errore al PC
			SysMonitor.state = Write_date;
			break;
		}
		DataOra.Day = atouint32(Rxpnt, 2);
		DataOra.Month = atouint32(Rxpnt+3, 2);
		DataOra.Year = atouint32(Rxpnt+6, 4);
/*MR19
		esito = RTC_SetTimeAndDate(&DataOra);
		if (esito != Resp_OK) {
			PrintError(DataErrata);																	// Data Errata: segnalo errore al PC
			SysMonitor.state = Write_date;
			break;
		}
*/
		RTC_SetTimeAndDate(&DataOra);
		SysMonitor.state = SendSceltaComando;
		break;

	case Read_time:
		LeggiOrologio();
		r = 0;
		gVars.ddcmpbuff[r++] = cr;
		gVars.ddcmpbuff[r++] = lf;
		sprintf((char*)&gVars.ddcmpbuff[r], "%2u", DataOra.Hour);											// Ore
		if (gVars.ddcmpbuff[r] == Space) gVars.ddcmpbuff[r] = '0';
		if (gVars.ddcmpbuff[r+1] == Space) gVars.ddcmpbuff[r+1] = '0';
		r +=2;
		gVars.ddcmpbuff[r++] = ':';
		sprintf((char*)&gVars.ddcmpbuff[r], "%2u", DataOra.Minute);										// Minuti
		if (gVars.ddcmpbuff[r] == Space) gVars.ddcmpbuff[r] = '0';
		if (gVars.ddcmpbuff[r+1] == Space) gVars.ddcmpbuff[r+1] = '0';
		r +=2;
		gVars.ddcmpbuff[r++] = ':';
		sprintf((char*)&gVars.ddcmpbuff[r], "%2u", DataOra.Second);										// Secondi
		if (gVars.ddcmpbuff[r] == Space) gVars.ddcmpbuff[r] = '0';
		if (gVars.ddcmpbuff[r+1] == Space) gVars.ddcmpbuff[r+1] = '0';
		r +=2;
		sprintf((char*)&gVars.ddcmpbuff[r],"%1s", Nuova_Ora);
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		SysMonitor.state = Write_time;
		break;

	case Write_time:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		DataOra.Hour = atouint32(Rxpnt, 2);
		DataOra.Minute = atouint32(Rxpnt+3, 2);
		DataOra.Second = atouint32(Rxpnt+6, 2);
/*MR19		
		esito = RTC_SetTimeAndDate(&DataOra);
		if (esito != Resp_OK) {
			PrintError(OraErrata);																	// Ora Errata: segnalo errore al PC
			SysMonitor.state = Write_time;
			break;
		}
*/
		RTC_SetTimeAndDate(&DataOra);
		SysMonitor.state = SendSceltaComando;
		break;
		

// ========================================================================
// ========    Debug  MIFARE: visualizza errori carta   CMD5  =============
// ========================================================================
	
	case Debug_Mifare:
		// Visualizzo messaggio sul PC
		SendMessageToPC(&CardErrors[0]);
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		SysMonitor.TxCount = 0;
		SysMonitor.state = ShowCardError;
		break;
	
	case ShowCardError:
		if (TestTxMsgInCorso()) break;
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Esco solo con ESC dal Debug Mifare
		}
		break;
		
// ========================================================================
// ========    FILL   EEPROM       CMD6   =================================
// ========================================================================

	case Fill_EEProm:
		// Scelto comando di Fill EEPROM: chiedo START address
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&EE_StartAddress[0]);
		SysMonitor.state = Fill_StartAddress;
		break;
	
	case Fill_StartAddress:
		if (TestTxMsgInCorso()) break;
		if (!SysMonitor.RxCMD) 	break;																	// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;														// Premuto "Escape": termino comando
			break;
		}
		if (!ElaboraAddress()) {
			SysMonitor.state = Fill_EEProm;
			break;
		}
		StartAddress = AbsoluteAddress;
		SysMonitor.state = Ask_Fill_EndAddress;
		break;

	case Ask_Fill_EndAddress:
		// Scelto comando di Fill EEPROM: chiedo END address
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&EE_EndAddress[0]);
		SysMonitor.state = Fill_EndAddress;
		break;
	
	case Fill_EndAddress:
		if (TestTxMsgInCorso()) break;
		if (!SysMonitor.RxCMD) 	break;																	// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;														// Premuto "Escape": termino comando
			break;
		}
		if (!ElaboraAddress() || AbsoluteAddress <= StartAddress) {
			SysMonitor.state = Ask_Fill_EndAddress;
			break;
		}
		EndAddress = AbsoluteAddress;
		SysMonitor.state = Fill_EE_Value;
		break;
		
	case Fill_EE_Value:
		// Scelto comando di Fill EEPROM: chiedo valore da programmare
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&ValueToWrite[0]);
		SysMonitor.state = WaitFillParameter;
		break;

	case WaitFillParameter:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		if (GetValToWrite()) {																		// Elaboro Parametro col quale riempire tutte le EEPROMS
			if (NumBytes > 1) {
				PrintError(InvalidParameter);														// Segnalo errore al PC
				SysMonitor.state = Fill_EE_Value;
				break;
			}
			if ((StartAddress == 0 && EndAddress == 0x1ffff) ||										// Se Fill Entire EEprom chiede conferma e non riempie le sezioni con i codici di sicurezza
				(StartAddress == 0 && EndAddress == 0x20000))
			{
				SysMonitor.state = AskConfirmFill_EEProm;
			}
			else
			{
				SysMonitor.state = Esegui_Fill_EEprom;												// ATTENZIONE a NON sovrascrivere le aree codici di sicurezza
			}
			break;
		} else {
			SysMonitor.state = Fill_EE_Value;
			break;
		}
	
	case AskConfirmFill_EEProm:
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&SicuroEEprom[0]);															// Chiedo conferma Azzeramento EEPROM
		SysMonitor.state = WaitConfirmEraseEEprom;
		break;
		
	case WaitConfirmEraseEEprom:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		if (*Rxpnt == 'Y' || *Rxpnt == 'y') {
			SendMessageToPC(&Erase_EEPROM[0]);														// Msg "Filling EEPROM: wait Restart"
			while (TestTxMsgInCorso()) {};
		}
		RedLedCPU(ON);
		FillEEPROMS(ValToWrite);
		SendConfToEEprom(false);																	// Ricreo il file di configurazione per esportarlo su chiave USB
		RedLedCPU(OFF);
		WatchDogReset();
		while (1) {}																				// Attendo Reset da WDT
			
	case Esegui_Fill_EEprom:
		SendMessageToPC(&Fill_EEPROM[0]);															// Msg "Filling EEPROM: wait...."
		while (TestTxMsgInCorso()) {};

		GreenLedCPU(OFF);
		RedLedCPU(ON);
		if (StartAddress >= 0x10000) {
			BankSelect(0x01);
		}
		FillEEPI2C((uint)StartAddress, ValToWrite, (EndAddress- StartAddress));
		BankSelect(0x00);
		RedLedCPU(OFF);
		SysMonitor.state = SendSceltaComando;
		break;

		
// ========================================================================
// ========    Azzeramento  AUDIT    CMD7 =================================
// ========================================================================

	case AzzeraAudit:
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&SicuroAudit[0]);															// Chiedo conferma Azzeramento Audit
		SysMonitor.state = WaitConfirmEraseAudit;
		break;

	case WaitConfirmEraseAudit:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		if (*Rxpnt == 'Y' || *Rxpnt == 'y') {
			AuditINClear();
			AuditLRClear();
			AuditExtClearData(true);																// Azzero contatore blocchi e contatore numero prelievi
			SendMessageToPC(&AuditErased[0]);														// Msg Audit Erased
		}
		SysMonitor.state = SendSceltaComando;
		break;
		
// ========================================================================
// ========    SET  Default  CONFIG    CMD8    ============================
// ========================================================================
// Inseriva in memoria una configurazione di default prima dello sviluppo 
// del configuratore.
// ========================================================================
// ========    Erase Area interscambio USB  (Fill con 0xFF)  CMD9  ========
// ========================================================================
/*
	case ClrAreaUSB:
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&SicuroClrAreaUSB[0]);														// Chiedo conferma Clr Area USB
		SysMonitor.state = ResetAreaInterscambioUSB;
		break;

	case ResetAreaInterscambioUSB:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		if (*Rxpnt == 'Y' || *Rxpnt == 'y') {
			FillEEPI2C((uint)SystemPswBoot, 0xFF, (ReservedFU_2-SystemPswBoot));
			SendMessageToPC(&AreaUSBErased[0]);														// Msg Area USB Erased
		}
		SysMonitor.state = SendSceltaComando;
		break;
*/
// ========================================================================
// ========    Visualizza Price e SelNum Vendita Richiesta  CMD10   =======
// ========================================================================

	case DebugVendReqData:
		// Visualizzo messaggio sul PC
		if (OpMode == MDB) {
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s - USF:%3d\r\n", SelecPriceNumMDB, UnitScalingFactor);
		} else {
			if (gOrionConfVars.PriceHolding) {
				sprintf((char*)&gVars.ddcmpbuff[0], "%1s - USF:%3d\r\n", SelecNumber, UnitScalingFactor);
			} else {
				sprintf((char*)&gVars.ddcmpbuff[0], "%1s - USF:%3d\r\n", SelecPrice, UnitScalingFactor);
			}
		}
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		SysMonitor.TxCount = 0;
		SysMonitor.state = ShowVendReqData;
		break;
	
	case ShowVendReqData:
		Monitor_SendProtChar_To_PC();
		Monitor_SendDeniedReason_To_PC();
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Con ESC esco dal Menu
		}
		break;


// ========================================================================
// ========    Verbose Executive and DDCMP   Protocol  CMD11 ==============
// ========================================================================

	case DebugExecutiveProtocol:
		// Visualizzo messaggio sul PC
		SendMessageToPC(&ExecProtComms[0]);
		SysMonitor.EnableLogRxExec = false;
		SysMonitor.GenericCntIN = 0;
		SysMonitor.GenericCntOUT = 0;
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		SysMonitor.TxCount = 0;
		SysMonitor.state = ShowExecProtComm;
		if (gVMC_ConfVars.ExecutiveSLV == TRUE) {												// CMD 11 per echo Executive
			Dummy = RecProtExecMST;
			WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
		}
		break;
			
	case ShowExecProtComm:
		if (gVMC_ConfVars.ExecutiveSLV == TRUE) {
			Monitor_SendProtChar_To_PC();														// CMD 11 per echo Executive
		} else {
			Send_DDCMP_Echo_Al_PC();															// CMD 11 per echo DDCMP
		}
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.EnableLogRxExec = false;														// Stop recording
			Dummy = 0;																				// Clr memo registrazione protocollo
			WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
			SysMonitor.state = SendSceltaComando;													//Con ESC esco dal Menu, con "Pause" metto in pausa la trasmissione al PC
		}
		break;

	case TxPauseExecMessage:
		// Visualizzo messaggio sul PC
		SysMonitor.EnableLogRxExec = false;
		SysMonitor.TxCount = 0;																		// Riparto dalla colonna 1
		SendMessageToPC(&PauseMSG[0]);
		SysMonitor.state = PauseShowExecProtComm;
		break;

	case PauseShowExecProtComm:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.GenericCntIN = 0;
		SysMonitor.GenericCntOUT = 0;
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.EnableLogRxExec = false;														// Stop recording
			Dummy = 0;																				// Clr memo registrazione protocollo
			WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
			SysMonitor.state = SendSceltaComando;													// Con ESC esco dal Menu, con "Pause" riprendo la trasmissione al PC
		}
		break;
		
// ========================================================================
// ========    Verbose MDB    Protocol  CMD12                ==============
// ========================================================================

	case DebugMDBProtocol:
		// Visualizzo messaggio sul PC
		SendMessageToPC(&MDBProtComms[0]);
		SysMonitor.EnableLogRxMDB = false;
		SysMonitor.GenericCntIN = 0;
		SysMonitor.GenericCntOUT = 0;
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		SysMonitor.TxCount = 0;
		SysMonitor.state = ShowMDBProtComm;
		Dummy = RecProtocolMDB;																		// Memo registrazione protocollo per registrare inizializzazione perif MDB al reset
		WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
		break;
			
	case ShowMDBProtComm:
		Monitor_SendProtChar_To_PC();
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.EnableLogRxMDB = false;														// Stop recording
			Dummy = 0;																				// Clr memo registrazione protocollo
			WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
			SysMonitor.state = SendSceltaComando;													//Con ESC esco dal Menu, con "Pause" metto in pausa la trasmissione al PC
		}
		break;
		
	case TxPauseMDBMessage:
		// Visualizzo messaggio sul PC
		SysMonitor.EnableLogRxMDB = false;
		SysMonitor.TxCount = 0;																		// Riparto dalla colonna 1
		SendMessageToPC(&PauseMSG[0]);
		SysMonitor.state = PauseShowMDBProtComm;
		break;
		
	case PauseShowMDBProtComm:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.GenericCntIN = 0;
		SysMonitor.GenericCntOUT = 0;
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.EnableLogRxMDB = false;														// Stop recording
			Dummy = 0;																				// Clr memo registrazione protocollo
			WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
			SysMonitor.state = SendSceltaComando;													// Con ESC esco dal Menu, con "Pause" riprendo la trasmissione al PC
		}
		break;
		
// ========================================================================
// ========    Azzeramento  Backup EEPROM    CMD13    =====================
// ========================================================================

	case AzzeraBackupEEPROM:
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&SicuroBackupEE[0]);														// Chiedo conferma Azzeramento Backup EEPROM
		SysMonitor.state = WaitConfirmEraseBackupEE;
		break;

	case WaitConfirmEraseBackupEE:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		if (*Rxpnt == 'Y' || *Rxpnt == 'y') {
			EraseBackupData();
			ReloadAndCkeck_BackupRAM();
			SendMessageToPC(&BackupErased[0]);														// Msg Backup EEProm Erased
		}
		SysMonitor.state = SendSceltaComando;
		break;
				
// ========================================================================
// ========    Send Config File To PC    CMD14        =====================
// ========================================================================

	case SendConfigToPC:
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&PressAnyKey[0]);															// Attendo qualsiasi tasto per dar tempo di attivare la memorizzazione
		SysMonitor.DontTxChrBack = true;															// Abilito a mettere qualsiasi chr ricevuto nel buffer Rx 
		SysMonitor.state = WaitConfirmSendConfig;
		break;

	case WaitConfirmSendConfig:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		MemAddr = TxConfigBuff;																		// In MemAddr address inizio file Config in EEPROM
		BankSelect(TxConfigBuffBank);
		i = 0;																						// Contatore pagine lette e trasmesse
		do {
			WDT_Clear(gVars.devWDT);  																// Trigger WDT
			ReadEEPI2C((MemAddr), (uint8_t*)&gVars.ddcmpbuff[0], PageSize);							// Leggo una pagina dalla EEPROM
			r = strlen((char*)&gVars.ddcmpbuff[0]);													// Cerco il fine file
			if (r == 0) break;
			SendBuffer(&gVars.ddcmpbuff[0], r);
			MemAddr += r;
			if (++i == 96) break;																	// Raggiunto il numero massimo di pagine senza incontrare il fine file
			TimeMonitor = gVars.gKernelFreeCounter + Milli100;										// Delay tra una trasmissione al PC e la successiva			
			while (TimeMonitor > gVars.gKernelFreeCounter);											// Attendo fine invio al PC
		} while (r == PageSize);
		SysMonitor.state = SendSceltaComando;
		break;
						
// ========================================================================
// ========    TOGGLE  DDCMP  LOG    CMD15   ==============================
// ========================================================================

	case Toggle_DDCMP:
		// Visualizzo attuale stato Flags di Log
		if (TestTxMsgInCorso()) break;
		sprintf((char*)&gVars.ddcmpbuff[0], "%1s", ActualDDCMPLogState);							// "Actual Log State: "
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		sprintf((char*)&gVars.ddcmpbuff[r],"%1d\r\n", (SystOpt4 & (TxIrDALog | MaskTipoLog)));
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		sprintf((char*)&gVars.ddcmpbuff[r],"%1s", SetLogDDCMP);										// "Set Log (0=Log disabled 1=Prot 3=Prot+Time 5=Prot+Flags"
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		SysMonitor.state = WaitDDCMP_LogParameter;
		break;

	case WaitDDCMP_LogParameter:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape())
		{
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}
		if (GetValToWrite())																		// Attendo inserimento Tipo di Log
		{
			if (ValToWrite > 0)
			{
				if (ValToWrite > 5 || ValToWrite%2 == 0)											// Se Tipo Log > 5 o multiplo di 2 segnalo Errore al PC
				{
					PrintError(InvalidParameter);
					SysMonitor.state = Toggle_DDCMP;
					break;
				}
			}
			SysMonitor.state = Memo_DDCMP_Log_State;
			break;
		}
		else 
		{
			SysMonitor.state = Toggle_DDCMP;
			break;
		}
	
	case Memo_DDCMP_Log_State:
		AbsoluteAddress = IICEEPConfigAddr(EEFree4);												// 0x11F
		Dummy = AbsoluteAddress / 0x10000;
		BankSelect((uint8_t) Dummy);
		MemAddr = (uint16_t)AbsoluteAddress;
		WriteEEPI2C(MemAddr, (uint8_t *)(&ValToWrite), NumBytes);
		BankSelect(0x00);
		ReadEEPI2C(IICEEPConfigAddr(EEFree4), (uint8_t *)(&SystOpt4), sizeof(SystOpt4));
		if (SystOpt4 > 0)
		{
			  InitLogBufferPointers(true);															// Attivando il LOG inizializzo pointers al Log Buffer in EEPROM
		}
		SysMonitor.state = SendSceltaComando;
		break;
		
// ========================================================================
// ========    Send Log Buffer To PC    CMD16        ======================
// ========================================================================

	case SendLogBufferToPC:
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&PressAnyKey[0]);															// Attendo qualsiasi tasto per dar tempo di attivare la memorizzazione
		SysMonitor.DontTxChrBack = true;															// Abilito a mettere qualsiasi chr ricevuto nel buffer Rx 
		SysMonitor.state = WaitConfirmSendLogBuffer;
		break;

	case WaitConfirmSendLogBuffer:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.state = SendSceltaComando;													// Premuto "Escape": termino comando
			break;
		}

		//	Leggo numero di caratteri presenti nel Log Buffer e inverto il valore
		//	perche' nella EEProm sono memorizzati BigEndian
		
		LogBuffCnt = ReadLogFileContentLen();
		if (LogBuffCnt == 0) {																		// Se non ci sono dati termino comando
			SendMessageToPC(&NoLogData[0]);															// Msg No Log Data
			SysMonitor.state = SendSceltaComando;
			break;
		}
		ReadEEPI2C(LogFileSize, (uint8_t *)(&MemoryBytes[0]), 3U);									// In LogBuffSize dimensioni del Log Buffer in EEProm
		LogBuffSize = MemoryBytes[0];																// LSB
		LogBuffSize += (MemoryBytes[1]<<8);															// Medium
		LogBuffSize += (MemoryBytes[2]<<16);														// MSB
		
		if (LogBuffCnt >= LogBuffSize) {
			LogBuffCnt = LogBuffSize;																// Buffer overflow: lo scarico tutto
		} else {
			LogBuffCnt %= LogBuffSize;																// Il buffer non e' pieno e prelevo solo LogBuffCnt byte
		}
		// Leggo address inizio Log Buffer in EEProm
		Rd_EEBuffStartAddr_And_BankSelect(BUF_LOGFILE);											// In  MemoryBytes[] StartAddress del buffer e bank della EE gia' selezionato
		LogBuffStartAddr = 0;
		LogBuffStartAddr += MemoryBytes[1];															// LSB
		LogBuffStartAddr += (MemoryBytes[0]<<8);													// MSB
		
		do {
			WDT_Clear(gVars.devWDT);  																// Trigger WDT
			if (LogBuffCnt > PageSize) {
				i = PageSize;
			} else {
				i = (uint16_t)LogBuffCnt;
			}
			memset(&gVars.ddcmpbuff[0], 0, 512);													// Azzero buffer ddcmp per far calcolare poi alla strlen la lunghezza del buff
			ReadEEPI2C((uint16_t)(LogBuffStartAddr), (uint8_t*)&gVars.ddcmpbuff[0], i);					// Leggo una pagina dalla EEPROM e gli ultimi bytes residui del log buffer
			k = ddcmpOffs;
			for (r=0; r < i; r++) {
				if (gVars.ddcmpbuff[r] == cr && gVars.ddcmpbuff[r+1] == lf 
						&& gVars.ddcmpbuff[r+2] != cr && gVars.ddcmpbuff[r+2] != lf) {
					gVars.ddcmpbuff[k++] = cr;
					gVars.ddcmpbuff[k++] = lf;
					r++;
				} else {
					HexByteToAsciiInt((uint8_t) gVars.ddcmpbuff[r], (uint16_t*)&gVars.ddcmpbuff[k], 0x01);
					k +=2;
				}
			}
			i = strlen((char const*)&gVars.ddcmpbuff[ddcmpOffs]);
			SendBuffer(&gVars.ddcmpbuff[ddcmpOffs], i);												// Trasmetto i dati al PC
			TimeMonitor = gVars.gKernelFreeCounter + Milli100;										// Delay tra una trasmissione al PC e la successiva			
			while (TimeMonitor > gVars.gKernelFreeCounter);											// Attendo fine invio al PC
			LogBuffStartAddr +=r;
			LogBuffCnt -= r;
			
			if (LogBuffCnt <= SpaceAvailable) {						// Debug
				SpaceAvailable = LogBuffCnt;	
			}
			
			
		} while (LogBuffCnt > 0);

		BankSelect(0x00);																					// Default: Address per bank0 della EEProm 1 da 1024kbit
		SysMonitor.state = SendSceltaComando;
		break;
						
// ========================================================================
// ========        Get Config from PC       CMD17    ======================
// ========================================================================
// Con l'USB la ricezione e' molto piu' rapida della memorizzazione in EE.
// Si usa un buffer circolare con due pointer di inserimento chr che si
// alternano: quando il primo ha raggiunto il limite del buffer, si inizia
// ad usare il secondo. Quando anche il secondo ha raggiunto il limite del
// buffer, si riutilizza il primo che nel frattempo era stato azzerato.
// La flag "ChrResidui" il  puntatore in uso sara' sostituito dall'altro
// alla prossima ricezione dati e serve quindi memorizzare gli ultimi chr 
// appena ricevuti pria dello switch.
// Utilizzo il buffer gVars.tempRTx1 riempiendolo fino a BUF_THRESHOLD
// caratteri prima dello switch al secondo pointer che riparte da zero.
// In caso di errori di scrittura EE si annulla tutto per evitare di
// utilizzare una configurazione corrotta.		
		
		
		uint32_t	LocalSecondBuff, LocalChrResidui;
		bool		resp;
		
	case GetConfFromPC:
		if (TestTxMsgInCorso()) break;
		SendMessageToPC(&StartSendingConf[0]);														// Msg "Start Sending Config File"
		Rd_EEBuffStartAddr_And_BankSelect(BUF_RXCONFIG);											// In  MemoryBytes[] StartAddress del buffer e bank della EE gia' selezionato
		LogBuffStartAddr = 0;
		LogBuffStartAddr += MemoryBytes[1];															// LSB
		LogBuffStartAddr += (MemoryBytes[0]<<8);													// MSB
		FirstByteFreeAddr = LogBuffStartAddr;														// Effettivo indirizzo del primo byte libero nel Log Buffer
		//DEB_Blk_FULL = 0;	// DEBUG
		//DEB_Blk_LESS = 0;	// DEBUG
		//StoreCnt_1 = 0;		// DEBUG
		//StoreCnt_2 = 0;		// DEBUG
		resp = false;
		CharCntOut = 0;
		SecondBuff = false;
		ChrResidui = false;
		NumCharIN_1 = 0;
		NumCharIN_2 = 0;
		NumCharOUT_1 = 0;
		NumCharOUT_2 = 0;
		memset(&gVars.tempRTx1[0], 0, sizeof(gVars.tempRTx1));										// Azzero buffer ricezione
		Touts.tmMon = TenSec;																		// Dopo 10 sec senza ricevere dati esco
		SysMonitor.state = GettingConfBuffer;
		break;

	case GettingConfBuffer:
		if (TestTxMsgInCorso()) break;
		
		do {
				__disable_irq();
				LocalSecondBuff = SecondBuff;
				LocalChrResidui = ChrResidui;
				__enable_irq();
				
				if (LocalSecondBuff == false)
				{
					//-- Pointer pari: NumCharIN_1 --
					if (LocalChrResidui == true)
					{
						ChrResidui = false;
						NumCharIN_2 -= NumCharOUT_2;
						resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], NumCharIN_2);			// Memo restanti chr ricevuti con pointer NumCharIN_2
						if (resp == false) goto END_CMD17;																	// Errore scrittura EE: interrompo e annullo dati ricevuti
						FirstByteFreeAddr += NumCharIN_2;
						CharCntOut = 0;																						// Offset nuovamente ad inizio buffer ricezione
						NumCharOUT_2 = 0;
						NumCharIN_2 = 0;
						//DEB_Blk_LESS++;		// DEBUG
					}
					else
					{
						if (NumCharIN_1 > (NumCharOUT_1 + BUF_THRESHOLD))
						{
							RedLedCPU_Toggle();
							resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], BUF_THRESHOLD);	// Scrivo BUF_THRESHOLD bytes nel ConfigRxBuffer in EEProm
							if (resp == false) goto END_CMD17;																// Errore scrittura EE: interrompo e annullo dati ricevuti
							FirstByteFreeAddr += BUF_THRESHOLD;
							CharCntOut += BUF_THRESHOLD;
							NumCharOUT_1 += BUF_THRESHOLD;
							Touts.tmMon = TOUT_ATTESA_CHR;																	// Dopo qualche secondo senza ricevere dati considero terminata la ricezione
							//DEB_Blk_FULL++;		// DEBUG
						}
					}
				}
				else
				{
					//-- Pointer dispari: NumCharIN_2 --
					if (LocalChrResidui == true)
					{
						ChrResidui = false;
						NumCharIN_1 -= NumCharOUT_1;
						resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], NumCharIN_1);			// Memo restanti chr ricevuti con pointer NumCharIN_1
						if (resp == false) goto END_CMD17;																	// Errore scrittura EE: interrompo e annullo dati ricevuti
						FirstByteFreeAddr += NumCharIN_1;
						CharCntOut = 0;																						// Offset nuovamente ad inizio buffer ricezione
						NumCharOUT_1 = 0;
						NumCharIN_1 = 0;
						//DEB_Blk_LESS++;
					}
					else
					{
						if (NumCharIN_2 > (NumCharOUT_2 + BUF_THRESHOLD))
						{
							RedLedCPU_Toggle();
							resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], BUF_THRESHOLD);	// Scrivo BUF_THRESHOLD bytes nel ConfigRxBuffer in EEProm
							if (resp == false) goto END_CMD17;																// Errore scrittura EE: interrompo e annullo dati ricevuti
							FirstByteFreeAddr += BUF_THRESHOLD;
							CharCntOut += BUF_THRESHOLD;
							NumCharOUT_2 += BUF_THRESHOLD;
							Touts.tmMon = TOUT_ATTESA_CHR;																	// Dopo qualche secondo senza ricevere dati considero terminata la ricezione
							//DEB_Blk_FULL++;
						}
					}
				}
		} while(Touts.tmMon > Milli100);
		//-- Timeout ricezione scaduto --
		if ((FirstByteFreeAddr == LogBuffStartAddr) && ((NumCharIN_1 + NumCharIN_2) == 0))
		{
			sprintf((char*)&gVars.ddcmpbuff[0], "\r\n%1s", NoConfigData);												// Msg "No Data Received"
			r = strlen((char const*)&gVars.ddcmpbuff[0]);
			resp = true;																								// Per uscire senza msg di FAIL
		}
		else
		{
			if (resp == true)
			{
				//-- Scrittura EE OK: Memo gli eventuali ultimi chr ricevuti --
				if (LocalSecondBuff == false)
				{
					//-- Pointer pari: NumCharIN_1 --
					if (LocalChrResidui == true)
					{
						gVars.tempRTx1[NumCharIN_2++] = NULL;															// Chr chiusura buffer in EE
						NumCharIN_2 -= NumCharOUT_2;
						resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], NumCharIN_2);		// Memo residui da NumCharIN_2
						if (resp == false) goto END_CMD17;
						FirstByteFreeAddr += NumCharIN_2;
						CharCntOut += NumCharIN_2;
					}
					gVars.tempRTx1[NumCharIN_1++] = NULL;																// Chr chiusura buffer in EE
					NumCharIN_1 -= NumCharOUT_1;
					resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], NumCharIN_1);			// Memo residui da NumCharIN_1
					if (resp == false) goto END_CMD17;
				}
				else
				{
					//-- Pointer dispari: NumCharIN_2 --
					if (LocalChrResidui == true)
					{
						gVars.tempRTx1[NumCharIN_1++] = NULL;															// Chr chiusura buffer in EE
						NumCharIN_1 -= NumCharOUT_2;
						resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], NumCharIN_1);		// Memo residui da NumCharIN_1
						if (resp == false) goto END_CMD17;
						FirstByteFreeAddr += NumCharIN_1;
						CharCntOut += NumCharIN_1;
					}
					gVars.tempRTx1[NumCharIN_2++] = NULL;																// Chr chiusura buffer in EE
					NumCharIN_2 -= NumCharOUT_2;
					resp = WriteEEPI2C((uint16_t)FirstByteFreeAddr, &gVars.tempRTx1[CharCntOut], NumCharIN_2);			// Memo residui da NumCharIN_2
					if (resp == false) goto END_CMD17;
				}
				if (resp == true)
				{
					snprintf((char*)&gVars.ddcmpbuff[0], 50U, RxConf_OK, (FirstByteFreeAddr - LogBuffStartAddr));		// Msg "Config File OK: Received %d bytes"
					r = strlen((char const*)(char const*)&gVars.ddcmpbuff[0]);
				}
			}
		}		

END_CMD17:
		if (resp == false)
		{
			//-- Scrittura EE fallita, annullo quanto ricevuto --
			memset(&gVars.tempRTx1[0], 0, 16U);																			// Predispongo nnullamento buffer in EE
			resp = WriteEEPI2C((uint16_t)LogBuffStartAddr, &gVars.tempRTx1[0], 4U);
			sprintf((char*)&gVars.ddcmpbuff[0], "\r\n%1s", EEWriteFail);												// Visualizzo errore scrittura EEprom
			r = strlen((char const*)&gVars.ddcmpbuff[0]);
		}
		BankSelect(0x00);
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SysMonitor.TxMsgInCorso = 1;
		Touts.tmMon = OneMin;
		SysMonitor.state = SendSceltaComando;
		break;

// ========================================================================
// ========    Verbose Executive Master  Protocol  CMD18     ==============
// ========================================================================

	case DebugExecMasterProtocol:
		// Visualizzo messaggio sul PC
		SendMessageToPC(&ExecProtComms[0]);
		SysMonitor.EnableLogRxExec = false;
		SysMonitor.GenericCntIN = 0;
		SysMonitor.GenericCntOUT = 0;
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		SysMonitor.TxCount = 0;
		SysMonitor.state = ShowExecMSTProtComm;
		Dummy = RecProtocolExec;																	// Memo registrazione protocollo executive
		WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
		break;
			
	case ShowExecMSTProtComm:
		Monitor_SendProtChar_To_PC();
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.EnableLogRxExec = false;														// Stop recording
			Dummy = 0;																				// Clr memo registrazione protocollo
			WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
			SysMonitor.state = SendSceltaComando;													//Con ESC esco dal Menu, con "Pause" metto in pausa la trasmissione al PC
		}
		break;

	case TxPauseExecMSTMessage:
		// Visualizzo messaggio sul PC
		SysMonitor.EnableLogRxExec = false;
		SysMonitor.TxCount = 0;																		// Riparto dalla colonna 1
		SendMessageToPC(&PauseMSG[0]);
		SysMonitor.state = PauseShowExecMSTProtComm;
		break;

	case PauseShowExecMSTProtComm:
		if (!SysMonitor.RxCMD) 	break;																// Nessun comando ricevuto
		SysMonitor.GenericCntIN = 0;
		SysMonitor.GenericCntOUT = 0;
		SysMonitor.RxCMD = 0;
		SysMonitor.RxCount = 0;
		if (FindEscape()) {
			SysMonitor.EnableLogRxExec = false;														// Stop recording
			Dummy = 0;																				// Clr memo registrazione protocollo
			WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);
			SysMonitor.state = SendSceltaComando;													// Con ESC esco dal Menu, con "Pause" riprendo la trasmissione al PC
		}
		break;
		
// ========================================================================
// ========    Exit Monitor             ===================================
// ========================================================================

	case Quit_Monitor:
		// Visualizza la fine della sessione Monitor
	    if (TestTxMsgInCorso()) break;
		sprintf((char*)&gVars.ddcmpbuff[0], "%1s", MonitorEndSession);
		r = strlen((char const*)&gVars.ddcmpbuff[0]);
		gVars.ddcmpbuff[r++] = Bel;
		SendBuffer(&gVars.ddcmpbuff[0], r);
		SCI_ClearFlagTx;																		// Attendo trasmissione ultimo carattere del buffer
		SysMonitor.state = WaitMonitorEndSession;
		break;
	
	case WaitMonitorEndSession:
	    if (!SCI_FlagTx) break;
		memset(&SysMonitor, 0, sizeof(SysMonitor));												// Azzera variabili SysMonitor
		memset(&gVars.tempRTx1[0], 0, sizeof(gVars.tempRTx1));									// Azzero buffer tempRTx1
		memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));								// Azzero buffer ddcmp
	    free (Rxpnt);																			// Libero spazio RAM usate per RxBuff
	    gVars.pnt_data = 0;																		// Azzero tutti gli eventuali chr registrati dalla seriale
		gVars.pnt_timing = OffsetDatiLog;
		Dummy = 0;
		WriteEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Dummy), 2U);							// Cancello registrazione sessione Monitor
		break;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: GetValToWrite
	Leggo dal buffer ddcmpbuff il numero ricevuto che rappresenta il parametro da scrivere in
	memoria.
	
	IN:	  - buffer ricevuto dal PC nel buffer allocato per il monitor e puntato da Rxpnt
	OUT:  - uint16_t Hex
\*----------------------------------------------------------------------------------------------*/
static bool GetValToWrite(void) {
		
	NumBytes = strlen((char const*)Rxpnt);														// Lunghezza comando ricevuto in NumBytes
	if ((NumBytes == 3) || (NumBytes > 4))
	{
		PrintError(InvalidParameter);															// Dato troppo lungo: segnalo errore al PC
		return false;
	} 
	ValToWrite = AsciiToHex((char*)Rxpnt, 0);													// In ValToWrite il valore hex dei chr ASCII ricevuti
	if (NumBytes > 1)
	{
		NumBytes /= 2;																			// Il numero di bytes HEx e' la meta' di quelli ASCII
	}
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: GetNumero
	Aspetto dal PC una serie di caratteri numerici.
	
	IN:	  - buffer ricevuto dal PC in gVars.ddcmpbuff e numero chr attesi
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static bool GetNumero(uint8_t numchar) {
	uint8_t	n, i;
	char	ch;
	
	n = strlen((char const*)Rxpnt);																// Lunghezza comando ricevuto in n
	if (n>numchar) return false;
	NumBytes = 0;
	for (i=0; i<n; i++){
		if ((ch = *(Rxpnt+i)) >='0' && ch<='9'){
			NumBytes*= 10;
			NumBytes+= ch-'0';																	// In NumBytes il valore hex dei chr ASCII ricevuti
		} else {
			return false;
		}
	}
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: GetNumBytes
	Leggo dal buffer ddcmpbuff il numero ricevuto che rappresenta il numero di bytes sul quale
	operare.
	
	IN:	  - buffer ricevuto dal PC in gVars.ddcmpbuff e numero bytes attesi
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static bool GetNumBytes(uint8_t numchar)
{
	uint8_t	n;
	
	n = strlen((char const*)Rxpnt);																// Lunghezza comando ricevuto in n
	if (n>numchar) return false;
	NumBytes = atouint32(Rxpnt, n);																// In NumBytes il valore hex dei chr ASCII ricevuti
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: ElaboraAddress
	Riceve l'indirizzo della Memoria in 4 caratteri Ascii e li trasforma in un uint16_t Hex. 
	
	IN:	  - buffer ricevuto dal PC in gVars.ddcmpbuff
	OUT:  - AbsoluteAddress + true se non ci sono caratteri errati (ad es "Z" o "N")
\*----------------------------------------------------------------------------------------------*/
static bool ElaboraAddress(void)
{
	uint8_t	ch, offset;
	
	offset = 0;
	ch = strlen((char const*)Rxpnt);															// Numero di caratteri dell'indirizzo
	if (ch > 5)
	{
		PrintError(InvalidAddress);																// Address troppo lungo: segnalo errore al PC
		return false;
	}
	AbsoluteAddress = 0;
	while((ch = *(Rxpnt+offset)) != 0)
	{
		AbsoluteAddress *= 16;
		AbsoluteAddress |= CharConversion(ch);
		offset++;
	}
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: ElaboraNumeroComando
	Verifica quale comando e' stato ricevuto.
	
	IN:	  - buffer ricevuto dal PC in gVars.ddcmpbuff
	OUT:  - True se comando corretto o "ESC" o "?", False se comando non valido
\*----------------------------------------------------------------------------------------------*/
static bool ElaboraNumeroComando(void) {
	uint8_t	n;
	
	if (FindEscape()) {
		SCI_RxOFF();																			// Disattivo ricezione seriale
		SysMonitor.state = Quit_Monitor;														// Premuto "Escape": termino sessione Monitor
		gVars.ddcmpbuff[0] = 0;
		return true;
	}
	if (*Rxpnt == QuestionMark) {
		SysMonitor.state = SendMenuComandi;														// Premuto "?": visualizzo elenco comandi
		return true;
	}
	n = strlen((char const*)Rxpnt);																// Lunghezza comando ricevuto in n
	cmdval = atouint32(Rxpnt, n);																// Trasformo in numero Hex il valore ricevuto in ASCII

	switch (cmdval) {
	case (1):
	case (2):
	case (21):
		if (!TestMenuAvailable(cmdval)) return false;
		SysMonitor.state = Ask_Address;
		if (cmdval == 21) {
			DisplayASCII = TRUE;
		} else {
			DisplayASCII = FALSE;
		}
		break;
	case (3):
		SysMonitor.state = Read_date;
		break;
	case (4):
		SysMonitor.state = Read_time;
		break;
	case (5):
		SysMonitor.state = Debug_Mifare;
		break;
	case (6):
		if (!TestMenuAvailable(cmdval)) return false;
		SysMonitor.state = Fill_EEProm;
		break;
	case (7):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = AzzeraAudit;
		break;
/*
	case (8):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = DefaultConfig;
		break;

	case (9):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = ClrAreaUSB;
		break;
*/		
	case (10):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = DebugVendReqData;
		break;
	case (11):
		//if (OpMode == MDB) return false;											// Nel PK3-MDB e PK3-PAR non c'e' il protocollo Executive
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = DebugExecutiveProtocol;
		break;
	case (12):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = DebugMDBProtocol;
		break;
	case (13):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = AzzeraBackupEEPROM;
		break;
	case (14):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = SendConfigToPC;
		break;
	case (15):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = Toggle_DDCMP;
		break;
	case (16):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = SendLogBufferToPC;
		break;
	case (17):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = GetConfFromPC;
		break;
/*
	case (18):
		if (!TestMenuAvailable(cmdval)) return false;
		SysMonitor.state = Read_gVars_Struct;
		break;
*/		
	case (18):
		if (OpMode == MDB) return false;
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = DebugExecMasterProtocol;
		break;

/*  Ex Read-Write Registri PN512
	case (17):
		if (!TestMenuAvailable(cmdval)) return false; 
		SysMonitor.state = AskSceltaRead_Write;
		break;
*/		
	case (99):
		if (SysMonitor.MonFeatures == MonCompleto) {
			while (1) {};																// Comando di RESET
		}
	default:
		// Comando sconosciuto
		PrintError(CmdUnknown);															// Segnalo errore al PC
		return false;
	}
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: TestMenuAvailable
	Verifica se il menu richiesto dal comando richiesto e' disponibile in base alla password 
	introdotta.
	
	IN:	  - 
	OUT:  - true se menu disponibile, altrimenti false 
\*----------------------------------------------------------------------------------------------*/
static bool TestMenuAvailable(uint16_t CmdNum) {
	
	uint8_t			i;
	const uint8_t* 	id_pntr;

	switch (SysMonitor.MonFeatures) {
	case (MonCompleto):
		return true;																// Disponibili tutti i menu
	case (MonCliente):
		//return true;																// Disponibili tutti i menu
	case (MonUtente):
		id_pntr = UserCmdList;														// Pointer all'array della lista comandi User
		for (i=0; i < UserCmdList_NumElem; i++) {
			if (CmdNum == id_pntr[i]) return true;									// Il comando richiesto e' nella lista dei comandi User
		}
	}
	return false;
}

/*---------------------------------------------------------------------------------------------*\
Method: PrintError
	Invia al PC un messaggio di errore
	
	IN:	  - pointer al messaggio di errore da inviare al PC
	OUT:  - Stringa da inviare al PC
\*----------------------------------------------------------------------------------------------*/
static void PrintError(const char * data) {
	uint8_t	r;

	sprintf((char*)&gVars.ddcmpbuff[0],"\r\n%1s\r\n", data);											// Memorizza una stringa ASCII + crlf + NULL
	r = strlen((char const*)&gVars.ddcmpbuff[0]);
	SendBuffer(&gVars.ddcmpbuff[0], r);															// Trasmetto il buffer al PC
	SysMonitor.TxMsgInCorso = 1;
}

/*---------------------------------------------------------------------------------------------*\
Method: FindEscape
	Controlla se all'interno dei dati ricevuti dal PC c'e' il carattere 0x1B (ESC) che indica
	la volonta'  di abortire il comando in corso.
	
	IN:	  - gVars.ddcmpbuff[0]
	OUT:  - true se trovato ESC
\*----------------------------------------------------------------------------------------------*/
static bool FindEscape(void) {
	
	char const	*pos;
		
	pos = strchr((char const*)Rxpnt, ESC);
	return (pos != 0 ? true : false);
}

/*---------------------------------------------------------------------------------------------*\
Method: SCI_RxOFF
	Disattiva la ricezione UART.
	
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static void SCI_RxOFF(void) {
	
}

/*---------------------------------------------------------------------------------------------*\
Method: MonitorRxData
	Ricezione da SCI GP
	
	IN:	  - carattere ricevuto dalla seriale in "data"
	OUT:  - SysMonitor.RxCMD = true quando ricevo Carriage Return 0x0d
\*----------------------------------------------------------------------------------------------*/
void MonitorRxData(uint8_t data)
{
	//MR19 uint32_t	pos;
	char	*pos;
	
	if (SysMonitor.state == GettingConfBuffer)
	{
		if (SysMonitor.RxCR == TRUE) {														// Hyperterminal trasmette le righe del file config senza il Line feed dopo il CR
			SysMonitor.RxCR = false;
			if (data != lf) {
				gVars.ddcmpbuff[SysMonitor.RxCount++] = lf;
			}
		}
		gVars.ddcmpbuff[SysMonitor.RxCount++] = data;
		if (data == cr) {
			SysMonitor.RxCR = TRUE;
		}
		gVars.tempRTx1[0] = data;															// ritrasmetto il chr ricevuto per essere visualizzato sul PC
		SendBuffer(&gVars.tempRTx1[0], 1U);							
		Touts.tmMon = TwoSec;																// Ricarico Timeout
/*
		gVars.ddcmpbuff[SysMonitor.RxCount++] = data;
		gVars.tempRTx1[0] = data;															// ritrasmetto il chr ricevuto per essere visualizzato sul PC
		SendBuffer(&gVars.tempRTx1[0], 1U);							
		Touts.tmMon = TwoSec;																// Ricarico Timeout
*/
        return;
	}
	
	Touts.tmMon = OneMin;																	// Ricarico Timeout
    if(data == ESC) {
    	*(Rxpnt + SysMonitor.RxCount++) = data;
    	*(Rxpnt + SysMonitor.RxCount) = NULL;												// 0x00 come ultimo chr ricevuto a chiusura del buffer
    	SysMonitor.RxCMD = true;															// Ricevuto Comando Escape
    	return;
    }
    if (SysMonitor.state == ShowCardError) {												// In "ShowCardError" non accetto altri caratteri
    	SendBackNULL();																		// Vuota se non USB_CDC
    	return;
    }
    if(data == ENTER) {
    	if (SysMonitor.RxCount == 0 && !SysMonitor.DontTxChrBack) {							// Se ricevo solo Enter NON lo ritrasmetto al PC
        	SendBackNULL();																	// Vuota se non USB_CDC
        	return;
    	} else {
    		*(Rxpnt + SysMonitor.RxCount)= NULL;											// 0x00 come ultimo chr ricevuto a chiusura del buffer
			SysMonitor.RxCMD = true;														// Ricevuto Comando ENTER
			return;
		}
    }
    if(data == Back) {
        if (SysMonitor.RxCount != 0) {
        	SysMonitor.RxCount--;															// "Back" decrementa il pointer al buffer e
        	*(Rxpnt + SysMonitor.RxCount)= NULL;											// sostituisce l'ultimo chr ricevuto con NULL
            gVars.tempRTx1[0] = data;														// Ritrasmetto il chr ricevuto e il carattere "Space"
            gVars.tempRTx1[1] = Space;														// per cancellare l'ultimo carattere digitato
            gVars.tempRTx1[2] = data;
            if (!SysMonitor.RxCMD && !SysMonitor.DontTxChrBack) {								
            	SendBuffer(&gVars.tempRTx1[0], 3U);
            }
        } else {
        	SendBackNULL();																	// Vuota se non USB_CDC
        }
        return;
    }
    if (data == Space) {
    	switch (SysMonitor.state) {
    	
    	case ShowExecProtComm:
    		SysMonitor.state = TxPauseExecMessage;
    		break;
    	case ShowExecMSTProtComm:
    		SysMonitor.state = TxPauseExecMSTMessage;
    		break;
    	case PauseShowExecProtComm:
    		SysMonitor.state = ShowExecProtComm;
    		break;
    	case PauseShowExecMSTProtComm:
    		SysMonitor.state = ShowExecMSTProtComm;
    		break;
    	case ShowMDBProtComm:
    		SysMonitor.state = TxPauseMDBMessage;
    		break;
    	case PauseShowMDBProtComm:
    		SysMonitor.state = ShowMDBProtComm;
    		break;
    	}
    }
    if(data == Canc) {
    	data = NULL;
        if (SysMonitor.RxCount != 0) SysMonitor.RxCount--;									
    }
	pos = strchr((char*)&ValidChars, data);															// Verifico se e' un char presente nella lista "ValidChars"
    if ( pos != 0 || (data >='0' && data <='9') || (data >='A' && data <='Z') || (data >='a' && data <='z') ) {
        *(Rxpnt + SysMonitor.RxCount++)= data;
        if (!SysMonitor.RxCMD && !SysMonitor.DontTxChrBack) {									// Se non e' un comando e non e' bloccata la ritrasmissione
			gVars.tempRTx1[0] = data;															// ritrasmetto il chr ricevuto per essere visualizzato sul PC
			SendBuffer(&gVars.tempRTx1[0], 1U);							
        }
    } else {
    	SendBackNULL();																			// Vuota se non USB_CDC
    }
}


/*---------------------------------------------------------------------------------------------*\
Method: TestMonitor
	Se ero in visualizzazione protocollo prima dello spegnimento, attivo subito il Monitor per 
	poter inviare i dati di inizializzazione delle periferiche MDB.
		
	IN:	  - 
	OUT:  - true se attivato il Log protocolli
\*----------------------------------------------------------------------------------------------*/
bool  TestMonitor(void)
{
	uint16_t	Temp;
	bool		result = false;

	ReadEEPI2C(RFUAddr(MonitorState), (uint8_t *)(&Temp), 2U);
	if (Temp == RecProtocolMDB || Temp == RecProtocolExec)
	{
		//SysMonitor.PC_Call = true;			*10.2019 Sara' eventualmente attivata in VMC_Main*				// Simulo ricezione carattere attivazione Monitor
		Touts.tmMon = OneMin;
		if (Temp == RecProtocolExec) {
			SysMonitor.LogProtocol = TypeExec;													// Attiva registrazione protocollo Executive
			result = true;
		} else if (Temp == RecProtocolMDB) {
			SysMonitor.LogProtocol = TypeMDB;													// Attiva registrazione protocollo MDB
			result = true;
		} else if (Temp == RecProtExecMST) {
			SysMonitor.LogProtocol = TypeMasterExec;											// Attiva registrazione protocollo Master Executive
			result = true;
		}
	}
	return result;
}

/*---------------------------------------------------------------------------------------------*\
Method: AttivaMonitor
	Funzione chiamata per attivare la modalita' Monitor.
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  AttivaMonitor(void)
{
  	if (SysMonitor.PC_Call == false)
	{
	  SysMonitor.PC_Call = true;																// Attivo Monitor
	  Touts.tmMon = OneMin;
	}
}	
	
/*---------------------------------------------------------------------------------------------*\
Method: TxCardErr
	Trasmette al PC l'errore della lettura carta se funzione abilitata dal Monitor 
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  TxCardErr(uint16_t Cerr) {
	uint8_t	charnum;
	uint16_t	asciiVal;
	
	if (SysMonitor.state == ShowCardError) {
		if (SysMonitor.MonFeatures == MonUtente) {
			if (!gOrionKeyVars.FirstAccess) return;										// La prima lettura errata non e' visualizzata in modalita' utente
		}
		charnum = 0;
		if (SysMonitor.TxCount >= 40) {
		    gVars.ddcmpbuff[charnum++] = cr;
			gVars.ddcmpbuff[charnum++] = lf;
			SysMonitor.TxCount = 0;
		}
		HexByteToAsciiInt((uint8_t)(Cerr>>8), &asciiVal, NULL);
		gVars.ddcmpbuff[charnum++] = asciiVal>>8;
		gVars.ddcmpbuff[charnum++] = asciiVal;
		HexByteToAsciiInt((uint8_t)Cerr, &asciiVal, NULL);
		gVars.ddcmpbuff[charnum++] = asciiVal>>8;
		gVars.ddcmpbuff[charnum++] = asciiVal;
	    gVars.ddcmpbuff[charnum++] = Space;
		gVars.ddcmpbuff[charnum] = NULL;
		charnum = strlen((char const*)&gVars.ddcmpbuff[0]);
		SysMonitor.TxCount += charnum;
		SendBuffer(&gVars.ddcmpbuff[0], charnum);
		SysMonitor.TxMsgInCorso = 1;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ClrFlags_PC
	Azzero le flags utilizzate per il prelievo DDCMP da PC.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  ClrFlags_PC(void) {
	
	SysMonitor.Call_DDCMP_PC = false;
	SysMonitor.Set_DDCMP_PC = false;
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_ExecVendReq
	Memorizzo richiesta di selezione dal VMC perche' possa essere trasmessa al PC. 

	IN:	  - VendReq
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_ExecVendReq(uint8_t VendReq) {
	
	if (SysMonitor.state == ShowVendReqData) {
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = VendReq;
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_VendReqMDB
	Memorizzo richiesta di selezione dal VMC MDB perche' possa essere trasmessa al PC. 

	IN:	  - price e selNumb ricevuti dal Master MDB
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_VendReqMDB(CreditCpcValue VendReq, uint16_t selnum) {
	
	if (SysMonitor.state == ShowVendReqData) {
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = VendReq>>8;
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = VendReq;
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = selnum>>8;
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = selnum;
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_LogExecTxToRR
	Memorizzo il carattere trasmesso alla RR.

	IN:	  - carattere inviato al VMC
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_LogExecTxToRR(uint8_t dato) {
	
	if (SysMonitor.state == ShowExecProtComm) {													// CMD11
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = dato;									// Chr inviato alla RR
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 's';
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.EnableLogRxExec = true;
	}		
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_EchoDDCMP_Slave
	Echo al PC del carattere di risposta al Master DDCMP

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_EchoDDCMP_Slave(uint8_t dato) {
	
	if (SysMonitor.state == ShowExecProtComm) {													// CMD11
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 's';
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = dato;									// Chr inviato al Carrier
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
	}		
}
/*---------------------------------------------------------------------------------------------*\
Method: Monitor_EchoDDCMP_Master
	Echo al PC del carattere ricevuto dal Master DDCMP

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_EchoDDCMP_Master(uint8_t RxData) {
	
	if (SysMonitor.state == ShowExecProtComm) {													// CMD11
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 'm';
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = RxData;								// Chr ricevuto dal Carrier
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_LogExecRxRR
	Memorizzo il carattere ricevuto sulla linea Executive Slave per trasmetterlo al PC.
	Il chr e' stato inviato dalla RR. 

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_LogExecRxRR(uint8_t RxData) {
	
	if (SysMonitor.EnableLogRxExec) {
		if (SysMonitor.state == ShowExecProtComm) {												// CMD11
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = RxData;							// Chr ricevuto dalla RR
			SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 'm';
			SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_LogExecTxToVMC2
	Memorizzo il carattere trasmesso al VMC2.

	IN:	  - carattere inviato al VMC
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_LogExecTxToVMC2(uint8_t dato) {
	
	if (SysMonitor.state == ShowExecMSTProtComm) {												// CMD18
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = dato;								// Chr inviato al VMC2
			SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 'm';
			SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
			SysMonitor.EnableLogRxExec = true;
	}		
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_LogExecRxVMC2
	Memorizzo il carattere ricevuto sulla linea Executive Master per trasmetterlo al PC.
	Il chr e' stato inviato dal VMC2. 

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_LogExecRxVMC2(uint8_t RxData) {
	
	if (SysMonitor.EnableLogRxExec) {
		if (SysMonitor.state == ShowExecMSTProtComm) {											// CMD18
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = RxData;							// Chr ricevuto dal VMC2
			SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 's';
			SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_LogMDBTxData
	Memorizzo il carattere trasmesso sulla linea MDB perche' possa essere trasmesso al PC.

	IN:	  - carattere inviato alle periferiche MDB
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_LogMDBTxData(uint8_t dato, uint8_t ModeBitStatus)
{
  	if (SysMonitor.state == ShowMDBProtComm)
	{
	  SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = dato;
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		if (OpMode == MDB) {
			if (ModeBitStatus) {
				SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = '{';
			} else {
				SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 'm';
			}
		} else {
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 's';
		}
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		SysMonitor.EnableLogRxMDB = true;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_LogMDBRxData
	Memorizzo il carattere ricevuto sulla linea MDB perche' possa essere trasmesso al PC. 

	IN:	  - carattere ricevuto dal VMC in gVars.devCOM4Buff[0]
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_LogMDBRxData(uint8_t RxData)
{
	if (SysMonitor.EnableLogRxMDB) {
		SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = RxData;
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
		if (OpMode == MDB) {
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 's';
		} else {
			SysMonitor.CommBuff[SysMonitor.GenericCntIN++] = 'm';
		}
		SysMonitor.GenericCntIN %= sizeof(SysMonitor.CommBuff);
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Send_DDCMP_Echo_Al_PC
	Echo al PC dei caratteri scambiati con protocollo DDCMP.

	IN:	  - SysMonitor.CommBuff
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Send_DDCMP_Echo_Al_PC(void) {
	
		uint8_t		charnum, LocalBuff[96];
		uint16_t	asciiVal;
static	bool		PrevChar_m = FALSE;
	
	
	if (SysMonitor.GenericCntOUT == SysMonitor.GenericCntIN) return;							// Tx in corso o nessun carattere da inviare

	charnum = 0;
	do {
		if ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT] == 'm') {
			if (PrevChar_m == FALSE) {
				LocalBuff[charnum++] = cr;
				LocalBuff[charnum++] = lf;
				SysMonitor.TxCount = 0;
				PrevChar_m = TRUE;
			}
			LocalBuff[charnum++] = 'm';
			SysMonitor.TxCount++;
		} else if ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT] == 's') {
			if (PrevChar_m == TRUE) {
				LocalBuff[charnum++] = cr;
				LocalBuff[charnum++] = lf;
				SysMonitor.TxCount = 0;
				PrevChar_m = FALSE;
			}
			LocalBuff[charnum++] = 's';
			SysMonitor.TxCount++;
		} else {
			HexByteToAsciiInt((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT], &asciiVal, NULL);
			LocalBuff[charnum++] = asciiVal>>8;
			LocalBuff[charnum++] = asciiVal;
		    LocalBuff[charnum++] = Space;
			SysMonitor.TxCount += 3;
			if (SysMonitor.TxCount >= 60) {														// Raggiunti i 60 chr inviati al PC inserisco un NL
				LocalBuff[charnum++] = cr;
				LocalBuff[charnum++] = lf;
				SysMonitor.TxCount = 0;
			}
		}
		SysMonitor.GenericCntOUT++;
		SysMonitor.GenericCntOUT %= sizeof(SysMonitor.CommBuff);
	} while (SysMonitor.GenericCntOUT != SysMonitor.GenericCntIN);
	
	SendBuffer(&LocalBuff[0], charnum);
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_SendProtChar_To_PC
	Echo al PC dei caratteri scambiati sulle seriali dei protocolli. 

	IN:	  - SysMonitor.CommBuff
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_SendProtChar_To_PC(void) {
	
	uint8_t	charnum, i;
	uint16_t	asciiVal, SelPrice, SelNum;
	
	if (SysMonitor.GenericCntOUT == SysMonitor.GenericCntIN) return;							// Tx in corso o nessun  carattere da inviare

	charnum = 0;
	do {

		if ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT] == '{') {
		    gVars.ddcmpbuff[charnum++] = 'M';
		    gVars.ddcmpbuff[charnum++] = Space;
		} else {
			if ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT] == 'm' || (uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT] == 's') {
			    gVars.ddcmpbuff[charnum++] = SysMonitor.CommBuff[SysMonitor.GenericCntOUT];
			    gVars.ddcmpbuff[charnum++] = Space;
				//SysMonitor.TxCount +=2;
			} else {
				if (SysMonitor.state == ShowVendReqData && OpMode == MDB) {
					// Visualizzo Price e Selnum MDB
					SelPrice = ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT] << 8) + ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT+1]);
					SelNum   = ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT+2] << 8) + ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT+3]);
					for (i=0; i<4; i++) {
						HexByteToAsciiInt((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT++], &asciiVal, NULL);
						gVars.ddcmpbuff[charnum++] = asciiVal>>8;
						gVars.ddcmpbuff[charnum++] = asciiVal;
						SysMonitor.GenericCntOUT %= sizeof(SysMonitor.CommBuff);
						if (i == 1) {
							gVars.ddcmpbuff[charnum++] = '-';
						}
					}
					gVars.ddcmpbuff[charnum++] = Space;											// Visualizzo SelPrice e SelNum in decimale
					sprintf((char*)&gVars.ddcmpbuff[charnum], "%5u", SelPrice);
					charnum +=5;
					gVars.ddcmpbuff[charnum++] = '-';
					sprintf((char*)&gVars.ddcmpbuff[charnum], "%5u", SelNum);
					charnum +=5;
					
					SysMonitor.GenericCntOUT--;													// Decrementato perche' sara' incrementato prima del while
					gVars.ddcmpbuff[charnum++] = cr;
					gVars.ddcmpbuff[charnum++] = lf;
					SysMonitor.TxCount = 0;
				} else {
					if (SysMonitor.state == ShowVendReqData && OpMode == EXECUTIVE) {
						// Visualizzo Price o Selnum MDB a seconda se Std o Price Holding
						SelPrice = ((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT]);
						HexByteToAsciiInt((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT++], &asciiVal, NULL);
						gVars.ddcmpbuff[charnum++] = asciiVal>>8;
						gVars.ddcmpbuff[charnum++] = asciiVal;
						SysMonitor.GenericCntOUT %= sizeof(SysMonitor.CommBuff);
						gVars.ddcmpbuff[charnum++] = '-';
						gVars.ddcmpbuff[charnum++] = Space;
						sprintf((char*)&gVars.ddcmpbuff[charnum], "%3u", SelPrice);					// In SelPrice c'e' il valore ricevuto, interpretato a seconda
						charnum +=3;															// dell'impostazione Std o Price Holding
						
						SysMonitor.GenericCntOUT--;												// Decrementato perche' sara' incrementato prima del while
						gVars.ddcmpbuff[charnum++] = cr;
						gVars.ddcmpbuff[charnum++] = lf;
						SysMonitor.TxCount = 0;
					} else {
						if (SysMonitor.CommBuff[SysMonitor.GenericCntOUT+1] == '{') {			// Primo byte Comando del Master MDB: inserisco NL
						    gVars.ddcmpbuff[charnum++] = cr;
							gVars.ddcmpbuff[charnum++] = lf;
							SysMonitor.TxCount = 0;
						}
						HexByteToAsciiInt((uint8_t)SysMonitor.CommBuff[SysMonitor.GenericCntOUT], &asciiVal, NULL);
						gVars.ddcmpbuff[charnum++] = asciiVal>>8;
						gVars.ddcmpbuff[charnum++] = asciiVal;
						//SysMonitor.TxCount += 2;
					}
				}
			}
		}
		SysMonitor.TxCount += 2;
		if (SysMonitor.TxCount >= 60) {															// Raggiunti i 60 chr inviati al PC inserisco un NL
		    gVars.ddcmpbuff[charnum++] = cr;
			gVars.ddcmpbuff[charnum++] = lf;
			SysMonitor.TxCount = 0;
		}
		SysMonitor.GenericCntOUT++;
		SysMonitor.GenericCntOUT %= sizeof(SysMonitor.CommBuff);
	} while (SysMonitor.GenericCntOUT != SysMonitor.GenericCntIN);
	
	SendBuffer((uint8_t*)&gVars.ddcmpbuff[0], charnum);
	SysMonitor.TxMsgInCorso = 1;
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_DeniedReason
	Memorizzo il motivo del rifiuto della selezione perche' sia inviato al PC

	IN:	  - tipo errore causa del denied
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_DeniedReason(uint16_t TipoDenied) {

	if (SysMonitor.state == ShowVendReqData) {
		SysMonitor.VendDeniedType = TipoDenied;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_DeniedPerNoIDLE
	Memorizzo rifiuto della selezione causato dal fatto che lo stato del MAIN Orion non e' IDLE.
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_DeniedPerNoIDLE(void) {

	if (SysMonitor.state == ShowVendReqData) {
		SysMonitor.VendDeniedType = VendDenNoIDLE;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_DeniedPerNoKey
	Memorizzo rifiuto della selezione causato da chiave estratta.
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_DeniedPerNoKey(void) {

	if (SysMonitor.state == ShowVendReqData) {
		SysMonitor.VendDeniedType = VendDenNoKey;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_DeniedPerSystOFF_DDCMP_AudExtFull
	Memorizzo rifiuto della selezione causato da sistema spento, o connessione DDCMP o Audit
	Estesa Full.
	
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_DeniedPerSystOFF_DDCMP_AudExtFull(void) {

	if (SysMonitor.state == ShowVendReqData) {
		SysMonitor.VendDeniedType = VendDenOFFDDCMPExtFull;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_DeniedPerNoSelVal
	Memorizzo rifiuto della selezione causato da Valore selezione non determinabile (ad es
	Price holding con NumSel out of range o relativo prezzo non abilitato).
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_DeniedPerNoSelVal(void) {

	if (SysMonitor.state == ShowVendReqData) {
		SysMonitor.VendDeniedType = VendDenNoPrice;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Monitor_SendDeniedReason_To_PC
	Trasmetto al PC il motivo del rifiuto della selezione se ne esiste uno.

	IN:	  - tipo errore causa del denied
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_SendDeniedReason_To_PC(void) {

	if (TestTxMsgInCorso() || SysMonitor.VendDeniedType == 0) return;									// Tx in corso o nessun messaggio da inviare

	switch (SysMonitor.VendDeniedType) {

		case VendDenReadError:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDenReadErrMsg);
			break;
		
		case VendDenWriteError:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDenWriteErrMsg);
			break;

		case VendDenInsufCred:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", CreditoInsuffMsg);
			break;

		case VendDenNoPrice:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDenNoPriceMsg);
			break;
			
		case VendDenPerTipoKey:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDen_TipoKey);
			break;

		case VendDenPerCash:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDen_Cash);
			break;
			
		case VendPending:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendPendingMsg);
			break;
			
		case VendDenOFFDDCMPExtFull:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDen_OFF_DDCMP_ExtAudFull);
			break;

		case VendDenNoKey:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDenNoKeyMsg);
			break;
			
		case VendDenNoIDLE:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", VendDenNoIDLEMsg);
			break;
			
		default:
			sprintf((char*)&gVars.ddcmpbuff[0], "%1s", AltreCauseMsg);

	}
	SendBuffer((uint8_t*)&gVars.ddcmpbuff[0], strlen((char*)&gVars.ddcmpbuff[0]));
	SysMonitor.TxMsgInCorso = 1;
	SysMonitor.VendDeniedType = 0;
}


/*---------------------------------------------------------------------------------------------*\
Method: AddLogDataToEEPromBufferLog
	Trasferisce il contenuto del buffer sorgente al buffer Log in EEProm e incrementa il 
	relativo contatore del numero di caratteri.
	Aggiunta la flag Pwdown_Cntrl perche' questa funzione puo' essere chiamata anche dentro
	la SaveBackupData e la chiamata alla funzione PSHCheckPowerDown innescava un nesting che 
	terminava con un reset.
	Se chiamata dalla SaveBackupData la funzione PSHCheckPowerDown non e' invocata.

	IN:	  - Address Source, Size Source data e flag per controllare power down
	OUT:  - LogBuffCnt
\*----------------------------------------------------------------------------------------------*/
uint32_t  AddLogDataToEEPromBufferLog(uint8_t *AddrDataSource, uint32_t Size, bool Pwdown_Cntrl)
{
	uint32_t	LogBuffCnt, LogBuffSize, LogBuffStartAddr, FirstByteFreeAddr, SpaceAvailable;
	
	RedLedCPU(ON);
	//	Leggo numero di caratteri presenti nel Log Buffer e inverto il valore
	//	perche' nella EEProm sono memorizzati BigEndian
	LogBuffCnt = ReadLogFileContentLen();															// In LogBuffCnt bytes presenti nel log buffer 

	//	Leggo il Log Size che e' di 3 bytes e inverto il valore
	//	perche' nella EEProm e' memorizzato BigEndian
	ReadEEPI2C(LogFileSize, (uint8_t *)(&MemoryBytes[0]), 3U);										// In LogBuffSize dimensioni del Log Buffer in EEProm
	LogBuffSize = MemoryBytes[2];																	// LSB
	LogBuffSize += (MemoryBytes[1]<<8);																// Medium
	LogBuffSize += (MemoryBytes[0]<<16);															// MSB

	// Determino address inizio Log Buffer in EEProm
	Rd_EEBuffStartAddr_And_BankSelect(BUF_LOGFILE);													// In  MemoryBytes[] StartAddress del buffer e bank della EE gia' selezionato
	LogBuffStartAddr = 0;
	LogBuffStartAddr += MemoryBytes[1];																// LSB
	LogBuffStartAddr += (MemoryBytes[0]<<8);														// MSB

	// Determino address primo byte libero nel Log Buffer in EEProm
	SpaceAvailable = (LogBuffSize - (LogBuffCnt % LogBuffSize));									// Bytes ancora disponibili prima di riempire il Log Buffer
	FirstByteFreeAddr = LogBuffStartAddr + (LogBuffCnt % LogBuffSize);								// Effettivo indirizzo del primo byte libero nel Log Buffer
	// Scrivo il buffer RAM nel Log Buffer EEProm
	if (Pwdown_Cntrl) {
		PSHProcessingData();
	}
	if (SpaceAvailable >= Size) {
		// I Size bytes ci stanno in EEProm senza sbordare da LogBuffSize
		WriteEEPI2C((uint16_t)FirstByteFreeAddr, AddrDataSource, (uint16_t)Size);					// Scrivo i Size nuovi bytes nel Log Buffer in EEProm
	} else {
		// I Size bytes NON ci stanno in EEProm senza sbordare: ne scrivo quanti
		// bastano per arrivare a LogBuffSize, il resto con rollover saranno scritti
		// da LogFileStartAddr
		WriteEEPI2C((uint16_t)FirstByteFreeAddr, AddrDataSource, (uint16_t)SpaceAvailable);			// Scrivo tanti nuovi bytes fino alla fine del buffer
		AddrDataSource += SpaceAvailable;
		WriteEEPI2C((uint16_t)LogBuffStartAddr, AddrDataSource, (uint16_t)(Size-SpaceAvailable));	// Scrivo i rimanenti nuovi bytes nel Log Buffer da zero
	}
	// Aggiorno counter bytes nel Log Buffer
	BankSelect(0x00);																				// Default: Address per bank0 della EEProm 1 da 1024kbit
	LogBuffCnt += Size;
	MemoryBytes[3] = LogBuffCnt;
	MemoryBytes[2] = LogBuffCnt>>8;
	MemoryBytes[1] = LogBuffCnt>>16;
	MemoryBytes[0] = LogBuffCnt>>24;
	WriteEEPI2C(LogFileContentLen, (uint8_t *)(&MemoryBytes[0]), 4U);									// Scrivo nuovo counter aggiornato
	if (Pwdown_Cntrl) {
		PSHCheckPowerDown();
	}
	RedLedCPU(OFF);
	return LogBuffCnt;
}

/*---------------------------------------------------------------------------------------------*\
Method: InitLogBufferPointers
	Inizializza tutti i valori in EEProm per poter utilizzare il Log buffer. 
	Azzera contatore byte presenti nel buffer, predispone corretta lunghezza del buffer Log e,
	opzionalmente, azzera il contenuto del buffer  Log. 

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  InitLogBufferPointers(bool MemClr)
{
	GreenLedCPU(OFF);
	RedLedCPU(ON);
	FillEEPI2C((uint16_t)LogFileContentLen, 0x00, 4U);											// Azzero counter dati di Log
	
	MemoryBytes[0] = (uint8_t) LOG_FILE_BUF_LEN;												// LSB
	MemoryBytes[1] = (uint8_t)(LOG_FILE_BUF_LEN >> 8);
	MemoryBytes[2] = 0;																			// MSB
	WriteEEPI2C(LogFileSize, (uint8_t *)(&MemoryBytes), 3U);									// Log Buffer size
	if (MemClr)
	{
		FillEE_LogBuffer(0x00);																	// Azzero tutto il log buffer in EE
	}
	RedLedCPU(OFF);
}

/*---------------------------------------------------------------------------------------------*\
Method: GetStartLog
	Memo data/ora inizio log nella struttura sDDCMPVars.LogStartTime.
	
	IN:	  - 
	OUT:  - sDDCMPVars.LogStartTime con data/ora attuale
\*----------------------------------------------------------------------------------------------*/
void  GetStartLog(void) {
/*MR19	
	LeggiOrologio();																		// Data/ora attuale nella Struttura DataOra 
	LogStartTime[0] = (uint8_t) (DataOra.Year >> 8);
	LogStartTime[1] = (uint8_t)DataOra.Year;
	LogStartTime[2] = (uint8_t)DataOra.Month;
	LogStartTime[3] = (uint8_t)DataOra.Day;
	LogStartTime[4] = (uint8_t)DataOra.Hour;
	LogStartTime[5] = (uint8_t)DataOra.Minute;
	LogStartTime[6] = (uint8_t)DataOra.Second;
*/
}

/*--------------------------------------------------------------------------------------------------------*\
Method: TranferLogToEE
	Trasferisce tutto quanto c'e' nel Log buffer RAM "gVars.tempRTx1" in EEprom.
	Il meccanismo e' studiato per ridurre al minimo il tempo che occorre per trasferire i dati
	in EE, preparando buffer da PageSize byte.
	Anche la chiamata a questa funzione e' fatta quando PageSize bytes sono presenti in RAM.
	Il record dati, per l'opzione zero (chr DDCMP e TimeStamp) e' composto da 8 bytes cosi' da
	riempire sempre una pagina EE esatta.
	Gli indirizzi in EE sono tenuti pari alle pagine proprio per non dover scrivere a cavallo
	delle pagine.
	
	IN:	  - buffer RAM gVars.tempRTx1, flag LastTransfer per indicare l'ultimo blocco dati,
			e flag Pwdown_Cntrl per controllare o meno il Power Down nella "AddLogDataToEEPromBufferLog"
	OUT:  - 
\*---------------------------------------------------------------------------------------------------------*/
void  TranferLogToEE(bool LastTransfer, bool Pwdown_Cntrl)
{
	
/*

#define	OnlyProtChr		1
#define	TimingLen		5
#define	FlagsLen		5
	
	uint8_t		RAMLogBuff[192];																// Riservo spazio temporaneo in RAM per preparare buffer di scrittura
	uint16_t	pntTS, pntdata, RAMLogBuffLen, i, r;
	uint32_t  	ByteCnt;

	if (gVars.pnt_data == 0) return;															// Entra anche dopo il reset quando si inizializza il DDCMP
	if (SystOpt4 & DiscardFirstRx)
	{																							// Scartare il primo chr ricevuto dall'IrDA come spurio
		gVars.pnt_data = 0;
		gVars.pnt_timing = OffsetDatiLog;
		SystOpt4 &= ~(DiscardFirstRx);
		return;
	}
	Transfer_EE = false;
	memset(RAMLogBuff, 0, 192);																	// Debug: per vedere bene il buffer risultante
	// Trasferisco in EE il log buffer gVars.tempRTx1. Ogni record di dati e' cosi' composto:
	// 1 byte  T/R
	// 2 bytes Time Stamp (gVars.gKernelFreeCounter)
	// 2 bytes Time Stamp Frazione
	// 1 byte  Dato DDCMP
	// 2 bytes LF come sincronismo per vedere la fine di ogni record
	// Totale 8 bytes: in una pagina da 128 bytes ci stanno 16 record
	// DDCMP con relativo TimeStamp

	pntdata = (PageSize * BancoLogBuff);														// Pointer ai chr scambiati dal protocollo
	pntTS = OffsetDatiLog + (TimingLen * pntdata);												// Pointer ai dati di timing
	if (BancoLogBuff) {
		BancoLogBuff = 0;
	} else {
		BancoLogBuff = 1;
	}
	if (LastTransfer) {																			// Trasferisco in EE gli ultimi dati rimasti nel log buffer in RAM
		RAMLogBuffLen = (gVars.pnt_data % PageSize);											// La TranferLogToEE puo' essere chiamata da piu' parti del fw, quindi alla fine
		gVars.pnt_data = 0;																		// devo azzerare il contatore dei dati per non far registrare piu' volte gli stessi
	} else {
		RAMLogBuffLen = PageSize;																// Trasferisco in EE un intero banco RAM
	}
	if ( (SystOpt4 & MaskTipoLog) == LogTipo0 ) {												// Log Tipo 0: solo Protocollo DDCMP
		pntTS = OffsetDatiLog + (OnlyProtChr * pntdata);										// Pointer ai dati di timing che in questo caso sono solo in Tx/Rx/Err
	} else {
		if ( (SystOpt4 & MaskTipoLog) == LogTipo1 ) {											// Log Tipo 1: Protocollo DDCMP + Time Stamp
			pntTS = OffsetDatiLog + (TimingLen * pntdata);										// Pointer ai dati di timing
		} else {
			pntTS = OffsetDatiLog + (FlagsLen * pntdata);										// Log Tipo 2: Protocollo DDCMP + Flags sDDCMPVars	
		}																						// Pointer ai dati di timing che in questo caso solo le flags
	}
	
	r = 0;																						// Counter destination buffer RAMLogBuff
	for (i=0; i < RAMLogBuffLen; i++) {															// Trasferisco RAMLogBuffLen record alla EE
		RAMLogBuff[r++] = gVars.tempRTx1[pntTS];												// Tipo char: T=Tx R=Rx E=Err				(1 chr)
		switch (SystOpt4 & MaskTipoLog) {
		
		case (LogTipo0):																		// Log Tipo 0: Log solo Protocollo DDCMP
			pntTS += OnlyProtChr;																// Pointer al successivo dato del tipo di chr registrato nel log
			break;

		case (LogTipo1):																		// Log Tipo 1: Protocollo DDCMP + Time Stamp
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+1];											// Time Stamp 2 bytes
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+2];
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+3];											// Time Stamp frazione 2 bytes
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+4];
			pntTS += TimingLen;																	// Pointer al successivo dato di timing
			break;

		case (LogTipo2):																		// Log Tipo 2: Protocollo DDCMP + Flags Varie DDCMP
			if (RAMLogBuff[r-1] == 'T') {
				RAMLogBuff[r-1] = 't';
			} else {
				if (RAMLogBuff[r-1] == 'R') {
					RAMLogBuff[r-1] = 'r';
				}
			}
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+1];											// Flag sDDCMPVars.state 	1 byte
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+2];											// Flag sDDCMPVars.phase 	1 byte
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+3];											// Flag sDDCMPVars.rx.count	1 byte
			RAMLogBuff[r++] = gVars.tempRTx1[pntTS+4];											// Flag sDTSVars.state		1 byte
			pntTS += FlagsLen;																	// Pointer al successivo dato di timing
			break;
			
		}
		RAMLogBuff[r++] = gVars.tempRTx1[pntdata++];											// Dato del protocollo DDCMP 1 byte
		RAMLogBuff[r++] = cr;																	// NL 2 bytes
		RAMLogBuff[r++] = lf;
		if (r >= PageSize) {																	// Quando RAMLogBuff = PageSize la copio in EE
			WDT_Clear(gVars.devWDT);  															// Trigger WDT
			AddLogDataToEEPromBufferLog((uint8_t *)&RAMLogBuff, r, Pwdown_Cntrl);					// Scrivo buffer log in eeprom
			r = 0;
			memset(RAMLogBuff, 0, 192);															// Debug: per vedere bene il buffer risultante
		}
	}

	//	Finita scansione buffer gVars.tempRTx1: se ci sono altri dati (r>0) li
	//  memorizzo in EE
	if (r) {
		AddLogDataToEEPromBufferLog((uint8_t *)&RAMLogBuff, r, Pwdown_Cntrl);						// Scrivo buffer log in eeprom
	}
	//	LastTransfer e' set quando ho terminato la regitrazione: aggiungo quindi in fondo
	//	al file data/ora inizio registrazione.
	if (LastTransfer) {
		ByteCnt = AddLogDataToEEPromBufferLog((uint8_t *)&LogStartTime, 6U, Pwdown_Cntrl);
		r = (uint8_t)(ByteCnt % PageSize);
		if (r) {																				// Riempio gli eventuali bytes non usati fino a riempire la pagina EEprom
			memset(RAMLogBuff, '$', PageSize);													// Preparo il buffer per riempire di '$' i bytes rimanenti  per completare
			r = PageSize - r;																	// la pagina in EEProm
			if (r >= 4) {
				RAMLogBuff[0] = cr;																// Primi due chr sono un LF
				RAMLogBuff[1] = lf;
				RAMLogBuff[r-2] = cr;															// Ultimi due chr sono un LF
				RAMLogBuff[r-1] = lf;
			} else {
				if (r >= 2) {
					RAMLogBuff[r-2] = cr;
					RAMLogBuff[r-1] = lf;
				}
			}
			AddLogDataToEEPromBufferLog((uint8_t *)&RAMLogBuff, r, Pwdown_Cntrl);					// Scrivo buffer log in eeprom
		}
		//MR19 MemoLogStartTime = false;
		gVars.pnt_timing = OffsetDatiLog;
	}
*/
	
}

/*---------------------------------------------------------------------------------------------*\
Method: Rd_EEBuffStartAddr_And_BankSelect
	Leggo l'address di inizio di un buffer in EEprom e seleziono il banco nel quale questo
	buffer e' locato. I buffers sono:
	1) RxConfigBuff
	2) TxConfigBuff
	3) AuditFileStartAddr
	4) LogFileBuffAddr
	
	IN:	  - 
	OUT:  - MemoryBytes[0] con Start Address e Bank del buffer richiesto gia' attivato
\*----------------------------------------------------------------------------------------------*/
void  Rd_EEBuffStartAddr_And_BankSelect(uint8_t TipoBuff)
{
	switch (TipoBuff)
	{																							// Determino di quale buffer si vuole conoscere lo StartAddress
		case BUF_RXCONFIG:
			MemoryBytes[0] = (uint8_t)(RxConfigBuff >> 8);										// MSB indirizzo di inizio del Buffer RxConfig
			MemoryBytes[1] = (uint8_t)RxConfigBuff;												// LSB
			MemoryBytes[2] = RxConfigBuffBank;
			break;
		
		case BUF_TXCONFIG:
			MemoryBytes[0] = (uint8_t)(TxConfigBuff >> 8);										// MSB indirizzo di inizio del Buffer TxConfig
			MemoryBytes[1] = (uint8_t)TxConfigBuff;												// LSB
			MemoryBytes[2] = TxConfigBuffBank;
			break;

		case BUF_LOGFILE:
			MemoryBytes[0] = (uint8_t)(LogFileBuffAddr >> 8);									// MSB indirizzo di inizio del Buffer LogFile
			MemoryBytes[1] = (uint8_t)LogFileBuffAddr;											// LSB
			MemoryBytes[2] = LogFileBuffAddrBank;
			break;
		
		case BUF_GENERIC_FILE:
			MemoryBytes[0] = (uint8_t)(LogFileBuffAddr >> 8);									// MSB indirizzo di inizio del Buffer Generic File Log Buffer
			MemoryBytes[1] = (uint8_t)LogFileBuffAddr;											// LSB
			MemoryBytes[2] = LogFileBuffAddrBank;
			break;

#if AWS_CONFIG
		case BUF_AWS_SECURE_CERTIF:
			MemoryBytes[0] = (uint8_t)(RxAWSSecureCertifBuff >> 8);								// MSB indirizzo di inizio del Buffer RxAWSSecureCertifBuff
			MemoryBytes[1] = (uint8_t)RxAWSSecureCertifBuff;									// LSB
			MemoryBytes[2] = RxConfigBuffBank;
			break;
		
		case BUF_AWS_PRIVATE_KEY:
			MemoryBytes[0] = (uint8_t)(RxAWSPrivateKeyfBuff >> 8);								// MSB indirizzo di inizio del Buffer RxAWSPrivateKeyfBuff
			MemoryBytes[1] = (uint8_t)RxAWSPrivateKeyfBuff;										// LSB
			MemoryBytes[2] = RxConfigBuffBank;
			break;
		
		case BUF_AWS_DEVICE:
			MemoryBytes[0] = (uint8_t)(RxAWS_arnDeviceObject >> 8);								// MSB indirizzo di inizio del Buffer RxAWS_arnDeviceObject
			MemoryBytes[1] = (uint8_t)RxAWS_arnDeviceObject;									// LSB
			MemoryBytes[2] = RxConfigBuffBank;
			break;

		case BUF_AWS_ACCOUNT_SERVER_ADDRESS:
			MemoryBytes[0] = (uint8_t)(RxAWS_AccountServerAddr >> 8);							// MSB indirizzo di inizio del Buffer RxAWS_AccountServerAddr
			MemoryBytes[1] = (uint8_t)RxAWS_AccountServerAddr;									// LSB
			MemoryBytes[2] = RxConfigBuffBank;
			break;
#endif
			
	}
	MemoryBytes[2] &= 0x07;																		// Nei 3 bit ci sono: Bank-A1-A0 se Microchip, A1-A0-Bank se Atmel
	if (Tipo_EE == Atmel)
	{
		//----  ATMEL  -----
		BankSelect(MemoryBytes[2]);																// La BankSelect determina Banco e memoria EEPROM alla quale accedere
	}
	else
	{
		//----  MICROCHIP  -----
		switch (MemoryBytes[2])
		{
			case 00:
				BankSelect(0x00);																// Address per bank0 della EEProm 1 da 1024kbit
				break;
			case 04:
				BankSelect(0x01);																// Address per bank1 della EEProm 1 da 1024kbit
				break;
			case 01:
				BankSelect(0x02);																// Address per bank0 della EEProm 2 da 1024kbit
				break;
			case 05:
				BankSelect(0x03);																// Address per bank1 della EEProm 2 da 1024kbit
				break;
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: SendMessageToPC
	Invia il messaggio passato come parametro al PC.
	Il messaggio viene copiato in gVars.ddcmpbuff e inviato tramite SCI.
	
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  SendMessageToPC(const char * Mess) {
	
	uint8_t	MessLen;

	sprintf((char*)&gVars.ddcmpbuff[0], "%1s", &Mess[0]);
	MessLen = strlen((char*)&gVars.ddcmpbuff[0]);
	SendBuffer(&gVars.ddcmpbuff[0], MessLen);
	SysMonitor.TxMsgInCorso = 1;
}

/*---------------------------------------------------------------------------------------------*\
Method: ReadLogFileContentLen
	Leggo il contatore dei bytes presenti nel LOG buffer.
	
	IN:	  - 
	OUT:  - LogFileContentLen
\*----------------------------------------------------------------------------------------------*/
uint32_t	ReadLogFileContentLen(void)
{
	uint32_t TempCnt;
	
	ReadEEPI2C(LogFileContentLen, (uint8_t *)(&MemoryBytes[0]), 4U);							// In LogBuffCnt bytes presenti nel log buffer 
	TempCnt = MemoryBytes[3];																	// LSB
	TempCnt += (MemoryBytes[2]<<8);
	TempCnt += (MemoryBytes[1]<<16);
	TempCnt += (MemoryBytes[0]<<24);															// MSB
	return TempCnt;
}


/*---------------------------------------------------------------------------------------------*\
Method: IsMonitorActive
	Controlla se il monitor e' in funzione.
	
	IN:	  - 
	OUT:  - true se monitor attivo, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool  IsMonitorActive(void)
{
	if (SysMonitor.state == WaitPC_Call) {
		return false;
	} else {
		return true;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: IsMonitorReceivingFile
	Controlla se il monitor e' in fase di ricezione files.
	
	IN:	  - 
	OUT:  - true se monitor in ricezione files, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool  IsMonitorReceivingFiles(void)
{
	return ((SysMonitor.state == GettingConfBuffer)? true : false);
}

/*---------------------------------------------------------------------------------------------*\
Method: TestTxMsgInCorso
	Controlla se il monitor e' in attesa della fine invio messaggio.
	
	IN:	  - 
	OUT:  - TRUE durante l'invio messaggio al PC, FALSE se invio msg completato
\*----------------------------------------------------------------------------------------------*/
static bool TestTxMsgInCorso(void) {
	
#if USB_CDC_PC	
	SysMonitor.TxMsgInCorso = FALSE;
#endif
	return (SysMonitor.TxMsgInCorso == TRUE ? TRUE : FALSE);
}

/*---------------------------------------------------------------------------------------------*\
Method: SendBuffer
	Controlla se il buffer e' stato inviato dall'USB senza errori.
	In caso negativo rimane in questa funzione fino ad una completa trasmissione.
	
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void SendBuffer(uint8_t* tx_buf, uint16_t len)
{
	uint8_t		devErr;
	
#if USB_CDC_PC
	
	do 
	{
		devErr = SCI_SendBlock(tx_buf, len);
	} while(devErr != USBD_OK);

#else
	gVars.devErr = SCI_SendBlock(tx_buf, len);
#endif

}

/*---------------------------------------------------------------------------------------------*\
Method: SendBackNULL
	Invia il chr NULL (0x0) quando non deve rispondere nulla al Terminal sul PC.
	Ad oggi (Mar 2017) non so perche' l'USB non funziona piu' se dopo aver ricevuto qualcosa 
	dal PC non si trasmette nulla: succede sia con HyperTerminal che con RealTerm.
	Per ovviare, al momento, invio NULL al PC.
	
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static void SendBackNULL(void) {

#if USB_CDC_PC
	gVars.ddcmpbuff[0] = 0;
	SendBuffer(&gVars.ddcmpbuff[0], 1U);
#endif
}

#if USB_CDC_PC
/*---------------------------------------------------------------------------------------------*\
Method: Monitor_CDC_Receive
	Memorizzo i caratteri ricevuti dal CDC USB nel buffer di ricezione Monitor.

	IN:	  - caratteri ricevuti dal PC
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Monitor_CDC_Receive(uint8_t* Buf, uint32_t *Len)
{
		uint32_t	NumChar, i;
  
	if (SysMonitor.state == GettingConfBuffer)
	{
		NumChar = *Len;
		if (SecondBuff == false)
		{
			memcpy(&gVars.tempRTx1[NumCharIN_1], Buf, NumChar);
			NumCharIN_1 += NumChar;
			if (NumCharIN_1 > USB_RX_BUFF_LIMIT)
			{
				SecondBuff = true;
				ChrResidui = true;
			}
			//StoreCnt_1++;		// DEBUG
		}
		else
		{
			memcpy(&gVars.tempRTx1[NumCharIN_2], Buf, NumChar);
			NumCharIN_2 += NumChar;
			if (NumCharIN_2 > USB_RX_BUFF_LIMIT)
			{
				SecondBuff = false;
				ChrResidui = true;
			}
			//StoreCnt_2++;		// DEBUG
		}
	}
	else
	{
		NumChar = *Len;
		i = 0;
		while (NumChar > 0)
		{
			usb_char[usb_char_cnt_IN++] = *(Buf+i);
			i++;
			usb_char_cnt_IN %= USB_RX_BUFF_SIZE;
			NumChar--;
		}
	}
	
	
}
#endif