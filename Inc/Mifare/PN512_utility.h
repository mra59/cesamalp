/****************************************************************************************
File:
    PN512_utility.h

Description:
    Include file con definizioni e strutture per le funzioni generali PN512 non legate
	alla Mifare.

History:
    Date       Aut  Note
    Giu 2019	MR   

 *****************************************************************************************/

#ifndef __PN512_utility__H
#define __PN512_utility__H


void		Accendi_LedGiallo(void);
void 		lamp_led_rosso(void);
void 		lamp_led_verde(void);
void 		Azzera_Lamp(void);

phcsBfl_Status_t 	PN512_mif_init(uint32_t speed, uint8_t mask);
phcsBfl_Status_t 	PN512_set_led (uint8_t pin, uint8_t mask);


/*--------------------------------------------------------------------------------------*\
Types definition
\*--------------------------------------------------------------------------------------*/

/*
// ===================================
// =======   PN512   =================
// ===================================
//  Pin D1, D2, D3 del PN512 usati per accendere i led
#define led_bianco		0x02
#define led_verde		0x04
#define led_rosso		0x08
#define led_giallo		(led_verde | led_rosso)
#define	Out_D1			0x02
#define	Out_D2			0x04
#define	Out_D3			0x08
#define DDRPN 			Out_D1|Out_D2|Out_D3
*/


#endif	// End  __PN512_utility__H



