/****************************************************************************************
File:
    OrionTimers.c

Description:
    File con funzioni di uso generale per la gestione dei timers.
    

History:
    Date       Aut  Note
    Feb 2013	MR   

*****************************************************************************************/

#include "stm32l4xx_hal.h"

#include "ORION.H"
#include "OrionTimers.h"


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------------------*\
Method:	KernelTmr32Start
	Starts a 32 bits local timer

Parameters:
	t		->	address of the local timer to start
	timeout	->	timeout
\*--------------------------------------------------------------------------------------*/
void KernelTmr32Start(Tmr32Local* t, uint32_t timeout)
{
	t->tmrEnd = gVars.gKernelFreeCounter + timeout;
}

/*--------------------------------------------------------------------------------------*\
Method:	KernelTmr32Add
	Add the specified timeout to current timer timeout

Parameters:
	t		->	address of the local timer to start
	timeout	->	timeout
\*--------------------------------------------------------------------------------------*/
void KernelTmr32Add(Tmr32Local* t, uint32_t timeout)
{
	t->tmrEnd += timeout;
}

/*--------------------------------------------------------------------------------------*\
Method:	KernelTmr32Timeout
	Confronta il gVars.gKernelFreeCounter con timer passato come parametro: se maggiore
	timeout scaduto.
	Per evitare che vicino al roll-over del timer si abbia il risultato di timeout scaduto
	anche se non e' vero, si controlla il MSB di entrambi i timer, i quali devono essere
	uguali perche' la comparazione avvenga.
	Quando i MSB sono diversi, il timout non e' sicuramente scaduto.
	Le due variabili locali servono ad velocizzare la funzione perche' l'operazione di
	copia del contenuto dei timer avviene una volta sola all'entrata: inoltre il uP
	utilizza due suopi registri come variabili e l'esecuzione e' piu' veloce.
	Inoltre la FreeRunningCounter rende ininfluente l'eventuale incremento in IRQ del 
	gKernelFreeCounter tra una lettura e la successiva, sempre all'interno della funzione.
	Presupposto dell'uso del MSB e' che l'offset da sommare al gKernelFreeCounter sia
	minore di 24 giorni, 20 ore, 31 minuti e 23 secondi circa, che corrisponde a 2^31.
	Esempio: gKernelFreeCounter = 0xFFFFFF00 e offset = 0x1F4 (500 msec).
	Risultato Timeout = 0xF4, quindi MSB = 0 mentre MSB del gKernelFreeCounter = 1.
	Risulta evidente il roll-over che darebbe il risultato sbagliato se comparassi i timer.
	Attendo allora che anche il MSB del gKernelFreeCounter sia zero per ricominciare le
	comparazioni: fino a quel momento il timeout non e' sicuramente scaduto.

	--------------------------------------------------------------------------------------------
	04.03.2021	Scoperto un grosso errore di implementazione: superato il valore 0x8000 0000
	da parte del FreeRunningCounter, tutti i test con valori dei timers < di 0x8000 0000
	falliscono.
	Non c'e' pero' soluzione alla comparazione di due valori a cavallo del roll-over del 
	FreeRunningCounter: non si pio' sapere se il timer di confronto e' piu' alto perche'
	il tempo non e' ancora trascorso o e' gia' scaduto da tanto tempo prima.
	Esempio:
	FreeRunningCounter = 0xffff ff00
	Timer1 impostato quando FreeRunningCounter era 0x0001 3f40
	Timer2 impostato ora con offset di 30 secondi: (0xffff ff00 + 30000d) = 0x0000 7340.
	La comparazione di FreeRunningCounter dopo il roll-over (supponiamo 0x0000 00fe) da lo
	stesso risultato di tempo non ancora trascorso, sia per il Timer1 che per il Timer2.
	
	Siccome il FreeRunningCounter e' incrementato ogni 1 msec, il roll-over avviene ogni
	49 giorni e 17 ore: e' probabile che non ci sia nessun timer caricato 49 giorni prima
	e ancora da controllare, pero' concettualmente non si pu� confrontare due timer quando
	uno di essi passa da un valore alto ad un valore basso (roll-over).
	
	Tale situazione poteva avvenire anche con altri timers impostati appena prima del valore di
	0x8000 0000 del FreeRunningCounter: questo potrebbe spiegare le segnalazioni di blocco del
	funzionamento del PK3 (tutte le versioni) che non sono mai riuscito a spiegarmi.
	Il difetto avviene quindi dopo circa 24 giorni e 20 ore di accensione ininterrotta.
	
	Modificata questa funzione per comparare il FreeRunningCounter con il timer passato come
	parametro ma senza controlli di roll-over.
	Dovra' essere implementata una funzione nel programma per resettare il sistema quando il
	FreeRunningCounter sta per raggiungere il suo valore massimo.
	--------------------------------------------------------------------------------------------
	
   IN:  - address struttura timeout da comparare con FreeRunningCounter
  OUT:  - true se timeout scaduto, altrimenti false

\*--------------------------------------------------------------------------------------*/
bool KernelTmr32Timeout(Tmr32Local* t)
{
	uint32_t	FreeRunningCounter, Timeout;
	
	FreeRunningCounter = gVars.gKernelFreeCounter;
	Timeout = t->tmrEnd;
	return ((FreeRunningCounter >= Timeout)? true:false);
}



