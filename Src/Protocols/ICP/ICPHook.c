/*--------------------------------------------------------------------------------------*\
ICPHook.c
\*--------------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/

#include <string.h>


#include "ORION.H"
//#include "icp.h"
#include "DevSerial.h"
#include "ICPHook.h"
#include "ICPProt.h"
#include "icpVMCProt.h"
#include "VMC_CPU_HW.h"
#include "General_utility.h"
#include "icpBILL.h"
#include "icpCHG.h"
#include "icpVMC.h"
#include "ICPBILLHook.h"
#include "ICPCHGHook.h"
#include "ICPUSDHook.h"

extern	char		MachineModelNumber[20];										// ID102

/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

uint8_t	TxModeBitVal;
uint8_t	MDB_RxModeBitVal;

// =========================================================
// --------------------   MDB  SLAVE  ----------------------
// =========================================================

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_SetSlaveLink
	In Master Mode called when ICP peripherals becomes [un]available.

Parameters:
	slaveLinkMask	->	mask of available slaves
\*--------------------------------------------------------------------------------------*/
void ICPHook_SetSlaveLink(ET_SLAVELINK slaveLinkMask) {}


/*--------------------------------------------------------------------------------------*\
Method: ICPHookCommOpen

Parameters:

\*--------------------------------------------------------------------------------------*/
void ICPHookCommOpen(void)
{
	if (icpMasterMode) {
		Master_MDB_Comm_Init();
	} else {
		MDB_Slave_COMM_Init();
	}
} 


/*
void ICPHookTxBegin(void) {} 
void ICPHookTxEnd(void) {}
void ICPHookRxBegin(void) {}
void ICPHookRxEnd(void) {}
*/


/*---------------------------------------------------------------------------------------------*\
Method: ICPHookPutChar

	IN:	  - byte da inviare
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void ICPHookPutChar(uint8_t ch)
{
	if (icpMasterMode) {
		Master_MDB_TxChar(ch, TxModeBitVal);
	} else {
	  	MDB_Slave_TxChar(ch, TxModeBitVal);
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPHookModeBitSet

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void ICPHookModeBitSet(void)
{
	TxModeBitVal = 0xff;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPHookModeBitClear

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void ICPHookModeBitClear(void)
{
	TxModeBitVal = 0;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPHookModeBitTest

	IN:	  - 
	OUT:  - true se ModeBit Set, altrimenti falase
\*----------------------------------------------------------------------------------------------*/
bool ICPHookModeBitTest(void) {
	
	return ((MDB_RxModeBitVal & 0x01)? true : false);	
}


void ICPHook_GetManufactCode(uint8_t *ptData, uint8_t dataLen) {
	strncpy((char*) (ptData), "MHD", dataLen);
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_GetSerialNumber
	Get product serial number
Parameters:
	ptData	<-	serial number
	dataLen	->	data len
\*--------------------------------------------------------------------------------------*/
void ICPHook_GetSerialNumber(uint8_t *ptData, uint8_t dataLen) {
//memset(ptData, '0', dataLen);
	uint32toa0((char*)(ptData), CodiceMacchina, dataLen);
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_GetModelNumber
	Get product model number
Parameters:
	ptData	<-	model number
	dataLen	->	data len
\*--------------------------------------------------------------------------------------*/
void ICPHook_GetModelNumber(uint8_t *ptData, uint8_t dataLen) {
	memset(ptData, ' ', dataLen);
	//if(dataLen>sizeof(kProductAuditIdString)-1) dataLen= sizeof(kProductAuditIdString)-1;
	//strncpy((char *) (ptData), kProductAuditIdString, dataLen);
	if(dataLen>sizeof(MachineModelNumber)) dataLen= sizeof(MachineModelNumber);
	strncpy((char *) (ptData), &MachineModelNumber[0], dataLen);
	
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_GetSWVersion
	Return software version
\*--------------------------------------------------------------------------------------*/
uint16_t ICPHook_GetSWVersion(void) {

  	uint8_t 	vh, vl;
	uint16_t 	sw;
	
	vh = kORIONVersion / 10000;
	vl = (kORIONVersion / 100) % 100;
	sw = BinToBcd(vl);
	sw = (sw << 8) + BinToBcd(vh);
	return sw;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_SetLink
	ICP master available/unavailable indication

Parameters:
	link	->	true if link available
\*--------------------------------------------------------------------------------------*/
void ICPHook_SetLink(bool link) {
	
	//KeyEnabled(link);

	// 15.11.13 MR Aggiunto per disattivare/riattivare cashless in caso di nolink
	if(link) {
		Clear_fVMC_NoLink;
	}
	else {
		Set_fVMC_NoLink;							
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPVMCHook_GetSlaveMode
	Determina se il protocollo ICP e' master o slave.
Returns:
	true if in slave mode
\*--------------------------------------------------------------------------------------*/
bool ICPVMCHook_GetSlaveMode(void) {
	
	return (gVMC_ConfVars.ProtMDBSLV ? true : false);
}

/*--------------------------------------------------------------------------------------*\
Method:	Gestione_MDB
	Chiamata nel Main per il POLL delle periferiche e durante il ciclo selezione per 
	mantenere il collegamento con le periferiche.
	
	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
void  Gestione_MDB(void) {
	
	//if ((gVMC_ConfVars.ProtMDB == true) && (gVMC_ConfVars.ProtMDBSLV == false)) {
	if (gVMC_ConfVars.ProtMDB) {
		if (icpMasterMode) {
			Bill_Task();
			Chg_Task();
			if (gVMC_ConfVars.CPC_MDB == true) {
				ICPCPCHookEnDis();
			}
			USD_Task_MasterMode();
			//Sel24_task();
		} else {
			// ---  DA as Slave  USD  ---  
			//USD_Task_SlaveMode();																// 09.17 Per ora non c'e' nulla da fare nella "USD_Task_SlaveMode"
		}
		ICPTask();
	}
}

//-----------------------------------------------------------------------------------------------------------------
#if kICPSlaveCPCSupport

typedef struct {
	bit8 fSync :1;
	bit8 fSyncReq :1;
	RTCDateTime dateTime;
} tDTSync;
static tDTSync sDTSync;

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_DateTimeSyncSet
	Slave requests date&time synchronization to VMC
\*--------------------------------------------------------------------------------------*/
void ICPHook_DateTimeSyncSet(void) {
	if(sDTSync.fSync) return;
	sDTSync.fSyncReq = true;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_DateTimeSyncGet
	Check Slave date&time synchronization request
\*--------------------------------------------------------------------------------------*/
bool ICPHook_DateTimeSyncGet(void) {
	if(sDTSync.fSyncReq) {
		sDTSync.fSyncReq = false;
		return true;
	}
	return false;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_DateTimeSync
	VMC date&time synchronization
\*--------------------------------------------------------------------------------------*/
void ICPHook_DateTimeSync(void) {
	if(sDTSync.fSync) {
		sDTSync.fSync = false;
		//MR19 RTCSet(&sDTSync.dateTime);
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPHook_DateTimeSet
	VMC requests date&time synchronization
\*--------------------------------------------------------------------------------------*/
void ICPHook_DateTimeSet(uint8_t *dt) {
	if(!dt) {
		sDTSync.fSync = false;
		sDTSync.fSyncReq = false;
		return;
	}
	sDTSync.fSyncReq = false;
	sDTSync.fSync = true;
  sDTSync.dateTime.year = BcdToBin(dt[0]);
  sDTSync.dateTime.month = BcdToBin(dt[1]);
  sDTSync.dateTime.day = BcdToBin(dt[2]);
  sDTSync.dateTime.hours = BcdToBin(dt[3]);
  sDTSync.dateTime.minutes = BcdToBin(dt[4]);
  sDTSync.dateTime.seconds = BcdToBin(dt[5]);
  sDTSync.dateTime.dayOfWeek = BcdToBin(dt[6]);
}
#endif	//kICPSlaveCPCSupport
//27/03/2007:  [RM33/2007] date&time synchronization (Vmc Rhea)
