/****************************************************************************************
File:
    VMC_Testing.h

Description:
    Include file con definizioni e strutture per il Collaudo della CPU PK4.

History:
    Date       Aut  Note
    Set 2022	MR   

 *****************************************************************************************/

#ifndef __VMC_Testing__H
#define __VMC_Testing__H


void 	Testing(void);
void 	Collaudo_Part_1(void);
static void VisErrCollaudo(uint8_t ErrorNum);


/*
#define	NUM_BYTES_TO_CHECK		32
#define	NUM_MIN_ZONE_FF			2
*/
#define	FREQ_1_LED				Milli200
#define	UART_FREQ_1				Milli100


#endif
