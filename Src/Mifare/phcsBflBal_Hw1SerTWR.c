/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflBalHw1SerLin.c
 *
 * Project: Object Oriented Library Framework phcsBfl_Bal Component.
 *
 *  Source: phcsBflBalHw1SerLin.c
 * $Author: mha $
 * $Revision: 1.3 $ 
 * $Date: Wed Jun  7 09:47:42 2006 $
 *
 * Comment:
 *  None
 *
 * History:
 *  blake: Generated 1. November 2003
 *  ct:    Adapted 12. March 2004
 *  MHa:   Migrated to MoReUse September 2005
 *
 */

#include "ORION.H"

#include <phcsBflBal.h>
#include "phcsBflBal_Hw1SerTWRInt.h"

#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void  MIFARE_SendBlock(uint8_t *DataBuff, uint8_t BuffLenght);
extern	void  MIFARE_Enable_Rx_Chr(void);

/*--------------------------------------------------------------------------------------*\
Global variables and types definition	
\*--------------------------------------------------------------------------------------*/

uint8_t	PN512_RxData;



/* Externally accessible functions: */
//---------------------------------------------------------------------------------------------------------------
void phcsBflBal_Hw1SerTWRInit(phcsBflBal_t  *cif, void *comm_params) {
	
    //MR19 phcsBflBal_Hw1SerLinInitParams_t *bp = comm_params;

    cif->mp_Members     = comm_params;
    cif->ReadBus        = phcsBflBal_Hw1SerTWRReadBus;
    cif->WriteBus       = phcsBflBal_Hw1SerTWRWriteBus;
}

//---------------------------------------------------------------------------------------------------------------
phcsBfl_Status_t phcsBflBal_Hw1SerTWRWriteBus(phcsBflBal_WriteBusParam_t *writebus_param)
{
static uint8_t	tempTxData;
	
	tempTxData = (uint8_t)(writebus_param->bus_data);													// Byte da inviare in tempTxData
	MIFARE_SendBlock(&tempTxData, 1U);

	WDT_Clear(gVars.devWDT);  
	Touts.tmUartMif_TX = Milli500;																		// MR 22.6.2013: ridotto da 1 sec per evitare reset da WDT
	while (gVars.MifareTxCplt == false) 
	{
		if (Touts.tmUartMif_TX == 0) 
		{
			return PH_ERR_BFL_INTERFACE_ERROR;
		}
    }
    gVars.MifareTxCplt = false;
	return PH_ERR_BFL_SUCCESS;
}

//---------------------------------------------------------------------------------------------------------------
phcsBfl_Status_t phcsBflBal_Hw1SerTWRReadBus(phcsBflBal_WriteBusParam_t *readbus_param)
{
	MIFARE_Enable_Rx_Chr();																				// Start reception of one character
	WDT_Clear(gVars.devWDT);  
	Touts.tmUartMif_RX = Milli500;																		// MR 22.6.2013: ridotto da 1 sec per evitare reset da WDT
	if (gOrionKeyVars.PN512_RapidSearch == true) Touts.tmUartMif_RX = Milli30;							// MR 29.7.2016: ridotto a 30 msec per non rallentare il prodotto

	while (gVars.MifareRDRF == false)
	{
		if (Touts.tmUartMif_RX == 0) 
		{
			return PH_ERR_BFL_INTERFACE_ERROR;
		}
    }
    gVars.MifareRDRF = false;
  	readbus_param->bus_data = PN512_RxData;
  	return PH_ERR_BFL_SUCCESS;
}
