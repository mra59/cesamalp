/****************************************************************************************
File:
    VMC_CPU_LCD.c

Description:
    Funzioni per display LCD della CPU VMC.
    

History:
    Date       Aut  Note
    Apr 2019 	MR   

*****************************************************************************************/

#include <string.h>
#include <stdio.h>

#include "main.h"
#include "ORION.H" 
#include "VMC_CPU_LCD.h"
#include "VMC_Main.h"
#include "IICEEP.h"
#include "VMC_CPU_HW.h"
#include "OrionTimers.h"
#include "OrionCredit.h"
#include "I2C_EEPROM.h"
#include "Funzioni.h"


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

DISPLCDTimes 	DispLCDTouts;																	// Struttura con i timeout display
LCD_Display		LCDdisp;																		// Struttura per la gestione del display LCD

bool			LCD_SendHalfCmd, LCD_No_Test_BF;
uint8_t			Lingua;
char			LCD_DataBuff[LCD_DBUF_SIZE];
char			MachineModelNumber[20];															// ID102 (EVA-DTS Machine Model Number)
char			LCD_StbyMsg[LCD_DBUF_SIZE];														// Messaggio di st-by per non continuare a prelevarlo dalla Flash/EE

char			LCD_MsgRow1[LEN_LCDMSG];														// Messaggio programmabile Erogazione 1  da mettere sul display LCD
char			LCD_MsgRow2[LEN_LCDMSG];														// Messaggio programmabile Erogazione 2  da mettere sul display LCD
char			LCD_MsgRow3[LEN_LCDMSG];														// Messaggio programmabile Erogazione 3  da mettere sul display LCD
char			LCD_MsgRow4[LEN_LCDMSG];														// Messaggio programmabile Erogazione 4  da mettere sul display LCD

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	bool  		Is_DA_Vuoto(void);
extern	bool  		Is_VMC_Fail(void);
extern	void  		LeggiOrologio(void);
extern	uint8_t 	ReadErogazStatus(uint8_t NumErogaz);
extern	bool  		IsErogazInProgress(void);
extern	uint16_t	Ventolino[];
extern	bool  		ErogazTimeRemaining(uint32_t *TimeResiduo, uint8_t NumErogaz);
extern	uint32_t	TimeRemaining;
extern	uint16_t	CntFlussRemaining;




//----------------------------------------
#if (LCDmsg_Lingua == 0)
extern	const char mInitMsgLCD;
extern	const char mStByNoZucDaResto;
extern	const char mStByNoZucNoResto;
extern	const char mStByZucDaResto;
extern	const char mStByZucNoResto;
extern	const char mStByDaResto;
extern	const char mStByNoResto;
extern	const char mRiscaldam;
extern	const char mLCDWait;  
extern	const char mLCDGetBev;
extern	const char mCostoSelez;
extern	const char mNoVMC;  	
extern	const char mProdEsaur;
extern	const char mNewConfig;
extern	const char mWaitReset;
extern	const char mLavGrup;  
extern	const char mLavMixer; 
extern	const char mWaitInit; 
extern	const char mProgrammaz;
extern	const char mLCDVendFail;
extern	const char mRaffreddam;
extern	const char mUSB_KEY; 
extern	const char mUSB_KEY_Withdraw;
extern	const char mKeyOK[];
extern	const char mFileErr[];
extern	const char mStBy_Msg;

extern	const char mInitMsgLCD_2x16;
extern	const char mStBy_Msg_2x16;
extern	const char mNoVMC_2x16;
extern	const char mProdEsaur_2x16;
extern	const char mCostoSelez_2x16;
extern	const char mNewConfig_2x16;
extern	const char mIN_CORSO_2x16;
extern	const char mIN_PAUSA_2x16;
extern	const char mPRONTA_2x16;
extern	const char mNON_DISPO_2x16;
extern	const char mWaitReset_2x16;
extern	const char mUSB_KEY_Withdraw_2x16;
extern	const char mUSB_KEY_2x16;


#endif


//   Messaggi  di  Guasto 

extern	const char mMotCapsGuasto;
extern	const char mErrMotFAIL;
extern	const char mGruppoFail; 
extern	const char mCaricoFail; 
extern	const char mColonnaFail;
extern	const char mTroppoPieno;
//extern	const char mErrNoCaps; 
extern	const char mSondaFail;

extern	const char mICCFail;
extern	const char mTempOutRange;
extern	const char mNoParam;
extern	const char mErrNoLink;  
extern	const char mErrNoCodes; 
extern	const char mFineDeconto; 
extern	const char mDosatMaisFail;
extern	const char mErrCooling;
extern	const char mFINE_PRODOTTO;
extern	const char mEMERGENCY_PRESSED;

extern	const  char mIN_CORSO;
extern	const  char mIN_PAUSA;
extern	const  char mPRONTA;
extern	const  char mNON_DISPO;


//   Messaggi  di  Collaudo  
extern	const char mCollInCorso;
extern	const char mRadConfig;
extern	const char mDownloadIrda;
extern	const char mClearMemory;
extern	const char mEndCollaudo_OK;

/*------------------------------------------------------------------------------------------*\
 Method: Gestione_LCD
	Predispone il buffer display in base a quanto e' richiesto di visualizzare.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  Gestione_LCD(void)
{
	if (gVMCState.InProgrammaz == false)														// Messaggi da gestire solo fuori dalla programmazione
	{
	  // Rimango in visual init per il tempo programmato
		if (LCDdisp.Phase == LCD_OutInit)
		{
			if (LCDdisp.ForzaVisCred == true)
			{
				LCDdisp.Phase = LCDNormalOper;
				LCDdisp.ForzaVisCred = true;
			}
			return;
		}
		if (LCDdisp.Phase != LCDPrelevare_Msg)
		{
			if (fVMC_NoCodici)																	// Non ci sono i codici di sicurezza
			{
			 	LCD_Disp_VMC_Fail();
				return;
			}
			if (fVMC_NoParam)																	// Non ci sono parametri sufficienti al funzionmento del DA
			{
			 	LCD_Disp_VMC_Fail();
				return;
			}
			if ((Is_VMC_Fail() == true) || (Is_DA_Vuoto() == true))								// C'e' almeno un guasto o da Vuoto
			{
				LCD_Disp_VMC_Fail();
				return;
			}
			// Visualizzazione guasti NON bloccanti
			if (fVMC_NoLink)																	// No Link con Master Executive
			{
			 	LCD_Disp_VMC_Fail();
				return;
			}
			else
			{
				LCD_ClearAllarmMessage();														// Tolgo eventuale messaggio di fail dal display
			}
		}
		// ======   VMC OK  =========
		if (gVMCState.InSelezione == false)
		{
			if (TmrTimeout(DispLCDTouts.TimeDispMsg))											// Solo a timeout scaduto elaboro nuovi messaggi
			{
				Norefresh = false;
				// May 2020 aggiunto test SelezReq per evitare che tra il decremento del credito per la selez
				// e l'inizio della stessa, l'LCD fosse aggiornato con la visualizz. di st-by col credito per
				// cambiare subito dopo con il msg di selez in corso
				
				if ( ((VMC_CreditoDaVisualizzare != LCDdisp.CreditoDisplay) && (gVMCState.SelezReq == false)) || 
					 LCDdisp.ForzaVisCred || ExChStatusChanged())
				{
					LCDdisp.ForzaVisCred = false;
					LCDdisp.CreditoDisplay = VMC_CreditoDaVisualizzare;
					LCDdisp.Phase = LCDNormalOper;
					LCD_Disp_Clear();
					MessaggioStBy();															// Messaggio di st-by
					if (VMC_CreditoDaVisualizzare != 0)
					{
						if (gOrionConfVars.LCD_Type == LCD_2x16)
						{
							ValutaInMessage(&LCD_DataBuff[0], VMC_CreditoDaVisualizzare, LCD_BUF_START_CREDITO);	// Visualizza credito
						}
						else
						{
							ValutaInMessage(&LCD_DataBuff[0], VMC_CreditoDaVisualizzare, CredCursorStart);	// Visualizza credito
						}
					}
					Refresh_LCD();
					LCDdisp.VisuAll = false;
				}
			}  // End attesa timer messaggio a tempo
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Init
	Inizializza il display LCD.
	Se richiesto predispone la visualizzazione della revisione firmware per il tempo 
	prestabilito.

   IN:  - VisFW_Rev true per visualizzare revisione firmware
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Init(bool InitMsg)
{
	uint32_t	k;
	const char	*msgPnt;
	uint8_t		j;
	char	FWRevBuffer[5];
		
	Clear_LCD_NoAnswer();
	LCD_POWER(ON);																				// Accendo display LCD Locale
	HAL_Delay(Milli100);

	// ******************************************************
	// ****  L C D    S T A N D A R D    Con   Delay   ******
	// ******************************************************
	for (j=0; j<3; j++) {																		// Trasmetto 3 volte 0x30 distanziati di 10 msec
		LCD_SendHalfCmd = true;
		SendCommand_LCD(0x30);
		HAL_Delay(Milli10);
	}

	// Set 4 bit MODE
	LCD_SendHalfCmd = true;
	SendCommand_LCD(0x20);
	HAL_Delay(Milli10);
		
	// 4 bit MODE, 2 lines, font 5x8
	SendCommand_LCD(0x28);
	HAL_Delay(Milli10);

	// Increment, No shift
	SendCommand_LCD(0x06);
	HAL_Delay(Milli10);
		
	// Display ON, Cursor OFF, Blink OFF
	#if LCDVisualCursor
		SendCommand_LCD(0x0F);																	// Tutto ON per debug
	#else
		SendCommand_LCD(0x0C);
	#endif
	HAL_Delay(Milli10);
			
//-----------------------------------------------------------------------------------------------------------------------------------
			
	if (gOrionConfVars.LCD_Type == LCD_2x16)
	{
	// *******************************************************
	// ****   M S G      I N I Z I A L E      2 x 16   *******
	// *******************************************************
		if (InitMsg)
		{
			// Visualizzo messaggio di accensione
			#if LCDmsg_Lingua
				// -----    Messaggi  in  EEprom  ----
				ReadMessage(&LCD_DataBuff[0], Lingua, mInitMsgLCD_2x16, FOUR_LCD_NUMCOL);
			#else		
				// -----    Messaggi  in  FLASH    ----
				strcpy((char*)&LCD_DataBuff[0], &mInitMsgLCD_2x16);
			#endif

				ReadEEPI2C(IICEEPConfigAddr(EEModelloMacchina), (uint8_t *)(&MachineModelNumber), Len_ID102);		// Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20)
				if ((MachineModelNumber[0] < FirstASCII_Code) || (MachineModelNumber[0] > LastASCII_Code))
				{
					sprintf(&MachineModelNumber[0], " A L P SrL          ");
					MachineModelNumber[Len_ID102-1] = ASCII_SPACE;
				}
				else
				{
					j = 0;
					for (k=0; k < sizeof(MachineModelNumber); k++)
					{
						if ((MachineModelNumber[0] == ASCII_SPACE)) j++;
					}
					if (j == sizeof(MachineModelNumber))
					{
						sprintf(&MachineModelNumber[0], " A L P SrL          ");
						MachineModelNumber[Len_ID102-1] = ASCII_SPACE;
					}
				}
				strncpy(&LCD_DataBuff[0], &MachineModelNumber[0], LCD_2x16_NUMCOL);
				strncpy(&FWRevBuffer[0], kProductSWVersionStr, 4U);													// Visualizzo Revisione Firmware
				LCD_DataBuff[LCD_POS_FW_REV_2x16] 	= FWRevBuffer[0];
				LCD_DataBuff[LCD_POS_FW_REV_2x16+1] = FWRevBuffer[1];
				LCD_DataBuff[LCD_POS_FW_REV_2x16+3] = FWRevBuffer[2];
				LCD_DataBuff[LCD_POS_FW_REV_2x16+4] = FWRevBuffer[3];
				Refresh_LCD();
			#if TimeFwBreve
				Time_Init(OneSec);																					// Almeno 2,5 sec per segnalare I/O Fail: il Fail I/O avviene dopo circa 2 sec di silenzio
			#else		
				Time_Init(ThreeSec);
			#endif
		}
		//-- in RAM messaggio di St-By per non fare continui accessi alla Flash/EE --
	#if LCDmsg_Lingua
		// -----    Messaggi  in  EEprom  ----
		ReadMessage(&LCD_StbyMsg, Lingua, mStBy_Msg_2x16, FOUR_LCD_NUMCOL);
	#else		
		// -----    Messaggi  in  FLASH    ----
		msgPnt = &mStBy_Msg_2x16;
		for (k=0; k < LCD_CHAR_NUM; k++)
		{
			LCD_StbyMsg[k] = *(msgPnt + k);
		}
	#endif

	}
	else	// gOrionConfVars.LCD_16x2
	{
	// *******************************************************
	// ****   M S G      I N I Z I A L E      4 x 20   *******
	// *******************************************************
		if (InitMsg)
		{
			// Visualizzo messaggio di accensione
			#if LCDmsg_Lingua
				// -----    Messaggi  in  EEprom  ----
				ReadMessage(&LCD_DataBuff[0], Lingua, mInitMsgLCD, FOUR_LCD_NUMCOL);
			#else		
				// -----    Messaggi  in  FLASH    ----
				strcpy((char*)&LCD_DataBuff[0], &mInitMsgLCD);
			#endif

				ReadEEPI2C(IICEEPConfigAddr(EEModelloMacchina), (uint8_t *)(&MachineModelNumber), Len_ID102);		// Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20)
				if ((MachineModelNumber[0] < FirstASCII_Code) || (MachineModelNumber[0] > LastASCII_Code))
				{
					sprintf(&MachineModelNumber[0], " A L P SrL          ");
					MachineModelNumber[Len_ID102-1] = ASCII_SPACE;
				}
				strncpy(&LCD_DataBuff[0], &MachineModelNumber[0], Len_ID102);
				strncpy(&FWRevBuffer[0], kProductSWVersionStr, 4U);													// Visualizzo Revisione Firmware
				LCD_DataBuff[LCD_POS_FW_REV_4x20] 	 = FWRevBuffer[0];
				LCD_DataBuff[LCD_POS_FW_REV_4x20+1] = FWRevBuffer[1];
				LCD_DataBuff[LCD_POS_FW_REV_4x20+3] = FWRevBuffer[2];
				LCD_DataBuff[LCD_POS_FW_REV_4x20+4] = FWRevBuffer[3];
				Refresh_LCD();
			#if TimeFwBreve
				Time_Init(OneSec);																					// Almeno 2,5 sec per segnalare I/O Fail: il Fail I/O avviene dopo circa 2 sec di silenzio
			#else		
				Time_Init(ThreeSec);
			#endif
		}
		//-- in RAM messaggio di St-By per non fare continui accessi alla Flash/EE --
	#if LCDmsg_Lingua
		// -----    Messaggi  in  EEprom  ----
		ReadMessage(&LCD_StbyMsg, Lingua, mStBy_Msg, FOUR_LCD_NUMCOL);
	#else		
		// -----    Messaggi  in  FLASH    ----
		msgPnt = &mStBy_Msg;
		for (k=0; k < LCD_CHAR_NUM; k++)
		{
			LCD_StbyMsg[k] = *(msgPnt + k);
		}
	#endif

		//-- In RAM i 4 messaggi programmabili Erogazione da visualizzare su LCD --	
		ReadMessagesFromEE();
		
		ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow1), (uint8_t *)(&LCD_MsgRow1), LEN_LCDMSG);
		if ((LCD_MsgRow1[0] < FirstASCII_Code) || (LCD_MsgRow1[0] > LastASCII_Code))
		{
			sprintf(&LCD_MsgRow1[0], "Erogaz 1            ");
		}
		ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow2), (uint8_t *)(&LCD_MsgRow2), LEN_LCDMSG);
		if ((LCD_MsgRow2[0] < FirstASCII_Code) || (LCD_MsgRow2[0] > LastASCII_Code))
		{
			sprintf(&LCD_MsgRow2[0], "Erogaz 2            ");
		}
		ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow3), (uint8_t *)(&LCD_MsgRow3), LEN_LCDMSG);
		if ((LCD_MsgRow3[0] < FirstASCII_Code) || (LCD_MsgRow3[0] > LastASCII_Code))
		{
			sprintf(&LCD_MsgRow3[0], "Erogaz 3            ");
		}
		ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow4), (uint8_t *)(&LCD_MsgRow4), LEN_LCDMSG);
		if ((LCD_MsgRow4[0] < FirstASCII_Code) || (LCD_MsgRow4[0] > LastASCII_Code))
		{
			sprintf(&LCD_MsgRow4[0], "Erogaz 4            ");
		}
	}
		
	LCDdisp.Phase = LCD_OutInit;
}

/*------------------------------------------------------------------------------------------*\
 Method: MessaggioStBy
	Visualizzazione status delle 4 erogazioni.
	Gli stati possono essere:
	1) Pronta	OK
	2) In Corso R
	3) In Pausa P
	4) -------  -- (Non disponibile)
	

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  MessaggioStBy(void)
{
	uint8_t		ErogazStatus, offsetRow;
	uint32_t	j, k;
	const char	*msgPnt;

	if (gOrionConfVars.LCD_Type == LCD_2x16)
	{
		// ************************************************
		// ****   STATUS 4  EROGAZIONI     2 x 16   *******
		// ************************************************
		for (j=0; j < LCD_DBUF_SIZE; j++)															//-- Loop visualizzazione status delle 4 erogazioni --
		{
			LCD_DataBuff[j] = LCD_StbyMsg[j];
		}
		if (IsErogazInProgress() == true)
		{
			for (j=0; j < 16; j++)
			{
				LCD_DataBuff[j] = ASCII_SPACE;
			}
			for (j=0; j < MAXNUMSEL; j++)
			{
				if (ReadErogazStatus(j) == true)
				{
					LCD_DataBuff[0] = ((j+1) + ASCII_ZERO);
					LCD_DataBuff[1] = ASCII_COLON;
					if (TimeRemaining == 0)
					{
						//-- Selezione a impulsi --
						Hex16InMessage(&LCD_DataBuff[0], CntFlussRemaining, 3);							// Visualizza flussimetro fino a 5 cifre
						break;
					}
					else
					{
						//-- Selezione a Tempo --
						k = (TimeRemaining/1000 + 1);
						Hex16InMessage(&LCD_DataBuff[0], (uint16_t)k, 3);								// Visualizza Time Remaining fino a 5 cifre
						break;
					}
				}
			}			
		}
		else
		{
			for (j=0; j < MAXNUMSEL; j++)
			{
				if ((sSelParam_A[j].SelFlags & BIT_INH_SEL) == true)									// Test flag INH della selezione in elaborazione
				{
					msgPnt = &mNON_DISPO_2x16;															// Erogazione NON disponibile
				}
				else
				{
					ErogazStatus = ReadErogazStatus(j);
					if ((ErogazStatus & INPROGR_STATUS) == true)
					{
						msgPnt = &mIN_CORSO_2x16;														// Erogazione in corso
					}
					else
					{
						msgPnt = &mPRONTA_2x16;															// Erogazione disponibile e in St-By
					}
				}
				switch(j)
				{
					case 0:
						offsetRow = LCD_BUF_POS_STATUS_EROGAZ_1;
						break;
					case 1:
						offsetRow = LCD_BUF_POS_STATUS_EROGAZ_2;
						break;
					case 2:
						offsetRow = LCD_BUF_POS_STATUS_EROGAZ_3;
						break;
					case 3:
						offsetRow = LCD_BUF_POS_STATUS_EROGAZ_4;
						break;
				}
				for (k=0; k < LEN_MSG_STATUS_EROGAZIONE; k++)
				{
					LCD_DataBuff[(offsetRow + k)] = *(msgPnt + k);
				}
			}
		}
	}
	else	// gOrionConfVars.LCD_Type
	{
		// ************************************************
		// ****   STATUS 4  EROGAZIONI     4 x 20   *******
		// ************************************************
		if (LCDdisp.NewMsgInEE == true)
		{
			ReadMessagesFromEE();
		}
		for (j=0; j < MAXNUMSEL; j++)																// Loop visualizzazione status delle 4 erogazioni --
		{
			switch(j)
			{
				case 0:
					offsetRow = (Riga_1_In_Buffer + 0);
					for (k=0; k < LEN_LCDMSG; k++)
					{
						LCD_DataBuff[(offsetRow + k)] = LCD_MsgRow1[k];
					}
					break;
				case 1:
					offsetRow = (Riga_2_In_Buffer + 0);
					for (k=0; k < LEN_LCDMSG; k++)
					{
						LCD_DataBuff[(offsetRow + k)] = LCD_MsgRow2[k];
					}
					break;
				case 2:
					offsetRow = (Riga_3_In_Buffer + 0);
					for (k=0; k < LEN_LCDMSG; k++)
					{
						LCD_DataBuff[(offsetRow + k)] = LCD_MsgRow3[k];
					}
					break;
				case 3:
					offsetRow = (Riga_4_In_Buffer + 0);
					for (k=0; k < LEN_LCDMSG; k++)
					{
						LCD_DataBuff[(offsetRow + k)] = LCD_MsgRow4[k];
					}
					break;
			}
			if ((sSelParam_A[j].SelFlags & BIT_INH_SEL) == true)									// Test flag INH della selezione della riga LCD in elaborazione
			{
				msgPnt = &mNON_DISPO;																// Erogazione NON disponibile
			}
			else
			{
				ErogazStatus = ReadErogazStatus(j);
				if ((ErogazStatus & INPROGR_STATUS) == true)
				{
					msgPnt = &mIN_CORSO;															// Erogazione in corso
				}
				else
				{
					msgPnt = &mPRONTA;																// Erogazione disponibile e in St-By
				}
			}
			offsetRow += OFFS_MSG_EROGAZ;
			for (k=0; k < LEN_MSG_EROGAZ_STATUS; k++)
			{
				LCD_DataBuff[(offsetRow + k)] = *(msgPnt + k);
			}
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: Y_N_InMessage
	Inserisce il carattere "N" oppure "Y" a seconda se il byte di entrata e' zero o diverso
	da zero.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Y_N_InMessage(char* pntDestMsg, uint8_t ValoreHex, uint8_t Pos)
{
	char		*destpnt;
	
	destpnt = pntDestMsg;
	if (ValoreHex != 0) {
		*(destpnt+Pos) = ASCII_YES;
	} else {
		*(destpnt+Pos) = ASCII_NO;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: ValutaInMessage
	Inserisce una valore in valuta nel buffer di destinazione.
	In base al DPP inserisce il punto decimale e toglie gli zeri non significativi.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ValutaInMessage(char* pntDestMsg, uint16_t Valuta, uint8_t ValPosition)
{
	uint8_t		CursPosition, numchar;
	char		buffer[10];
	char		*destpnt;
	
	destpnt = pntDestMsg;
	sprintf(&buffer[0], "%u", Valuta);																// Trasformo valuta in chr ASCII in buffer temporaneo
	numchar = (strlen(&buffer[0]));																// Lunghezza della valuta in ASCII

	if (DecimalPointPosition == 2) {
		CursPosition = ValPosition;
		// ***********     Credito in centesimi    **************
		switch (numchar-1) {
			case 0:																				// Credito a 1 cifra significativa: visualizzo --0.0x
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_ZERO;											// Unita' = 0
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_DP;												// DPP
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_ZERO;											// Decimi = 0
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Centesimi
				break;
			
			case 1:																				// Credito a 2 cifre significative: visualizzo --0.xx
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_ZERO;											// Unita' = 0
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_DP;												// DPP
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Decimi
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Centesimi
				break;

			case 2:																				// Credito a 3 cifre significative: visualizzo --x.xx
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Unita'
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_DP;												// DPP
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Decimi
				CursPosition++;
				*(destpnt+CursPosition) = buffer[2];											// Centesimi
				break;

			case 3:																				// Credito a 4 cifre significative: visualizzo -xx.xx
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Decine
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Unita'
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_DP;												// DPP
				CursPosition++;
				*(destpnt+CursPosition) = buffer[2];											// Decimi
				CursPosition++;
				*(destpnt+CursPosition) = buffer[3];											// Centesimi
				break;
			
			case 4:																				// Credito a 5 cifre significative: visualizzo xxx.xx
				*(destpnt+CursPosition) = buffer[0];											// Centinaia
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Decine
				CursPosition++;
				*(destpnt+CursPosition) = buffer[2];											// Unita'
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_DP;												// DPP
				CursPosition++;
				*(destpnt+CursPosition) = buffer[3];											// Decimi
				CursPosition++;
				*(destpnt+CursPosition) = buffer[4];											// Centesimi
				break;
			
			default:
				break;
		}
	} else {
		// ***********     Credito senza  DPP      **************
		CursPosition = ValPosition;
		switch (numchar-1) {
			case 0:																				// Credito a 1 cifra significativa: visualizzo -----x
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Unita'
				break;
			
			case 1:																				// Credito a 2 cifre significative: visualizzo ----xx
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Decine
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Unita'
				break;

			case 2:																				// Credito a 3 cifre significative: visualizzo ---xxx
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Centinaia
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Decine
				CursPosition++;
				*(destpnt+CursPosition) = buffer[2];											// Unita'
				break;

			case 3:																				// Credito a 4 cifre significative: visualizzo --xxxx
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Migliaia
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Centinaia
				CursPosition++;
				*(destpnt+CursPosition) = buffer[2];											// Decine
				CursPosition++;
				*(destpnt+CursPosition) = buffer[3];											// Unita'
				break;
			
			case 4:																				// Credito a 5 cifre significative: visualizzo -xxxxx
				*(destpnt+CursPosition) = ASCII_SPACE;											// SPACE
				CursPosition++;
				*(destpnt+CursPosition) = buffer[0];											// Decine di Migliaia
				CursPosition++;
				*(destpnt+CursPosition) = buffer[1];											// Migliaia
				CursPosition++;
				*(destpnt+CursPosition) = buffer[2];											// Centinaia
				CursPosition++;
				*(destpnt+CursPosition) = buffer[3];											// Decine
				CursPosition++;
				*(destpnt+CursPosition)	 = buffer[4];											// Unita'
				break;
			
			default:
				break;
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: Hex32InMessage
	Inserisce un valore a 32 bit nel buffer di destinazione allineandolo a sinistra, senza
	zeri non significativi.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Hex32InMessage(char* pntDestMsg, uint32_t Valore, uint8_t ValPosition, uint8_t NumCifre)
{
	uint8_t		i, CursPosition;
	char		buffer[16];
	char		*destpnt;
	
	destpnt = pntDestMsg;
	switch(NumCifre)
	{
	case 8:
		sprintf(buffer,"%8u", Valore);														// Trasformo hex32 in chr ASCII in buffer temporaneo di 8 chr
	  	break;
	  
	case 9:
		sprintf(buffer,"%9u", Valore);														// Trasformo hex32 in chr ASCII in buffer temporaneo di 9 chr
	  	break;
	  
	case 10:
		sprintf(buffer,"%10u", Valore);														// Trasformo hex32 in chr ASCII in buffer temporaneo di 10 chr
	  	break;
	  
	default:
		sprintf(buffer,"%10u", Valore);														// Trasformo hex32 in chr ASCII in buffer temporaneo di 10 chr
	  	break;
	}
	CursPosition = ValPosition;
	for (i=0; i < NumCifre; i++)															// Copio buffer temporaneo del buffer destinazione
	{
		*(destpnt+CursPosition) = buffer[i];
		CursPosition++;
	}

/*MR19  Non funziona, non mette gli spazi al posto degli zeri non significativi  
  	uint8_t		i, numchar;
	char		buffer[16];

	sprintf(&buffer[0],"%u",Valore);																// Trasformo hex32 in chr ASCII in buffer temporaneo
	numchar = (strlen(&buffer[0]));
	for (i=0; i < numchar; i++) {
		pntDestMsg[ValPosition+i] = buffer[i];
	}
*/
}

/*------------------------------------------------------------------------------------------*\
 Method: Hex16InMessage
	Inserisce una valore a 16 bit nel buffer di destinazione sostituendo spazi agli zeri non
	significativi.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Hex16InMessage(char* pntDestMsg, uint16_t Valore, uint8_t ValPosition)
{
	uint8_t		CursPosition;
	char		buffer[10];
	char		*destpnt;
	
	destpnt = pntDestMsg;
	sprintf(buffer,"%5u",Valore);																// Trasformo hex16 in chr ASCII in buffer temporaneo

	CursPosition = ValPosition;
	*(destpnt+CursPosition) = buffer[0];														// Decine di Migliaia
	CursPosition++;
	*(destpnt+CursPosition) = buffer[1];														// Migliaia
	CursPosition++;
	*(destpnt+CursPosition) = buffer[2];														// Centinaia
	CursPosition++;
	*(destpnt+CursPosition) = buffer[3];														// Decine
	CursPosition++;
	*(destpnt+CursPosition)	= buffer[4];														// Unita'
}

/*------------------------------------------------------------------------------------------*\
 Method: Hex8InMessage
	Inserisce una valore a 8 bit nel buffer di destinazione sostituendo spazi agli zeri non
	significativi.
	Se la flag "integer" e' true si visualizza il "Valore_int8" con int8 con segno. 

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Hex8InMessage(char* pntDestMsg, uint8_t Valore, uint8_t ValPosition, bool integer, int8_t Valore_int8) {
	
	uint8_t		CursPosition;
	char		buffer[5];
	char		*destpnt;
	
	destpnt = pntDestMsg;
	if (integer == true) {
		sprintf(buffer,"%3d",Valore_int8);														// Tratto Valore come int8
	} else {
		sprintf(buffer,"%3u",Valore);															// Trasformo hex8 in chr ASCII in buffer temporaneo
	}
	CursPosition = ValPosition;
	*(destpnt+CursPosition) = buffer[0];														// Centinaia
	CursPosition++;
	*(destpnt+CursPosition) = buffer[1];														// Decine
	CursPosition++;
	*(destpnt+CursPosition) = buffer[2];														// Unita'
}

/*------------------------------------------------------------------------------------------*\
 Method: Visual_BCD_2_Cifre
	Inserisce un numero di 2 cifre (00-99) nel buffer di destinazione nella posizione 
	richiesta.
	Visualizza sempre 2 cifre.

   IN:  - cifra in Hex (limiti 0x0-0x63)
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Visual_BCD_2_Cifre(char* pntDestMsg, uint8_t Numero, uint8_t NumPosition) {
	
	uint8_t		CursPosition, numchar;
	char		buffer[5];
	char		*destpnt;
	
	destpnt = pntDestMsg;
	CursPosition = NumPosition;
	sprintf(buffer,"%u",Numero);																// Trasformo il numero in chr ASCII in buffer temporaneo
	numchar = (strlen(&buffer[0]));																// Lunghezza della valuta in ASCII
	if (numchar == 1) {
		*(destpnt+CursPosition) = ASCII_ZERO;
		CursPosition++;
		*(destpnt+CursPosition) = buffer[0];
	} else if (numchar == 2) {
		*(destpnt+CursPosition) = buffer[0];
		CursPosition++;
		*(destpnt+CursPosition) = buffer[1];
	} else {
		CursPosition--;
		*(destpnt+CursPosition) = buffer[0];
		CursPosition++;
		*(destpnt+CursPosition) = buffer[1];
		CursPosition++;
		*(destpnt+CursPosition) = buffer[2];
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: Visual_BCD_1_Cifra
	Inserisce un numero di 1 cifra (0-9) nel buffer di destinazione nella posizione 
	richiesta.

   IN:  - cifra in Hex (limiti 0x0-0x9)
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Visual_BCD_1_Cifra(char* pntDestMsg, uint8_t Numero, uint8_t NumPosition)
{
	//uint8_t		CursPosition;
	//uint8_t		numchar;
	char		buffer[2];
	char		*destpnt;
	
	destpnt = pntDestMsg;
	//CursPosition = NumPosition;
	sprintf(buffer,"%1u",Numero);																// Trasformo il numero in chr ASCII in buffer temporaneo
	//numchar = (strlen(&buffer[0]));																// Lunghezza della valuta in ASCII
	*(destpnt+NumPosition) = buffer[0];
}

/*------------------------------------------------------------------------------------------*\
 Method: Message_In_Message
	Inserisce una stringa messaggio nell'attuale databuff dalla posizione desiderata.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Message_In_Message(const char* pntMsgIn, uint8_t RowNum)
{
	uint8_t		i, CursPosition;
	
	if (gOrionConfVars.LCD_Type == LCD_4x20)
	{
		switch (RowNum)
		{
			case 1:
				CursPosition = Riga_1_In_Buffer + HALF_ROW_POS;
				break;
			case 2:
				CursPosition = Riga_2_In_Buffer + HALF_ROW_POS;
				break;
			case 3:
				CursPosition = Riga_3_In_Buffer + HALF_ROW_POS;
				break;
			case 4:
			default:
				CursPosition = Riga_4_In_Buffer + HALF_ROW_POS;
				break;
		}
		for(i=0; i < (LCD_NUMCOL - HALF_ROW_POS); i++)
		{
			LCD_DataBuff[CursPosition + i] = *(pntMsgIn + i);
		}
	}
	else
	{
		switch (RowNum)
		{
			case 1:
				CursPosition = Riga_1_In_Buffer + POSIT_MSG1_ROW1;
				break;
			case 2:
				CursPosition = Riga_2_In_Buffer + POSIT_MSG1_ROW2;
				break;
			case 3:
				CursPosition = Riga_1_In_Buffer + POSIT_MSG2_ROW1;
				break;
			case 4:
				CursPosition = Riga_2_In_Buffer + POSIT_MSG2_ROW2;
				break;
		}
		LCD_DataBuff[CursPosition] = *(pntMsgIn);
		LCD_DataBuff[CursPosition+1] = *(pntMsgIn+1);
	}
	Refresh_LCD();																				// Visualizza messaggio aggiornato
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_SetNormalVisual
	Predispone per la normale visualizzazione del DA.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_SetNormalVisual(void)
{
	if (LCDdisp.Phase != LCDNormalOper)
	{
		//LCDdisp.Phase = LCDNormalOper;			// E' in Gestione_LCD che viene impostata a LCDNormalOper allo scadere del timeout
		LCDdisp.ForzaVisCred = true;
		TmrStart(DispLCDTouts.TimeDispMsg, Milli10);
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_VendFail
	Predispone il buffer display per visualizzare il messaggio Vend Fail.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_VendFail(void) {
	
	if ((LCDdisp.Phase != LCDVendFail_Msg) && (LCDdisp.Phase != LCD_VMC_FAIL)) {				// Messaggio di vendita fallita se non c'e' gia' un msg di fail
		LCDdisp.Phase = LCDVendFail_Msg;
		LCD_Disp_Clear();
#if LCDmsg_Lingua
		ReadMessage(&LCD_DataBuff[0], Lingua, mLCDVendFail, FOUR_LCD_NUMCOL);
#else		
		strcpy(LCD_DataBuff, &mLCDVendFail);
#endif	
		Refresh_LCD();
		TmrStart(DispLCDTouts.TimeDispMsg, TMSG_FAILVEND);
	    LCDdisp.ForzaVisCred = true;															// Set visualizzata di st-by dallo scadere del timeout
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_NOSEL
	Predispone il buffer display per visualizzare il messaggio NOSEL.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_NOSEL(void) {
	
	if (LCDdisp.Phase != LCDNoSel_Msg)
	{
		LCDdisp.Phase = LCDNoSel_Msg;
		LCD_Disp_Clear();
		if (gOrionConfVars.LCD_Type == LCD_2x16)
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mProdEsaur_2x16, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mProdEsaur_2x16);
	#endif	
		}
		else
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mProdEsaur, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mProdEsaur);
	#endif	
		}
		Refresh_LCD();
		TmrStart(DispLCDTouts.TimeDispMsg, TMSG_NOSEL);
	    LCDdisp.ForzaVisCred = true;															// Set visualizzata di st-by dallo scadere del timeout
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_CredNoSuff
	Predispone il buffer display per visualizzare il costo della selezione richiesta
	quando non c'e' credito sufficiente per acquistarla.
	Feb 2023: aggiunto test flag "NoDispCreditoInsuff" per evitare che la richiesta di vendita
	generata automaticamente dalla opzione "MultiCycle" causi la visualizzazione del costo
	selezione quando il credito residuo e' insufficiente.
	Senza questa opzione gli ultimi secondi della selezione sarebbero nascosti dalla 
	visualizzazione del costo selezione causato dalla mancanza di credito.

   IN:  - costo della selezione richiesta
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_CredNoSuff(uint8_t SelezNum, CreditCpcValue PrezzoSel)
{
	if (gVMCState.NoDispCreditoInsuff == true)
	{
		gVMCState.NoDispCreditoInsuff = false;
		return;
	}
	
	
	if (LCDdisp.Phase != LCDNoSel_Msg)
	{
		LCD_Disp_Clear();
		if (gOrionConfVars.LCD_Type == LCD_2x16)
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mCostoSelez_2x16, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mCostoSelez_2x16);
	#endif	
			Visual_BCD_2_Cifre(&LCD_DataBuff[0], SelezNum, LCD_2X16_ROW2_NUMSEL);				// Visualizza numero selezione richiesta
			ValutaInMessage(&LCD_DataBuff[0], PrezzoSel, LCD_BUF_START_CREDITO);				// Visualizza costo selezione richiesta
		}
		else
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mCostoSelez, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mCostoSelez);
	#endif	
			Visual_BCD_2_Cifre(&LCD_DataBuff[0], SelezNum, ROW3_NUMSEL);						// Visualizza numero selezione richiesta
			ValutaInMessage(&LCD_DataBuff[0], PrezzoSel, CredCursorStart);						// Visualizza costo selezione richiesta
		}
		Refresh_LCD();
		TmrStart(DispLCDTouts.TimeDispMsg, TMSG_CREDNOSUFF);
	    LCDdisp.ForzaVisCred = true;															// Set visualizzata di st-by dallo scadere del timeout
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_SelInProgress
	Predispone il buffer display per visualizzare il messaggio dell'erogazione in corso.
	Lascia invariate le altre righe.
	Se l'erogazione fallisce o non e' disponibile visualizza i caratteri del msg "mNON_DISPO".

   IN:  - Numero Erogazione e Fallita/InErogazione
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_SelInProgress(uint8_t NumSel, bool SelezFail)
{
	char		LocalBuff[21];
	uint8_t		offsetRow;
	uint32_t	j;
	
	LCDdisp.Phase = LCDSelInCorso_Msg;
	if (SelezFail == VEND_FAIL)
	{
		strcpy(LocalBuff, &mNON_DISPO);
	}
	else
	{
		strcpy(LocalBuff, &mIN_CORSO);
	}

#if LCDmsg_Lingua
		ReadMessage(&LCD_DataBuff[0], Lingua, mLCDWait, FOUR_LCD_NUMCOL);
#else		
		
		if (gOrionConfVars.LCD_Type == LCD_2x16)
		{
			switch(NumSel)
			{
				case 01:
					offsetRow = (Riga_1_In_Buffer + OFFS_MSG_EROGAZ);
					break;

				case 02:
					offsetRow = (Riga_2_In_Buffer + OFFS_MSG_EROGAZ);
					break;
					
				case 03:
					offsetRow = (Riga_3_In_Buffer + OFFS_MSG_EROGAZ);
					break;
					
				case 04:
					offsetRow = (Riga_4_In_Buffer + OFFS_MSG_EROGAZ);
					break;
			}				
			for (j=0; j < LEN_MSG_EROGAZ_STATUS; j++)
			{
				LCD_DataBuff[(offsetRow + j)] = LocalBuff[j];
			}
		}
		else
		{
			
			
		}
	
	
	
	
	
	
	
#endif	
		Refresh_LCD();
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_PrelevareBevanda
	Visualizza il messaggio al termine di una selezione per 2 secondi

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_PrelevareBevanda(void)
{
	if (LCDdisp.Phase != LCDPrelevare_Msg) {
		LCDdisp.Phase = LCDPrelevare_Msg;
		LCD_Disp_Clear();
#if LCDmsg_Lingua
		ReadMessage(&LCD_DataBuff[0], Lingua, mLCDGetBev, FOUR_LCD_NUMCOL);
#else		
		strcpy(LCD_DataBuff, &mLCDGetBev);
#endif	
		Refresh_LCD();
		TmrStart(DispLCDTouts.TimeDispMsg, TMSG_PRELEVARE);
	    LCDdisp.ForzaVisCred = true;															// Set visualizzata di st-by dallo scadere del timeout
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_TastoDecinaSelez
	Visualizza il primo tasto dei due necessari per richiedere la selezione 

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_TastoDecinaSelez(void) {
/*	
	LCD_Disp_Clear();
	strcpy(LCD_DataBuff, &mLCDGetBev);
	Refresh_LCD();
	TmrStart(DispLCDTouts.TimeDispMsg, TASTO_DECINA_SELEZ);
    LCDdisp.ForzaVisCred = true;															// Set visualizzata di st-by dallo scadere del timeout
*/    
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_ClearBuffer
	Fill del buffer display con chr SPACE.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_ClearBuffer(void) {
	
	memset(&LCD_DataBuff, ASCII_SPACE, sizeof(LCD_DataBuff));
}

/*------------------------------------------------------------------------------------------*\
 Method: Msg_InAnyRow
	Memorizza il messaggio nel buffer display alla riga desiderata.
	Se flag clrbuff set, riempie il buffer display di spazi.

   IN:  - pointer al buffer messaggio e numero riga
  OUT:  - messaggio nel buffer display LCD_DataBuff
\*------------------------------------------------------------------------------------------*/
void  Msg_InAnyRow(char *pnt, uint8_t NumRiga, bool clrbuff) {

	uint8_t	j, k, offsetRow;
	
	if (clrbuff == true) {
		LCD_ClearBuffer();																		// Fill all buffer con ASCII_SPACE
	}
	switch (NumRiga) {
		case 1:
			offsetRow = Riga_1_In_Buffer;
			break;
		case 2:
			offsetRow = Riga_2_In_Buffer;
			break;
		case 3:
			offsetRow = Riga_3_In_Buffer;
			break;
		default:
		case 4:
			offsetRow = Riga_4_In_Buffer;
			break;
	}
	for (j=offsetRow, k=0; j < (offsetRow+LCD_NUMCOL); j++, k++) {
		LCD_DataBuff[j] = *(pnt+k);
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_WaitRestart
	Predispone il buffer display per visualizzare il messaggio Attendere Restart.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_WaitRestart(void)
{
	LCD_Disp_Clear();
#if LCDmsg_Lingua
		ReadMessage(&LCD_DataBuff[0], Lingua, mWaitReset, FOUR_LCD_NUMCOL);
#else		
		strcpy(LCD_DataBuff, &mWaitReset_2x16);
#endif	
	Refresh_LCD();
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_Clear
	Predispone il buffer display clear per un nuovo messaggio.
	Il comando e' eseguito in 1,52 msec @ 270KHz

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_Clear(void)
{
	SendCommand_LCD(LCD_CLEAR_DISPLAY);
	TmrStart(DispLCDTouts.ToutInitDisp, TMSG_CMD_CLEAR);
	while (!TmrTimeout(DispLCDTouts.ToutInitDisp)) {};
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Home
	Set a 0 il DDRAM address per puntare al primo chr della prima riga.
	Il comando e' eseguito in 1,52 msec @ 270KHz

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Home(void)
{
	SendCommand_LCD(LCD_RETURN_HOME);
	Tmr32Start(DispLCDTouts.ToutInitDisp, Milli3);
	while (!TmrTimeout(DispLCDTouts.ToutInitDisp)) {};
}

/*------------------------------------------------------------------------------------------*\
 Method: SetVisualAll
	Predispone per la visualizzazione di tutte le cifre del credito anche quando e' zero.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  SetVisualAll(void)
{
	LCDdisp.VisuAll = true;
    LCDdisp.ForzaVisCred = true;																// Set visualizzata di st-by dallo scadere del timeout
}

/*------------------------------------------------------------------------------------------*\
 Method: ClrVisualAll
	Predispone per la visualizzazione delle sole cifre significative del credito.
	E' chiamata per poter rivisualizzare la sola cifra delle unita' togliendo la carta con 
	credito che va/rimane a zero. 

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  ClrVisualAll(void) {

	LCDdisp.ForzaVisCred = true;
}

/*------------------------------------------------------------------------------------------*\
 Method: ForceNewVisual
	Forza un refresh del messaggio su display.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  ForceLCD_NewVisual(void)
{
	LCDdisp.ForzaVisCred = true;
}

/*------------------------------------------------------------------------------------------*\
 Method: ForceLCD_NewMsgEE
	Forza la rilettura dei messaggi dalla EE.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  ForceLCD_NewMsgEE(void)
{
	LCDdisp.NewMsgInEE = true;
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_VMC_Fail
	Predispone il buffer display per visualizzare il messaggio VMC Fail.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_VMC_Fail(void) {
	
	if (LCDdisp.Phase != LCD_VMC_FAIL)
	{
		LCDdisp.Phase = LCD_VMC_FAIL;
		LCD_Disp_Clear();
		if (gOrionConfVars.LCD_Type == LCD_2x16)
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mNoVMC_2x16, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mNoVMC_2x16);														//"  MACCHINA   SERVIZIO FUORI       "
	#endif	
		}
		else
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mNoVMC, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mNoVMC);															//"  MACCHINA   SERVIZIO FUORI       "
	#endif	
		}
		if (fVMC_NoParam)
		{
			strcpy(&LCD_DataBuff[GuastoCursorPos], &mNoParam);
			LCDdisp.AllPhase = msgNoParam;
		}
		else if (fVMC_MacchinaVuota)
		{
				//strcpy(&LCD_DataBuff[GuastoCursorPos], &mErrNoCaps);
				//LCDdisp.AllPhase = msgErrPompaVent;
		} 
		else if (fMOT_Caps_FAIL)
		{
				strcpy(&LCD_DataBuff[GuastoCursorPos], &mErrMotFAIL);
				LCDdisp.AllPhase = msgMotCapsFail;
		} 
		else if (fVMC_NoCodici)
		{
				strcpy(&LCD_DataBuff[GuastoCursorPos], &mErrNoCodes);
				LCDdisp.AllPhase = msgErrNoCodes;
		}
		else if (fVMC_NoLink)
		{
				strcpy(&LCD_DataBuff[GuastoCursorPos], &mErrNoLink);
				LCDdisp.AllPhase = msgErrNoCodes;
		}
		else if (fVMC_DecontoOver)
		{
				strcpy(&LCD_DataBuff[GuastoCursorPos], &mFineDeconto);
				LCDdisp.AllPhase = msgErrNoCodes;
		}
		else if (fVMC_ICC)
		{
				strcpy(&LCD_DataBuff[GuastoCursorPos], &mICCFail);
				LCDdisp.AllPhase = msgMotCapsFail;
		} 
		else if (Is_DA_Vuoto() == true)
		{
			strncpy(&LCD_DataBuff[FAIL_CODE_POS], &mFINE_PRODOTTO, 2U);
		}
		else if ((Micro1_8 & EMERGENCY) == EMERGENCY)
		{
			strncpy(&LCD_DataBuff[FAIL_CODE_POS], &mEMERGENCY_PRESSED, 2U);
		}
		else if (gOrionConfVars.Remote_INH == true)
		{																						// Visualizza un asterisco dopo la scritta "Fuori Servizio"
				LCD_DataBuff[55] = '*';
				LCDdisp.AllPhase = msgMotCapsFail;
		} 
Refresh_LCD();
		Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
		Norefresh = false;																		// Set da fine vendita fail e non piu' clear in Gestione_LCD
	}
/*	else {
		// Se l'allarme che ha generato il messaggio non e' piu' attivo, passo al
		// primo guasto attualmente attivo (es guasto NoAnswer dalla I/O che poi riprende
		// a rispondere)
		if ((fVMC_NoAnswer == 0) && (LCDdisp.AllPhase = msgSchedaIOFail)) {						// Non e' piu' attivo l'allarme Scheda I/O FAIL
			strcpy(&LCD_DataBuff[GuastoCursorPos], &mSchedaIOFail);
			LCDdisp.AllPhase = msgNoAll;
		}
		
	}
*/	
}

/*------------------------------------------------------------------------------------------*\
 Method: VMC_Programmaz_Msg
	Visualizza il messaggio di programmazione VMC

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  VMC_Programmaz_Msg(void) {
	
	if (LCDdisp.Phase != LCD_VMC_Programmaz) {
		LCDdisp.Phase = LCD_VMC_Programmaz;
		LCD_Disp_Clear();
#if LCDmsg_Lingua
		ReadMessage(&LCD_DataBuff[0], Lingua, mProgrammaz, FOUR_LCD_NUMCOL);
#else		
		strcpy(LCD_DataBuff, &mProgrammaz);
#endif	
		Refresh_LCD();
		Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_ConfigUpdate
	Visualizza il messaggio di aggiornamento configurazione

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_ConfigUpdate(void)
{
	if (LCDdisp.Phase != LCD_VMC_NewConfig)
	{
		LCDdisp.Phase = LCD_VMC_NewConfig;
		LCD_Disp_Clear();
		if (gOrionConfVars.LCD_Type == LCD_2x16)
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mNewConfig_2x16, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mNewConfig_2x16);
	#endif	
		}
		else
		{
	#if LCDmsg_Lingua
			ReadMessage(&LCD_DataBuff[0], Lingua, mNewConfig, FOUR_LCD_NUMCOL);
	#else		
			strcpy(LCD_DataBuff, &mNewConfig);
	#endif	
		}
		Refresh_LCD();
		Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_CollaudoInCorso

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_CollaudoInCorso(void)
{
	LCD_Disp_Clear();
	strcpy(LCD_DataBuff, &mCollInCorso);
	Refresh_LCD();
	Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_ReadConfDaChiaveUSB

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_ReadConfDaChiaveUSB(void)
{
	LCD_Disp_Clear();
	strcpy(LCD_DataBuff, &mRadConfig);
	Refresh_LCD();
	Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_PrelievoIrdaLocale

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_PrelievoIrdaLocale(void)
{
	LCD_Disp_Clear();
	strcpy(LCD_DataBuff, &mDownloadIrda);
	Refresh_LCD();
	Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_AzzeramMemoria

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_AzzeramMemoria(void)
{
	LCD_Disp_Clear();
	strcpy(LCD_DataBuff, &mClearMemory);
	Refresh_LCD();
	Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_Disp_EndColl_OK

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_Disp_EndColl_OK(void)
{
	LCD_Disp_Clear();
	strcpy(LCD_DataBuff, &mEndCollaudo_OK);
	Refresh_LCD();
	Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
}

/*------------------------------------------------------------------------------------------*\
 Method: VMC_USB_KEY_Msg
	Visualizza il messaggio quando introdotta chiave USB

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  VMC_USB_KEY_Msg(void)
{
	if (LCDdisp.Phase != LCD_USB_KEY_IN)
	{
		LCDdisp.Phase = LCD_USB_KEY_IN;
		LCD_Disp_Clear();
#if LCDmsg_Lingua
		ReadMessage(&LCD_DataBuff[0], Lingua, mUSB_KEY, FOUR_LCD_NUMCOL);
#else		
		if (gOrionConfVars.LCD_Type == LCD_4x20)
		{
			strcpy(LCD_DataBuff, &mUSB_KEY);
		}
		else
		{
			strcpy(LCD_DataBuff, &mUSB_KEY_2x16);
		}
#endif	
		Refresh_LCD();
		Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: VMC_USB_ErrKeyRilev
	Visualizza il messaggio di errore quando introdotta una chiave USB il cui rilevamento
	e' fallito.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void VMC_USB_ErrKeyRilev(void)
{
	if (LCDdisp.Phase != LCD_USB_KEY_IN_ERR)
	{
		LCDdisp.Phase = LCD_USB_KEY_IN_ERR;
		LCD_Disp_Clear();
#if LCDmsg_Lingua
		ReadMessage(&LCD_DataBuff[0], Lingua, mUSB_KEY_Withdraw, FOUR_LCD_NUMCOL);
#else		
		strcpy(LCD_DataBuff, &mUSB_KEY_Withdraw_2x16);
#endif	
		Refresh_LCD();
		Tmr32Start(DispLCDTouts.TimeDispMsg, TMSG_ENDLESS);
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_ClearAllarmMessage
	Predispone per la normale visualizzazione del DA.
	Utilizzata per disattivare i messaggi di allarme temporanei.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_ClearAllarmMessage(void) {

	if (LCDdisp.Phase == LCD_VMC_FAIL) {
		LCDdisp.AllPhase = msgNoAll;
		LCDdisp.ForzaVisCred = true;
		DispLCDTouts.TimeDispMsg.tmrEnd = 0;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_ForzaNormalVisual
	Riporta in "LCDNormalOper" lo stato del display.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  LCD_ForzaNormalVisual(void) {

	LCDdisp.Phase = LCDNormalOper;
}

/*------------------------------------------------------------------------------------------*\
 Method: LCD_SetCursor
	Posiziona il cursore sul carattere desiderato
	
   IN:  - Posizione del cursore
  OUT:  - 

// ********************************************************
// Valori Hex da inviare come comandi al display LCD  per * 
// posizionare il cursore sulla riga e sul chr desiderato *
//                                                        *                           
//   Riga       Hex          Offset decimale              *
// Riga 1: 0x80 ---  0x93       0  ---  19                *
// Riga 2: 0xC0 ---  0xD3      64  ---  83                *
// Riga 3: 0x94 ---  0xA7      20  ---  39                *
// Riga 4: 0xD4 ---  0xE7      84  ---  103               *
// ********************************************************

\*------------------------------------------------------------------------------------------*/
void  LCD_SetCursor(uint8_t position)
{
	SendCommand_LCD(position);
}

/*------------------------------------------------------------------------------------------*\
 Method: IsLCD_NoInitMsg
	Funzione che controlla se il display LCD ha terminato la visualizzazione delle rev fw
	all'accensione. 
	
   IN:  - 
  OUT:  - true se display LCD ha terminato visualizzazione msg di Init
\*------------------------------------------------------------------------------------------*/
bool  IsLCD_NoInitMsg(void)
{
	return ((LCDdisp.Phase != LCD_OutInit)? true : false);
}

/*------------------------------------------------------------------------------------------*\
 Method: IsLCD_SelNonDisponibileMsg
	Funzione che controlla se il display LCD ha terminato la visualizzazione del messaggio
	di Selezione Non disponibile. 
	
   IN:  - 
  OUT:  - true se display LCD sta visualizzando msg Selezione Non disponibile
\*------------------------------------------------------------------------------------------*/
bool  IsLCD_SelNonDisponibileMsg(void)
{
	return ((LCDdisp.Phase == LCDNoSel_Msg)? true : false);
}

/*------------------------------------------------------------------------------------------*\
 Method: IsLCD_PrelevaProdottoMsg
	Funzione che controlla se il display LCD ha terminato la visualizzazione del messaggio
	"Preleva prodotto"
	
   IN:  - 
  OUT:  - true se display LCD sta visualizzando msg "Preleva prodotto"
\*------------------------------------------------------------------------------------------*/
bool  IsLCD_PrelevaProdottoMsg(void) 
{
	return ((LCDdisp.Phase == LCDPrelevare_Msg)? true : false);
}


/*------------------------------------------------------------------------------------------*\
 Method: ReadMessagesFromEE
	Legge eventuli messaggi programmabiti in EE come parametri di configurazione.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  ReadMessagesFromEE(void)
{
	ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow1), (uint8_t *)(&LCD_MsgRow1), LEN_LCDMSG);
	if ((LCD_MsgRow1[0] < FirstASCII_Code) || (LCD_MsgRow1[0] > LastASCII_Code))
	{
		sprintf(&LCD_MsgRow1[0], "Erogaz 1            ");
	}
	ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow2), (uint8_t *)(&LCD_MsgRow2), LEN_LCDMSG);
	if ((LCD_MsgRow2[0] < FirstASCII_Code) || (LCD_MsgRow2[0] > LastASCII_Code))
	{
		sprintf(&LCD_MsgRow2[0], "Erogaz 2            ");
	}
	ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow3), (uint8_t *)(&LCD_MsgRow3), LEN_LCDMSG);
	if ((LCD_MsgRow3[0] < FirstASCII_Code) || (LCD_MsgRow3[0] > LastASCII_Code))
	{
		sprintf(&LCD_MsgRow3[0], "Erogaz 3            ");
	}
	ReadEEPI2C(IICEEPConfigAddr(EE_MsgRow4), (uint8_t *)(&LCD_MsgRow4), LEN_LCDMSG);
	if ((LCD_MsgRow4[0] < FirstASCII_Code) || (LCD_MsgRow4[0] > LastASCII_Code))
	{
		sprintf(&LCD_MsgRow4[0], "Erogaz 4            ");
	}
	LCDdisp.NewMsgInEE = false;
}





