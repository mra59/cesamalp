/*--------------------------------------------------------------------------------------*\
File:
	ICPBILLHook.h

Description:
    Dichiarazione strutture e routines file ICPBILLHook

History:
	Date	   Aut	Note
    Giu 2019	MR   
\*--------------------------------------------------------------------------------------*/

#ifndef __ICPBILLHook_H_
#define __ICPBILLHook_H_



#define BILL_EN 1
#define BILL_DIS 2


void ICPBillHookEnDis(void);
void CheckBillCashType(void);
void ICPBillHookTaskInit(void);
void Bill_Task(void);
void ICPBILLHook_SetReady(bool avail);
void ICPBILLHook_SetBillsDeposited(ET_BILLPOLLDEST billDest, uint8_t billType, CreditCoinValue billValue);
void ICPBILLHook_SetDPP(uint8_t nDPP);
void ICPBILLHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen);
void ICPBILLHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPBILLHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPBILLHook_SetSWVersion(uint16_t nSwVersion);
void  DisattivaEnDisBill(void);

/*--------------------------------------------------------------------------------------*\
Unused ICP protocol BILL hooks  
\*--------------------------------------------------------------------------------------*/
void ICPBILLHook_SetCountryCode(uint16_t nCountryCode);
void ICPBILLHook_SetCurrency(uint16_t nCurrency);
void ICPBILLHook_SetFTL(bool fAvailable);
bool ICPBILLHook_GetFTL(void);
void ICPBILLHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen);

void ICPBILLHook_SetEscrow(bool fEscrow);
void ICPBILLHook_SetStackerFull(bool fStackerFull);

#endif	// End ICPBILLHook

