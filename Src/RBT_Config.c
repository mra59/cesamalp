/****************************************************************************************
File:
    RBT_Config.c

Description:
	Funzioni di importazione nuova configurazione da file memorizzato in EEprom 
	esterna e, su richiesta, memorizzazione del file contenente la configurazione 
	in EEprom esterna.  

History:
    Date       Aut  Note
    Giu 2013	MR-AB   

*****************************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>

#include "ORION.H" 
#include "IICEEP.h"
#include "Sel24.h"
#include "RBT_Config.h"
#include "I2C_EEPROM.h"
#include "Funzioni.h"
#include "general_utility.h"
#include "VMC_CPU_HW.h"
#include "VMC_CPU_LCD.h"
#include "IoT_Comm.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void  		LCD_Disp_ConfigUpdate(void);
extern	const char	gSWRevCode[];
extern	void  		Rd_EEBuffStartAddr_And_BankSelect(uint8_t TipoBuff);




/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

static uint8_t		Found_Zero, banco;
static uint16_t		StringLen, EEAddr;


/*---------------------------------------------------------------------------------------------*\
Method: CheckNewConfig
	Routine che legge dalla EEPROM il buffer "RxConfigBuff" e, se presenti, memorizza nuovi
	parametri di funzionamento in EEProm.

	IN:		- RxConfigBuff con file di configurazione prelevato da chiave USB
	OUT:	- Resp con esito aggiornamento
\*----------------------------------------------------------------------------------------------*/
uint8_t	CheckNewConfig(void) {
	
	bool		FirstByteNoZero = false;
	uint8_t		Resp;
	uint16_t	NumParam = 0;																	// Numero di parametri aggiornati in EEprom

	Resp = RD_WR_CFG_OK;
	Found_Zero = false;
	GetRxConfigBuffAddress();																	// In EEAddr start address RxConfigBuff e banco
	
	do {
		if (banco > 0) BankSelect(banco);
		memset(&gVars.ddcmpbuff[0], 0, 64);														// Azzeramento per evitare "residui" nelle stringhe SSID e WPSW
		StringLen = GetStringFromEEprom(&EEAddr);												// L'update config termina quando incontro il chr NULL nel buffer in EEProm
		BankSelect(0x00);
		if (StringLen == 0) break;																// Non ci sono altri dati in EEPROM
		FirstByteNoZero = true;																	// Il primo byte del buffer RxConfig non e' zero
		LCD_Disp_ConfigUpdate();																// Visualizzo messaggio di attesa per aggiornamento configurazione
		EEAddr += StringLen + 1;																// Address next record: sommo uno perche' StringLen esce puntando all'ultimo dei due chr crlf
		if (StringLen > 1) StringLen -= 2;														// Se la stringa non e' solo CRLF tolgo gli ultimi 2 chr letti che dovrebbero essere crlf
		if (StringLen >= 5U)																	// Se Record troppo corto lo scarto
		{
			Resp = write_new_param(&gVars.ddcmpbuff[0], StringLen);	
			if (Resp == RD_WR_CFG_OK)
			{
				NumParam ++;
				RedLedCPU(ON);																	// Led Rosso CPU ON
			} else {
				if ((Resp == WrCfg_WriteFail) || (Resp == WrCfg_Incomplete)) {
					BlinkErrRedLed_HALT(Milli500, ErrWriteEE_Conf_Param);						// Errore scrittura parametro: lampeggia rosso e mi fermo
				}
			}
		}
		if (Found_Zero)
		{																						// Trovato il chr zero nel buffer
			StringLen = 0;
		}
	} while (StringLen != 0);

	if (NumParam == 0)
	{																							// Non c'era alcuna nuova configurazione valida
		if (FirstByteNoZero == true)
		{																						// Il primo byte di RxConfig non e' zero: lo azzero per non visualizzare ad ogni  
			GetRxConfigBuffAddress();															// accensione il msg "Attendere fine programmazione" anche se non c'e' nulla
			if (banco > 0) BankSelect(banco);
			gVars.ddcmpbuff[0] = NULL_VALUE;
			WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), 1);
			BankSelect(0x00);
		}
		return (Resp = NULL);																	
	}
	else
	{
		RedLedCPU(OFF);																			// Led Rosso CPU OFF
		return (Resp = UpdateCfg_OK);															// Crea in EEPROM il file Conf per essere prelevato con chiave USB
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: SendConfToEEprom
	Funzione che crea in EEPROM un buffer ASCII contenente tutti gli ID gestiti dal prodotto e
	relativi valori perche' possa essere prelevato tramite chiave USB.

	IN:	 - opzione per aggiornare solo i primi 2 param del file (MODM e SNXX)
	OUT: - Resp con esito aggiornamento
\*----------------------------------------------------------------------------------------------*/
uint8_t	SendConfToEEprom(uint8_t Solo_SW_Rev)
{
#define	DEST_BUF_SIZE	128

  	uint8_t				LoopIndex, resp;
	uint16_t			i, NumLoop, LenghtCnt, LastByteAddr, ee_add, len;
	uint32_t			paramVal;
	unsigned char		*pntMsgIx;

	RedLedCPU(ON);																				// Led Rosso CPU ON
					
	// Determino address inizio Buffer EEProm dove memorizzare il file config				
	Rd_EEBuffStartAddr_And_BankSelect(BUF_TXCONFIG);											// In MemoryBytes[] StartAddress del buffer e bank della EE gia' selezionato
	EEAddr = 0;				
	EEAddr += MemoryBytes[1];																	// LSB
	EEAddr += (MemoryBytes[0]<<8);																// MSB
	LastByteAddr = EEAddr + (EE_TXCONFBUF_LEN - 1);												// Address fine buffer per registrare chr NULL se si sfora il buffer in EE
	LenghtCnt = 0;																				// Counter bytes per controllare Limite buffer Tx_Config in EE
	
//*************************************************************************
//**********  Memo Tipo Prodotto  e   Rev Fw                ***************
//*************************************************************************
//	Questa parte della funzione SendConfToEEprom puo' essere chiamata dal
//	Reset per aggiornare i soli primi 2 ID del buffer TxConfig gia' aggiornato
//	dopo l'ultima ricezione di una nuova configurazione: e' importante,
//	quindi, che la lunghezza sia fissa.
//	Per questo il primo parametro OpMode e' sempre trasformato in dato a
//	3 bytes e il secondo SWRV e' sempre di 4 bytes. 

	
/* //MR21 Tolto dalla configurazione Ecoline
	// PROT = Tipo di Prodotto				
	memset(&gVars.ddcmpbuff[0], 0, DEST_BUF_SIZE);
	snprintf((char*)&gVars.ddcmpbuff[0], 20U, PROT_ID, OpMode);									// "PROT*%3u\r\n",
	len = strlen((char const*)&gVars.ddcmpbuff[0]);
	WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);							// scrittura buffer EEProm
	EEAddr += len;																				// Address next record
	LenghtCnt += len;				
*/
	
	// SWRV = Revisione Firmware Prodotto				
	memset(&gVars.ddcmpbuff[0], 0, DEST_BUF_SIZE);
	snprintf((char*)&gVars.ddcmpbuff[0], 20U, SWRV_ID, kProductSWVersionStr);					// "SWRV*%4s\r\n",
	len = strlen((char const*)&gVars.ddcmpbuff[0]);
	WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t)len);							// scrittura buffer EEProm
	EEAddr += len;																				// Address next record
	LenghtCnt += len;				
	if (Solo_SW_Rev) {																			// Aggiornamento delle sole revisioni sw completato.
		EEAddr = LastByteAddr;																	// Punto comunque all'ultimo uint8_t del buffer EE per memorizzare il chr NULL di chiusura perche'
		gVars.ddcmpbuff[0] = NULL_VALUE;														// potrebbe essere ancora tutto a 0xff se non e' stato fatto alcun azzeramento
		WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), 1);									// Memo fine file (chr NULL)
		RedLedCPU(OFF);																			// Led Rosso CPU OFF
		return resp = 0;				
	}
	
	//*************************************************************************
	//******* Memorizzazione ID   CFG_PARAM                              ******
	//*************************************************************************

	pntMsgIx = (unsigned char *)&CFG_PARAM[0];												// Punto al primo elemento dell'array CFG_PARAM
	do {
		memset(&gVars.ddcmpbuff[0], 0, DEST_BUF_SIZE);
		if (pntMsgIx[0] == NULL) break;														// Chr NULL, non ci sono altri elementi nell'array
  		for(i=0; i < ID_PLUS_ASTERISC_LEN; i++)
		{
			gVars.ddcmpbuff[i] = pntMsgIx[i];
		}
		gVars.ddcmpbuff[i] = NULL;
		LoopIndex = (uint8_t) ((pntMsgIx - (unsigned char *)&CFG_PARAM[0]) / ID_PLUS_ASTERISC_LEN);	// LoopIndex as offset all'array CFG_PARAM da usare nella RW_ConfParam
		pntMsgIx += ID_PLUS_ASTERISC_LEN;
		len = strlen((char const*)&gVars.ddcmpbuff[0]);

#if (IOT_PRESENCE == true)
		// -- SSID Wi-Fi (Alfanumerico 1-32) --			
		if ((strstr((char*)&gVars.ddcmpbuff[0], "SSID") != NULL))							// Ricerca stringa "SSID"
		{
			ee_add = IICEEPConfigAddr(EESSID);												// Address primo byte SSID in EE
			ReadEEPI2C(ee_add, (uint8_t*)(&gVars.ddcmpbuff[len]), LEN_SSID);				// Leggo SSID dalla EEPROM e lo memorizzo in gVars.ddcmpbuff
			len = strlen((char const*)&gVars.ddcmpbuff[0]);
			gVars.ddcmpbuff[len++] = ASCII_CR;			
			gVars.ddcmpbuff[len++] = ASCII_LF;			
			if ((LenghtCnt+len) >= EE_TXCONFBUF_LEN) {										// Raggiungo o supero limite Buffer Audit in EE
				EEAddr = LastByteAddr;														// Punto all'ultimo byte del buffer EE per memorizzare il chr NULL di chiusura
				break;			
			} else {			
				WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);			// scrittura buffer EEProm
				EEAddr += len;																// Address next record
				LenghtCnt += len;			
			}
			//goto Next_ID;			
		}
		// -- WPSW: Password SSID Wi-Fi (Alfanumerico 1-32) --			
		else if ((strstr((char*)&gVars.ddcmpbuff[0], "WPSW") != NULL))						// Ricerca stringa "WPSW"
		{
			ee_add = IICEEPConfigAddr(EE_Psw_SSID);											// Address primo byte EE_Psw_SSID in EE
			ReadEEPI2C(ee_add, (uint8_t*)(&gVars.ddcmpbuff[len]), LEN_PSW_SSID);			// Leggo stringa WPSW dalla EEPROM e lo memorizzo in gVars.ddcmpbuff
			len = strlen((char const*)&gVars.ddcmpbuff[0]);
			gVars.ddcmpbuff[len++] = ASCII_CR;			
			gVars.ddcmpbuff[len++] = ASCII_LF;			
			if ((LenghtCnt+len) >= EE_TXCONFBUF_LEN) {										// Raggiungo o supero limite Buffer Audit in EE
				EEAddr = LastByteAddr;														// Punto all'ultimo byte del buffer EE per memorizzare il chr NULL di chiusura
				break;			
			} else {			
				WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);			// scrittura buffer EEProm
				EEAddr += len;																// Address next record
				LenghtCnt += len;			
			}			
			//goto Next_ID;			
		}
		// -- Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20) --			
		else if ((strstr((char*)&gVars.ddcmpbuff[0], "MODM") != NULL))						// Ricerca stringa "MODM"
#else
		// -- Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20) --			
		if ((strstr((char*)&gVars.ddcmpbuff[0], "MODM") != NULL))							// Ricerca stringa "MODM"
#endif
		{
			ee_add = IICEEPConfigAddr(EEModelloMacchina);									// Address primo byte Modello Macchina in EE
			ReadEEPI2C(ee_add, (uint8_t*)(&gVars.ddcmpbuff[len]), Len_ID102);				// Leggo Modello Macchina dalla EEPROM e lo memorizzo in gVars.ddcmpbuff
			len = strlen((char const*)&gVars.ddcmpbuff[0]);
			gVars.ddcmpbuff[len++] = ASCII_CR;			
			gVars.ddcmpbuff[len++] = ASCII_LF;			
			if ((LenghtCnt+len) >= EE_TXCONFBUF_LEN) {										// Raggiungo o supero limite Buffer Audit in EE
				EEAddr = LastByteAddr;														// Punto all'ultimo byte del buffer EE per memorizzare il chr NULL di chiusura
				break;			
			} else {			
				WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);			// scrittura buffer EEProm
				EEAddr += len;																// Address next record
				LenghtCnt += len;			
			}			
		}
		// -- Serial Number Produttore ID105 EVA-DTS (User Define Field Alfanum. 1-12) --			
  		else if ((strstr((char*)&gVars.ddcmpbuff[0], "SNXX") != NULL))						// Ricerca stringa "SNXX"
		{
			ee_add = IICEEPSNProduttAddr(EE_SN_Produttore);									// Address primo byte Serial Number Produttore in EE
			ReadEEPI2C(ee_add, (uint8_t*)(&gVars.ddcmpbuff[len]), Len_ID105);				// Leggo Serial Number Produttore dalla EEPROM
			len = strlen((char const*)&gVars.ddcmpbuff[0]);
			gVars.ddcmpbuff[len++] = ASCII_CR;			
			gVars.ddcmpbuff[len++] = ASCII_LF;			
			if ((LenghtCnt+len) >= EE_TXCONFBUF_LEN) {										// Raggiungo o supero limite Buffer Audit in EE
				EEAddr = LastByteAddr;														// Punto all'ultimo byte del buffer EE per memorizzare il chr NULL di chiusura
				break;			
			} else {			
				WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);			// scrittura buffer EEProm
				EEAddr += len;																// Address next record
				LenghtCnt += len;			
			}			
		}
		// -- Messaggio programmabile da mettere sul display LCD --			
  		else if ((strstr((char*)&gVars.ddcmpbuff[0], "MS") != NULL))						// Ricerca stringa "MS"
		{
			resp = gVars.ddcmpbuff[2];														// Trasformo i due caratteri numerici da ASCII a Hex per usarli come offset address in EE
			i = (ascii_to_hex_num(resp) << 4);
			resp = gVars.ddcmpbuff[3];
			i |= ascii_to_hex_num(resp);
			ee_add = IICEEPConfigAddr(EE_MsgRow1);											// Determino address EE_MsgRow da leggere
			ee_add += (i * LEN_LCDMSG);
			ReadEEPI2C(ee_add, (uint8_t*)(&gVars.ddcmpbuff[len]), LEN_LCDMSG);				// Leggo Serial Number Produttore dalla EEPROM
			len = strlen((char const*)&gVars.ddcmpbuff[0]);
			gVars.ddcmpbuff[len++] = ASCII_CR;			
			gVars.ddcmpbuff[len++] = ASCII_LF;			
			if ((LenghtCnt+len) >= EE_TXCONFBUF_LEN) {										// Raggiungo o supero limite Buffer Audit in EE
				EEAddr = LastByteAddr;														// Punto all'ultimo byte del buffer EE per memorizzare il chr NULL di chiusura
				break;			
			} else {			
				WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);			// scrittura buffer EEProm
				EEAddr += len;																// Address next record
				LenghtCnt += len;			
			}			
		}
		else
		{
			resp = RW_ConfParam((PAR_ID)LoopIndex, &paramVal, EE_READ, 0);					// Lettura del valore Hex del parametro dalla EEProm identificato da LoopIndex
			if (resp == RD_WR_CFG_OK)
			{			
				sprintf((char*)&gVars.ddcmpbuff[len],"%1u\r\n",paramVal);					// Memorizza una stringa ASCII con formato "ABCD*" + Dato + crlf + NULL
				len = strlen((char const*)&gVars.ddcmpbuff[0]);
			}
			else
			{
				gVars.ddcmpbuff[len++] = ASCII_CR;											// ID non trovato nell'array CFG_PARAM: memo solo CRLF
				gVars.ddcmpbuff[len++] = ASCII_LF;			
			}
			if ((LenghtCnt+len) >= EE_TXCONFBUF_LEN) {										// Raggiungo o supero limite Buffer Audit in EE
				EEAddr = LastByteAddr;														// Punto all'ultimo byte del buffer EE per memorizzare il chr NULL di chiusura
				break;			
			} else {			
				WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);			// scrittura buffer EEProm
				EEAddr += len;																// Address next record
				LenghtCnt += len;
			}
		}
//Next_ID:		
	} while (true);

	//*************************************************************************
	//******* Memorizzazione Loop ID CFG_PAR_LOOP                        ******
	//*************************************************************************
	if (EEAddr == LastByteAddr) goto Fine_SendConfToEEprom;										// Buffer EE Full
	pntMsgIx = (unsigned char *)&CFG_PAR_LOOP[0];												// POINTER all'array di loop CFG_PAR_LOOP

	do {
			memset(&gVars.ddcmpbuff[0], 0, DEST_BUF_SIZE);
			if (pntMsgIx[0] == NULL) break;														// Chr NULL, non ci sono altri elementi nell'array
	  		for(i=0; i < ID_PLUS_ASTERISC_LEN; i++)
			{
				gVars.ddcmpbuff[i] = pntMsgIx[i];
			}
			gVars.ddcmpbuff[i] = NULL;
			LoopIndex = (uint8_t) ((pntMsgIx - (unsigned char *)&CFG_PAR_LOOP[0]) / ID_PLUS_ASTERISC_LEN);	// LoopIndex as offset all'array CFG_PAR_LOOP da usare nella RW_ConfParam
			NumLoop = idLoopLen[LoopIndex];															// In NumLoop il numero di record da inviare per l'ID in elaborazione
			pntMsgIx += ID_PLUS_ASTERISC_LEN;

			// Loop interno che scandisce l'ID della tabella "idLoop"				
			// tante volte quanto indicato in "idLoopLen"				
			LoopIndex += ID_LOOP_ENUM_START;
			for (i=0; i < NumLoop; i++)																// Loop di generazione degli elementi dell'indice prelevato da idLoop
			{ 
				if (EEAddr == LastByteAddr) goto Fine_SendConfToEEprom;								// Buffer EE Full
				resp = BinToBcd(i);
				gVars.ddcmpbuff[2] = (uint8_t)(hex_to_ascii(resp >> 4));							// Ennesimo elemento del loop (01-99) in ASCII 
				gVars.ddcmpbuff[3] = (uint8_t)(hex_to_ascii(resp));
				resp = RW_ConfParam((PAR_ID)LoopIndex, &paramVal, EE_READ, i);						// Lettura del valore Hex dell'ennesimo parametro dell'ID prelevato da idLoop
				if (resp == RD_WR_CFG_OK)
				{				
					sprintf((char*)&gVars.ddcmpbuff[ID_PLUS_ASTERISC_LEN],"%1u\r\n",paramVal);		// Memorizza una stringa ASCII con formato "ABCD*" + Dato + crlf + NULL
					len = strlen((char const*)&gVars.ddcmpbuff[0]);
					if ((LenghtCnt + len) >= EE_TXCONFBUF_LEN)
					{																				// Raggiungo o supero limite Buffer Audit in EE
					  	EEAddr = LastByteAddr;														// Punto all'ultimo byte del buffer EE per memorizzare il chr NULL di chiusura
					}
					else
					{			
						WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) len);			// scrittura buffer EEProm
						EEAddr += len;																// Address next record
						LenghtCnt += len;
					}
				}
			} 
		} while(true);  
  
//=================================================================================
	
Fine_SendConfToEEprom:
	gVars.ddcmpbuff[0] = NULL_VALUE;
	WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), 1U);										// Memo fine file (chr NULL)
				
	RedLedCPU(OFF);																				// Led Rosso CPU OFF
	GetRxConfigBuffAddress();																	// In EEAddr start address RxConfigBuff e banco
	if (banco > 0) BankSelect(banco);

#if (DEB_NO_CLR_NEW_CONFIG == true)	
	BankSelect(0x00);
	return resp;
#else	
	WriteEEPI2C(EEAddr, (uint8_t *)(&gVars.ddcmpbuff), 1U);										// Azzero la Config ricevuta solo al termine della SendConfToEEprom  per evitare che uno
	
#if AWS_CONFIG	
	//-- Azzero primo byte buffers AWS ---
	ClrConfigAWS(BUF_AWS_SECURE_CERTIF);
	ClrConfigAWS(BUF_AWS_PRIVATE_KEY);
	ClrConfigAWS(BUF_AWS_ACCOUNT_SERVER_ADDRESS);
	ClrConfigAWS(BUF_AWS_DEVICE);
#endif
	
	BankSelect(0x00);																			// spegnimento prematuro interrompa la scrittura di tutta la config in eeprom
	return resp;
#endif
}


/*---------------------------------------------------------------------------------------------*\
Method: write_new_param
	Riceve un buffer ASCII con formato "ABCD*Dato" e memorizza in EEPROM il valore Hex del 
	parametro di configurazione identificato dall'ID "ABCD".
	Controlla anche quei parametri il cui valore non e' Hex.

	IN:		- Address del buffer ASCII contenente la stringa da decodificare e sua lunghezza
	OUT:	- true se parametro aggiornato
\*----------------------------------------------------------------------------------------------*/
uint8_t	write_new_param(uint8_t *SourceBuff, uint8_t len)
{
	uint8_t			i, j, response, ArrayIx, ArrayOffset;
	uint16_t		ee_add, Len;
	uint32_t		hexdato;
	char 			SourceID[6], LocalBuff[21];
	char 			*pnt;

	// Controllo se il dato e' "MODM", cioe' Modello Macchina (ID102 EVA-DTS)
	if (SourceBuff[0] == 'M' && SourceBuff[1] == 'O' && SourceBuff[2] == 'D' && SourceBuff[3] == 'M')			// Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20)
	{
	  	if (SourceBuff[4] == ASCII_STAR)																		// Il quarto chr e' '*'
		{
			i = strlen((char*)SourceBuff);
			if (i > (ID_LEN+ASCIISTARLEN+NEWLINELEN))
			{
				i -= (ID_LEN+ASCIISTARLEN+NEWLINELEN);														// Lunghezza stringa del codice ID102 ricevuto
				j = (ID_LEN + ASCIISTARLEN + i);																// Addr primo chr non usato dei 20 chr di ID102
				for (; j < (Len_ID102+ID_LEN+ASCIISTARLEN); j++)
				{
					*(SourceBuff + j) = ASCII_SPACE;
				}
				ee_add = IICEEPConfigAddr(EEModelloMacchina);													// Address primo byte Modello Macchina in EE
				WriteEEPI2C(ee_add, (uint8_t *)(SourceBuff+(ID_LEN+ASCIISTARLEN)), Len_ID102);
				return RD_WR_CFG_OK;
			}
		}
	}
	// Controllo se il dato e' "SNXX", cioe' il Serial Number Costruttore (ID105 EVA-DTS)
	if (SourceBuff[0] == 'S' && SourceBuff[1] == 'N' && SourceBuff[2] == 'X' && SourceBuff[3] == 'X')			// Serial Number Costruttore ID105 EVA-DTS (Alfanumerico 1-12)
	{
		if (SourceBuff[4] == ASCII_STAR)																		// Il quarto chr e' '*'
		{	
		  	i = strlen((char*)SourceBuff);
			if (i > (ID_LEN+ASCIISTARLEN+NEWLINELEN)) {
				i -= (ID_LEN+ASCIISTARLEN+NEWLINELEN);														// Lunghezza stringa del codice ID105 ricevuto
				j = (ID_LEN + ASCIISTARLEN + i);																// Addr primo chr non usato dei 12 chr di ID105
				for (; j < (Len_ID105+ID_LEN+ASCIISTARLEN); j++)
				{
					*(SourceBuff + j) = ASCII_SPACE;
				}
				ee_add = IICEEPSNProduttAddr(EE_SN_Produttore);													// Address primo byte Serial Number Costruttore in EE
				WriteEEPI2C(ee_add, (uint8_t *)(SourceBuff+(ID_LEN+ASCIISTARLEN)), Len_ID105);
				return RD_WR_CFG_OK;
			}
		}
	}
	// Controllo se il dato e' "MSXX", cioe' il Messaggio programmabile da mettere sul display LCD
	if ((SourceBuff[0] == 'M') && (SourceBuff[1] == 'S') && (SourceBuff[4] == ASCII_STAR))
	{
		if ( (is_ascii_num(SourceBuff[2])) && (is_ascii_num(SourceBuff[3])) )
		{
			ArrayOffset = ((ascii_to_hex_num(SourceBuff[2]) << 4) | (ascii_to_hex_num(SourceBuff[3])));			// Indice a EE_MsgRow in ArrayOffset
			if (ArrayOffset <= NUM_MAX_LCD_MSG)
			{
				i = ID_LEN + ASCIISTARLEN;																	// i as pointer al messaggio
				memset(&LocalBuff, ASCII_SPACE, sizeof(LocalBuff));
				for (j=0; j < LEN_LCDMSG; j++)
				{
					if (is_ascii_printable(*(SourceBuff + i)) == true)											// Trasferisco il messaggio in LocalBuff fino al primo chr non stampabile
					{
						LocalBuff[j] = *(SourceBuff + i);
						i++;
					}
				}
				ee_add = IICEEPConfigAddr(EE_MsgRow1);															// Determino address EE_MsgRow da aggiornare
				ee_add += (ArrayOffset * LEN_LCDMSG);
				WriteEEPI2C(ee_add, (uint8_t *)(LocalBuff), LEN_LCDMSG);
				ForceLCD_NewMsgEE();																			// Aggiorna il buffer display
				return RD_WR_CFG_OK;
			}
		}
	}

#if (IOT_PRESENCE == true)
	// Controllo se il dato e' "SSID"
	if (SourceBuff[0] == 'S' && SourceBuff[1] == 'S' && SourceBuff[2] == 'I' && SourceBuff[3] == 'D')			// SSID Wi-Fi (Alfanumerico 1-32)
	{
	  	if (SourceBuff[4] == ASCII_STAR)																		// Il quarto chr e' '*'
		{
			//-- Memo 32 bytes azzerando quelli non utilizzati --
			if (len > (ID_LEN+ASCIISTARLEN+NEWLINELEN))
			{
				Len = (uint8_t)strlen((char*)SourceBuff);
				for (j = (LEN_SSID + ID_LEN + ASCIISTARLEN - 1); j > (Len - (NEWLINELEN + 1)); j--)				// Azzera tutti i residui bytes partendo da NEWLINE
				{
					*(SourceBuff + j) = 0U;
				}
				ee_add = IICEEPConfigAddr(EESSID);																// Address primo byte SSID in EE
				WriteEEPI2C(ee_add, (uint8_t *)(SourceBuff+(ID_LEN+ASCIISTARLEN)), LEN_SSID);
				return RD_WR_CFG_OK;
			}
		}
	}
	// Controllo se il dato e' "WPSW"
	if (SourceBuff[0] == 'W' && SourceBuff[1] == 'P' && SourceBuff[2] == 'S' && SourceBuff[3] == 'W')			// Password SSID Wi-Fi (Alfanumerico 1-32)
	{
	  	if (SourceBuff[4] == ASCII_STAR)																		// Il quarto chr e' '*'
		{
			//-- Memo 32 bytes azzerando quelli non utilizzati --
			if (len > (ID_LEN+ASCIISTARLEN+NEWLINELEN))
			{
				Len = (uint8_t)strlen((char*)SourceBuff);
				for (j = (LEN_PSW_SSID + ID_LEN + ASCIISTARLEN - 1); j > (Len - (NEWLINELEN + 1)); j--)			// Azzera tutti i residui bytes partendo da NEWLINE
				{
					*(SourceBuff + j) = 0U;
				}
				ee_add = IICEEPConfigAddr(EE_Psw_SSID);															// Address primo byte WPSW in EE
				WriteEEPI2C(ee_add, (uint8_t *)(SourceBuff+(ID_LEN+ASCIISTARLEN)), LEN_PSW_SSID);
				return RD_WR_CFG_OK;
			}
		}
	}
#endif	
	
	// Converto parametro da ASCII a Hex in hexdato
	for (i=ID_LEN; i < len; i++)
	{
		if (SourceBuff[i] == ASCII_STAR)
		{
			hexdato = atouint32(&SourceBuff[i+1], len-i);														// Valore Hex del parametro in hexdato
			break;
		}
	}
	if (i >= len) return (response = CFG_NO_DELIMITER);															// Non trovato delimitatore "*"

	//if (SourceBuff[2] >= '0' && SourceBuff[2] <= '9' && SourceBuff[3] >= '0' && SourceBuff[3] <= '9')
	if ( (is_ascii_num(SourceBuff[2])) && (is_ascii_num(SourceBuff[3])) )
	{
		// -- Il Param e' un elemento di loop, estraggo ArrayOffset e     --
	  	// -- azzero i due bytes numerici nell'ID per metterli in ArrayIx --
		ArrayOffset = ((ascii_num_to_BCD(SourceBuff[2]) << 4) | (ascii_num_to_BCD(SourceBuff[3])));				// Numero BCD in ArrayOffset
		ArrayOffset = bcd_to_hex(ArrayOffset);																	// ArrayOffset in Hex
		SourceID[0] = SourceBuff[0];																			// Nel buffer SourceID il parametro "xx00*" per cercarlo nell'array CFG_PAR_LOOP
		SourceID[1] = SourceBuff[1];
		SourceID[2] = ASCII_ZERO;
		SourceID[3] = ASCII_ZERO;
		SourceID[4] = ASCII_STAR;
		SourceID[5] = NULL;
		pnt = strstr((const char*)&CFG_PAR_LOOP, (char*)SourceID);												// Ricerca stringa ID prelevata dalla EE nell'array CFG_PAR_LOOP
		if (pnt == NULL) return (response = ID_NOT_FOUND);														// ID inesistente nell'array CFG_PAR_LOOP
		ArrayIx = (uint8_t)((pnt - (char *)&CFG_PAR_LOOP[0]) / ID_PLUS_ASTERISC_LEN);							// ArrayIx as indice all'enum PAR_ID da usare nella RW_ConfParam
		ArrayIx += ID_LOOP_ENUM_START;
	}
	else
	{
		// -- Cerco il Param nell'array CFG_PARAM --
	  	for(i=0; i < ID_PLUS_ASTERISC_LEN; i++)																	// Copio il solo (ID + *) nel buffer SourceID per cercarlo nell'array CFG_PARAM
		{
			SourceID[i] = SourceBuff[i];
		}
		SourceID[i] = NULL;
		pnt = strstr((const char*)&CFG_PARAM, (char*)SourceID);													// Ricerca stringa ID prelevata dalla EE nell'array CFG_PARAM
		if (pnt == NULL) return (response = ID_NOT_FOUND);
		ArrayIx = (uint8_t)((pnt - (char *)&CFG_PARAM[0]) / ID_PLUS_ASTERISC_LEN);								// ArrayIx as offset all'enum PAR_ID da usare nella RW_ConfParam
	}
	response = RW_ConfParam((PAR_ID)ArrayIx, &hexdato, EE_WRITE, ArrayOffset);									// Richiesta scrittura parametro in EEPROM
	return response;
}

/*---------------------------------------------------------------------------------------------*\
Method: GetStringFromEEprom
	La ReadString trasferisce dalla EEPROM, a partire dall'indirizzo in EEAddr, tanti caratteri 
	fino a crlf  (compresi) o a NULL.
	I byte letti sono memorizzati in "gVars.ddcmpbuff".
	
	IN:		- EEprom adress start
	OUT:	- stringa ASCII in "gVars.DDMCPBUFF" e numero caratteri.
\*----------------------------------------------------------------------------------------------*/
uint16_t  GetStringFromEEprom(uint16_t * Addr_EEP)
{
	uint16_t	i, TempEE_Addr;
	
	TempEE_Addr = *Addr_EEP;
	for (i=0; i < MAX_LEN_STRING; i++)
	{
		ReadEEPI2C((uint)(TempEE_Addr), &gVars.ddcmpbuff[i], 0x01);												// Leggo un byte dalla EEPROM
		TempEE_Addr ++;
		if (gVars.ddcmpbuff[i] == NULL)
		{
			Found_Zero = true;
			return i;
		}
		
#if (IOT_PRESENCE == true)
		if ((gVars.ddcmpbuff[i] == ASCII_n) && (gVars.ddcmpbuff[i-1] == ASCII_BS)) break;						// Nei JSON file ricevuti da AWS il NL lo rappresento con \n
#endif
		if ((gVars.ddcmpbuff[i] == ASCII_CR) && (gVars.ddcmpbuff[i-1] == ASCII_LF)) break;
		if ((gVars.ddcmpbuff[i] == ASCII_LF) && (gVars.ddcmpbuff[i-1] == ASCII_CR)) break;
	}
	return (i >= MAX_LEN_STRING) ? NULL: i;																		// Test anche per superamento di MAX_LEN_STRING senza trovare crlf
}

/*---------------------------------------------------------------------------------------------*\
Method: GetRxConfigBuffAddress
	Legge l'indirizzo di inizio e del relativo banco del  RxConfigBuffer.
	
	IN:	 - 
	OUT: - EEAddr + banco
\*----------------------------------------------------------------------------------------------*/
void  GetRxConfigBuffAddress(void)
{
	MemoryBytes[0] = (uint8_t)(RxConfigBuff >> 8);																// MSB indirizzo di inizio del Buffer RxConfig
	MemoryBytes[1] = (uint8_t)RxConfigBuff;																		// LSB
	MemoryBytes[2] = RxConfigBuffBank;
  	EEAddr = 0;
	EEAddr += MemoryBytes[1];																					// LSB
	EEAddr += (MemoryBytes[0]<<8);																				// MSB
	banco = MemoryBytes[2];
}

/*---------------------------------------------------------------------------------------------*\
Method: ReadWrite_ConfParam
	Routine per memorizzare un parametro di configurazione ricevuto o per leggere il valore di
	un parametro da inviare nel prelievo configurazione.

	IN:		- ID del parametro da elaborare, indirizzo dato da leggere/scrivere e flag Read/write
	OUT:	- esito della funzione
\*----------------------------------------------------------------------------------------------*/
static uint8_t	RW_ConfParam(PAR_ID id, uint32_t * dato, uint8_t ReadWrite, uint16_t Id_Index)
{
	uint8_t		esito, newPrice;
	uint16_t	ee_add, ee_size;
	uint32_t	ParamVal;

	newPrice = 0;
	switch (id)
	{
		case AUDP:
			ee_add = IICEEPConfigAddr(EEAuditPsw);
			ee_size = sizeof(SizeEEAuditPsw);
			break;
		case CODE:
			ee_add = IICEEPConfigAddr(EEPin2);
			ee_size = sizeof(SizeEEPin2);
			break;
		case CODM:
			ee_add = IICEEPConfigAddr(EECodiceMacchina);
			ee_size = sizeof(SizeEECodiceMacchina);
			break;
		case CRMK:
			ee_add = IICEEPConfigAddr(EECreditoMaxCarta);
			ee_size = sizeof(SizeEECreditoMaxCarta);
			break;
		case CRMM:
			ee_add = IICEEPConfigAddr(EECreditoMaxCash);
			ee_size = sizeof(SizeEECreditoMaxCash);
			break;
		case DECP:
			ee_add = IICEEPConfigAddr(EEDPP);
			ee_size = sizeof(SizeEEDPP);
			break;
		case FLAG:
			ee_add = IICEEPConfigAddr(EEPin1);
			ee_size = sizeof(SizeEEPin1);
			break;
		case GEST:
			ee_add = IICEEPConfigAddr(EEGestore);
			ee_size = sizeof(SizeEEGestore);
			break;
/*
		case LOC1:
			ee_add = IICEEPConfigAddr(EELocaz1);
			ee_size = sizeof(SizeEELocazione);
			break;
		case LOC2:
			ee_add = IICEEPConfigAddr(EELocaz2);
			ee_size = sizeof(SizeEELocazione);
			break;
		case LOC3:
			ee_add = IICEEPConfigAddr(EELocaz3);
			ee_size = sizeof(SizeEELocazione);
			break;
		case LOC4:
			ee_add = IICEEPConfigAddr(EELocaz4);
			ee_size = sizeof(SizeEELocazione);
			break;
		case LOCM:
			ee_add = IICEEPConfigAddr(EELocazione);
			ee_size = sizeof(SizeEELocazione);
			break;
		case LOCP:
			ee_add = IICEEPConfigAddr(EELocazEnable);
			ee_size = sizeof(SizeEELocazEna);
			break;
*/
		case SING:
			ee_add = IICEEPConfigAddr(EEMultiVendCard);
			ee_size = sizeof(SizeEEMultiVendCard);
			break;
		case PRHD:
			ee_add = IICEEPConfigAddr(EEPriceHolding);
			ee_size = sizeof(SizeEEPriceHolding);
			break;
		case PRDS:
			ee_add = IICEEPConfigAddr(EEPriceDisplay);
			ee_size = sizeof(SizeEEPriceDisplay);
			break;
		case SYSP:
			ee_add = IICEEPConfigAddr(EEAccessPsw);
			ee_size = sizeof(SizeEEAccessPsw);
			break;
		case UNSF:
			ee_add = IICEEPConfigAddr(EEUSF);
			ee_size = sizeof(SizeEEUSF);
			break;
		case VALU:
			ee_add = IICEEPConfigAddr(EEDescrValuta);
			ee_size = sizeof(SizeEEDescrValuta);
			break;
		case DISC:
			ee_add = IICEEPConfigAddr(EEDiscountEna);
			ee_size = sizeof(SizeEE_Discount);
			break;
/*
		case CLEV:
			ee_add = IICEEPConfigAddr(EECardLevelsEna);
			ee_size = sizeof(SizeEE_CardLevels);
			break;
		case PRSC:
			ee_add = IICEEPConfigAddr(EEListaPrezzo);
			ee_size = sizeof(SizeEE_ListaPrezzo);
			break;
		case FSIN:
			ee_add = IICEEPConfigAddr(EEFasciaSconti1Inizio);
			ee_size = sizeof(SizeEE_TimeFascia);
			break;
		case FSFI:
			ee_add = IICEEPConfigAddr(EEFasciaSconti1Fine);
			ee_size = sizeof(SizeEE_TimeFascia);
			break;
		case AOPT:
			ee_add = IICEEPConfigAddr(EEAuditOptions);
			ee_size = sizeof(SizeEEAuditOption);
			break;
		case FRCM:
			ee_add = IICEEPConfigAddr(EEFreeCrMode);
			ee_size = sizeof(SizeEEFreeCrMode);
			break;
		case FRCP:
			ee_add = IICEEPConfigAddr(EEFreeCrPeriod);
			ee_size = sizeof(SizeEEFreeCrPeriod);
			break;
		case FRVM:
			ee_add = IICEEPConfigAddr(EEFreeVendMode);
			ee_size = sizeof(SizeEEFreeVendMode);
			break;
		case FRVP:
			ee_add = IICEEPConfigAddr(EEFreeVendPeriod);
			ee_size = sizeof(SizeEEFreeVendPeriod);
			break;
		case FRC1:
			ee_add = IICEEPConfigAddr(EEFreeCred1);
			ee_size = sizeof(SizeEEFreeCred);
			break;
		case FRC2:
			ee_add = IICEEPConfigAddr(EEFreeCred2);
			ee_size = sizeof(SizeEEFreeCred);
			break;
		case FRC3:
			ee_add = IICEEPConfigAddr(EEFreeCred3);
			ee_size = sizeof(SizeEEFreeCred);
			break;
		case FRC4:
			ee_add = IICEEPConfigAddr(EEFreeCred4);
			ee_size = sizeof(SizeEEFreeCred);
			break;
		case FRC5:
			ee_add = IICEEPConfigAddr(EEFreeCred5);
			ee_size = sizeof(SizeEEFreeCred);
			break;
		case FRV1:
			ee_add = IICEEPConfigAddr(EEFreeVend1);
			ee_size = sizeof(SizeEEFreeVend);
			break;
		case FRV2:
			ee_add = IICEEPConfigAddr(EEFreeVend2);
			ee_size = sizeof(SizeEEFreeVend);
			break;
		case FRV3:
			ee_add = IICEEPConfigAddr(EEFreeVend3);
			ee_size = sizeof(SizeEEFreeVend);
			break;
		case FRV4:
			ee_add = IICEEPConfigAddr(EEFreeVend4);
			ee_size = sizeof(SizeEEFreeVend);
			break;
		case FRV5:
			ee_add = IICEEPConfigAddr(EEFreeVend5);
			ee_size = sizeof(SizeEEFreeVend);
			break;
*/
		case VEGA:
			ee_add = IICEEPConfigAddr(EEVega);
			ee_size = sizeof(SizeEE_Vega);
			break;
		case USER:
			ee_add = IICEEPConfigAddr(EEUser);
			ee_size = sizeof(SizeEE_User);
			break;
		case UCOD:
			ee_add = IICEEPConfigAddr(EEUtente);
			ee_size = sizeof(SizeEEUtente);
			break;
		case INHP:
			ee_add = IICEEPConfigAddr(EEInhp);
			ee_size = sizeof(SizeEE_INHP);
			break;
		case CHGM:
			ee_add = IICEEPConfigAddr(EEMaxChange);
			ee_size = sizeof(SizeEEMaxChange);
			break;
		case BNKE:
			ee_add = IICEEPConfigAddr(EEBillValidWCard);
			ee_size = sizeof(SizeEEBillValidWCard);
			break;
/*
		case MODE:
		case SPEC:
			ee_add = IICEEPConfigAddr(EEModLav);
			ee_size = sizeof(SizeEEModLav);
			break;
*/
		case EXTR:
			ee_add = IICEEPConfigAddr(EEPrelevaAudExt);
			ee_size = sizeof(SizeEEPrelevaAudExt);
			break;
		case INHX:
			ee_add = IICEEPConfigAddr(EETestAudExtFull);
			ee_size = sizeof(SizeEETestAudExtFull);
			break;
		case PERM:
			ee_add = IICEEPConfigAddr(EEPermanentCash);
			ee_size = sizeof(SizeEEPermanentCash);
			break;
		case MICT:
			ee_add = IICEEPConfigAddr(EEMinimoCoinTube);
			ee_size = sizeof(SizeEEMinimoCoinTube);
			break;
/*
		case FRCS:
			ee_add = IICEEPConfigAddr(EEFreeCredAllaSelez);
			ee_size = sizeof(SizeEEFreeCredAllaSelez);
			break;
		case FRVS:
			ee_add = IICEEPConfigAddr(EEFreeVendAllaSelez);
			ee_size = sizeof(SizeEEFreeVendAllaSelez);
			break;
*/
		case SRNM:
			ee_add = ProductSerialNum;
			ee_size = sizeof(uint32_t);
			break;
		case EXCH:
			ee_add = IICEEPConfigAddr(EEEquazExChng);
			ee_size = sizeof(SizeEEEquazExChng);
			break;
		case RICU:
			ee_add = IICEEPConfigAddr(EEInhRechargeCard);
			ee_size = sizeof(SizeEEInhRechargeCard);
			break;
		case ACCK:
			ee_add = IICEEPConfigAddr(EEMaxAcceptCreditCard);
			ee_size = sizeof(SizeEEMaxAcceptCreditCard);
			break;
		case CHNG:
			ee_add = IICEEPConfigAddr(EECambiaMonete);
			ee_size = sizeof(SizeEECambiaMonete);
			break;
		case TERR:
			ee_add = IICEEPConfigAddr(EETimeDiff);
			ee_size = sizeof(SizeEETimeDiff);
			break;
		case TCAM:
			ee_add = IICEEPConfigAddr(EETimeSample);
			ee_size = sizeof(SizeEETimeSample);
			break;
		case SEGN:
			ee_add = IICEEPConfigAddr(EEDiffSign);
			ee_size = sizeof(SizeEEDiffSign);
			break;
		case SA00:
			ee_add = EEPConfVMCAddr(EE_SelOptA[Id_Index]);
			ee_size = sizeof(SizeEE_VMCConfig);
			break;
/*
		case SB00:
			ee_add = EEPConfVMCAddr(EE_SelOptB[Id_Index]);
			ee_size = sizeof(SizeEE_VMCConfig);
			break;
*/
		case SP00:
			ee_add = EEPConfVMCAddr(EE_SelezPrezzo[Id_Index]);
			ee_size = sizeof(SizeEE_VMCConfig);
			break;
		case OP00:
			ee_add = EEPVMCParamAddr(EE_ParamOpt[Id_Index]);
			ee_size = sizeof(SizeEE_OP_Array);
			break;
/*
		case FA00:
			ee_add = EEFasceOrarieAddr(EE_ParamFasc[Id_Index]);
			ee_size = sizeof(SizeEE_FA_Array);
			break;
*/
		case FB00:
			ee_add = (EEFasceOrarieAddr(EE_ParamFasc[Id_Index]) + (InizioFasceFB * sizeof(SizeEE_FA_Array)));
			ee_size = sizeof(SizeEE_FA_Array);
			break;
		case MI00:
			ee_add = EEPVMCParam16Addr(EE_ParamMisc[Id_Index]);
			ee_size = sizeof(SizeEE_MI_Array);
			break;
		case PR00:
			ee_add = IICEEPConfigAddr(EEPrezzoPR[Id_Index]);
			ee_size = sizeof(SizeEEPrezzo);
			newPrice = 1;
			break;
		case PA00:
			ee_add = IICEEPConfigAddr(EEPrezzoPA[Id_Index]);
			ee_size = sizeof(SizeEEPrezzo);
			newPrice = 1;
			break;
		case MN00:
			ee_add = IICEEPConfigAddr(EEMonete[Id_Index]);
			ee_size = sizeof(SizeEEMonete);
			break;

#if IOT_PRESENCE
		case AUST:
			ee_add = IICEEPConfigAddr(EEFasciaAuditSalesInizio);
			ee_size = sizeof(SizeEE_TimeFascia);
			break;
		case AUSP:
			ee_add = IICEEPConfigAddr(EEFasciaAuditSalesFine);
			ee_size = sizeof(SizeEE_TimeFascia);
			break;
		case CIFR:
			ee_add = IICEEPConfigAddr(EECifratura);
			ee_size = sizeof(SizeEECifratura);
			break;
#endif
			
		default:
			ee_add = 0;																					// ID sconosciuto
			ee_size = 0;
			break;
	}

	if (ee_add == 0 & ee_size == 0) return (esito = ID_NOT_FOUND);									// ID non trovato
	esito = RD_WR_CFG_OK;
	if (ReadWrite) {
		// *****  Lettura  Dato  *******
		ParamVal = 0;																				// Ripulisco da letture precedenti perche' l'attuale puo' essere piu' corta
		if (!ReadEEPI2C(ee_add, (uint8_t *)(&ParamVal), ee_size)) return (esito = WrRdCfg_ReadFail);	// Lettura Parametro dalla EEProm fallita
			*dato = ParamVal;
	} else {
		// *****  Scrittura Dato  *******
		if (ee_add != ProductSerialNum) {															// Skip scrittura Serial Number in EE da file di configurazione
			ParamVal = *dato;
			if (!WriteEEPI2C(ee_add, (uint8_t *)(&ParamVal), ee_size)) esito = WrCfg_WriteFail;
			if (esito != WrCfg_WriteFail) {
				if (newPrice) SetDatePrice();
			}
		}
	}
	return esito;
}


