/* ***************************************************************************************
File:
    I2C_EEPROM.c

Description:
    File con funzioni di read/write EEProm.
    

History:
    Date       Aut  Note
    Apr 2019 	MR   

 *****************************************************************************************/
#include "ORION.H" 
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"
#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

		uint8_t		Tipo_EE, EE_Buff[130];
static	uint16_t	banco;								// Device Address

#if DBG_I2C_EEP_WR
	uint32_t	WriteEEP_ErrCnt;
	uint32_t	ReadEEP_ErrCnt;
#endif


/*--------------------------------------------------------------------------------------*\
Method: ReadEEPI2C
	Legge dalla EEPROM size bytes partendo dall'indirizzo AddrEEP e li 	memorizza 
	all'indirizzo AddrDest.
	
	**** NOTA : utilizza EE_Buff come buffer temporaneo dei bytes letti dalla memoria *****
	****        quindi il numero massimo di bytes da leggere e' di 128 per volta      *****
	
Parameters:
	IN:  - AddrEEP contiene l'indirizzo EEPROM del primo byte da leggere nel formato
			LittleEndian (Kinetis) che verra' convertito in BigEndian perche' la
			EEPROM vuole per primo l'Address HIGH
		 - In AddrDest c'e' l'indirizzo della destinazione dei byte letti.
		 - size e' il numero di byte da leggere.
	OUT: - True e dato in AddrDest, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  ReadEEPI2C(uint16_t AddrEEP, uint8_t *AddrDest, uint16_t size)
{
#if DBG_I2C_EEP_WR
	bool		read_res;
#endif

	uint8_t		n, ChrToRead, numbytepage;
	uint16_t	Source, memaddr, cnt;

	memaddr = AddrEEP;																			// Indirizzo EEprom dal quale leggere i dati richiesti
	numbytepage = (PageSize - (AddrEEP % PageSize));											// Numero di bytes da poter leggere prima del termine della pagina		
	cnt = 0;																					// Indice del buffer di destinazione
	while (size > 0) {
		Source = memaddr;																		// Predispongo in Source l'address EEPROM invertito Low-High come
		if (size > numbytepage) {
			ChrToRead = numbytepage;
		} else {
			ChrToRead = size;
		}
#if DBG_I2C_EEP_WR
		read_res = EEPROM_Read (banco, Source, &EE_Buff[0], ChrToRead);							// Leggo "ChrToRead" bytes dalla EEPROM
		if (read_res == false)
		{
			ReadEEP_ErrCnt++;
			return false;
		}
#else
		if (!EEPROM_Read (banco, Source, &EE_Buff[0], ChrToRead)) return false;					// Leggo "ChrToRead" bytes dalla EEPROM: se errore esco con false
#endif
		for (n=0; n<ChrToRead; n++) {
			*(AddrDest+cnt) = EE_Buff[n];
			cnt++;
		}
		size -= ChrToRead;
		memaddr += ChrToRead;
		numbytepage = PageSize;
		WDT_Clear(gVars.devWDT);
	}

	return true;
}

/*--------------------------------------------------------------------------------------*\
Method: FillEEPI2C
  Prepara il/i buffer EE_Buff per riempire la memoria EEPROM con il carattere passato nella
  chiamata.
  Ottimizzo il fill scrivendo, quando possibile, una pagina per volta. 
Parameters:
	IN:  - AddrEEP contiene l'indirizzo EEPROM del primo byte da scrivere nel formato
			LittleEndian (Kinetis) che verra' convertito in BigEndian perche' la
			EEPROM vuole per primo l'Address HIGH
		 - In ValDato c'e' il dato da scrivere.
		 - size e' il numero di volte che il dato deve essere scritto.
\*--------------------------------------------------------------------------------------*/
bool  FillEEPI2C(uint16_t AddrEEP, uint8_t ValDato, uint32_t size)
{

#if DBG_I2C_EEP_WR
	bool		write_res;
#endif
	uint8_t		parz1, parz2;
	uint16_t	pageaddr, numpages, i;

	pageaddr = AddrEEP;																			// Successivo indirizzo dal quale riempire la memoria
	parz1 = (PageSize - (AddrEEP % PageSize));													// Frazione della prima pagina che rimane a partire dall'indirizzo di inizio fino alla fine della pagina
	if (size > parz1) {
		numpages = ((size-parz1) / PageSize);													// Pagine intere da riempire
		parz2 = ((size-parz1) - (numpages*PageSize));											// Byte dell'ultima pagina da riempire
	} else {
		numpages = 0;																			// Nessuna pagina intera da riempire
		parz1 = 0;																				// Frazione totale della prima pagina
		parz2 = size;																			// Byte della pagina da riempire
	}
	for (i=0; i<PageSize; i++) {																// Riempio EE_Buff del valore di fill
		EE_Buff[i] = ValDato;
	}
	if (parz1 != 0)
	{

#if DBG_I2C_EEP_WR
		// Frazione della prima pagina da riempire per parz1 bytes
		write_res = EEPROM_Write(banco, pageaddr, &EE_Buff[0], (parz1));
		if (write_res == false)
		{
			WriteEEP_ErrCnt++;
		}
#else
		// Frazione della prima pagina da riempire per parz1 bytes
		EEPROM_Write(banco, pageaddr, &EE_Buff[0], (parz1));
#endif
		pageaddr += parz1;																		// Successivo indirizzo dal quale riempire la memoria
	}
	if (numpages != 0)
	{
		// Riempio tutte le pagine intere
		for (i=0; i<numpages; i++)
		{
#if DBG_I2C_EEP_WR
			write_res = EEPROM_Write(banco, pageaddr, &EE_Buff[0], PageSize);
			if (write_res == false)
			{
				WriteEEP_ErrCnt++;
			}
#else
			EEPROM_Write(banco, pageaddr, &EE_Buff[0], PageSize);
#endif
			pageaddr += PageSize;
			WDT_Clear(gVars.devWDT);
		}
	}
	if (parz2 != 0)
	{
		// Frazione dell'ultima pagina da riempire per parz2 bytes
#if DBG_I2C_EEP_WR
		write_res = EEPROM_Write(banco, pageaddr, &EE_Buff[0], parz2);
		if (write_res == false)
		{
			WriteEEP_ErrCnt++;
		}
#else
		EEPROM_Write(banco, pageaddr, &EE_Buff[0], parz2);
#endif
	}
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method: WriteEEPI2C
	Scrive size bytes dal buffer sorgente indirizzato da AddrSource.
	Prepara il buffer EE_Buff con i dati e lo invia alla "EEPROM_Write".
	Controlla il salto pagina.
  
Parameters:
	IN:  - AddrEEP contiene l'indirizzo EEPROM del primo byte da scrivere
		 - In AddrSource c'e' l'indirizzo della sorgente dei byte da scrivere.
		 - size e' il numero di byte da scrivere.
	OUT: - True e dato in memoria, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  WriteEEPI2C(uint16_t AddrEEP, uint8_t *AddrSource, uint16_t size)
{
#if DBG_I2C_EEP_WR
	bool		write_res;
#endif

  	uint8_t		n, numbytepage;
	uint8_t *	SourcePnt;
	uint16_t	memaddr;
	
	if (!size) return false;
	memaddr = AddrEEP;																			// Indirizzo destinazione primo byte del buffer da scrivere
	numbytepage = (PageSize - (AddrEEP % PageSize));											// Numero di bytes da poter scrivere fino al termine della pagina		
	SourcePnt = AddrSource;
	do {
		for (n=0; n<numbytepage; n++)
		{																						// Posso trasferire "numbytepage" bytes da Source a EE_Buff
			EE_Buff[n] = *(SourcePnt+n);
			size--;
			if (size == 0) 
			{
#if DBG_I2C_EEP_WR
				write_res = EEPROM_Write(banco, memaddr, &EE_Buff[0], (1+n));					// Memo last EE_Buff in EEPROM e non ci sono altri dati da scrivere
				if (write_res == false)
				{
					WriteEEP_ErrCnt++;
				}
#else
				EEPROM_Write(banco, memaddr, &EE_Buff[0], (1+n));								// Memo last EE_Buff in EEPROM e non ci sono altri dati da scrivere
#endif	
				break;
			}
		}
		if (size == 0) break;
#if DBG_I2C_EEP_WR
		write_res = EEPROM_Write(banco, memaddr, &EE_Buff[0], (n));								// Memo EE_Buff in EEPROM
		if (write_res == false)
		{
			WriteEEP_ErrCnt++;
		}
#else
		EEPROM_Write(banco, memaddr, &EE_Buff[0], (n));											// Memo EE_Buff in EEPROM
#endif
		memaddr += n;																			// Prossimo indirizzo eeprom
		SourcePnt += n;
		numbytepage = PageSize;
		WDT_Clear(gVars.devWDT);
	} while(size != 0);
	
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method: BankSelect
	Seleziona il banco della eeprom al quale accedere.

Parameters:
	IN	- banco eeprom (0 o 1) 
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  BankSelect(uint8_t bank){

	if (Tipo_EE == Microchip) {
		// ****  MICROCHIP  **********
		switch (bank) {
		case 0:
			banco = Bank0_ee1;
			break;
		case 1:
			banco = Bank1_ee1;
			break;
		case 2:
			banco = Bank0_ee2;
			break;
		case 3:
			banco = Bank1_ee2;
			break;
		}
	} else {
		switch (bank) {
		// ****  ATMEL ***************
		case 0:
			banco = Atmel_Bank0_ee1;
			break;
		case 1:
			banco = Atmel_Bank1_ee1;
			break;
		case 2:
			banco = Atmel_Bank0_ee2;
			break;
		case 3:
			banco = Atmel_Bank1_ee2;
			break;
		}
	}
	banco = banco << 1;																			// Device Address
}

