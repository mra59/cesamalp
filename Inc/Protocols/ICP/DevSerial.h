/*--------------------------------------------------------------------------------------*\
	DevSerial.h
\*--------------------------------------------------------------------------------------*/

#ifndef __DevSerial__
#define __DevSerial__

/*MR19
// Serial line configuration parameters
typedef struct {
	uint8_t	bitRate;									// Bit Rate
	uint8_t	parity;										// Parity type
	uint8_t	dataBits;									// Number of data bits	
	uint8_t	stopBits;									// Number of stop bits
} DevSerOpt;											// Options for a serial line device

// Serial line working mode
typedef struct {
	void	(*rxErrInd)(uint8_t rxData, uint8_t rxErr);		// Rx error indication
	void	(*rxDataInd)(uint8_t rxData);					// Rx data indication
	void	(*txReadyInd)(void);						// Tx empty indication
	void	(*txDoneInd)(void);							// Tx done indication (N1)
	void*	hndData;									// Ptr to handler data, if needed
} DevSerHnd;											// Serial line handler
*/

/* BitRate Type */
#define	kDevSerBitRate110		0
#define	kDevSerBitRate150		1
#define	kDevSerBitRate300		2
#define	kDevSerBitRate600		3
#define	kDevSerBitRate1200		4
#define	kDevSerBitRate2400		5
#define	kDevSerBitRate4800		6
#define	kDevSerBitRate9600		7
#define	kDevSerBitRate19200		8
#define	kDevSerBitRate38400		9

#define	kDevSerBitRate57600		10
#define	kDevSerBitRate115200	11
#define	kDevSerBitRate230400	12

#define	kDevSerBitRate50000		13
#define	kDevSerBitRate62500		14
#define	kDevSerBitRate100000	15
#define	kDevSerBitRate125000	16
#define	kDevSerBitRate250000	17


// Parity Type 
#define	kDevSerParityNone	0
#define	kDevSerParityOdd	1
#define	kDevSerParityEven	2
#define	kDevSerParitySpace	3
#define	kDevSerParityMark	4

// Data bits 
#define	kDevSerDataBits7	0
#define	kDevSerDataBits8	1
#define	kDevSerDataBits9	2


// Stop bits 
#define	kDevSerStopBits1_5	0
#define	kDevSerStopBits1	1
#define	kDevSerStopBits2	2

#endif		// End  __DevSerial__

