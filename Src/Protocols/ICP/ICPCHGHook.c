/****************************************************************************************
File:
    ICPCHGHook.c

Description:
    Funzioni di aggancio all'applicazione per protocollo ICP rendiresto

History:
    Date       Aut  Note
    Set 2012 	Abe   

*****************************************************************************************/


#include "ORION.H"
#include "icpCHGProt.h"
#include "ICP.h"
#include "ICPVMC.h"
#include "ICPGlobal.h"
#include "icpCHG.h"
#include "OrionCredit.h"
#include "AuditCash.h"
#include "Totalizzazioni.h"
#include "Power.h"

#include "Dummy.h"


uint8_t EnDis_Chg = 0;

#define CHG_EN 		1
#define CHG_DIS 	2

extern CreditCoinValue CoinValuesList[];

extern	uint16_t	Dec16Touint16(uint16_t);
extern	void 		CoinAuditStart(ET_CHGPOLLCOINDEST coinDest, uint8_t coinType, CreditCoinValue coinValue);
extern	void 		CoinPayoutAuditStart (CreditCoinValue ChangeAmount, CMDrvPAYPOS route);
extern	void 		CoinPayoutExpAuditStart(CreditCoinValue ChangeAmount, CMDrvPAYPOS route);


//MR19 extern bool CoinValueChanged;

/*------------------------------------------------------------------------------------------*\
 Method: ICPChgHookEnDis
	Controlla se abilitare/inibire la gettoniera in base a diversi parametri quali lo stato
	dell'Orion, flags varie (NoLink, Inh, etc), il credito massimo cash raggiunto.
	La flag "EnDis_Chg" contiene lo stato che la RR potra' assumere: ad es EnDis_Chg = CHG_EN 
	significa che la RR e' inibita e potra' solo essere enabled, se EnDis_Chg = CHG_DIS 
	la RR e' enabled e potra' solo essere inibita.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPChgHookEnDis(void) {

	uint16_t CashLimit = 0;
	uint16_t CashMax = MaxCash;

	// Test se la RR deve essere inhibit: se il master non e' ENABLE o IDLE la RR deve essere inhibit
	// if ((gOrionKeyVars.State != kOrionCPCStates_Enabled && gOrionKeyVars.State != kOrionCPCStates_Idle) || fVMC_NoLink || fVMC_Inibito || gVars.AudExtFull ) {
	if (fVMC_NoLink || fVMC_Inibito || gVars.AudExtFull ) {
		if (EnDis_Chg == CHG_DIS){
			if(!ICPCHGInhibit()){
				EnDis_Chg = CHG_EN;
			}
		}
		return;
	}
	if (gOrionKeyVars.Inserted) {
		CashMax = MaxRevalue;   																// MaxRevalue con carta inserita
	}
	switch (EnDis_Chg) {
		case CHG_EN:
			if (!gOrionKeyVars.Inserted && !gOrionKeyVars.ChgValidEnaW_Card) break;
				if (CashMax >= gOrionCashVars.creditCash) CashLimit = CashMax - gOrionCashVars.creditCash;
				if (!ICPCHGEnable(CashLimit)) EnDis_Chg = CHG_DIS;
				break;

		case CHG_DIS:
			if (gOrionKeyVars.ChgValidEnaW_Card) break;
			if (gOrionKeyVars.Inserted) break;
			if (!ICPCHGInhibit()) EnDis_Chg = CHG_EN;
			break;
					
		default:
			EnDis_Chg = CHG_EN;
			break;
	}
}


/*--------------------------------------------------------------------------------------*\
Method: CheckChgCashType
	Funzione chiamata al cambiamento del cash per verificare l'abilitazione/inibizione
	del validatore monete o del cash MDB.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void CheckChgCashType(void) {
	
	EnDis_Chg = CHG_EN;
	if (gOrionKeyVars.ChgValidEnaW_Card) return;
	if (!gOrionKeyVars.Inserted) EnDis_Chg = CHG_DIS;
}


/*--------------------------------------------------------------------------------------*\
Method: ICPChgHookTaskInit

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  ICPChgHookTaskInit(void) {
	
	// Tolto il 08.08.17 CoinEnMask = 0xffff;																		// Valore fisso per prova --> da sostituire con valore configurazione
	gOrionKeyVars.ChgValidEnaW_Card = TRUE;														// Rendiresto sempre abilitato anche senza carta.
	num_icp_coin = 0;
}

/*--------------------------------------------------------------------------------------*\
Method: Chg_Task
	Main Task RendiResto: chiamata continuamente dal Main.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Chg_Task(void) {
	
	if (sCMDICPCash.acceptAmountCoinChgd) {
		ICPChgHookEnDis();
		ICPCHGHook_SetExactChange(MinCoinsTube);
		TestExch_InOut();																		// Controlla se entrato o uscito da Ex-ch per registrare l'evento
		UpdateCHGInfoKeyState(gOrionKeyVars.Inserted);
	}
	if (gOrionCashVars.StartChange == true) {
		if (!ICPCHGPayout(gOrionCashVars.creditCash)) {											// Sottraggo dall'attuale cash il credito da rendere per evitare che con OFF-ON
			PSHProcessingData();																// si possa ottenerlo e poi vederlo riapparire all'accensione
			CashDaRendere = gOrionCashVars.creditCash;
			CashResoParziale = 0;
			SottraeCash(gOrionCashVars.creditCash);												// Memo credito reso per aggiornare l'Audit in caso di power down durante Payout
			gOrionCashVars.creditCash = 0;											
			gOrionCashVars.StartChange = false;
		}
	}
/*
   	if (ICPProt_CHGGet_exp_state() == CHG_MANUAL_FILL) {
   		if(!ICPCHG_fill_report())ICPProt_CHGRes_exp_state();
   	}
*/    
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_Change
	Se c'e' credito cash inizia la procedure di restituzione del resto.

	IN:	  - 
	OUT:  - TRUE se attivato il Payout, altrimenti FALSE
\*--------------------------------------------------------------------------------------*/
bool ICPCHGHook_Change(void) {
	
	if (gOrionCashVars.creditCash == 0) return FALSE;
	sCMDICPCash.payoutState = kPOSRequest;
	CreditMasterGiveChange();
	return TRUE;
}
	
/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetEscrow
	Premuta leva Escrow, verifica se iniziare la procedure di restituzione del resto.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void ICPCHGHook_SetEscrow(void) {
	
	if (sCMDICPCash.inFillMode) return;    												// Ignora escrow se in Fill Mode
	if (!gOrionConfVars.CambiaMonete && !gVars.EnablePayout) return;					// Ignora escrow se non cambiamonete e non e' stata richiesta una vendita
	sCMDICPCash.payoutState = kPOSRequest;
	CreditMasterEscrowInd();
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetPaidoutAmount
  Chiamata ad ogni Payout Poll del master.

   IN:  - credito parziale restituito
  OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  ICPCHGHook_SetPaidoutAmount(CreditCoinValue payoutVal) {
	
	if (payoutVal != 0) {
		PSHProcessingData();
		CashResoParziale += payoutVal;
		PSHCheckPowerDown();
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetPayoutComplete
  Chiamata al termine dell'operazione di payout

   IN:  - credito totale restituito
  OUT:  - 
\*--------------------------------------------------------------------------------------*/
void ICPCHGHook_SetPayoutComplete(CreditCoinValue paidoutVal) {

	if (sCMDICPCash.inFillMode) return;    														// Ignora payout se in fill mode.
	if (sCMDICPCash.payoutState != kPOSNone) {
		CreditMasterChangePaidOutInd(paidoutVal);
		CoinPayoutExpAuditStart(paidoutVal, (CMDrvPAYPOS) Pay_Change);
		sCMDICPCash.payoutState= kPOSNone;
	}
}
	
/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetCoinsDeposited
	Moneta incassata

Parameters:
  coinDest  ->  coin destination
  coinType  ->  coin channel
  coinValue ->  coin value
\*--------------------------------------------------------------------------------------*/
void ICPCHGHook_SetCoinsDeposited(ET_CHGPOLLCOINDEST coinDest, uint8_t coinType, CreditCoinValue coinValue) {
	
	//MR19 bool inCashBox = false;

	CreditMasterCoinInInd(coinType);
	//CheckChgCashType();								// Chiamata spostata in CreditMasterCashAdd (in ogni caso qui non andava bene perche' il credito non e' ancora aggiornato)
	if(sCMDICPCash.inFillMode) return;      													// Ignora escrow quando in Fill Mode
	switch(coinDest) {
		
		case ICP_CHG_CASHBOX:
			//MR19 inCashBox= true;		Non usata da nessuno
		
		case ICP_CHG_TUBES:
			CreditMasterCashAdd(coinValue, kCreditCashTypeCoin);
			CoinAuditStart(coinDest, coinType, coinValue);
			break;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetDPP
  Remember DPP being used by Changer
  
Parameters:
  inDPP ->  decimal point position
\*--------------------------------------------------------------------------------------*/
void ICPCHGHook_SetDPP(uint8_t inDPP) {
//  sCMDICPCash.coinDPP= inDPP;
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetExactChange
	Determina lo stato di Exact-change (set o clear).

   IN:  - numero minimo monete nei tubi
  OUT:  - true se in Ex-Ch, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool ICPCHGHook_SetExactChange(uint8_t MinCoinNum) {
	
	return DeterminaExactChange(MinCoinNum);
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetReady
  Indicate change giver state
  
Parameters:
  avail ->  true if ready and available
\*--------------------------------------------------------------------------------------*/
void ICPCHGHook_SetReady(bool avail) {
	
	uint8_t i;
	uint16_t cMask = 0;
	CreditCoinValue valmon;
	
	sCMDICPCash.chgReady = avail;
	if (avail) {
		num_icp_coin = 0;
		sCMDICPCash.acceptAmountCoinChgd = true;
		for (i=0; i<ICP_MAX_COINTYPES; i++) {
			if (ICPProt_CHGGetCoinValue(i)) {													// Legge il valore della i-esima moneta ricevuta dalla gettoniera (valore diviso per l'USF)
				cMask |= 1<<i;																	// Se c'e' un valore di moneta la abilita in cMask e la conteggia in num_icp_coin
				num_icp_coin++;
			}
			valmon = ICPCHGGetCoinValue(i);														// Legge il valore reale della i-esima moneta e lo memorizza nell'array CoinValuesList
			if (valmon != CoinValuesList[i]) {
				CoinValuesList[i] = valmon;
				//MR19 CoinValueChanged = TRUE;			// Non utilizzata da nessuno
    		}
		}
		sCMDICPCash.coinEnableMask = (CoinEnMask & cMask);   									// CoinEnableMask contiene la maschera delle monete abilitate
		ICPCHGSetCoinEnableMask(sCMDICPCash.coinEnableMask);
//    	(*sCMDICPCash.cashTypeAvail)(kCreditCashTypeCoin);
	} else {
		if(sCMDICPCash.payoutState!=kPOSNone) {
			CreditMasterChangePaidOutInd(sCMDICPCash.changeToPayout);							// Funzione chiamata al termine dell'erogazione resto.
			sCMDICPCash.payoutState= kPOSNone;
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: CHGHook_GetCoinTubeAmount
  Calcola il valore totale del Cash presente nei tubi rendiresto
  NOTA: viene calcolato in base all'informazione Tube Status ricevuta dal CoinChanger

Parameters:

\*--------------------------------------------------------------------------------------*/

uint32_t CHGHook_GetCoinTubeAmount(uint8_t NumCoin, uint8_t ix){
	
	uint32_t	amount = 0;
	
	amount = (NumCoin * ICPProt_CHGGet_USF() * ICPProt_CHGGetCoinValue(ix));
	return amount;
}

/*--------------------------------------------------------------------------------------*\
Method: CHGHook_GetNumCoinInTube
	Restituisce il numero di monete  presenti nel tubo del CoinType passato come parametro.

   IN:  - CoinType
  OUT:  - Numero pezzi in tubo
\*--------------------------------------------------------------------------------------*/

uint8_t  CHGHook_GetNumCoinInTube(uint8_t CoinType){
	
	return (ICPCHGInfo.TubeStatus.aCoinsInTube[CoinType]);
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetCoinsDispMan
	Audit monete rese manualmente.

   IN:  - monete rese e tipo di moneta resa
  OUT:  - 
\*--------------------------------------------------------------------------------------*/
void ICPCHGHook_SetCoinsDispMan(uint8_t nCoinsDispensed, uint8_t coinType) {
	
	CreditCoinValue ValueAmount;

	//CreditMasterCoinsDispManVal(coinType);													// Vuota
	ValueAmount = ICPProt_CHGGet_USF() * ICPProt_CHGGetCoinValue(coinType) * nCoinsDispensed;
    CoinPayoutAuditStart(ValueAmount, (CMDrvPAYPOS) Pay_Manual);
}

/*--------------------------------------------------------------------------------------*\
Method: ICPCHGHook_SetManufactCode
store CoinChanger Manufacturer for audit purpose

\*--------------------------------------------------------------------------------------*/

void ICPCHGHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen){
	
	Set_Chg_Id_Data(0, nDataLen, ptData );
}

void ICPCHGHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen){
	
	Set_Chg_Id_Data(SIZE_MANUFACTURER, nDataLen, ptData );
}

void ICPCHGHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen){
	
	Set_Chg_Id_Data(SIZE_MANUFACTURER+SIZE_SERIAL, nDataLen, ptData );
}

void ICPCHGHook_SetSWVersion(uint16_t nSwVersion) {

  	uint16_t	HexVal;
//MR19 Questa dava errore "Error[Pe167]: argument of type "uint16_t *" is incompatible with parameter of type "uint8_t *"
/*
	HexVal = Dec16Touint16(nSwVersion);														// nSwVersion e' gia' ASCII, lo trasformo in Hex perche' nell'Audit.c la sprintf lo
	Set_Chg_Id_Data(SIZE_MANUFACTURER+SIZE_SERIAL+SIZE_MODEL, sizeof(HexVal), &HexVal );	// ritrasforma nuovamente in ASCII prima di metterlo nel CA1
	//Set_Chg_Id_Data(SIZE_MANUFACTURER+SIZE_SERIAL+SIZE_MODEL, sizeof(nSwVersion), &nSwVersion );
*/
	HexVal = Dec16Touint16(nSwVersion);
	Set_Chg_SwRev_Data(HexVal);
}


/*------------------------------------------------------------------------------------------*\
 Method: DisattivaEnDisCHG
   Disattiva la variabile "EnDis_Chg" per far trasmettere la nuova abilitazione monete
   al termine del ciclo di reset changer

   IN:  -   
  OUT:  -   
\*------------------------------------------------------------------------------------------*/
void  DisattivaEnDisCHG(void) {
	EnDis_Chg = CHG_EN;
}

/*--------------------------------------------------------------------------------------*\
Unused ICP protocol CHG hooks 
\*--------------------------------------------------------------------------------------*/
void ICPCHGHook_SetCountryCode(uint16_t countryCode){}
void ICPCHGHook_SetCurrency(uint16_t currency){}
/////void ICPCHGHook_SetCoinTypeInTube(uint16_t coinInTubeMask){}
/////void ICPCHGHook_SetTubeStatus(uint8_t *ptTubeStatus){}
void ICPCHGHook_SetPayout(bool fAvailable){}
void ICPCHGHook_SetExtDiag(bool fAvailable){}
void ICPCHGHook_SetManualFill(bool fAvailable){}
void ICPCHGHook_SetFTL(bool fAvailable){}
void ICPCHGHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen){}
void ICPCHGHook_SetTubesFull(bool fTubesFull) {}

bool ICPCHGHook_GetPayout(void) { return true; }
bool ICPCHGHook_GetExtDiag(void) { return false; }
bool ICPCHGHook_GetManualFill(void) { return false; }
bool ICPCHGHook_GetFTL(void) { return false; }



