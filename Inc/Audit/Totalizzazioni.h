/*--------------------------------------------------------------------------------------*\
File:
    Totalizzazioni.h

Description:
    Include file con definizioni e strutture per il calcolo di totali vari.
    

History:
    Date       Aut  Note
    Set 2012 	MR   
\*--------------------------------------------------------------------------------------*/

#ifndef __Totalizzazioni_h
#define __Totalizzazioni_h


void  AzzeraCash(uint8_t TestPWDN_Flag);
void  SottraeCash(uint16_t val);
void GestioneAuditFreeCredit(void);
void PresetAudFreeCredit(void);
void PresetAudFreeVend(void);
uint16_t  TotaleCash(void);
uint16_t TotaleCreditoAttuale(void);
void AuditResetNoSyncMDB(void);
void Audit_WD_Event_Da_ChiaveUSB(void);


#endif		// End __Totalizzazioni_h



