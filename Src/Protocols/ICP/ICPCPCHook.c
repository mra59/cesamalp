/****************************************************************************************
File:
    ICPCPCHook.c

Description:
    Funzioni di aggancio all'applicazione per protocollo ICP cashless

History:
    Date       Aut  Note
    Set 2012 	Abe   

*****************************************************************************************/

#include <string.h>


#include "ORION.H"
#include "ICPCPC.h"
#include "ICPGlobal.h"
#include "icpVMC.h"
#include "OrionCredit.h"
#include "Vendita.h"
#include "general_utility.h"
#include "Monitor.h"
#include "icpCPCProt.h"

#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void	ClrCardFlags(void);
extern	bool	Is_CPC_Level_1(void);


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

uint32_t	CPC_MaxNoResponseTime;

#define kModifMDB_KeyInOut true


#ifndef kCt5ICPCPCCashSaleTestSupport
#define kCt5ICPCPCCashSaleTestSupport	false
	// Set to true to test cash vend sale
#endif

/*--------------------------------------------------------------------------------------*\
Private types and variables
\*--------------------------------------------------------------------------------------*/

static	uint8_t		FeatureLevel, MaxNoResponse;
static	uint8_t		KR_TipoChiave;				//MR19 Aggiunta perche' nella "ICPCPCHook_GetMediaInfo" si usa ma non so da chi e' gestita
		uint16_t	CountryCode;

// My own state
typedef enum {
	kCT5ICPCPCStateInactive,
	kCT5ICPCPCStateDisabled,
	kCT5ICPCPCStateEnabled,	
#ifdef kModifMDB_KeyInOut
	// Key inserted but begin session not yet sent to vmc!
	kCT5ICPCPCStateKeyIns,
#endif
	kCT5ICPCPCStateIdle,	
	kCT5ICPCPCStateVend,	
	kCT5ICPCPCStateRevalue,	
	kCT5ICPCPCStateRefund,	
	kCT5ICPCPCSimulateExtraction
} CT5ICPCPCState;

//MR19 static CT5ICPCPCState sICPCPCState;			// Never Referenced


/*--------------------------------------------------------------------------------------*\
Private methods
\*--------------------------------------------------------------------------------------*/



/*--------------------------------------------------------------------------------------*\
Method:	CashlessTaskInit
	Nel Reset sono calcolati o letti dalla EE i seguenti parametri:
	CurrencyCode, MinPrice, MaxPrice e MultiVend

\*--------------------------------------------------------------------------------------*/
void CashlessTaskInit(void) {

	FeatureLevel = LivelloCashlessMDB;
	MaxNoResponse = MAX_NON_RESP_TIME_CPC_MDB;
	CountryCode = 0x0039;  
	gOrionKeyVars.State = kOrionCPCStates_Inactive;
	gOrionKeyVars.TemporaryState = kOrionCPCStates_Inactive;
	gOrionKeyVars.Inserted = false;
	gOrionKeyVars.VendAppAmount = 0;
	gOrionKeyVars.Credit = 0;
	StatusVendita = VEND_NULL;
	TipoVendita = NO_VEND_IN_CORSO;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_Init
	Reset received from master. Start init phase
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_Init(void) {
	CashlessTaskInit();
}


/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_IsAvailable
	Return true if Cashless Payment peripheral is used
\*--------------------------------------------------------------------------------------*/
bool ICPCPCHook_IsAvailable(void) {
	//TODO: da integrare...
	return true;	
}


/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_IsInitCompleted
	Check if init phase has completed
\*--------------------------------------------------------------------------------------*/
bool ICPCPCHook_IsInitCompleted(void) {
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCTest_IdleState

\*--------------------------------------------------------------------------------------*/
bool ICPCPCTest_IdleState(void) {
	return (gOrionKeyVars.State == kOrionCPCStates_Idle ? true : false);
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_VendRequest
	Cashless Slave: Vend Request received from the Master.

Parameters:
	price	->	sel price
	selNum	->	selection number
	currCode->	currency code
	ptVendAmount	<-	vend amount
	ptVendCurrency	<-	vend currency
Returns:
	No response ready at this moment
\*--------------------------------------------------------------------------------------*/
uint8_t ICPCPCHook_VendRequest(CreditCpcValue price, uint16_t selNumber, uint16_t currCode, CreditCpcValue *ptVendAmount, uint16_t *ptVendCurrency) {

	//TODO: aggiungere ICP_CPC_RESPDENY se in connessione DDCMP o sistema spento o audit ext full (vedi Executive)
	if (!ICPCPCTest_IdleState()) {
		Monitor_DeniedPerNoIDLE();
		return ICP_CPC_RESPDENY;															// Vend denied se gOrionKeyVars.state non e' IDLE
	}
	if (!gOrionKeyVars.Inserted) {
		Monitor_DeniedPerNoKey();
		return ICP_CPC_RESPDENY;															// Vend denied se non c'e' chiave
	}
	if (CheckVendInh()) {
		Monitor_DeniedPerNoKey();
		return ICP_CPC_RESPDENY;															// Vend denied se sistema OFF, o in connessione DDCMP o Audit estesa Full
	}
	VMC_ValoreSelezione = price;
	if (!SetPrice_e_Selnum(VMC_ValoreSelezione, selNumber)) {								// Determino prezzo e/o numero selezione
		Monitor_DeniedPerNoSelVal();														// Vend denied se prezzo non determinabile
		return ICP_CPC_RESPDENY;
	}
	Monitor_VendReqMDB(price, selNumber);													// Se in Monitor, echo al PC la richiesta di vendita del VMC
	gOrionKeyVars.State = kOrionCPCStates_Vend;												// Stato di richiesta vend per OrionCredit
	//Set_fVMC_VenditaRichiesta;
	return ICP_CPC_WAITRESP;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_NegVendRequest
	Cashless Slave: Negative Vend Request received from the Master.

Parameters:
	price	->	sel price
	selNum	->	selection number
	currCode->	currency code
	ptVendAmount	<-	vend amount
	ptVendCurrency	<-	vend currency

Returns:
	Vend deny or Wait response
\*--------------------------------------------------------------------------------------*/
uint8_t ICPCPCHook_NegVendRequest(CreditCpcValue nPrice, uint16_t nSelNumber, uint16_t nCCode, CreditCpcValue *ptVendAmount, uint16_t *ptVendCurrency) {

	if (!ICPCPCTest_IdleState()) return ICP_CPC_RESPDENY;
	if(!gOrionKeyVars.Inserted) {
		return ICP_CPC_RESPDENY;
	}

	gOrionKeyVars.VendNeg = true;
	gOrionKeyVars.VendSel = nSelNumber;
	gOrionKeyVars.VendReq = nPrice;
	gOrionKeyVars.State = kOrionCPCStates_Vend;
			
	return ICP_CPC_WAITRESP;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_RevalueRequest
	Cashless Slave: Revalue Request received from the Master.

Parameters:
	revalueAmount	->	amount to be revalued
	currencyCode	->	currency
Returns:
	No response ready at this moment
\*--------------------------------------------------------------------------------------*/
uint8_t ICPCPCHook_RevalueRequest(CreditCpcValue revalueAmount, uint16_t currencyCode) {

	if (!ICPCPCTest_IdleState()) return ICP_CPC_RESPDENY;
	if (!gOrionKeyVars.Inserted) {
		return ICP_CPC_RESPDENY;
	}
	if(!revalueAmount) return ICP_CPC_RESPDENY;

	gOrionKeyVars.RevalueAmount = revalueAmount;
	gOrionKeyVars.State = kOrionCPCStates_Revalue;
	return ICP_CPC_WAITRESP;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_VendCancel
	Cashless Slave: Vend Cancel Request received from the Master.
	Called to abort a vend request not yet approved or denied.

\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_VendCancel(void) {
	//TODO: da integrare...
	/**
//    sICPCPCState= kCT5ICPCPCStateIdle;
//	VMC_Response= VMC_RESP_VEND_FAILED;

	// Simulate vend fail to generate a refund
	if(StatusCashless==KR_ENABLED || StatusCashless==KR_DISABLED) {
	    // No key. Nothing else to do.
	    sICPCPCState= kCT5ICPCPCStateIdle;
	}
	else {
		ICPCPCHook_VendFailure();
	}
	**/
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_VendSuccess
	Cashless Slave: Vend Success received from the Master.
	Called to signal that the vend session is completed successfully.
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_VendSuccess(uint16_t nSelNumber) {
	
	VMC_Response = VMC_RESP_VEND_OK;
	//gOrionKeyVars.State = kOrionCPCStates_Idle;			// MR in OrionCredit sara' predisposta ad Idle

	//TODO: da integrare...
	/**
	VMC_Response = VMC_RESP_VEND_OK;
	sICPCPCState= kCT5ICPCPCStateIdle;
	**/
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_VendFailure
	Cashless Slave: Vend Failure received from the Master.
	Called to signal that the vend session is failed.
	Try to refund the credit on the key.
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_VendFailure(void) {
	
	VMC_Response = VMC_RESP_VEND_FAILED;
	//gOrionKeyVars.State = kOrionCPCStates_Refund;			// MR in OrionCredit sara' predisposta ad kOrionCPCStates_Refund

	//TODO: da integrare...
	/**
	VMC_Response = VMC_RESP_VEND_FAILED;
	VMC_Command = VMC_CMD_NULL;
	sICPCPCState= kCT5ICPCPCStateRefund;
	**/
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_VendComplete
	Cashless Slave: Vend Complete received from the Master.
	Called to signal that the vend session is completed.
	
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_VendComplete(void) {

	gOrionKeyVars.State = kOrionCPCStates_Enabled;

}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_VendCashSale
	Called to audit vend sessions...
	Nothing to do here
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_VendCashSale(CreditCpcValue nPrice, uint16_t nSelNumber, uint16_t nCCode) {
	#if kCt5ICPCPCCashSaleTestSupport
	// For test only
	void LCDWrite(const char* str, uint16_t time);
	LCDWrite("CASH", 200);
	#endif
}


/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_ReaderEnable
	Cashless Slave: Reader Enable/Disable received from the Master.
	Called to enable/disable the reader

Parameters:
	enable	->	en/dis flag
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_ReaderEnable(bool enable) {
	if(enable) {
		gOrionKeyVars.State = kOrionCPCStates_Enabled;
		Clear_fVMC_Inibito;
	} else {
		gOrionKeyVars.State = kOrionCPCStates_Disabled;
		Set_fVMC_Inibito;
	}
}


/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_ReaderCancel
	Called to abort reader activities at Enabled state (!?!)
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_ReaderCancel(void) {
	// ?
//	sICPCPCState= kCT5ICPCPCStateEnabled; 
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetScalingFactor
	Returns units scaling factor	
\*--------------------------------------------------------------------------------------*/
uint8_t ICPCPCHook_GetScalingFactor(void) {
	return UnitScalingFactor;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetDecimalPoint
	Returns decimal point position	
\*--------------------------------------------------------------------------------------*/
uint8_t ICPCPCHook_GetDecimalPoint(void) {
	return DecimalPointPosition;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetMaxNoResponse
	Returns maximum time required to reply to master commands (in seconds)
\*--------------------------------------------------------------------------------------*/
uint8_t ICPCPCHook_GetMaxNoResponse(void) {
	return MaxNoResponse;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetFeatureLevel
	Returns ICP supported feature level
\*--------------------------------------------------------------------------------------*/
uint8_t ICPCPCHook_GetFeatureLevel(void) {
	return FeatureLevel;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetCountryCode
	Returns ICP country code
\*--------------------------------------------------------------------------------------*/
uint16_t ICPCPCHook_GetCountryCode(void) {
	return CountryCode;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetCurrencyCode
	Returns ICP Currency Description in BCD format
\*--------------------------------------------------------------------------------------*/
uint16_t ICPCPCHook_GetCurrencyCode(void) {
	
	uint16_t	CurrCode;
	
	HexWordToDec(CurrencyCode);
	CurrCode = (DecH << 8) | DecL;
	return CurrCode;
}
/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetFeatures
	Return reader miscellaneous options: refund, multivend, display, cash sale
\*--------------------------------------------------------------------------------------*/
ET_CPCFOPTIONS ICPCPCHook_GetFeatures(void) {
	//ET_CPCFOPTIONS opt= fNoOutCard? ICP_CPCPOLL01_REFUNDS|ICP_CPCPOLL01_MULTIVEND : ICP_CPCPOLL01_REFUNDS;
	ET_CPCFOPTIONS opt = ICP_CPCPOLL01_NONE;

	switch(FeatureLevel) {
	case 2:
		opt = ICP_CPCPOLL01_REFUNDS;
		if(MultiVend) {
			opt |= ICP_CPCPOLL01_MULTIVEND;
		}
		break;
	default:
	case 1:
		break;
	}
	//TODO: da integrare...
	/**
	if(fNoOutCard) opt |= ICP_CPCPOLL01_MULTIVEND;
	if(!fInhibitRechargeKey) opt |= ICP_CPCPOLL01_REFUNDS;


	#if kCt5ICPCPCCashSaleTestSupport
	opt|= ICP_CPCPOLL01_CASHSALE;
	#endif
	**/
	return opt;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetRevalueLimit
	Cashless Slave: Revalue Limit Request received from the Master.
	Called to get the maximum amount the key will accept
	
Parameters:
	*ptLimitAmount	<-	revalue limit amount
	*ptCCode		<-	currency code
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_GetRevalueLimit(CreditCpcValue *ptLimitAmount, uint16_t *ptCCode) {

	*ptLimitAmount = 0;
	*ptCCode = CurrencyCode;
	
//	if(MaxRecharge && gOrionKeyVars.Inserted && (MaxRecharge > gOrionKeyVars.Credit)) {
//		*ptLimitAmount = MaxRecharge - gOrionKeyVars.Credit;
//	}

	// 13.07.2013 - MR sostituito MaxRecharge con MaxRevalue per poterne condizionare
	//				il valore in situazioni come la carta vendite di test
	if(gOrionKeyVars.Inserted) {
		*ptLimitAmount = MaxRevalue;
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetMediaInfo
	Cashless Slave: send Begin Session to the Master.
	Funzione chiamata per inviare i dati di BEGIN SESSION
Parameters:
	...	<-	media info	
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_GetMediaInfo(CreditCpcValue *ptFunds, uint32_t *ptPaymentID, uint8_t *ptPaymentType,
	uint16_t *ptPaymentData, uint16_t *ptUserLanguage, uint16_t *ptUserCurrency, uint8_t *ptUserOptions) {

	VMC_CreditoDaVisualizzare = TotCreditCard();												// 25.05.15 Aggiunto per visualizzare anche il FreeCredit
	*ptFunds = VMC_CreditoDaVisualizzare;
	//*ptFunds = gOrionKeyVars.Credit;															// 25.05.15 Con questo puntatore non si visualizzava il FreeCredit
	*ptPaymentID = gOrionKeyVars.CodiceUtente;
	*ptPaymentData = gOrionKeyVars.LivelliChiave;
	*ptUserLanguage = 0;
	*ptUserCurrency = 0;
	*ptUserOptions = ICP_CPCPOLL03_REFUND | ICP_CPCPOLL03_REVALUENEGVEND;

	if(KR_TipoChiave == KR_CHIAVE_VENDITE_PROVA) {
		*ptPaymentType= ICP_CPCMEDIA_TEST;
	} else {
		if (gOrionKeyVars.LivelliChiave > 0) {
			*ptPaymentType = ICP_CPCUserGroupPriceListNumber;
		} else {
			*ptPaymentType = ICP_CPCMEDIA_NORMAL;
		}
	}

	gOrionKeyVars.State = kOrionCPCStates_Idle;
}

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetVendAppAmount
	Cashless Slave: Vend Success received from the Master.
	Called to get VendAmount approved.

Parameters:
	*ptVendAmount	<-	
	*ptVendCurrency	<-
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_GetVendAppAmount(CreditCpcValue *ptVendAmount, uint16_t *ptVendCurrency) {
	//TODO: da integrare...
	/**
	*ptVendAmount= VendAmountKR;
	**/
	
	*ptVendAmount = gOrionKeyVars.VendAppAmount;	
	*ptVendCurrency = CurrencyCode;
}


/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_GetDispInfo
	Called to get the "Message" that the reader needs/wants to display.
	Msg length = 32, msgTime = requested display time (0.1 second units)
Parameters:
	*ptVendAmount	<-	
	*ptVendCurrency	<-
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_GetDispInfo(uint8_t *ptMsg, uint8_t *ptMsgTime) {
	ptMsg[0]= '\0';
	*ptMsgTime = 0;
}


/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_SetXXXX
	All this info is ignored for now
\*--------------------------------------------------------------------------------------*/

// Called to save the price range of the Vmc
void ICPCPCHook_SetMaxMinPrices(CreditCpcValue maxPrice, CreditCpcValue minPrice, uint16_t countryCode) {
	MaxPrice = maxPrice;
	MinPrice = minPrice;
}

void ICPCPCHook_SetFTLEnable(bool fEnable){}
	// Called to advise the reader that the "File Transport Layer" feature is available

void ICPCPCHook_SetCredit32Enable(bool fEnable){}
	// Called to advise the reader that the "32 bit monetary format" feature is using

void ICPCPCHook_SetMultiCurrencyEnable(bool fEnable){}
	// Called to advise the reader that the "multi currency/lingual" feature is available

void ICPCPCHook_SetNegVendEnable(bool fEnable){}
	// Called to advise the reader that the "Negative Vend" feature is available

void ICPCPCHook_SetDataEntryEnable(bool fEnable){}
	// Called to advise the reader that the "Data Entry" feature is available

/*--------------------------------------------------------------------------------------*\
Method:	ICPCPCHook_EndSession
	MDB Master: chiamata quando lo slave CPC risponde End_Session.
	
Parameters:
	*ptLimitAmount	<-	revalue limit amount
	*ptCCode		<-	currency code
\*--------------------------------------------------------------------------------------*/
void ICPCPCHook_EndSession(void) {

	ClrCardFlags();																				// Per compatibilita' con Mifare Locale Azzero tutte le flags carta
	gOrionKeyVars.Credit = 0;																	// Azzero anche il credito
	if (gOrionKeyVars.State == kOrionCPCStates_RevalueWr)
	{																							// Ero in attesa di risposta al Revalue
		gOrionKeyVars.CreditWriteEnd 	= true;
		gOrionKeyVars.ExtKeyRevalueApp 	= false;
	}
	else if (gOrionKeyVars.State == kOrionCPCStates_VendWr)
	{																							// Ero in attesa di risposta al Vend Request
		gOrionKeyVars.CreditWriteEnd 	= true;
		gOrionKeyVars.ExtKeyVendApp  	= false;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_MediaCredit
	MDB Master: inserita carta nel CPC Slave.
	Il valore massimo ricaricabile non e' calcolato ma richiesto con comando successivo
	al CPC.
	La flag displayCredit e' ignorata.
	10.10.2019 Con un lettore di carte Brasiliano scoperto che lavora a Livello 1 non deve
	essere inviato il comando di Rivalue Amout, per cui occorre ritornare alla sequenza
	originale e attivare in questa funzione la flag Inserted. 
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPCPCHook_MediaCredit(CreditCpcValue cpcValue, bool displayCredit) {
	
	if (gOrionKeyVars.Inserted == false) {
		gOrionKeyVars.CheckSecure1 	= true;														// Per compatibilita' con Mifare Locale
		gOrionKeyVars.CheckSecure2 	= true;
		gOrionKeyVars.Credit 		= cpcValue;
		gOrionKeyVars.KR_TipoChiave = KR_CHIAVE_UTENTE;
		if (Is_CPC_Level_1() == true)
		{
			gOrionKeyVars.Inserted		= true;													// 10.2019 Col Livello 1 non si manda il CMD Revalue Limit Request, quindi set qui flag Inserted
		}
		//SetMaxRevalue();																		// Determino credito caricabile sulla Carta
		//gOrionKeyVars.Inserted		= true;													// 08.08.17 Spostato nella "ICPCPCHook_RevalueLimit", vedi commento alla funzione
	}
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_RevalueLimit
	MDB Master: richiesto limite di ricarica della carta inserita nel CPC Slave.
	
	Si inserisce qui il set della flag "Inserted" perche' l'Orion Credit attiva l'invio del
	CoinType e del BillType quando il CPC comunica il Begin Session e si passa ad IDLE.
	Inserendo qui il set della "Inserted" posso inviare CoinType e BillType quando e' noto il
	MaxRevalue; lasciando invece come in precedenza il set della "Inserted" in "ICPCPCHook_MediaCredit",
	CoinType e Billtype erano inviati calcolandoli con MaxRevalue non ancora aggiornato.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_RevalueLimit(CreditCpcValue maxRevalueAmount, uint16_t currencyCode) {
	
	MaxRevalue = maxRevalueAmount;
	gOrionKeyVars.Inserted = true;
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHookSetMaxRevalue
	MDB Master: chiamata dall'OrionCredit dopo una vendita OK per aggiornare il limite di 
	ricarica della carta inserita nel CPC Slave.
	10.2019 Aggiunto controllo livello reader: se Lev 1 non e' ricaricabile.
	
   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHookSetMaxRevalue(CreditCpcValue CostoSelezione)
{
	if (Is_CPC_Level_1() == true) return;
	MaxRevalue += (uint32_t)CostoSelezione;
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_MediaID
	MDB Master: registro codice carta come codice utente, anche se poi sara' trattato a 16 bit
	e non a 32 come ricevuto.
	
   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_MediaID(uint32_t nMediaID)
{
	gOrionKeyVars.CodiceUtente = nMediaID;
}

void ICPCPCHook_OpCancelled(void) {
	
}

/*------------------------------------------------------------------------------------------*\
 Method: ICPCPCHookEnDis
	MDB Master: controlla se abilitare/inibire il CPC in base a diversi parametri del DA.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPCPCHookEnDis(void) {

	if (fVMC_Inibito) {
		if (gOrionKeyVars.State == kOrionCPCStates_Enabled) {
			if (ICPCPCDisable() == false) {														// Comando predisposto per l'invio
				gOrionKeyVars.State = kOrionCPCStates_Disabled;
			}
		}
	} else {
		if (gOrionKeyVars.State == kOrionCPCStates_Disabled) {
			if (ICPCPCEnable() == false) {														// Comando predisposto per l'invio
				gOrionKeyVars.State = kOrionCPCStates_Enabled;
			}
		}
	}
}


/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_RechargeOption
	Determina se il reader ha la possibilita' di rivalutare le carte.
	Cio' dipende dal livello del reader: se Liv 1 non effetta la ricarica.
	Il valore e' complementare a quello utilizzato nella nostra configurazione dove ricarica
	carta permessa e' = 0 e inibita e' = 1.
	Disabilito la nostra flag di ricarica solo se il reader non ammette ricarica, altrimenti
	lascio in uso la nostra configurazione.

   IN: - enable 1 = ricarica abilitata - enable 0 = ricarica inibita
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_RechargeOption(bool enable) {

	if (enable == false) {
		gOrionConfVars.InhRechargeCard = true;
	}
}

void ICPCPCHook_RefundError(bool refundFail) {
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_RevalueApproved
	Risposta Approved da parte del CPC Slave al Revalue Request del Master.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_RevalueApproved(void) {
	
	gOrionKeyVars.CreditWriteEnd 	= true;
	gOrionKeyVars.ExtKeyRevalueApp 	= true;
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_RevalueDenied
	Risposta Denied da parte del CPC Slave al Revalue Request del Master.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_RevalueDenied(void) {

	gOrionKeyVars.CreditWriteEnd 	= true;
	gOrionKeyVars.ExtKeyRevalueApp 	= false;
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_VendApproved
	Risposta Approved da parte del CPC Slave al Vend Request del Master.
	Utilizzo la differenza di credito scalato per determinare costo selezione attribuito 
	dal CPC che potrebbe aver dato sconti.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_VendApproved(CreditCpcValue vendAmount, uint16_t nCurrencyCode) {
	
	if (vendAmount != gOrionKeyVars.VendAppAmount) {
		gOrionKeyVars.VendAppAmount = vendAmount;
		VendAmountKR = gOrionKeyVars.VendAppAmount;												// Uso VendAmountKR per non cambiare il file Vendita.c
		Update_DatiAuditVendita(vendAmount);													// Aggiorno dati Audit per vendita con CPC esterno
	}
	gOrionKeyVars.Credit -= gOrionKeyVars.VendAppAmount;										// Nuovo credito cashless da visualizzare
	gOrionKeyVars.CreditWriteEnd = true;
	gOrionKeyVars.ExtKeyVendApp  = true;
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_VendDenied
	Risposta Denied da parte del CPC Slave al Vend Request del Master.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_VendDenied(void) {
	
	gOrionKeyVars.CreditWriteEnd = true;
	gOrionKeyVars.ExtKeyVendApp  = false;
	CreditoMancante = gOrionKeyVars.VendAppAmount;
	ResetDatiVendita();																			// Reset TipoVendita altrimenti si bloccano tutte le selez successive
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_SetReady
	Stato del CPC.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_SetReady(bool avail) {
	
	gOrionKeyVars.State = avail ? kOrionCPCStates_Enabled : kOrionCPCStates_Disabled;
}


/*------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_CheckOrionState
	MDB Master: funzione chiamata dalla "CPCInit" prima dell'invio del Reset Command per 
	verificare se lo stato interno dell'OrionCredit sia congruo (non deve esserci credito 
	da cashless MDB, flag Inserted set o stato OrionCredit diverso da kOrionCPCStates_Enabled).

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPCPCHook_CheckOrionState(void)
{
	OrionCreditStateCheck();
}	
	
/*------------------------------------------------------------------------------------------*\
 Method: CheckZeroCPC_USF
	MDB Master: verifica se e' collegato un CPC per controllare che il suo USF non sia zero.

   IN:  - 
  OUT:  - true se USF = 0, altrimenti false
\*------------------------------------------------------------------------------------------*/
bool  CheckZeroCPC_USF(void)
{
	if (ICPInfo.aPeriphInfo[2].nState >= ICP_CPC_ENABLED)
	{
		if (ICPCPCInfo.Setup00.nUSF == 0)
		{
			return true;
		}
	}
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Is_CPC_Idle_State
	MDB Master:

   IN:  - 
  OUT:  - true se CPC in Idle State
\*------------------------------------------------------------------------------------------*/
bool  Is_CPC_Idle_State(void)
{
	if (ICPInfo.aPeriphInfo[2].nState >= ICP_CPC_SESSIDLE) return true;
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Is_CPC_EQ_LESS_Enable_State
	MDB Master:

   IN:  - 
  OUT:  - true se CPC in Enable State o inferiori
\*------------------------------------------------------------------------------------------*/
bool  Is_CPC_EQ_LESS_Enable_State(void)
{
	if (ICPInfo.aPeriphInfo[2].nState <= ICP_CPC_ENABLED) return true;
	return false;
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_MediaID
	MDB Master: registro codice carta come codice utente, anche se poi sara' trattato a 16 bit
	e non a 32 come ricevuto.
	
   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
uint32_t  GetLastCPC_MediaID(void)
{
	return ICPCPCInfo.FundInfo.nPaymentID;
}

//===================================================================================

void ICPCPCHook_TimeDateRequest(void){}
void ICPCPCHook_SetOutOfSequence(uint8_t nCpcState){}
void ICPCPCHook_SetCountryCode(uint16_t nCountryCode){}
void ICPCPCHook_SetCurrency(uint16_t nCurrency){}
void ICPCPCHook_SetDPP(uint8_t nDPP){}
void ICPCPCHook_MediaLanguage(uint16_t nLanguageCode){}
void ICPCPCHook_MediaCurrency(uint16_t nCurrencyCode){}
void ICPCPCHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen){}



/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_SetMaxNonResponseTime
	Memorizza Manufacturer Code per l'Audit.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_SetMaxNonResponseTime(uint8_t nTmMNR)
{
	if (nTmMNR > MAX_NON_RESP_TIME_CPC_MDB)
	{
		CPC_MaxNoResponseTime = (nTmMNR * OneSec);
	}
	else
	{
		CPC_MaxNoResponseTime = ICP_TM_CPCMAXNORESP;
	}
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_SetManufactCode
	Memorizza Manufacturer Code per l'Audit.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen){
	
	Set_CPC_Id_Data(0, nDataLen, ptData );
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_SetSerialNumber
	Memorizza Serial Number per l'Audit.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen){
	
	Set_CPC_Id_Data(SIZE_MANUFACTURER, nDataLen, ptData );
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_SetModelNumber
	Memorizza Modello per l'Audit.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen){
	
	Set_CPC_Id_Data(SIZE_MANUFACTURER+SIZE_SERIAL, nDataLen, ptData );
}

/*----------------------------------------------------------------------------------------------*\
 Method: ICPCPCHook_SetSWVersion
	Memorizza Software Revision per l'Audit.

   IN: - 
  OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPCPCHook_SetSWVersion(uint16_t nSwVersion) {
	
	uint16_t	HexVal;

	HexVal = Dec16Touint16(nSwVersion);																// nSwVersion e' gia' ASCII, lo trasformo in Hex perche' nell'Audit.c la sprintf lo
	Set_CPC_Id_Data(SIZE_MANUFACTURER+SIZE_SERIAL+SIZE_MODEL, sizeof(HexVal), (uint8_t*)&HexVal );	// ritrasforma nuovamente in ASCII prima di metterlo nel CA1
}


bool ICPHook_GetSlaveMode(void) {
	return (OpMode == MDB);
}

void ICPVMCHook_DisplayMsg(uint8_t *ptMsg, uint8_t nMsgLen, uint16_t nMsecDisplayTime) {}
void ICPVMCHook_GetDate(uint8_t *ptYears, uint8_t *ptMonths, uint8_t *ptDays) {}
void ICPVMCHook_GetDateInfo(uint8_t *ptDayOfWeek, uint8_t *ptWeekNumber, uint8_t *ptSummertime, uint8_t *ptHoliday) {}

void ICPVMCHook_GetManufactureCode(uint8_t *ptData, uint8_t nDataLen) {
  ptData[0] = 'M';
  ptData[1] = 'H';
  ptData[2] = 'D';
}

CreditCpcValue ICPVMCHook_GetMaxPrice(void) {
	if(!MaxPrice) MaxPrice = 0xFFFF;
  return MaxPrice;
}

CreditCpcValue ICPVMCHook_GetMinPrice(void) {
  return MinPrice;
}

void ICPVMCHook_GetModelNumber(uint8_t *ptData, uint8_t nDataLen) {
  memset(ptData, '0', nDataLen);
}

void ICPVMCHook_GetSerialNumber(uint8_t *ptData, uint8_t nDataLen) {
  memset(ptData, '0', nDataLen);
}

uint16_t ICPVMCHook_GetSwVersion(void) {
	return kORIONVersion;
}

void ICPVMCHook_GetTime(uint8_t *ptHours, uint8_t *ptMinutes, uint8_t *ptSeconds) {
}

bool ICPVMCHook_GetMultiVend(void) {
  return MultiVend;
}
