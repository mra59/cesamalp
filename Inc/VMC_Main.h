/****************************************************************************************
File:
    VMC_Main.h

Description:
    Include file con definizioni e strutture per il Main CPU

History:
    Date       Aut  Note
    Apr 2019	MR   

 *****************************************************************************************/

#ifndef __VMC_Main__H
#define __VMC_Main__H

#include "ORION.H"

// ===  definizione variabili e strutture   ===
typedef struct {
	Tmr32Local	MainTime;																	// Timer di scansione Main
	Tmr32Local	ToutRTC;																	// Timer per eseguire il controllo dell'ora legale/solare
	Tmr32Local	LCDRefreshTime;																// Timer di scansione Display LCD
	Tmr32Local	GetStatusVMC;																// Timer di interrogazione I/O per avere lo stato
	Tmr32Local	TimeInitMsg;																// Tempo visualizzazione messaggi di init
} MAINTimes;

//------------------------------------------------------------------------------------------

void  MAIN_CPU(void);
void  Time_Init(uint32_t TimeDispl);
void  GestioneUSB_Host_Device(void);
bool  Is_Machine_StBy(void);
static void  ProgKeyToClearFails(void);

#endif