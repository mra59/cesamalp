/****************************************************************************************
File:
    RTC_utility.h

Description:
    Include file con definizioni e strutture per l'uso del Real Time Clock.

History:
    Date       Aut  Note
    Gen 2013	abe   

 *****************************************************************************************/

#ifndef __RTC_utility__H
#define __RTC_utility__H

void		LeggiOrologio(void);
void		Ora_LS_align(LDD_RTC_TTime *dh);
char		LegalTime(uint32_t iYear, uint32_t iMonth, uint32_t iDay, uint32_t iHour, uint32_t iDayW);
void  		FromSecondsToDateTime(uint32_t, uint32_t *data, uint *time);
bool  		IsPeriodElapsed(DATEFREE *);
bool		Ckeck_SystemClock(void);
bool  		IsPeriodElapsed(DATEFREE *DataCarta);

#define		BKP_SOLARE			0
#define		BKP_LEGALE			1

#define STM32_RTC_JANUARY		1
#define STM32_RTC_FEBRUARY		2 
#define STM32_RTC_MARCH         3 
#define STM32_RTC_APRIL         4 
#define STM32_RTC_MAY           5 
#define STM32_RTC_JUNE          6 
#define STM32_RTC_JULY          7 
#define STM32_RTC_AUGUST        8 
#define STM32_RTC_SEPTEMBER     9 
#define STM32_RTC_OCTOBER       10 
#define STM32_RTC_NOVEMBER      11 
#define STM32_RTC_DECEMBER      12 

#define STM32_RTC_MONDAY 		01
#define STM32_RTC_TUESDAY       02
#define STM32_RTC_WEDNESDAY     03
#define STM32_RTC_THURSDAY      04
#define STM32_RTC_FRIDAY        05
#define STM32_RTC_SATURDAY      06
#define STM32_RTC_SUNDAY        07



#endif	// End __RTC_utility__H