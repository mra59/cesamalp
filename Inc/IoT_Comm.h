/****************************************************************************************
File:
    IoT_Comm.h

Description:
    Include file con definizioni e strutture per la comunicazione con Gateway ST.

History:
    Date       Aut  Note
    Dic 2018	MR   

 *****************************************************************************************/

#ifndef __IoT_Comm_H
#define __IoT_Comm_H


/*--------------------------------------------------------------------------------------*\
Private Declarations 
\*--------------------------------------------------------------------------------------*/

#define	TRANSAC_ID_LEN					36					// Massimo numero di chr che costituiscono il TransactionIDValue
#define	REMOTE_EROGAZ_ANSW_BUFSIZE		256
	
#define	PK3_FW_REV_LEN					4
#define	PK3_PROD_CODE_LEN				11
#define	TX_GTWY_MAX_ATTEMPT_COUNT		200
	
#define	BUFF_TYPE_0_SIZE				32	
#define	BUFF_TYPE_1_SIZE				128	

#define	ONE_TOTAL_BLOCKS				0x01
#define	ONE_BLOCK_NUMBER				0x01

// **** Settings  Comunicazione  *****
#define	TX_GTWY_BUF_SIZE				(2000-32)				// Numero di caratteri da trasmettere
#define	RX_GTWY_BUF_SIZE				2000				// Numero di caratteri da ricevere

#define	AWS_CMD_OK						0x00
#define	AWS_CMD_REJECTED				0x01
#define	LASTBLOCK						0xff
#define	TIME_TO_SEND_DATA_BLOCK			TwoSec

#define	NEW_AWS_SECURE_CERTIF_BIT		0x01
#define	NEW_AWS_PRIVATE_KEY_BIT			0x02
#define	NEW_AWS_DEVICE_BIT				0x04
#define	NEW_AWS_ACCOUNT_SERVER_ADDR_BIT	0x08



typedef enum {
	Gtwy_IDLE,
	Gtwy_IOAddress,
	Gtwy_BuffLen_LSB,
	Gtwy_BuffLen_MSB,
	Gtwy_START_RX,
	Gtwy_Finish,
	Gtwy_NoSync
}Gtwy_STAT;
	
typedef enum {
	Cmd_NoInitState,										// Uart non inizializzata, non utilizzarla
	Cmd_IDLE,
	Cmd_WaitACK,
	Cmd_WaitAnswer
}Cmd_STAT;

typedef enum
{
	IoT_POLL,												// 0
	IoT_reset,			/* Non usato */						// 1
	IoT_TX_BUFFER_TO_AWS,									// 2
	IoT_NEW_AWS_SECURE_CERTIF,								// 3
	IoT_NEW_AWS_PRIVATE_KEY,								// 4
	IoT_NEW_AWS_ACCOUNT_SERVER_ADDRESS,						// 5
	IoT_NEW_AWS_DEVICE,										// 6
	IoT_RESET = 0x80
}IOT_COMM_STATE;


typedef struct {
	Tmr32Local	TimeAudSales;								// Tempo di controllo per invio Audit Sales
} IOT_TIMES;


typedef struct {
	Gtwy_STAT		Gtwy_State;
	Cmd_STAT		CMD_State;								// Macchina a stati per RTx Comandi
	char			Gtwy_TxBuff[TX_GTWY_BUF_SIZE];
	uint8_t			Gtwy_RxBuff[RX_GTWY_BUF_SIZE];
	uint16_t		CommandTxCnt;							// Counter Numero di trasmissioni del comando alla Gateway
	uint8_t			RxErr;
	uint8_t			TxBusy;
	uint16_t		pntRxGtwy;								// Pointer al buffer di ricezione
	bool			BuffComplete;
	bool			SendStatusMsgToAWS;
	bool			SendRemoteErogazAnswToAWS;
	Tmr32Local		GtwyTime;								// Timer di scansione comunicazione con Gateway
} RTxVars;

extern RTxVars gCommVars;

//   Definizioni  
#define	StartRxCmd				0x02							// Comando inizio ricezione buffer dati
#define	VendOK					0x00
#define	VendFail				0x80
#define	ACK						0x88
#define	NACK					0x77
#define	JustReset				0x44
#define	TOUT_WAIT_ANSWER		Milli200
#define	STATUS_PERIODIC_TIME	FIVEMIN
#define	LONG_TOUT_WAIT_ANSWER	TwoSec

	
#if DEB_AUDITSALES
	#define	TIME_PERIODIC_CHECK		OneSec
#else
	#define	TIME_PERIODIC_CHECK		OneMin
#endif


#define	DummyChar				0x69							// Valore per chr non usati
#define	SecondLastChr			0x33							// Penultimo chr di ogni comunicazione
#define	LastChr					0xaa							// Ultimo chr di ogni comunicazione

/*
//  ****  Definizioni bit Status Byte   ******
#define	PK3_SendBuffType0			0x00					// Invia POLL periodico alla Gateway
#define	PK3_SendBuffType1			0x01					// Invia FileType 1 di INIT alla Gateway
#define	PK3_TXINIT_CCT				0x02					// Invia FileType 2 di INIT alla Gateway con i parametri ccTalk
#define	PK3_TXBUFCREDIT_CCT			0x04					// New buffer credit ccTalk da inviare alla Gateway
#define	PK3_WAITBUFCREDANSW_CCT		0x08					// Wait Risposta dalla Gateway al New buffer credit ccTalk inviato
*/


//  ****  Definizioni Gateway Status Byte   ******
/*
#define	GTWY_READY		0x00							// Gateway Ready
#define	GTWY_IDLE		0x01							// Gateway IDLE
#define	GTWY_CONNECTED	0x02							// Gateway Connected
#define	GTWY_BUSY		0x03							// Gateway Busy
#define	GTWY_WARMUP		0x04							// Gateway Warm-up
#define	GTWY_RESET		0x80							// Just Reset
*/

typedef enum
{
	GTWY_READY,				// 0
	GTWY_IDLE,				// 1
	GTWY_CONNECTED,			// 2
	GTWY_BUSY,				// 3
	GTWY_WARMUP,			// 4
	GTWY_RESET = 0x80
}GW_STAT;


//  ****  Definizioni Risposte Gateway     ******
#define	GTWY_ACK		ASCII_ZERO						// Risposta ACK della Gateway al buffer inviato dalla CPU
#define	GTWY_NACK		ASCII_ONE						// Risposta NACK della Gateway al buffer inviato dalla CPU


// **** Settings  Buffer Types  *****
#define	BUFFER_TYPE_IX		0								// Indice al buffer ricevuto
#define	BUFFER_FIRST_DATA	(BUFFER_TYPE_IX + 1)			// Indice al buffer ricevuto


#define	BUFFER_TYPE_0		ASCII_ZERO
#define	BUFFER_TYPE_1		ASCII_ONE
#define	BUFFER_TYPE_2		ASCII_TWO
#define	BUFFER_TYPE_3		ASCII_THREE
#define	BUFFER_TYPE_4		ASCII_FOUR
#define	BUFFER_TYPE_5		ASCII_FIVE
#define	BUFFER_TYPE_6		ASCII_SIX
#define	BUFFER_TYPE_7		ASCII_SEVEN


/*
#define	PosGtwyReqPrice1	19								// Posizione del byte GtwyReqPrice1 nel buffer Type Zero
#define	PosGtwyReqPrice2	23								// Posizione del byte GtwyReqPrice2 nel buffer Type Zero
#define	PosGtwyStatus		27								// Posizione del byte GtwyStatus nel buffer Type Zero
#define	PosGtwyCommand		29								// Posizione del byte GtwyCommand nel buffer Type Zero
#define	PosccTalkINH_NoLINK	31								// Posizione del byte INH+NoLINK ccTalk nel buffer Type Zero
#define	PosGtwyPntOUT		1								// Posizione del byte PntOUT nella risposta della IoT al buffer Type Due
*/

#define	POSITION_GTWY_REQPRICE1		19								// Posizione del byte GtwyReqPrice1 nel buffer Type Zero
#define	POSITION_GTWY_REQPRICE2		23								// Posizione del byte GtwyReqPrice2 nel buffer Type Zero
#define	POSITION_GTWY_STATUS		27								// Posizione del byte GtwyStatus nel buffer Type Zero
#define	POSITION_GTWY_COMMAND										// Posizione del byte GtwyCommand nel buffer Type Zero
#define	POSITION_CCTALK_INH_NOLINK	31								// Posizione del byte INH+NoLINK ccTalk nel buffer Type Zero
#define	POSITION_GTWY_PNTOUT		1								// Posizione del byte PntOUT nella risposta della IoT al buffer Type Due


//  ****  Definizioni Comandi ricevuti dalla Gateway   ******
#define	GTW_AUDREQ_WOUT_RES		0x01						// Comando Richiesta Audit senza reset da AWS
#define	GTW_AUDREQ_W_RES		0x02						// Comando Richiesta Audit con   reset da AWS
#define	GTWCMD_NEWPRICE			0x05						// Comando Cambio Prezzi
#define	GTWCMD_NEWPRICE			0x05						// Comando Cambio Prezzi
#define	GTWCMD_INH				0x06						// Comando Inibizione sistema di pagamento
#define	GTWCMD_ENA				0x07						// Comando Abilitazione sistema di pagamento


#define	FixBytesNum			6								// Numero di bytes fissi nel buffer (STX + BufferType + * + CRCH + CRCL + ETX)
#define	BuffTypeZero_Len	FixBytesNum + 21
#define	BuffTypeUno_Len		FixBytesNum + 101
#define	CodiceMacchina_Len	8								// Lunghezza stringa codice macchina
#define	INH_Gett			1
#define	ENA_Gett			0

//-- Posizione bits ccTalk nella comunicazione con la Gateway --
//#define	FLAG_INH_CCT		0x01
//#define	FLAG_NOLINK_CCT		0x02
#define	FLAG_START_PULSE	0x04
//#define	FLAG_RESET_CCT		0x08
#define	FLAG_AUDITSENT		0x10

/* Definizioni della scheda Gateway MH620
#define	POS_INHIBIT_CCT		0
#define	POS_NOLINK_CCT		1
#define	POS_START_PULSE		2
#define	POS_RESET_CCT		3
#define	POS_AUDITSENT		4
*/

/*--------------------------------------------------------------------------------------*\
Functions Prototypes 
\*--------------------------------------------------------------------------------------*/

void  			Task_Gtwy(void);
IOT_COMM_STATE  Gtwy_Poll_Answ(void);	
uint8_t  		Gtwy_SendBlock(uint16_t Size);
void  			Gtwy_Poll(void);
void  			Gtwy_RxData(uint8_t Data);
static uint8_t	SetLRCBuff_Gtwy(char *destbuff, uint16_t buff_size);
static bool 	Check_LRC(uint8_t *rdbuffPnt, uint16_t buffsize, uint8_t sourcecrc);
static bool  	WriteAuditBufferIntoEEprom(void);
void 			Gtwy_Comm_Init(void);

#if AWS_CONFIG
	static 	void  CheckForGTWYReset(void);
			void  ClrConfigAWS(uint8_t  BuffConfigAWS);
#endif
	
/*--------------------------------------------------------------------------------------*\
Public Declarations 
\*--------------------------------------------------------------------------------------*/

extern	char 		TransactionIDValue[TRANSAC_ID_LEN];
extern	uint8_t		GTW_command;
extern	uint16_t 	ts_milliseconds;
extern	char		RemoteErogazAnswerBuffer[REMOTE_EROGAZ_ANSW_BUFSIZE];


#endif		// __IoT_Comm_H


