/****************************************************************************************
File:
    ICPCpc.c

Description:
    Protocollo ICP per Cashless

History:
    Date       Aut  Note
    Set 2012 	Abe  

*****************************************************************************************/

#include <string.h>
#include <intrinsics.h>

#include "ORION.H"
#include "icpProt.h"
#include "icpCPCProt.h"


#ifndef kModifMDB_RM33Inhibited																	// Date/Time Sync Req
#define kModifMDB_RM33Inhibited true															// Non richiedere Date/Time sync al VMC
#endif

#if (_IcpSlave_ & ICP_CARDREADER)
	#ifndef kModifMDB_PatchBorsellinoCASH														// Richiesta di Sovera per borsellino CASH
	#define kModifMDB_PatchBorsellinoCASH true
#endif


T_ICP_CPCINFO 	ICPCPCInfo;
bool 			fSessionCancel;

#define DebugICP 0

/*---------------------------------------------------------------------------------------------*\
Method: CPCInit
	Funzione di inizializzazione variabili per cashless.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void CPCInit(uint8_t nSlaveType) {

	ICPProt_InitSlave(nSlaveType);
	memset(&ICPCPCInfo, 0, sizeof(T_ICP_CPCINFO));
	#if (_IcpMaster_ == true)
	ICPCPCInfo.nWaitInitCnt = ICP_TM_CPCWAITINIT / ICP_TM_POLLPERIPH;
	ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
	ICPInfo.nSlaveLinkMask &= ~icpSlvLinkMask;
	#endif
	#if (_IcpSlaveCode_ & ICP_CARDREADER)
	ICPCPCInfo.nInfoMask = ICP_CPCINFO_NONE;
	ICPProt_CPCSetEvtMask(ICP_CPCINFO_JUSTRESET);
	Set_fVMC_Inibito;
	ICPSetFrameSent(false);
	ICPSetFrameAcked(false);
	ICPSetNewState(-1);
	ICPSetNewSubState(0);
	CPCSetLastEvent(0);
	#endif
	fSessionCancel = false;
	ICPHook_DateTimeSet(nil);																	// Date/Time Sync Req
	ICPCPCHook_CheckOrionState();
}


//-------------------------------------------------
//
//					C P C	Slave Code
//
//-------------------------------------------------
#if (_IcpSlaveCode_ & ICP_CARDREADER)
void CPCSlaveResp(void) {
	uint8_t nRespType;
	//
	// Check if Retry have to do...
	//
	if(ICPGetFrameSent) {
		if(ICPGetFrameAcked) {
			//
			// Last frame sent is ok, reset last event && change to new state
			//
			ResetCPCInfoMask(CPCGetLastEvent);
			if(ICPGetNewState != -1)
				ICPProt_ChangeState(icpSlv, ICPGetNewState, ICPGetNewSubState);
			ICPSetFrameSent(false);
			ICPSetFrameAcked(false);
			ICPSetNewState(-1);
			ICPSetNewSubState(0);
			CPCSetLastEvent(0);
		}
	}
	if(ICPDrvGetComState() == COM_RXFRAME) {
		#if (_IcpSmsAlarms_ == true)
			//if(!ICPCPCHook_IsEnabled()) {
				switch(ICPCommand) {
				case ICP_CPC_CMD_RESET:
					ICPProt_CPCResetResp();
					break;
				case ICP_CPC_CMD_READERDISABLE:
					ICPProt_CPCReader00Resp();
					break;
				case ICP_CPC_CMD_READERENABLE;
					ICPProt_CPCReader01Resp();
					break;
				default:
					break;
				}
				return;
			//}
		#endif

		switch(ICPCommand) {
		//----------------------//
		//	RESET command
		//----------------------//
		case ICP_CPC_CMD_RESET:
			ICPProt_CPCResetResp();
			//
			// Jump to INACTIVE State!
			//
			ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_WAITSETUP);
			break;

		//----------------------//
		//	SETUP commands
		//----------------------//
		case ICP_CPC_CMD_SETUP:
			switch(icpSlvState) {
			case ICP_CPC_VEND:
			case ICP_CPC_REVALUE:
				// **offending cmd**
				SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
				ICPDrvSendRW(ICP_RW_ACK);
				break;
			default:
				if(ICPSubCommand == ICP_CPC_CMD_SETUPID) {
					icpSlvSubState |= ICP_CPC_INACTIVE_RXSETUPID;
					ICPProt_CPCSetup00Resp();
				} else {
					if(ICPSubCommand == ICP_CPC_CMD_SETUPMAXMINPRICE) {
						icpSlvSubState |= ICP_CPC_INACTIVE_RXSETUPMMP;
						ICPProt_CPCSetup01Resp();
					}
				}
				if(icpSlvState == ICP_CPC_INACTIVE)
					if(icpSlvSubState == (ICP_CPC_INACTIVE_RXSETUPID + ICP_CPC_INACTIVE_RXSETUPMMP)) {
						//
						// BOTH SetupConfig && SetupMaxMinPrices received!
						// Jump to DISABLED State!
						//
						ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, 0);
					} else {
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, icpSlvSubState);
					}
				break;
			}
			break;

		//----------------------//
		//	POLL command
		//----------------------//
		case ICP_CPC_CMD_POLL:
			ICPProt_CPCPollResp();
			break;

		//----------------------//
		//	VEND commands
		//----------------------//
		case ICP_CPC_CMD_VEND:
			switch(icpSlvState) {
			//
			//	Vend cmds in SESSION IDLE State
			//
			case ICP_CPC_SESSIDLE:
				switch(ICPSubCommand) {
				case ICP_CPC_CMD_VENDREQUEST:
				case ICP_CPC_CMD_NEGVENDREQUEST:
					//
					// Jump to VEND State!
					//
					ICPProt_ChangeState(icpSlv, ICP_CPC_VEND, ICP_CPC_VEND_RXVENDREQ);
					if(ICPSubCommand == ICP_CPC_CMD_VENDREQUEST)
						nRespType = ICPProt_CPCVend00Resp();
					else
						nRespType = ICPProt_CPCVend06Resp();
					switch(nRespType) {
					case ICP_CPC_RESPDENY:
						ICPProt_ChangeState(ICPInfo.nCurrPeriph, ICP_CPC_SESSIDLE, 0);
						break;
					case ICP_CPC_RESPAPP:
						ICPProt_ChangeState(ICPInfo.nCurrPeriph, ICP_CPC_VEND, ICP_CPC_VEND_WAITVENDSUCCFAIL);
						break;
					case ICP_CPC_WAITRESP:
						// wait... tx vend deny/approved at Poll response!
					default:
						break;
					}
					break;
				case ICP_CPC_CMD_VENDCOMPLETE:
					//
					// Jump to ENABLED State!
					//
					// 08/03/02 modifica per risolvere difetto del Vmc Vendo:
					//			non accetta EndSession al cmd Session Complete ma solo al cmd Poll
					/*ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, 0);
					ICPProt_CPCVend04Resp();*/
					// 12/03/02 modifica per Vmc Vendo NON va pi� bene per Vmc FAS
					//			...e probabilmente altri!
					/*SetCPCInfoMask(ICP_CPCINFO_ENDSESS);
					ICPDrvSendRW(ICP_RW_ACK);*/
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, 0);
					ICPProt_CPCVend04Resp();
					break;
				case ICP_CPC_CMD_VENDCANCEL:
				case ICP_CPC_CMD_VENDSUCCESS:
				case ICP_CPC_CMD_VENDFAILURE:
					// **unexecutable cmd**
					SetCPCInfoMask(ICP_CPCINFO_SEQCMDERR);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				case ICP_CPC_CMD_VENDCASHSALE:
					ICPProt_CPCVend05Resp();
					break;
				default:
					//	Invalid sub-commands: no response!
					break;
				}
				break;

			//
			//	Vend cmds VEND State
			//
			case ICP_CPC_VEND:
				switch(ICPSubCommand) {
				case ICP_CPC_CMD_NEGVENDREQUEST:
				case ICP_CPC_CMD_VENDREQUEST:
				case ICP_CPC_CMD_VENDCOMPLETE:
				case ICP_CPC_CMD_VENDCASHSALE:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				case ICP_CPC_CMD_VENDCANCEL:
					if(icpSlvSubState == ICP_CPC_VEND_RXVENDREQ) {
						//
						// Jump to SESSIDLE State!
						//
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, 0);
						ICPProt_CPCVend01Resp();
					} else {
						// **offending cmd**
						SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
						ICPDrvSendRW(ICP_RW_ACK);
					}
					break;
				case ICP_CPC_CMD_VENDSUCCESS:
				case ICP_CPC_CMD_VENDFAILURE:
					if(icpSlvSubState == ICP_CPC_VEND_WAITVENDSUCCFAIL) {
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, 0);
#if DebugICP
						ICPSubCommand=0x03;			//Simulo fail vend
						
#endif
						if(ICPSubCommand == ICP_CPC_CMD_VENDSUCCESS)
							ICPProt_CPCVend02Resp();
						else
							ICPProt_CPCVend03Resp();
						if(!ICPProt_CPCGetMultiVend())
							SetCPCInfoMask(ICP_CPCINFO_SESSCANCREQ);
					} else {
						// **offending cmd**
						SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
						ICPDrvSendRW(ICP_RW_ACK);
					}
					break;
				default:
					//	Invalid sub-commands: no response!
					break;
				}
				break;

			//
			//	Vend cmds in DISABLED/ENABLED States
			//
			case ICP_CPC_DISABLED:
			case ICP_CPC_ENABLED:
				switch(ICPSubCommand) {
				case ICP_CPC_CMD_NEGVENDREQUEST:
				case ICP_CPC_CMD_VENDREQUEST:
				case ICP_CPC_CMD_VENDCANCEL:
				case ICP_CPC_CMD_VENDSUCCESS:
				case ICP_CPC_CMD_VENDFAILURE:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				case ICP_CPC_CMD_VENDCOMPLETE:
					// 08/03/02 modifica per risolvere difetto del Vmc Vendo:
					//			non accetta EndSession al cmd Session Complete ma solo al cmd Poll
					/*ICPProt_CPCVend04Resp();*/
					// 12/03/02 modifica per Vmc Vendo NON va pi� bene per Vmc FAS
					//			...e probabilmente altri!
					/*SetCPCInfoMask(ICP_CPCINFO_ENDSESS);
					ICPDrvSendRW(ICP_RW_ACK);*/
					ICPProt_CPCVend04Resp();
					break;
				case ICP_CPC_CMD_VENDCASHSALE:
					ICPProt_CPCVend05Resp();
					break;
				default:
					//	Invalid sub-commands: no response!
					break;
				}
				break;

			//
			//	Vend cmds in INACTIVE/REVALUE States
			//
			case ICP_CPC_REVALUE:
			case ICP_CPC_INACTIVE:
				// **offending cmd**
				SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
			default:
				// **unexecutable cmd**
				SetCPCInfoMask(ICP_CPCINFO_SEQCMDERR);
				ICPDrvSendRW(ICP_RW_ACK);
				break;
			}
			break;

		//----------------------//
		//	READER commands
		//----------------------//
		case ICP_CPC_CMD_READER:
			switch(ICPSubCommand) {
			case ICP_CPC_CMD_READERDISABLE:
// RM46_2006: PK Modifiche 19/10/2006
//#ifdef kModifMDB_KeyInOut
/*
				switch(icpSlvState) {
				case ICP_CPC_SESSIDLE:
				case ICP_CPC_VEND:
				case ICP_CPC_REVALUE:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				case ICP_CPC_ENABLED:
					//
					// Jump to DISABLED State!
					//
					ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, 0);
				case ICP_CPC_INACTIVE:
				case ICP_CPC_DISABLED:
				default:
					ICPProt_CPCReader00Resp();
					break;
				}
*/
				switch(icpSlvState) {
				case ICP_CPC_ENABLED:
					//
					// Jump to DISABLED State!
					//
					ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, 0);
					ICPProt_CPCReader00Resp();
					break;
				case ICP_CPC_DISABLED:
					// ignore this cmd!
					// no notification to application level!
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				default:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				}
//#endif
				break;
			case ICP_CPC_CMD_READERENABLE:
// RM46_2006: PK Modifiche 19/10/2006
//#ifdef kModifMDB_KeyInOut
/*
				switch(icpSlvState) {
				case ICP_CPC_INACTIVE:
				case ICP_CPC_VEND:
				case ICP_CPC_REVALUE:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				case ICP_CPC_DISABLED:
					//
					// Jump to ENABLED State!
					//
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, 0);
				case ICP_CPC_ENABLED:
				case ICP_CPC_SESSIDLE:
				default:
					ICPProt_CPCReader01Resp();
					break;
				}
*/
				switch(icpSlvState) {
				case ICP_CPC_DISABLED:
					//
					// Jump to ENABLED State!
					//
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, 0);
					ICPProt_CPCReader01Resp();
					break;
				case ICP_CPC_ENABLED:
					// ignore this cmd!
					// no notification to application level!
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				default:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				}
//#endif
				break;
			case ICP_CPC_CMD_READERCANCEL:
				switch(icpSlvState) {
				case ICP_CPC_SESSIDLE:
				case ICP_CPC_VEND:
				case ICP_CPC_REVALUE:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				case ICP_CPC_INACTIVE:
				case ICP_CPC_DISABLED:
				case ICP_CPC_ENABLED:
				default:
					ICPProt_CPCReader02Resp();
					break;
				}
				break;
			// occhio: livello 3!!!
			case ICP_CPC_CMD_READERDATAENTRY:
				//ICPProt_CPCReader03Resp();
			default:
				//	Invalid commands: no response!
				break;
			}
			break;

		//----------------------//
		//	REVALUE commands
		//----------------------//
		case ICP_CPC_CMD_REVALUE:
			switch(ICPSubCommand) {
			case ICP_CPC_CMD_REVALUEREQUEST:
				switch(icpSlvState) {
				case ICP_CPC_VEND:
				case ICP_CPC_REVALUE:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				case ICP_CPC_SESSIDLE:
					//
					// Jump to REVALUE State!
					//
					ICPProt_ChangeState(icpSlv, ICP_CPC_REVALUE, 0);
					ICPProt_CPCRevalue00Resp();
					break;
				case ICP_CPC_INACTIVE:
				case ICP_CPC_DISABLED:
				case ICP_CPC_ENABLED:
				default:
					{
						T_CPCRESP_REVAL00 *ptRespR00;
						ptRespR00 = (T_CPCRESP_REVAL00 *)&ICPInfo.buff[0];
						ptRespR00->nRevalDenied = ICP_CPC_REVALDENY;
						ICPInfo.nFrameLen = sizeof(ptRespR00->nRevalDenied);
						ICPDrvSendMsg(ICPInfo.nFrameLen);
					}
					break;
				}
				break;
			case ICP_CPC_CMD_REVALUELIMIT:
				switch(icpSlvState) {
				case ICP_CPC_VEND:
				case ICP_CPC_REVALUE:
					// **offending cmd**
					SetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ);
					ICPDrvSendRW(ICP_RW_ACK);
					break;

				case ICP_CPC_SESSIDLE:
					ICPProt_CPCRevalue01Resp();
					break;

				case ICP_CPC_INACTIVE:
				case ICP_CPC_DISABLED:
				case ICP_CPC_ENABLED:
				default:
					SetCPCInfoMask(ICP_CPCINFO_REVALDENY);
					ICPDrvSendRW(ICP_RW_ACK);
					break;
				}
				break;
			default:
				break;
			}
			break;

		//----------------------//
		//	EXPANSION commands
		//----------------------//
		case ICP_CPC_CMD_EXP:
			switch(ICPSubCommand) {
			case ICP_CPC_CMD_EXPREQUESTID:
				ICPProt_CPCExp00Resp();
				break;
			case ICP_CPC_CMD_EXPWRITETIMEDATE:
				ICPProt_CPCExp03Resp();
				break;
			// occhio: livello 3!!!
			case ICP_CPC_CMD_EXPENABLEOPT:
				ICPProt_CPCExp04Resp();
				break;
			case ICP_CPC_CMD_EXPDIAG:
				ICPProt_CPCExpFFResp();
				break;
			case ICP_CPC_CMD_EXPREADUSERFILE:
			case ICP_CPC_CMD_EXPWRITEUSERFILE:
			default:
				//	Invalid commands: no response!
				break;
			}
			break;

		//
		//	Invalid commands: no response!
		//
		default:
			break;
		}

		// If command not handled no following commands recognized!
		if(ICPDrvGetComState() == COM_RXFRAME) {
			// if rxframe not handled, no tx data, then discard it
			ICPDrvSetComState(COM_IDLE);
		}
		
	}
}

void GetCPCSlaveStatus(void) {
	uint8_t nLevel;
	CPCCreditType_L3 nAmount;

	/*icpSlv = ICPInfo.nCurrPeriph;
	icpSlvState = ICPInfo.aPeriphInfo[icpSlv].nState;
	icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/


	//
	// Check if Retry has to be done...
	//
	if(ICPGetFrameSent) {
		if(ICPGetFrameAcked == false) {
			//
			// Retry to send last frame if ACK not received from Master
			//
			ICPInfo.nFrameLen = ICPLoadSlaveFrameSent();
			ICPDrvSendMsg(ICPInfo.nFrameLen);
			return;
		} else {
			//
			// Last frame sent is ok, reset last event && change to new state
			//
			ResetCPCInfoMask(CPCGetLastEvent);
			if(ICPGetNewState != -1)
				ICPProt_ChangeState(icpSlv, ICPGetNewState, ICPGetNewSubState);
		}
	}
	ICPSetFrameSent(false);
	ICPSetFrameAcked(false);
	ICPSetNewState(-1);
	ICPSetNewSubState(0);
	CPCSetLastEvent(0);
//***********************


	ICPInfo.nFrameLen = 0;
	if(IsSetCPCInfoMask(ICP_CPCINFO_JUSTRESET)) {
		//
		// Send 00 = JUST RESET
		//
		CPCSetLastEvent(ICP_CPCINFO_JUSTRESET);
		ICPInfo.buff[0] = ICP_CPC_JUSTRESET;
		ICPInfo.nFrameLen = 1;
//!		ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, 0);
	}

	if(!ICPInfo.nFrameLen && IsSetCPCInfoMask(ICP_CPCINFO_OUTOFSEQ)) {
		T_CPCRESP_POLL0B *ptResp0B;
		//
		// Send 0B = OUT OF SEQUENCE
		//
		CPCSetLastEvent(ICP_CPCINFO_OUTOFSEQ | ICP_CPCINFO_SEQCMDERR);
		ptResp0B = (T_CPCRESP_POLL0B *)&ICPInfo.buff[0];
		ptResp0B->poll0b.nPoll0B = ICP_CPC_OUTSEQ;
		ICPInfo.nFrameLen = sizeof(ptResp0B->poll0b.nPoll0B);
		if(ICPProt_GetLevel() > 1) {
			ptResp0B->poll0b.nCpcState = (icpSlvState +1);											// Invio anche lo status attuale
			ICPInfo.nFrameLen += sizeof(ptResp0B->poll0b.nCpcState);
		}
	}

	if(!ICPInfo.nFrameLen && IsSetCPCInfoMask(ICP_CPCINFO_SEQCMDERR)) {
		//
		// Send ACK to unexecutable cmd
		//
		ResetCPCInfoMask(ICP_CPCINFO_SEQCMDERR);
		ICPDrvSendRW(ICP_RW_ACK);
		return;
	}

	if(!ICPInfo.nFrameLen && IsSetCPCInfoMask(ICP_CPCINFO_MEDIAERR)) {
		T_CPCRESP_POLL0A *ptResp0A;
		//
		// Send 0A = MALFUNCTION ERROR:Payment Media Error
		//
		CPCSetLastEvent(ICP_CPCINFO_MEDIAERR);
		ptResp0A = (T_CPCRESP_POLL0A *)&ICPInfo.buff[0];
		ptResp0A->poll0a.nPoll0A = ICP_CPC_MALFUNCT;
		ptResp0A->poll0a.nErrorCode = 0;
		ICPInfo.nFrameLen = sizeof(ptResp0A->poll0a);
	}

	if(!ICPInfo.nFrameLen && IsSetCPCInfoMask(ICP_CPCINFO_REFUNDERR)) {
		T_CPCRESP_POLL0A *ptResp0A;
		//
		// Send 0A = MALFUNCTION ERROR:Refund Error
		//
		CPCSetLastEvent(ICP_CPCINFO_REFUNDERR);
		ptResp0A = (T_CPCRESP_POLL0A *)&ICPInfo.buff[0];
		ptResp0A->poll0a.nPoll0A = ICP_CPC_MALFUNCT;
		ptResp0A->poll0a.nErrorCode = 0xC0;
		ICPInfo.nFrameLen = sizeof(ptResp0A->poll0a);
	}

	// modifica per risolvere difetto del Vmc Vendo: non accetta EndSession al cmd Session Complete ma solo al cmd Poll
	// modifica per Vmc Vendo NON va pi� bene per Vmc FAS e probabilmente altri!
	/*if(!ICPInfo.nFrameLen && IsSetCPCInfoMask(ICP_CPCINFO_ENDSESS)) {
		ResetCPCInfoMask(ICP_CPCINFO_ENDSESS);
		ICPProt_CPCVend04Resp();
		ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, 0);
	}*/


	if(!ICPInfo.nFrameLen) {
		switch(icpSlvState) {
		case ICP_CPC_INACTIVE:
			break;

		case ICP_CPC_DISABLED:
			break;

		case ICP_CPC_ENABLED:
			if(IsSetCPCInfoMask(ICP_CPCINFO_SESSCANCREQ)) {										// Modifica per evitare Begin Session e subito dopo Session calcel Req
				ResetCPCInfoMask(ICP_CPCINFO_SESSCANCREQ);
			}
			nLevel = ICPProt_GetLevel();
			if(IsSetCPCInfoMask(ICP_CPCINFO_BEGINSESSION)) {

				T_CPCRESP_POLL03 *ptResp03;
			
				ptResp03 = (T_CPCRESP_POLL03 *)&ICPInfo.buff[0];
				CPCSetLastEvent(ICP_CPCINFO_BEGINSESSION);
				ICPCPCHook_GetMediaInfo(&nAmount,
										&ICPCPCInfo.FundInfo.nPaymentID,
										&ICPCPCInfo.FundInfo.nPaymentType,
										&ICPCPCInfo.FundInfo.nPaymentData,
										&ICPCPCInfo.FundInfo.nUserLanguage,
										&ICPCPCInfo.FundInfo.nUserCurrency,
										&ICPCPCInfo.FundInfo.nUserOptions);
				if(nAmount != 0xFFFFFFFF) {
					nAmount /= ICPCPCInfo.Setup00.nUSF;
				}
				ICPCPCInfo.FundInfo.nFunds = nAmount;
				switch(nLevel) {
				case 2:
					ptResp03->poll03.FunfInfo.L2.nFunds = nAmount;
					ptResp03->poll03.FunfInfo.L2.nPaymentID = ICPCPCInfo.FundInfo.nPaymentID;
					ptResp03->poll03.FunfInfo.L2.nPaymentType = ICPCPCInfo.FundInfo.nPaymentType;
					ptResp03->poll03.FunfInfo.L2.nPaymentData = ICPCPCInfo.FundInfo.nPaymentData;
					Cvt16ToBigEndian(&ptResp03->poll03.FunfInfo.L2.nFunds)
					Cvt32ToBigEndian(&ptResp03->poll03.FunfInfo.L2.nPaymentID)
					Cvt16ToBigEndian(&ptResp03->poll03.FunfInfo.L2.nPaymentData)
					ICPInfo.nFrameLen = sizeof(ptResp03->poll03.FunfInfo.L2);
					break;
				case 3:
					ptResp03->poll03.FunfInfo.L3.nFunds = nAmount;
					ptResp03->poll03.FunfInfo.L3.nPaymentID = ICPCPCInfo.FundInfo.nPaymentID;
					ptResp03->poll03.FunfInfo.L3.nPaymentType = ICPCPCInfo.FundInfo.nPaymentType;
					ptResp03->poll03.FunfInfo.L3.nPaymentData = ICPCPCInfo.FundInfo.nPaymentData;
					ptResp03->poll03.FunfInfo.L3.nUserLanguage = ICPCPCInfo.FundInfo.nUserLanguage;
					ptResp03->poll03.FunfInfo.L3.nUserCurrency = ICPCPCInfo.FundInfo.nUserCurrency;
					ptResp03->poll03.FunfInfo.L3.nUserOptions = ICPCPCInfo.FundInfo.nUserOptions;
					Cvt32ToBigEndian(&ptResp03->poll03.FunfInfo.L3.nFunds)
					Cvt32ToBigEndian(&ptResp03->poll03.FunfInfo.L3.nPaymentID)
					Cvt16ToBigEndian(&ptResp03->poll03.FunfInfo.L3.nPaymentData)
					Cvt16ToBigEndian(&ptResp03->poll03.FunfInfo.L3.nUserLanguage)
					Cvt16ToBigEndian(&ptResp03->poll03.FunfInfo.L3.nUserCurrency)
					ICPInfo.nFrameLen = sizeof(ptResp03->poll03.FunfInfo.L3);
					break;
				case 1:
				default:
					ptResp03->poll03.FunfInfo.L1.nFunds = nAmount;
					Cvt16ToBigEndian(&ptResp03->poll03.FunfInfo.L1.nFunds)
					ICPInfo.nFrameLen = sizeof(ptResp03->poll03.FunfInfo.L1);
					break;
				}
				ptResp03->poll03.nPoll03 = ICP_CPC_BEGSESS;
				ICPInfo.nFrameLen += sizeof(ptResp03->poll03.nPoll03);
				ICPSetNewState(ICP_CPC_SESSIDLE);
				ICPSetNewSubState(0);
			}
			break;

		case ICP_CPC_SESSIDLE:
			{
			T_CPCRESP_POLL04 *ptResp04;
			ptResp04 = (T_CPCRESP_POLL04 *)&ICPInfo.buff[0];
			if(IsSetCPCInfoMask(ICP_CPCINFO_SESSCANCREQ)) {
				CPCSetLastEvent(ICP_CPCINFO_SESSCANCREQ);
				ptResp04->nPoll04 = ICP_CPC_SESSCANC;
				ICPInfo.nFrameLen = sizeof(ptResp04->nPoll04);
			}
			}
			break;

		case ICP_CPC_VEND:
			if(icpSlvSubState == ICP_CPC_VEND_RXVENDREQ) {

				T_CPCRESP_VEND00 *ptRespV00;
				ptRespV00 = (T_CPCRESP_VEND00 *)&ICPInfo.buff[0];
				if(IsSetCPCInfoMask(ICP_CPCINFO_VENDAPP)) {
					CPCSetLastEvent(ICP_CPCINFO_VENDAPP);
					ICPCPCHook_GetVendAppAmount(&ICPCPCInfo.VendApproved.nVendAmount,
												&ICPCPCInfo.VendApproved.nVendCurrency);
					ICPCPCInfo.VendApproved.nVendAmount /= ICPCPCInfo.Setup00.nUSF;
					if(ICPProt_GetLevel() <= 2) {
						ptRespV00->vendapp.VendApproved.L1.nVendAmount = ICPCPCInfo.VendApproved.nVendAmount;
						Cvt16ToBigEndian(&ptRespV00->vendapp.VendApproved.L1.nVendAmount)
						ICPInfo.nFrameLen = sizeof(ptRespV00->vendapp.VendApproved.L1);
					} else {
						ptRespV00->vendapp.VendApproved.L3.nVendAmount = ICPCPCInfo.VendApproved.nVendAmount;
						ptRespV00->vendapp.VendApproved.L3.nVendCurrency = ICPCPCInfo.VendApproved.nVendCurrency;
						Cvt32ToBigEndian(&ptRespV00->vendapp.VendApproved.L3.nVendAmount)
						Cvt16ToBigEndian(&ptRespV00->vendapp.VendApproved.L3.nVendCurrency)
						ICPInfo.nFrameLen = sizeof(ptRespV00->vendapp.VendApproved.L3);
					}
					ptRespV00->vendapp.nVendApp = ICP_CPC_VENDAPP;
					ICPInfo.nFrameLen += sizeof(ptRespV00->vendapp.nVendApp);
					ICPSetNewState(ICP_CPC_VEND);
					ICPSetNewSubState(ICP_CPC_VEND_WAITVENDSUCCFAIL);
				} else {
					if(IsSetCPCInfoMask(ICP_CPCINFO_VENDDENY)) {
						CPCSetLastEvent(ICP_CPCINFO_VENDDENY);
						ptRespV00->nVendDenied = ICP_CPC_VENDDENY;
						ICPInfo.nFrameLen = sizeof(ptRespV00->nVendDenied);
						ICPSetNewState(ICP_CPC_SESSIDLE);
						ICPSetNewSubState(0);
					}
				}
			}
			break;

		case ICP_CPC_REVALUE:
			{
			T_CPCRESP_REVAL00 *ptRespR00;
			ptRespR00 = (T_CPCRESP_REVAL00 *)&ICPInfo.buff[0];
			if(IsSetCPCInfoMask(ICP_CPCINFO_REVALAPP)) {
				CPCSetLastEvent(ICP_CPCINFO_REVALAPP);
				ptRespR00->nRevalApp = ICP_CPC_REVALAPP;
				ICPInfo.nFrameLen = sizeof(ptRespR00->nRevalApp);
				ICPSetNewState(ICP_CPC_SESSIDLE);
				ICPSetNewSubState(0);
			} else {
				if(IsSetCPCInfoMask(ICP_CPCINFO_REVALDENY)) {
					CPCSetLastEvent(ICP_CPCINFO_REVALDENY);
					ptRespR00->nRevalDenied = ICP_CPC_REVALDENY;
					ICPInfo.nFrameLen = sizeof(ptRespR00->nRevalDenied);
					ICPSetNewState(ICP_CPC_SESSIDLE);
					ICPSetNewSubState(0);
				}
			}
			}
			break;
		default:
			break;
		}
	}

	if(!ICPInfo.nFrameLen && IsSetCPCInfoMask(ICP_CPCINFO_DISPLAYREQ)) {
	
		T_CPCRESP_POLL02 *ptResp02;
		// solo se display reso disponibile dal Vmc
		//
		// Send 02 = DISPLAYREQUEST
		//
		CPCSetLastEvent(ICP_CPCINFO_DISPLAYREQ);
		ptResp02 = (T_CPCRESP_POLL02 *)&ICPInfo.buff[0];
		ptResp02->poll02.nPoll02 = ICP_CPC_DISPREQ;
		ICPCPCHook_GetDispInfo(ptResp02->poll02.DispReq.aMsg, &ptResp02->poll02.DispReq.nMsgTime);
		ICPInfo.nFrameLen = sizeof(ptResp02->poll02);
	}


	// Date/time synchronization request
	if(!ICPInfo.nFrameLen && (icpSlvState == ICP_CPC_ENABLED)) {
		if(ICPHook_DateTimeSyncGet()) {
			T_CPCRESP_POLL11 *ptResp;
			ICPInfo.buff[0] = ICP_CPC_TIMEREQ;
			ICPInfo.nFrameLen = sizeof(ptResp->nPoll11);
		}
	}
	if(ICPInfo.nFrameLen) {
		ICPDrvSendMsg(ICPInfo.nFrameLen);
		//
		// Backup this frame needed in Retry process...
		//
		ICPSaveSlaveFrameSent(ICPInfo.nFrameLen);
		ICPSetFrameSent(true);
	} else {
		ICPDrvSendRW(ICP_RW_ACK);
	}
}
#endif


//-------------------------------------------------
//
//					I N A C T I V E
//
//-------------------------------------------------
void CPCInactive(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;
		bool fJustReset = false;

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CPC_INACTIVE)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CPC_INACTIVE_TXRESET:
					if(icpSlvCheckNoRespState <= 1) {
						ICPProt_CPCReset();
					}
					break;
				case ICP_CPC_INACTIVE_WAITINIT:
					if(!ICPProt_WaitInit(icpSlvAddr))
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_INACTIVE_TXPOLL_1);
					break;
				case ICP_CPC_INACTIVE_TXPOLL_1:
				case ICP_CPC_INACTIVE_TXPOLL_2:
					ICPProt_CPCPoll();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CPC_INACTIVE_TXRESET:
				nRespType = ICPProt_CPCResetResp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_INACTIVE_WAITINIT);
				break;
			case ICP_CPC_INACTIVE_TXPOLL_1:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, ICP_CPC_DISABLED_TXSETUPID);
					}
					break;
				case ICP_RESPTYPE_JUSTRESET:
#if kModifMDB_PatchBorsellinoCASH
	ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, ICP_CPC_DISABLED_TXSETUPID);
#endif
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, ICP_CPC_DISABLED_TXSETUPID);
					} else {
						ICPInfo.nSlaveJustResetMask |= icpSlvLinkMask;
					}
					break;
				default:
					break;
				}
				///ICPProt_ChangeState(icpSlv, -1, ICP_CPC_INACTIVE_TXPOLL_2);
				break;
			case ICP_CPC_INACTIVE_TXPOLL_2:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) fJustReset = true;
					break;
				case ICP_RESPTYPE_JUSTRESET:
					fJustReset = true;
					break;
				default:
					break;
				}
				if(fJustReset) {
					ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
					ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, ICP_CPC_DISABLED_TXSETUPID);
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	CPCSlaveResp();
#endif
}


//-------------------------------------------------
//
//					D I S A B L E D
//
//-------------------------------------------------
void CPCDisabled(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CPC_DISABLED)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CPC_DISABLED_TXSETUPID:
					ICPProt_SetReady(icpSlvAddr, false);
					ICPProt_CPCSetup00();
					break;
				case ICP_CPC_DISABLED_TXSETUPMMPRICE_1:
				case ICP_CPC_DISABLED_TXSETUPMMPRICE_2:
					ICPProt_CPCSetup01();
					break;
				case ICP_CPC_DISABLED_TXEXPID:
					ICPProt_CPCExp00();
					break;
				case ICP_CPC_DISABLED_TXEXPFOPT:
					ICPProt_CPCExp04();
					break;
				case ICP_CPC_DISABLED_TXVENDCASH:
					ICPProt_CPCVend05();
					break;
				case ICP_CPC_DISABLED_TXREADERENABLE_1:
				case ICP_CPC_DISABLED_TXREADERENABLE_2:
					ICPProt_CPCReader01();
					break;
				case ICP_CPC_DISABLED_WAITSETUPID:
				case ICP_CPC_DISABLED_WAITEXPID:
				case ICP_CPC_DISABLED_TXPOLL:
					ICPProt_CPCPoll();
					break;
				case ICP_CPC_DISABLED_TXEXPTIMEDATE:
					ICPProt_CPCExp03();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CPC_DISABLED_TXSETUPID:
				nRespType = ICPProt_CPCSetup00Resp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_WAITSETUPID);
					break;
				case ICP_CPC_RESPTYPE_SETUPID:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXSETUPMMPRICE_1);
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_DISABLED_TXSETUPMMPRICE_1:
				nRespType = ICPProt_CPCSetup01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXEXPID);
				break;
			case ICP_CPC_DISABLED_TXSETUPMMPRICE_2:
				nRespType = ICPProt_CPCSetup01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXREADERENABLE_1);
				break;
			case ICP_CPC_DISABLED_TXEXPID:
				nRespType = ICPProt_CPCExp00Resp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_WAITEXPID);
					break;
				case ICP_CPC_RESPTYPE_PERIPHID:
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXREADERENABLE_1);
						break;
					case 3:
					default:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXEXPFOPT);
						break;
					}
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_DISABLED_TXEXPFOPT:
				nRespType = ICPProt_CPCExp04Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXSETUPMMPRICE_2);
				break;
			case ICP_CPC_DISABLED_TXVENDCASH:
				nRespType = ICPProt_CPCVend05Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXPOLL);
				break;
			case ICP_CPC_DISABLED_TXREADERENABLE_1:
				nRespType = ICPProt_CPCReader01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
					ICPProt_SetReady(icpSlvAddr, true);
				}
				break;
			case ICP_CPC_DISABLED_TXREADERENABLE_2:
				nRespType = ICPProt_CPCReader01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENABLED);
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
				}
				break;
			case ICP_CPC_DISABLED_WAITSETUPID:
			case ICP_CPC_DISABLED_WAITEXPID:
			case ICP_CPC_DISABLED_TXPOLL:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
				case ICP_CPC_RESPTYPE_OUTSEQ:
					ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
					break;
				case ICP_CPC_RESPTYPE_TIMEREQ:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXEXPTIMEDATE);
					break;
				case ICP_CPC_RESPTYPE_SETUPID:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXSETUPMMPRICE_1);
					break;
				case ICP_CPC_RESPTYPE_PERIPHID:
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXREADERENABLE_1);
						break;
					case 3:
					default:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXEXPFOPT);
						break;
					}
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_CPC_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					case ICP_VMCCMD_CPC_CASHSALE:
						if(ICPProt_CPCGetCashSale())
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXVENDCASH);
						else
							ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					case ICP_VMCCMD_CPC_ENABLE:
						if(icpSlvSubState == ICP_CPC_DISABLED_TXPOLL) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXREADERENABLE_2);
						else ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				case ICP_CPC_RESPTYPE_DISPREQ:
					// AVT2800 ?!
				case ICP_CPC_RESPTYPE_MALFUNCT:
					// ?!
				default:
					break;
				}
				break;
			case ICP_CPC_DISABLED_TXEXPTIMEDATE:
				nRespType = ICPProt_CPCExp03Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_DISABLED_TXPOLL);
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	CPCSlaveResp();
#endif
}


//-------------------------------------------------
//
//					E N A B L E D
//
//-------------------------------------------------
void CPCEnabled(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CPC_ENABLED)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CPC_ENABLED_TXPOLL:
				case ICP_CPC_ENABLED_WAITENDSESSION:
				case ICP_CPC_ENABLED_WAITREVALUELIMIT:
				case ICP_CPC_ENABLED_WAITCANCELLED:
					ICPProt_CPCPoll();
					break;
				case ICP_CPC_ENABLED_TXSESSCOMP:
					ICPProt_CPCVend04();
					break;
				case ICP_CPC_ENABLED_TXVENDCASH:
					ICPProt_CPCVend05();
					break;
				case ICP_CPC_ENABLED_TXREADERDISABLE:
					ICPProt_CPCReader00();
					break;
				case ICP_CPC_ENABLED_TXREADERCANCEL:
					ICPProt_CPCReader02();
					break;
				case ICP_CPC_ENABLED_TXREVALLIM:
					ICPProt_CPCRevalue01();
					break;
				case ICP_CPC_ENABLED_TXEXPTIMEDATE:
					ICPProt_CPCExp03();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CPC_ENABLED_TXPOLL:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
				case ICP_CPC_RESPTYPE_OUTSEQ:
					ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
					break;
				case ICP_CPC_RESPTYPE_BEGSESS:
					//if(keyNotValid)
					//	ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXSESSCOMP);
					//else

					// 02.08.17 ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
					
					// 10.10.2019: la modifica del 2017 provoca l'invio del CMD Revalue Limit Request
					// anche ai cashless Level 1 che non supportano tale comando
					if (ICPCPCInfo.Setup00.nFLevel == 1)
					{
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
					}
					else
					{
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXREVALLIM);			// 02.08.17 Aggiunto per richiedere Revalue Limit all'entrata della carta
					}
					//	ICPBILLEnable(5);
					break;
				case ICP_CPC_RESPTYPE_SESSCANC:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXSESSCOMP);
					break;
				case ICP_CPC_RESPTYPE_TIMEREQ:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXEXPTIMEDATE);
					break;
				case ICP_RESPTYPE_RW_ACK:
					//test: ICPCPCRevalueLimit();
					//test: ICPCPCRevalueRequest(100, 0); 
					//test:	ICPCPCVendReq(100, 0xffff, 0);
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_CPC_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					case ICP_VMCCMD_CPC_DISABLE:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXREADERDISABLE);
						break;
					case ICP_VMCCMD_CPC_CANCEL:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXREADERCANCEL);
						break;
					case ICP_VMCCMD_CPC_CASHSALE:
						if(ICPProt_CPCGetCashSale())
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXVENDCASH);
						else
							ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					//30/05/01
					/*case ICP_VMCCMD_CPC_REVALUELIMIT:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXREVALLIM);
						break;
					*/
					
						case ICP_VMCCMD_CPC_REVALUELIMIT:
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXREVALLIM);
							break;

						
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				case ICP_CPC_RESPTYPE_DISPREQ:
					// AVT2800?!
				case ICP_CPC_RESPTYPE_MALFUNCT:
					// ?!
				default:
					break;
				}
				break;
			case ICP_CPC_ENABLED_WAITENDSESSION:
			case ICP_CPC_ENABLED_WAITREVALUELIMIT:
			case ICP_CPC_ENABLED_WAITCANCELLED:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
				case ICP_CPC_RESPTYPE_OUTSEQ:
					ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
					break;
				case ICP_CPC_RESPTYPE_REVALLIMIT:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUELIMIT);
				case ICP_CPC_RESPTYPE_ENDSESS:
				case ICP_CPC_RESPTYPE_CANCELLED:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_CPC_RESPTYPE_REVALDENY:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEDENY);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
					break;

				case ICP_RESPTYPE_RW_ACK:
					//test:	ICPCPCReset();
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_CPC_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;

				case ICP_CPC_RESPTYPE_DISPREQ:
					// AVT2800?!
				case ICP_CPC_RESPTYPE_MALFUNCT:
					// ?!?!
				default:
					break;
				}
				break;
			case ICP_CPC_ENABLED_TXSESSCOMP:
				nRespType = ICPProt_CPCVend04Resp();
				switch(nRespType) {
				case ICP_CPC_RESPTYPE_ENDSESS:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_WAITENDSESSION);
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_ENABLED_TXVENDCASH:
				nRespType = ICPProt_CPCVend05Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
				break;
			case ICP_CPC_ENABLED_TXREADERDISABLE:
				nRespType = ICPProt_CPCReader00Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_DISABLED);
					ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, ICP_CPC_DISABLED_TXPOLL);
				}
				break;
			case ICP_CPC_ENABLED_TXREADERCANCEL:
				nRespType = ICPProt_CPCReader02Resp();
				switch(nRespType) {
				case ICP_CPC_RESPTYPE_CANCELLED:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_CANCELLED);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_WAITCANCELLED);
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_ENABLED_TXREVALLIM:
				nRespType = ICPProt_CPCRevalue01Resp();
				switch(nRespType) {
				case ICP_CPC_RESPTYPE_REVALLIMIT:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUELIMIT);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_CPC_RESPTYPE_REVALDENY:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEDENY);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_WAITREVALUELIMIT);
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_ENABLED_TXEXPTIMEDATE:
				nRespType = ICPProt_CPCExp03Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_ENABLED_TXPOLL);
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	CPCSlaveResp();
#endif
}


//-------------------------------------------------
//
//					S E S S I D L E
//
//-------------------------------------------------
void CPCSessionIdle(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CPC_SESSIDLE)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CPC_SESSIDLE_TXPOLL:
				case ICP_CPC_SESSIDLE_WAITENDSESSION:
				case ICP_CPC_SESSIDLE_WAITREVALUELIMIT:
					ICPProt_CPCPoll();
					break;
				case ICP_CPC_SESSIDLE_TXSESSCOMP:
					ICPProt_CPCVend04();
					break;
				case ICP_CPC_SESSIDLE_TXREADERDISABLE:
					ICPProt_CPCReader00();
					break;
				case ICP_CPC_SESSIDLE_TXEXPTIMEDATE:
					ICPProt_CPCExp03();
					break;
				case ICP_CPC_SESSIDLE_TXREVALLIM:
					ICPProt_CPCRevalue01();
					break;
				case ICP_CPC_SESSIDLE_TXVENDCASH:
					ICPProt_CPCVend05();
					break;
				case ICP_CPC_SESSIDLE_CHKMULTIVEND:
					if(ICPProt_IsMultiVend() && !fSessionCancel) {
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXPOLL);
						ICPProt_CPCPoll();
					} else {
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXSESSCOMP);
						ICPProt_CPCVend04();
					}
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CPC_SESSIDLE_TXPOLL:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
				case ICP_CPC_RESPTYPE_OUTSEQ:
					ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
					break;
				case ICP_CPC_RESPTYPE_SESSCANC:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXSESSCOMP);
					break;
				case ICP_CPC_RESPTYPE_ENDSESS:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_CPC_RESPTYPE_TIMEREQ:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXEXPTIMEDATE);
					break;

				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_CPC_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					case ICP_VMCCMD_CPC_DISABLE:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXREADERDISABLE);
						break;
					case ICP_VMCCMD_CPC_SESSCOMP:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXSESSCOMP);
						break;
					case ICP_VMCCMD_CPC_REVALUELIMIT:
						if(ICPProt_CPCGetRevalue()) {
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXREVALLIM);
						} else {
							ICPProt_VMCCmdRejected(icpSlvAddr);
							ICPCPCHook_RevalueDenied();		// 30/05/01 per sbloccare il Gestore Credito
						}
						break;
					case ICP_VMCCMD_CPC_REVALUEREQ:
						fSessionCancel = false;
						if(ICPProt_CPCGetRevalue()) {
							ICPProt_ChangeState(icpSlv, ICP_CPC_REVALUE, ICP_CPC_REVALUE_TXREVALREQ);
						} else {
							ICPProt_VMCCmdRejected(icpSlvAddr);
							ICPCPCHook_RevalueDenied();		// 30/05/01 per sbloccare il Gestore Credito
						}
						break;
					case ICP_VMCCMD_CPC_VENDREQ:
						fSessionCancel = false;
						ICPProt_ChangeState(icpSlv, ICP_CPC_VEND, ICP_CPC_VEND_TXVENDREQ);
						break;
#if	_IcpCpcLevel_ >= 3
					case ICP_VMCCMD_CPC_NEGVENDREQ:
						fSessionCancel = false;
						if(ICPProt_CPCGetNegVend())
							ICPProt_ChangeState(icpSlv, ICP_CPC_VEND, ICP_CPC_VEND_TXNEGVENDREQ);
						else
							ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
#endif
					case ICP_VMCCMD_CPC_CASHSALE:
						if(ICPProt_CPCGetCashSale())
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXVENDCASH);
						else
							ICPProt_VMCCmdRejected(icpSlvAddr);
						break;

					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				case ICP_CPC_RESPTYPE_DISPREQ:
					// AVT2800?!
				case ICP_CPC_RESPTYPE_MALFUNCT:
					// ?!?!
				default:
					break;
				}
				break;
			case ICP_CPC_SESSIDLE_WAITENDSESSION:
			case ICP_CPC_SESSIDLE_WAITREVALUELIMIT:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
				case ICP_CPC_RESPTYPE_OUTSEQ:
					ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
					break;
				case ICP_CPC_RESPTYPE_ENDSESS:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
//	ICPBILLInhibit();
					break;
				case ICP_CPC_RESPTYPE_REVALLIMIT:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUELIMIT);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXPOLL);
					break;
				case ICP_CPC_RESPTYPE_REVALDENY:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEDENY);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXPOLL);
					break;

				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_CPC_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;

				case ICP_CPC_RESPTYPE_DISPREQ:
					// AVT2800?!
				case ICP_CPC_RESPTYPE_MALFUNCT:
					// ?!?!
				default:
					break;
				}
				break;
			case ICP_CPC_SESSIDLE_TXSESSCOMP:
				nRespType = ICPProt_CPCVend04Resp();
				switch(nRespType) {
				case ICP_CPC_RESPTYPE_ENDSESS:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_WAITENDSESSION);
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_SESSIDLE_TXREADERDISABLE:
				nRespType = ICPProt_CPCReader00Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, ICP_CPC_DISABLED, ICP_CPC_DISABLED_TXPOLL);
				break;
			case ICP_CPC_SESSIDLE_TXEXPTIMEDATE:
				nRespType = ICPProt_CPCExp03Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXPOLL);
				break;
			case ICP_CPC_SESSIDLE_TXREVALLIM:
				nRespType = ICPProt_CPCRevalue01Resp();
				switch(nRespType) {
				case ICP_CPC_RESPTYPE_REVALLIMIT:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUELIMIT);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXPOLL);
					break;
				case ICP_CPC_RESPTYPE_REVALDENY:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEDENY);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXPOLL);
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_WAITREVALUELIMIT);
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_SESSIDLE_TXVENDCASH:
				nRespType = ICPProt_CPCVend05Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_SESSIDLE_TXPOLL);
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	CPCSlaveResp();
#endif
}



//-------------------------------------------------
//
//					V E N D
//
//-------------------------------------------------
void CPCVend(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CPC_VEND)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
			case COM_IDLE:
			case COM_MSTIDLE:
				if(ICPInfo.fTmPoll) {
					switch(icpSlvSubState) {
					case ICP_CPC_VEND_TXVENDREQ:
						ICPProt_CPCVend00();
						break;
					case ICP_CPC_VEND_TXNEGVENDREQ:
						ICPProt_CPCVend06();
						break;
					case ICP_CPC_VEND_WAITVENDAPPDENY:
					case ICP_CPC_VEND_WAITVENDSUCCFAIL:
					case ICP_CPC_VEND_WAITENDSESSION:
					case ICP_CPC_VEND_WAITENDVENDFAIL:
					case ICP_CPC_VEND_WAITENDVENDSUCC:
					case ICP_CPC_VEND_WAITENDVENDSUCC1:
						ICPProt_CPCPoll();
						break;
					case ICP_CPC_VEND_TXVENDCANCEL:
						ICPProt_CPCVend01();
						break;
					case ICP_CPC_VEND_TXVENDSUCC:
					case ICP_CPC_VEND_TXVENDSUCC1:
						ICPProt_CPCVend02();
						break;
					case ICP_CPC_VEND_TXVENDFAILED:
						ICPProt_CPCVend03();
						break;
					case ICP_CPC_VEND_TXSESSCOMP:
						ICPProt_CPCVend04();
						break;
					default:
						break;
					}
				}
				break;
			case COM_RXFRAME:
				ICPProt_SetLink(icpSlvAddr, true);
				switch(icpSlvSubState) {
				case ICP_CPC_VEND_TXVENDREQ:
					nRespType = ICPProt_CPCVend00Resp();
					switch(nRespType) {
					case ICP_CPC_RESPTYPE_VENDDENY:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDDENY);
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
						break;
					case ICP_CPC_RESPTYPE_VENDAPP:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDAPP);
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITVENDSUCCFAIL);
						break;
					case ICP_RESPTYPE_RW_ACK:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITVENDAPPDENY);
						break;
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_TXNEGVENDREQ:
					nRespType = ICPProt_CPCVend06Resp();
					switch(nRespType) {
					case ICP_CPC_RESPTYPE_VENDDENY:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDDENY);
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
						break;
					case ICP_CPC_RESPTYPE_VENDAPP:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDAPP);
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITVENDSUCCFAIL);
						break;
					case ICP_RESPTYPE_RW_ACK:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITVENDAPPDENY);
						break;
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_WAITVENDAPPDENY:
					nRespType = ICPProt_CPCPollResp();
					switch(nRespType) {
					case ICP_RESPTYPE_JUSTRESET:
					case ICP_CPC_RESPTYPE_OUTSEQ:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					case ICP_CPC_RESPTYPE_SESSCANC:
						// ?!? non dovrebbe succedere!
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDDENY);
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXSESSCOMP);
						break;
					case ICP_CPC_RESPTYPE_VENDAPP:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDAPP);
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITVENDSUCCFAIL);
						break;
					case ICP_CPC_RESPTYPE_VENDDENY:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDDENY);
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
						break;

					case ICP_RESPTYPE_RW_ACK:
						//test:	ICPCPCVendCancel();
						//test: ICPCPCVendFailure();
						//test:	ICPCPCReset();
						//test: ICPCPCVendSuccess(0xffff);
						switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
						case ICP_VMCCMD_NONE:
							break;
						case ICP_VMCCMD_CPC_RESET:
							ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
							break;
						case ICP_VMCCMD_CPC_VENDCANCEL:
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXVENDCANCEL);
							break;
						default:
							ICPProt_VMCCmdRejected(icpSlvAddr);
							break;
						}
						break;

					case ICP_CPC_RESPTYPE_DISPREQ:
						// AVT2800?!
					case ICP_CPC_RESPTYPE_MALFUNCT:
						// ?!?!
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_WAITVENDSUCCFAIL:
					nRespType = ICPProt_CPCPollResp();
					switch(nRespType) {
					case ICP_RESPTYPE_JUSTRESET:
					case ICP_CPC_RESPTYPE_OUTSEQ:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					case ICP_CPC_RESPTYPE_SESSCANC:
						/**05/06/2003
						***baco: servendo l'evento di carta fuori qui NON completo la sequenza di vendita
						***      quindi con i cashless che non scalano il credito al vend-approved ma a fine vendita
						***      si ottiene il fenomeno di "Vendite Gratuite"!!! o altri malfunzionamenti...
						ICPProt_ChangeState(nPeriphID, -1, ICP_CPC_VEND_TXSESSCOMP);
						**/
						fSessionCancel = true;
						break;
					case ICP_CPC_RESPTYPE_ENDSESS:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
						ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
						break;

					case ICP_RESPTYPE_RW_ACK:
						//test:	ICPCPCReset();
						//test: ICPCPCVendSuccess(0xffff);
						//test: ICPCPCVendFailure();
						//test:	ICPCPCPullMedia();
						switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
						case ICP_VMCCMD_NONE:
							break;
						case ICP_VMCCMD_CPC_RESET:
							ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
							break;
						case ICP_VMCCMD_CPC_VENDSUCC:
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXVENDSUCC);
							break;
						case ICP_VMCCMD_CPC_VENDFAIL:
							if(ICPProt_CPCGetRefund())
								ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXVENDFAILED);
							else
								ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXVENDSUCC1);
							break;
						default:
							ICPProt_VMCCmdRejected(icpSlvAddr);
							break;
						}
						break;

					case ICP_CPC_RESPTYPE_DISPREQ:
						// AVT2800?!
					case ICP_CPC_RESPTYPE_MALFUNCT:
						// ?!?!
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_WAITENDSESSION:
					nRespType = ICPProt_CPCPollResp();
					switch(nRespType) {
					case ICP_RESPTYPE_JUSTRESET:
					case ICP_CPC_RESPTYPE_OUTSEQ:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					case ICP_CPC_RESPTYPE_ENDSESS:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
						ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
						break;

					case ICP_RESPTYPE_RW_ACK:
						switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
						case ICP_VMCCMD_NONE:
							break;
						case ICP_VMCCMD_CPC_RESET:
							ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
							break;
						default:
							ICPProt_VMCCmdRejected(icpSlvAddr);
							break;
						}
						break;

					case ICP_CPC_RESPTYPE_DISPREQ:
						// AVT2800?!
					case ICP_CPC_RESPTYPE_MALFUNCT:
						// ?!?!
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_TXVENDCANCEL:
					nRespType = ICPProt_CPCVend01Resp();
					switch(nRespType) {
					case ICP_CPC_RESPTYPE_VENDDENY:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDDENY);
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
						break;
					case ICP_RESPTYPE_RW_ACK:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITVENDAPPDENY);
						break;
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_TXVENDSUCC:
					nRespType = ICPProt_CPCVend02Resp();
					if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITENDVENDSUCC);
					break;
				case ICP_CPC_VEND_TXVENDSUCC1:
					nRespType = ICPProt_CPCVend02Resp();
					if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITENDVENDSUCC1);
					break;
				case ICP_CPC_VEND_TXVENDFAILED:
					nRespType = ICPProt_CPCVend03Resp();
					if(nRespType == ICP_RESPTYPE_RW_ACK) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITENDVENDFAIL);
					break;
				case ICP_CPC_VEND_WAITENDVENDSUCC:
					nRespType = ICPProt_CPCPollResp();
					switch(nRespType) {
					case ICP_RESPTYPE_JUSTRESET:
					case ICP_CPC_RESPTYPE_OUTSEQ:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;

					case ICP_CPC_RESPTYPE_SESSCANC:
					case ICP_RESPTYPE_RW_ACK:
					case ICP_CPC_RESPTYPE_REFUNDERR:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDSUCC);
						if(nRespType == ICP_CPC_RESPTYPE_SESSCANC) ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXSESSCOMP);
						else ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_CHKMULTIVEND);
						break;

					case ICP_CPC_RESPTYPE_DISPREQ:
						// AVT2800?!
					case ICP_CPC_RESPTYPE_MALFUNCT:
						// ?!?!
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_WAITENDVENDSUCC1:
					nRespType = ICPProt_CPCPollResp();
					switch(nRespType) {
					case ICP_RESPTYPE_JUSTRESET:
					case ICP_CPC_RESPTYPE_OUTSEQ:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;

					case ICP_RESPTYPE_RW_ACK:
					case ICP_CPC_RESPTYPE_SESSCANC:
						ICPCPCHook_RefundError(true);
					case ICP_CPC_RESPTYPE_REFUNDERR:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDFAIL);
						if(nRespType == ICP_CPC_RESPTYPE_SESSCANC)
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXSESSCOMP);
						else
							ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_CHKMULTIVEND);
						break;

					case ICP_CPC_RESPTYPE_DISPREQ:
						// AVT2800?!
					case ICP_CPC_RESPTYPE_MALFUNCT:
						// ?!?!
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_WAITENDVENDFAIL:
					nRespType = ICPProt_CPCPollResp();
					switch(nRespType) {
					case ICP_RESPTYPE_JUSTRESET:
					case ICP_CPC_RESPTYPE_OUTSEQ:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;

					case ICP_RESPTYPE_RW_ACK:
					case ICP_CPC_RESPTYPE_SESSCANC:
						ICPCPCHook_RefundError(false);
					case ICP_CPC_RESPTYPE_REFUNDERR:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_VENDFAIL);
						if(nRespType == ICP_CPC_RESPTYPE_SESSCANC)
							ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_TXSESSCOMP);
						else
							ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_CHKMULTIVEND);
						break;

					case ICP_CPC_RESPTYPE_DISPREQ:
						// AVT2800?!
					case ICP_CPC_RESPTYPE_MALFUNCT:
						// ?!?!
					default:
						break;
					}
					break;
				case ICP_CPC_VEND_TXSESSCOMP:
					nRespType = ICPProt_CPCVend04Resp();
					switch(nRespType) {
					case ICP_CPC_RESPTYPE_ENDSESS:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
						ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
						break;
					case ICP_RESPTYPE_RW_ACK:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_VEND_WAITENDSESSION);
						break;
					default:
						break;
					}
					break;
				default:
					break;
				}
				break;
			default:
				break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	CPCSlaveResp();
#endif
}



//-------------------------------------------------
//
//					R E V A L U E
//
//-------------------------------------------------
void CPCRevalue(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CPC_REVALUE)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CPC_REVALUE_TXREVALREQ:
					ICPProt_CPCRevalue00();
					break;
				case ICP_CPC_REVALUE_TXPOLL:
				case ICP_CPC_REVALUE_WAITENDSESSION:
					ICPProt_CPCPoll();
					break;
				case ICP_CPC_REVALUE_TXSESSCOMP:
					ICPProt_CPCVend04();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CPC_REVALUE_TXREVALREQ:
				nRespType = ICPProt_CPCRevalue00Resp();
				switch(nRespType) {
					case ICP_CPC_RESPTYPE_REVALDENY:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEDENY);
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
						break;
					case ICP_CPC_RESPTYPE_REVALAPP:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEAPP);
						ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
						break;
					case ICP_RESPTYPE_RW_ACK:
						ICPProt_ChangeState(icpSlv, -1, ICP_CPC_REVALUE_TXPOLL);
						break;
					default:
						break;
				}
				break;
			case ICP_CPC_REVALUE_TXPOLL:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
				case ICP_CPC_RESPTYPE_OUTSEQ:
					ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
					break;
				case ICP_CPC_RESPTYPE_SESSCANC:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEDENY);
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_REVALUE_TXSESSCOMP);
					break;
				case ICP_CPC_RESPTYPE_ENDSESS:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
					break;

				case ICP_RESPTYPE_RW_ACK:
					//test:	ICPCPCPullMedia();
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_CPC_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;

				case ICP_CPC_RESPTYPE_REVALAPP:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEAPP);
					ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
					break;
				case ICP_CPC_RESPTYPE_REVALDENY:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_REVALUEDENY);
					ICPProt_ChangeState(icpSlv, ICP_CPC_SESSIDLE, ICP_CPC_SESSIDLE_TXPOLL);
					break;

				case ICP_CPC_RESPTYPE_DISPREQ:
					// AVT2800?!
				case ICP_CPC_RESPTYPE_MALFUNCT:
					// ?!
				default:
					break;
				}
				break;
			case ICP_CPC_REVALUE_TXSESSCOMP:
				nRespType = ICPProt_CPCVend04Resp();
				switch(nRespType) {
				case ICP_CPC_RESPTYPE_ENDSESS:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CPC_REVALUE_WAITENDSESSION);
					break;
				default:
					break;
				}
				break;
			case ICP_CPC_REVALUE_WAITENDSESSION:
				nRespType = ICPProt_CPCPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
				case ICP_CPC_RESPTYPE_OUTSEQ:
					ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
					break;
				case ICP_CPC_RESPTYPE_ENDSESS:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_ENDSESSION);
					ICPProt_ChangeState(icpSlv, ICP_CPC_ENABLED, ICP_CPC_ENABLED_TXPOLL);
					break;

				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_CPC_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CPC_INACTIVE, ICP_CPC_INACTIVE_TXRESET);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;

				case ICP_CPC_RESPTYPE_DISPREQ:
					// AVT2800?!
				case ICP_CPC_RESPTYPE_MALFUNCT:
					// ?!?!
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	CPCSlaveResp();
#endif
}


//===============================================
//		RESET Command (10H)
//===============================================
void ICPProt_CPCReset(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_RESET *ptCmd = (T_CPCCMD_RESET *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	uint8_t nCmd, nCmdState;
	if((nCmd = ICPProt_VMCGetCmd(icpSlvAddr)) == ICP_VMCCMD_CPC_RESET) {
		ICPProt_VMCGetPar(icpSlvAddr);
		nCmdState = ICPCPCInfo.nCmdState;
	}
	CPCInit(icpSlvAddr);
	if(nCmd == ICP_VMCCMD_CPC_RESET) {
		ICPCPCInfo.nCmdType = nCmd;
		ICPCPCInfo.nCmdState = nCmdState;
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_RESET;
	nFrameLen = sizeof(ptCmd->nCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCResetResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_RESET *ptResp = (T_CPCRESP_RESET *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
//			if (gVMC_ConfVars.CPC_MDB) {														// Rifiuto CPC se abilitato Mifare Locale
				nRespType = ICP_RESPTYPE_RW_ACK;
//			}
		}
		ICPDrvSetComState(COM_IDLE);
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	ICPDrvSendRW(ICP_RW_ACK);
	CPCInit(icpSlvAddr);
	ICPCPCHook_Init();
	ICPProt_SetInhibit(icpSlvAddr, true);
#endif
	return nRespType;
}


void ICPProt_CPCSetup00(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_SETUP00 *ptCmd = (T_CPCCMD_SETUP00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_SETUP;
	ptCmd->nSubCmd = ICP_CPC_CMD_SETUPID;
	ICPProt_VMCGetSetupInfo(&ptCmd->Setup00);
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCSetup00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_SETUP00 *ptResp = (T_CPCRESP_SETUP00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			bool enable;
			ICPDrvSendRW(ICP_RW_ACK);
			ICPCPCInfo.Setup00 = ptResp->setup00.Setup00;
			//MR20 Cvt16ToBigEndian(&ICPCPCInfo.Setup00.nCCode);
			ICPCPCInfo.Setup00.nCCode = __REV16(ICPCPCInfo.Setup00.nCCode);			
			ICPProt_SetLevel(icpSlvAddr, ICPCPCInfo.Setup00.nFLevel);
			ICPCPCHook_SetDPP(ICPCPCInfo.Setup00.nDPP);
			enable = (ICPCPCInfo.Setup00.nFLevel > 1) ? true : false;							// Ricarica carta: 1=permessa, 0=inibita
			ICPCPCHook_RechargeOption(enable);
			ICPCPCHook_SetMaxNonResponseTime(ICPCPCInfo.Setup00.nTmMNR);
			nRespType = ICP_CPC_RESPTYPE_SETUPID;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_SETUP00 *ptCmd;
	T_CPCRESP_SETUP00 *ptResp; 

	// Per evitare malfunzionamenti con VMC Mdb che non attendono
	// il ns. JustReset prima di far partire la sequenza di inizializzazione!
	ResetCPCInfoMask(ICP_CPCINFO_JUSTRESET);
	ptCmd = (T_CPCCMD_SETUP00 *)&ICPInfo.buff[0];
	ICPProt_SetLevel(icpSlvAddr, ptCmd->Setup00.nFLevel);
	// display info not used!
	ptResp = (T_CPCRESP_SETUP00 *)&ICPInfo.buff[0];
	ptResp->setup00.nSetup00 = ICP_CPC_SETUPID;
	ptResp->setup00.Setup00.nFLevel = ICPCPCHook_GetFeatureLevel();
//	ptResp->setup00.Setup00.nCCode = ICPCPCHook_GetCountryCode();			// Sostituito con Currency Code 09.09.2013
	ptResp->setup00.Setup00.nCCode = ICPCPCHook_GetCurrencyCode();
	Cvt16ToBigEndian(&ptResp->setup00.Setup00.nCCode)
	ptResp->setup00.Setup00.nUSF = ICPCPCHook_GetScalingFactor();
	ptResp->setup00.Setup00.nDPP = ICPCPCHook_GetDecimalPoint();
	ptResp->setup00.Setup00.nTmMNR = ICPCPCHook_GetMaxNoResponse();
	ptResp->setup00.Setup00.nFOptions = ICPCPCHook_GetFeatures();
	ICPCPCInfo.Setup00 = ptResp->setup00.Setup00;
	ICPInfo.nFrameLen = sizeof(ptResp->setup00);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif
	return nRespType;
}


void ICPProt_CPCSetup01(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_SETUP01 *ptCmd = (T_CPCCMD_SETUP01 *)&ICPInfo.buff[0];
	CreditCpcValue nMaxPrice, nMinPrice;
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_SETUP;
	ptCmd->nSubCmd = ICP_CPC_CMD_SETUPMAXMINPRICE;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	nMaxPrice = ICPVMCHook_GetMaxPrice();
	nMinPrice = ICPVMCHook_GetMinPrice();
	if(ICPCPCInfo.Setup00.nUSF) {
		if(nMaxPrice != 0xFFFFFFFF) nMaxPrice /= ICPCPCInfo.Setup00.nUSF;
		if(nMinPrice) nMinPrice /= ICPCPCInfo.Setup00.nUSF;
	}
	switch(ICPProt_GetLevel()) {
	case 3:
		ptCmd->Setup01.L3.nMaxPrice = nMaxPrice;
		ptCmd->Setup01.L3.nMinPrice = nMinPrice;
		//MR20 Cvt32ToBigEndian(&ptCmd->Setup01.L3.nMaxPrice);
		ptCmd->Setup01.L3.nMaxPrice = __REV(ptCmd->Setup01.L3.nMaxPrice);	
		//MR20 Cvt32ToBigEndian(&ptCmd->Setup01.L3.nMinPrice);
		ptCmd->Setup01.L3.nMinPrice = __REV(ptCmd->Setup01.L3.nMinPrice);			
		//?!?!?ICPVMCHook_GetCCode()?!?!?!
		nFrameLen += sizeof(ptCmd->Setup01.L3);
		break;
	default:
		ptCmd->Setup01.L1.nMaxPrice = (CPCCreditType_L1)nMaxPrice;
		ptCmd->Setup01.L1.nMinPrice = (CPCCreditType_L1)nMinPrice;
		//MR20 Cvt16ToBigEndian(&ptCmd->Setup01.L1.nMaxPrice);
		ptCmd->Setup01.L1.nMaxPrice = __REV16(ptCmd->Setup01.L1.nMaxPrice);			
		//MR20 Cvt16ToBigEndian(&ptCmd->Setup01.L1.nMinPrice);
		ptCmd->Setup01.L1.nMinPrice = __REV16(ptCmd->Setup01.L1.nMinPrice);			
		nFrameLen += sizeof(ptCmd->Setup01.L1);
		break;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCSetup01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {

		T_CPCRESP_SETUP01 *ptResp = (T_CPCRESP_SETUP01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_SETUP01 *ptCmd;
	CPCCreditType_L3 nMaxPrice, nMinPrice;
	uint16_t nCCode = 0xFFFF;
	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_CPCCMD_SETUP01 *)&ICPInfo.buff[0];
	if(ICPProt_GetLevel() <= 2) {
		Cvt16ToBigEndian(&ptCmd->Setup01.L1.nMaxPrice)
		Cvt16ToBigEndian(&ptCmd->Setup01.L1.nMinPrice)
		nMaxPrice = ptCmd->Setup01.L1.nMaxPrice;
		nMinPrice = ptCmd->Setup01.L1.nMinPrice;
	} else {
		Cvt32ToBigEndian(&ptCmd->Setup01.L3.nMaxPrice)
		Cvt32ToBigEndian(&ptCmd->Setup01.L3.nMinPrice)
		Cvt16ToBigEndian(&ptCmd->Setup01.L3.nCurrencyCode)
		nMaxPrice = ptCmd->Setup01.L3.nMaxPrice;
		nMinPrice = ptCmd->Setup01.L3.nMinPrice;
		nCCode = ptCmd->Setup01.L3.nCurrencyCode;
	}
	if(nMaxPrice != 0xFFFFFFFF)	nMaxPrice *= ICPCPCInfo.Setup00.nUSF;
	if(nMinPrice) nMinPrice *= ICPCPCInfo.Setup00.nUSF;
	// Gestire CURRENCY diversi?? vedi "EXPANDED CURRENCY MODE"
	ICPCPCHook_SetMaxMinPrices(nMaxPrice, nMinPrice, nCCode);
	}
#endif
	return nRespType;
}


void ICPProt_CPCPoll(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_POLL *ptCmd = (T_CPCCMD_POLL *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_POLL;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCPollResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_POLL *ptResp = (T_CPCRESP_POLL *)&ICPInfo.buff[0];
		int16 i, n, nRespLen;

		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPDrvSendRW(ICP_RW_ACK);
			n = ICPInfo.iRX - 1;
			for(i = 0; i < n; ) {
				nRespType = ptResp->aPollData[i];
				switch(nRespType) {
				case ICP_CPC_JUSTRESET:
					{
					nRespType = ICP_RESPTYPE_JUSTRESET;
					i = n;
					}
					break;
				case ICP_CPC_SETUPID:
					{
					T_CPCRESP_SETUP00 *pt = (T_CPCRESP_SETUP00 *)&ptResp->aPollData[i];
					ICPCPCInfo.Setup00 = pt->setup00.Setup00;
					//MR20 Cvt16ToBigEndian(&ICPCPCInfo.Setup00.nCCode);
					ICPCPCInfo.Setup00.nCCode = __REV16(ICPCPCInfo.Setup00.nCCode);			
					ICPProt_SetLevel(icpSlvAddr, ICPCPCInfo.Setup00.nFLevel);
					//ICPCPCHook_SetCountryCode(ICPCPCInfo.Setup00.nCCode);
					//ICPCPCHook_SetCurrency(ICPCPCInfo.Setup00.nCCode);
					ICPCPCHook_SetDPP(ICPCPCInfo.Setup00.nDPP);
					ICPCPCHook_SetMaxNonResponseTime(ICPCPCInfo.Setup00.nTmMNR);
					i += sizeof(pt->setup00);
					nRespType = ICP_CPC_RESPTYPE_SETUPID;
					}
					break;
				case ICP_CPC_DISPREQ:
					{
					T_CPCRESP_POLL02 *pt = (T_CPCRESP_POLL02 *)&ptResp->aPollData[i];
					
					ICPVMCHook_DisplayMsg(&pt->poll02.DispReq.aMsg[0], sizeof(pt->poll02.DispReq.aMsg), pt->poll02.DispReq.nMsgTime);
					if(nRespType == ICP_RESPTYPE_NONE)
						nRespType = ICP_CPC_RESPTYPE_DISPREQ;
					i = n;
					}
					break;
				case ICP_CPC_BEGSESS:
					{
					T_CPCRESP_POLL03 *pt = (T_CPCRESP_POLL03 *)&ptResp->aPollData[i];

					if(ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nState == ICP_CPC_ENABLED) {

						bool fDispCredit = true;
						uint8_t nUSF = ICPCPCInfo.Setup00.nUSF;

						switch(ICPProt_GetLevel()) {
						case 1:
							ICPCPCInfo.FundInfo.nFunds = pt->poll03.FunfInfo.L1.nFunds;
							//MR20 Cvt16ToBigEndian(&ICPCPCInfo.FundInfo.nFunds);
							ICPCPCInfo.FundInfo.nFunds = __REV16(ICPCPCInfo.FundInfo.nFunds);			
							break;
						case 2:
#if	_IcpCpcLevel_ >= 2
							ICPCPCInfo.FundInfo.nFunds = pt->poll03.FunfInfo.L2.nFunds;
							ICPCPCInfo.FundInfo.nPaymentID = pt->poll03.FunfInfo.L2.nPaymentID;
							ICPCPCInfo.FundInfo.nPaymentData = pt->poll03.FunfInfo.L2.nPaymentData;
							ICPCPCInfo.FundInfo.nPaymentType = pt->poll03.FunfInfo.L2.nPaymentType;
							//MR20 Cvt16ToBigEndian(&ICPCPCInfo.FundInfo.nFunds);
							ICPCPCInfo.FundInfo.nFunds = __REV16(ICPCPCInfo.FundInfo.nFunds);
							//MR20 Cvt32ToBigEndian(&ICPCPCInfo.FundInfo.nPaymentID);
							ICPCPCInfo.FundInfo.nPaymentID = __REV(ICPCPCInfo.FundInfo.nPaymentID);
							//MR20 Cvt16ToBigEndian(&ICPCPCInfo.FundInfo.nPaymentData);
							ICPCPCInfo.FundInfo.nPaymentData = __REV16(ICPCPCInfo.FundInfo.nPaymentData);
							ICPCPCHook_MediaID(ICPCPCInfo.FundInfo.nPaymentID);
							//ICPCPCHook_MediaUsage(ICPCPCInfo.FundInfo.nPaymentType, ICPCPCInfo.FundInfo.nPaymentData);
#endif
							break;
						case 3:
						default:
#if	_IcpCpcLevel_ >= 3
							ICPCPCInfo.FundInfo.nFunds = pt->poll03.FunfInfo.L3.nFunds;
							ICPCPCInfo.FundInfo.nPaymentID = pt->poll03.FunfInfo.L3.nPaymentID;
							ICPCPCInfo.FundInfo.nPaymentData = pt->poll03.FunfInfo.L3.nPaymentData;
							ICPCPCInfo.FundInfo.nPaymentType = pt->poll03.FunfInfo.L3.nPaymentType;
							ICPCPCInfo.FundInfo.nUserLanguage = pt->poll03.FunfInfo.L3.nUserLanguage;
							ICPCPCInfo.FundInfo.nUserCurrency = pt->poll03.FunfInfo.L3.nUserCurrency;
							ICPCPCInfo.FundInfo.nUserOptions = pt->poll03.FunfInfo.L3.nUserOptions;
							Cvt32ToBigEndian(&ICPCPCInfo.FundInfo.nFunds);
							Cvt32ToBigEndian(&ICPCPCInfo.FundInfo.nPaymentID);
							Cvt16ToBigEndian(&ICPCPCInfo.FundInfo.nPaymentData);
							Cvt16ToBigEndian(&ICPCPCInfo.FundInfo.nUserLanguage);
							Cvt16ToBigEndian(&ICPCPCInfo.FundInfo.nUserCurrency);
							ICPCPCHook_MediaID(ICPCPCInfo.FundInfo.nPaymentID);
							//ICPCPCHook_MediaUsage(ICPCPCInfo.FundInfo.nPaymentType, ICPCPCInfo.FundInfo.nPaymentData);
							ICPCPCHook_MediaLanguage(ICPCPCInfo.FundInfo.nUserLanguage);
							ICPCPCHook_MediaCurrency(ICPCPCInfo.FundInfo.nUserCurrency);
							fDispCredit = (ICPCPCInfo.FundInfo.nUserOptions & ICP_CPCPOLL03_DISPCREDIT) ? true : false;
#endif
							break;
						}
						
						if(ICPCPCInfo.FundInfo.nFunds != 0xFFFFFFFF) {
							ICPCPCHook_MediaCredit((ICPCPCInfo.FundInfo.nFunds*nUSF), fDispCredit);
						}
					}
					nRespLen = sizeof(pt->poll03.nPoll03);
					switch(ICPProt_GetLevel()) {
					case 1:
						nRespLen += sizeof(pt->poll03.FunfInfo.L1);
						break;
					case 2:
						nRespLen += sizeof(pt->poll03.FunfInfo.L2);
						break;
					case 3:
					default:
						nRespLen += sizeof(pt->poll03.FunfInfo.L3);
						break;
					}
					i += nRespLen;
					nRespType = ICP_CPC_RESPTYPE_BEGSESS;
					}
					break;
				case ICP_CPC_SESSCANC:
					{
					T_CPCRESP_POLL04 *pt = (T_CPCRESP_POLL04 *)&ptResp->aPollData[i];
					i += sizeof(pt->nPoll04);
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
					nRespType = ICP_CPC_RESPTYPE_SESSCANC;
					}
					break;
				case ICP_CPC_VENDAPP:
					{
					T_CPCRESP_VEND00 *pt = (T_CPCRESP_VEND00 *)&ptResp->aPollData[i];

					if((ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nState == ICP_CPC_VEND) &&
						(ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nSubState == ICP_CPC_VEND_WAITVENDAPPDENY)) {

						switch(ICPProt_GetLevel()) {
						case 1:
						case 2:
							ICPCPCInfo.VendApproved.nVendAmount = pt->vendapp.VendApproved.L1.nVendAmount;
							//MR20 Cvt16ToBigEndian(&ICPCPCInfo.VendApproved.nVendAmount);
							ICPCPCInfo.VendApproved.nVendAmount = __REV16(ICPCPCInfo.VendApproved.nVendAmount);
							ICPCPCHook_VendApproved(ICPCPCInfo.VendApproved.nVendAmount * ICPCPCInfo.Setup00.nUSF, 0);
							break;
						case 3:
						default:
#if	_IcpCpcLevel_ >= 3
							ICPCPCInfo.VendApproved.nVendAmount = pt->vendapp.VendApproved.L3.nVendAmount;
							ICPCPCInfo.VendApproved.nVendCurrency = pt->vendapp.VendApproved.L3.nVendCurrency;
							Cvt32ToBigEndian(&ICPCPCInfo.VendApproved.nVendAmount);
							Cvt16ToBigEndian(&ICPCPCInfo.VendApproved.nVendCurrency);
							ICPCPCHook_VendApproved(ICPCPCInfo.VendApproved.nVendAmount * ICPCPCInfo.Setup00.nUSF, ICPCPCInfo.VendApproved.nVendCurrency);
#endif
							break;
						}
					}
					nRespLen = sizeof(pt->vendapp.nVendApp);
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						nRespLen += sizeof(pt->vendapp.VendApproved.L1);
						break;
					case 3:
					default:
						nRespLen += sizeof(pt->vendapp.VendApproved.L3);
						break;
					}
					i += nRespLen;
					nRespType = ICP_CPC_RESPTYPE_VENDAPP;
					}
					break;
				case ICP_CPC_VENDDENY:
					{
					T_CPCRESP_VEND00 *pt = (T_CPCRESP_VEND00 *)&ptResp->aPollData[i];

					if((ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nState == ICP_CPC_VEND) &&
						(ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nSubState == ICP_CPC_VEND_WAITVENDAPPDENY)) {

						ICPCPCHook_VendDenied();
					}
					i += sizeof(pt->nVendDenied);
					nRespType = ICP_CPC_RESPTYPE_VENDDENY;
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
					}
					break;
				case ICP_CPC_ENDSESS:
					{
					T_CPCRESP_POLL07 *pt = (T_CPCRESP_POLL07 *)&ptResp->aPollData[i];
					ICPCPCHook_EndSession();
					i += sizeof(pt->nPoll07);
					nRespType = ICP_CPC_RESPTYPE_ENDSESS;
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
					}
					break;
				case ICP_CPC_CANCELLED:
					{
					T_CPCRESP_POLL08 *pt = (T_CPCRESP_POLL08 *)&ptResp->aPollData[i];
					if(ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nState == ICP_CPC_ENABLED) {
						ICPCPCHook_OpCancelled();
					}
					i += sizeof(pt->nPoll08);
					nRespType = ICP_CPC_RESPTYPE_CANCELLED;
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
					}
					break;
				case ICP_CPC_PERIPHID:
					{
					T_CPCRESP_EXP00 *pt = (T_CPCRESP_EXP00 *)&ptResp->aPollData[i];

					if((ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nState == ICP_CPC_DISABLED) &&
						(ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nSubState == ICP_CPC_DISABLED_WAITEXPID)) {

						ICPCPCInfo.nFOptions = ICP_CPCOPT_NONE;
						switch(ICPProt_GetLevel()) {
						case 1:
						case 2:
							ICPCPCHook_SetManufactCode(&pt->exp00.PeriphID.L1.aManufactCode[0], sizeof(pt->exp00.PeriphID.L1.aManufactCode));
							ICPCPCHook_SetSerialNumber(&pt->exp00.PeriphID.L1.aSerialNumber[0], sizeof(pt->exp00.PeriphID.L1.aSerialNumber));
							ICPCPCHook_SetModelNumber(&pt->exp00.PeriphID.L1.aModelNumber[0], sizeof(pt->exp00.PeriphID.L1.aModelNumber));
							//MR20 Cvt16ToBigEndian(&pt->exp00.PeriphID.L1.nSwVersion)
							pt->exp00.PeriphID.L1.nSwVersion = __REV16(pt->exp00.PeriphID.L1.nSwVersion);
							ICPCPCHook_SetSWVersion(pt->exp00.PeriphID.L1.nSwVersion);
							break;
						case 3:
						default:
#if	_IcpCpcLevel_ >= 3
							ICPCPCHook_SetManufactCode(&pt->exp00.PeriphID.L3.aManufactCode[0], sizeof(pt->exp00.PeriphID.L3.aManufactCode));
							ICPCPCHook_SetSerialNumber(&pt->exp00.PeriphID.L3.aSerialNumber[0], sizeof(pt->exp00.PeriphID.L3.aSerialNumber));
							ICPCPCHook_SetModelNumber(&pt->exp00.PeriphID.L3.aModelNumber[0], sizeof(pt->exp00.PeriphID.L3.aModelNumber));
							Cvt16ToBigEndian(&pt->exp00.PeriphID.L3.nSwVersion)
							ICPCPCHook_SetSWVersion(pt->exp00.PeriphID.L3.nSwVersion);
							Cvt32ToBigEndian(&pt->exp00.PeriphID.L3.nFOptions)
							ICPCPCInfo.nFOptions = pt->exp00.PeriphID.L3.nFOptions;
#endif
							break;
						}
					}
					nRespLen = sizeof(pt->exp00.nPeriphID);
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						nRespLen += sizeof(pt->exp00.PeriphID.L1);
						break;
					case 3:
					default:
						nRespLen += sizeof(pt->exp00.PeriphID.L3);
						break;
					}
					i += nRespLen;
					nRespType = ICP_CPC_RESPTYPE_PERIPHID;
					}
					break;
				case ICP_CPC_MALFUNCT:
					{
					T_CPCRESP_POLL0A *pt = (T_CPCRESP_POLL0A *)&ptResp->aPollData[i];
					if((pt->poll0a.nErrorCode & 0xF0) == 0xC0) {
						ICPCPCHook_RefundError(true);
						if(nRespType == ICP_RESPTYPE_NONE)
							nRespType = ICP_CPC_RESPTYPE_REFUNDERR;
					} else {
						if(nRespType == ICP_RESPTYPE_NONE)
							nRespType = ICP_CPC_RESPTYPE_MALFUNCT;
					}
					i += sizeof(pt->poll0a);
					}
					break;
				case ICP_CPC_OUTSEQ:
					{
					T_CPCRESP_POLL0B *pt = (T_CPCRESP_POLL0B *)&ptResp->aPollData[i];

					nRespLen = sizeof(pt->poll0b.nPoll0B);
					switch(ICPProt_GetLevel()) {
					case 1:
						break;
					case 2:
					case 3:
					default:
#if	_IcpCpcLevel_ >= 2
						nRespLen += sizeof(pt->poll0b.nCpcState);
#endif
						pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
						break;
					}
					i += nRespLen;
					nRespType = ICP_CPC_RESPTYPE_OUTSEQ;
					}
					break;
				case ICP_CPC_BUSY:
					{
					T_CPCRESP_POLL0C *pt = (T_CPCRESP_POLL0C *)&ptResp->aPollData[i];
					i += sizeof(pt->nPoll0C);
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
					if(nRespType == ICP_RESPTYPE_NONE)
						nRespType = ICP_CPC_RESPTYPE_BUSY;
					}
					break;
				case ICP_CPC_REVALAPP:
					{
					T_CPCRESP_REVAL00 *pt = (T_CPCRESP_REVAL00 *)&ptResp->aPollData[i];

					if(ICPInfo.aPeriphInfo[ICPInfo.nCurrPeriph].nState == ICP_CPC_REVALUE) {

						ICPCPCHook_RevalueApproved();
					}
					i += sizeof(pt->nRevalApp);
					nRespType = ICP_CPC_RESPTYPE_REVALAPP;
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
					}
					break;
				case ICP_CPC_REVALDENY:
					{
					T_CPCRESP_REVAL00 *pt = (T_CPCRESP_REVAL00 *)&ptResp->aPollData[i];
					ICPCPCHook_RevalueDenied();
					i += sizeof(pt->nRevalDenied);
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
					nRespType = ICP_CPC_RESPTYPE_REVALDENY;
					}
					break;
				case ICP_CPC_REVALLIMIT:
					{
					T_CPCRESP_REVAL01 *pt = (T_CPCRESP_REVAL01 *)&ptResp->aPollData[i];

					nRespLen = sizeof(pt->nRevalDenied);
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						nRespLen += sizeof(pt->reval01.RevalueLimit.L1);
						ICPCPCInfo.RevalueLimit.nLimitAmount = pt->reval01.RevalueLimit.L1.nLimitAmount;
						//MR20 Cvt16ToBigEndian(&ICPCPCInfo.RevalueLimit.nLimitAmount);
						ICPCPCInfo.RevalueLimit.nLimitAmount = __REV16(ICPCPCInfo.RevalueLimit.nLimitAmount);
						ICPCPCHook_RevalueLimit(ICPCPCInfo.RevalueLimit.nLimitAmount * ICPCPCInfo.Setup00.nUSF, 0);
						break;
					case 3:
					default:
#if	_IcpCpcLevel_ >= 3
						nRespLen += sizeof(pt->reval01.RevalueLimit.L3);
						ICPCPCInfo.RevalueLimit.nLimitAmount = pt->reval01.RevalueLimit.L3.nLimitAmount;
						ICPCPCInfo.RevalueLimit.nCurrencyCode = pt->reval01.RevalueLimit.L3.nCurrencyCode;
						Cvt32ToBigEndian(&ICPCPCInfo.RevalueLimit.nLimitAmount);
						Cvt16ToBigEndian(&ICPCPCInfo.RevalueLimit.nCurrencyCode);
						ICPCPCHook_RevalueLimit(ICPCPCInfo.RevalueLimit.nLimitAmount * ICPCPCInfo.Setup00.nUSF, ICPCPCInfo.RevalueLimit.nCurrencyCode);
#endif
						break;
					}
					i += nRespLen;
					nRespType = ICP_CPC_RESPTYPE_REVALLIMIT;
					}
					break;
				case ICP_CPC_USERFILE:
					{
					//ICPCPCInfo.UserFile = ptResp->UserFile;
					nRespType = ICP_CPC_RESPTYPE_USERFILE;
					i = n;
					}
					break;
				case ICP_CPC_TIMEREQ:
					{
					T_CPCRESP_POLL11 *pt = (T_CPCRESP_POLL11 *)&ptResp->aPollData[i];
					i += sizeof(pt->nPoll11);
					pt +=0;				// Dummy Instruction, inserita solo per evitare il warning "variable pt was set but never used"
						nRespType = ICP_CPC_RESPTYPE_TIMEREQ;
					}
					break;
				case ICP_CPC_DIAG:
					{
					T_CPCRESP_EXPFF *pt = (T_CPCRESP_EXPFF *)&ptResp->aPollData[i];
					ICPCPCHook_SetDiagData(pt->expff.aDiagData, (uint8_t)(ICPInfo.iRX - sizeof(pt->expff.nExpFF) - 1));
					nRespType = ICP_CPC_RESPTYPE_EXPFF;
					i = n;
					}
					break;
				default:
					// ignorare inattesi caratteri spuri dopo frame dati con checksum corretto!!!
					return nRespType;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	GetCPCSlaveStatus();
#endif
	return nRespType;
}
/*
uint8_t ICPProt_CPCPollResp0B(uint8_t nState)
{
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == false)
	#if (_IcpSlaveCode_ & ICP_CARDREADER)

		T_CPCRESP_POLL0B *ptResp = (T_CPCRESP_POLL0B *)&ICPInfo.buff[0];
		ptResp->poll0b.nPoll0B = ICP_CPC_OUTSEQ;
		ICPInfo.nFrameLen = sizeof(ptResp->poll0b.nPoll0B);
		if(ICPProt_GetLevel() > 1) {
			ptResp->poll0b.nCpcState = nState;
			ICPInfo.nFrameLen += sizeof(ptResp->poll0b.nCpcState);
		}
		ICPDrvSendMsg(ICPInfo.nFrameLen);
	#endif
#endif

	return nRespType;
}
*/

void ICPProt_CPCVend00(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_VENDxx *ptCmd = (T_CPCCMD_VENDxx *)&ICPInfo.buff[0];
	CreditCpcValue nPrice = 0;
	uint16_t nSelNumber = 0xffff, nCurrencyCode = 0;
	uint8_t *ptCmdPar = NULL;
	uint8_t nFrameLen;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_VENDREQ) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_VEND;
	ptCmd->nSubCmd = ICP_CPC_CMD_VENDREQUEST;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	if(ptCmdPar) {
		nPrice = *((CreditCpcValue *)ptCmdPar) / ICPCPCInfo.Setup00.nUSF;
		nSelNumber = *((uint16_t *)(ptCmdPar + sizeof(uint32_t)));
		nCurrencyCode = *((uint16_t *)(ptCmdPar + sizeof(uint32_t) + sizeof(uint16_t)));
	}
	switch(ICPProt_GetLevel()) {
	case 3:
		ptCmd->Vend.L3.nPrice = nPrice;
		ptCmd->Vend.L3.nSelNumber = nSelNumber;
		ptCmd->Vend.L3.nCurrencyCode = nCurrencyCode;
		//MR20 Cvt32ToBigEndian(&ptCmd->Vend.L3.nPrice);
		ptCmd->Vend.L3.nPrice = __REV(ptCmd->Vend.L3.nPrice);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L3.nSelNumber);
		ptCmd->Vend.L3.nSelNumber = __REV16(ptCmd->Vend.L3.nSelNumber);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L3.nCurrencyCode);
		ptCmd->Vend.L3.nCurrencyCode = __REV16(ptCmd->Vend.L3.nCurrencyCode);
		nFrameLen += sizeof(ptCmd->Vend.L3);
		break;
	default:
		ptCmd->Vend.L1.nPrice = (CPCCreditType_L1)nPrice;
		ptCmd->Vend.L1.nSelNumber = nSelNumber;
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L1.nPrice);
		ptCmd->Vend.L1.nPrice = __REV16(ptCmd->Vend.L1.nPrice);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L1.nSelNumber);
		ptCmd->Vend.L1.nSelNumber = __REV16(ptCmd->Vend.L1.nSelNumber);
		nFrameLen += sizeof(ptCmd->Vend.L1);
		break;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCVend00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_VEND00 *ptResp = (T_CPCRESP_VEND00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			if(ICPInfo.iRX == 2) {
				if(ptResp->nVendDenied == ICP_CPC_VENDDENY) {
					ICPDrvSendRW(ICP_RW_ACK);
					ICPCPCHook_VendDenied();
					nRespType = ICP_CPC_RESPTYPE_VENDDENY;
				}
			} else {
				if(ptResp->vendapp.nVendApp == ICP_CPC_VENDAPP) {
					ICPDrvSendRW(ICP_RW_ACK);
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						ICPCPCInfo.VendApproved.nVendAmount = ptResp->vendapp.VendApproved.L1.nVendAmount;
						//MR20 Cvt16ToBigEndian(&ICPCPCInfo.VendApproved.nVendAmount);
						ICPCPCInfo.VendApproved.nVendAmount = __REV16(ICPCPCInfo.VendApproved.nVendAmount);
						ICPCPCHook_VendApproved(ICPCPCInfo.VendApproved.nVendAmount * ICPCPCInfo.Setup00.nUSF, 0);
						break;
					case 3:
					default:
	#if	_IcpCpcLevel_ >= 3
						ICPCPCInfo.VendApproved.nVendAmount = ptResp->vendapp.VendApproved.L3.nVendAmount;
						ICPCPCInfo.VendApproved.nVendCurrency = ptResp->vendapp.VendApproved.L3.nVendCurrency;
						Cvt32ToBigEndian(&ICPCPCInfo.VendApproved.nVendAmount);
						Cvt16ToBigEndian(&ICPCPCInfo.VendApproved.nVendCurrency);
						ICPCPCHook_VendApproved(ICPCPCInfo.VendApproved.nVendAmount * ICPCPCInfo.Setup00.nUSF, ICPCPCInfo.VendApproved.nVendCurrency);
	#endif
						break;
					}
					// currency?!
					nRespType = ICP_CPC_RESPTYPE_VENDAPP;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_VENDxx *ptCmd;
	T_CPCRESP_VEND00 *ptResp;
	CPCCreditType_L3 nPrice;
	uint16_t nSelNumber;
	uint16_t nCCode = 0xFFFF;
	CPCCreditType_L3 nVendAmount;
	uint16_t nVendCurrency;

	ptCmd = (T_CPCCMD_VENDxx *)&ICPInfo.buff[0];
	if(ICPProt_GetLevel() <= 2) {
		Cvt16ToBigEndian(&ptCmd->Vend.L1.nPrice)
		Cvt16ToBigEndian(&ptCmd->Vend.L1.nSelNumber)
		nPrice = ptCmd->Vend.L1.nPrice;
		nSelNumber = ptCmd->Vend.L1.nSelNumber;
	} else {
		Cvt32ToBigEndian(&ptCmd->Vend.L3.nPrice)
		Cvt16ToBigEndian(&ptCmd->Vend.L3.nSelNumber)
		Cvt16ToBigEndian(&ptCmd->Vend.L3.nCurrencyCode)
		nPrice = ptCmd->Vend.L3.nPrice;
		nSelNumber = ptCmd->Vend.L3.nSelNumber;
		nCCode = ptCmd->Vend.L3.nCurrencyCode;
	}
	nPrice *= ICPCPCInfo.Setup00.nUSF;
	// Gestire CURRENCY diversi?? vedi "EXPANDED CURRENCY MODE"
	nRespType = ICPCPCHook_VendRequest(nPrice, nSelNumber, nCCode, &nVendAmount, &nVendCurrency);
	ptResp = (T_CPCRESP_VEND00 *)&ICPInfo.buff[0];
	switch(nRespType) {
	case ICP_CPC_RESPDENY:
		ptResp->nVendDenied = ICP_CPC_VENDDENY;
		ICPInfo.nFrameLen = sizeof(ptResp->nVendDenied);
		ICPDrvSendMsg(ICPInfo.nFrameLen);
		break;
	case ICP_CPC_RESPAPP:
		nVendAmount /= ICPCPCInfo.Setup00.nUSF;
		if(ICPProt_GetLevel() <= 2) {
			ptResp->vendapp.VendApproved.L1.nVendAmount = nVendAmount;
			Cvt16ToBigEndian(&ptResp->vendapp.VendApproved.L1.nVendAmount)
			ICPInfo.nFrameLen = sizeof(ptResp->vendapp.VendApproved.L1);
		} else {
			ptResp->vendapp.VendApproved.L3.nVendAmount = nVendAmount;
			ptResp->vendapp.VendApproved.L3.nVendCurrency = nVendCurrency;
			Cvt32ToBigEndian(&ptResp->vendapp.VendApproved.L3.nVendAmount)
			Cvt16ToBigEndian(&ptResp->vendapp.VendApproved.L3.nVendCurrency)
			ICPInfo.nFrameLen = sizeof(ptResp->vendapp.VendApproved.L3);
		}
		ptResp->vendapp.nVendApp = ICP_CPC_VENDAPP;
		ICPInfo.nFrameLen += sizeof(ptResp->vendapp.nVendApp);
		ICPDrvSendMsg(ICPInfo.nFrameLen);
		break;
	case ICP_CPC_WAITRESP:
		// wait... tx vend deny/approved at Poll response!
	default:
		ICPDrvSendRW(ICP_RW_ACK);
		break;
	}
	}
#endif
	return nRespType;
}


void ICPProt_CPCVend01(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_VEND01 *ptCmd = (T_CPCCMD_VEND01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_VENDCANCEL) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_VEND;
	ptCmd->nSubCmd = ICP_CPC_CMD_VENDCANCEL;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCVend01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_VEND01 *ptResp = (T_CPCRESP_VEND01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			if(ICPInfo.iRX == 2) {
				if(ptResp->nVendDenied == ICP_CPC_VENDDENY) {
					ICPDrvSendRW(ICP_RW_ACK);
					ICPCPCHook_VendDenied();
					nRespType = ICP_CPC_RESPTYPE_VENDDENY;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCRESP_VEND01 *ptResp;
	ptResp = (T_CPCRESP_VEND01 *)&ICPInfo.buff[0];
	ptResp->nVendDenied = ICP_CPC_VENDDENY;
	ICPInfo.nFrameLen = sizeof(ptResp->nVendDenied);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	ICPCPCHook_VendCancel();
	}
#endif
	return nRespType;
}


void ICPProt_CPCVend02(void)
{
#if (_IcpMaster_ == true)
	T_CPCCMD_VEND02 *ptCmd = (T_CPCCMD_VEND02 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar;
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_VEND;
	ptCmd->nSubCmd = ICP_CPC_CMD_VENDSUCCESS;
	ptCmdPar = NULL;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_VENDSUCC) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	if(ptCmdPar) {
		ptCmd->nSelNumber = *((uint16_t *)ptCmdPar);
		//MR20 Cvt16ToBigEndian(&ptCmd->nSelNumber);
		ptCmd->nSelNumber = __REV16(ptCmd->nSelNumber);
	} else ptCmd->nSelNumber = 0xFFFF;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCVend02Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_VEND02 *ptResp = (T_CPCRESP_VEND02 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_VEND02 *ptCmd;
	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_CPCCMD_VEND02 *)&ICPInfo.buff[0];
	Cvt16ToBigEndian(&ptCmd->nSelNumber)
	ICPCPCHook_VendSuccess(ptCmd->nSelNumber);
	}
#endif
	return nRespType;
}


void ICPProt_CPCVend03(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_VEND03 *ptCmd = (T_CPCCMD_VEND03 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_VENDFAIL) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_VEND;
	ptCmd->nSubCmd = ICP_CPC_CMD_VENDFAILURE;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCVend03Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_VEND03 *ptResp = (T_CPCRESP_VEND03 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	ICPDrvSendRW(ICP_RW_ACK);
	ICPCPCHook_VendFailure();
#endif
	return nRespType;
}


void ICPProt_CPCVend04(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_VEND04 *ptCmd = (T_CPCCMD_VEND04 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_SESSCOMP) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_VEND;
	ptCmd->nSubCmd = ICP_CPC_CMD_VENDCOMPLETE;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCVend04Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_VEND04 *ptResp = (T_CPCRESP_VEND04 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			if(ICPInfo.iRX == 2) {
				if(ptResp->nSessionComplete == ICP_CPC_ENDSESS) {
					ICPDrvSendRW(ICP_RW_ACK);
					ICPCPCHook_EndSession();
					nRespType = ICP_CPC_RESPTYPE_ENDSESS;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCRESP_VEND04 *ptResp;
	ptResp = (T_CPCRESP_VEND04 *)&ICPInfo.buff[0];
	ptResp->nSessionComplete = ICP_CPC_ENDSESS;
	ICPInfo.nFrameLen = sizeof(ptResp->nSessionComplete);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	ResetCPCInfoMask(ICP_CPCINFO_SESSCANCREQ);
	ICPCPCHook_VendComplete();												// Fa solo "gOrionKeyVars.State = kOrionCPCStates_Enabled"
	}
#endif

	return nRespType;
}


void ICPProt_CPCVend05(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_VENDxx *ptCmd = (T_CPCCMD_VENDxx *)&ICPInfo.buff[0];
	CreditCpcValue nPrice = 0;
	uint16_t nSelNumber = 0xffff, nCurrencyCode = 0;
	uint8_t *ptCmdPar = NULL;
	uint8_t nFrameLen;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_CASHSALE) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_VEND;
	ptCmd->nSubCmd = ICP_CPC_CMD_VENDCASHSALE;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	if(ptCmdPar) {
		nPrice = *((CreditCpcValue *)ptCmdPar) / ICPCPCInfo.Setup00.nUSF;
		nSelNumber = *((uint16_t *)(ptCmdPar + sizeof(uint32_t)));
		nCurrencyCode = *((uint16_t *)(ptCmdPar + sizeof(uint32_t) + sizeof(uint16_t)));
	}
	switch(ICPProt_GetLevel()) {
	case 3:
		ptCmd->Vend.L3.nPrice = nPrice;
		ptCmd->Vend.L3.nSelNumber = nSelNumber;
		ptCmd->Vend.L3.nCurrencyCode = nCurrencyCode;
		//MR20 Cvt32ToBigEndian(&ptCmd->Vend.L3.nPrice);
		ptCmd->Vend.L3.nPrice = __REV(ptCmd->Vend.L3.nPrice);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L3.nSelNumber);
		ptCmd->Vend.L3.nSelNumber = __REV16(ptCmd->Vend.L3.nSelNumber);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L3.nCurrencyCode);
		ptCmd->Vend.L3.nCurrencyCode = __REV16(ptCmd->Vend.L3.nCurrencyCode);
		nFrameLen += sizeof(ptCmd->Vend.L3);
		break;
	default:
		ptCmd->Vend.L1.nPrice = (CPCCreditType_L1)nPrice;
		ptCmd->Vend.L1.nSelNumber = nSelNumber;
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L1.nPrice);
		ptCmd->Vend.L1.nPrice = __REV16(ptCmd->Vend.L1.nPrice);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L1.nSelNumber);
		ptCmd->Vend.L1.nSelNumber = __REV16(ptCmd->Vend.L1.nSelNumber);
		nFrameLen += sizeof(ptCmd->Vend.L1);
		break;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCVend05Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_VEND05 *ptResp = (T_CPCRESP_VEND05 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CPC_CASHSALEDONE);
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_VENDxx *ptCmd;
	CPCCreditType_L3 nPrice;
	uint16_t nSelNumber;
	uint16_t nCCode = 0xFFFF;

	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_CPCCMD_VENDxx *)&ICPInfo.buff[0];
	if(ICPProt_GetLevel() <= 2) {
		Cvt16ToBigEndian(&ptCmd->Vend.L1.nPrice)
		Cvt16ToBigEndian(&ptCmd->Vend.L1.nSelNumber)
		nPrice = ptCmd->Vend.L1.nPrice;
		nSelNumber = ptCmd->Vend.L1.nSelNumber;
	} else {
		Cvt32ToBigEndian(&ptCmd->Vend.L3.nPrice)
		Cvt16ToBigEndian(&ptCmd->Vend.L3.nSelNumber)
		Cvt16ToBigEndian(&ptCmd->Vend.L3.nCurrencyCode)
		nPrice = ptCmd->Vend.L3.nPrice;
		nSelNumber = ptCmd->Vend.L3.nSelNumber;
		nCCode = ptCmd->Vend.L3.nCurrencyCode;
	}
	nPrice *= ICPCPCInfo.Setup00.nUSF;
	// Gestire CURRENCY diversi?? vedi "EXPANDED CURRENCY MODE"
	ICPCPCHook_VendCashSale(nPrice, nSelNumber, nCCode);
	}
#endif

	return nRespType;
}


void ICPProt_CPCVend06(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_VENDxx *ptCmd = (T_CPCCMD_VENDxx *)&ICPInfo.buff[0];
	CreditCpcValue nPrice = 0;
	uint16_t nSelNumber = 0xffff, nCurrencyCode = 0;
	uint8_t *ptCmdPar = NULL;
	uint8_t nFrameLen;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_NEGVENDREQ) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_VEND;
	ptCmd->nSubCmd = ICP_CPC_CMD_NEGVENDREQUEST;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	if(ptCmdPar) {
		nPrice = *((CreditCpcValue *)ptCmdPar) / ICPCPCInfo.Setup00.nUSF;
		nSelNumber = *((uint16_t *)(ptCmdPar + sizeof(uint32_t)));
		nCurrencyCode = *((uint16_t *)(ptCmdPar + sizeof(uint32_t) + sizeof(uint16_t)));
	}
	switch(ICPProt_GetLevel()) {
	case 3:
		ptCmd->Vend.L3.nPrice = nPrice;
		ptCmd->Vend.L3.nSelNumber = nSelNumber;
		ptCmd->Vend.L3.nCurrencyCode = nCurrencyCode;
		//MR20 Cvt32ToBigEndian(&ptCmd->Vend.L3.nPrice);
		ptCmd->Vend.L3.nPrice = __REV(ptCmd->Vend.L3.nPrice);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L3.nSelNumber);
		ptCmd->Vend.L3.nSelNumber = __REV16(ptCmd->Vend.L3.nSelNumber);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L3.nCurrencyCode);
		ptCmd->Vend.L3.nCurrencyCode = __REV16(ptCmd->Vend.L3.nCurrencyCode);
		nFrameLen += sizeof(ptCmd->Vend.L3);
		break;
	default:
		ptCmd->Vend.L1.nPrice = (CPCCreditType_L1)nPrice;
		ptCmd->Vend.L1.nSelNumber = nSelNumber;
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L1.nPrice);
		ptCmd->Vend.L1.nPrice = __REV16(ptCmd->Vend.L1.nPrice);
		//MR20 Cvt16ToBigEndian(&ptCmd->Vend.L1.nSelNumber);
		ptCmd->Vend.L1.nSelNumber = __REV16(ptCmd->Vend.L1.nSelNumber);
		nFrameLen += sizeof(ptCmd->Vend.L1);
		break;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCVend06Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_VEND00 *ptResp = (T_CPCRESP_VEND00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			if(ICPInfo.iRX == 2) {
				if(ptResp->nVendDenied == ICP_CPC_VENDDENY) {
					ICPDrvSendRW(ICP_RW_ACK);
					ICPCPCHook_VendDenied();
					nRespType = ICP_CPC_RESPTYPE_VENDDENY;
				}
			} else {
				if(ptResp->vendapp.nVendApp == ICP_CPC_VENDAPP) {
					ICPDrvSendRW(ICP_RW_ACK);
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						ICPCPCInfo.VendApproved.nVendAmount = ptResp->vendapp.VendApproved.L1.nVendAmount;
						//MR20 Cvt16ToBigEndian(&ICPCPCInfo.VendApproved.nVendAmount);
						ICPCPCInfo.VendApproved.nVendAmount = __REV16(ICPCPCInfo.VendApproved.nVendAmount);
						ICPCPCHook_VendApproved(ICPCPCInfo.VendApproved.nVendAmount * ICPCPCInfo.Setup00.nUSF, 0);
						break;
					case 3:
					default:
	#if	_IcpCpcLevel_ >= 3
						ICPCPCInfo.VendApproved.nVendAmount = ptResp->vendapp.VendApproved.L3.nVendAmount;
						ICPCPCInfo.VendApproved.nVendCurrency = ptResp->vendapp.VendApproved.L3.nVendCurrency;
						Cvt32ToBigEndian(&ICPCPCInfo.VendApproved.nVendAmount);
						Cvt16ToBigEndian(&ICPCPCInfo.VendApproved.nVendCurrency);
						ICPCPCHook_VendApproved(ICPCPCInfo.VendApproved.nVendAmount * ICPCPCInfo.Setup00.nUSF, ICPCPCInfo.VendApproved.nVendCurrency);
	#endif
						break;
					}
					// currency?!
					nRespType = ICP_CPC_RESPTYPE_VENDAPP;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_VENDxx *ptCmd;
	T_CPCRESP_VEND00 *ptResp;
	CPCCreditType_L3 nPrice;
	uint16_t nSelNumber;
	uint16_t nCCode = 0xFFFF;
	CPCCreditType_L3 nVendAmount;
	uint16_t nVendCurrency;

	ptCmd = (T_CPCCMD_VENDxx *)&ICPInfo.buff[0];
	if(ICPProt_GetLevel() <= 2) {
		Cvt16ToBigEndian(&ptCmd->Vend.L1.nPrice)
		Cvt16ToBigEndian(&ptCmd->Vend.L1.nSelNumber)
		nPrice = ptCmd->Vend.L1.nPrice;
		nSelNumber = ptCmd->Vend.L1.nSelNumber;
	} else {
		Cvt32ToBigEndian(&ptCmd->Vend.L3.nPrice)
		Cvt16ToBigEndian(&ptCmd->Vend.L3.nSelNumber)
		Cvt16ToBigEndian(&ptCmd->Vend.L3.nCurrencyCode)
		nPrice = ptCmd->Vend.L3.nPrice;
		nSelNumber = ptCmd->Vend.L3.nSelNumber;
		nCCode = ptCmd->Vend.L3.nCurrencyCode;
	}
	nPrice *= ICPCPCInfo.Setup00.nUSF;
	// Gestire CURRENCY diversi?? vedi "EXPANDED CURRENCY MODE"
	nRespType = ICPCPCHook_NegVendRequest(nPrice, nSelNumber, nCCode, &nVendAmount, &nVendCurrency);
	ptResp = (T_CPCRESP_VEND00 *)&ICPInfo.buff[0];
	switch(nRespType) {
	case ICP_CPC_RESPDENY:
		ptResp->nVendDenied = ICP_CPC_VENDDENY;
		ICPInfo.nFrameLen = sizeof(ptResp->nVendDenied);
		ICPDrvSendMsg(ICPInfo.nFrameLen);
		break;
	case ICP_CPC_RESPAPP:
		nVendAmount /= ICPCPCInfo.Setup00.nUSF;
		if(ICPProt_GetLevel() <= 2) {
			ptResp->vendapp.VendApproved.L1.nVendAmount = nVendAmount;
			Cvt16ToBigEndian(&ptResp->vendapp.VendApproved.L1.nVendAmount)
			ICPInfo.nFrameLen = sizeof(ptResp->vendapp.VendApproved.L1);
		} else {
			ptResp->vendapp.VendApproved.L3.nVendAmount = nVendAmount;
			ptResp->vendapp.VendApproved.L3.nVendCurrency = nVendCurrency;
			Cvt32ToBigEndian(&ptResp->vendapp.VendApproved.L3.nVendAmount)
			Cvt16ToBigEndian(&ptResp->vendapp.VendApproved.L3.nVendCurrency)
			ICPInfo.nFrameLen = sizeof(ptResp->vendapp.VendApproved.L3);
		}
		ptResp->vendapp.nVendApp = ICP_CPC_VENDAPP;
		ICPInfo.nFrameLen += sizeof(ptResp->vendapp.nVendApp);
		ICPDrvSendMsg(ICPInfo.nFrameLen);
		break;
	case ICP_CPC_WAITRESP:
		// wait... tx vend deny/approved at Poll response!
	default:
		ICPDrvSendRW(ICP_RW_ACK);
		break;
	}
	}
#endif
	return nRespType;
}


void ICPProt_CPCReader00(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_READER00 *ptCmd = (T_CPCCMD_READER00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_DISABLE) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_READER;
	ptCmd->nSubCmd = ICP_CPC_CMD_READERDISABLE;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCReader00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_READER00 *ptResp = (T_CPCRESP_READER00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	ICPDrvSendRW(ICP_RW_ACK);
	ICPCPCHook_ReaderEnable(false);
	ICPProt_SetInhibit(icpSlvAddr, true);
#endif
	return nRespType;
}


void ICPProt_CPCReader01(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_READER01 *ptCmd = (T_CPCCMD_READER01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_ENABLE) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_READER;
	ptCmd->nSubCmd = ICP_CPC_CMD_READERENABLE;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCReader01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_READER01 *ptResp = (T_CPCRESP_READER01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	ICPDrvSendRW(ICP_RW_ACK);
	ICPCPCHook_ReaderEnable(true);
	ICPProt_SetInhibit(icpSlvAddr, false);
#endif
	return nRespType;
}


void ICPProt_CPCReader02(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_READER02 *ptCmd = (T_CPCCMD_READER02 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_CANCEL) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_READER;
	ptCmd->nSubCmd = ICP_CPC_CMD_READERCANCEL;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCReader02Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_READER02 *ptResp = (T_CPCRESP_READER02 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			if(ICPInfo.iRX == 2) {
				if(ptResp->nReaderCancelled == ICP_CPC_CANCELLED) {
					ICPDrvSendRW(ICP_RW_ACK);
					ICPCPCHook_OpCancelled();
					nRespType = ICP_CPC_RESPTYPE_CANCELLED;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCRESP_READER02 *ptResp;
	ptResp = (T_CPCRESP_READER02 *)&ICPInfo.buff[0];
	ptResp->nReaderCancelled = ICP_CPC_CANCELLED;
	ICPInfo.nFrameLen = sizeof(ptResp->nReaderCancelled);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	ICPCPCHook_ReaderCancel();
	}
#endif
	return nRespType;
}


void ICPProt_CPCRevalue00(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_REVAL00 *ptCmd = (T_CPCCMD_REVAL00 *)&ICPInfo.buff[0];
	CreditCpcValue nRevalue = 0;
	uint16_t nCurrencyCode = 0;
	uint8_t *ptCmdPar = NULL;
	uint8_t nFrameLen;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_REVALUEREQ) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_REVALUE;
	ptCmd->nSubCmd = ICP_CPC_CMD_REVALUEREQUEST;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	if(ptCmdPar) {
		nRevalue = *((CreditCpcValue *)ptCmdPar) / ICPCPCInfo.Setup00.nUSF;
		nCurrencyCode = *((uint16_t *)(ptCmdPar + sizeof(uint32_t)));
	}
	switch(ICPProt_GetLevel()) {
	case 3:
		ptCmd->Revalue.L3.nRevalue = nRevalue;
		ptCmd->Revalue.L3.nCurrencyCode = nCurrencyCode;
		//MR20 Cvt32ToBigEndian(&ptCmd->Revalue.L3.nRevalue);
		ptCmd->Revalue.L3.nRevalue = __REV(ptCmd->Revalue.L3.nRevalue);
		//MR20 Cvt16ToBigEndian(&ptCmd->Revalue.L3.nCurrencyCode);
		ptCmd->Revalue.L3.nCurrencyCode = __REV16(ptCmd->Revalue.L3.nCurrencyCode);
		nFrameLen += sizeof(ptCmd->Revalue.L3);
		break;
	default:
		ptCmd->Revalue.L1.nRevalue = (CPCCreditType_L1)nRevalue;
		//MR20 Cvt16ToBigEndian(&ptCmd->Revalue.L1.nRevalue);
		ptCmd->Revalue.L1.nRevalue = __REV16(ptCmd->Revalue.L1.nRevalue);
		nFrameLen += sizeof(ptCmd->Revalue.L1);
		break;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCRevalue00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_REVAL00 *ptResp = (T_CPCRESP_REVAL00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			if(ICPInfo.iRX == 2) {
				if(ptResp->nRevalDenied == ICP_CPC_REVALDENY) {
					ICPDrvSendRW(ICP_RW_ACK);
					ICPCPCHook_RevalueDenied();
					nRespType = ICP_CPC_RESPTYPE_REVALDENY;
				} else {
					if(ptResp->nRevalApp == ICP_CPC_REVALAPP) {
						ICPDrvSendRW(ICP_RW_ACK);
						ICPCPCHook_RevalueApproved();
						nRespType = ICP_CPC_RESPTYPE_REVALAPP;
					}
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_REVAL00 *ptCmd;
	T_CPCRESP_REVAL00 *ptResp;
	CPCCreditType_L3 nRevalue;
	uint16_t nCCode = 0xFFFF;

	ptCmd = (T_CPCCMD_REVAL00 *)&ICPInfo.buff[0];
	if(ICPProt_GetLevel() <= 2) {
		Cvt16ToBigEndian(&ptCmd->Revalue.L1.nRevalue)
		nRevalue = ptCmd->Revalue.L1.nRevalue;
		ICPCPCInfo.RevalueLimit.nLimitAmount = nRevalue;
	} else {
		Cvt32ToBigEndian(&ptCmd->Revalue.L3.nRevalue)
		Cvt16ToBigEndian(&ptCmd->Revalue.L3.nCurrencyCode)
		nRevalue = ptCmd->Revalue.L3.nRevalue;
		nCCode = ptCmd->Revalue.L3.nCurrencyCode;
		ICPCPCInfo.RevalueLimit.nLimitAmount = nRevalue;
		ICPCPCInfo.RevalueLimit.nCurrencyCode = nCCode;
	}
	nRevalue *= ICPCPCInfo.Setup00.nUSF;
	// Gestire CURRENCY diversi?? vedi "EXPANDED CURRENCY MODE"
	nRespType = ICPCPCHook_RevalueRequest(nRevalue, nCCode);
	ptResp = (T_CPCRESP_REVAL00 *)&ICPInfo.buff[0];
	
	/* Manca cambio stato in SESSIONIDLE
	// Iinvio risposta al POLL e non subito
	switch(nRespType) {
	case ICP_CPC_RESPDENY:
		ptResp->nRevalDenied = ICP_CPC_REVALDENY;
		ICPInfo.nFrameLen = sizeof(ptResp->nRevalDenied);
		ICPDrvSendMsg(ICPInfo.nFrameLen);
		break;
	case ICP_CPC_RESPAPP:
		ptResp->nRevalApp = ICP_CPC_REVALAPP;
		ICPInfo.nFrameLen = sizeof(ptResp->nRevalApp);
		ICPDrvSendMsg(ICPInfo.nFrameLen);
		break;
	case ICP_CPC_WAITRESP:
		// wait... tx reval deny/approved at Poll response!
	default:
		ICPDrvSendRW(ICP_RW_ACK);
		break;
	}
	*/
	
	switch(nRespType) {
	case ICP_CPC_RESPDENY:
		SetCPCInfoMask(ICP_CPCINFO_REVALDENY);
		break;
	case ICP_CPC_RESPAPP:
		SetCPCInfoMask(ICP_CPCINFO_REVALAPP);
		break;
	case ICP_CPC_WAITRESP:
		// wait... tx reval deny/approved at Poll response!
	default:
		break;
	}
	ICPDrvSendRW(ICP_RW_ACK);
	}
#endif
	return nRespType;
}


void ICPProt_CPCRevalue01(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_REVAL01 *ptCmd = (T_CPCCMD_REVAL01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_REVALUELIMIT) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_REVALUE;
	ptCmd->nSubCmd = ICP_CPC_CMD_REVALUELIMIT;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCRevalue01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		
		T_CPCRESP_REVAL01 *ptResp = (T_CPCRESP_REVAL01 *)&ICPInfo.buff[0];
		
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			if(ICPInfo.iRX == 2) {
				if(ptResp->nRevalDenied == ICP_CPC_REVALDENY) {
					ICPDrvSendRW(ICP_RW_ACK);
					ICPCPCHook_RevalueDenied();
					nRespType = ICP_CPC_RESPTYPE_REVALDENY;
				}
			} else {
				if(ptResp->reval01.nRevalLimit == ICP_CPC_REVALLIMIT) {
					ICPDrvSendRW(ICP_RW_ACK);
					switch(ICPProt_GetLevel()) {
					case 1:
					case 2:
						ICPCPCInfo.RevalueLimit.nLimitAmount = ptResp->reval01.RevalueLimit.L1.nLimitAmount;
						//MR20 Cvt16ToBigEndian(&ICPCPCInfo.RevalueLimit.nLimitAmount);
						ICPCPCInfo.RevalueLimit.nLimitAmount = __REV16(ICPCPCInfo.RevalueLimit.nLimitAmount);
						ICPCPCHook_RevalueLimit(ICPCPCInfo.RevalueLimit.nLimitAmount * ICPCPCInfo.Setup00.nUSF, 0);
						break;
					case 3:
					default:
	#if	_IcpCpcLevel_ >= 3
						ICPCPCInfo.RevalueLimit.nLimitAmount = ptResp->reval01.RevalueLimit.L3.nLimitAmount;
						ICPCPCInfo.RevalueLimit.nCurrencyCode = ptResp->reval01.RevalueLimit.L3.nCurrencyCode;
						Cvt32ToBigEndian(&ICPCPCInfo.RevalueLimit.nLimitAmount);
						Cvt16ToBigEndian(&ICPCPCInfo.RevalueLimit.nCurrencyCode);
						ICPCPCHook_RevalueLimit(ICPCPCInfo.RevalueLimit.nLimitAmount * ICPCPCInfo.Setup00.nUSF, ICPCPCInfo.RevalueLimit.nCurrencyCode);
	#endif
						break;
					}
					nRespType = ICP_CPC_RESPTYPE_REVALLIMIT;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCRESP_REVAL01 *ptResp;
	CPCCreditType_L3 nLimitAmount;
	uint16_t nCCode = 0xFFFF;
	ICPCPCHook_GetRevalueLimit(&nLimitAmount, &nCCode);
	// Gestire CURRENCY diversi?? vedi "EXPANDED CURRENCY MODE"
	nLimitAmount /= ICPCPCInfo.Setup00.nUSF;
	ptResp = (T_CPCRESP_REVAL01 *)&ICPInfo.buff[0];
	if(ICPProt_GetLevel() <= 2) {
		ptResp->reval01.RevalueLimit.L1.nLimitAmount = nLimitAmount;
		Cvt16ToBigEndian(&ptResp->reval01.RevalueLimit.L1.nLimitAmount)
		ICPInfo.nFrameLen = sizeof(ptResp->reval01.RevalueLimit.L1);
	} else {
		ptResp->reval01.RevalueLimit.L3.nLimitAmount = nLimitAmount;
		ptResp->reval01.RevalueLimit.L3.nCurrencyCode = nCCode;
		Cvt32ToBigEndian(&ptResp->reval01.RevalueLimit.L3.nLimitAmount)
		Cvt16ToBigEndian(&ptResp->reval01.RevalueLimit.L3.nCurrencyCode)
		ICPInfo.nFrameLen = sizeof(ptResp->reval01.RevalueLimit.L3);
	}

	ptResp->reval01.nRevalLimit = ICP_CPC_REVALLIMIT;
	ICPInfo.nFrameLen += sizeof(ptResp->reval01.nRevalLimit);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif

	return nRespType;
}


void ICPProt_CPCExpFF(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_EXPFF *ptCmd = (T_CPCCMD_EXPFF *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar;
	uint8_t nFrameLen;
	uint8_t **pt, *ptData, nDataLen, i;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_EXP;
	ptCmd->nSubCmd = ICP_CPC_CMD_EXPDIAG;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	ptCmdPar = NULL;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CPC_DIAG) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}

	if(ptCmdPar) {
		nDataLen = *((uint8_t *)ptCmdPar);
		if(nDataLen > (ICP_MAX_FRAMELEN - 2))
			nDataLen = ICP_MAX_FRAMELEN - 2;
		pt = (uint8_t **)(ptCmdPar + sizeof(uint8_t));
		ptData = (uint8_t *)(*pt);
		for(i = 0; i < nDataLen; i++) {
			ptCmd->aDiagData[i] = ptData[i];
		}
		nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd) + nDataLen;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCExpFFResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_EXPFF *ptResp = (T_CPCRESP_EXPFF *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPDrvSendRW(ICP_RW_ACK);
			ICPCPCHook_SetDiagData(ptResp->expff.aDiagData, (uint8_t)(ICPInfo.iRX - sizeof(ptResp->expff.nExpFF) - 1));
			nRespType = ICP_CPC_RESPTYPE_EXPFF;
		}
		return nRespType;
	}
#endif

#if (_IcpSlave_ & ICP_CARDREADER)
	// Decido di NON rispondere a questo comando.
#endif
	return nRespType;
}


void ICPProt_CPCExp00(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_EXP00 *ptCmd = (T_CPCCMD_EXP00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_EXP;
	ptCmd->nSubCmd = ICP_CPC_CMD_EXPREQUESTID;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	ICPVMCHook_GetManufactureCode(&ptCmd->Exp00.L1.aManufCode[0], sizeof(ptCmd->Exp00.L1.aManufCode));
	ICPVMCHook_GetSerialNumber(&ptCmd->Exp00.L1.aSerialNumber[0], sizeof(ptCmd->Exp00.L1.aSerialNumber));
	ICPVMCHook_GetModelNumber(&ptCmd->Exp00.L1.aModelNumber[0], sizeof(ptCmd->Exp00.L1.aModelNumber));
	ptCmd->Exp00.L1.nSwVersion = ICPVMCHook_GetSwVersion();
	ICPCPCInfo.nFOptions = ICP_CPCOPT_NONE;
	switch(ICPProt_GetLevel()) {
	case 3:
		//MR20 Cvt16ToBigEndian(&ptCmd->Exp00.L3.nSwVersion);
		ptCmd->Exp00.L3.nSwVersion = __REV16(ptCmd->Exp00.L3.nSwVersion);
		ptCmd->Exp00.L3.nFOptions = ICP_CPCOPT_NONE;
		if(ICPProt_CPCGetFTL())
			ptCmd->Exp00.L3.nFOptions |= ICP_CPCOPT3_FTL;
		if(ICPProt_CPCGetCredit32())
			ptCmd->Exp00.L3.nFOptions |= ICP_CPCOPT3_CREDIT32;
		if(ICPProt_CPCGetMultiCurr())
			ptCmd->Exp00.L3.nFOptions |= ICP_CPCOPT3_MULTICURRENCY;
		if(ICPProt_CPCGetNegVend())
			ptCmd->Exp00.L3.nFOptions |= ICP_CPCOPT3_NEGVEND;
		if(ICPProt_CPCGetDataEntry())
			ptCmd->Exp00.L3.nFOptions |= ICP_CPCOPT3_DATAENTRY;
		//MR20 Cvt32ToBigEndian(&ptCmd->Exp00.L3.nFOptions);
		ptCmd->Exp00.L3.nFOptions = __REV(ptCmd->Exp00.L3.nFOptions);
		ICPCPCInfo.nFOptions = ptCmd->Exp00.L3.nFOptions;
		nFrameLen += sizeof(ptCmd->Exp00.L3);
		break;
	default:
		//MR20 Cvt16ToBigEndian(&ptCmd->Exp00.L1.nSwVersion);
		ptCmd->Exp00.L1.nSwVersion = __REV16(ptCmd->Exp00.L1.nSwVersion);
		nFrameLen += sizeof(ptCmd->Exp00.L1);
		break;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCExp00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_EXP00 *ptResp = (T_CPCRESP_EXP00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPDrvSendRW(ICP_RW_ACK);
			ICPCPCInfo.nFOptions = ICP_CPCOPT_NONE;
			switch(ICPProt_GetLevel()) {
			case 1:
			case 2:
				ICPCPCHook_SetManufactCode(&ptResp->exp00.PeriphID.L1.aManufactCode[0], sizeof(ptResp->exp00.PeriphID.L1.aManufactCode));
				ICPCPCHook_SetSerialNumber(&ptResp->exp00.PeriphID.L1.aSerialNumber[0], sizeof(ptResp->exp00.PeriphID.L1.aSerialNumber));
				ICPCPCHook_SetModelNumber(&ptResp->exp00.PeriphID.L1.aModelNumber[0], sizeof(ptResp->exp00.PeriphID.L1.aModelNumber));
				//MR20 Cvt16ToBigEndian(&ptResp->exp00.PeriphID.L1.nSwVersion)
				ptResp->exp00.PeriphID.L1.nSwVersion = __REV16(ptResp->exp00.PeriphID.L1.nSwVersion);
				ICPCPCHook_SetSWVersion(ptResp->exp00.PeriphID.L1.nSwVersion);
				break;
			case 3:
			default:
	#if	_IcpCpcLevel_ >= 3
				ICPCPCHook_SetManufactCode(&ptResp->exp00.PeriphID.L3.aManufactCode[0], sizeof(ptResp->exp00.PeriphID.L3.aManufactCode));
				ICPCPCHook_SetSerialNumber(&ptResp->exp00.PeriphID.L3.aSerialNumber[0], sizeof(ptResp->exp00.PeriphID.L3.aSerialNumber));
				ICPCPCHook_SetModelNumber(&ptResp->exp00.PeriphID.L3.aModelNumber[0], sizeof(ptResp->exp00.PeriphID.L3.aModelNumber));
				Cvt16ToBigEndian(&ptResp->exp00.PeriphID.L3.nSwVersion)
				ICPCPCHook_SetSWVersion(ptResp->exp00.PeriphID.L3.nSwVersion);
				Cvt32ToBigEndian(&ptResp->exp00.PeriphID.L3.nFOptions)
				ICPCPCInfo.nFOptions = ptResp->exp00.PeriphID.L3.nFOptions;
	#endif
				break;
			}
			nRespType = ICP_CPC_RESPTYPE_PERIPHID;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCRESP_EXP00 *ptResp;
/*non serve!
	T_CPCCMD_EXP00 *ptCmd;
	ptCmd = (T_CPCCMD_EXP00 *)&ICPInfo.buff[0];
	ICPCPCHook_SetManufactCode(&ptCmd->Exp00.L1.aManufCode[0], sizeof(ptCmd->Exp00.L1.aManufCode));
	ICPCPCHook_SetSerialNumber(&ptCmd->Exp00.L1.aSerialNumber[0], sizeof(ptCmd->Exp00.L1.aSerialNumber));
	ICPCPCHook_SetModelNumber(&ptCmd->Exp00.L1.aModelNumber[0], sizeof(ptCmd->Exp00.L1.aModelNumber));
	Cvt16ToBigEndian(&ptCmd->Exp00.L1.nSwVersion)
	ICPCPCHook_SetSWVersion(ptCmd->Exp00.L1.nSwVersion);
*/
	ptResp = (T_CPCRESP_EXP00 *)&ICPInfo.buff[0];
	ICPHook_GetManufactCode(&ptResp->exp00.PeriphID.L1.aManufactCode[0], sizeof(ptResp->exp00.PeriphID.L1.aManufactCode));
	ICPHook_GetSerialNumber(&ptResp->exp00.PeriphID.L1.aSerialNumber[0], sizeof(ptResp->exp00.PeriphID.L1.aSerialNumber));
	ICPHook_GetModelNumber(&ptResp->exp00.PeriphID.L1.aModelNumber[0], sizeof(ptResp->exp00.PeriphID.L1.aModelNumber));
	ptResp->exp00.PeriphID.L1.nSwVersion = ICPHook_GetSWVersion();
	if(ICPProt_GetLevel() <= 2) {
		ICPCPCInfo.nFOptions = ICP_CPCOPT_NONE;
		ICPCPCHook_SetFTLEnable(false);
		ICPCPCHook_SetCredit32Enable(false);
		ICPCPCHook_SetMultiCurrencyEnable(false);
		ICPCPCHook_SetNegVendEnable(false);
		ICPCPCHook_SetDataEntryEnable(false);
		ICPInfo.nFrameLen = sizeof(ptResp->exp00.PeriphID.L1);
	} else {
		ptResp->exp00.PeriphID.L3.nFOptions = ICP_CPCOPT_NONE;
		if(ICPProt_CPCGetFTL())
			ptResp->exp00.PeriphID.L3.nFOptions |= ICP_CPCOPT3_FTL;
		if(ICPProt_CPCGetCredit32())
			ptResp->exp00.PeriphID.L3.nFOptions |= ICP_CPCOPT3_CREDIT32;
		if(ICPProt_CPCGetMultiCurr())
			ptResp->exp00.PeriphID.L3.nFOptions |= ICP_CPCOPT3_MULTICURRENCY;
		if(ICPProt_CPCGetNegVend())
			ptResp->exp00.PeriphID.L3.nFOptions |= ICP_CPCOPT3_NEGVEND;
		if(ICPProt_CPCGetDataEntry())
			ptResp->exp00.PeriphID.L3.nFOptions |= ICP_CPCOPT3_DATAENTRY;
		Cvt32ToBigEndian(&ptResp->exp00.PeriphID.L3.nFOptions)
		ICPCPCInfo.nFOptions = ptResp->exp00.PeriphID.L3.nFOptions;
		ICPInfo.nFrameLen = sizeof(ptResp->exp00.PeriphID.L3);
	}
	ptResp->exp00.nPeriphID = ICP_CPC_PERIPHID;
	ICPInfo.nFrameLen += sizeof(ptResp->exp00.nPeriphID);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif

	return nRespType;
}


void ICPProt_CPCExp01(void) {
#if (_IcpMaster_ == true)
	/*
	uint8_t nFrameLen = 0;

	ICPInfo.buff[nFrameLen++] = icpSlvAddr + ICP_CPC_CMD_EXP;
	ICPInfo.buff[nFrameLen++] = ICP_CPC_CMD_EXPREADUSERFILE;
	ICPInfo.buff[nFrameLen++] = 0;		// N. User File
	ICPDrvSendMsg(nFrameLen);
	*/
#endif
}

uint8_t ICPProt_CPCExp01Resp(void) {
	return ICP_RESPTYPE_NONE;
}


void ICPProt_CPCExp02(void) {
#if (_IcpMaster_ == true)
	/*
	uint8_t nFrameLen = 0;

	ICPInfo.buff[nFrameLen++] = icpSlvAddr + ICP_CPC_CMD_EXP;
	ICPInfo.buff[nFrameLen++] = ICP_CPC_CMD_EXPWRITEUSERFILE;
	ICPInfo.buff[nFrameLen++] = 0;		// N. User File
	ICPInfo.buff[nFrameLen++] = ...;		// User File Length
	ICPInfo.buff[nFrameLen++] = ...;		// User Data
	ICPDrvSendMsg(nFrameLen);
	*/
#endif
}

uint8_t ICPProt_CPCExp02Resp(void) {
	ICPDrvSendRW(ICP_RW_ACK);																	// Per non creare condizioni di blocco e/o provocare il reset
	return ICP_RESPTYPE_NONE;
}


void ICPProt_CPCExp03(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_EXP03 *ptCmd = (T_CPCCMD_EXP03 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_EXP;
	ptCmd->nSubCmd = ICP_CPC_CMD_EXPWRITETIMEDATE;
	ICPVMCHook_GetDate(&ptCmd->Exp03.nYears, &ptCmd->Exp03.nMonths, &ptCmd->Exp03.nDays);
	ICPVMCHook_GetTime(&ptCmd->Exp03.nHours, &ptCmd->Exp03.nMinutes, &ptCmd->Exp03.nSeconds);
	ICPVMCHook_GetDateInfo(&ptCmd->Exp03.nDayOfWeek, &ptCmd->Exp03.nWeekNumber, &ptCmd->Exp03.nSummertime, &ptCmd->Exp03.nHoliday);
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCExp03Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_EXP03 *ptResp = (T_CPCRESP_EXP03 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	ICPDrvSendRW(ICP_RW_ACK);																	// Per non creare condizioni di blocco e/o provocare il reset

	// Date/time synchronization (Vmc Rhea)
	#if(!kModifMDB_RM33Inhibited)
	{
	T_CPCCMD_EXP03 *ptCmd;
	ptCmd = (T_CPCCMD_EXP03 *)&ICPInfo.buff[0];
	ICPHook_DateTimeSet((uint8_t*)&ptCmd->Exp03);
	}
	#endif	//(!kModifMDB_RM33Inhibited)
	// Date/time synchronization (Vmc Rhea)
#endif
	return nRespType;
}


void ICPProt_CPCExp04(void) {
#if (_IcpMaster_ == true)
	T_CPCCMD_EXP04 *ptCmd = (T_CPCCMD_EXP04 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CPC_CMD_EXP;
	ptCmd->nSubCmd = ICP_CPC_CMD_EXPENABLEOPT;
	ptCmd->nFOptions = ICP_CPCOPT_NONE;
	if((ICPCPCInfo.nFOptions & ICP_CPCOPT3_FTL) && ICPProt_CPCGetFTL())
		ptCmd->nFOptions |= ICP_CPCOPT3_FTL;
	if((ICPCPCInfo.nFOptions & ICP_CPCOPT3_CREDIT32) && ICPProt_CPCGetCredit32())
		ptCmd->nFOptions |= ICP_CPCOPT3_CREDIT32;
	if((ICPCPCInfo.nFOptions & ICP_CPCOPT3_MULTICURRENCY) && ICPProt_CPCGetMultiCurr())
		ptCmd->nFOptions |= ICP_CPCOPT3_MULTICURRENCY;
	if((ICPCPCInfo.nFOptions & ICP_CPCOPT3_NEGVEND) && ICPProt_CPCGetNegVend())
		ptCmd->nFOptions |= ICP_CPCOPT3_NEGVEND;
	if((ICPCPCInfo.nFOptions & ICP_CPCOPT3_DATAENTRY) && ICPProt_CPCGetDataEntry())
		ptCmd->nFOptions |= ICP_CPCOPT3_DATAENTRY;
	//MR20 Cvt32ToBigEndian(&ptCmd->nFOptions);
	ptCmd->nFOptions = __REV(ptCmd->nFOptions);
	ICPCPCInfo.nFOptions = ptCmd->nFOptions;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CPCExp04Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CPCRESP_EXP04 *ptResp = (T_CPCRESP_EXP04 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	{
	T_CPCCMD_EXP04 *ptCmd;
	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_CPCCMD_EXP04 *)&ICPInfo.buff[0];
	if(ICPProt_GetLevel() <= 2) {
		ICPCPCInfo.nFOptions = ICP_CPCOPT_NONE;
		ICPCPCHook_SetFTLEnable(false);
		ICPCPCHook_SetCredit32Enable(false);
		ICPCPCHook_SetMultiCurrencyEnable(false);
		ICPCPCHook_SetNegVendEnable(false);
		ICPCPCHook_SetDataEntryEnable(false);
	} else {
		if((!(ptCmd->nFOptions & ICP_CPCOPT3_FTL)) && ICPProt_CPCGetFTL())
			ICPCPCHook_SetFTLEnable(false);
		if((!(ptCmd->nFOptions & ICP_CPCOPT3_CREDIT32)) && ICPProt_CPCGetCredit32())
			ICPCPCHook_SetCredit32Enable(false);
		if((!(ptCmd->nFOptions & ICP_CPCOPT3_MULTICURRENCY)) && ICPProt_CPCGetMultiCurr())
			ICPCPCHook_SetMultiCurrencyEnable(false);
		if((!(ptCmd->nFOptions & ICP_CPCOPT3_NEGVEND)) && ICPProt_CPCGetNegVend())
			ICPCPCHook_SetNegVendEnable(false);
		if((!(ptCmd->nFOptions & ICP_CPCOPT3_DATAENTRY)) && ICPProt_CPCGetDataEntry())
			ICPCPCHook_SetDataEntryEnable(false);
		Cvt32ToBigEndian(&ptCmd->nFOptions);
		ICPCPCInfo.nFOptions = ptCmd->nFOptions;
	}
	}
#endif
	return nRespType;
}



#if _IcpCpcFTL_ > 0

void ICPProt_CPCExpFA(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CPCExpFAResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	//...
#endif
	return nRespType;
}

void ICPProt_CPCExpFB(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CPCExpFBResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	//...
#endif
	return nRespType;
}

void ICPProt_CPCExpFC(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CPCExpFCResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	//...
#endif
	return nRespType;
}

void ICPProt_CPCExpFD(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CPCExpFDResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	//...
#endif
	return nRespType;
}

void ICPProt_CPCExpFE(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CPCExpFEResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CARDREADER)
	//...
#endif
	return nRespType;
}
#endif	// _IcpCpcFTL_ > 0




//-------------------------------------------------
// Routines to give CPC info
//-------------------------------------------------
bool ICPProt_CPCGetOptFeature(uint8_t nOptType) {
	return FALSE;
}

bool ICPProt_CPCGetFTL(void) { 
	return _IcpCpcFTL_; 
}

bool ICPProt_CPCGetCredit32(void) {
	return ((ICPProt_GetLevel() >= 3) && (sizeof(CreditValue) >= sizeof(uint32_t))) ? true : false; 
}

bool ICPProt_CPCGetMultiCurr(void) {
	return ICPProt_CPCGetCredit32();
}

bool ICPProt_CPCGetDataEntry(void) {
	return false;
}

bool ICPProt_CPCGetNegVend(void) {
	bool fNegVend = true;
	bool fKeyNegVend;
	if(ICPProt_GetLevel() <= 2)
		return !fNegVend;
	fKeyNegVend = (ICPCPCInfo.FundInfo.nUserOptions & ICP_CPCPOLL03_REVALUENEGVEND) ? true : false;
	return fNegVend & fKeyNegVend;
}


bool ICPProt_CPCGetMultiVend(void) {
	return (ICPCPCInfo.Setup00.nFOptions & ICP_CPCPOLL01_MULTIVEND) ? true : false;
}

bool ICPProt_CPCGetDisplay(void) {
	return false; 
}

bool ICPProt_CPCGetCashSale(void) {
	return (ICPCPCInfo.Setup00.nFOptions & ICP_CPCPOLL01_CASHSALE) ? true : false;
}

bool ICPProt_CPCGetRefund(void) {
	bool fRefund = (ICPCPCInfo.Setup00.nFOptions & ICP_CPCPOLL01_REFUNDS) ? true : false;
	bool fKeyRefund = true;
	if(ICPProt_GetLevel() >= 3)
		fKeyRefund = (ICPCPCInfo.FundInfo.nUserOptions & ICP_CPCPOLL03_REFUND) ? true : false;
	return fRefund & fKeyRefund;
}

bool ICPProt_CPCGetRevalue(void) {
	bool fRevalue = (ICPProt_GetLevel() >= 2) ? true : false;
	bool fKeyRevalue = true;
	if(ICPProt_GetLevel() >= 3)
		fKeyRevalue = (ICPCPCInfo.FundInfo.nUserOptions & ICP_CPCPOLL03_REVALUENEGVEND) ? true : false;
	return fRevalue & fKeyRevalue;
}

#endif //(_IcpSlave_ & ICP_CARDREADER)

/// ABELE

void ICPProt_CPCSetEvtMask(uint16_t nEvtMask) {
#if (_IcpSlaveCode_ & ICP_CARDREADER)
	nEvtMask &= ~(uint16_t)ICP_CPCINFO_SEQCMDERR;
	nEvtMask &= ~(uint16_t)ICP_CPCINFO_OUTOFSEQ;
	nEvtMask &= ~(uint16_t)ICP_CPCINFO_REFUNDERR;
	SetCPCInfoMask(nEvtMask);
#endif
}


// RM46_2006: PK Modifiche 19/10/2006
//#ifdef kModifMDB_KeyInOut
void ICPProt_CPCResetEvtMask(uint16_t nEvtMask) {
#if (_IcpSlaveCode_ & ICP_CARDREADER)
	ResetCPCInfoMask(nEvtMask);
#endif
}

bool ICPProt_CPCIsSetEvtMask(uint16_t nEvtMask) {
#if (_IcpSlaveCode_ & ICP_CARDREADER)
	return IsSetCPCInfoMask(nEvtMask);
#endif
	return false;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPCPC_IsStateEnable
	Controlla se lo stato del Cashless MDB e' Enable.
	
	IN:	  -
	OUT:  -	true se reader in Enable State, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool  ICPCPC_IsStateEnable(void) {
#if (_IcpSlaveCode_ & ICP_CARDREADER)
	return (ICPCPCInfo.cpc_state == ICP_CPC_ENABLED ? true : false);
#endif
	return false;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPCPC_IsStateIdle
	Controlla se lo stato del Cashless MDB e' Idle.
	
	IN:	  -
	OUT:  -	true se reader in Idle State, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool  ICPCPC_IsStateIdle(void) {
#if (_IcpSlaveCode_ & ICP_CARDREADER)
	return (ICPCPCInfo.cpc_state == ICP_CPC_SESSIDLE ? true : false);
#endif
	return false;
}

/*---------------------------------------------------------------------------------------------*\
Method: Set_CPC_Id_Data
	Controlla se lo stato del Cashless MDB e' Idle.
	
	IN:	  -
	OUT:  -	true se reader in Idle State, altrimenti false
\*----------------------------------------------------------------------------------------------*/
void Set_CPC_Id_Data(uint8_t ix, uint8_t l, uint8_t *point){
	
	uint8_t	 i;
	
	for (i=0;i<l;i++){
		ICPCPCInfo.CPCInfo.aManufactCode[i+ix] = *point;										// info dati CPCP
		point++;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Is_CPC_Level_1
	
	IN:	  -
	OUT:  -	true se reader Level 1, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool Is_CPC_Level_1(void)
{
	return ((ICPCPCInfo.Setup00.nFLevel == 1)? true : false);
}
