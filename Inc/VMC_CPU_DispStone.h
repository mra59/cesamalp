/****************************************************************************************
File:
    VMC_CPU_DispStone.h

Description:
    Definizioni e costanti per il Display Touch Stone.
    

History:
    Date       Aut  Note
    Lug 2021	MR   

*****************************************************************************************/

#ifndef __VMC_CPU_DispStone_H
#define __VMC_CPU_DispStone_H

/*--------------------------------------------------------------------------------------*\
Types definition
\*--------------------------------------------------------------------------------------*/

// --- Definizioni Vecchio Touch ----
#define OldHeader_NumByte	6																	// Numero bytes header vecchio touch
#define Pos_OldFirstChr		6																	// Posizione primo carattere della stringa da inviare
#define OldTrailer_NumByte	4																	// Numero bytes chiusura stringa vecchio touch


// --- Comandi New Touch ----
#define	WRITE_REGS				0x80															// Scrittura Registro Touch
#define	READ_REGS				0x81															// Lettura   Registro Touch
#define	WRITE_VAR				0x82															// Scrittura Variabile Touch
#define	READ_VAR				0x83															// Lettura 	 Variabile Touch
#define	PIC_ID					0x03															// Registro  Pagina attualmente visualizzata
#define	RTC_NOW					0x1f															// Registro  Orologio

#define	HEADER_BYTES_NUM		3																// Lunghezza dell'header del buffer da trasmettere 
#define	PIC_Swicth_CMD_Len		4																// Lunghezza del comando per cambiare pagina


// -- Stringa  ricevuta dal New touch ---
#define PIC_ID_MSB				2																// Posizione nel buffer ricevuto del MSB Pagina attualmente visualizzata
#define PIC_ID_LSB				3																// Posizione nel buffer ricevuto del LSB Pagina attualmente visualizzata
#define Butt_MSB				3																// Posizione nel buffer ricevuto del MSB Codice Pulsante premuto
#define Butt_LSB				4																// Posizione nel buffer ricevuto del LSB Codice Pulsante premuto

// -- Stringa  trasmessa al New touch ---
#define POS_R3					0																// Posizione Byte 1 address Display touch
#define POS_RA					1																// Posizione Byte 2 address Display touch
#define POS_BYTE_CNT			2																// Posizione counter numero di bytes da trasmettere
#define POS_CMD					3																// Posizione del Comando da trasmettere
#define POS_VAR_ADDR_MSB		4																// Posizione del MSB dell'address Variant
#define POS_VAR_ADDR_LSB		5																// Posizione del LSB dell'address Variant
#define POS_FIRST_CHR_STRING	6																// Posizione primo carattere della stringa

#define POS_REG_ADDR			4																// Posizione dell'indirizzo del registro da elaborare
#define POS_FIRST_CHR_REG		5																// Posizione del primo chr da inviare al registro
#define	HEADERLEN				2
#define	COMMANDLEN				1


/*===============================================================
 *             Touch Stone Display Commands
 * =============================================================*/

#define	REG_VERSION			0x00																// Registro versione Nuovo Touch Stone



#define	TouchTxStart		0xAA
#define	Text_8x16			0x54
#define	Text_16x32			0x55
#define	Text_12x24			0x6F
#define	SwitchPicture		0x70
#define	CodeAnimaz			0x9A
#define	STARTANIMAZ			0x00
#define	StandardANIMAZ		0x00
#define	Blend_1_ANIMAZ		0x00
#define	Blend_2_ANIMAZ		0x01
#define	Blend_3_ANIMAZ		0x02
#define	POR_ANIMAZ			0x03
#define	AfterHome			0x04
#define	ESP_ANIMAZ			0x05
#define	ENG_ANIMAZ			0x06
#define	FR_ANIMAZ			0x07
#define	Deca_ANIMAZ			0x08


#define	STOPANIMAZ			0xFF
#define	ColorCommand		0x40
#define	DisplCharacters		0x98

#define	Lib_ID_22			0x22
#define	Lib_ID_23			0x23
#define	Lib_ID_25			0x25
#define	Lib_ID_26			0x26

#define	C_Mode				0x80
#define	C_Dots				0x03
#define	F_color_0_H			0x0
#define	F_color_0_L			0x0
#define	F_color_F8_H		0xF8
#define	F_color_F8_L		0x00
#define	B_color_H			0xff
#define	B_color_L			0xff

// Numero di immagine relativa ai Guasti
#define	NumPag_MSB			0x00									// MSB del numero di pagina da visualizzare: fino a 255 pagine sempre zero

/*===============================================================
 *                     Display States 
 * =============================================================*/

typedef enum {
	TouchNormalOper			=	0,
	TouchInH_Msg			=	1,
	TouchNoLink_Msg			=	2,
	TouchAudit_Msg			=	3,
	TouchVendFail_Msg		=	4,
	TouchNoSel_Msg			=	5,
	TouchSelInCorso_Msg		=	6,
	Touch_OutInit			=	7,
	Touch_VMC_FAIL			=	8,
	Touch_VMC_WaitInit		=	9,
	Touch_VMC_WaitTemper	=	10,
	TouchPrelevaBev_Msg		=	11,
	Touch_VMC_WaitNewVisual	=	12									// Fase di attesa di qualsiasi altra nuova visualizzazione
} T_DispMsg;

// ==================================================================
// ===========    Struct  Stati di Funzionamento Display  ===========
// ==================================================================

typedef struct {
	T_DispMsg	Phase;												// Tipo di pagina visualizzata
	uint16_t	CreditoDisplay;
	uint16_t	ActualPageOnDisplay;
	uint8_t		T_DataBuff[100];
	uint8_t		CmdBuff[20];										// Buffer comandi
	bool		VisFW_Rev;											// Prima Inizializzazione: visualizza Revisione Firmware
	bool		VisFW_IO_Rev;										// Visualizza Revisione Firmware scheda I/O
	bool		ForzaNewVisCred;									// Forza nuova visualizzazione credito
	bool		SetPagePrelevaBevanda;								// Al termine di una selezione si chiede al Touch di visualizzare la pagina di prelievo bevanda
	bool		AnimazInCorso;										// 1 = animazione su touch
	Tmr32Local	ToutAnimazDopoHome;									// Timer di pagina Home dopo il quale attivare l'animazione
	Tmr32Local	ToutReturnHome;										// Timer per tornare alla pagina zero
	Tmr32Local	TimeVisTempCaldaia;									// Timer per visualizzare la temperatura della caldaia quando LOw
	Tmr32Local	TimeTouchDispMsg;									// Timer per visualizzare un messaggio per un determinato periodo
	Tmr32Local	TimeIntegrSportellino;								// Timer integrazione sportellino per evitare continui cambi di pagine touch
} T_Display;








/*--------------------------------------------------------------------------------------*\
Function Prototypes
\*--------------------------------------------------------------------------------------*/

static void 	DispTouch_TxString(uint8_t BuffLenght);
static void 	TextCopy(uint8_t OldBuffLen, uint8_t TextLen);
static void	 	Touch_VisFirmware(void);
static void  	Touch_ReadVersion(void);
static uint8_t  MemoCoordinate(const uint8_t *Coordinate, uint8_t index);
static uint8_t  MemoEndFrame(uint8_t index);

	 

void	Touch_Gest_Visualizzazioni(void);
void 	ReadActualTouchPage(void);
void  	Touch_Visual_Init(void);

static void  Create_Wr_VAR_Buff(uint8_t VAR_Addr_MSB, 													// Address MSB Variabile da aggiornare
						 uint8_t VAR_Addr_LSB, 													// Address LSB Variabile da aggiornare
						 uint8_t OldTouchBuffLen, 												// Lunghezza del buffer da copiare nel nuovo buffer
						 uint8_t StringSize); 													// Lunghezza della stringa da creare




#endif  // __VMC_CPU_DispStone_H