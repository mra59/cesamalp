/****************************************************************************************
File:
    IoT_Comm.c

Description:
    File con funzioni di comunicazione con Gateway MH620.
    

History:
    Date       Aut  Note
    Apr 2021	MR   

*****************************************************************************************/

#include <string.h>
#include <stdio.h>

#include "ORION.H" 
#include "Audit.h"
#include "IoT_Comm.h"
#include "IoT_Files.h"
#include "IICEEP.h"
#include "stm32l4xx_hal.h"
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"
#include "Funzioni.h"
#include "general_utility.h"
#include "OrionCredit.h"
#include "DDCMPHook.h"
#include "DDCMPDTSHook.h"
#include "Monitor.h"
#include "OrionTimers.h"
#include "AWS_subscribe_publish.h"
#include "AuditExt.h"


#if (IOT_PRESENCE == true)

#define	AZZERA_NEW_AWS		true		// Debug


/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

#if AWS_CONFIG
	bool		AWS_Certificate, AWS_PrivateKey, AWS_Device, AWS_AccountAddress;
	uint8_t		NumNewAWSConfigs;
#endif

uint8_t		GTW_command;

/*--------------------------------------------------------------------------------------*\
Private Declarations 
\*--------------------------------------------------------------------------------------*/

RTxVars 			gCommVars;
IOT_COMM_STATE		DeviceIoT_State, NextBuffType;
static	uint16_t	rcvdnumchar;
GW_STAT	 			GtwyStatus;
char				RemoteErogazAnswerBuffer[REMOTE_EROGAZ_ANSW_BUFSIZE];

#if AWS_CONFIG
	uint16_t	EEAddr;
#endif

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	LDD_RTC_TTime 		DataOra;
extern	uint8_t			NumCifratura;

#if AWS_CONFIG
	extern	bool		Check_New_AWS_Config(uint8_t* resp, uint8_t* Quantity);
	extern	uint16_t  	GetMessageFromEE(uint16_t *Addr_EEP, uint8_t *DestBuf);
#endif

/*---------------------------------------------------------------------------------------------*\
Method: Task_Gtwy
	Chiamata continuamente dal main per controllare l'arrivo di un buffer dalla Gateway e
	l'eventuale timeout di attesa di questa risposta dal parte della CPU.
	Si esce con un valore relativo alla eventuale attivita' richiesta dalla Gateway:
	possono essere il comando oppure l'informazione che l'Audit e' stata regolarmente trasmessa
	ad AWS.
	Questa funzione, tramite la "Gtwy_Poll_Answ", esegue anche azioni conseguenti ai comandi
	ricevuti: ad es. reset della CPU alla ricezione dei comandi "CMD_DIS_GETT" e "CMD_ENA_GETT".

	IN:	  - 
	OUT:  true se bisogna inviare subito qualcosa alla Gateway
\*----------------------------------------------------------------------------------------------*/
void  Task_Gtwy(void)
{
	uint32_t	TimeoutGtwy;																					// Usate per ridurre il tempo di disable IRQ
	uint8_t		RxCrc;

	if (gCommVars.BuffComplete == true)
	{																											// Ricevuto buffer dalla Gateway
		gCommVars.BuffComplete = false;
		gCommVars.CommandTxCnt = 0;
		RxCrc = ((ascii_to_hex(gCommVars.Gtwy_RxBuff[rcvdnumchar-2])<<4) | (ascii_to_hex(gCommVars.Gtwy_RxBuff[rcvdnumchar-1])));
		if (Check_LRC(gCommVars.Gtwy_RxBuff, (rcvdnumchar-2), RxCrc))
		{
			NextBuffType = Gtwy_Poll_Answ();																	// CRC corretto, elaboro risposta Gateway
			if (NextBuffType != IoT_POLL)
			{
				TmrStart(gCommVars.GtwyTime, OneSec);															// Ricarico Timeout Comunicazione Gateway per riprendere le normali comunicazioni dopo questa
				Gtwy_Poll();
			}
			return;
		}		
	}
	else
	{
		if (gCommVars.CMD_State == Cmd_WaitAnswer)
		{
			HAL_SuspendTick();
			TimeoutGtwy = Touts.GtwyToutAnsw;
			HAL_ResumeTick();
			if (TimeoutGtwy == 0)
			{																									// Timeout scaduto senza avere risposta dalla IoT
				gCommVars.CMD_State = Cmd_IDLE;																	// Restore comunicazione
				gCommVars.BuffComplete = false;
				if (gCommVars.CommandTxCnt > TX_GTWY_MAX_ATTEMPT_COUNT)
				{
					gCommVars.CommandTxCnt= 0;
					DeviceIoT_State = IoT_RESET;																// Procedo col comando Reset al superamento nel numero massimo di comunicazione
					Reset_WiFi_Module();
				}
			}
		}
		else
		{
			//-- Normali comunicazioni con scheda Gateway ogni 1 sec ---
			if (TmrTimeout(gCommVars.GtwyTime))
			{
				TmrStart(gCommVars.GtwyTime, OneSec);															// Ricarico Timeout Comunicazione Gateway
				Gtwy_Poll();
			}
		}
	}
/*
	//-- Normali comunicazioni con scheda Gateway ogni 1 sec ---
	if (TmrTimeout(gCommVars.GtwyTime))
	{
		TmrStart(gCommVars.GtwyTime, OneSec);																	// Ricarico Timeout Comunicazione Gateway
		Gtwy_Poll();
	}
*/
	//-- Check invio immediato Status quando cambiano parametri ---
	if (gVmcStatusIoT.StatusChang_IN != gVmcStatusIoT.StatusChang_OUT)
	{
		if (GtwyStatus == GTWY_CONNECTED)
		{																										// Invio nuovo Status solo se Gateway connessa ad AWS, cosi' invia lo Status dopo una accensione
			gCommVars.SendStatusMsgToAWS = true;
			gVmcStatusIoT.StatusChang_OUT = gVmcStatusIoT.StatusChang_IN;
		}
	}
}	

/*---------------------------------------------------------------------------------------------*\
Method: Gtwy_Poll_Answ
	Elaborazione risposta della Gateway: chiamata nel Main tramite la "Task_Gtwy" alla 
	ricezione completa di un buffer dati corretto.

	IN:	  - risposta Gateway in gCommVars.Gtwy_RxBuff
	OUT:  - DeviceIoT_State
\*----------------------------------------------------------------------------------------------*/
IOT_COMM_STATE  Gtwy_Poll_Answ(void)
{
	uint32_t	j;
	
	switch(gCommVars.Gtwy_RxBuff[BUFFER_TYPE_IX])
	{
		case BUFFER_TYPE_0:
			// ***************************************************
			// *** Buffer  Type 0:  POLL response da Gateway  ****
			// ***************************************************
			GtwyStatus = (GW_STAT)((ascii_to_hex(gCommVars.Gtwy_RxBuff[BUFFER_FIRST_DATA]) << 4) |
						  			(ascii_to_hex(gCommVars.Gtwy_RxBuff[BUFFER_FIRST_DATA+1] )));
			if ((GtwyStatus == GTWY_RESET) || (GtwyStatus == GTWY_READY))
			{																									// Gateway resettata o Ready(non ha ricevuto BuffType1): tx BuffTypeUno e...
				DeviceIoT_State = IoT_RESET;																	// ...non elaboro l'attuale risposta al POLL
			}
			else if (GtwyStatus == GTWY_CONNECTED)
			{
				// I comandi da AWS tramite la gateway sono elaborati qui come fossero
				// una risposta al POLL, in realta' ricevuti come Buffer Types
				switch(GTW_command)
				{
					case NULL:
						//-- Non ci sono comandi da elaborare --
						if (gCommVars.SendStatusMsgToAWS == true)
						{
							gCommVars.SendStatusMsgToAWS = false;
							Prepare_MachineStatus_Buffer();
							DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
						}
						else if (gCommVars.SendRemoteErogazAnswToAWS == true)
						{
							for (j=0; j < REMOTE_EROGAZ_ANSW_BUFSIZE; j++)
							{
								gCommVars.Gtwy_TxBuff[j] = RemoteErogazAnswerBuffer[j];							// Buffer risposta al comando erogazione remota nel buffer di trasmissione ad AWS
								if (gCommVars.Gtwy_TxBuff[j] == 0) break;
							}
							gCommVars.SendRemoteErogazAnswToAWS = false;
							DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
						}
						/* Commentare per non inviare Status periodici
						//-- Non ci sono comandi da elaborare: controllo Status periodico --
						if (TmrTimeout(gVmcStatusIoT.TxStatusIntervalTimer))
						{
							TmrStart(gVmcStatusIoT.TxStatusIntervalTimer, AWS_StatusSend_Interval_Time);		// Ricarico Timer Invio Status periodico
							Prepare_MachineStatus_Buffer();
							DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
						}
						*/
						break;
					
					case CMD_CPU_RESET:
						HAL_Delay(TwoSec);																		// Attendo 2 secondi per permettere alla Gateway di mandare l'ACK ad AWS, poi reset
						WatchDogReset();																		// Reset uP tramite WatchDog
						while(true){};
						break;

					case CMD_STATUS_REQUEST:
						Prepare_MachineStatus_Buffer();
						DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
						//TmrStart(gVmcStatusIoT.TxStatusIntervalTimer, AWS_StatusSend_Interval_Time);			// Ricarico Timer Invio Status periodico
						break;
						
					case CMD_DIS_GETT:
						gOrionConfVars.Remote_INH = true;
						WriteEEPI2C(IICEEPConfigAddr(EERemoteINH), (byte *)(&gOrionConfVars.Remote_INH), 1U);	// Save Inibizione da remoto
						HAL_Delay(TwoSec);																		// Attendo 2 secondi per permettere alla Gateway di mandare l'ACK ad AWS, poi reset
						WatchDogReset();																		// Reset uP tramite WatchDog
						while(true){};
						break;
						
					case CMD_ENA_GETT:
						gOrionConfVars.Remote_INH = false;
						WriteEEPI2C(IICEEPConfigAddr(EERemoteINH), (byte *)(&gOrionConfVars.Remote_INH), 1U);	// Save abilitazione da remoto
						HAL_Delay(TwoSec);																		// Attendo 2 secondi per permettere alla Gateway di mandare l'ACK ad AWS, poi reset
						WatchDogReset();																		// Reset uP tramite WatchDog
						while(true){};
						break;
						
				}
				GTW_command = NULL;
			}
			break;
			
		case BUFFER_TYPE_1:
			// ***************************************************
			// *** Buffer  Type 1:  INIT response da Gateway  ****
			// ***************************************************
			if (gCommVars.Gtwy_RxBuff[BUFFER_FIRST_DATA] == GTWY_ACK)											// Inviato Buffer Type 1: Init Gateway 
			{
				DeviceIoT_State = IoT_POLL;
			}
			break;
			
		case BUFFER_TYPE_2:
			// **************************************************
			// ***   Buffer  Type 2:  Messaggio  da   AWS   *****
			// **************************************************
			DeviceIoT_State = MQTTcallbackHandler((char*)&gCommVars.Gtwy_RxBuff[0]);							// Elaborazione msg ricevuto da AWS: restituisce il buffer type da inviare in risposta
			break;




	#if AWS_CONFIG
		case BUFFER_TYPE_3:
			// **************************************************
			// ***   Buffer  Type 3:  New AWS Certificate   *****
			// **************************************************
			if (gCommVars.Gtwy_RxBuff[BUFFER_FIRST_DATA] == GTWY_ACK)											// Azzero buffer type command solo se eseguito dalla Gateway
			{
				ClrConfigAWS(BUF_AWS_SECURE_CERTIF);
				AWS_Certificate = false;
				CheckForGTWYReset();
			}
			break;
	
		case BUFFER_TYPE_4:
			// **************************************************
			// ***   Buffer  Type 4:  New AWS Private Key   *****
			// **************************************************
			if (gCommVars.Gtwy_RxBuff[BUFFER_FIRST_DATA] == GTWY_ACK)											// Azzero buffer type command solo se eseguito dalla Gateway
			{
				ClrConfigAWS(BUF_AWS_PRIVATE_KEY);
				AWS_PrivateKey = false;
				CheckForGTWYReset();
			}
			break;
	
		case BUFFER_TYPE_5:
			// *****************************************************
			// ***   Buffer  Type 5:  New AWS Account Address  *****
			// *****************************************************
			if (gCommVars.Gtwy_RxBuff[BUFFER_FIRST_DATA] == GTWY_ACK)											// Azzero buffer type command solo se eseguito dalla Gateway
			{
				ClrConfigAWS(BUF_AWS_ACCOUNT_SERVER_ADDRESS);
				AWS_AccountAddress = false;
				CheckForGTWYReset();
			}
			break;

		case BUFFER_TYPE_6:
			// **************************************************
			// ***   Buffer  Type 6:  New AWS arn Device    *****
			// **************************************************
			if (gCommVars.Gtwy_RxBuff[BUFFER_FIRST_DATA] == GTWY_ACK)											// Azzero buffer type command solo se eseguito dalla Gateway
			{
				ClrConfigAWS(BUF_AWS_DEVICE);
				AWS_Device = false;
				CheckForGTWYReset();
			}
			break;
	
	#endif
	}
	gCommVars.CMD_State = Cmd_IDLE;
	return DeviceIoT_State;
}

/*---------------------------------------------------------------------------------------------*\
Method: Gtwy_Poll
	Chiamata periodicamente dal Main, tramite la Task_Gtwy, per inviare buffer types alla
	Gateway.
	Buffer Type 0: POLL
	Buffer Type 1: INIT Gateway
	Buffer Type 2: send Buffers to AWS
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void  Gtwy_Poll(void)
{
#if AWS_CONFIG
	bool	AWS_Config_Buffer_Type = false;
#endif
	
	uint8_t		j;
	uint16_t	asciiVal, charnum;
	char		*bufpnt;

	if (gCommVars.CMD_State == Cmd_IDLE)
	{
		RTC_GetTimeAndDate(&DataOra);
		
	#if AWS_CONFIG
				AWS_Config_Buffer_Type = false;
				if (DeviceIoT_State == IoT_POLL)
				{
					if (NumNewAWSConfigs != 0)
					{
						if (AWS_Certificate == true)
						{
							DeviceIoT_State = IoT_NEW_AWS_SECURE_CERTIF;
						}
						else if (AWS_PrivateKey == true)
						{
							DeviceIoT_State = IoT_NEW_AWS_PRIVATE_KEY;
						}
						else if (AWS_Device == true)
						{
							DeviceIoT_State = IoT_NEW_AWS_DEVICE;
						}
						else if (AWS_AccountAddress == true)
						{
							DeviceIoT_State = IoT_NEW_AWS_ACCOUNT_SERVER_ADDRESS;
						}
					}
				}
	#endif
		
		switch (DeviceIoT_State)
		{
			case IoT_POLL:
			{
				//***************************************************
				// ****   Buffer  Type  0:  Polling  Gateway  *******
				//***************************************************
				memset(&gCommVars.Gtwy_TxBuff[0], 0x00, BUFF_TYPE_0_SIZE);							// Per rapidita' azzero solo la parte utile per inviare il buffer type
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				*(bufpnt++) = ASCII_STX;
				*(bufpnt++) = BUFFER_TYPE_0;
				break;
			}
			
			case IoT_RESET:
			{
				//***************************************************
				// ****   Buffer  Type  1:   Init  Gateway  *********
				//***************************************************
				if ((SSID_WiFi[0] == 0) || (Psw_SSID_WiFi[0] == 0)) return;							// Non sono programmati SSID e Password, non invio nulla alla Gateway
				memset(&gCommVars.Gtwy_TxBuff[0], 0x00, BUFF_TYPE_1_SIZE);							// Per rapidita' azzero solo la parte utile per inviare il buffer type
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				*(bufpnt++) = ASCII_STX;
				*(bufpnt++) = BUFFER_TYPE_1;
				sprintf(bufpnt, "%.8d", CodiceMacchina);											// Codice macchina come stringa ASCII BCD
				bufpnt +=CodiceMacchina_Len;
				for (j=0; j < LEN_SSID; j++) {														// SSID
					*(bufpnt++) = SSID_WiFi[j];
				}
				for (j=0; j < LEN_PSW_SSID; j++) {													// SSID Password
					*(bufpnt++) = Psw_SSID_WiFi[j];
				}
				sprintf(bufpnt, "%.2d", NumCifratura);												// Numero Cifratura come stringa ASCII BCD
				bufpnt +=2;
				break;
			}

			case IoT_TX_BUFFER_TO_AWS:
				// **************************************************
				// ***   Buffer  Type 2:  Send Buffer to AWS    *****
				// **************************************************
				charnum = strlen(gCommVars.Gtwy_TxBuff);											// Il buffer e' gia stato preparato dalla "Response_AWS_OK_Rejected"
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				bufpnt += charnum;
				DeviceIoT_State = IoT_POLL;															// Per non innescare un loop di invio continuo della risposta
				break;

#if AWS_CONFIG
			case IoT_NEW_AWS_SECURE_CERTIF:
				// **************************************************
				// ***   Buffer  Type 3:  New  AWS  Certificate  ****
				// **************************************************
				Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_SECURE_CERTIF);							// In EEAddr start address RxAWSSecureCertifBuff e banco
				EEAddr = 0;				
				EEAddr += MemoryBytes[1];
				EEAddr += (MemoryBytes[0]<<8);
				memset(&gCommVars.Gtwy_TxBuff[0], 0x00, sizeof(gCommVars.Gtwy_TxBuff));
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				*(bufpnt++) = ASCII_STX;
				*(bufpnt++) = BUFFER_TYPE_3;
				GetMessageFromEE(&EEAddr, (uint8_t*)&gCommVars.Gtwy_TxBuff[2]);
				charnum = strlen(gCommVars.Gtwy_TxBuff);
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				bufpnt += charnum;
				DeviceIoT_State = IoT_POLL;															// Per non innescare un loop di invio continuo della risposta
				AWS_Config_Buffer_Type = true;
				break;

			case IoT_NEW_AWS_PRIVATE_KEY:
				// **************************************************
				// ***   Buffer  Type 4:  New AWS Private Key  ****
				// **************************************************
				Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_PRIVATE_KEY);								// In EEAddr start address RxAWSPrivateKeyfBuff e banco
				EEAddr = 0;				
				EEAddr += MemoryBytes[1];
				EEAddr += (MemoryBytes[0]<<8);
				memset(&gCommVars.Gtwy_TxBuff[0], 0x00, sizeof(gCommVars.Gtwy_TxBuff));
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				*(bufpnt++) = ASCII_STX;
				*(bufpnt++) = BUFFER_TYPE_4;
				GetMessageFromEE(&EEAddr, (uint8_t*)&gCommVars.Gtwy_TxBuff[2]);
				charnum = strlen(gCommVars.Gtwy_TxBuff);
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				bufpnt += charnum;
				DeviceIoT_State = IoT_POLL;															// Per non innescare un loop di invio continuo della risposta
				AWS_Config_Buffer_Type = true;
				break;

			case IoT_NEW_AWS_ACCOUNT_SERVER_ADDRESS:
				// **************************************************
				// ***   Buffer  Type 5:  New AWS Account Adrdress *****
				// **************************************************
				Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_ACCOUNT_SERVER_ADDRESS);					// In EEAddr start address RxAWS_AccountServerAddr e banco
				EEAddr = 0;				
				EEAddr += MemoryBytes[1];
				EEAddr += (MemoryBytes[0]<<8);
				memset(&gCommVars.Gtwy_TxBuff[0], 0x00, sizeof(gCommVars.Gtwy_TxBuff));
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				*(bufpnt++) = ASCII_STX;
				*(bufpnt++) = BUFFER_TYPE_5;
				GetMessageFromEE(&EEAddr, (uint8_t*)&gCommVars.Gtwy_TxBuff[2]);
				charnum = strlen(gCommVars.Gtwy_TxBuff);
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				bufpnt += charnum;
				DeviceIoT_State = IoT_POLL;															// Per non innescare un loop di invio continuo della risposta
				AWS_Config_Buffer_Type = true;
				break;

		case IoT_NEW_AWS_DEVICE:
				// **************************************************
				// ***   Buffer  Type 6:  New AWS arn Device    *****
				// **************************************************
				// **************************************************
				Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_DEVICE);									// In EEAddr start address RxAWS_arnDeviceObject e banco
				EEAddr = 0;				
				EEAddr += MemoryBytes[1];
				EEAddr += (MemoryBytes[0]<<8);
				memset(&gCommVars.Gtwy_TxBuff[0], 0x00, sizeof(gCommVars.Gtwy_TxBuff));
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				*(bufpnt++) = ASCII_STX;
				*(bufpnt++) = BUFFER_TYPE_6;
				GetMessageFromEE(&EEAddr, (uint8_t*)&gCommVars.Gtwy_TxBuff[2]);
				charnum = strlen(gCommVars.Gtwy_TxBuff);
				bufpnt = &gCommVars.Gtwy_TxBuff[0];
				bufpnt += charnum;
				DeviceIoT_State = IoT_POLL;															// Per non innescare un loop di invio continuo della risposta
				AWS_Config_Buffer_Type = true;
				break;
#endif
				
			default:
				// Todo: verificare come modificare DeviceIoT_State
				DeviceIoT_State = IoT_POLL;
				return;
		}
			
		// ---- Calcolo CRC e invio buffer -----
		charnum = (bufpnt-&gCommVars.Gtwy_TxBuff[0]);
		j = SetLRCBuff_Gtwy(&gCommVars.Gtwy_TxBuff[1], charnum);									// Byte checksum in j
		HexByteToAsciiInt(j, &asciiVal, NULL);
		*(bufpnt++) = asciiVal>>8;
		*(bufpnt++) = asciiVal;
		charnum = (bufpnt-&gCommVars.Gtwy_TxBuff[0]);
		gCommVars.Gtwy_TxBuff[charnum++] = ASCII_ETX;
		if (Gtwy_SendBlock(charnum) != ERR_OK)
		{

#if DEB_UART_BUSY_IOTCOMM
			gCommVars.TxBusy++;
			if (gCommVars.TxBusy > 0)
			{
				BlinkErrRedLed_HALT(Milli50, IOT_BUSY);
			}
#endif			
		}


#if AWS_CONFIG
		HAL_SuspendTick();
		if (AWS_Config_Buffer_Type == true)
		{
			Touts.GtwyToutAnsw = LONG_TOUT_WAIT_ANSWER;											// Timeout lungo attesa risposta
		}
		else
		{
			Touts.GtwyToutAnsw = TOUT_WAIT_ANSWER;												// Timeout attesa risposta
		}
		HAL_ResumeTick();
#else
		HAL_SuspendTick();
		Touts.GtwyToutAnsw = TOUT_WAIT_ANSWER;													// Timeout attesa risposta
		HAL_ResumeTick();
#endif

		gCommVars.CommandTxCnt++;																// Counter tentativi di invio del Comando
		gCommVars.CMD_State = Cmd_WaitAnswer;													// Attendo risposta dalla Gateway
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Gtwy_RxData
	Ricezione dati dalla Gateway

	IN:	  - byte ricevuto sulla seriale Gtwy
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Gtwy_RxData(uint8_t Data)
{
	switch(Data)
	{
		case ASCII_STX:
			gCommVars.pntRxGtwy = 0;
			memset(&gCommVars.Gtwy_RxBuff[0], 0x00, sizeof(gCommVars.Gtwy_RxBuff));
			break;
			
		case ASCII_ETX:
			if (gCommVars.BuffComplete == false) {
				gCommVars.BuffComplete = true;
				rcvdnumchar = gCommVars.pntRxGtwy;
			}
			break;
			
		default:
			gCommVars.Gtwy_RxBuff[gCommVars.pntRxGtwy++] = Data;
			break;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Gtwy_RxError
	Errore nella ricezione dati dalla CPU

	IN:	  - byte ricevuto sulla seriale Gtwy
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Gtwy_RxError(uint8_t Data) {

	gCommVars.RxErr++;
}

/*---------------------------------------------------------------------------------------------*\
Method: Gtwy_SendBlock
	Trasmette un blocco dati sulla Gtwy

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
uint8_t  Gtwy_SendBlock(uint16_t Size)
{
	uint8_t		txresult;
	gCommVars.Gtwy_State = Gtwy_IDLE;															// Per ripartire da zero anche dopo aver ricevuto solo un pezzo di risposta
	gCommVars.pntRxGtwy = 0;
	gCommVars.BuffComplete = false;
	gCommVars.RxErr = 0;
	txresult = IoT_SendBlock((uint8_t*)&gCommVars.Gtwy_TxBuff, Size);;
	return txresult;
}

/*--------------------------------------------------------------------------------------*\
Method: SetLRCBuff_Gtwy
	Calcola il LRC di un buffer secondo lo Standard Internazionale ISO 1155:1978

	IN	- pointer al buffer e lunghezza del buffer
	OUT	- byte LRC 
\*--------------------------------------------------------------------------------------*/
static uint8_t SetLRCBuff_Gtwy(char *destbuff, uint16_t buff_size) {

	uint8_t		LRC = 0;
	uint16_t	i;
	
	for (i=0; i < buff_size; i++) {
		LRC += *(destbuff+i);
	}
	return ((LRC ^ 0xFF) + 1);
	//*(destbuff+i) = (LRC ^ 0xFF) + 1;
}

/*--------------------------------------------------------------------------------------*\
Method: Check_LRC
	Calcola il Longitudinal Redundacy Check del buffer passato come parametro e lo
	confronta con l'ultimo byte del buffer che contiene il LRC. 

	IN	- pointer al buffer e lunghezza del buffer
	OUT	- True se LRC Ok, altrimenti False
\*--------------------------------------------------------------------------------------*/
static bool Check_LRC(uint8_t *rdbuffPnt, uint16_t buffsize, uint8_t buffcrc)
{
	uint8_t		LRC = 0;
	uint16_t	i;
	
	if (buffsize == 0) return false;
	for (i=0; i < buffsize; i++) {
		LRC += *(rdbuffPnt+i);
	}
	LRC = (LRC ^ 0xFF) + 1;
	if (LRC == buffcrc) {
		return true;
	} else {
		return false;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Gtwy_Comm_Init
	Inizializzazione ricetrasmissione dati con la Gateway (il set della seriale e' gia' fatto
	nel reset).
	Nel caso sia presente una nuova configurazione AWS da invaire alla GTWAY, si esce con
	DeviceIoT_State = POLL per inviarla e fare in modo che la GTWAY, alla ricezione del primo
	buffertype della configurazione, si posizioni in uno stato di attesa della configurazione
	completa e poi attenda il Reset Command finale.

	IN:	  - 
	OUT:  - DeviceIoT_State
\*----------------------------------------------------------------------------------------------*/
void  Gtwy_Comm_Init(void)
{
#if AWS_CONFIG
	uint8_t	fNewConfResp;
#endif
	
	gCommVars.Gtwy_State = Gtwy_IDLE;
	gCommVars.pntRxGtwy = 0;
	gCommVars.CommandTxCnt = 0;
	gCommVars.BuffComplete = false;
	gCommVars.CMD_State = Cmd_IDLE;
	DeviceIoT_State = IoT_RESET;

#if AWS_CONFIG
	Check_New_AWS_Config(&fNewConfResp, &NumNewAWSConfigs);
	if (NumNewAWSConfigs != 0)
	{
		AWS_Certificate		= ((fNewConfResp & NEW_AWS_SECURE_CERTIF_BIT)? true:false);
		AWS_PrivateKey 		= ((fNewConfResp & NEW_AWS_PRIVATE_KEY_BIT)	? true:false);
		AWS_Device 			= ((fNewConfResp & NEW_AWS_DEVICE_BIT)? true:false);
		AWS_AccountAddress	= ((fNewConfResp & NEW_AWS_ACCOUNT_SERVER_ADDR_BIT)? true:false);
		DeviceIoT_State 	= IoT_POLL;
	}
	else
	{
		AWS_Certificate		= false;
		AWS_PrivateKey 		= false;
		AWS_Device 			= false;
		AWS_AccountAddress	= false;
	}
#endif
}	

#if AWS_CONFIG
/*---------------------------------------------------------------------------------------------*\
Method: CheckForGTWYReset
	Controlla se e' stata inviata alla GTWY tutta la nuova configurazioen AWS per procedere
	all'invio di un comando Reset e renderli attivi.

	IN:	  - 
	OUT:  - DeviceIoT_State aggiornato
\*----------------------------------------------------------------------------------------------*/
static void  CheckForGTWYReset(void)
{
	if (NumNewAWSConfigs > 0)
	{
		NumNewAWSConfigs--;
		if (NumNewAWSConfigs == 0)
		{
			DeviceIoT_State = IoT_RESET;																	// Reset Gateway al termine dell'invio nuova configurazione AWS 
		}
		else
		{
			DeviceIoT_State = IoT_POLL;
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ClrConfigAWS
	Azzera il primo byte della configurazione AWS in EEProm.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  ClrConfigAWS(uint8_t  BuffConfigAWS)
{
#if (AZZERA_NEW_AWS == true)
	uint8_t		Param = 0;
#endif
	
	Rd_EEBuffStartAddr_And_BankSelect(BuffConfigAWS);														// In EEAddr start address Configurazione AWS da azzerare e banco
	EEAddr = 0;				
	EEAddr += MemoryBytes[1];
	EEAddr += (MemoryBytes[0]<<8);

	#if (AZZERA_NEW_AWS == true)
		WriteEEPI2C(EEAddr, &Param, sizeof(Param));
	#endif
}


#endif	// AWS_CONFIG

#endif	// IOT_PRESENCE
	
