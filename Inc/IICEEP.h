/****************************************************************************************
File:
    IICEEP.h

Description:
    Include file con definizioni e strutture della EEProm.

History:
    Date       Aut  Note
    Apr 2019	MR   

 *****************************************************************************************/


//====================  BANK  0  ==========================
#define IICEEPBaseAddress			0x00							// Inizio della EEPROM 24LC1025 (MicroChip) o AT24CM01 (Atmel)
#define EEprom_Bank_Size			0xffff

#define BackupDataStart				(IICEEPBaseAddress)					// Lenght 256  Bytes (128 * 2)
#define ConfigDataStart				(IICEEPBaseAddress + 0x0100)		// Lenght 2048 Bytes (128 * 16)
#define AuditDataStart				(IICEEPBaseAddress + 0x0900)		// Lenght 4096(2432) Bytes (128 * 32)

#define AuditIN_Begin				(IICEEPBaseAddress + 0x0900)
#define AuditIN_Len					0x0800
#define AuditLR_Begin				(IICEEPBaseAddress + 0x1100)
#define AuditLR_Len					0x1000

#define RefundDataStart				(IICEEPBaseAddress + 0x2100)		// Lenght 256  Bytes (128 * 2)
#define BlackListDataStart			(IICEEPBaseAddress + 0x2200)		// Lenght 2048 Bytes (128 * 16)
#define GP_CODES					(IICEEPBaseAddress + 0x2A00)		// Lenght 256 Bytes (128 * 2)

#define EE_FREE1					(IICEEPBaseAddress + 0x2B00)		// Lenght 1280 Bytes (128 * 10)

#define EE_VMC_CONFIG_DATA			(IICEEPBaseAddress + 0x3000)		// Lenght 12288 Bytes (128 * 96)  LEN 0x3000

#define RxConfigBuff				(IICEEPBaseAddress + 0x6000)		// Lenght 24576 Bytes (128 * 128) LEN 16384 (0x6000 - 0xA000)
#define RxAWSSecureCertifBuff		(RxConfigBuff + 0x100)				// Si sovrappone a RxConfigBuff - Lenght 2048 Bytes (16 * 128) (0x6100 - 0x6900)
#define RxAWSPrivateKeyfBuff		(RxAWSSecureCertifBuff + 0x800)		// Si sovrappone a RxConfigBuff - Lenght 2048 Bytes (16 * 128) (0x6900 - 0x7100)
#define RxAWS_AccountServerAddr		(RxAWSPrivateKeyfBuff  + 0x800)		// Si sovrappone a RxConfigBuff - Lenght 128 Bytes  (1 * 128)  (0x7100 - 0x7180)
#define RxAWS_arnDeviceObject		(RxAWS_AccountServerAddr  + 0x80)	// Si sovrappone a RxConfigBuff - Lenght 128 Bytes  (1 * 128)  (0x7180 - 0x7200)
#define RxConfigBuffBank			0

#define TxConfigBuff				(IICEEPBaseAddress + 0xA000)		// Lenght 24576 Bytes (128 * 192) LEN 0x6000 (0xA000-0xffff)
#define TxConfigBuffBank			0

//====================  BANK  1  ==========================
#define EEprom_Bank1				0x10000UL							// Inizio Bank 1 EEPROM (2 banchi da 64K)
#define ExtAudBuffAddr				(IICEEPBaseAddress + 0x0000)		// Lenght 32768 Bytes (128 * 256)  (0x0000 - 0x8000)  Bank 1 della EEPROM
#define LogFileBuffAddr				(IICEEPBaseAddress + 0x8000)		// Lenght 32768 Bytes (128 * 256)  (0x8000 - 0xFFFF)  Bank 1 della EEPROM
#define LogFileBuffAddrBank			1
//===================================================================

//****  Lunghezza dei Buffer in EEprom  ******
#define EE_TXCONFBUF_LEN				0x6000								// 24K
#define EE_RxConfBuff_Len				0x6000								// 24K
#define AuditEstesa_Len					0x8000								// 32K
#define LOG_FILE_BUF_LEN				(EEprom_Bank1- LogFileBuffAddr)		// 32K
#define EE_RxAWSConfigBuff_Len			0x1000								// 4K
#define	RxAWS_AccountServerAddr_Len		0x80								// 128 bytes
#define	RxAWS_arnDeviceName_Len			0x80								// 128 bytes


// ========================================================================
// ================    B A C K U P     D A T A   ==========================
// ========================================================================
//  Definizione Backup Data    256 Bytes - 0x0000 - 0x00FF 
#define BeginBackupData               BackupDataStart					// Inizio della memoria EEPROM
#define BackupDataLenght              (128*2)							// (128 * 2)
#define EndBackupData                 (BeginBackupData+BackupDataLenght-1)

// ========================================================================
// ========    C O N F I G U R A Z I O N E       ==========================
// ========================================================================
// Configurazione    2048 Bytes - 0x0100 - 0x08FF */        

#define BeginConfigData				  ConfigDataStart
#define ConfigDataLenght			  (128*16)							// (128 * 16)
#define EndConfigData                 (BeginConfigData+ConfigDataLenght-1)

#define IICEEPConfigAddr(IICEEPConfigField)		((uint32_t)(&((IICEEPConfig *) BeginConfigData) -> IICEEPConfigField))
#define IICEEPConfigOffset(IICEEPConfigField) 	((byte *)(&((IICEEPConfig *) 0) -> IICEEPConfigField))+ BeginConfigData

typedef	   uint16_t		SizeEEGestore;
typedef    uint16_t		SizeEELocazione;
typedef    uint8_t		SizeEEDPP;
typedef    uint8_t		SizeEEUSF;
typedef    uint8_t		SizeEEOpMode;
typedef    uint8_t		SizeEEMultiVendCard;
typedef    uint8_t		SizeEEPriceHolding;
typedef    uint8_t		SizeEEPriceDisplay;
typedef    uint16_t		SizeEEFree1;
typedef    uint16_t		SizeEEFree2;
typedef    uint8_t		SizeEEFree3;
typedef    uint8_t		SizeEEFree4;
typedef    uint16_t		SizeEEPin1;
typedef    uint16_t		SizeEEPin2;
typedef    uint32_t		SizeEECodiceMacchina;
typedef    uint32_t		SizeEECreditoMaxCarta;
typedef    uint32_t		SizeEEUtente;								// Memorizza il codice utente dell'ultima carta inizializzata
typedef    uint32_t		SizeEEDescrValuta;
typedef    uint16_t		SizeEEMonete;
typedef    uint16_t		SizeEEBanconote;
typedef    uint16_t		SizeEEPrezzo;
typedef    uint8_t		SizeEE_99Sel;
typedef    uint16_t		SizeEEFreeCred;
typedef    uint32_t		SizeEEAccessPsw;
typedef    uint32_t		SizeEEGPPsw;
typedef    uint32_t		SizeEEAuditPsw;
typedef    uint8_t		SizeEEBillValidWCard;
typedef    uint8_t		SizeEERechargeTestCard;
typedef    uint32_t		SizeEECreditoMaxCash;
typedef    uint8_t		SizeEE_CardLevels;
typedef    uint8_t		SizeEE_Discount;
typedef    uint8_t		SizeEE_ListaPrezzo;
typedef    uint16_t		SizeEE_TimeFascia;
typedef	   uint8_t		SizeEEAuditOption;
typedef	   uint8_t		SizeEEFreeCrMode;
typedef	   uint8_t		SizeEEFreeCrPeriod;
typedef	   uint8_t		SizeEEFreeVendMode;
typedef	   uint8_t		SizeEEFreeVendPeriod;
typedef    uint8_t		SizeEEFreeVend;
typedef    uint8_t		SizeEELocazEna;
typedef	   uint8_t		SizeEE_Vega;
typedef	   uint8_t		SizeEE_User;
typedef	   uint8_t		SizeEE_INHP;
typedef    uint16_t		SizeEEMaxChange;
typedef    uint8_t		SizeEEModLav;
typedef    uint16_t		SizeEEGeneralPurpose;
typedef    uint8_t		SizeEEPrelevaAudExt;
typedef    uint8_t		SizeEETestAudExtFull;
typedef    uint8_t		SizeEEPermanentCash;
typedef    uint8_t		SizeEEMinimoCoinTube;
typedef    uint8_t		SizeEEFreeCredAllaSelez;
typedef    uint8_t		SizeEEFreeVendAllaSelez;
typedef    uint8_t		SizeEECntTotSelezLiv;
typedef    uint8_t		SizeEESelCounter;
typedef    uint8_t		SizeEEEquazExChng;
typedef    uint8_t		SizeEEInhRechargeCard;
typedef    uint32_t		SizeEEMaxAcceptCreditCard;
typedef    uint8_t		SizeEECambiaMonete;
typedef    uint8_t		SizeEEGestSel;
typedef    uint8_t		SizeEECoinBillEna;
typedef    uint8_t		SizeEERemoteINH;
typedef    uint16_t		SizeEETimeDiff;
typedef    uint16_t		SizeEETimeSample;
typedef    uint8_t		SizeEEDiffSign;
typedef	   uint8_t		SizeEECifratura;



// ***** ATTENZIONE AI DUMMY BYTES INSERITI DAL COMPILATORE SE SI  DEVE *****
// ***** FARE IL CONTO MANUALMENTE DEGLI INDIRIZZI DENTRO LE STRUTTURE  *****
typedef struct{
	SizeEEAccessPsw				EEAccessPsw;					// SYSP  0x100
	SizeEEGPPsw					EEGP1Psw;						// xxxx  0x104
	SizeEEGPPsw					EEGP2Psw;						// xxxx  0x108
	SizeEEAuditPsw				EEAuditPsw;						// AUDP	 0x10C
	SizeEEGestore 				EEGestore;						// GEST  0x110
    SizeEELocazione				EELocazione;					// LOCM  0x112
    SizeEEDPP					EEDPP;							// DECP	 0x114
    SizeEEUSF					EEUSF;							// UNSF  0x115
    SizeEEOpMode				EEOpMode;						// PROT  0x116
    SizeEEMultiVendCard			EEMultiVendCard;				// SING  0x117 
    SizeEEPriceHolding			EEPriceHolding;					// PRHD  0x118
    SizeEEFree1					EEFree1;						// SO01  0x11A
    SizeEEFree2					EEFree2;						// SO02  0x11C
    SizeEEFree3					EEFree3;						// SO03  0x11E
    SizeEEFree4					EEFree4;						// SO04	 0x11F
    SizeEEPin1					EEPin1;							// FLAG  0x120
    SizeEEPin2					EEPin2;							// CODE  0x122
    SizeEE_99Sel				EE_99Sel;						// xxxx  0x124
    SizeEECodiceMacchina		EECodiceMacchina;				// CODM  0x128			// 27.07.17 Non piu' utilizzato, sostituito da stringa ASCII a 10 cifre
    SizeEECreditoMaxCarta		EECreditoMaxCarta;				// CRMK  0x12C
    SizeEEUtente				EEUtente;						// xxxx  0x130
	SizeEEDescrValuta			EEDescrValuta;					// VALU  0x134
	SizeEEFreeCred				EEFreeCred1;					// FRC1  0x138
	SizeEEFreeCred				EEFreeCred2;					// FRC2  0x13A
	SizeEEFreeCred				EEFreeCred3;					// FRC3  0x13C
	SizeEEFreeCred				EEFreeCred4;					// FRC4  0x13E
	SizeEEFreeCred				EEFreeCred5;					// FRC5  0x140
	SizeEEBillValidWCard		EEBillValidWCard;				// BNKE  0x142
	SizeEERechargeTestCard		EERechargeTestCard;				// xxxx  0x143
	SizeEEMonete				EEMonete[16];					// xxxx  0x144
	SizeEEBanconote				EEBanconote[16];				// xxxx  0x164
	SizeEECreditoMaxCash		EECreditoMaxCash;				// CRMM  0x184
	SizeEE_CardLevels			EECardLevelsEna;				// CLEV  0x188
	SizeEE_Discount				EEDiscountEna;					// DISC  0x189
	SizeEE_ListaPrezzo			EEListaPrezzo;					// PRSC  0x18A
	SizeEE_TimeFascia			EEFasciaSconti1Inizio;			// FSIN  0x18C
	SizeEE_TimeFascia			EEFasciaSconti1Fine;			// FSFI  0x18E
	SizeEEAuditOption			EEAuditOptions;					// AOPT  0x190
	SizeEEFreeCrMode			EEFreeCrMode;					// FRCM  0x191
	SizeEEFreeCrPeriod			EEFreeCrPeriod;					// FRCP  0x192
	SizeEEFreeVendMode			EEFreeVendMode;					// FRCM  0x193
	SizeEEFreeVendPeriod		EEFreeVendPeriod;				// FRCP  0x194
	SizeEEFreeVend				EEFreeVend1;					// FRV1  0x195
	SizeEEFreeVend				EEFreeVend2;					// FRV2  0x196
	SizeEEFreeVend				EEFreeVend3;					// FRV3  0x197
	SizeEEFreeVend				EEFreeVend4;					// FRV4  0x198
	SizeEEFreeVend				EEFreeVend5;					// FRV5  0x199
	SizeEELocazEna				EELocazEnable;					// LOCP  0x19A
    SizeEELocazione				EELocaz1;						// LOCM  0x19C
    SizeEELocazione				EELocaz2;						// LOCM  0x19E
    SizeEELocazione				EELocaz3;						// LOCM  0x1A0
    SizeEELocazione				EELocaz4;						// LOCM  0x1A2
    SizeEE_Vega					EEVega;							// VEGA  0x1A4
    SizeEE_User					EEUser;							// UCOD  0x1A5
    SizeEE_INHP					EEInhp;							// INHP  0x1A6
    SizeEEMaxChange				EEMaxChange;					// CHGM  0x1A8
    SizeEEModLav				EEModLav;						// MODO  0x1AA
    SizeEEPrelevaAudExt			EEPrelevaAudExt;				// EXTR	 0x1AB
    SizeEETestAudExtFull		EETestAudExtFull;				// EXTR	 0x1AC
    SizeEEPermanentCash			EEPermanentCash;				// PERM	 0x1AD
    SizeEEMinimoCoinTube		EEMinimoCoinTube;				// MICT	 0x1AE
    SizeEEFreeCredAllaSelez		EEFreeCredAllaSelez;			// FRCS	 0x1AF
    SizeEEFreeVendAllaSelez		EEFreeVendAllaSelez;			// FRVS	 0x1B0
    SizeEECntTotSelezLiv		EECntTotSelezLiv1;				// CBT1	 0x1B1
    SizeEECntTotSelezLiv		EECntTotSelezLiv2;				// CBT2	 0x1B2
    SizeEECntTotSelezLiv		EECntTotSelezLiv3;				// CBT3	 0x1B3
    SizeEECntTotSelezLiv		EECntTotSelezLiv4;				// CBT4	 0x1B4
	SizeEESelCounter			EESelCounter[64];				// CNxx	 0x1B5 - 0x1F4
	
	SizeEECoinBillEna			EEfCoinEnabled[2];				// xxxx	 0x1F5 - 0x1F6
	SizeEECoinBillEna			EEfBillEnabled[2];				// xxxx	 0x1F7 - 0x1F8
	SizeEETimeDiff				EETimeDiff;						// TERR	 0x1FA - 0x1FB		Il compiltore salta 0x01F9
	SizeEETimeSample			EETimeSample;					// TCAM	 0x1FC - 0x1FD
	SizeEEDiffSign				EEDiffSign;						// SEGN	 0x1FE
	uint8_t						EEByteFree[6];					// xxxx	 0x1FF - 0x204
	
	SizeEEPriceDisplay			EEPriceDisplay;					// PRDS  0x205
    SizeEEEquazExChng			EEEquazExChng;					// EXCH  0x206
    SizeEEInhRechargeCard		EEInhRechargeCard;				// RICU	 0x207
    SizeEEMaxAcceptCreditCard	EEMaxAcceptCreditCard;			// ACCK  0x208 - 0x20B
    SizeEECambiaMonete			EECambiaMonete;					// CHNG  0x20C
    SizeEEGestSel				EEGestSel;						// GSEL  0x20D
    char						EEModelloMacchina[20];			// MODM  0x20e - 0x221		ID102 EVA-DTS (20 chr AN)
    char						EE_SN_Produttore[12];			// SNXX  0x222 - 0x22d		ID105 EVA-DTS (12 chr AN)
    char						EE_CodeMacchina[20];			// CMXX  0x22e - 0x241		ID106 EVA-DTS (20 chr AN) Machine Asset Number
	char						EE_MsgRow1[20];					// MS00	 0x242 - 0x255		Messaggio programmabile Erogazione 1  da mettere sul display LCD
	char						EE_MsgRow2[20];					// MS01	 0x256 - 0x269		Messaggio programmabile Erogazione 2  da mettere sul display LCD
	char						EE_MsgRow3[20];					// MS02	 0x26a - 0x27d		Messaggio programmabile Erogazione 3  da mettere sul display LCD
	char						EE_MsgRow4[20];					// MS03	 0x27e - 0x291		Messaggio programmabile Erogazione 4  da mettere sul display LCD

    char						EESSID[32];						// SSID  0x292 - 0x2b1
    char						EE_Psw_SSID[32];				// WPSW  0x2b2 - 0x2d1
    SizeEERemoteINH				EERemoteINH;					// xxxx  0x2d2
	SizeEE_TimeFascia			EEFasciaAuditSalesInizio;		// AUST  0x2d3 - 0x2d4
	SizeEE_TimeFascia			EEFasciaAuditSalesFine;			// AUSP  0x2d5 - 0x2d6
	SizeEECifratura				EECifratura;					// CIFR  0x2d7				// Determina quale psw la MH620 deve usare per la cifratura

    
	/* ***** DECREMENTARE LO SPAZIO LIBERO QUANDO SI INSERISCONO NUOVI PARAMETRI  **********/
	byte	 					ConfFree[534];					// ConfFree 0x2d8 - 0x04ef
	SizeEEGeneralPurpose		EE_GP[20];						// 0x04F0

	SizeEEPrezzo				EEPrezzoPR[100];				// 0x0518
	SizeEEPrezzo				EEPrezzoPA[100];				// 0x05E0
	SizeEEPrezzo				EEPrezzoPB[100];				// 0x06A8
	SizeEEPrezzo				EEPrezzoPC[100];				// 0x0770
	SizeEEPrezzo				EEPrezzoPD[100];				// 0x0838
}IICEEPConfig;

#define EE_Prezzo_Hlimit	IICEEPConfigAddr(EEPrezzoPR[100])						// Limite superiore array EEPrezzoPR
#define EE_Sconto_Hlimit	IICEEPConfigAddr(EEPrezzoPA[100])						// Limite superiore array EEPrezzoPA

#define	GP_Len				20
#define	Len_ID102			20														// Lunghezza parametro EEModelloMacchina (ID102 EVA-DTS)
#define	EE_CodeMacchinaLen	10														// Lunghezza parametro Codice Macchina   (ID106 EVA-DTS)
#define	NUM_MAX_LCD_MSG		4														// Numero massimo di messaggi MSxx programmabili per LCD
#define	LEN_LCDMSG			20														// Lunghezza massima parametro messaggi MSxx per LCD programmabili
#define	LEN_SSID			32														// Lunghezza parametro SSID
#define	LEN_PSW_SSID		32														// Lunghezza parametro Password SSID


// ========================================================================
// ========    A U D I T   I N                   ==========================
// ========================================================================
//  Definizione Audit IN: 2048 Bytes - 0x0900 - 0x10FF

#define BeginAuditData							AuditDataStart
#define IICEEPAuditAddr(IICEEPAuditField)		((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> IICEEPAuditField))
#define IICEEPAuditOffset(IICEEPAuditField) 	((byte *)(&((IICEEPAudit *) 0) -> IICEEPAuditField))+ BeginAuditData

#define EVENT_NUMBER  96

typedef struct {

    uint32_t   DatePrice;                                                              // EA501		0x0900
    uint16_t   TimePrice;                                                              // EA502		0x0904
    uint32_t   DateInit;                                                               // EA401 		0x0908
    uint16_t   TimeInit;                                                               // EA402 		0x090C

// **** ATTENZIONE: fare in modo che "DateAuditRead" e "TimeAuditRead" siano  *****
// ****             sempre indirizzi consecutivi                              *****    
    uint32_t   DateAuditRead;                                                          // Save Data Prelievo Audit   0x0910
    uint16_t   TimeAuditRead;                                                          // Save Time Prelievo Audit   0x0914

//  Valori AUDIT INitialisation      

    uint32_t   NumReads;               // Address + 0x10 rispetto a DatePrice          // EA301		0x0918
    uint32_t   DateLast;                                                               // EA305 		0x091C
    uint16_t   TimeLast;                                                               // EA306 		0x0920
    
    uint32_t   INCardVnd;    											               // DA201 
    uint32_t   INCardNum;                                                              // DA202 

    uint32_t   INCardDeb;                                                              // DA301 

    uint32_t   INCardCre;                                                              // DA401 
    
    uint32_t   INCardDis;                                                              // DA503 

    uint32_t   INDebFRCR;                                                              // DA1001 
    uint32_t   INCreFRCR;                                                              // DA1003 

    uint32_t   INTestVnd;                                                              // VA201 
    uint32_t   INTestNum;                                                              // VA202 

    uint32_t   INFreeVnd;                                                              // VA301 
    uint32_t   INFreeNum;                                                              // VA302 

    uint32_t   INCashVnd;                                                              // CA201 
    uint32_t   INCashNum;                                                              // CA202 

    uint32_t   INCashBox;                                                              // CA306 
    uint32_t   INCashTub;                                                              // CA307 
    uint32_t   INBankInp;                                                              // CA308 

    uint32_t   INDispensed;                                                            // CA403                                  
    uint32_t   INManDispen;                                                            // CA404                                  

    uint32_t   INOutage;                                                               // CA502 					0x096C

    uint32_t   INCashDis;                                                              // CA702 
    uint32_t   INOverpay;                                                              // CA802 
    uint32_t   INVndExc;                                                               // CA902 

    uint32_t   INCashFill;                                                             // CA1002 

    uint32_t   INEvent1;                                                               //  "EA2*EK_01"
    uint32_t   INEvent2;                                                               //  EA-203_2 
    uint32_t   INEvent3;                                                               //  EA-203_3 
    uint32_t   INEvent4;                                                               //  EA-203_4 
    uint32_t   INEvent8;                                                               //  "EA2*EK_04"
																					   
    uint32_t   INCashNumSel[100];                                       			   //  LA1_0_05 				NON UTILIZZATI
    uint32_t   INCardNumSel[100];                                       			   //  LA1_1_05 				NON UTILIZZATI
																					   
    uint32_t   INTokenVend;        													   //  TA205 valore delle vendite con token 
    uint32_t   INNumSelecToken;    													   //  TA206 numero di selezioni con token 
    uint32_t   INTokenOverPay;     													   //  TA501 valore dell'overpay da token 
																					   
    uint8_t    TubCoinStatus[16];     												   //  CA-1501 locale								0x0CC0
    uint32_t   IN_EA_01_Event;														   //  EA_01  "Payout Interrotto"					0x0CD0
    uint16_t   IN_EK_07_Event;														   //  "EA2*EK_07" "WD da No Sync MDB"				0x0CD4
    uint32_t   IN_EK_08_Event;														   //  "EA2*EK_08" "WD da Chiave USB"				0x0CD8
    uint32_t   IN_EK_10_Event;														   //  "EA2*EK_10" "Card Init"						0x0CDC
    uint32_t   IN_EK_11_Event;														   //  "EA2*EK_11" "InOut Exact Change"				0x0CE0
    uint32_t   IN_EK_12_Event;														   //  "EA2*EK_12" "RFU"							0x0CE4
    uint32_t   IN_EK_13_Event;														   //  "EA2*EK_13" "RFU"							0x0CE8
																					   
    uint32_t   INOverpayCard;														   // DA902 Overpay Cashless						0x0CEC

	//-- DECREMENTARE LO SPAZIO LIBERO QUANDO SI INSERISCONO NUOVI ID --
    byte	 	freeINspace[1038];														// Riservati 2048 bytes Tot, quindi freeINspace = (2048 - bytes usati)		0x0CF0
	uint16_t	INEnd;																	// ***********   Fine ID IN   *************									0x10FE

// ========================================================================
// ========    A U D I T   I N                   ==========================
// ========================================================================
//  Valori AUDIT LastReset LR: 4096 Bytes - 0x1100 - 0x20FF
//  NON SPOSTARE LRCardVnd dalla prima posizione: serve a calcolare inizio e lunghezza Audit LR 

    uint32_t   LRCardVnd;                                                              // DA203 								0x1100
    uint16_t   LRCardNum;                                                              // DA204 
    uint32_t   LRCardDeb;                                                              // DA302 
    uint32_t   LRManCard;                                                              // DA303 
    uint32_t   LRCardCre;                                                              // DA402 
    uint32_t   LRCreFRCR;                                                              // DA403 DA1004 
    uint32_t   LRCardDis;                                                              // DA501 
    uint16_t   NumCardDis;                                                             // DA502 
    uint32_t   LRDebFRCR;                                                              // DA1002 
    uint32_t   LRCashVnd;                                                              // CA203 
    uint16_t   LRCashNum;                                                              // CA204 
    uint32_t   LRCashBox;                                                              // CA302 
    uint32_t   LRCashTub;                                                              // CA303 
    uint32_t   LRBankInp;                                                              // CA304 
    uint32_t   Dispensed;                                                              // CA401                                  
    uint32_t   ManDispen;                                                              // CA402                                  
    uint32_t   LROutage;                                                               // CA501									0x1140
    uint32_t   LRCashDis;                                                              // CA701 
    uint32_t   LROverpay;                                                              // CA801 
    uint32_t   LRVndExc;                                                               // CA901 
    uint32_t   CashFill;                                                               // CA306
    uint16_t   numCoin[16];                                                			   // CA1102 
    uint16_t   numCoin_Free[2];                                                        // libero  
    uint16_t   CashCoin[16];                                               			   // CA1103
    uint16_t   CashCoin_Free[2];                                                       // libero  
    uint16_t   TubCoin[16];                                                			   // CA1104 
    uint16_t   TubCoin_Free[2];                                                		   // libero  
    uint16_t   numBank[16];                                       					   // CA1402 
    uint16_t   LREvent1;                                                               // "EA2*EK_01"	"mSelezSenzaPrezzo"
    uint16_t   LREvent2;                                                               // EA202_2 
    uint16_t   LREvent3;                                                               // EA202_3	"EA2*EK_03 accredito dubbio LR"
    uint16_t   LREvent4;                                                               // EA202_4 
    uint16_t   LREvent8;                                                               // "EA2*EK_04"	"mCaricamBlocchi"
    uint32_t   LREv1Val;                                                               // EA204_1 
    uint32_t   LREv3Val;                                                               // EA204_3 
    uint32_t   LREv4Val;                                                               // EA204_4        
    uint32_t   LREv8Val;                                                               // EA204_8        
    uint32_t   LREv8Ovp;                                                               // EA205_8 
    uint16_t   NumSelFullPrice[100];                                  				   // PA203									0x1200
    uint16_t   NumSelDiscount1[100];                                  				   // PA203									0x12c8
    uint16_t   NumSelDisc[100];                                       				   // PA203	****** OBSOLETO:  NON  PIU'  UTILIZZATO  ******
    uint32_t   LRSelVnd[100];                                         				   // PA204   
    uint32_t   LRTestVnd;                                                              // VA203 
    uint16_t   LRTestNum;                                                              // VA204 
    uint32_t   LRFreeVnd;                                                              // VA303 
    uint16_t   LRFreeNum;                                                              // VA304 
    uint16_t   LRFreeNum4Liv[4];                                                       // EA202_96 EA202_97 EA202_98 EA202_99   
    uint32_t   LRFreeVnd4Liv[4];                                                       // EA204_96 EA204_97 EA204_98 EA204_99   
    uint16_t   LRNumNegativeVend;                                                      // EA202_94 
    uint32_t   LRVndNegativeVend;                                                      // EA204_94 
    uint16_t   NumSelDiscount2[100];                                  				   // PA203									0x1618
    uint16_t   NumSelDiscount3[100];                                 				   // PA203									0x16E0
    uint16_t   NumSelDiscount4[100];                                 				   // PA203									0x17A8
    uint16_t   LR_EA_01_Event;														   // EA_01  "Payout Interrotto"
    uint32_t   LR_EA_01_Val;                                                           
    uint16_t   LR_EK_07_Event;														   // "EA2*EK_07" "WD da No Sync MDB"
    uint16_t   LR_EK_08_Event;														   // "EA2*EK_08" "WD da Chiave USB"
    uint16_t   LR_EK_10_Event;														   // "EA2*EK_10" "Card Init"				0x187C
    uint16_t   LR_EK_11_Event;														   // "EA2*EK_11" "InOut Exact Change"		0x187E
    uint16_t   LR_EK_12_Event;														   // "EA2*EK_12" "RFU"						0x1880
    uint16_t   LR_EK_13_Event;														   // "EA2*EK_13" "RFU"						0x1882

	byte  	   TypeEventList[EVENT_NUMBER];      									   // Lista tipo degli eventi				0x1884
	uint32_t   DateEventList[EVENT_NUMBER];										   	   //										0x18E4
	uint16_t   TimeEventList[EVENT_NUMBER];										       //										0x1A64
	uint8_t    IndexDataEvent;             										       // Indice al buffer circolare eventi		0x1B24

    uint32_t   LROverpayCard;                                                          // DA901   Overpay Cashless				0x1B28

	//-- DECREMENTARE LO SPAZIO LIBERO QUANDO SI INSERISCONO NUOVI ID --
  	byte     freeLRspace[1489];														   // Riservati 2048 bytes Tot, quindi freeLRspace = (2048 - bytes usati) 	0x1B2C
	uint16_t 	 LREnd;																   // ***********   Fine ID LR   *************								0x20FE

}IICEEPAudit;

#define INAuditStart ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> NumReads))
#define INAuditLenght (((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> INEnd)) - ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> NumReads)))

#define LRAuditStart ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> LRCardVnd))
#define LRAuditLenght (((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> LREnd)) - ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> LRCardVnd)))

#define DateTimeLenght (((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> LRCardVnd)) - ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> DatePrice)))

#define EndAuditData		(BeginAuditData+INAuditLenght+LRAuditLenght+DateTimeLenght)

#define OnOffEventsStart ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> TypeEventList[0]))
#define OnOffEventsEnd ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> IndexDataEvent))
#define OnOffEventsLenght (((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> IndexDataEvent)) - ((uint32_t)(&((IICEEPAudit *) BeginAuditData) -> TypeEventList[0]))+1)



// ========================================================================
// ========    R E F U N D    M I F A R E        ==========================
// ========================================================================
// REINSTATE CREDITS  256 Bytes - 0x2100 - 0x21FF        

#define BeginRefundData				  RefundDataStart
#define RefundDataLenght			  256
#define BeginRefundDatetime			  BeginRefundData + (RefundSpace*MaxRefund)
#define EndRefundData                 (BeginRefundData+RefundDataLenght-1)

/*
  Struttura dei dati di Refund.
  MaxRefund e' 5 (al 03.07.15) quindi la eeprom e' occupata per:
  4*5 = 20 Bytes UID
  2*5 = 10 Bytes Credito 
  2*5 = 10 Bytes CRC 
  4        Bytes per il Counter Refund
  --------
  44      Bytes
  
typedef struct {
	uint32_t	UID[MaxRefund];
	uint16_t	RefundVal[MaxRefund];
	uint16_t	CRC16Refund[MaxRefund];
	uint8_t	RefundCnt;							// Il compilatore riserva 4 bytes anche per questo membro: la lunghezza totale della struttura e' di 44 bytes (0x2c)
} REFUNDS;

RefundValues;			0x2100 UID:0x14 - Val:0x1E - CRC:0x28   Counter (il compilatore riserva 4 bytes anche per il counter a 1 byte)
RefundFreeCredit;		0x212C
RefundFreeVend;			0x2158
RefundCashCB;			0x2184
SpazionFree;			0x21B0

DataRefundValues		0x21DC					// Date di salvataggio Refund Values
DataRefundFC			0x21F0					// Date di salvataggio Refund Free Credit
DataRefundFV			0x2204					// Date di salvataggio Refund Free Vend
DataRefundCashCB		0x2218					// Date di salvataggio Refund Carta Cariacamento a Blocchi
SpazioFree				0x222C
*/


// ========================================================================
// ========    B L A C K   L I S T               ==========================
// ========================================================================
// Definizione Black List  2048 Bytes - 0x2200 - 0x29FF 

#define BlackListDataLenghtStd		2048
#define kBlackListMaxCodesStd		1000

#define BeginBlackListDataStd     	BlackListDataStart
#define EndBlackListDataStd       	(BeginBlackListDataStd+BlackListDataLenghtStd-1)

#define BeginBlackListData    		BeginBlackListDataStd
#define BLACKLISTMAXCODES   		kBlackListMaxCodesStd

#define IICEEPBLBeginCodeStd        (BeginBlackListDataStd+4)
#define IICEEPBLBeginCodeExt        (BeginBlackListDataExt+4)
#define IICEEPBLBeginCode           (BeginBlackListData+4)
    
#define IICEEPBLCode(IICEEPBlackListField) ((uint32_t)(&((IICEEPBlackList *) BeginBlackListData) -> IICEEPBlackListField))

typedef struct{
  uint16_t  CodeSorted[1];
  uint16_t  CodeNumber[1];
  uint16_t  Code[kBlackListMaxCodesStd];
} IICEEPBlackList;



// ========================================================================
// ======= G E N E R A L   P U R P O S E   C O D E S   ====================
// ========================================================================
// Codici Vari    256 Bytes - 0x2A00 - 0x2B00     

#define BEGIN_DATA_CODES			GP_CODES
#define DATA_CODES_LEN				256
#define END_DATA_CODES				(BEGIN_DATA_CODES+DATA_CODES_LEN-1)

#define RFUAddr(RFU_Field)			((uint32_t)(&((RFU_Data *) BEGIN_DATA_CODES) -> RFU_Field))
#define IICEEPExtAuditHeader() 		((uint32_t)(BEGIN_DATA_CODES + sizeof(RFU_Data)))
#define BattuteCostrutt(Batt_Field)	((uint32_t)(&((sVMCTotBatt *) (END_DATA_CODES-68+1)) -> Batt_Field))
#define AlTur(Field)				((uint32_t)(&((AlTur *) (END_DATA_CODES-64+1)) -> Field))

#define IICEEPSNProduttAddr(Field)	((uint32_t)(&((SNproduttore *) ((END_DATA_CODES+1)-12)) -> Field))

// Struttura per test interni
// !!!! NON MODIFICABILE PER NON SPOSTARE STRUCT SUCCESSIVA CON INFO AUDIT ESTESA !!!!
// !!!! IL CUI INDIRIZZO E' CALCOLATO CON IL SIZE DI QUESTA STRUTTURA            !!!!
typedef struct {
	uint8_t		TestByte;					// 0x2A00
	uint8_t		EEDummyByte;				// 0x2A01													Libero per usi diversi
	uint16_t	MonitorState;				// 0x2A02
	uint32_t	TestArea;					// 0x2A04
} RFU_Data;

// Header dell'Audit Estesa localizzato in 0x2A08 della EEprom
typedef struct{
  uint16_t  Blocks;							// 0x2A08
  uint8_t   RollOver;						// 0x2A0A
  uint16_t  NumReads;						// 0x2A0C
} sExtAuditHdr;

typedef struct {
	uint32_t   VMCTotBattute; 				// 0x2abc													// Counter Totale Battute mai azzerabile: serve al produttore per garanzia
} sVMCTotBatt;

typedef struct {
	uint8_t		Data[16];					// 0x2ac0
} AlTur;

//----- Parametri file di LOG che servono al prelievo tramite chiave USB ----
#define LogFileContentLen			(IICEEPBaseAddress + 0x2AD0)	// Lenght 4 Bytes
#define LogFileSize					(IICEEPBaseAddress + 0x2AD4)	// Lenght 3 Bytes
#define RowLen						(IICEEPBaseAddress + 0x2AD7)	// Lenght 1 Byte

//----- Codici di abilitazione azzeramento Audit ----
#define AuditFileReady				(IICEEPBaseAddress + 0x2AD8)	// Lenght 1 Bytes
#define AuditClearEnabled			(IICEEPBaseAddress + 0x2AD9)	// Lenght 1 Bytes

#define	ProductSwRevision	 		(IICEEPBaseAddress + 0x2ADA)	// Lenght 4 Bytes
#define	ProductSerialNum	 		(IICEEPBaseAddress + 0x2ADE)	// Lenght 4 Bytes

//  Il Serial Number Produttore occupa gli ultimi 12 bytes della sezione BEGIN_DATA_CODES
#define	Len_ID105	12																					// Lunghezza parametro EE_SN_Produttore  (ID105 EVA-DTS)
typedef struct {
	char		EE_SN_Produttore[Len_ID105];															// 0x2AF4 - 0x2AFF		ID105 EVA-DTS (12 chr AN)
} SNproduttore;


// ========================================================================
// ===========  P A R A M E T R I    S E L E Z I O N I    =================
// ========================================================================
// --  100 sel x 52 bytes = 5200 Bytes - 0x3000 - 0x3B00

#define BEGIN_CONF_VMC			EE_VMC_CONFIG_DATA
#define CONF_VMC_LEN			(128*22)														// Riservo 22 pagine da 128 bytes = 2816 Bytes 0x3000-0x3B00
#define END_CONF_VMC			(BEGIN_CONF_VMC + CONF_VMC_LEN)

#define EEPConfVMCAddr(EEPConfVMCField)		((uint32_t)(&((VMCConfig *) BEGIN_CONF_VMC) -> EEPConfVMCField))

typedef	uint8_t		SizeEE_VMCConfig;

typedef struct {
    uint8_t			EE_SelOptA[MAXNUMSEL];					// SA00  0x3000						// 100 bytes	Byte A Opzioni Selezioni
    uint8_t			EE_SelOptB[MAXNUMSEL];					// SB00  0x3004						// 100 bytes	Byte B Opzioni Selezioni
    uint8_t			EE_SelezPrezzo[MAXNUMSEL];				// SP00  0x3008						// 100 bytes	Tabella Selezione/Prezzo in Price Holding 
    uint8_t			EE_ParamFree[1];															// Primo byte Libero 
} VMCConfig;

#define	SA_LEN	MAXNUMSEL
#define	SB_LEN	MAXNUMSEL
#define	SP_LEN	MAXNUMSEL

// ========================================================================
// ===========  P A R A M E T R I    M A C C H I N A      =================
// ========================================================================
// Parametri Macchina a 8 bit 256 Bytes - 0x3B00 - 0x3C00

#define BeginVMCParam			END_CONF_VMC
#define VMCParamDataLenght		(128*2)															// Riservo 2 pagine da 128 bytes (256)
#define EndVMCParam				(BeginVMCParam + VMCParamDataLenght)

#define EEPVMCParamAddr(EEPConfVMCField)		((uint32_t)(&((VMCParam *) BeginVMCParam) -> EEPConfVMCField))

typedef	uint8_t	SizeEE_OP_Array;

typedef  enum {
	ExecSlave,						// OP00  0x3B00
    MifarePaymSyst,					// OP01  0x3B01
	GratuitoPaymSyst,				// OP02  0x3B02
    OptLinguaMessaggi,				// OP03  0x3B03
    OptExecRestoSubito,				// OP04  0x3B04
    OptDeconto,						// OP05  0x3B05
    OptCntDecontoUP,				// OP06  0x3B06												// true=counter filtri up - false=counter filtri down
    OptDecontoInhSel,				// OP07  0x3B07												// Inibisce la selezione se deconto filtro over
    OptEnaRisparmioEnerg,			// OP08  0x3B08
    OptEnaONLuci,					// OP09  0x3B09
    OptExecMstSlav,					// OP10  0x3B0A
    MDBPaymSyst,					// OP11  0x3B0B
    MDB_SLV,						// OP12  0x3B0C
    EE_Keyb_Type,					// OP13  0x3B0D
    SenzaPulsanti,					// OP14  0x3B0E
    Erogaz1_FlussimNum,				// OP15  0x3B0F
    Erogaz2_FlussimNum,				// OP16  0x3B10
    Erogaz3_FlussimNum,				// OP17  0x3B11
    Erogaz4_FlussimNum,				// OP18  0x3B12
	OptLCD_Type,					// OP19  0x3B13												// Set a 1 anche se c'e' un 4x20 perche' si sono abituati al 16x2 durante il funzionamento
    StartAutomatico,				// OP20  0x3B14
    PolaritaSwitchProdotto_1,		// OP21  0x3B15
    Erogaz1_Out1_Num,				// OP22  0x3B16
    Erogaz1_Out2_Num,				// OP23  0x3B17
    Erogaz2_Out1_Num,				// OP24  0x3B18
    Erogaz2_Out2_Num,				// OP25  0x3B19
    Erogaz3_Out1_Num,				// OP26  0x3B1A
    Erogaz3_Out2_Num,				// OP27  0x3B1B
    Erogaz4_Out1_Num,				// OP28  0x3B1C
    Erogaz4_Out2_Num,				// OP29  0x3B1D
    CleanFilter_OutNum,				// OP30  0x3B1E
    MultiVendAutomatica,			// OP31  0x3B1F												// Scala automaticamante il credito per prolungare la selezione
	MultiCiclo,						// OP32  0x3B20												// Multi ciclo cash in macchina a pulsanti
    PolaritaSwitchProdotto_2,		// OP33  0x3B21
    NumSelInhDaFineProd_1,			// OP34  0x3B22
    NumSelInhDaFineProd_2,			// OP35  0x3B23
	OptEnd,																						// Determina il numero di elementi dell'array utilizzato in RBTConfig
	
	
	
    GatewayCablata,					// OP..  0x3B..												// Gateway collegata con cavo a J18
    ValTemperVentolaON,				// OP..  0x3B..
	OptPresOzono,					// OP..  0x3B..
	OptSirena,						// OP..  0x3B..
	Opt_RTC_COE						// OP..  0x3B..												// RTC Calibration Output Enable
//	OptEnd																						// Determina il numero di elementi dell'array utilizzato in RBTConfig
} ParamName;

typedef struct {
    SizeEE_OP_Array		EE_ParamOpt[256];				// 0x3B00 - 0x3BFF
} VMCParam;

#define	OP_LEN	OptEnd

// -----------------------------------------------------------------------------------------------------------------------------
// Parametri Macchina a 16 bit  512 Bytes - 0x3C00 - 0x3E00

#define BEGIN_VMC_PARAM_16			0x3C00
#define VMC_PARAM_16_LEN			(128*4)													// Riservo 4 pagine da 128 bytes (512)
#define END_VMC_PARAM_16			(BEGIN_VMC_PARAM_16 + VMC_PARAM_16_LEN)

#define EEPVMCParam16Addr(EEPConfVMCField)		((uint32_t)(&((VMCParam_16 *) BEGIN_VMC_PARAM_16) -> EEPConfVMCField))

typedef	uint16_t  SizeEE_MI_Array;

typedef  enum {
	VMCPsw_1,						// MI00  0x3C00											// PSW Livello 1: caricatore
    VMCPsw_2,						// MI01  0x3C02											// PSW Livello 2: Tecnico
    VMCPsw_3,						// MI02  0x3C04											// PSW Livello 3: Costruttore
    ValMaxDeconto,					// MI03  0x3C06
    ValRiservaDeconto,				// MI04  0x3C08
    ActualDeconto,					// MI05  0x3C0A
	ValMaxDeconto_1,				// MI06  0x3C0C
    ValRiservaDeconto_1,			// MI07  0x3C0E
    ActualDeconto_1,				// MI08  0x3C10
    ValMaxDeconto_2,				// MI09  0x3C12
    ValRiservaDeconto_2,			// MI10  0x3C14
    ActualDeconto_2,				// MI11  0x3C16
    ValMaxDeconto_3,				// MI12  0x3C18
    ValRiservaDeconto_3,			// MI13  0x3C1A
    ActualDeconto_3,				// MI14  0x3C1C
    ValMaxDeconto_4,				// MI15  0x3C1E
    ValRiservaDeconto_4,			// MI16  0x3C20
    ActualDeconto_4,				// MI17  0x3C22
    DelayRisparmioEnerg,			// MI18  0x3C24

	H2OFlowCounter_1,				// MI19  0x3C26											// Impulsi selezione 1
	H2OFlowCounter_2,				// MI20  0x3C28											// Impulsi selezione 2
	H2OFlowCounter_3,				// MI21  0x3C2A											// Impulsi selezione 3
	H2OFlowCounter_4,				// MI22  0x3C2C											// Impulsi selezione 4

	OZONO_Timer_WAIT,				// MI23  0x3C2E											// Tempo espresso in minuti
	OZONO_Timer_ON,					// MI24  0x3C30											// Tempo espresso in minuti
    TxStatusIntervalTime,			// MI25  0x3C32											// Tempo espresso in minuti
	SIRENA_Timer_Delay,				// MI26  0x3C34											// Tempo espresso in minuti
	SIRENA_Timer_ON,				// MI27  0x3C36											// Tempo espresso in minuti
	
	EETimeFlushing,					// MI28  0x3C38 										// Tempo espresso in minuti

	TIMESelez_1,					// MI29  0x3C3A											// Temporizzazione selezione 1
	TIMESelez_2,					// MI30  0x3C3C											// Temporizzazione selezione 2
	TIMESelez_3,					// MI31  0x3C3E											// Temporizzazione selezione 3
	TIMESelez_4,					// MI32  0x3C40											// Temporizzazione selezione 4

    MiscEnd																					// Determina il numero di elementi dell'array 
} MiscName;

typedef struct {
    SizeEE_MI_Array		EE_ParamMisc[256];				// 0x3C00 - 0x3DFF
} VMCParam_16;

#define	MI_LEN				MiscEnd

// ========================================================================
// ===========  F A S C E   O R A R I E                   =================
// ========================================================================
// Parametri Fasce Orarie - Dato da 16 bit  512 Bytes - 0x3E00 - 0x4000

#define BEGIN_FASCE_ORARIE			0x3E00
#define FASCE_ORARIE_LEN			(128*4)														// Riservo 4 pagine da 128 bytes (512)
#define END_FASCE_ORARIE			(BEGIN_FASCE_ORARIE + FASCE_ORARIE_LEN)

#define EEFasceOrarieAddr(EEPFasceField)		((uint32_t)(&((Param_Fasce *) BEGIN_FASCE_ORARIE) -> EEPFasceField))

typedef	uint16_t  SizeEE_FA_Array;

typedef  enum {
	// -- Fasce orarie Temperatura di Spegnimento --
    Lun_F1_OFF_IN,					// FA00  0x3E00
    Lun_F1_OFF_FI,
    Lun_F2_OFF_IN,
    Lun_F2_OFF_FI,
    Lun_F3_OFF_IN,
    Lun_F3_OFF_FI,
    Lun_F4_OFF_IN,
    Lun_F4_OFF_FI,
    
    Mar_F1_OFF_IN,					// FA08  0x3E10
    Mar_F1_OFF_FI,
    Mar_F2_OFF_IN,
    Mar_F2_OFF_FI,
    Mar_F3_OFF_IN,
    Mar_F3_OFF_FI,
    Mar_F4_OFF_IN,
    Mar_F4_OFF_FI,
    
    Mer_F1_OFF_IN,					// FA16  0x3E20
    Mer_F1_OFF_FI,
    Mer_F2_OFF_IN,
    Mer_F2_OFF_FI,
    Mer_F3_OFF_IN,
    Mer_F3_OFF_FI,
    Mer_F4_OFF_IN,
    Mer_F4_OFF_FI,
   
    Gio_F1_OFF_IN,					// FA24  0x3E30
    Gio_F1_OFF_FI,
    Gio_F2_OFF_IN,
    Gio_F2_OFF_FI,
    Gio_F3_OFF_IN,
    Gio_F3_OFF_FI,
    Gio_F4_OFF_IN,
    Gio_F4_OFF_FI,

    Ven_F1_OFF_IN,					// FA32  0x3E40
    Ven_F1_OFF_FI,
    Ven_F2_OFF_IN,
    Ven_F2_OFF_FI,
    Ven_F3_OFF_IN,
    Ven_F3_OFF_FI,
    Ven_F4_OFF_IN,
    Ven_F4_OFF_FI,

    Sab_F1_OFF_IN,					// FA40  0x3E50
    Sab_F1_OFF_FI,
    Sab_F2_OFF_IN,
    Sab_F2_OFF_FI,
    Sab_F3_OFF_IN,
    Sab_F3_OFF_FI,
    Sab_F4_OFF_IN,
    Sab_F4_OFF_FI,

    Dom_F1_OFF_IN,					// FA48  0x3E60
    Dom_F1_OFF_FI,
    Dom_F2_OFF_IN,
    Dom_F2_OFF_FI,
    Dom_F3_OFF_IN,
    Dom_F3_OFF_FI,
    Dom_F4_OFF_IN,
    Dom_F4_OFF_FI,

	// -----------------------------------------------
	// -- Fasce orarie accensione Luci --
    Lun_F1_LUCI_IN,					// FB00  0x3E70
    Lun_F1_LUCI_FI,
    Lun_F2_LUCI_IN,
    Lun_F2_LUCI_FI,
    Lun_F3_LUCI_IN,
    Lun_F3_LUCI_FI,
    Lun_F4_LUCI_IN,
    Lun_F4_LUCI_FI,
                                       
    Mar_F1_LUCI_IN,					// FB08  0x3E80
    Mar_F1_LUCI_FI,
    Mar_F2_LUCI_IN,
    Mar_F2_LUCI_FI,
    Mar_F3_LUCI_IN,
    Mar_F3_LUCI_FI,
    Mar_F4_LUCI_IN,
    Mar_F4_LUCI_FI,
                                       
    Mer_F1_LUCI_IN,					// FB16  0x3E90
    Mer_F1_LUCI_FI,
    Mer_F2_LUCI_IN,
    Mer_F2_LUCI_FI,
    Mer_F3_LUCI_IN,
    Mer_F3_LUCI_FI,
    Mer_F4_LUCI_IN,
    Mer_F4_LUCI_FI,
                                       
    Gio_F1_LUCI_IN,					// FB24  0x3EA0
    Gio_F1_LUCI_FI,
    Gio_F2_LUCI_IN,
    Gio_F2_LUCI_FI,
    Gio_F3_LUCI_IN,
    Gio_F3_LUCI_FI,
    Gio_F4_LUCI_IN,
    Gio_F4_LUCI_FI,
                                       
    Ven_F1_LUCI_IN,					// FB32  0x3EB0
    Ven_F1_LUCI_FI,
    Ven_F2_LUCI_IN,
    Ven_F2_LUCI_FI,
    Ven_F3_LUCI_IN,
    Ven_F3_LUCI_FI,
    Ven_F4_LUCI_IN,
    Ven_F4_LUCI_FI,
                                      
    Sab_F1_LUCI_IN,					// FB40  0x3EC0
    Sab_F1_LUCI_FI,
    Sab_F2_LUCI_IN,
    Sab_F2_LUCI_FI,
    Sab_F3_LUCI_IN,
    Sab_F3_LUCI_FI,
    Sab_F4_LUCI_IN,
    Sab_F4_LUCI_FI,
                                       
    Dom_F1_LUCI_IN,					// FB48  0x3ED0
    Dom_F1_LUCI_FI,
    Dom_F2_LUCI_IN,
    Dom_F2_LUCI_FI,
    Dom_F3_LUCI_IN,
    Dom_F3_LUCI_FI,
    Dom_F4_LUCI_IN,
    Dom_F4_LUCI_FI,
    FasceEnd																					// Determina il numero di elementi dell'array
} FasceName;

typedef struct {
    SizeEE_FA_Array		EE_ParamFasc[256];				// 0x3E00 - 0x3FFF
} Param_Fasce;

#define	FA_LEN				Lun_F1_LUCI_IN-Lun_F1_OFF_IN
#define	FB_LEN				FasceEnd-Lun_F1_LUCI_IN
#define	FasciaSettimana_Len	(Lun_F1_LUCI_IN - Lun_F1_OFF_IN)									// Lunghezza della fasce di una settimana
#define	InizioFasce			Lun_F1_OFF_IN
#define	InizioFasceFB		Lun_F1_LUCI_IN


// ========================================================================
// ===========     A U D I T    E S T E S A               =================
// ========================================================================
//	Definizione Audit Estesa   0x10000 - 0x18000
//  Ogni record e' lungo 16 bytes: 0x8000 / 0x10 = 2048 record di Audit Estesa memorizzabili 

#define BeginExtAuditData		((uint32_t)(EEprom_Bank1))
#define EndExtAuditData         ((uint32_t)(AuditEstesa_Len + BeginExtAuditData))				// 256 pagine da 128 bytes = 32768 Bytes nel Bank 1
#define ExtAuditDataLenght      ((uint32_t)(EndExtAuditData - BeginExtAuditData))

#define	TypeZero				0x00															// Tipo record di intestazione
#define	ExtAudRev_USB			120																// Revisione dell'Audit Estesa (vale per il prelievo USB)
#define	ExtAudRev_DDCMP			0x0120															// Revisione dell'Audit Estesa (vale per il prelievo DDCMP)

//#pragma pack(1)
//typedef struct {

typedef __packed struct {
	byte		dateTime[4];
	byte		type;
	uint16_t	value;
	byte		selNumber;
	uint16_t	userCode;
	uint16_t	keyValue;
	uint16_t	value1;
	uint16_t	value2;
} sExtAuditBlock;

#define ExtAuditMaxBlocks  ((uint32_t)((uint32_t)ExtAuditDataLenght / ((uint32_t)sizeof(sExtAuditBlock))) - 2)

#define IICEEPExtAuditBlock(block) ((uint32_t)(BeginExtAuditData + ((uint32_t)(block) * (uint32_t)(sizeof(sExtAuditBlock)))))


// ========================================================================
// ===========     B U F F E R   L O G                    =================
// ========================================================================
//	Quest'area di EEPROM e' riservata per un file di Log, un buffer che puo' essere trasferito su una chiave USB dal bootloader.
//  Address inizio e' LogFileBuffAddr e banco EE in LogFileBuffAddrBank.
//	Essendo un buffer circolare che puo' contenere sia Hex che ASCII, dobbiamo stabilire il meccanismo attraverso il quale il bootloader capisce quanti 
//	bytes ci sono da trasferire sulla chiave: interviene allora il "LogFileSize" che indica l'ampiezza massima del buffer.
//	Si verificano i seguenti casi:
//	1) "LogFileContentLen" = 0 (buffer vuoto): il bootloader non trasferisce nulla.
//	2) "LogFileContentLen" > 0 e "LogFileContentLen" < "LogFileSize" (buffer non pieno): il bootloader trasferisce i "LogFileContentLen" bytes e azzera "LogFileContentLen".
//	3) "LogFileContentLen" > 0 e "LogFileContentLen" > "LogFileSize": il buffer circolare e' stato riempito oltre la sua capacita' ed e' gia'  avvenuto un roll-over;
//		il bootloader trasferisce tutto il buffer sulla chiave ("LogFileSize" bytes) e azzera "LogFileContentLen".
//	C'e' anche la possibilita' di far aggiungere al bootloader, durante il trasferimento del buffer alla chiave USB, un NL ad intervalli predefiniti determinati dal
//	contenuto di RowLen, se > 0 (ad es con RowLen=80 il bootloader inserisce un NL ogni 80 caratteri).

#define BeginLogBufferData        	(LogFileBuffAddr)
#define EndLogBufferData          	(uint32_t)(BeginLogBufferData + LOG_FILE_BUF_LEN)				// 0x87F0 = 34.800 bytes
#define	TestAreaBank1				(EndLogBufferData - 4)											// Bytes per testare funzionamento Bank 1


// ========================================================================
// =====    M E S S A G G I   L C D   E E P R O M  /  F L A S H   =========
// ========================================================================

#define 	kApplMsgFileBegin 				0x1D800
#define 	kApplImageFileBegin 			kApplMsgFileBegin
#define 	kApplMultiLanguageSupport      	false

#define IICEepMsgBegin    typedef enum {        \
				kIICEepMsgIndex_Null
  		#define IICEepMsgDef      ,
  		#define IICEepMsgEnd      ,  kIICEepMsgIndex_Count \
} IICEepMsgIndex;

//-------------------------------------
	IICEepMsgBegin
#if LCDmsg_Lingua
	IICEepMsgDef	mInitMsgLCD
	IICEepMsgDef	mInitMsgLCD2
	IICEepMsgDef	mInitMsgLCD3
	IICEepMsgDef	mInitMsgLCD4
	IICEepMsgDef	mStByNoZucNoResto
	IICEepMsgDef	mStByNoZucNoResto2
	IICEepMsgDef	mStByNoZucNoResto3
	IICEepMsgDef	mStByNoZucNoResto4
	IICEepMsgDef	mStByNoZucDaResto
	IICEepMsgDef	mStByNoZucDaResto2
	IICEepMsgDef	mStByNoZucDaResto3
	IICEepMsgDef	mStByNoZucDaResto4
	IICEepMsgDef	mStByZucNoResto
	IICEepMsgDef	mStByZucNoResto2
	IICEepMsgDef	mStByZucNoResto3
	IICEepMsgDef	mStByZucNoResto4
	IICEepMsgDef	mStByZucDaResto
	IICEepMsgDef	mStByZucDaResto2
	IICEepMsgDef	mStByZucDaResto3
	IICEepMsgDef	mStByZucDaResto4
	IICEepMsgDef	mLCDWait
	IICEepMsgDef	mLCDWait2
	IICEepMsgDef	mLCDWait3
	IICEepMsgDef	mLCDWait4
	IICEepMsgDef	mLCDGetBev
	IICEepMsgDef	mLCDGetBev2
	IICEepMsgDef	mLCDGetBev3
	IICEepMsgDef	mLCDGetBev4
	IICEepMsgDef	mCostoSelez
	IICEepMsgDef	mCostoSelez2
	IICEepMsgDef	mCostoSelez3
	IICEepMsgDef	mCostoSelez4
	IICEepMsgDef	mNoVMC
	IICEepMsgDef	mNoVMC2
	IICEepMsgDef	mNoVMC3
	IICEepMsgDef	mNoVMC4
	IICEepMsgDef	mProdEsaur
	IICEepMsgDef	mProdEsaur2
	IICEepMsgDef	mProdEsaur3
	IICEepMsgDef	mProdEsaur4
	IICEepMsgDef	mNewConfig
	IICEepMsgDef	mNewConfig2
	IICEepMsgDef	mNewConfig3
	IICEepMsgDef	mNewConfig4
	IICEepMsgDef	mWaitReset
	IICEepMsgDef	mWaitReset2
	IICEepMsgDef	mWaitReset3
	IICEepMsgDef	mWaitReset4
	IICEepMsgDef	mRiscaldam
	IICEepMsgDef	mRiscaldam2
	IICEepMsgDef	mRiscaldam3
	IICEepMsgDef	mRiscaldam4
	IICEepMsgDef	mLavGrup
	IICEepMsgDef	mLavGrup1
	IICEepMsgDef	mLavGrup2
	IICEepMsgDef	mLavGrup3
	IICEepMsgDef	mLavMixer
	IICEepMsgDef	mLavMixer1
	IICEepMsgDef	mLavMixer2
	IICEepMsgDef	mLavMixer3
	IICEepMsgDef	mWaitInit
	IICEepMsgDef	mWaitInit1
	IICEepMsgDef	mWaitInit2
	IICEepMsgDef	mWaitInit3
	IICEepMsgDef	mProgrammaz
	IICEepMsgDef	mProgrammaz1
	IICEepMsgDef	mProgrammaz2
	IICEepMsgDef	mProgrammaz3
	IICEepMsgDef	mLCDVendFail
	IICEepMsgDef	mLCDVendFail1
	IICEepMsgDef	mLCDVendFail2
	IICEepMsgDef	mLCDVendFail3
	IICEepMsgDef	mDAVuoto
	IICEepMsgDef	mDAVuoto2
	IICEepMsgDef	mDAVuoto3
	IICEepMsgDef	mDAVuoto4
	IICEepMsgDef	mUSB_KEY
	IICEepMsgDef	mUSB_KEY2
	IICEepMsgDef	mUSB_KEY3
	IICEepMsgDef	mUSB_KEY4
	IICEepMsgDef	mUSB_KEY_Withdraw
	IICEepMsgDef	mUSB_KEY_Withdraw2
	IICEepMsgDef	mUSB_KEY_Withdraw3
	IICEepMsgDef	mUSB_KEY_Withdraw4
	  
#endif

IICEepMsgDef		mInserire
IICEepMsgDef		mPassword
IICEepMsgDef		mNewPassword
IICEepMsgDef		mAzzerare
IICEepMsgDef		mMemoriaDati
IICEepMsgDef		mMenu
IICEepMsgDef		mTempiDosi
IICEepMsgDef		mPrezzoSelez
IICEepMsgDef		mScontoSelez
IICEepMsgDef		mTemperMacchina
IICEepMsgDef		mMenuAudit
IICEepMsgDef		mSistemiPagamento
IICEepMsgDef		mSelezPrezzo
IICEepMsgDef		mOpzioniVarie
IICEepMsgDef		mImpostazioniProduttore
IICEepMsgDef		mCollaudo
IICEepMsgDef		mLavaggi
IICEepMsgDef		mRisparmio
IICEepMsgDef		menuParamFabbrica
IICEepMsgDef		mSelezioneNumero
IICEepMsgDef		mSelezioneUnaCifra
IICEepMsgDef		mReq_DaProgrammare
IICEepMsgDef		mSel_Disable
IICEepMsgDef		mSel_Impulsi
IICEepMsgDef		mSel_Prezzo
IICEepMsgDef		mSel_Sconto
IICEepMsgDef		mTab_SelPrez
IICEepMsgDef		mSel_ColCaps
IICEepMsgDef		mPay_Executive
IICEepMsgDef		mPay_ExecPrHold
IICEepMsgDef		mPay_ExecPrDisplay
IICEepMsgDef		mPay_ExecMDBRestoSubito
IICEepMsgDef		mPay_Mifare
IICEepMsgDef		mPay_MifarePin1
IICEepMsgDef		mPay_MifarePin2
IICEepMsgDef		mPay_NewMifarePin
IICEepMsgDef		mPay_MifareGest
IICEepMsgDef		mPay_CodiceMacchina
IICEepMsgDef		mPay_CodiceMacchina10Cifre  
IICEepMsgDef		msg_CodiceMacchina
IICEepMsgDef		mPay_CodiceLocazione
IICEepMsgDef		msg_CodiceLocazione
IICEepMsgDef		mPay_EnaDiscount
IICEepMsgDef		mPay_CoinNum
IICEepMsgDef		mPay_CoinVal
IICEepMsgDef		mPay_MaxCard
IICEepMsgDef		mPay_MaxCash
IICEepMsgDef		mPay_Gratis
IICEepMsgDef		mPay_MDB
IICEepMsgDef		mPay_MDBSLV
IICEepMsgDef		mPay_MDBMultiVend
IICEepMsgDef		mPay_MDBMaxChange
IICEepMsgDef		mPay_MDBCambiaMonete
IICEepMsgDef		mPay_MDBBillEnaWithCard
IICEepMsgDef		mPay_MDBCoinBillEna
IICEepMsgDef		mPay_MDBBillNum
IICEepMsgDef		mPay_MDBCoinsInTube
IICEepMsgDef		mOpt_DPP
IICEepMsgDef		mOpt_DaType
IICEepMsgDef		mOpt_KeyBoardType
IICEepMsgDef		mOpt_EnaDeconto
IICEepMsgDef		msg_DecontoAttuale
IICEepMsgDef		mOpt_DecontoMassimo
IICEepMsgDef		mOpt_DecontoRiserva
IICEepMsgDef		mCMD_ResetDeconto
IICEepMsgDef		msg_RiservaDeconto
IICEepMsgDef		mOpt_EnaDecalcific
IICEepMsgDef		mOpt_DecalcifMassimo
IICEepMsgDef		mOpt_ClearEE
IICEepMsgDef		mOpt_ClearAuditEE
IICEepMsgDef		mOpt_Sicuro
IICEepMsgDef		mOpt_CmdCollaudo
IICEepMsgDef		mOpt_CmdAttesaTraSelezioni
IICEepMsgDef		mOpt_CollaudoTerminato
IICEepMsgDef		mAud_TotBattuteLR
IICEepMsgDef		mAud_TotBattuteIN
IICEepMsgDef		mAud_TotBattuteTest
IICEepMsgDef		mAud_TotVendutoLR
IICEepMsgDef		mAud_TotVendutoIN
IICEepMsgDef		mOrologio
IICEepMsgDef		mDataOra
IICEepMsgDef		mClockTipoDato
IICEepMsgDef		mOpt_EnaONLuci
IICEepMsgDef		mDayOfTheWeek
IICEepMsgDef		mFasciaNum
IICEepMsgDef		mImpostaFasce
IICEepMsgDef		mSetDefaultFabbrica
IICEepMsgDef		mLanguageNum
//IICEepMsgDef		mPay_ExecMstSlv
IICEepMsgDef		mOpt_AutomaticStart
IICEepMsgDef		mLCD_16x2
IICEepMsgDef		mOpzNumRotaz
//IICEepMsgDef		mOpz_Ozono
IICEepMsgDef		mOpt_SwitchFineProdotto_N_1
IICEepMsgDef		mVal_TimeSel
IICEepMsgDef		mVal_CounterSel
IICEepMsgDef		mOpt_SenzaPulsanti
IICEepMsgDef		mVal_Sel_1_Out1
IICEepMsgDef		mVal_Sel_1_Out2
IICEepMsgDef		mVal_Sel_2_Out1
IICEepMsgDef		mVal_Sel_2_Out2
IICEepMsgDef		mVal_Sel_3_Out1
IICEepMsgDef		mVal_Sel_3_Out2
IICEepMsgDef		mVal_Sel_4_Out1
IICEepMsgDef		mVal_Sel_4_Out2
IICEepMsgDef		mVal_CleanFilterOutNum
IICEepMsgDef		mVal_FlussimErogaz_1
IICEepMsgDef		mVal_FlussimErogaz_2
IICEepMsgDef		mVal_FlussimErogaz_3
IICEepMsgDef		mVal_FlussimErogaz_4
IICEepMsgDef		mVal_TemperVentolaON
IICEepMsgDef		mPay_CPU_SerialNumber
IICEepMsgDef		mLCD_RemoteInhibit
IICEepMsgDef		mVal_ErogazOzono
IICEepMsgDef		mVal_AttesaOzono
IICEepMsgDef		mOpz_Sirena
IICEepMsgDef		mVal_AttivazSirena
IICEepMsgDef		mVal_RitardoSirena
IICEepMsgDef		mOpt_AutomaticMultiVend
IICEepMsgDef		mTipoValuta
IICEepMsgDef		mErrCollaudo
IICEepMsgDef		mOpt_SwitchFineProdotto_N_2
IICEepMsgDef		mOpt_InhSelAfterEndProd_N_1
IICEepMsgDef		mOpt_InhSelAfterEndProd_N_2
IICEepMsgDef		mMultiCiclo



IICEepMsgEnd
//-------------------------------------

//  Definizione Messaggi
#define MessMax					(kIICEepMsgIndex_Count-1)
#define MessLength				20
#define MaxNumLanguage			1																// 2 lingue: 0 e 1

/*
#if	MsgInFlash
  	  #define BeginMess			kSWVectorsBegin
#else
  	  #define BeginMess			((uint32_t)((uint32_t)IICEEPBaseAddress + (uint32_t)kApplMsgFileBegin))
#endif
*/

#define BeginMess				((uint32_t)((uint32_t)IICEEPBaseAddress + (uint32_t)kApplMsgFileBegin))
  
#define IICEEPLangMess(ofs,Lang,Number) { \
  ofs= ((uint32_t)((((Number) * MessLength) + MultiLingua * MessMax * MessLength * (Lang)))); \
  ofs= (uint32_t)(BeginMess + ofs); \
}

#if MultiLingua
  #define MessTotalLength		((uint32_t)((MessMax) * (MessLength) * (gCT5MsgLangCount)))
  #define CT5MsgLangGet()		gCT5MsgLangIx
  #define CT5MsgLangCountGet()	gCT5MsgLangCount
#else
  #define MessTotalLength		((uint32_t)((MessMax) * (MessLength) * (MsgLangCount)))
  //#define MessTotalLength		((uint32_t)((MessMax) * (MessLength)))							// Sostituito dalla define con le lingue
  #define MsgLangCount			1
#endif

#define EndMess					((uint32_t)BeginMess - (uint32_t)MessTotalLength)


// ********************************************************
// ***  Defines   Tipi  Buffers  EEprom                ****
// ********************************************************
#define		BUF_RXCONFIG					0
#define		BUF_TXCONFIG					1
#define		BUF_LOGFILE						2
#define		BUF_GENERIC_FILE				3
#define		BUF_AWS_SECURE_CERTIF			4
#define		BUF_AWS_PRIVATE_KEY				5
#define		BUF_AWS_DEVICE					6
#define		BUF_AWS_ACCOUNT_SERVER_ADDRESS	7

