/* ***************************************************************************************
File:
    general_utility.c

Description:
    Funzioni di utilita' generale

History:
    Date       Aut  Note
    Set 2012 	Abe   

 *****************************************************************************************/

#include "ORION.H" 
#include "general_utility.h"



/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void decipher(uint32_t*, const uint32_t*);
extern	void Read_UniqueID(uint32_t *DestCodes);


/*--------------------------------------------------------------------------------------*\
Private types and variables
\*--------------------------------------------------------------------------------------*/

uint8_t	DecL, DecH, DecHM;


/*---------------------------------------------------------------------------------------------*\
Method: CompareBuffer
	Compara due buffers

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
bool CompareBuffer (uint8_t *ptBuf1, uint8_t *ptBuf2, uint8_t bufLen) {
	
	uint8_t n;
	
	for (n=0; n<bufLen; n++)
		if (ptBuf1[n] != ptBuf2[n]) {
			return false;
		}
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: atouint32
	Routine che converte buffer di valori BCD ASCII in Hex 32bit.

	IN:		- strVal (Pointer to buffer), len (buffer lenght)
	OUT:	- val (HEX 32 bit)
\*----------------------------------------------------------------------------------------------*/
uint32_t  atouint32(uint8_t *strVal, char len)
{
	uint32_t val=0;
	char ch, i;
	
	for (i=0;i<len;i++){
		if ((ch = strVal[i])>='0' && ch<='9'){
			val*= 10;
			val+= ch-'0';
		}
	}
	return val;
}

/*---------------------------------------------------------------------------------------------*\
Method: Dec16Touint16
	Routine che converte un numero decimale su due bytes in un Hex uint16_t.

	IN:		- pointer al dato Decimale
	OUT:	- Hex 16 bit
\*----------------------------------------------------------------------------------------------*/
uint16_t  Dec16Touint16(uint16_t num) {

	//MR19 uint8_t	Ldec, Hdec, HexL, HexH, conv;
	uint8_t	Ldec, Hdec, conv;
	uint16_t	result;
	
	//MR19 HexL = 0;
	//MR19 HexH = 0;
	
	Hdec = (num >> 8);
	conv = DecHex(Hdec);
	result = conv*100;
	
	Ldec = num & 0x00FF;
	conv = DecHex(Ldec);
	result += conv;
	return result;
}
	
/*---------------------------------------------------------------------------------------------*\
Method: DecHex
	Routine che converte un byte decimale in un byte Hex.

	IN:   - byte decimale
	OUT:  - Hex uint8_t
\*----------------------------------------------------------------------------------------------*/
uint8_t   DecHex(uint8_t Decimal) {
	
	uint8_t  TempDec, TempDec1;
	
	TempDec = Decimal & 0xF0;
	//TempDec1 = TempDec / 2;
	//TempDec1 += TempDec / 8;
	TempDec1 = ((TempDec / 2) + (TempDec / 8) + (Decimal & 0x0F));
	//TempDec1 += (Decimal & 0x0F);
	return TempDec1;
}


/*--------------------------------------------------------------------------------------*\
Method:	BcdTouint16
	Converte da BCD a Hex 
	
	IN:	  - pointer al dato BCD e lunghezza del dato sorgente
	OUT:  - uint16_t Hex
\*--------------------------------------------------------------------------------------*/
/*
void BcdTouint16(uint16_t* out, char* AddrSource, uint8_t len)
{
	uint8_t 	i;
	char*	temp;
	
	temp = AddrSource;														// Save address entrata
	BigLittleEndianSwap16(AddrSource);										// La "BigLittleEndianSwap16" vuole il puntatore al dato

	*out = 0;
	for(i=0; i<len; i++) {
		if((i%2) == 0) {
			*out = (*out * 10) + ((*AddrSource & 0xF0)>>4);
		} else {
			*out = (*out * 10) + (*AddrSource & 0x0F);
			AddrSource++;
		}
	}
	BigLittleEndianSwap16(temp);	
}
*/
/*-------------------------------------------------------------------------------------------*\
Method:	HexToASCII
	Converte un byte Hex in 2 caratteri ASCII

	IN:  - Hex8 con dato Hex da convertire, AsciiDest con address destinazione e 
		   BLEndian come flag per determinare il formato del risultato
	OUT: - 2 chr ASCII in ASCIIDest (ASCIIDest=ASCIIL e ASCIIDestH=ASCIIH se BLEndian = false)
\*-------------------------------------------------------------------------------------------*/
void  HexByteToAsciiInt(uint8_t Hex8, uint16_t* AsciiDest, uint8_t BLEndian) {

	uint16_t* pnt;
	uint8_t	temp;

	pnt = AsciiDest;
	// Converto Nibble High in temp
	temp = Hex8>>4;
	if (temp <= 9) {
		temp +='0';							// E' un numero da 0 a 9
	} else {
		temp += 0x37;						// E' una lettera da A a F
	}

	// Converto Nibble Low in Hex8
	Hex8 = Hex8 & 0x0f;
	if (Hex8 <= 9) {
		Hex8 +='0';							// E' un numero da 0 a 9
	} else {
		Hex8 += 0x37;						// E' una lettera da A a F
	}
	if (BLEndian) {
		*pnt = temp;						// High-Low
		*pnt |= Hex8<<8;
	} else {
		*pnt = temp<<8;						//  Low-High
		*pnt |= Hex8;
	}
}
	
/*--------------------------------------------------------------------------------------*\
Method:	AsciiToHex
	Converte i caratteri di una stringa ASCII in un Hex: termina la conversione quando
	trova "NULL" nella stringa ASCII oppure raggiunge il numero di caratteri Ascii da
	convertire.
	NON c'e' alcun controllo sul contenuto e la lunghezza della stringa ASCII.

	IN:  - pointer alla stringa ASCII e, opzionale, numero di caratteri da convertire
	OUT: - uint16_t
\*-------------------------------------------------------------------------------------------*/
uint16_t  AsciiToHex(char *AsciiSource, uint8_t numchar)
{
	char* strVal;
	uint8_t	ch;
	uint16_t	i, Hex = 0;

	strVal = AsciiSource;
	if (numchar == 0)
	{
		while(ch = *strVal++)
		{
			Hex *= 16;
			Hex |= CharConversion(ch);
		}
	}
	else
	{
		for (i=0; i < numchar; i++)
		{
			ch = *(strVal+i);
			Hex *= 16;
			Hex |= CharConversion(ch);
		}
	}
	return Hex;
}

/*--------------------------------------------------------------------------------------*\
Method:	CharConversion
	Converte il carattere Ascii passato come parametro in un byte Hex.
	Se non e' un char Ascii esce con zero.

	IN:  - char Ascii da convertire
	OUT: - uint16_t
\*---------------------------------------------------------------------------------------*/
uint16_t  CharConversion(char ch)
{
	uint16_t  HexResult = 0;

	if (ch >='0' && ch<='9'){
		HexResult |= ch-'0';
	} else {
		if (ch >='A' && ch<='F'){
			HexResult |= ch-'A'+10;
		} else {
			if (ch >='a' && ch<='f'){
				HexResult |= ch-'a'+10;
			}
		}
	}
	return HexResult;
}

/*-------------------------------------------------------------------------------------------*\
Method:	HexWordToDec
	Converte una word Hex in 3 bytes Dec.

	IN:  - Hex16 con il dato da convertire
	OUT: - 3 bytes Dec nel buffer di destinazione
\*-------------------------------------------------------------------------------------------*/
void  HexWordToDec(uint16_t HexWord) {

	uint16_t	Divisor_1 = 10000, Divisor_2 = 100;

	DecHM = HexWord / Divisor_1;					// Byte Decine di Migliaia
	HexWord -= (DecHM * Divisor_1);
	DecH = HexWord / Divisor_2;						// Byte Migliaia e Centinaia
	HexWord -= (DecH * Divisor_2);
	HexToDec(DecH, &DecH);
	HexToDec(HexWord, &DecL);						// Byte Decine e Unita'
}

/*-------------------------------------------------------------------------------------------*\
Method:	HexToDec
	Converte un byte Hex in un byte Dec.
	****  ATTENZIONE : MASSIMO VALORE DI ENTRATA 0x63 CHE EQUIVALE A 99 DECIMALE *****

	IN:  - Address byte Hex
	OUT: - valore decimale nel byte di destinazione
\*-------------------------------------------------------------------------------------------*/
void  HexToDec(uint8_t HexByte, uint8_t* DecDest) {

	uint8_t*  DestPnt;
	
	DestPnt = DecDest;
	*DestPnt = (HexByte/10) << 4;					// Decine
	*DestPnt += HexByte%10;							// Unita'
}	
	
/*-------------------------------------------------------------------------------------------*\
Method:	Check_Hour_Min
	Controlla se il valore di ore e minuti e' un valore lecito compreso tra 00:00 e 23:59.
	In caso negativo esce con zero.
	
	IN:  - 
	OUT: - 
\*-------------------------------------------------------------------------------------------*/
uint16_t Check_Hour_Min(uint16_t val) {
	
	HexWordToDec(val);
	if (DecH > 23) return 0;
	if (DecL > 59) return 0;
	return val;
}

/*--------------------------------------------------------------------------------------*\
Method:	uint32toa0
	Convert a 32 bits unsigned integer to a string with the specified number of digits
	filling with 0s
	
Parameters:
	strVal	<-	converted value (+ string terminator)
	uintVal	->	value to be converted
	nDigits	->	number of digits
\*--------------------------------------------------------------------------------------*/
void uint32toa0(char* strVal, uint32_t val, uint8_t nDigits) {
	
	strVal+= nDigits;
	*strVal= '\0';
	
	while (nDigits--) {
		*--strVal= val%10+'0';
		val/= 10;
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	uint32toa
	Convert a 32 bits unsigned integer to a string with the specified number of digits
Parameters:
	strVal	<-	converted value (+ string terminator)
	uintVal	->	value to be converted
	nDigits	->	number of digits
\*--------------------------------------------------------------------------------------*/
void uint32toa(char* strVal, uint32_t val, uint8_t nDigits) {
	
	char *ptSrc = strVal;
	strVal+= nDigits;
	*strVal= '\0';
	
	while (nDigits--) {
		*--strVal= val%10+'0';
		val/= 10;
		if(!val) break;
	}
	while (*strVal) {
		*ptSrc++ = *strVal++;
	}
	*ptSrc = nil;
}

/*--------------------------------------------------------------------------------------*\
Method:	BinToBcd
	Trascodifica da Bin a Bcd

Parameters:
	
Returns:
	
\*--------------------------------------------------------------------------------------*/
uint8_t BinToBcd(uint8_t bin)
{
	return(((bin / 10) * 16) + (bin % 10));
}


/*--------------------------------------------------------------------------------------*\
Method:	BcdToBin
	Trascodifica da Bcd a Bin

Parameters:
	
Returns:
	
\*--------------------------------------------------------------------------------------*/
uint8_t BcdToBin(uint8_t bcd)
{
	return(((bcd / 16) * 10) + (bcd % 16));
}

/*-------------------------------------------------------------------------------------------*\
Method:	ZeroToSpaces
	Sostituisce il chr ASCII_SPACE agli zeri non significativi di un buffer ASCII.
	Si considera il primo byte del buffer come MSB.
	Es Buff[0] = '3'
	Es Buff[1] = '0'
	Es Buff[2] = '1'
	Es Buff[3] = '2'
	Es Buff[4] = '5'
	Il chr "3" e' il MSB 

	IN:  - pointer al buffer ASCII e sua lunghezza
	OUT: - 
\*-------------------------------------------------------------------------------------------*/
void  ZeroToSpaces(char *Buff, uint16_t BuffLen) {
	
	uint16_t	j;
	
	for (j=0; j<BuffLen; j++) {
		if (*(Buff+j) != '0') return;
		*(Buff+j) = ' ';
	}
	*(Buff+(BuffLen-1)) = '0';																	// Arrivo da se non ci sono cifre significative quindi esco con LSB = 0
}

#if (IOT_PRESENCE == true)
/*--------------------------------------------------------------------------------------*\
Method:	uint32ToAscii
	Convert a 32 bits unsigned integer to a string ASCII of 8 bytes
	
	IN:	  - dato e pointer al buffer
	OUT:  - buffer contenente 8 chr ASCII del dato uint32
\*--------------------------------------------------------------------------------------*/
char* uint32ToAscii(uint32_t Hex32, char* bufpnt)
{
	uint16_t	asciiVal;

	HexByteToAsciiInt((uint8_t)(Hex32 >> 24), &asciiVal, NULL);
	*(bufpnt++) = asciiVal>>8;
	*(bufpnt++) = asciiVal;
	HexByteToAsciiInt((uint8_t)(Hex32 >> 16), &asciiVal, NULL);
	*(bufpnt++) = asciiVal>>8;
	*(bufpnt++) = asciiVal;
	HexByteToAsciiInt((uint8_t)(Hex32 >> 8), &asciiVal, NULL);
	*(bufpnt++) = asciiVal>>8;
	*(bufpnt++) = asciiVal;
	HexByteToAsciiInt((uint8_t)(Hex32 >> 0), &asciiVal, NULL);
	*(bufpnt++) = asciiVal>>8;
	*(bufpnt++) = asciiVal;
	return bufpnt;
}
#endif









