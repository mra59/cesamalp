/* ***************************************************************************************
File:
    VMC_CPU_TastiSelezione.c

Description:
    File con le funzioni per l'elaborazione della richiesta di selezione, effettuata
	sia con touch che con tastiera numerica.

History:
    Date       Aut  Note
    Ott 2016 	MR  

 *****************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "ORION.H"
#include "VMC_CPU_TastiSelezione.h"
#include "VMC_CPU_Prog.h"
#include "VMC_CPU_Tasti.h"
#include "VMC_CPU_HW.h"
#include "VMC_CPU_LCD.h"
#include "VMC_CPU_Motori.h"
#include "VMC_GestSelez.h"

#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	const uint8_t 	SelezAvailable[];
extern	bool  			Is_VMC_Fail(void);

#if (IOT_PRESENCE == true)
	void  	SetAuditVendInGratuito(uint32_t SelVal);
	void  	SetDeconto(void);
	void  	Esegui_Selezione(void);
	void  	UpDateDeconto(uint8_t NumSelez);
	void  	Buz_start(uint32_t time, uint32_t tono1, uint32_t tono2);
#endif
	
	
/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------*\
Method: IsCheckSelezParam_OK
 
	IN:	 - 
	OUT: - true se selezione disponibile, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool  IsCheckSelezParam_OK(uint8_t NumSel)
{
#if DEB_NO_VMC
	return true;
#else
	if ((sSelParam_A[NumSel].SelFlags & BIT_INH_SEL) == BIT_INH_SEL)
	{
	 	return false;
	}
	if (IsProdottoEsaurito(NumSel) == true)
	{
		return false;
	}
	return true;																				// Selezione disponibile
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: GestTastiSelez
	Elabora il tasto premuto per effettuare la selezione.
	Se nessuna selezione in corso si attiva la richiesta della selezione richiesta.
	Se la selezione richiesa e' gia' attiva si aggiunge Time/Pulses.
	Se c'e' una selezione in corso e se ne richiede un'altra, si cambia da una selezione 
	all'altra calcolando la percentuale residua del tempo o degli impulsi per applicarli
	alla nuova selezione richiesta. 

	IN:	 - numero del tasto premuto (1-n) = selezione richiesta
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void  GestTastiSelez(uint16_t tasto)
{
	Buz_start(Milli100, HZ_2000, 0);															// Segnalo tasto premuto
	if (tasto != 0)
	{
		if (tasto > MAXNUMSEL)
		{
			NumVMCSelez = 0;
			LCD_Disp_NOSEL();																	// Visualizzo non disponibile
		}
		// Non eseguo selezioni se c'e' un guasto o VMC non inizializzato
		if (/*(gVMCState.SelezReq == true) ||*/ (Is_VMC_Fail() == true) 
				|| (Imene == false) || fVMC_NoLink || fVMC_Inibito)
		{
			Buz_start(OneSec*2, HZ_2000, 0);													// Segnale buzzer tasto rifiutato
			return;
		}
		NumVMCSelez = tasto;

		if (IsCheckSelezParam_OK(NumVMCSelez-1) == true)
		{
			gVMCState.SelezReq = true;															// Richiesta selezione
		}
		else
		{
			NumVMCSelez = 0;
			LCD_Disp_NOSEL();																	// Visualizzo non disponibile
		}
	}
}

#if (IOT_PRESENCE == true)


/*-------------------------------------------------------------------------*\
Method:	IsSelParameterCorrect
	Abilita l'erogazione richiesta da remoto se il parametro e' nei range.
	
 	IN :  - Numero erogazione richiesta (1-n) e parametro
 	OUT:  -  
\*-------------------------------------------------------------------------*/
bool  IsSelParameterCorrect(uint8_t NumErogaz, uint32_t Param)
{
	if ((sSelParam_A[NumErogaz-1].SelFlags & BIT_IMPULSI) == false)
	{
		if ((Param >= MIN_TIME_SEL) && (Param <= MAX_TIME_SEL)) return true;
	}
	else
	{
		if ((Param >= MIN_FLOW_SEL) && (Param <= MAX_FLOW_SEL)) return true;
	}
	return false;
}

/*-------------------------------------------------------------------------*\
Method:	StartErogazioneDaRemoto
	Predispone per eseguire l'erogazione richiesta da remoto.
	Si utilizzano le stesse funzioni delle normali erogazioni per poter
	registrare correttamente l'Audit e le eventuali funzioni collaterali
	alla erogazione (display LCD, etc).
	
 	IN :  - Numero erogazione richiesta (1-n)
 	OUT:  -  
\*-------------------------------------------------------------------------*/
void  StartErogazioneDaRemoto(uint8_t NumErogaz, uint16_t ErogParam)
{
	uint16_t	Backup_FlowCounterNominale;
	uint32_t	Backup_TimeErogazione;
	
	NumVMCSelez = NumErogaz;																	// Numero selezione richiesta
	sSelParam_A[NumErogaz-1].SelFlags &= (~BIT_INH_SEL);										// Enable Erogazione
	sSelParam_A[NumErogaz-1].EV_CC_Failure = false;												// Clr EV guasta
	if ((sSelParam_A[NumErogaz-1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
	{
		Backup_FlowCounterNominale = sSelParam_A[NumErogaz-1].FlowCounterNominale;				// Save programmed parameter
		sSelParam_A[NumErogaz-1].FlowCounterNominale = ErogParam;								// Counter flow-meter
		sSelParam_A[NumErogaz-1].FlowCounterPercentuale = 0;
	}
	else
	{
		Backup_TimeErogazione = sSelParam_A[NumErogaz-1].TimeErogazione;						// Save programmed parameter
		sSelParam_A[NumErogaz-1].TimeErogazione = (ErogParam * OneSec);									// Time Erogazione
		sSelParam_A[NumErogaz-1].TimeErogazionePercentuale = 0;
	}
	gVmcStatusIoT.RemoteSelezNum = NumErogaz;													// Save numero erogazione richiesta
	gVmcStatusIoT.RemoteSelectionReq = true;													// Selezione richiesta da remoto

	VMC_ValoreSelezione = MRPPrices.Prices99.MRPPricesList99[NumVMCSelez-1];					// Determino valore selezione solo per l'audit vendite gratuite di test
	SetAuditVendInGratuito(VMC_ValoreSelezione);
	gVMCState.InSelezione = true;																// Selezione da remoto in corso
	Norefresh = true;																			// Non aggiornare display LCD fino all'elaborazione di fine selezione
	gVMCState.Fase = WaitEndVend;
	SetTouchNewViscredit();																		// Per riattivare visualizzazione credito dopo la selezione
	SetDeconto();																				// Predispongo il deconto se funzione attivata
	gVMCState.SelezReq = true;																	// Richiesta selezione da remoto
	Esegui_Selezione();
	UpDateDeconto(NumVMCSelez);																	// Call UpDateDeconto prima di azzerare NumVMCSelez per aggiornare contatori usura filtri
	NumVMCSelez = 0;						
	gVMCState.SelezReq = false;						
	if ((sSelParam_A[NumErogaz-1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
	{
		sSelParam_A[NumErogaz-1].FlowCounterNominale = Backup_FlowCounterNominale;				// Restore programmed parameter
	}
	else
	{
		sSelParam_A[NumErogaz-1].TimeErogazione = Backup_TimeErogazione;						// Restore programmed parameter
	}
	if (gVMCState.EsitoFineVend == VEND_FAIL)						
	{																							// FAIL Vend
		StatusVendita = VEND_FAILURE;						
		LCD_Disp_VendFail();						
		Touch_Msg_VendFail();																	// Msg Fail Vend su Touch
		Norefresh = true;						
		Buz_start(TIMEBUZ_VEND_FAIL, HZ_2000, 0);												// Buzzer lungo Segnala vendita FAIL
	}						
	else						
	{						
		StatusVendita = VEND_SUCCESS;						
		Norefresh = false;																		// Riabilita refresh display LCD
	}						
	NumVMCSelez = 0;																			// Riabilito DA per nuova selezione
}
#endif
