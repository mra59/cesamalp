/****************************************************************************************
File:
    Sel24.c

Description:
    Funzioni Selettore Parallelo.

History:
    Date       Aut  Note
    Dic 2013	Abe

 *****************************************************************************************/

#include "ORION.H"
#include "Sel24.h"
#include "IICEep.h"
#include "I2C_EEPROM.h"
#include "ICPglobal.h"
#include "ICPChg.h"


/*--------------------------------------------------------------------------------------*\
Global variables and types definition	
\*--------------------------------------------------------------------------------------*/

uint8_t 	n_coin;
uint8_t 	valid_coin[10];
uint8_t 	n_valid_coin = 0;
uint16_t 	sel_warmup = WARMUP_TIME;

static	uint16_t	ValCoins[16];									//Valore monete in RAM

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

//MR19 extern 	bool		CoinValueChanged;			// Non usata da nessuno
extern 	uint16_t	MaxCash;
extern	void  		EnaDisCoinValidator(uint8_t enable);
extern	void 		CoinAuditStart(ET_CHGPOLLCOINDEST coinDest, uint8_t coinType, CreditCoinValue coinValue);
extern	void 		CreditMasterCashAdd(CreditValue amount, CreditCashTypes cashType);
extern	void  		ClearSelezPrenotata(void);

extern 	CreditCoinValue 	CoinValuesList[];

/*---------------------------------------------------------------------------------------------*\
Method: Sel24_init
	Inizializzazione variabili selettore 24V.
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/

void Sel24_init(void)
{
	CreditCoinValue valmon;
	uint8_t i;

	num_icp_coin = 0;
	for (i=0;i<ICP_MAX_COINTYPES;i++) {
		ReadEEPI2C(IICEEPConfigAddr(EEMonete[i]), (byte *)(&valmon), sizeof(valmon));
		ValCoins[i] = valmon;
		if (valmon > 0 && valmon != 0xffff) {
			num_icp_coin++;
		}
		if (CoinValuesList[i] != valmon) {
			CoinValuesList[i] = valmon;
			//MR19 CoinValueChanged = true;				// Non usata da nessuno
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Sel24_read_coin
	Campionamento ingressi monete.
	Chiamata dall'IRQ ogni 1 msec.
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/

void Sel24_read_coin(void){
	
	uint32_t porta;
	uint32_t mask = (COIN_A|COIN_B|COIN_C|COIN_D|COIN_E|COIN_F);
	uint8_t coin_ix;
	
	if (!(PAR_Inp & INPDATAVALID)) return;														// Non e' stata effettuata lettura 589 in PAR_Inp
	porta = (uint32_t)PAR_Inp;																	// La lettura e' stata effettuata in IRQ da 1 msec
	
	if ((porta & mask) != mask)
	{
    	if (Inp_Active == true) return;															// se ho gi� validato la moneta, attendo canali moneta a riposo
    	porta = (porta & mask);
    	mask = 1;
    	for (coin_ix = 0; coin_ix < 6; coin_ix++)
		{
    		if (!(porta & mask))
			{
    			if (porta & (~mask) & ((COIN_A|COIN_B|COIN_C|COIN_D|COIN_E|COIN_F)))
				{
    				if (Touts.CoinTime == 0)
					{
    					Touts.CoinTime = COIN_DELAY;
    					n_coin = coin_ix;
    					break;
    				} 
					else 
					{
    					// Tolta il 25.2.15 perche' c'e' gia' all'entrata **** if (Inp_Active == true) return;	// se ho gi� validato la moneta, attendo canali moneta a riposo
    					if (n_coin == coin_ix)													// stesso canale moneta?
						{
						 	Touts.CoinTime--;
    						if (Touts.CoinTime == 0)											// timer di lettura scaduto?
							{
							  	valid_coin[n_valid_coin] = n_coin | COIN_FLAG;					// moneta valida!
    							n_valid_coin++;
    							if (n_valid_coin >= 9)
								{
    								gOrionCashVars.INH_DaFullCoinBuff = true;					// se ho quasi finito il buffer monete, inibisco il selettore
    							}
    							Inp_Active = true;												// valid coin detected
    						}
    				 	}
    					break;
    			 	}
    			}
    		}
    		else
			{
    			mask = mask<<1;
    		}
    	}
    }
	else
	{																					// Input tutti a riposo (high)
    	if (Touts.CoinTime == 0)
		{
        	Inp_Active = false;
    	}
		else 
		{
    		if (Touts.CoinTime > 1)
			{
    			Touts.CoinTime -= 2;															// Decremento counter integrazione con velocita' doppia
    		}
			else 
			{
    			Touts.CoinTime = 0;
				Inp_Active = false;
    		}
    	}
    }
}

/*---------------------------------------------------------------------------------------------*\
Method: Sel24_task
	Funzione chiamata continuamente dal MAIN.
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/

void Sel24_task(void) {
	
	uint16_t 			valmon, CashMax = MaxCash;
	ET_CHGPOLLCOINDEST 	coinDest = ICP_CHG_CASHBOX;

	if ( num_icp_coin == 0 || (gOrionKeyVars.State != kOrionCPCStates_Enabled 
			&& gOrionKeyVars.State != kOrionCPCStates_Idle
			&& gOrionKeyVars.State != kOrionCPCStates_Revalue 
			&& gOrionKeyVars.State != kOrionCPCStates_RevalueWr)) {
		EnaDisCoinValidator(Disable);															// inhibit Coin acceptor
	} else {
		if (gOrionCashVars.Sel24_INH || fVMC_Inibito || fVMC_NoLink || sCMDICPCash.chgReady || sel_warmup || gVars.AudExtFull) {
			EnaDisCoinValidator(Disable);														// inhibit Coin acceptor
		} else {
			if (gOrionKeyVars.Inserted) CashMax = MaxRevalue;   								// MaxRevalue se la carta e' inserita
			if (CashMax > gOrionCashVars.creditCash) {											// 11.12.14: modificato da ">=" a ">" altrimenti il selettore era abilitato anche
				EnaDisCoinValidator(Enable);													// quando entrava una carta con credito > MaxCred
			} else {
				EnaDisCoinValidator(Disable);
			}
		}
	}
	if (n_valid_coin) {
		do {
			if (valid_coin[n_valid_coin-1] & COIN_FLAG) {
				valid_coin[n_valid_coin-1] &= ~COIN_FLAG;
				// 08.08.17 Commentato  -  ReadEEPI2C(IICEEPConfigAddr(EEMonete[valid_coin[n_valid_coin-1]]), (byte *)(&valmon), sizeof(valmon));
				valmon = ValCoins[valid_coin[n_valid_coin-1]];
				CoinAuditStart(coinDest, valid_coin[n_valid_coin-1], valmon);
			
				valid_coin[n_valid_coin-1] = 0;
				n_valid_coin--;
				gOrionCashVars.Sel24_INH = FALSE;
				CreditMasterCashAdd(valmon, kCreditCashTypeCoin);
				ClearSelezPrenotata();
			}
		} while (n_valid_coin > 0);
	}
}
