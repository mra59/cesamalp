/****************************************************************************************
File:
    VMC_CPU_TastiSelezione.h

Description:
    Include file con definizioni e strutture per la gestione dei tasti di selezione.

History:
    Date       Aut  Note
    Ott 2016 	MR  

 *****************************************************************************************/

#ifndef __VMC_CPU_TastiSelezione_H_
#define __VMC_CPU_TastiSelezione_H_

void  GestTastiSelez(uint16_t tasto);
bool  IsCheckSelezParam_OK(uint8_t NumSel);



#if (IOT_PRESENCE == true)
	void  StartErogazioneDaRemoto(uint8_t NumErogaz, uint16_t FlowMeterCount);
	bool  IsSelParameterCorrect(uint8_t NumErogaz, uint32_t Param);
#endif

#endif	//  __VMC_CPU_TastiSelezione_H_
