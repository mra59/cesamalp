/****************************************************************************************
File:
    VMC_Main.c

Description:
    MAIN della CPU VMC
    

History:
    Date       Aut  Note
    Mag 2019 	MR   

*****************************************************************************************/

#include "main.h"
#include "VMC_Main.h"
#include "OrionTimers.h"
#include "VMC_CPU_LCD.h"
#include "ExecSlave.h"
#include "OrionCredit.h"
#include "Gestore_Exec_MSTSLV.h"
#include "VMC_CPU_HW.h"
#include "ExecMaster.h"
#include "Gestore_Exec_MSTSLV.h"
#include "VMC_Selez_STD.h"
#include "VMC_GestSelez.h"
#include "VMC_CPU_Motori.h"
#include "Monitor.h"
#include "phcsBflStatus.h"
#include "RTC_utility.h"
#include "Funzioni.h"
#include "IoT_Comm.h"

#include "usb_host.h"
#include "usb_device.h"
#include "gpio.h"
#include "explorer.h"        
#include "AuditOverpay.h"

#include "Dummy.h"

#define	DEB_KEY_PWS_LATER				0			// Attiva l'alimentazione della chiave USB dopo averla rilevata sulle linee bilanciate


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void  		Gestione_MDB(void);
extern	void  		Check_Clear_Audit_LR(void);
extern	void  		GestioneRevalue(void);
extern	void  		GestioneAuditVendita(void);
extern	void  		GestioneAuditOverpay(void);
extern	void  		GestioneAuditFreeCredit(void);
extern	void  		GestioneAuditCash(void);
extern	void  		GestioneAuditManualFill(void);
extern	void  		GestioneAuditCoinPayout(void);
extern	void		DDCMPTask(void);
extern	void 		ICPProt_Init(void);
extern	void 		ICPBillHookTaskInit(void);
extern	void  		ICPChgHookTaskInit(void);
extern	void 		SetCHGInfoExChange(void);
extern	bool		ConnessioneDDCMP_IrDA(void);
extern	bool  		MifareReaderRequest(void);
extern	bool 		MifareReaderAnticoll(uint8_t *ptSnum);
extern	uint16_t	ReadMifare(void);
extern	uint16_t 	MF_DisableDrivers(void);
extern	void  		ClrCardFlags(void);
extern	bool 		CompareBuffer (uint8_t *ptBuf1, uint8_t *ptBuf2, uint8_t bufLen);
extern	bool		WrCredit(void);
extern	void 		Azzera_Lamp (void);
extern	void 		lamp_led_verde(void);
extern	void 		lamp_led_rosso(void);
extern	void  		Gestione_Touch_Tastiera(void);
extern	void 		Sel24_task(void);
extern	void 		CashlessTaskInit(void);
extern	void  		CashTaskInit(void);
extern	void  		Esegui_Selezione(void);
extern	void		Accendi_LedGiallo(void);
extern	bool 		MSC_File_Operations(void);
extern	void  		CheckCreditForSelection(void);


extern	LDD_RTC_TTime 		DataOra;
extern  ApplicationTypeDef 	Appli_state;			// USB_Host
extern	bool		NewCPU;


extern	phcsBfl_Status_t 	PN512_set_led (uint8_t pin, uint8_t mask);
extern	phcsBfl_Status_t 	PN512_mif_init(uint32_t speed, uint8_t mask);
extern	void 		HAL_NVIC_SystemReset(void);
extern	void 		VMC_USB_ErrKeyRilev(void);


#if DEB_VISTIME
	extern	bool  Is_First_Puls_Selez_Premuto(void);
#endif

	
	
extern	void Restart_Da_Prog(void);
extern	void  SaveBackupData (void);
	

/*--------------------------------------------------------------------------------------*\
Global variables and types definition	
\*--------------------------------------------------------------------------------------*/

MAINTimes 	MainTouts;																		// Struttura con i timeout del Main
Timers 		Touts;
//Piezo 		Buz;
USB_STATE_ENUM USB_state = USB_MODE_NULL;

uint8_t		Snum_media_in[UID_MEDIA_BUF_LEN+1], Snum_media_chk[UID_MEDIA_BUF_LEN+1];
uint8_t 	msc_state_machine = 2;

#if DEB_VISTIME
	uint32_t TimeClockCnt;
#endif

#if DEB_RTC_SPEED
#define	RTCBUFFLEN		16
#define	INTERVALLO		FIVESEC
	uint32_t	RTC_sec[RTCBUFFLEN];
	uint32_t	kernel_sec[RTCBUFFLEN];
	uint8_t		rtc_in;
#endif

static	bool	Restart_LCD;

//	==============================================================================
//	==================    M A I N      C P U    VMC              =================
//	==============================================================================

void MAIN_CPU(void)
{
	bool			WriteResult;
	uint8_t	  		LedWhiteCnt;
	static	bool	FreeRunningNearRollOver;
	
#if DEB_OUTPUTS
	uint8_t	  		j, mask;
#endif	
	
// ========================================================================
// ========    I N I T      V A R I E             =========================
// ========================================================================
	
	MainTouts.ToutRTC.tmrEnd = 0;																// Controlla al primo main l'ora legale/solare e fasce orarie
	FreeRunningNearRollOver = false;
	
	Buz_start(OneSec, HZ_2000, 0);						                						// Test del Buzzer
	TmrStart(MainTouts.LCDRefreshTime, REFRESH_LCD_FREQ);										// Init Frequenza refresh display LCD
	
	//	Controllo se attivare la funzione Monitor
	if (TestMonitor() == true)																	// Attivato Log protocolli
	{
   		GestioneUSB_Host_Device();						        								// Controllo se CPU collegata a Host CDC (Terminal)
        if (USB_state == USB_MODE_DEVICE)
		{
			TmrStart(MainTouts.MainTime, OneSec);												// Attesa Init USB CDC per dare tempo al terminal di inviare "Enter" e attivare il Monitor
			while (TmrTimeout(MainTouts.MainTime) == false);
			SysMonitor.LogMDBPowerUp = true;
			SystemMonitor();																	// Doppia chiamata a SystemMonitor per inizializzare il Monitor e trasmettere
			SystemMonitor();																	// dati log protocollo MDB
		}
	}

	Set_fVMC_Inibito;																			// Parto con VMC Inhibit
	TmrStart(MainTouts.MainTime, Milli100);														// Init Frequenza scansione Mifare Locale
        
	//Clear_fVMC_Inibito;				//DEBUG
	//OpMode = EXEC_Slave;			//DEBUG

	// ----------------------------------------
	// ---     INIT   Mifare  Locale       ----
	// ----------------------------------------

	if (gVMC_ConfVars.PresMifare == true) {
		CashlessTaskInit();
		CashTaskInit();
		gOrionKeyVars.State = kOrionCPCStates_Enabled;
	}

	// ----------------------------------------
	// ---     INIT      EXECUTIVE         ----
	// ----------------------------------------
	if (OpMode == EXEC_Slave)
	{																							// Executive Slave: inizializzo protocollo
		ExecSlaveInit();
		if (gVMC_ConfVars.ExecMSTSLV == true) {
			GestMS_Init();																		// Init Gestore MST/SLV
			ExecMasterInit();																	// Executive Master: inizializzo protocollo
		}
	}
	
	// ----------------------------------------
	// ---     INIT      MDB               ----
	// ----------------------------------------
	if (gVMC_ConfVars.ProtMDB == true) {
		ICPProt_Init();																			// Init MDB
		#if kICPMasterSupport
			ICPBillHookTaskInit();
			ICPChgHookTaskInit();
			SetCHGInfoExChange();																// Default exact change --> aggiornata se presente rendiresto e equazione vera
			//sExecVar.VariazExCh = true;
			SetAlreadyExChange();
		#endif
	}

	// ---------------------------------------------
	// --- Attesa Fine Tempo visualizzazione FW ----
	// ---------------------------------------------
	gVMCState.TastieraDisable = true;															// Tastiera e tasti disabilitati, funziona solo tasto PROG
	do {
		Gestione_LCD();																			// Visualizza revisioni Fw su LCD
		Touch_Gest_Visualizzazioni();															// Visualizza revisioni Fw su Touch
		WDT_Clear(gVars.devWDT);
	} while (TmrTimeout(MainTouts.TimeInitMsg) == false);

	// ----------------------------------------
	// ---     Test Guasti Bloccanti       ----
	// ----------------------------------------
        /*
	if ((TestGuastiBloccanti() == true) || fVMC_NoAnswer) {
		ProgKeyToClearFails();																	// Si esce se il guasto e' ripristinabile o si resetta dopo l'entrata in programmazione
	}
	*/
	gVMCState.InitAttuatori = true;
	if (Is_VMC_Fail() == true)
	{
		ProgKeyToClearFails();
	}
	else 
	{
		gVMCState.TastieraDisable = false;													// Riabilito tastiera
	}
	LCD_SetNormalVisual();																	// Abilita messaggi vari su LCD e Touch

	
#if DEB_OUTPUTS
	// ----------------------------------------
	// ---     Test OUTPUTS                ----
	// ----------------------------------------
	Out1_8 = 0;
	Out9_16 = 0;
	for (j=0, mask=1; j<8; j++, (mask = (mask << 1)))
	{
		Out1_8 |= mask;
		TmrStart(MainTouts.MainTime, Milli200);
		while (TmrTimeout(MainTouts.MainTime) == false) {};
		Out1_8 = 0;
	}
	for (j=0, mask=1; j<8; j++, (mask = (mask << 1)))
	{
		Out9_16 |= mask;
		TmrStart(MainTouts.MainTime, Milli200);
		while (TmrTimeout(MainTouts.MainTime) == false) {};
		Out9_16 = 0;
	}
#endif
	
	// ========================================================================
	// ========    L O O P     M A I N                =========================
	// ========================================================================
	while(true)
	{
	  	WDT_Clear(gVars.devWDT);
		if ((gVMC_ConfVars.PresMifare == false) && (gVMC_ConfVars.Gratuito == false)) {			// E' Mifare non programmato se anche Gratuito e' false, altrimenti potrebbe essere 
	    	MIFARE_OFF();																		// stato disattivato temporaneamente Mifare per fare la selezione gratuita e spegnendo
		}																						// l'antenna si ha un errore bloccante da antenna quando finisce la selezione  
		if (gVMC_ConfVars.PresMifare == true)

		{
			// ========================================================================
			// ========    CASHLESS   INTERNO                 =========================
			// ========================================================================
			if (gOrionKeyVars.AntennaRespFail == true)
			{
				if (gVMCState.InProgrammaz == false)													// In entrata in programmaz. non devo attivare +5V Mifare altrimenti, se esco subito, il uP e' in Boot
				{
					// Antenna non risponde
					if (Touts.tmRF == 0)																// Trascorsa l'attesa tento riattivazione Antenna
					{
						RedLedCPU(OFF);																	// Led Rosso CPU OFF
						MIFARE_ON();
						gOrionKeyVars.PN512_RapidSearch = true;											// Attesa breve risposta dal PN512
						response = PN512_mif_init (MIFARE_BAUD_FAST, DDRPN);  							// Reinizializzo Mifare Reader
						gOrionKeyVars.PN512_RapidSearch = false;
						if (response == Resp_OK)
						{
							gOrionKeyVars.AntennaRespFail = false;
							gVars.ReaderInhibit 		  = true;										// Per riattivare Led Bianco
						} 
						else 
						{
							Touts.tmRF = OneSec;														// Carico Timeout per tentare riattivazione antenna
							MIFARE_OFF();																// !!!! Spegnere il PN512 perche' nella ricerca rapida successiva presume sia gia' OFF
							RedLedCPU(ON);																// Led Rosso CPU ON
						}
					}
				}
			} 
			else 
			{
				if (gOrionKeyVars.Inserted == false)
				{
					if (ConnessioneDDCMP_IrDA())
					{
						GreenLedCPU(OFF);															    // Led Verde CPU OFF: cashless Inhibit
						goto FineMainMifare;														    // Sono in DDCMP: salto gestione Mifare
					}
				}
				// *******  Determino flag ReaderInhibit ******
#if (IOT_PRESENCE == true)
				if ((fVMC_NoLink == false) 			&& (fVMC_Inibito == false)	&&
					((Pin1 != 0) || (Pin2 != 0)) 	&& (!gVars.AudExtFull)	 	&&
					(gOrionKeyVars.State >= kOrionCPCStates_Disabled) 			&& 
					(gOrionConfVars.Remote_INH == false))
#else
				if ((fVMC_NoLink == false) && (fVMC_Inibito == false) && (gOrionKeyVars.State != kOrionCPCStates_Inactive)
					&& (gOrionKeyVars.State != kOrionCPCStates_Disabled) && (Pin1 != 0 || Pin2 != 0)
					&& (gVars.AudExtFull == false) && (gVMCState.InProgrammaz == false))
#endif				
				{
					// ***********   Reader Enabled  ***************
					// GreenLedCPU(ON);									// 23.09.2021 Spostato a "FineMainMifare:" 					// CPU Enable, Led Verde CPU ON
					if (gVars.ReaderInhibit == true)
					{
						gVars.ReaderInhibit = false;
						Azzera_Lamp();																    // Sincronizzo Led verde antenna con verde CPU
					}
				}
				else
				{
					// ReaderInhibit da Nolink o VMC-Inh o gOrionKeyVars.State
					// o (Pin1+Pin2=zero) o Audit Estesa Full
					
					// GreenLedCPU(OFF);								// 23.09.2021 Spostato a "FineMainMifare:" 					// Led Verde CPU OFF: CPU Disable
					//MR19  saltare se DDCMP abilitato !?!?! PN512_set_led (led_bianco, !led_bianco);								// Led Bianco su Antenna OFF
					gVars.ReaderInhibit = true;
					LedWhiteCnt = 0;
				}	

				// ========================================================================
				// ========    GESTIONE    MIFARE                 =========================
				// ========================================================================
				if (gOrionKeyVars.CreditWrite == false)
				{
					if (TmrTimeout(MainTouts.MainTime))
					{
						TmrStart(MainTouts.MainTime, Milli100);									        // Ricarico Timeout Gestione carta
						if (gVMCState.IrDA_ENA == true)
						{
							//MR19 SetMifareComm();
						}
						lamp_led_verde();															    // Controllo se far lampeggiare Led Verde Antenna Mifare
						lamp_led_rosso();															    // Controllo se far lampeggiare Led Rosso Antenna Mifare
						if (gOrionKeyVars.Inserted && !fVMC_NoLink && !fVMC_Inibito && !gVars.AudExtFull)
						{
							PN512_set_led (led_bianco, led_bianco);									    // Led Bianco su Antenna ON
							LedWhiteCnt = 0;
						}
						else
						{
							if (!fVMC_NoLink && !fVMC_Inibito)
							{
								LedWhiteCnt++;
								if (LedWhiteCnt == 2)
								{
									PN512_set_led (led_bianco, !led_bianco);						    // Led Bianco su Antenna OFF
								}
								if (LedWhiteCnt == 10)
								{
									LedWhiteCnt = 0;
									PN512_set_led (led_bianco, led_bianco);							    // Led Bianco su Antenna ON
								}
							}
						}
						if (gVMCState.InProgrammaz == false)
						{
							if (MifareReaderRequest())
							{
								// ========================================================================
								// ========================      Mifare   nel    campo  ===================
								// ========================================================================
								if (gOrionKeyVars.RFFieldFree && !gOrionKeyVars.Inserted)
								{																		// Carta rifiutata all'inserimento (Inserted=0) e in attesa di estrazione
									if (IsBuzzerOFF())
									{
										Buz_start(Milli150, HZ_2000, HZ_500);							// Buzzer ON
									}
								}
								if (!gOrionKeyVars.RFFieldFree && !gOrionKeyVars.Inserted)
								{
									if (MifareReaderAnticoll(&Snum_media_in[0]))
									{
										ReadMifare();
										gOrionKeyVars.failvendcardcredit = 0;							// Azzero eventuale refund carta su display
										if (response == WaitElab) goto FineMainMifare;					// Non elaborare carta: attendo main successivi
										if (response == Resp_OK || response == OK_WaitEstraz)
										{
											gOrionKeyVars.Inserted = true;								// Card/key inserita e valida
											gOrionKeyVars.CardReadErr = false;
											if (response == OK_WaitEstraz)
											{
												gOrionKeyVars.RFFieldFree = true;						// Attendo estrazione
											}
										}
										else
										{
											// ****  Lettura fallita o carta rifiutata  ****
											if (response == ReaderInh)
											{
												gOrionKeyVars.RFFieldFree = true;
												gOrionKeyVars.FirstAccess = true;						// Accendo subito il led giallo
											}
											if (response == NoInfoSect)
											{															// Errore NoInfoSect indica che ho letto tutti i settori e non e' un
												gOrionKeyVars.FirstAccess = true;						// errore di lettura per cui ritentare, quindi termino qui
											}
											//TxCardErr(response);										// Controllo in Monitor se trasmettere al PC errore carta
											if (gOrionKeyVars.FirstAccess)
											{
												gOrionKeyVars.CardReadErr = true;
												Accendi_LedGiallo();
												Azzera_Lamp();
												gOrionKeyVars.RFFieldFree = true;						// Attendo estrazione
											}
											else
											{
												gOrionKeyVars.FirstAccess = true;
											}
										}
									}
								}
							}
							else
							{
								// ========================================================================
								// ============      Nessuna    Mifare   nel    campo   ===================
								// ========================================================================
								// Il test serve a saltare la disattivazione del campo RF se per una volta
								// la carta inserita e in uso non risponde correttamente al REQA
								if (gOrionKeyVars.Inserted == false) {	 
									response = MF_DisableDrivers();
									if (response != Resp_OK) {
										gVars.ReaderInhibit = true;										// Per riattivare Led Bianco se PN512 si fosse spento
										gOrionKeyVars.AntennaRespFail = true;
									} else {															// Abilito IrDA solo se l'antenna risponde, altrimenti non funziona il reset del PN512 e non ho disattivato il campo
										Start_IrDA();
									}
								}
								ClrCardFlags();															// Azzero tutte le flags carta (Inserted, RFFieldFree, etc)
								if (gOrionKeyVars.AntennaRespFail)
								{
									MIFARE_OFF();														// Spengo Antenna
									GreenLedCPU(OFF);													// Led Verde CPU OFF: cashless fuori servizio
									RedLedCPU(ON);														// Led Rosso CPU ON
									Touts.tmRF = OneSec;												// Carico Timeout per tentare riattivazione antenna
								}
							}

						}
					}
				}
				else
				{
					// ========================================================================
					// ========================      Scrittura   Mifare     ===================
					// ========================================================================
					PN512_set_led (led_verde+led_rosso, !led_verde|led_rosso);								// Led Antenna verde OFF - Led Rosso ON
					if ((gOrionKeyVars.RFFieldFree == false) && (gOrionKeyVars.CreditWriteEnd == false) &&
					   (gOrionKeyVars.CreditWriteCnt < kMaxCreditWriteCnt))
					{	
						gOrionKeyVars.CreditWriteCnt++;
						WriteResult = false;																// Flag risultato scrittura carta
						if (MifareReaderAnticoll(&Snum_media_chk[0]))
						{			
							if (CompareBuffer(Snum_media_chk, Snum_media_in, UID_MEDIA_BUF_LEN))
							{ 
								gOrionKeyVars.CreditWriteEnd = WrCredit();									// SCRITTURA CREDITO CARTA
								if (gOrionKeyVars.CreditWriteEnd)
								{
									WriteResult = true;
								}
							}
						}
						if (WriteResult == false)
						{																					// Scrittura fallita: clr flag inserted e set flag RFFieldFree
							gOrionKeyVars.RFFieldFree = true;
							gOrionKeyVars.Inserted = false;
						}
					}
					PN512_set_led ((led_verde+led_rosso), (!led_verde|!led_rosso));							// Led Antenna verde OFF - Led Rosso OFF
				} 	// Fine gestione Mifare
			} 	// Fine test AntennaRespFail
		} 		// Fine test carta esterna

	
FineMainMifare:

// ========================================================================
// ========             TASK    VARIE             =========================
// ========================================================================

		// ------------------------------------------------------------------------------
		// Lettura Temperature Sonde
		if (Touts.ToutScanTemper == 0)
		{
			Touts.ToutScanTemper = OneSec;
			Scan_Temperatures();																// Scansione Sonde Temperature
		}

		// ------------------------------------------------------------------------------
		// Gestione Inibizione/Abilitazione macchina

		if ((gVMCState.InProgrammaz == false) && (gVMCState.InitAttuatori == true))
		{
			// -----------------------------------------------------------
			// Controllo guasti
			if ((Is_VMC_Fail() == true) 		 || (gVMCState.InSelezione) 	 ||
				(gVMCState.USD_Ext_In_Selezione) || (gVMCState.USD_Loc_Disabled) ||
				IsFilerCleaningInProgress()
			   )
			{																					// C'e' almeno un guasto o in selezione o USD Disabilitato
				Set_fVMC_Inibito;
				RedLedCPU(ON);																	// Led Rosso CPU ON
				GreenLedCPU(OFF);																// CPU Enable, Led Verde CPU OFF
			}
			else
			{
				Clear_fVMC_Inibito;																// VMC OK
				RedLedCPU(OFF);																	// Led Rosso CPU OFF
				GreenLedCPU(ON);																// CPU Enable, Led Verde CPU ON
			}
		}
		
		// ---------------------------------------------------------------
		// Gestione Credito da Mifare Locale e da Cashless MDB 
		if (gVMC_ConfVars.ProtMDBSLV == false)
		{
			CashlessTask();																		// E' la OrionCredit
		}
		
		// ---------------------------------------------------------------
		// Gestione Credito Cash 
		if (gVMC_ConfVars.ProtMDBSLV == false)
		{
			CashTask();																			// E' nella OrionCredit
		}
		
		// ---------------------------------------------------------------
		// Gestione Cashless MDB 
		if (gVMC_ConfVars.ProtMDB == true)
		{
			Gestione_MDB();
		}
		
		// ---------------------------------------------------------------
		// Gestione Executive SLAVE e Master/Slave
		if (gVMC_ConfVars.ExecutiveSLV == true)
		{
			if (gVMC_ConfVars.ExecMSTSLV == true)
			{
				GestoreMS_Task();																// Gestore Master/Slave Executive
				EXEC_Master_Task();																// Executive Master
			}
			EXEC_Slave_Task();																	// Executive Slave
		}
		// ---------------------------------------------------------------
		// Gestione Selettore Parallelo 
		if ((gVMC_ConfVars.PresMifare == true) || (gVMC_ConfVars.ProtMDB == true))
		{
			Sel24_task();
		}
		
		// ---------------------------------------------------------------
		// Protocollo DDCMP
		if ((gVMC_ConfVars.PresMifare == true) || (gVMC_ConfVars.LocalIrDA == true))
		{
			DDCMPTask();
		}

		// ---------------------------------------------------------------
		// Gestione Display LCD
		Gestione_LCD();

		if (gVMCState.InProgrammaz == false)
		{
			if (TmrTimeout(MainTouts.LCDRefreshTime))
			{
				TmrStart(MainTouts.LCDRefreshTime, REFRESH_LCD_FREQ);							// Ricarico Timeout Refresh Display LCD
				UpdateStatusErogazioni();														// Verifico se c'e' un aggiornamento display LCD da fare
				if (gVMCState.SelezReq == false)												// In fase di richiesta selezione non aggiorno display
				{
					if (Norefresh == false)
					{
						if (Is_LCD_NoAnswerSet() == true)										// Con LCD locale verifico se non risponde piu' per tentare un riavvio 
						{
							//-- LCD sempre busy, tento restart --
							if (Restart_LCD == false)
							{
								LCD_POWER(OFF);													// Spengo display LCD Locale
								Restart_LCD = true;
							}
							else
							{
								LCD_Init(true);													// Reinit LCD
								Restart_LCD = false;
								ClrVisualAll();													// Visualizza msg di st-by dopo msg di Init
							}
						}
						else
						{
							Refresh_LCD();
						}
					}
				}
			}
		}
		else
		{
			VMC_Programmaz_Msg();																// Messaggio programmazione sul display (si esce solo con reset)
		}
		
		// ---------------------------------------------------------------
		// Gestione Visualizzazione immagini Display Touch
		
		//Touch_Gest_Visualizzazioni();
		
		// ---------------------------------------------------------------
		// Eseguo funzioni di Monitor del prodotto
		SystemMonitor();

		// ---------------------------------------------------------------
		// Gestione Display Touch
		Gestione_Touch_Tastiera();
		CheckCreditForSelection();																// ** WARNING **: nella macchina Cesam la "CheckCreditForSelection" deve essere chiamata prima di "Esegui_Selezione"
																								// cosi' che nella ver. "No pulsanti" con credito insuff. possa chiedere VendReq solo una volta 
		// ---------------------------------------------------------------
		// Esecuzione Selezioni
		Esegui_Selezione();	
		
		// ---------------------------------------------------------------
		// Gestione Selezioni																	// ** WARNING **: "Gestione_Selezioni" deve essere chiamata dopo "Esegui_Selezione"
		Gestione_Selezioni();																	// per permettere la corretta sequenza dei messaggi su display Touch
		
		// ---------------------------------------------------------------
		// Controllo Timer Vari e MicroSwitch che generano allarmi
#if (JUMP_CHECK_VARI == false)
		Check_Timer_Vari_e_Allarmi();
#endif

		// ---------------------------------------------------------------
		// Controllo se azzerare Audit LR dopo un prelievo con chiave USB
		// e successivamente controllo se ci sono eventi di Power Outage 
		// e/o WDReset da registrare in EE
		Check_Clear_Audit_LR();

		// ---------------------------------------------------------------
		// Audit Revalue																		// ** WARNING **: "GestioneRevalue" deve essere chiamata prima della "GestioneAuditVendita"
		GestioneRevalue();																		// perche' quest'ultima azzera le variabili utilizzate anche dalla "GestioneRevalue"

		// ---------------------------------------------------------------
		// Audit Vendite
		GestioneAuditVendita();

		// ---------------------------------------------------------------
		// Audit Overpay
		GestioneAuditOverpay();
		
		// ---------------------------------------------------------------
		// Audit FreeCredit
		GestioneAuditFreeCredit();

		// ---------------------------------------------------------------
		// Audit Cash da Bill o ChangeGiver (selettore ??)
		GestioneAuditCash();

		// ---------------------------------------------------------------
		// Audit monete in fill manual
		GestioneAuditManualFill();

		// Audit monete rese come resto
		
		GestioneAuditCoinPayout();

		// ---------------------------------------------------------------
		// Controllo Alimentazione Antenna Mifare e IrDA
		if (gVMC_ConfVars.PresMifare == true)
		{
			if (gOrionKeyVars.AntennaRespFail == false)
			{
				if (Check_MIF_Voltage() == false)
				{
					MIFARE_OFF();																// Spengo PN512
					BlinkErrRedLed_HALT(Milli100, AntennaShortCircuit);							// Blink led rosso CPU ogni 100 msec e mi fermo
				}
			}
		}

		// ---------------------------------------------------------------
		// Controllo Ora Solare-Legale e Fasce orarie Giornaliere
#if DEB_RTC_SPEED
		if (TmrTimeout(MainTouts.ToutRTC))
		{
			TmrStart(MainTouts.ToutRTC, INTERVALLO);
			RTC_GetTimeAndDate(&DataOra);

			if (rtc_in == 0)
			{
				RTC_sec[rtc_in] = RTC_Counter;
				kernel_sec[rtc_in] = gVars.gKernelFreeCounter;
			}
			else
			{
				if (RTC_Counter  !=  (RTC_sec[rtc_in] + (INTERVALLO/1000)))
				{
					RTC_sec[++rtc_in] = RTC_Counter;
					kernel_sec[rtc_in] = gVars.gKernelFreeCounter;
				}
			}
			rtc_in %= RTCBUFFLEN;
		}
#else
		if (TmrTimeout(MainTouts.ToutRTC))
		{																						// Ogni minuto eseguo la routine che modifica automaticamente
			TmrStart(MainTouts.ToutRTC, OneMin);												// l'ora del RTC quando si passa all'ora legale/solare
			Ora_LS_align(&DataOra);
			GestioneFasceSconto();																// Determina se il sistema e' in una qualsiasi fascia sconti
			TestFasceGiornaliere();																// Fasce orarie OFF caldaia e Luci
		}
#endif	

#if (IOT_PRESENCE == true)
		// ---------------------------------------------------------------
		// Gestione scheda Gateway 
		UpdateVMCStatusIoT();
		GestioneErogazioneDaRemoto();															// ** WARNING **: "GestioneErogazioneDaRemoto" deve essere chiamata dopo "Gestione_Selezioni" e prima della "Esegui_Selezione"
		Task_Gtwy();
#endif
		
		// ---------------------------------------------------------------
		// Controllo se introdotta chiave USB o connesso a PC con CDC.
		GestioneUSB_Host_Device();

		// ---------------------------------------------------------------
		// Eseguo funzioni di Monitor del prodotto
		//SystemMonitor();
		
		// ---------------------------------------------------------------
		// Controllo se c'e' un buffer log IrDA in RAM da trasferire in EE
	/*
		if (SystOpt4 & TxIrDALog)
		{																						// Opzione Tx Log Audit Irda al PC
			if (ConnessioneDDCMP_IrDA())
			{																					// Se la connessione DDCMP termina regolarmente
				gVars.Started_RxIrDA = 1;														// la SendLog() sara' chiamata in kDDCMPPhaseFinished:
				if (Transfer_EE)
				{
					TranferLogToEE(false, true);
				}
			}
			else
			{																					// in caso contrario la chiamo qui per inviare
				if (gVars.Started_RxIrDA)
				{																				// la comunicazione registrata
					TranferLogToEE(true, true);													// Trasferisce tutto il rimanente log buffer RAM in  EE
					gVars.Started_RxIrDA = NULL;
				}
			}
		}
	*/
		// --------------------------------------------------------------------------------------------------------------
		// Quando mancano meno di 2 giorni al roll-over del gKernelFreeCounter (0xf5b347ff) procedo al Reset del sistema
		// se in st-by e senza credito.
		// Se permangono le condizioni che impediscono il reset, dopo un'ora dal limite di 2 giorni, procedo comunque ad
		// un reset forzato dopo aver cancellato eventuale credito in Overpay.
	
		if (gVars.gKernelFreeCounter > (0xffffffff - TWO_DAYS))
		{																							// Mancano meno di due giorni al Roll-over del Free Running Counter
			if (gVars.gKernelFreeCounter > (0xffffffff - (TWO_DAYS - ONE_HOUR)))
			{																						// Limite meno di 2 giorni gia' trascorso da un'ora: se c'e' cash lo azzero in Overpay e
				FreeRunningNearRollOver = true;														// al prossimo passaggio eseguo reset del PK3
				if (gOrionCashVars.creditCash > 0)
				{
					Overpay(OVERPAY_CASH);															// Il cash verra' azzerato nella "OverpayCash"
					GestioneAuditOverpay();
				}
				WatchDogReset();																	/***** ATTENDO RESET DA WD ******/
				while(true){};																		
			}
			else
			{
				if ((Is_Machine_StBy() == false) && (gVMCState.InProgrammaz == false))
				{
					WatchDogReset();																/***** ATTENDO RESET DA WD ******/
					while(true){};
				}
			}
		}
		else
		{
			//-- Qui non dovrebbe arrivare mai con FreeRunningNearRollOver true --
			if (FreeRunningNearRollOver == true)													// Se per qualsiasi motivo non e' stato fatto il reset del PK3 prima che il gKernelFreeCounter
			{																						// avesse un roll-over, resetto ora 
				WatchDogReset();																	/***** ATTENDO RESET DA WD ******/
				while(true){};
			}
		}
	}		// Fine While
}		// Fine Main CPU

/*------------------------------------------------------------------------------------------*\
 Method: Time_Init
	Carica un timer con il tempo iniziale di visualizzazione firmware.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  Time_Init(uint32_t TimeDispl)
{
	TmrStart(MainTouts.TimeInitMsg, TimeDispl);													// Tempo visualizzazione messaggi iniziali
}

#if (SaltaResAttuat == false)
/*------------------------------------------------------------------------------------------*\
 Method: ProgKeyToClearFails
	Attende solo la pressione del Tasto PROG per resettare gli allarmi.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void  ProgKeyToClearFails(void)
{
	LCD_SetNormalVisual();
	Motori_OFF();
	Led_Vano(OFF, 0);																			// Spengo led vano prelievo prodotti

#if (IOT_PRESENCE == true)
	Force_Tx_Status();
#endif	
	
	do {
		Gestione_LCD();																			// Visualizza Fail su LCD
		//MR19 Touch_Gest_Visualizzazioni();															// Visualizza Fail su Touch
		if (gVMCState.InProgrammaz == true) VMC_Programmaz_Msg();								// Messaggio programmazione sul display
		Gestione_Touch_Tastiera();
		WDT_Clear(gVars.devWDT);
		GestioneMS_Exec();																		// Check if Exec MST/SLV per eventuale POLL al VMC2
		GestioneUSB_Host_Device();																// Per accettare chiavi USB di configurazione dopo un erase EE completo
		SystemMonitor();

#if (IOT_PRESENCE == true)
		Task_Gtwy();
#endif
		
	} while (true);
}
#endif

/*------------------------------------------------------------------------------------------*\
 Method: GestioneUSB_Host_Device
	Controllo se introdotta chiave USB o connesso a PC con CDC:
	a) se presente +5V sul pin VDD_USB_Pin si attiva la modalita' DEVICE (CDC)
	b) in presenza di chiave USB si attiva il +5V e la modalita' HOST (MSD)
	In programmazione (appena premuto pulsante PROG) non eseguo la funzione per non
	introdurre ritardi se si vuole uscire subito ad es per resettare solo gli allarmi.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  GestioneUSB_Host_Device(void)
{
	static bool RestartNeeded, MsgOnLCD;
	
	if (gVMCState.InProgrammaz == false)
	{
		if (Is_5V_USB_HOST_PC() == false)
		{																						// NON c'e' +5V proveniente dall'USB Host (PC) 

#if DEB_KEY_PWS_LATER			
			if (USB_state != USB_MODE_HOST)
	  		{
	      		USB_state = USB_MODE_HOST;
	      		MX_USB_HOST_Init();
	  		}
	  		else
	  		{
	      		MX_USB_HOST_Process();
	      		if (Appli_state != APPLICATION_START) return;									// Nessuna chiave rilevata
				USB_Key_Voltage(ON);															// Attiva +5V per alimentare la chiave USB presente
				HAL_Delay(Milli500);
#else			
			USB_Key_Voltage(ON);																// Attiva +5V per alimentare l'eventuale chiave USB
			if (USB_state != USB_MODE_HOST)
	  		{
	      		USB_state = USB_MODE_HOST;
	      		MX_USB_HOST_Init();
	  		}
	  		else
	  		{
	      		MX_USB_HOST_Process();
	      		if (Appli_state != APPLICATION_START) return;									// Nessuna chiave rilevata
#endif
				
				MIFARE_OFF();																	// Tolgo +5V Mifare altrimenti il uP riparte in Boot se si esce con reset 
				MIFARE_COMM_DeInit();															// Tolgo l'ulteriore tensione proveniente dai pin UART 
				gOrionKeyVars.AntennaRespFail = true;											// Predispongo per riattivare Mifare se non si esce con reset
				RestartNeeded = false;
				MsgOnLCD = false;
				Touts.tmUSB_Key = TMAX_RILEV_USB;												// Tout attesa passaggio da START a READY
				do
				{
					//-- Rilevata chiave USB ---
				  	MX_USB_HOST_Process();
					if (Appli_state == APPLICATION_READY)
					{    
						if (msc_state_machine != 0)
						{
							GreenLedCPU(ON);
							RedLedCPU(OFF);
							switch (msc_state_machine)
							{
								case 1:
									Explore_Disk("0:/", 1);
									break;
								case 2:
									RestartNeeded = MSC_File_Operations();						// Elabora i comandi della chiave USB
									break;
								default:
									break;
							}
							msc_state_machine = 0;												// Scansione chiave terminata

							if (NewCPU == true)													// In collaudo esco
							{
								return;
							}
							Buz_start(TIME_ENDLESS, HZ_2000, 0);								// Buzzer ON senza fine
						}
					}
					else if (Appli_state == APPLICATION_DISCONNECT)
					{
						//-- Chiave USB Estratta ---
						Buz_stop();						               							 // Buzzer OFF
						GreenLedCPU(ON);
						if (RestartNeeded == true)
						{
							LCD_Disp_WaitRestart();												// Msg "Attendere restart" su LCD
							HAL_Delay(OneSec);
							HAL_NVIC_SystemReset();												// RESET VMC
						}
						else
						{
							msc_state_machine = 2;
							LCD_SetNormalVisual();
							if (gVMC_ConfVars.ProtMDBSLV == false)
							{
								MIFARE_COMM_Init();												// Inizializza la COMM per l'antenna MIFARE
							}
							break;
						}
					}
					else if (Appli_state == APPLICATION_START)
					{
						//-- Chiave USB in rilevamento ---
						if (Touts.tmUSB_Key == 0)
						{
							if (MsgOnLCD == false)
							{
							  	Buz_start(TIME_ENDLESS, HZ_2000, 0);							// Buzzer ON senza fine
								VMC_USB_ErrKeyRilev();											// Messaggio di errore nel rilevamento chiave USB
							  	MsgOnLCD = true;												// messaggio su display LCD e buzzer attivato
								GreenLedCPU(OFF);
								RedLedCPU(ON);
							}
							else
							{
								if (TmrTimeout(MainTouts.LCDRefreshTime))
								{
									TmrStart(MainTouts.LCDRefreshTime, REFRESH_LCD_FREQ);		// Ricarico Timeout Refresh Display LCD
									//Refresh_LCD();
								}
							}
						}
						else
						{
							if (TmrTimeout(MainTouts.LCDRefreshTime))
							{
								TmrStart(MainTouts.LCDRefreshTime, Milli500);
								GreenLedCPU_Toggle();
							}
						}
					}
				} while(true);
	  		}
		}
		else
		{
			//-- +5V su ingresso USB_FS_VBUS: PC collegato --
		  	if (USB_state != USB_MODE_DEVICE)
  			{
  				USB_Key_Voltage(OFF);															// Disattivo uscita +5V per Key USB
    			MX_USB_HOST_Denit();
				HAL_Delay(Milli100);
    			MX_USB_DEVICE_Init();
    			USB_state = USB_MODE_DEVICE;
			}
  		}
	}
}

/*-----------------------------------------------------------------------------------------------*\
Method: Is_Machine_StBy
	Controlla lo stato di inoperativita' della macchina.
 
	IN:	  - 
	OUT:  - true se macchina in St-By e senza credito, altrimenti false
\*------------------------------------------------------------------------------------------------*/
bool Is_Machine_StBy(void)
{
	if ((gOrionKeyVars.Inserted == false) && (gOrionCashVars.creditCash == 0))					// Inserted e creditCash sono utilizzati sia dal Mifare+selettore che dall'MDB
	{
		if (gVMC_ConfVars.ExecutiveSLV == true)
		{
			if (VMC_CreditoDaVisualizzare == 0)
			{
				return true;
			}
			else
			{
				return false;																	// C'e' credito nel Master Executive
			}
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;																			// C'e' carta o cash
	}
}
