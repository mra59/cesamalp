/*--------------------------------------------------------------------------------------*\
	ICP.c
\*--------------------------------------------------------------------------------------*/

#include <string.h>

#include "ORION.H"
#include "icpProt.h"
#include "icpVMCProt.h"
#include "icpCHGProt.h"
#include "icpBILLProt.h"
#include "icpCPCProt.h"
#include "icpUSDProt.h"
#include "DevSerial.h"
#include "OrionCredit.h"
#include "ICPHook.h"
#include "ICPUSDHook.h"


#ifndef nil
  #define  nil    NULL
#endif


#if (_IcpSlave_ & ICP_CHANGER)
const T_PSLVFUNCT tbCHGStates[] = {
	#if (_IcpMaster_ == true)
	&CHGInactive,
	&CHGDisabled,
	&CHGEnabled,
	&CHGPayout
	#else
	&CHGEnabled
	#endif
};
#endif

#if (_IcpSlave_ & ICP_BILLVALIDATOR)
const T_PSLVFUNCT tbBILLStates[] = {
//	#if (_IcpMaster_ == true)
	&BILLInactive,
	&BILLDisabled,
	&BILLEnabled
/*	#else
    &BILLEnabled
	#endif*/
};
#endif

#if (_IcpSlave_ & ICP_CARDREADER)
const T_PSLVFUNCT tbCPCStates[] = {
	&CPCInactive, 
	&CPCDisabled, 
	&CPCEnabled, 
	&CPCSessionIdle, 
	&CPCVend, 
	&CPCRevalue
};
#endif

#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
const T_PSLVFUNCT tbUSDStates[] = {
	&USDInactive, 
	&USDDisabled, 
	&USDEnabled, 
	&USDVend
};
#endif

T_PSLVFUNCT * const tbPeriphInfo[] = {
#if (_IcpSlave_ & ICP_CHANGER)
    (T_PSLVFUNCT *)&tbCHGStates[0],
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
    (T_PSLVFUNCT *)&tbBILLStates[0],
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
    (T_PSLVFUNCT *)&tbCPCStates[0],
#endif
#if (_IcpSlave_ & ICP_USD1)
    (T_PSLVFUNCT *)&tbUSDStates[0],
#endif
#if (_IcpSlave_ & ICP_USD2)
    (T_PSLVFUNCT *)&tbUSDStates[0],
#endif
#if (_IcpSlave_ & ICP_USD3)
    (T_PSLVFUNCT *)&tbUSDStates[0],
#endif
	NULL
};

uint8_t const tbPeriphID[] = {
#if (_IcpSlave_ & ICP_CHANGER)
    ICP_CHG_ADDR,
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	ICP_BILL_ADDR,
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
    ICP_CPC_ADDR,
#endif
	
/*	
#if (_IcpSlave_ & ICP_USD1)
    ICP_USD1_ADDR,
#endif
#if (_IcpSlave_ & ICP_USD2)
    ICP_USD2_ADDR,					// 06.09.17 MR Disabilitata chiamata all'USD2
#endif
#if (_IcpSlave_ & ICP_USD3)
    ICP_USD3_ADDR,					// 06.09.17 MR Disabilitata chiamata all'USD3
#endif
*/
	
	0
};

T_ICP_GLBINFO 	ICPInfo;
bool 			icpRS232Mode;
bool 			icpMasterMode;
uint8_t 			icpSlv, icpSlvAddr, icpSlvState, icpSlvSubState, icpSlvLinkMask;
uint8_t 			*pticpSlvState;
uint8_t 			*pticpSlvSubState;
uint8_t 			*pticpSlvLevel;
T_ICP_PERIPINFO *pticpSlvInfo;

#if (_IcpMaster_ == true)
uint8_t *pticpSlvCheckNoRespState;
uint8_t icpSlvCheckNoRespState;
#endif
#if	kApplExecutiveRS232MDB
void	ResetPolling(void);
#endif

uint8_t 			*pticpSlvNewState;
uint8_t 			*pticpSlvNewSubState;
uint8_t 			*pticpSlvFrameSent;
uint8_t 			*pticpSlvFrameAcked;

//***********************


TaskPhase ICPProt_PollPeriph(TaskPhase fase, TaskArg arg);

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_Init
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_Init(void) {

/*MR19 Eliminati perche' non piu' utilizzati
	static const DevSerOpt icpSerOptions= {
		kDevSerBitRate9600, kDevSerParityNone, kDevSerDataBits9, kDevSerStopBits1
	};
	static const DevSerHnd hnd= {
		ICPErrInd,
		ICPDataInd,
		nil, //ICPTxEmptyInd,
		ICPTxComplete
	};
	DevSerOpt *ptSerOpt = (DevSerOpt *)&icpSerOptions;
*/
	//MR22 Non usata da nessuna parte
	//if (ICPInfo.nICPPollTaskID) return;															// Per evitare di creare pi� volte il task ICP

	icpRS232Mode = false;
	icpMasterMode = ICPProt_IsMaster();

	ICPInfo.WaitAckNackRet = 0;
	ICPInfo.nFrameLen = 0;
	ICPInfo.nModebitPos = 0;
	ICPInfo.iTX = 0;
	ICPInfo.iRX = 0;
	ICPInfo.nComDrvState = COM_IDLE;
	//ICPInfo.nCurrPeriph = 0;
	icpSlv = 0;
	ICPInfo.nTotPeriph = sizeof(tbPeriphID) / sizeof(uint8_t);
	ICPInfo.fSuspendRequest = false;

#if (_IcpMaster_ == true)
	ICPInfo.fMstIdle = false;
	ICPInfo.fSlaveRW = false;
	ICPInfo.fTmPoll = false;
	ICPInfo.nSlaveJustResetMask = 0;
	ICPInfo.nSlaveLinkMask = 0;
	ICPInfo.nSlaveReadyMask = 0;
#endif
#if (_IcpSlaveCode_)
	ICPInfo.fLink = false;
#endif

#if (_IcpSlave_ & ICP_CHANGER)
	CHGInit(ICP_CHG_ADDR);
#endif

#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	BILLInit(ICP_BILL_ADDR);
#endif

#if (_IcpSlave_ & ICP_CARDREADER)
	CPCInit(ICP_CPC_ADDR);
#endif

#if (_IcpSlave_ & ICP_USD1)
	USDInit(ICP_USD1_ADDR);
	ICPInfo.fLink = true;																		// 09.17 Set fLink per visualizzare "Master no Link" se non c'e'...
	ICPTimerStart(ICP_TMID_NOLINK);																// ... connessione con Master all'accensione
#endif

#if (_IcpSlave_ & ICP_USD2)
	USDInit(ICP_USD2_ADDR);
#endif

#if (_IcpSlave_ & ICP_USD3)
	USDInit(ICP_USD3_ADDR);
#endif

#if (_IcpMaster_ == true)
	ICPTimerStart(ICP_TMID_POLLPERIPH);
#endif

	ICPHookCommOpen();

}

/*---------------------------------------------------------------------------------------------*\
Method: ICPSaveSlaveFrameSent
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPSaveSlaveFrameSent(uint8_t frameLen) {
	
	memset(ICPInfo.aPeriphInfo[icpSlv].frameSent, 0, sizeof(ICPInfo.aPeriphInfo[icpSlv].frameSent));
	memcpy(ICPInfo.aPeriphInfo[icpSlv].frameSent, ICPInfo.buff, frameLen);
	ICPInfo.aPeriphInfo[icpSlv].frameSentLen = frameLen;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPLoadSlaveFrameSent
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
uint8_t ICPLoadSlaveFrameSent(void) {
	
	memset(ICPInfo.buff, 0, sizeof(ICPInfo.buff));
	memcpy(ICPInfo.buff, ICPInfo.aPeriphInfo[icpSlv].frameSent, ICPInfo.aPeriphInfo[icpSlv].frameSentLen);
	return ICPInfo.aPeriphInfo[icpSlv].frameSentLen;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_ChangeSlave
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_ChangeSlave(uint8_t slvId) {

	icpSlvAddr = tbPeriphID[slvId];
	pticpSlvInfo = &ICPInfo.aPeriphInfo[slvId];
	pticpSlvState = (uint8_t *)pticpSlvInfo;
	pticpSlvSubState = pticpSlvState + 1;
	pticpSlvLevel = pticpSlvState + 2;
	pticpSlvNewState = pticpSlvState + 3;
	pticpSlvNewSubState = pticpSlvState + 4;
	pticpSlvFrameSent = pticpSlvState + 5;
	pticpSlvFrameAcked = pticpSlvState + 6;
	icpSlvState = *pticpSlvState;
	icpSlvSubState = *pticpSlvSubState;

	#if (_IcpMaster_ == true)
	pticpSlvCheckNoRespState = pticpSlvState + 7;
	icpSlvCheckNoRespState = *pticpSlvCheckNoRespState;
	#endif

	switch(icpSlvAddr) {
#if (_IcpSlave_ & ICP_CHANGER)
	case ICP_CHG_ADDR:
		icpSlvLinkMask = ICP_CHG_LINK;
		break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	case ICP_BILL_ADDR:
		icpSlvLinkMask = ICP_BILL_LINK;
		break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
	case ICP_CPC_ADDR:
		icpSlvLinkMask = ICP_CPC_LINK;
		break;
#endif
#if (_IcpSlave_ & ICP_USD1)
	case ICP_USD1_ADDR:
		icpSlvLinkMask = ICP_USD1_LINK;
		break;
#endif
#if (_IcpSlave_ & ICP_USD2)
	case ICP_USD2_ADDR:
		icpSlvLinkMask = ICP_USD2_LINK;
		break;
#endif
#if (_IcpSlave_ & ICP_USD3)
	case ICP_USD3_ADDR:
		icpSlvLinkMask = ICP_USD3_LINK;
		break;
#endif
	}

	ICPInfo.nCurrPeriph = icpSlv;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_InitSlave
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_InitSlave(uint8_t nSlaveType) {
	
	uint8_t i;
	
	for(i = 0; i < ICPInfo.nTotPeriph; i++) {
		if(tbPeriphID[i] == nSlaveType) {
			ICPInfo.aPeriphInfo[i].nState = 0;
			ICPInfo.aPeriphInfo[i].nSubState = 0;
			ICPInfo.aPeriphInfo[i].nLevel = 1;
			ICPInfo.aPeriphInfo[i].SlaveType = nSlaveType;
			break;
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPTask
	Funzione chiamata dal MAIN.
	Chiama a rotazione le periferiche predefinite da compilazione.
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPTask(void) {
	
#if (_IcpMaster_ == true)
	
	if(icpMasterMode) {
    
		T_PSLVFUNCT *ptPeriphStates;

		if (ICPTimerTimeout(ICP_TMID_POLLPERIPH)) {
			ICPInfo.fTmPoll = true;
		}
		if (ptPeriphStates = tbPeriphInfo[icpSlv]) {
		
			bool fRxFrame;

			fRxFrame = (ICPDrvGetComState() == COM_RXFRAME) ? true : false;
			ICPProt_ChangeSlave(icpSlv);
			(*ptPeriphStates[icpSlvState])();													// Chiamata a funzione
			if (fRxFrame) {
				if(ICPDrvGetComState() == COM_RXFRAME) {
					ICPDrvSetComState(COM_IDLE);												// If rxframe not handled, no tx data, then discard it
				}
			}
		} else ICPDrvSetComState(COM_IDLE);

		ICPProt_HandleNoRespTm(icpSlv);
		ICPDrvHandleComState();
	
		if (ICPDrvGetComState() == COM_IDLE) {
			if(ICPInfo.fTmPoll) {
				if(ptPeriphStates) {
					ICPInfo.fTmPoll = false;
					ICPTimerStart(ICP_TMID_POLLPERIPH);											// Carica Timeout chiamata successiva periferica
				}
				do {
					if (++icpSlv >= ICPInfo.nTotPeriph) icpSlv = 0;								// Cambia periferica con la quale comunicare
					// 09.17 Disabilito chiamate all'USD2, USD3
					// 09.17 Disabilito chiamate al CPC quando c'e' Mifare Locale
					while ((ICPInfo.aPeriphInfo[icpSlv].SlaveType == ICP_CPC_ADDR) && (gVMC_ConfVars.CPC_MDB == false) 	||
						   (ICPInfo.aPeriphInfo[icpSlv].SlaveType == ICP_USD2_ADDR)										||
						   (ICPInfo.aPeriphInfo[icpSlv].SlaveType == ICP_USD3_ADDR)
						  )
					{
						if (++icpSlv >= ICPInfo.nTotPeriph) icpSlv = 0;							// Cambia periferica con la quale comunicare
					}
					ICPProt_ChangeSlave(icpSlv);
					ICPProt_HandleNoRespTm(icpSlv);
				} while (ICPInfo.aPeriphInfo[icpSlv].slvCheckNoRespState == 3);
			}
		}

#if (_IcpSlaveCode_ & ICP_CARDREADER)		//ABELE
	CashlessTask();																				// E' la OrionCredit
#endif

	return;
}
//$USD$#else
#endif

#if (_IcpSlaveCode_)
	#if (_IcpSlaveCode_ != ICP_CARDREADER)
		ICPProt_DispatchCmds();
	#endif
	CashlessTask();																				// E' la OrionCredit
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_PollPeriph
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
TaskPhase ICPProt_PollPeriph(TaskPhase fase, TaskArg arg) {
	
	return fase;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_DispatchCmds
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_DispatchCmds(void) {

#if (_IcpSlaveCode_)
	ICPDrvHandleComState();
	switch(ICPDrvGetComState()) {
		case COM_RXFRAME:
			ICPProt_DispatchFrame();
			break;
		case COM_RETRYTX:
			// check dest-periph?!!!
			ICPDrvSendMsg(ICPInfo.iTX - 1);
			break;
	}
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_DispatchFrame
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_DispatchFrame(void) {
	
//$USD$#if (_IcpMaster_ == false)
#if (_IcpSlaveCode_)
    T_PSLVFUNCT *ptPeriphStates;
    uint8_t i, nPeriphAddr;

	nPeriphAddr = ICPAddr;
	for(i = 0; i < ICPInfo.nTotPeriph; i++) {

		if(tbPeriphID[i] == nPeriphAddr) {
			if(ptPeriphStates = tbPeriphInfo[i]) {
				icpSlv = i;
				ICPProt_ChangeSlave(icpSlv);
				(*ptPeriphStates[icpSlvState])();					// Chiamata allo stato attuale della periferica (es. CPCSessionIdle(), CPCVend(), etc)
			}
			break;
		}
	}
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_IsMaster
	
	IN:	 - 
	OUT: - false se Slave Mode, true se Master Mode
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_IsMaster(void) {
	
	return (!ICPVMCHook_GetSlaveMode());
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_IsRS232
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_IsRS232(void) {
	
	bool fRS232 = false;

#if(kIcpRs232Support == true)
	fRS232 = ICPHook_GetRS232Mode();
#endif
	return fRS232;
}


/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_ChangeState
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_ChangeState(uint8_t nSlaveID, int8 nState, int8 nSubState) {
	
	uint8_t SlType = ICPInfo.aPeriphInfo[nSlaveID].SlaveType;
	
	switch(SlType){
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	case ICP_BILL_ADDR:
		if(nState>0)ICPBILLInfo.bill_state = nState;
		break;
#endif
		
#if (_IcpSlave_ & ICP_CHANGER)
	case ICP_CHG_ADDR:
		if(nState>0)ICPCHGInfo.chg_state = nState;
		break;
#endif		
		
#if (_IcpSlave_ & ICP_CARDREADER)
	case ICP_CPC_ADDR:
		ICPCPCInfo.cpc_state = nState;
		break;
#endif
		
	default:
		break;
		
	}
	if(nState >= 0) *pticpSlvState = nState;
	if(nSubState >= 0) *pticpSlvSubState = nSubState;
	icpSlvState = *pticpSlvState;
	icpSlvSubState = *pticpSlvSubState;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_HandleNoRespTm
	Predispone i timeout della periferica e relativo "SlvCheckNoRespState" nella struct
	"ICPInfo.aPeriphInfo".
	SlvCheckNoRespState assume questi valori:
	IN = 2: 
		quando scaduto timeout --> carica timeout 10 sec per invio RESET
							   --> pone a INACTIVE lo stato della periferica
							   --> chiama la "ICPProt_SetLink" con false
							   --> ricarica il timer periferica con i 10 sec del comando RESET
							   --> OUT = 7
	IN = 7: call "ICPProt_xxxReset" che init periferica e Tx RESET;  OUT = 3 
	IN = 3:  
		quando scaduto timeout --> carica timeout 10 sec per invio RESET
							   --> OUT = 7
	
	IN:	 - Numero periferica (0-n)
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_HandleNoRespTm(uint8_t nPeriphID) {
	
#if (_IcpMaster_ == true)

	uint16_t nNoRespTmID;
	uint16_t nSlaveResetTmID;
	int8 nSlaveState;
	int8 nSlaveSubState;
	T_PSLVFUNCT ptReset;
	
	ICPInfo.SlaveReqReset = false;

	if(nPeriphID >= ICPInfo.nTotPeriph)
		return;
	if(icpSlvCheckNoRespState == 0)
		return;

	switch(icpSlvAddr) {
#if (_IcpSlave_ & ICP_CHANGER)
	case ICP_CHG_ADDR:
		nNoRespTmID = ICP_TMID_CHGMAXNORESP;
		nSlaveResetTmID = ICP_TMID_CHGRESET;
		nSlaveState = ICP_CHG_INACTIVE;
		nSlaveSubState = ICP_CHG_INACTIVE_TXRESET;
		ptReset = &ICPProt_CHGReset;
		break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	case ICP_BILL_ADDR:
		nNoRespTmID = ICP_TMID_BILLMAXNORESP;
		nSlaveResetTmID = ICP_TMID_BILLRESET;
		nSlaveState = ICP_BILL_INACTIVE;
		nSlaveSubState = ICP_BILL_INACTIVE_TXRESET;
		ptReset = &ICPProt_BILLReset;
		break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
	case ICP_CPC_ADDR:
		nNoRespTmID = ICP_TMID_CPCMAXNORESP;
		nSlaveResetTmID = ICP_TMID_CPCRESET;
		nSlaveState = ICP_CPC_INACTIVE;
		nSlaveSubState = ICP_CPC_INACTIVE_TXRESET;
		ptReset = &ICPProt_CPCReset;
		break;
#endif
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	case ICP_USD1_ADDR:
	case ICP_USD2_ADDR:
	case ICP_USD3_ADDR:
		nNoRespTmID = ICP_TMID_USDMAXNORESP;
		nSlaveResetTmID = ICP_TMID_USDRESET;
		nSlaveState = ICP_USD_INACTIVE;
		nSlaveSubState = ICP_USD_INACTIVE_TXRESET;
		ptReset = &ICPProt_USDReset;
		break;
#endif
	default:
		break;
	}

	switch(icpSlvCheckNoRespState) {
	case 1:
		ICPTimerStart(nNoRespTmID);
		*pticpSlvCheckNoRespState = 2;
		break;
	case 2:
		if(ICPTimerTimeout(nNoRespTmID)) {
			ICPTimerStart(nSlaveResetTmID);
			ICPProt_SetLink(icpSlvAddr, false);
			ICPProt_ChangeState(nPeriphID, nSlaveState, nSlaveSubState);
			*pticpSlvCheckNoRespState = 7;
		}
		break;
	case 3:
		if(ICPTimerTimeout(nSlaveResetTmID)) {
			ICPTimerStart(nSlaveResetTmID);
			*pticpSlvCheckNoRespState = 7;
		}
		break;
	case 7:	// tx Reset
		ptReset();
		*pticpSlvCheckNoRespState = 3;
		ICPInfo.SlaveReqReset = true;
		
		break;
	}
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_WaitInit
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_WaitInit(uint8_t nSlaveType) {

#if (_IcpMaster_ == true)
	//uint8_t nWaitInitCnt;
	uint8_t *ptWaitInitCnt;	//16/11/00 OSz (-12)

	switch(nSlaveType) {
#if (_IcpSlave_ & ICP_CHANGER)
	case ICP_CHG_ADDR:
		ptWaitInitCnt = &ICPCHGInfo.nWaitInitCnt;
		break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
	case ICP_BILL_ADDR:
		ptWaitInitCnt = &ICPBILLInfo.nWaitInitCnt;
		break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
	case ICP_CPC_ADDR:
		ptWaitInitCnt = &ICPCPCInfo.nWaitInitCnt;
		break;
#endif
#if (_IcpSlave_ & ICP_USD1)
	case ICP_USD1_ADDR:
		ptWaitInitCnt = &ICPUSD1Info.nWaitInitCnt;
		break;
#endif
#if (_IcpSlave_ & ICP_USD2)
	case ICP_USD2_ADDR:
		ptWaitInitCnt = &ICPUSD2Info.nWaitInitCnt;
		break;
#endif
#if (_IcpSlave_ & ICP_USD3)
	case ICP_USD3_ADDR:
		ptWaitInitCnt = &ICPUSD3Info.nWaitInitCnt;
		break;
#endif
	default:
		return true;
	}

	if(*ptWaitInitCnt)
		*ptWaitInitCnt = (*ptWaitInitCnt) - 1;
	else
		return false;

	return true;
#else
	return false;
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_SetInhibit
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_SetInhibit(uint8_t nSlaveType, bool fInh) {
	
#if (_IcpMaster_ == false)
	#if (_IcpSmsAlarms_ == true)
		if(!fInh) {
			if(ICPInfo.fMstInhibit) {
				ICPInfo.fMstInhibit = false;
				ICPHook_SetMstInhibit(ICPInfo.fMstInhibit);
			}
			ICPInfo.nSlaveInhMask &= ~icpSlvLinkMask;
		} else {
			Tmr32Start(ICPInfo.nTmMstInhibit, ICP_TM_MSTINH);
			ICPInfo.nSlaveInhMask |= icpSlvLinkMask;
		}
		ICPInfo.nSlvInhMask |= icpSlvLinkMask;
	#endif
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_SetLink
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_SetLink(uint8_t nSlaveType, bool fLink) {
	
#if (_IcpMaster_ == true)
	if(!fLink) {
		if(ICPInfo.nSlaveLinkMask & icpSlvLinkMask) {
			ICPInfo.nSlaveLinkMask &= ~icpSlvLinkMask;
			ICPHook_SetSlaveLink((ET_SLAVELINK)ICPInfo.nSlaveLinkMask);
		}
	} else {
		if(!(ICPInfo.nSlaveLinkMask & icpSlvLinkMask)) {
			ICPInfo.nSlaveLinkMask |= icpSlvLinkMask;
			ICPHook_SetSlaveLink((ET_SLAVELINK)ICPInfo.nSlaveLinkMask);
		}
	}
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_SetReady
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_SetReady(uint8_t nSlaveType, bool fReady) {
	
#if (_IcpMaster_ == true)
	bool fSlvReady, fSet = false;
	if(!fReady) {
		if(ICPInfo.nSlaveReadyMask & icpSlvLinkMask) {
			ICPInfo.nSlaveReadyMask &= ~icpSlvLinkMask;
			fSet = true;
			fSlvReady = false;
		}
	} else {
		if(!(ICPInfo.nSlaveReadyMask & icpSlvLinkMask)) {
			ICPInfo.nSlaveReadyMask |= icpSlvLinkMask;
			fSet = true;
			fSlvReady = true;
		}
	}

	if(fSet) {
		switch(nSlaveType) {
#if (_IcpSlave_ & ICP_CHANGER)
		case ICP_CHG_ADDR:
			ICPCHGHook_SetReady(fSlvReady);
			break;
#endif
#if (_IcpSlave_ & ICP_BILLVALIDATOR)
		case ICP_BILL_ADDR:
			ICPBILLHook_SetReady(fSlvReady);
			break;
#endif
#if (_IcpSlave_ & ICP_CARDREADER)
		case ICP_CPC_ADDR:
			ICPCPCHook_SetReady(fSlvReady);
			break;
#endif
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
		case ICP_USD1_ADDR:
		case ICP_USD2_ADDR:
		case ICP_USD3_ADDR:
			ICPUSDHook_SetReady(nSlaveType, fSlvReady);
			break;
#endif
		default:
			break;
		}
	}
#endif
}


/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_DrvGetLevel
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
uint8_t ICPProt_DrvGetLevel(uint8_t nPeriphType) {
	
	uint8_t i;
	
	for(i = 0; i < ICPInfo.nTotPeriph; i++) {
		if(tbPeriphID[i] == nPeriphType) {
			return ICPInfo.aPeriphInfo[i].nLevel;
		}
	}
	return 0;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_GetLevel
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
uint8_t ICPProt_GetLevel(void) {

	return *pticpSlvLevel;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_SetLevel
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPProt_SetLevel(uint8_t nPeriphType, uint8_t nPeriphLevel) {
	
	uint8_t nVmcLevel = 0;

#if (_IcpMaster_ == true)
if(icpMasterMode) {

	switch(nPeriphType) {
	case ICP_CPC_ADDR:
		nVmcLevel = _IcpCpcLevel_;
		// Vmc level?!
		break;
	case ICP_CHG_ADDR:
		nVmcLevel = _IcpChgLevel_;
		break;
	case ICP_BILL_ADDR:
		nVmcLevel = _IcpBillLevel_;
		break;
	case ICP_USD1_ADDR:
	case ICP_USD2_ADDR:
	case ICP_USD3_ADDR:
		nVmcLevel = _IcpUsdLevel_;
		break;
	default:
		break;
	}

	if(nVmcLevel)
		*pticpSlvLevel = (nPeriphLevel > nVmcLevel) ? nVmcLevel : nPeriphLevel;
return;
}
//$USD$#else
#endif
#if (_IcpSlaveCode_)
	switch(nPeriphType) {
	#if (_IcpSlaveCode_ & ICP_CARDREADER)
	case ICP_CPC_ADDR:
		nVmcLevel = nPeriphLevel;
		nPeriphLevel = ICPCPCHook_GetFeatureLevel();
		*pticpSlvLevel = (nPeriphLevel > nVmcLevel) ? nVmcLevel : nPeriphLevel;
		break;
	#endif
	#if (_IcpSlaveCode_ & ICP_CHANGER)
	case ICP_CHG_ADDR:
		*pticpSlvLevel = ICPCHGHook_GetFeatureLevel();
		break;
	#endif
	#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	case ICP_BILL_ADDR:
		*pticpSlvLevel = _IcpBillLevel_;	//ICPBILLHook_GetFeatureLevel();
		break;
	#endif
	#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	case ICP_USD1_ADDR:
	case ICP_USD2_ADDR:
	case ICP_USD3_ADDR:
		*pticpSlvLevel = _IcpUsdLevel_;	//ICPUSDHook_GetFeatureLevel();
		break;
	#endif
	default:
		*pticpSlvLevel = 0;
		break;
	}
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_IsMultiVend
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_IsMultiVend(void) {
	
	bool fMultiVend = true;

#if (_IcpMaster_ == true)
	fMultiVend = ICPVMCHook_GetMultiVend();
	#if (_IcpSlave_ & ICP_CARDREADER)
		fMultiVend &= ICPProt_CPCGetMultiVend();
	#endif
#endif
	return fMultiVend;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_IsCredit32
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_IsCredit32(void) {
	
	bool fCredit32 = false;

#if (_IcpMaster_ == true)
	#if (_IcpSlave_ & ICP_CARDREADER)
		fCredit32 = ICPProt_CPCGetCredit32();
	#endif
#endif
	return fCredit32;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_IsMultiCurr
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_IsMultiCurr(void) {
	
	bool fMultiCurr = false;

#if (_IcpMaster_ == true)
	#if (_IcpSlave_ & ICP_CARDREADER)
		fMultiCurr = ICPProt_CPCGetCredit32();
	#endif
#endif
	return fMultiCurr;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_IsNegVend
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_IsNegVend(void) {
	
	bool fNegVend = false;

#if (_IcpMaster_ == true)
	#if (_IcpSlave_ & ICP_CARDREADER)
		fNegVend = ICPProt_CPCGetNegVend();
	#endif
#endif
	return fNegVend;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPProt_IsDataEntry
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool ICPProt_IsDataEntry(void) {
	
	bool fDataEntry = false;

#if (_IcpMaster_ == true)
	#if (_IcpSlave_ & ICP_CARDREADER)
		fDataEntry = ICPProt_CPCGetDataEntry();
	#endif
#endif
	return fDataEntry;
}
#if	kApplExecutiveRS232MDB
void	ResetPolling(void) {
	TmrStart(ICPInfo.nTmPoll, 10);
}
#endif
