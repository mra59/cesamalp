/****************************************************************************************
File:
    ExecSlave.c

Description:
    File con le funzioni del Protocollo Executive Slave
    

History:
    Date       Aut  Note
    Apr 2016	MR   

*****************************************************************************************/

#include <string.h>

#include "main.h"
#include "ORION.H"
#include "Gestore_Exec_MSTSLV.h"
#include "ExecSlave.h"
#include "OrionCredit.h"
#include "VMC_GestSelez.h"
#include "OrionTimers.h"
#include "VMC_CPU_HW.h"


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

static ExecSlaveVars sExecSlvVars;


// ========================================================================
// ========    EXECUTIVE    SLAVE     TASK    VMC           ===============
// ========================================================================

/*---------------------------------------------------------------------------------------------*\
Method: EXEC_Slave_Task
	Funzione chiamata continuamente dal MAIN
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void EXEC_Slave_Task(void) {
	
	uint16_t		prezzosel;
	CreditInfo 		creditInfo;
	CreditPerm 		VendReqResult;
	
	if (sExecSlvVars.VendInProgress == false) {
		ExecSlaveVendEnable(!fVMC_Inibito);														// Predispongo risposta allo STATUS

		// ================  Controllo Nuova Visualizzata =======================
		if (ExecSlaveGetCreditInfo(&creditInfo)) {												// Credito variato - Contiene il test del timeout NoLink
			VMC_CreditoDaVisualizzare = creditInfo.creditVal;									// Visualizza come credito anche quando e' il prezzo selezione in Price Display
			if (creditInfo.exactChange) {
				Set_fExactChange;
			} else {
				Clear_fExactChange;
			}
		}

		// ================  Elaborazione risposta al VendReq ===================
		if (sExecSlvVars.vendState != kVendNone) {
			// VendRequest in corso
			creditInfo = sExecSlvVars.ci;														// Controllo Price Display
			prezzosel = (CreditValue) sExecSlvVars.priceCode;
			VendReqResult = CreditHndExecVendAllowed(&prezzosel, &creditInfo);
			//VendReqResult = ExecSlaveVendAllowed(&creditInfo);								// Da chiamare in alternativa al Price Display Check qui sopra
			switch (VendReqResult) {
				case kCreditPermWaiting:
					break;

				case kCreditPermGranted:
					VendAuthorResult(true);														// Vendita approvata
					
			/*	Tolgo se attivo controllo Price Display qui sopra	
					if(gOrionConfVars.RestoSubito) {
						ExecSlaveVendCompleted();												// Vend Success
						ExecSlaveVendEnable(false);												// Rispondo INH al Master
						sExecSlvVars.waitingRealVendEnd = true;									// Attesa fine vendita reale
					}
			*/
					sExecSlvVars.VendInProgress = true;
					break;
			
				case kCreditPermDenied:
					sExecSlvVars.vendState = kVendNone;
					sExecSlvVars.statusCnt = 0;
					if (gOrionConfVars.PriceDisplay) {
						VMC_ValoreSelezione = sExecSlvVars.prDispCreditInfo.creditVal; 
					}
					VendAuthorResult(false);													// Vendita negata
					break;
			}
		// ----- Fine sostituzione del Price Display Test ------
		}	
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	CreditHndExecVendAllowed
	Funzione di verifica credito sufficiente.
	
Parameters:
	value		<-	selection value, if known
	creditInfo	<-	(if not nil) current credit info
Returns:
	kCreditPermGranted se credito sufficiente
------------------------------------------------------------------------------------------
Communication description when Price Display enabled:

- Fage/Mars systems			Vending Machine
			CREDIT			VEND REQUEST
			ACCEPT DATA		ACK				-> selection price
			STATUS			ACK				-> means vend denied
			...				...
			STATUS			ACK				
			ACCEPT DATA		ACK				-> actual credit

- Coges systems			Vending Machine
			CREDIT			VEND REQUEST
			ACCEPT DATA		ACK				-> selection price
			nothing sent
			ACCEPT DATA		ACK				-> actual credit

- Elkey systems			Vending Machine
			CREDIT			VEND REQUEST
			ACCEPT DATA		ACK				-> selection price
			STATUS			ACK				-> means vend denied
			ACCEPT DATA		ACK				-> actual credit
			STATUS			ACK				


- Vend Accepted			Vending Machine
			CREDIT			VEND REQUEST
			ACCEPT DATA		ACK				-> selection price
			VEND			   				-> means vend accepted
			nothing sent until vend complete
			                VEND OK
			ACCEPT DATA		ACK				-> actual credit


Nei sistemi Coges dopo la prima visualizzata contenente il prezzo della selezione il master
Executive si ferma (mentre i Fage/Mars continuano con la sequenza status / credit), quindi
trascorso il tempo "Price Display" invia un'altra visualizzata con il credito.

Il sistema (in Price Holding) si aggancia alle due visualizzate di credito:
- la prima per visualizzare l'importo necessario per la selezione;
- la seconda per abortire la vendita (credito insufficiente).
NB. Non vanno comunque gestiti timeout brevi (dell'ordine dei 200-500msec) per abortire nel
caso in cui la richiesta di vendita non arrivi in quanto potrebbe essere necessario un tempo
pi� lungo al sistema di pagamento per scalare il credito e aggiornare i suoi dati interni.

Sistemi a chiave Elkey:
1. In caso di credito insufficiente il master Elkey, dopo la prima visualizzata contenente 
  il prezzo selezione, invia status e subito dopo la nuova visualizzata del credito (senza
  aspettare i 2 secondi); diventa compito del VMC garantire un tempo minimo di 2 secondi
  per visualizzare il prezzo selezione, restituendo vend denied ritardato. 
  Modifica:	 TEMPORIZZAZIONE DELLA VISUALIZZA SE TROPPO BREVE.

2. In caso di credito insufficiente la visualizzata  contiene gi� il credito mancante e non
  il costo intero della selezione (come su sistemi Fage, Coges, ...). Esempio: 
  credito 0,05, prezzo 0,20 ==> visualizzata 0,15
  
  Modifica:	 ELIMINATO CALCOLO INTERNO PER DISPLAY VALORE CREDITO INSUFFICIENTE:
  Visualizza sempre il prezzo della selezione indipendentemente dai sistemi collegati.
  Per sistemi Fage, Mars, Coges e simili sar� sempre il prezzo della selezione, mentre per
  sistemi Elkey e simili sar� il credito necessario per la selezione. Esempio: 
  -ELKEY:  credito 0,05, prezzo 0,20 ==> visualizza " SELEZIONE xx 0,15"
  -ALTRI:  credito 0,05, prezzo 0,20 ==> visualizza " SELEZIONE xx 0,20"

3. In caso di credito insufficiente pari al valore di credito attualmente presente sul VMC
  NON viene inviata alcuna visualizzata. Esempio:
  credito 0,05, prezzo 0,10 ==> NON INVIA (visualizzata 0,05) 
  credito 0,10, prezzo 0,20 ==> NON INVIA (visualizzata 0,10) 

  Modifica:	 VISUALIZZA ALMENO UNA VOLTA IL PREZZO ANCHE QUANDO NON RICEVE ALCUN "PRICE
  DISPLAY" DOPO LA RICHIESTA DI VENDITA. VIENE VISUALIZZATO UN PREZZO PARI A VALORE ULTIMO
  CREDITO RICEVUTO. Esempio:
  credito 0,05, prezzo 0,20 ==> visualizza " SELEZIONE xx 0,05"
\*--------------------------------------------------------------------------------------*/
static CreditPerm CreditHndExecVendAllowed(CreditValue* value, CreditInfo* creditInfo) {

	CreditPerm vendState;

	vendState = ExecSlaveVendAllowed(creditInfo);
	if ((gOrionConfVars.PriceDisplay) && (vendState != kCreditPermGranted)) {
		// Price Display e credito insufficiente: attendo visualizzata di credito mancante
		if (vendState == kCreditPermDenied) return vendState;
		// kCreditPermWaiting: in attesa risposta dal Master
		if (sExecSlvVars.prDispStart == false) {
			if (creditInfo->creditVal != sExecSlvVars.prDispCreditInfo.creditVal) {				// Ricevuta nuova visualizzata, non so ancora se nuovo credito o prezzo selezione
				sExecSlvVars.prDispStart = true;
				sExecSlvVars.prDispCreditInfo.creditVal = creditInfo->creditVal;				// Valore credito ricevuto in prDispCreditInfo
				return kCreditPermWaiting;														// Attende Vend Approved/Denied dal Master
			}
		}
		return kCreditPermWaiting;																// Attendo nuova visualizzata
	}
	if (vendState == kCreditPermGranted) {
		if (gOrionConfVars.RestoSubito) {
			ExecSlaveVendCompleted();															// Simula vendita completata e risponde VEND OK
			ExecSlaveVendEnable(false);															// Rispondo Inhibit al Master fino al termine della selezione
			sExecSlvVars.waitingRealVendEnd = true;												// Attendere fine selezione
		} else {
			if (gVMC_ConfVars.ExecMSTSLV == TRUE) {
				//SetMasterTxStatusOnly();														// RR Busy attende fine vend per cui Master trasmette solo STATUS al VMC2
			}
		}
	}
/*
	if(gOrionConfVars.RestoSubito && vendState == kCreditPermGranted) {							// Resto subito e Vend Approved
		ExecSlaveVendCompleted();																// Simula vendita completata e risponde VEND OK
		ExecSlaveVendEnable(false);																// Rispondo Inhibit al Master fino al termine della selezione
		sExecSlvVars.waitingRealVendEnd = true;													// Attendere fine selezione
	}
*/	
	return vendState;
}

/*--------------------------------------------------------------------------------------*\
Method:	CreditGetInfo
	Get info on current available credit

Parameters:
	creditInfo	<-	credit information
Returns:
	bool if credit info changed since last time	
\*--------------------------------------------------------------------------------------*/
/*
static bool CreditGetInfo(CreditInfo* creditInfo) {
	
	return (*sCreditHnd->getInfo)(creditInfo);
}
*/

/*---------------------------------------------------------------------------------------------*\
Method: ExecSlaveInit
	Funzione chiamata la prima volta che si esegue il MAIN
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/

void ExecSlaveInit(void) {
	
	memset(&sExecSlvVars, 0, sizeof(sExecSlvVars));
	//sExecSlvVars.state= kStateWait;
	sExecSlvVars.vendState= kVendNone;
	sExecSlvVars.cd.scalingFact= 1;
	UnitScalingFactor = sExecSlvVars.cd.scalingFact;
	#if kProtExecSlaveAuditUnitSupport
	sExecSlvVars.ad.scalingFact= 1;
	#endif

	sExecSlvVars.masterUnavail = false /*true*/;
		// Changed to signal at initialization if master unavailable
		// Probably the best solution would be to indicate both events at startup:
		// - the commAvailable as soon as we find it available
		// - the commUnavailable if we detect that it is unavailable

	sExecSlvVars.identifyReq= true;
	ExecSlaveCommTmrStart();
	Executive_COMM_Init();
	ExecSlaveTx(kExecPNAK);																		// Invia PNACK al master per iniziare la comunicazione executive
	Set_fVMC_Inibito;																			// Predispongo sistema inibito
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveAuditDataGet
	Check if we have received any audit info
Parameters:
	ade	<-	next audit data element
Returns:
	true if valid audit data returned	
\*--------------------------------------------------------------------------------------*/
#if kProtExecSlaveAuditUnitSupport
bool ExecSlaveAuditDataGet(ExecAuditDataEl* ade) {
	
	uint8_t 				addr;
	uint16_t 				cvtFact= 1;
	ExecAuditPrivEl*	apde;

	if(sExecSlvVars.ad.putIx==sExecSlvVars.ad.getIx) return false;
	apde= &sExecSlvVars.ad.data[sExecSlvVars.ad.getIx];
	addr= apde->addr;
	
	// Extract unit scaling factor
	if(addr==4 || addr==123) sExecSlvVars.ad.scalingFact= apde->value;
	
	if(addr>=7 && addr<=44 || addr>=52 && addr<=89) cvtFact= sExecSlvVars.ad.scalingFact;
	ade->addr= addr;
	ade->value= apde->value * cvtFact;
		// Convert credit data from USF units to real units

	if(++sExecSlvVars.ad.getIx>=nelof(sExecSlvVars.ad.data)) sExecSlvVars.ad.getIx= 0;
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveAuditUSFSet
	Set default Unit Scaling Factor for Audit Storage Unit 
	May be used to set a default USF which is used until the actual USF is received

Parameters:
	usf	-> default usf
\*--------------------------------------------------------------------------------------*/
void ExecSlaveAuditUSFSet(uint8_t usf) {
	sExecSlvVars.ad.scalingFact= usf;
}

#endif

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveGetCreditInfo
	Get info on current available credit
	
Parameters:
	creditInfo	<-	credit miscellaneous information

Returns:
	true if credit info has changed	
\*--------------------------------------------------------------------------------------*/
bool  ExecSlaveGetCreditInfo(CreditInfo* creditInfo) {
	
	bool creditInfoChanged = false;

	ExecSlaveCommCheck();																		// Predispone la flag fVMC_NoLink set quando scade timeout
	if (sExecSlvVars.cdChanged) {
		// New credit data received. Update credit info
		ExecSlaveUpdateCreditInfo();
		sExecSlvVars.cdChanged = false;
		creditInfoChanged = true;
		if (!sExecSlvVars.haveCreditData) {
			//ExecSlaveHook1stCredit();
		}
		sExecSlvVars.haveCreditData = true;
	}
	if (creditInfo) *creditInfo = sExecSlvVars.ci;
	return creditInfoChanged;
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveVendRequest
	Determino costo/numero selezione e, se parametri leciti, attivo la VendRequest al
	Master.

   IN:  -  valore selezione o battuta richiesta
  OUT:  -  result=0 con Valselez/numsel in sExecSlvVars.priceCode e kVendRq se leciti, 
  	  	   altrimenti result>0.
\*--------------------------------------------------------------------------------------*/
void ExecSlaveVendRequest(ExecVendReqResp* risul, uint8_t numtasto) {
	
	CreditValue	ValSelez;
	CreditInfo tempCreditInfo;
	
	if (numtasto == 0) {																		// Il numero selezione non puo' essere zero
		risul->result = 1;
		return;
	}
	if (gOrionConfVars.PriceHolding) {
		// -----  Executive  Price Holding  -------
		sExecSlvVars.priceCode = TabellaSelezPrezzo[numtasto-1];								// Numero selezione come da tabella selezione/Prezzo
		if (sExecSlvVars.priceCode == 0) {														// Prezzo non programmato nella tabella
			risul->result = 2;
			return;
		}
		VMC_ValoreSelezione = MRPPrices.Prices99.MRPPricesList99[numtasto-1];					// Per la visualizzazione locale di eventuale credito insufficiente
	} else {
		// -----  Executive  Standard -------
		ValSelez = MRPPrices.Prices99.MRPPricesList99[numtasto-1];
		
		if (SetPrice_e_Selnum(0, numtasto) == FALSE) {
			risul->result = 2;																	// Prezzo non valido
			return;
		}
		ValSelez = VMC_ValoreSelezione;
		// Todo: modificare in modo che con ValSelez=0 e VMC_CreditoDaVisualizzare=0 chiedo FreeVend allo Status
		if (VMC_CreditoDaVisualizzare == 0) {													// Se non c'e' credito non c'e' Comando CRED (32) e non posso richiedere selezioni
			risul->creditomancante = ValSelez;													// Quindi visualizzo il costo della selezione
			risul->result = 4;
			return;
		}
		sExecSlvVars.priceCode = ExecSlaveCvtPrice(ValSelez);
		if (sExecSlvVars.priceCode >= kExec1stReserved) {										// Prezzo non corretto, non eseguo Vend Req
			risul->result = 3;
			return;
		}
		
/*		
		if (ValSelez == PrezzoNonDisponibile) {													// Prezzo non valido
			risul->result = 2;
			return;
		}
		if (VMC_CreditoDaVisualizzare == 0) {													// Se non c'e' credito non c'e' Credit CMD e non posso richiedere selezioni
			risul->creditomancante = ValSelez;
			risul->result = 4;
			return;
		}
		sExecSlvVars.priceCode = ExecSlaveCvtPrice(ValSelez);
		if (sExecSlvVars.priceCode >= kExec1stReserved) {										// Prezzo non corretto, non eseguo Vend Req
			risul->result = 3;
			return;
		}
*/
	}
	VMC_NumeroSelezione = numtasto;
	sExecSlvVars.statusCnt = 0;
	sExecSlvVars.vendState = kVendRq;															// Attiva una VendRequest
	risul->result = 0;

	// Istruzioni per l'eventuale Price Display
	sExecSlvVars.prDispStart = false;
/*	
	if (gOrionConfVars.PriceDisplay) {															// Price Display: eseguo solamente visualizzazione display
		sExecSlvVars.lastSelValue = 0;
		sExecSlvVars.prDispCreditInfo.creditVal = 0;
	}
*/	
	ExecSlaveVendAllowed(&tempCreditInfo);														// Memorizza credito attuale come vecchio credito
/*	
	if (sExecSlvVars.lastSelValue == tempCreditInfo.creditVal) {
		tempCreditInfo.creditVal = sExecSlvVars.prDispCreditInfo.creditVal;
	}
*/	
	sExecSlvVars.prDispCreditInfo = tempCreditInfo;
	return;
}

/*--------------------------------------------------------------------------------------*\
Method:	CreditVendAllowed
	Check if there is enough credit for the selection requested

Parameters:
	creditInfo	<-	(if not nil) current credit info
Returns:
	kCreditPermGranted if credit is enough	
\*--------------------------------------------------------------------------------------*/
CreditPerm ExecSlaveVendAllowed(CreditInfo* creditInfo) {
	
	if (creditInfo) {
		ExecSlaveUpdateCreditInfo();															// Be sure credit info is up-to-date
		*creditInfo = sExecSlvVars.ci;
	}
	
	switch (sExecSlvVars.vendState) {
	
		case kVendGranted:
			return kCreditPermGranted;

		case kVendRejected:
		case kVendNone:																			// Should never happen
			return kCreditPermDenied;

		default:
			ExecSlaveCommCheck();
			if (sExecSlvVars.masterUnavail) {
				sExecSlvVars.vendState = kVendRejected;
			}
	}
	return kCreditPermWaiting;
}


/*--------------------------------------------------------------------------------------*\
Method:	CreditExecVendCompleted
	Called when the erogation of the previously requested selection	has been successful
	Send a positive acknowledge
\*--------------------------------------------------------------------------------------*/
void ExecSlaveVendCompleted(void) {
	
	if (sExecSlvVars.vendState == kVendGranted) {
		ExecSlaveTx(kExecACK);
		// In this state the master has been waiting for our response, so restart comm timer
		ExecSlaveCommTmrStart();
	}
	sExecSlvVars.vendState = kVendNone;
}

/*--------------------------------------------------------------------------------------*\
Method:	CreditExecVendFailed
	Must be called when the erogation of the previously requested selection	has FAILED
	Send a vend failed indication
\*--------------------------------------------------------------------------------------*/
void ExecSlaveVendFailed(void) {
	
	if (sExecSlvVars.vendState == kVendGranted) {
		ExecSlaveTx(kExecVMCVReplyVendFailed);
		// In this state the master has been waiting for our response, so restart comm timer
		ExecSlaveCommTmrStart();
	}
	sExecSlvVars.vendState = kVendNone;
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveVendEnable
	Enable/disable vends

Parameters:
	inEnableVend	->	if true enable vends
\*--------------------------------------------------------------------------------------*/
void ExecSlaveVendEnable(bool inEnableVend) {
	
	if (inEnableVend) {
		sExecSlvVars.vendEnabled = true;
		//ExecSlaveCommTmrStart();																// Tolto il 24.04.16 perche' non permette il detect del nolink
	}
	else {
		sExecSlvVars.vendEnabled = false;
		sExecSlvVars.vendState = kVendNone;
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveMaxCredit
	Determine maximum credit value representable with current protocol options.

Returns:
	maximum representable credit	
\*--------------------------------------------------------------------------------------*/
CreditValue ExecSlaveMaxCredit(void) {
	
	return sExecSlvVars.cd.scalingFact*(kExec1stReserved-1);
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveVMCDataInd
	Rx data from Master Executive (funzione chiamata ed eseguita in IRQ)
	
Parameters:
	data	->	character received
\*--------------------------------------------------------------------------------------*/
static void ExecSlaveVMCDataInd(uint8_t data)
{
	
	uint8_t reply = kExecACK;
	
	if(ExecCharIsCmd(data) == false) {
		// Ricevo un byte di dato
		// Note: sExecSlvVars.cd.decimalPoint is used as a temporary location when
		// computing the scaling factor, so that sExecSlvVars.cd.scalingFact is always valid 
		data&= kExecDataMask;
		switch(sExecSlvVars.dataCount++) {
			case 0:	sExecSlvVars.cd.baseUnitValue= data;								break;
			case 1: sExecSlvVars.cd.baseUnitValue+= data*16;							break;
			case 2: sExecSlvVars.cd.baseUnitValue+= data*256;							break;
			case 3: sExecSlvVars.cd.baseUnitValue+= data*256*16;						break;
			case 4:	sExecSlvVars.cd.decimalPoint= data;									break;
			case 5: sExecSlvVars.cd.scalingFact=data*16+sExecSlvVars.cd.decimalPoint;	break;
			case 6:	sExecSlvVars.cd.decimalPoint= data;									break;
			case 7:	sExecSlvVars.cd.exactChange= data;									break;
		}
		ExecSlaveTx(kExecACK);
		return;
	}

	// Ricevo un Comando dal Master
	switch (data & kExecDataMask) {
	
		//  ------- 0x31 S T A T U S  -------
		case kExecVMCCmdStatus:
			sExecSlvVars.commActivity = true;													// Serve come trigger al timeout NoLink
			if (sExecSlvVars.vendState == kVendRqSent ||
			   sExecSlvVars.vendState == kVendRq && ++sExecSlvVars.statusCnt > kProtExecSlaveMaxStatNoCredit) {
				// Ricevere Status in VendReq equivale a Vend Denied
				sExecSlvVars.vendState = kVendRejected;
			}
			if (!sExecSlvVars.vendEnabled) {
				// Vends are disabled
				reply|= kExecVMCStatVendInhibit;
			}
			else if (sExecSlvVars.vendState == kVendRq && sExecSlvVars.priceCode == 0) {
				reply |= kExecVMCStatFreeVendRq;												// Free Vend Request da richiedere al Master
			}
			break;

		//  ------- 0x32  C R E D I T  -------
		case kExecVMCCmdCredit:
			if(sExecSlvVars.vendState == kVendRq) {
				reply = sExecSlvVars.priceCode;
				sExecSlvVars.vendState = kVendRqSent;
			}
			else reply= kExecNoVendRq;
			break;

		//  ------- 0x33  V E N D    -------
		case kExecVMCCmdVend:
			if(sExecSlvVars.vendState == kVendRqSent) {
				sExecSlvVars.vendState = kVendGranted;											// Risposta al termine della selezione
				return;
			} else {																			// A un comando sconosciuto rispondo con Vend Fail
				reply |= kExecVMCVReplyVendFailed;
			}
			break;

		case kExecVMCCmdAudit: 																	// Non implementato
		case kExecVMCCmdSendAuditAddr:															// Non implementato
		case kExecVMCCmdSendAuditData:															// Non implementato
		case kExecVMCCmdIdentify:																// Non implementato
			break;

		//  ------- 0x38  A C C E P T   D A T A -------
		case kExecVMCCmdAcceptData:
			sExecSlvVars.dataCount = 0;
			break;

		//  ------- 0x39    S Y N C   -------
		case kExecVMCCmdDataSync:
			if (sExecSlvVars.dataCount >= 8) {
				sExecSlvVars.cdChanged = true;													// Ricevuta una nuova visualizzata, aggiorno la struttura cd
			}
			break;

		//  ------- 0x3F    N A C K   -------
		case kExecVMCCmdNACK:
			// retransmit last character sent
			reply= sExecSlvVars.lastCharSent;
			break;

		default:
			break;																				// Comando sconosciuto, rispondo comunque ACK (0x00) (Ref1p18)
	}

	ExecSlaveTx(reply);
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveASUDataInd
	Rx data indication for Audit Storage Unit
Parameters:
	data	->	character received
\*--------------------------------------------------------------------------------------*/
#if kProtExecSlaveAuditUnitSupport
static void ExecSlaveASUDataInd(char data) {
	uint8_t reply= kExecACK;

	if(!ExecCharIsCmd(data)) {
		// This is a data byte.
		ExecAuditPrivEl* ade= &sExecSlvVars.ad.data[sExecSlvVars.ad.putIx];
		data&= kExecDataMask;
		switch(sExecSlvVars.auditDataCount++) {
			case 0:	ade->addr= data;		break;
			case 1: ade->addr+=data*16;		break;
			case 2: ade->value= data;		break;
			case 3: ade->value+= data*16;	break;
		}
		ExecSlaveTx(kExecACK);
		return;
	}

	// This is a command byte. Decode it
	switch(data&kExecDataMask) {
		case kExecASUCmdStatus:
			sExecSlvVars.auditDataCount= 0;
			sExecSlvVars.commActivity= true;
			if(sExecSlvVars.identifyReq) {
				reply|= kExecASUReplyIdentifyReqMask;
				sExecSlvVars.identifyReq= false;
			}
			break;

		case kExecASUCmdDataSync:
			if(sExecSlvVars.auditDataCount==4) {
				// All the requested audit data have been received. Notify user
				uint8_t putIx= sExecSlvVars.ad.putIx+1;
				if(putIx>=nelof(sExecSlvVars.ad.data))	putIx= 0;
				if(putIx==sExecSlvVars.ad.getIx) 	// Audit buffer full - retry later
					reply= kExecPNAK;
				else 							// Update buffer content
					sExecSlvVars.ad.putIx= putIx;
			}
			sExecSlvVars.auditDataCount= 0;
			break;

		case kExecVMCCmdNACK:
			// retransmit last character sent
			reply= sExecSlvVars.lastCharSent;
			break;

		default:
			// Unknown command: reply with 0 (Ref1p18)
			break;
	}

	ExecSlaveTx(reply);

}
#endif

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveDataInd
	IRQ di ricezione carattere dal Master.

	IN:	 - Byte ricevuto dal Master
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  ExecSlaveDataInd(uint8_t data) {
	
	switch(data&kExecPeripheralIdMask) {
	
		case kExecPeripheralIdVMC:
			ExecSlaveVMCDataInd(data);
			break;
			
		case kExecPeripheralIdAU:
			#if kProtExecSlaveAuditUnitSupport
			ExecSlaveASUDataInd(data);
			#elif kProtExecSlaveSimulAuditUnit
			ExecSlaveTx(kExecACK);
			#endif
			break;
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveErrInd
	Errore di ricezione dalla RR: se il byte e' indirizzato al VMC rispondo PNACK.

	IN:	 - 
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  ExecSlaveErrInd(uint8_t data) {
	
	switch(data&kExecPeripheralIdMask) {
	
		case kExecPeripheralIdVMC:
		#if kProtExecSlaveAuditUnitSupport || kProtExecSlaveSimulAuditUnit
	
		case kExecPeripheralIdAU:
		#endif
		
			ExecSlaveTx(kExecPNAK);
			break;
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveUpdateCreditInfo
	Aggiorna struttura ci tramite struttura cd se quest'ultima e' stata modificata
	da una nuova visualizzazione dal Master.

	IN:	  - struttura "sExecSlvVars.cd" + sExecSlvVars.cdChanged + sExecSlvVars.dataCount
	OUT:  - TRUE e aggiornamento struttura "sExecSlvVars.ci", DecimalPointPosition e
			UnitScalingFactor se ricevuto nuovo credito, altrimenti solo return FALSE
\*--------------------------------------------------------------------------------------*/
static void ExecSlaveUpdateCreditInfo(void) {
	
	if (sExecSlvVars.cdChanged == false) return;												// Credito invariato
	if (sExecSlvVars.dataCount < 8) return;														// Si stanno ricevendo nuovi dati di visualizzazione

	sExecSlvVars.ci.creditVal = sExecSlvVars.cd.baseUnitValue*sExecSlvVars.cd.scalingFact;
	sExecSlvVars.ci.exactChange = sExecSlvVars.cd.exactChange;
	if (sExecSlvVars.cd.decimalPoint == 0x2) sExecSlvVars.ci.dpPosition = 1;
	else if (sExecSlvVars.cd.decimalPoint == 0x4) sExecSlvVars.ci.dpPosition = 2;
	else if (sExecSlvVars.cd.decimalPoint == 0x8) sExecSlvVars.ci.dpPosition = 3;
	else sExecSlvVars.ci.dpPosition = 0;
	DecimalPointPosition = sExecSlvVars.ci.dpPosition;
	UnitScalingFactor = sExecSlvVars.cd.scalingFact;

	VMC_CreditoDaVisualizzare = sExecSlvVars.ci.creditVal;										// Aggiunto il 22.04.16
	if (sExecSlvVars.ci.exactChange) {
		Set_fExactChange;
	} else {
		Clear_fExactChange;
	}
}


/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveCommCheck
	Check communication activity.
	Call hook procedure when master becomes available or unavailable
\*--------------------------------------------------------------------------------------*/
static void ExecSlaveCommCheck(void) {

	#if	kProtExecSlaveReplyDelay
	// Check if a pending char to send
	if(sExecSlvVars.sendIt && TmrTimeout(sExecSlvVars.delayReplyTmr)) {
		SendCharToRR(sExecSlvVars.lastCharSent);
		sExecSlvVars.sendIt= false;
	}
	#endif

	if (sExecSlvVars.commActivity) {
		// Line activity detected. Restart timeout
		sExecSlvVars.commActivity = false;
		ExecSlaveCommTmrStart();
		if (sExecSlvVars.masterUnavail) {
			sExecSlvVars.masterUnavail = false;
			Clear_fVMC_NoLink;
		}
	}
	else {
		// No line activity timeout detected only if Vend None state.
		// Vend Request and/or Vend states could be take a long time (based on Exec Master). 
		if (sExecSlvVars.vendState == kVendNone) {
			if (IsSlaveTxSuspendStatus() == FALSE) {											// Non controllo timeout NoLink quando il VMC2 e' in vend
				if (TmrTimeout(sExecSlvVars.commTmr)) {
					// No line activity and timeout elapsed. Declare master unavailable.
					if (!sExecSlvVars.masterUnavail) {
						sExecSlvVars.masterUnavail = true;
						sExecSlvVars.haveCreditData = false;
						Set_fVMC_NoLink;
					}
				}
			}
		}
        else
			ExecSlaveCommTmrStart();
	}
}


/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveTx
	Send a character

Parameters:
	ch	->	char to send
\*--------------------------------------------------------------------------------------*/
static void ExecSlaveTx(char ch) {
	
	sExecSlvVars.lastCharSent= ch;																	// Remember character in case it has to be retransmitted
	
#if	kProtExecSlaveReplyDelay
	sExecSlvVars.sendIt= true;
	TmrStart(sExecSlvVars.delayReplyTmr, kProtExecSlaveReplyDelay);
//	{
//		uint16_t replyDelay= PriceValueGet(5, 0);
//		if(replyDelay) {
//			sExecSlvVars.sendIt= true;
//			TmrStart(sExecSlvVars.delayReplyTmr, replyDelay-1);
//		}
//		else SendCharToRR(ch);
//	}
#else
	SendCharToRR(ch);
#endif
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveCommTmrStart
	Restart communication timer
\*--------------------------------------------------------------------------------------*/
static void ExecSlaveCommTmrStart(void) {
	
	TmrStart(sExecSlvVars.commTmr, TenSec);
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlaveCvtPrice
	Convert a price to ExecSlave price format
	
Parameters:
	price	->	price to convert
Return:
	ExecSlave representation of the price
	If the price is not representable correctly, returns kExecNoVendRq
\*--------------------------------------------------------------------------------------*/
static uint8_t ExecSlaveCvtPrice(CreditValue price) {
	
	uint16_t cvtdPrice = price/sExecSlvVars.cd.scalingFact;
	
	if (price == kCreditValueInvalid || cvtdPrice >= kExec1stReserved) {
		return kExecNoVendRq;
	}
	return (uint8_t)cvtdPrice;
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlave_VendSuccess
	
\*--------------------------------------------------------------------------------------*/
void  ExecSlave_VendSuccess(void) {

	//EVAAuditPaidVendInd(CreditCurrSelCode(), CreditCurrSelPrice(),0);
	
	sExecSlvVars.VendInProgress = false;
	if (!sExecSlvVars.waitingRealVendEnd) ExecSlaveVendCompleted();
	else ExecSlaveVendEnable(sExecSlvVars.vendEnabled);
	sExecSlvVars.waitingRealVendEnd = false;
}

/*--------------------------------------------------------------------------------------*\
Method:	ExecSlave_VendFail
	
\*--------------------------------------------------------------------------------------*/
void  ExecSlave_VendFail(void) {
	
	sExecSlvVars.VendInProgress = false;
	if (!sExecSlvVars.waitingRealVendEnd) ExecSlaveVendFailed();
	else ExecSlaveVendEnable(sExecSlvVars.vendEnabled);
	sExecSlvVars.waitingRealVendEnd = false;
}

/*--------------------------------------------------------------------------------------*\
Method:	SendCharToRR
	Trasmette subito il chr se non e' Master/Slave, altrimenti la trasmissione alla RR e'
	gestita dal MST/SLV.

	IN:	 - 
	OUT: -
\*--------------------------------------------------------------------------------------*/
static void  SendCharToRR(uint8_t data) {
	
	if (gVMC_ConfVars.ExecMSTSLV == FALSE) {													// VMC in Slave normale (No MST/SLV)
		ExecSlaveTxChar(data);																	// Trasmette sulla SCI Exec il chr di risposta alla RR Executive
	} else {
		VMC_Locale_Reply(data);																	// Risposta gestita dal MST/SLV
	}
}

