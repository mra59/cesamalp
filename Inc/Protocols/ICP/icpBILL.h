/****************************************************************************************
File:
    icpBILL.h

Description:
    Include file protocollo protocollo MDB per bill validator

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __icpBILL__
#define __icpBILL__

#include "Icp.h"


/*--------------------------------------------------------------------------------------*\
Types and constants
\*--------------------------------------------------------------------------------------*/

#define ICP_MAX_BILLTYPES	16

// Feature options
typedef enum  {
	ICP_BILLOPT_NONE = 0x00,
	ICP_BILLOPT2_FTL = 0x01
} ET_BILLFOPTIONS;

// Status events
typedef enum {
	ICP_BILL_DEFECTIVEMOTOR = 1,
	ICP_BILL_SENSORERROR,
	ICP_BILL_BUSY,
	ICP_BILL_ROMCHKERROR,
	ICP_BILL_VALIDATORJAM,
	ICP_BILL_JUSTRESET,
	ICP_BILL_BILLREMOVED,
	ICP_BILL_CASHBOXOPEN,
	ICP_BILL_UNITDISABLED,
	ICP_BILL_INVALIDESCROWREQ,
	ICP_BILL_BILLREJECTED,
	ICP_BILL_NINPUTBILL
} ET_BILLPOLLSTATUS;

// Coin destinations
typedef enum {
	ICP_BILL_STACKED = 0,
	ICP_BILL_ESCROWPOS,
	ICP_BILL_RETURNED,
	ICP_BILL_NOTUSED,
	ICP_BILL_DISBILLREJECTED
} ET_BILLPOLLDEST;


/*--------------------------------------------------------------------------------------*\
Interface routines
\*--------------------------------------------------------------------------------------*/

void ICPProt_BILLSetStatus(ET_BILLPOLLSTATUS statusType);
	// Called to signal billvalidator states to ICP protocol, see ET_BILLPOLLSTATUS

void ICPProt_BILLSetBillsDeposited(uint8_t nBillType, ET_BILLPOLLDEST nDestType);
	// Called to signal bill acceptance conditions to ICP protocol, see ET_BILLPOLLDEST


void ICPBILLHook_Init(void);
	// Called at reception of the Reset command.

void ICPBILLHook_EscrowBill(bool fStackBill);
	// Called at reception of the Escrow command (stack or return the bill in escrow position).


bool ICPBILLHook_IsAvailable(void);
	// Return true if Bill Validator peripheral is used

bool ICPBILLHook_IsInitCompleted(void);
	// Return true when initialisation is completed

bool ICPBILLHook_IsStackerFull(void);
	// Return true to indicate stacker full condition

uint8_t ICPBILLHook_GetBillsInStacker(void);
	// Return the total bills deposited into the stacker

CreditCoinValue ICPBILLHook_GetBillTypeVal(uint8_t nBillType);
	// Return the value of bill type specified

uint16_t ICPBILLHook_GetScalingFactor(void);
	// Return the scaling factor used to scale all monetary values transactions

uint8_t ICPBILLHook_GetDecimalPoint(void);
	// Return the decimal places used to comunicate all monetary values transactions

uint16_t ICPBILLHook_GetStackerSize(void);
	// Return the number of bills that the stacker will hold

uint16_t ICPBILLHook_GetSecurityLevel(void);
	// Return the security level for bill types.

bool ICPBILLHook_GetEscrowCapacity(void);
	// Return true if the escrow capacity is available.

uint8_t ICPBILLHook_GetFeatureLevel(void);
	// Return bill validator feature level

uint16_t ICPBILLHook_GetCountryCode(void);
	// Return bill validator country code

uint8_t ICPBILLHook_GetMaxNoResponse(void);
	// Return the maximum time the bill validator provide a response to any command from the Vmc

ET_BILLFOPTIONS ICPBILLHook_GetFeatures(void);
	// Return bill validator miscellaneous options: ftl

void ICPBILLHook_SetSecurityLevel(uint16_t nSecurityMask);
	// Called to set the security level of bills (1=high level, 0=low level)

void ICPBILLHook_SetBillEnable(uint8_t nBillType, bool fEnable);
	// Called to enable/disable the acceptance of bill type specified

void ICPBILLHook_SetEscrowEnable(uint8_t nBillType, bool fEnable);
	// Called to enable/disable the escrow for bill type specified

bool ICPProt_BILLGetFTL(void);

uint16_t ICPProt_BILLGetBillValue(uint8_t ix);

void  Set_Bill_Info_Data(uint8_t ix, uint8_t l, uint8_t *point);
void  Set_Bill_SwRev_Data(uint16_t Bill_Rev);


#endif