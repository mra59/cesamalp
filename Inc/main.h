/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/

	#define LCD_DB0_Pin GPIO_PIN_0
	#define LCD_DB0_GPIO_Port GPIOE
	#define LCD_DB1_Pin GPIO_PIN_1
	#define LCD_DB1_GPIO_Port GPIOE
	#define LCD_DB2_Pin GPIO_PIN_2
	#define LCD_DB2_GPIO_Port GPIOE
	#define LCD_DB3_Pin GPIO_PIN_3
	#define LCD_DB3_GPIO_Port GPIOE
	#define LCD_RS_Pin GPIO_PIN_4
	#define LCD_RS_GPIO_Port GPIOE
	#define LCD_E_1_Pin GPIO_PIN_5
	#define LCD_E_1_GPIO_Port GPIOE
	#define LED_B_Pin GPIO_PIN_6
	#define LED_B_GPIO_Port GPIOE
	#define LED_V_Pin GPIO_PIN_7
	#define LED_V_GPIO_Port GPIOE
	#define Input_9_Pin GPIO_PIN_8
	#define Input_9_GPIO_Port GPIOE
	#define Input_10_Pin GPIO_PIN_9
	#define Input_10_GPIO_Port GPIOE
	#define GPIO_1_Pin GPIO_PIN_10
	#define GPIO_1_GPIO_Port GPIOE
	#define FLUSS_1_Pin GPIO_PIN_11
	#define FLUSS_1_GPIO_Port GPIOE
	//#define	LCD_BUSY_Pin		LCD_DB3_Pin
	//#define	LCD_BUSY_GPIO_Port	GPIOE

	#define GPIO_0_Pin GPIO_PIN_13
	#define GPIO_0_GPIO_Port GPIOC
	#define Current_Pin GPIO_PIN_6
	#define Current_GPIO_Port GPIOA
	#define V_Input_Pin GPIO_PIN_7
	#define V_Input_GPIO_Port GPIOA
	#define GP_AN_0_Pin GPIO_PIN_0
	#define GP_AN_0_GPIO_Port GPIOB
	#define LED_Y_Pin GPIO_PIN_1
	#define LED_Y_GPIO_Port GPIOB
	#define GPIO_2_Pin GPIO_PIN_2
	#define GPIO_2_GPIO_Port GPIOB
	#define PB10_Pin 	GPIO_PIN_10
	#define PB10_Port 	GPIOB
	#define LED_R_Pin 	GPIO_PIN_11
	#define LED_R_GPIO_Port GPIOB
	#define InhGett_2_Pin GPIO_PIN_12
	#define InhGett_2_GPIO_Port GPIOB
	#define GPIO_3_Pin GPIO_PIN_15
	#define GPIO_3_GPIO_Port GPIOB
	#define MIF_2_PWS_ENA_Pin GPIO_PIN_8
	#define MIF_2_PWS_ENA_GPIO_Port GPIOD
	#define MIF_FAULT_Pin GPIO_PIN_9
	#define MIF_FAULT_GPIO_Port GPIOD
	#define GtwyRES_Pin GPIO_PIN_11
	#define GtwyRES_GPIO_Port GPIOD
	#define GtwyBOOT_Pin GPIO_PIN_10
	#define GtwyBOOT_GPIO_Port GPIOD
	#define RS485_DE_Pin GPIO_PIN_12
	#define RS485_DE_GPIO_Port GPIOD
	#define PL165_Pin GPIO_PIN_13
	#define PL165_GPIO_Port GPIOD
	#define OE595_Pin GPIO_PIN_14
	#define OE595_GPIO_Port GPIOD
	#define PD15_Pin GPIO_PIN_15
	#define PD15_Port GPIOD
	#define IRDA_ENA_Pin GPIO_PIN_7
	#define IRDA_ENA_GPIO_Port GPIOC
	#define InhGett_1_Pin GPIO_PIN_8
	#define InhGett_1_GPIO_Port GPIOC
	#define EEWP_Pin GPIO_PIN_9
	#define EEWP_GPIO_Port GPIOC
	#define USB_PWR_ENA_Pin GPIO_PIN_8
	#define USB_PWR_ENA_GPIO_Port GPIOA
	#define VDD_USB_Pin GPIO_PIN_9
	#define VDD_USB_GPIO_Port GPIOA
	#define GP_USART3_Tx_Pin GPIO_PIN_10
	#define GP_USART3_Tx_GPIO_Port GPIOC
	#define GP_USART3_Rx_Pin GPIO_PIN_11
	#define GP_USART3_Rx_GPIO_Port GPIOC
	#define MIF_1_PWS_ENA_Pin GPIO_PIN_0
	#define MIF_1_PWS_ENA_GPIO_Port GPIOD
	#define LCD_RESET_Pin GPIO_PIN_6
	#define LCD_RESET_GPIO_Port GPIOD
	#define LCD_DB3_Busy_Pin GPIO_PIN_7
	#define LCD_DB3_Busy_GPIO_Port GPIOD
	#define LCD_E_2_Pin GPIO_PIN_3
	#define LCD_E_2_GPIO_Port GPIOB
	#define PROG_Pin GPIO_PIN_4
	#define PROG_GPIO_Port GPIOB
	#define STB595_Pin GPIO_PIN_5
	#define STB595_GPIO_Port GPIOB



/* USER CODE BEGIN Private defines */


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
