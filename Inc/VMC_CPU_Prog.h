/****************************************************************************************
File:
    VMC_CPU_Prog.h

Description:
    Include file con definizioni e strutture per gestire la programmazione del VMC.

History:
    Date       Aut  Note
    Set 2016	MR   

 *****************************************************************************************/

#ifndef __VMC_CPU_Prog_H
#define __VMC_CPU_Prog_H


/*--------------------------------------------------------------------------------------*\
Types definition
\*--------------------------------------------------------------------------------------*/

#define	 MAX_NUM_COINTYPES		16

#define	 Tasto_Enter			0x00
#define	 Tasto_Zero				0x00
#define	 Tasto_Uno				0x01
#define	 Tasto_Due				0x02
#define	 Tasto_Nove				0x09
#define	 Tasto_NextMenu			Tasto_Piu				// 254	0xfe
#define	 Tasto_PrevMenu			Tasto_Meno				// 253	0xfd
#define	 NoTastoPremuto			Tasto_NessunTasto		// 255	0xff

#define	Row_1					1
#define	Row_2					2
#define	Row_3					3
#define	Row_4					4

//-------------------------------------------------------
// --- Minimo - Massimo Parametri ----

#define	Min_CodiceMacchina		0
#define	Max_CodiceMacchina		99999999
#define	Min_DelayRispEnerg		0
#define	Max_DelayRispEnerg		65000
#define	MinPrezzo				0
#define	MaxPrezzo				65535
#define	MinSconto				0
#define	MaxSconto				65000
#define	NumeroMonete			6					// Numero monete selettore parallelo
#define	MinCoinVal				1
#define	MaxCoinVal				1000
#define	Min_DPP					0
#define	Max_DPP					3
#define	MIN_NUMPRICE			1
#define	MAX_NUMPRICE			99
#define	Min_Deconto				0
#define	Max_Deconto				10000
#define	Min_RiservaDeconto		0
#define	Max_RiservaDeconto		10000

#define	Min_uint16				0
#define	Max_uint16				0xffff
#define	NUMCIFRE_COD_MACCH		8					// Numero di cifre del Codice Macchina
#define	NUMCIFRE_SERIALNUMB		10

#define	NoValuta				false
#define	SiValuta				true

#define	MIN_DATYPE				1
#define	MAX_DATYPE				2

#define	MIN_TASTIERA			0
#define	MAX_TASTIERA			3

#define	MIN_LCD_TYPE			0
#define	MAX_LCD_TYPE			1

#if (DBG_POCA_ACQUA == true)
	#define	MIN_FLOW_CNT			1
#else
	#define	MIN_FLOW_CNT			100
#endif

#define	MAX_FLOW_CNT			10000
#define	MIN_EVNUM				1
#define	MAX_EVNUM				4
#define	MIN_FLUSSNUM			1
#define	MAX_FLUSSNUM			4
#define	MIN_TEMPERVENTOLA		30
#define	MAX_TEMPERVENTOLA		50
#define	MIN_OZONO_DELAY			0
#define	MAX_OZONO_DELAY			1440				// 24 ORE
#define	MIN_OZONO_ON			0
#define	MAX_OZONO_ON			60					// 1 ORA
#define	MIN_SIRENA_DELAY		0
#define	MAX_SIRENA_DELAY		30					// 30 minuti
#define	MIN_SIRENA_ON			1
#define	MAX_SIRENA_ON			30					// 30 minuti


#define	MIN_TIME_SEL			1
#define	MAX_TIME_SEL			1000
#define	MIN_FLOW_SEL			1
#define	MAX_FLOW_SEL			9999
#define	MIN_OUTPUT				0
#define	MAX_OUTPUT				12

#define	MIN_VALUTA				1008				// Valore minimo: Lek albanese (008 + 1000 per indicare tipo valuta in EVA-DTS)
#define	MAX_VALUTA				1998				// Valore max: dollaro USA (998 + 1000)

#define	MIN_NUMERO_SEL			1
#define	MAX_NUMERO_SEL			4



//-------------------------------------------------------


#define	PSW_NumCifre			4					// Numero di cifre che compongono la password
#define	NumeroDiPSW				3					// Numero di password utilizzate dal VMC
#define	AccLevel_0				1
#define	AccLevel_1				2
#define	AccLevel_2				3

#define		FILTER_CLEAN		250

#define		Selez_01  01
#define		Selez_02  02
#define		Selez_03  03
#define		Selez_04  04
#define		Selez_05  05
#define		Selez_06  06
#define		Selez_07  07
#define		Selez_08  0x08
#define		Selez_09  0x09
#define		Selez_10  10
#define		Selez_11  11
#define		Selez_12  12
#define		Selez_13  13
#define		Selez_14  14
#define		Selez_15  15
#define		Selez_16  16
#define		Selez_21  21
#define		Selez_22  22
#define		Selez_23  23
#define		Selez_24  24
#define		Selez_25  25
#define		Selez_26  26
#define		Selez_31  31
#define		Selez_32  32
#define		Selez_33  33
#define		Selez_34  34
#define		Selez_35  35
#define		Selez_36  36
#define		Selez_41  41
#define		Selez_42  42
#define		Selez_43  43
#define		Selez_44  44
#define		Selez_45  45
#define		Selez_46  46
#define		Selez_51  51
#define		Selez_52  52
#define		Selez_53  53
#define		Selez_54  54
#define		Selez_55  55
#define		Selez_56  56
#define		Selez_61  61
#define		Selez_62  62
#define		Selez_63  63
#define		Selez_64  64
#define		Selez_65  65
#define		Selez_66  66

//MR19 #define	  	FirstSelez  		1				Defines in VMC_CPU_Tasti.h
//MR19 #define	  	LastSelez  		MAXNUMSEL-1


//********************************************************************************
//********************************************************************************
//********************************************************************************
// !!!! NON CAMBIARE ORDINE DEI MENU !!!!!

#if CESAM
	  typedef enum {
		  Pr_AskPSWV,						// 0
		  Pr_Menu_TempiDosi,				// 1			Livelli 0 - 1 - 2
		  //Pr_Menu_PrezziSel,				// 2			Livelli 0 - 1 - 2
		  Pr_Menu_Audit,					// 3			Livelli 0 - 1 - 2
		  Pr_Menu_FasceOrarie,				// 4			Livelli 0 - 1 - 2
		  Pr_Menu_SystPagam,				// 5			Livelli 1 - 2
		  //Pr_Menu_ScontiSel,				// 6			Livelli 1 - 2
		  //Pr_Menu_AssociazSelPrez,			// 7			Livelli 1 - 2
		  //Pr_Menu_SettaggiFabbrica,			// 8			Livelli 1 - 2
		  Pr_Menu_OpzVarie,					// 9			Livelli 1 - 2
		  Pr_Menu_ImpostazProduttore,		// 10			Livelli 2
		  NoMoreMenu
	  } MainMenuSTAT;

	#define	FirstMenu_Liv_0		Pr_Menu_TempiDosi
	#define	LastMenu_Liv_0		Pr_Menu_Audit

	#define	FirstMenu_Liv_1		Pr_Menu_TempiDosi
	#define	LastMenu_Liv_1		Pr_Menu_OpzVarie

	#define	FirstMenu_Liv_2		Pr_Menu_TempiDosi
	#define	LastMenu_Liv_2		Pr_Menu_ImpostazProduttore

#else

	  typedef enum {
		  Pr_AskPSWV,						// 0
		  Pr_Menu_TempiDosi,				// 1			Livelli 0 - 1 - 2
		  Pr_Menu_PrezziSel,				// 2			Livelli 0 - 1 - 2
		  //Pr_Menu_Temperature,			// 3			Livelli 0 - 1 - 2
		  Pr_Menu_Audit,					// 4			Livelli 0 - 1 - 2
		  //Pr_Menu_Lavaggi,				// 5			Livelli 0 - 1 - 2
		  Pr_Menu_FasceOrarie,				// 6			Livelli 0 - 1 - 2
		  Pr_Menu_SystPagam,				// 7			Livelli 1 - 2
		  Pr_Menu_ScontiSel,				// 8			Livelli 1 - 2
		  Pr_Menu_AssociazSelPrez,			// 9			Livelli 1 - 2
		  Pr_Menu_SettaggiFabbrica,			// 10			Livelli 1 - 2
		  Pr_Menu_OpzVarie,					// 11			Livelli 1 - 2
		  Pr_Menu_ImpostazProduttore,		// 12			Livelli 2
		  Pr_Menu_Collaudo					// 13			Livello 2
	  } MainMenuSTAT;

	  #define	FirstMenu_Liv_0		Pr_Menu_TempiDosi
	  #define	LastMenu_Liv_0		Pr_Menu_FasceOrarie

	  #define	FirstMenu_Liv_1		Pr_Menu_TempiDosi
	  #define	LastMenu_Liv_1		Pr_Menu_OpzVarie

	  #define	FirstMenu_Liv_2		Pr_Menu_TempiDosi
	  #define	LastMenu_Liv_2		Pr_Menu_Collaudo

#endif

typedef enum {
	Paym_Executive,
	Paym_ExecRestoSubito,
	Paym_ExecPriceHolding,
	Paym_ExecPriceDisplay,
	Paym_ExecMasterSlave,
	Paym_Mifare,
	Paym_Mif_Pin1,
	Paym_Mif_Pin2,
	Paym_Mif_Gest,
	Paym_MaxCard,
	Paym_EnableDiscount,
	Paym_CoinVal,
	Paym_MaxCash,
	Paym_Gratuito,
	Paym_MDB,
	Paym_MDB_SLV,
	Paym_MDB_MultiVend,
	Paym_MDB_MaxCash,
	Paym_MDB_MaxChange,
	Paym_MDB_CambiaMonete,
	Paym_MDBRestoSubito,
	Paym_MDB_BillEnaWithCard,
	Paym_MDB_EnableDiscount,
	Paym_MDBCoinEna,
	Paym_MDBBillEna
} MenuPaymSTAT;

typedef enum {
	Opt_CambioLingua,
	Opt_DPP,								// DPP
	TipoValuta,								// VALU
	Opt_SN_Produttore,
	Opt_Spirali,
	Opt_Tastiera,
	Opt_NumRotazMot,
	Opt_ClearEE,							// Azzeramento memoria
	Opt_EnaDeconto,							// Opzione abilitazione deconto
	Val_DecontoAttuale,						// Valore attuale counter deconto
	Val_InizialeDeconto,					// Valore iniziale counter deconto
	Val_RiservaDeconto,						// Valore di riserva counter deconto
	Opt_ResetDeconto,						// Opzione ricarica deconto per togliere l'allarme dal display
	Opt_SetOrologio,						// Programmazione orologio

	Opt_EnaRisparmioEnerg,					// Opzione abilitazione risparmio energetico
	Opt_DelayTemperRisparmioEnerg,			// Attesa dopo ultima selezione per impostare la temperatura di risparmio energetico
	Opt_TemperRisparmioEnerg,				// Temperatura di risparmio energetico
	
	Opt_Ena_Luci_ON,						// Opzione abilitazione fasce ON luci
	Opt_ImpostaFasceLuciRequest,			// Richiesta Impostazione fasce Luci per ogni giorno della settimana
	Opt_ImpostaFasceONLuci,					// Impostazione fasce luci ON per ogni giorno della settimana

	Opt_CodiceMacchina,
	Opt_CodiceLocazione,
	CPU_SERIAL_NUMBER,
	Audit_Battute_Costruttore,
	Opt_LCD_Remoto,
	Opt_LCD_16x2,
	Val_Volumetrico1,
	Val_Volumetrico2,
	Val_Volumetrico3,
	Val_Volumetrico4,
	Opt_NoPushbuttons,
	OUTNum_Erogaz_1,
	II_OUTNum_Erogaz_1,
	OUTNum_Erogaz_2,
	II_OUTNum_Erogaz_2,
	OUTNum_Erogaz_3,
	II_OUTNum_Erogaz_3,
	OUTNum_Erogaz_4,
	II_OUTNum_Erogaz_4,
	OUT_PuliziaFiltro,
	FlussNum_Erogaz_1,
	FlussNum_Erogaz_2,
	FlussNum_Erogaz_3,
	FlussNum_Erogaz_4,
	TemperaturaVentola,
	RemoteInh,
	Opt_Ozono,
	Opt_TimeActiveOzono,
	Opt_TimeWaitOzono,
	Opt_Sirena,
	Opt_TimeSirenaON,
	Opt_TimeDelaySirena,
	Opt_START_AUTOMATICO,
	Opt_SWITCH_PRODOTTO_1,
	Opt_SEL_INH_FINE_PROD_1,
	Opt_SWITCH_PRODOTTO_2,
	Opt_SEL_INH_FINE_PROD_2,
	Opt_MULTI_VEND_AUTOMATICO,
	Opt_MULTICICLO,
	Opt_Last
} MenuOptionSTAT;

typedef struct {
	MainMenuSTAT	MainStatus;				// Stato programmazione Menu principale di entrata
	MenuPaymSTAT	PaymentStatus;			// Stato programmazione Menu sistemi di pagamento
	MenuOptionSTAT	OptionsStatus;			// Stato programmazione Menu Opzioni Varie
	uint8_t			AccessLev;				// Livello di accesso alla programmazione
	uint8_t			NumSel;					// Selezione in programmazione
	uint8_t			NumSetCol;				// Numero del set di colonne da programmare
	const uint8_t	*pntselnum;				// Pointer alla tabella delle selezioni della macchina
	Tmr32Local    	ToutProgram;
} ProgramVars;

typedef struct {
	uint8_t		Msg;						// Numero del messaggio da visualizzare
	uint8_t		NumSel;						// Numero selezione nel messaggio da visualizzare
	uint8_t		Row;						// Riga sulla quale visualizzare il messaggio
	uint16_t	AddrData;					// Indirizzo del parametro da elaborare 
	uint16_t	MinData;					// Limite Minimo
	uint16_t	MaxData;					// Limite Massimo
	uint16_t	paramVal;					// Valore del parametro programmato
} tProgHex16;

typedef struct {
	uint8_t		Msg;						// Numero del messaggio da visualizzare
	uint8_t		NumSel;						// Numero selezione nel messaggio da visualizzare
	uint8_t		Row;						// Riga sulla quale visualizzare il messaggio
	uint16_t	AddrData;					// Indirizzo del parametro da elaborare 
	uint32_t	MinData;					// Limite Minimo
	uint32_t	MaxData;					// Limite Massimo
	uint32_t	paramVal;					// Valore del parametro programmato
} tProgHex32;

typedef struct {
	uint8_t		Msg;						// Numero del messaggio da visualizzare
	uint8_t		NumSel;						// Numero selezione nel messaggio da visualizzare
	uint8_t		Row;						// Riga sulla quale visualizzare il messaggio
	uint16_t	AddrData;					// Indirizzo del parametro da elaborare 
	uint8_t		MinData;					// Limite Minimo
	uint8_t		MaxData;					// Limite Massimo
	uint8_t		paramVal;					// Valore del parametro programmato
} tProgHex8;

typedef struct {
	uint8_t		Msg;						// Numero del messaggio da visualizzare
	uint8_t		NumSel;						// Numero selezione nel messaggio da visualizzare
	uint8_t		Row;						// Riga sulla quale visualizzare il messaggio
	uint16_t	AddrData;					// Indirizzo del parametro da elaborare 
	uint8_t		MinData;					// Limite Minimo
	uint8_t		MaxData;					// Limite Massimo
	uint8_t		paramVal;					// Valore del parametro programmato
} tProgBCD;

typedef struct {
	uint8_t		Msg;						// Numero del messaggio da visualizzare
	uint8_t		NumSel;						// Numero selezione nel messaggio da visualizzare
	uint8_t		Row;						// Riga sulla quale visualizzare il messaggio
	uint16_t	AddrData;					// Indirizzo del parametro da elaborare 
	uint8_t		MaskData;					// Bit da elaborare 
	uint16_t	MaskData16;					// Bit da elaborare in una word 
	uint8_t		paramVal;					// Valore del parametro programmato a 8 bit
	uint16_t	paramVal16;					// Valore del parametro programmato a 16 bit
} tProgBool;

typedef enum {
	kLocLCDCifraIntera, 
	kLocBitYN,           
	kLocCifraIntera,     
	kLocCifraDecimale,   
	kLocCifraPsw,        
	kLocCifraInputPsw,   
	kLocBitY,            
	kLocBitN,            
	kLocDivisoreMigliaia,
	kLocDivisoreDecimali,
	kLocTastoY,          				// Key for YES
	kLocTastoN,           				// Key for NO         

	kLocAttr_Count						// Number of localization attributes
} LocalAttrId;							// Localization attributes

//static const char sLocalizationDefault[]= "|||~-*YN.,YN";	// Default localization settings

#define		Fasce_ON_OFF	0
#define		Fasce_Luci		1
#define		MAX_NUM_FASCE	2


/*--------------------------------------------------------------------------------------*\
Tipi funzioni di programmazione
\*--------------------------------------------------------------------------------------*/

void   		Prog_Main(void);
static void MenuPrezzi(void);
static void MenuSconti(void);
static void MenuAudit(void);
static void MenuPayments(void);
static void MenuOpzioniVarie(void);
static void MenuImpostazioniProduttore(void);
static void MenuCollaudo(void);
static bool TD_SubMenu_SceltaSelez(void);
static bool GestTastiMainMenu(void);
static bool TD_SubMenu_CoinVal(void);	
static void MenuTabSelezPrezzo(void);
static void MenuPSW(void);
bool 		IsMenuCollaudo(void);

#if (CESAM == false)
	static void MenuTempiDosi(void);
	static void TD_SubMenu_ProgSelez(void);
	static void MenuRisparmio(void);
	static void MenuDefaultFabbrica(void);
#endif




#endif		// __VMC_CPU_Prog_H


