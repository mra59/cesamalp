/****************************************************************************************
File:
    icpCPC.h

Description:
    Include file protocollo MDB per cashless

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __icpCPC__
#define __icpCPC__

#include "Icp.h"


/*--------------------------------------------------------------------------------------*\
Types and constants
\*--------------------------------------------------------------------------------------*/

typedef enum {
	ICP_CPCPOLL01_NONE = 0x00,
	ICP_CPCPOLL01_REFUNDS = 0x01,
	ICP_CPCPOLL01_MULTIVEND = 0x02,
	ICP_CPCPOLL01_DISPLAY = 0x04,
	ICP_CPCPOLL01_CASHSALE = 0x08
} ET_CPCFOPTIONS;

typedef enum  {
	ICP_CPCPOLL03_NONE = 0x00,
	ICP_CPCPOLL03_DISPCREDIT = 0x01,
	ICP_CPCPOLL03_REFUND = 0x02,
	ICP_CPCPOLL03_REVALUENEGVEND = 0x04
} ET_CPCPOLL03OPT;

typedef enum  {
	ICP_CPCINFO_JUSTRESET =		0x0004,
	ICP_CPCINFO_DISPLAYREQ =	0x0008,
	ICP_CPCINFO_BEGINSESSION =	0x0010,
	ICP_CPCINFO_SESSCANCREQ =	0x0020,
	ICP_CPCINFO_VENDAPP =		0x0040,
	ICP_CPCINFO_VENDDENY =		0x0080,
	ICP_CPCINFO_ENDSESS =		0x0100,
	ICP_CPCINFO_CANCELLED =		0x0200,
	ICP_CPCINFO_PERIPHID =		0x0400,
	ICP_CPCINFO_REVALAPP =		0x0800,
	ICP_CPCINFO_REVALDENY =		0x1000,
	ICP_CPCINFO_MEDIAERR =		0x2000,
	ICP_CPCINFO_REFUNDERR =		0x4000,
	ICP_CPCINFO_DIAGxx =		0x8000
} ET_CPCEVENTINFO;

typedef enum {
	ICP_CPC_WAITRESP = 0,
	ICP_CPC_RESPAPP = 1,
	ICP_CPC_RESPDENY = 2
} ET_CPCRESPTYPES;

typedef enum {
	ICP_CPCMEDIA_NORMAL 				= 0x00,
	ICP_CPCUserGroupPriceListNumber		= 0x01,
	ICP_CPCUserGroupDiscountGroupIndex	= 0x02,
	ICP_CPCDiscountPercentageFactor		= 0x03,
	ICP_CPCSurchargePercentageFactor	= 0x04,
	ICP_CPCMEDIA_TEST 					= 0x40,
	ICP_CPCMEDIA_FREEVEND 				= 0x80
} ET_CPCMEDIATYPES;


/*--------------------------------------------------------------------------------------*\
Interface routines
\*--------------------------------------------------------------------------------------*/

void ICPProt_CPCSetEvtMask(uint16_t nEvtMask);
	// Called to signal reader events to ICP protocol, see ET_CPCEVENTINFO


// RM46_2006: PK Modifiche 19/10/2006
//#ifdef kModifMDB_KeyInOut
void ICPProt_CPCResetEvtMask(uint16_t nEvtMask);
bool ICPProt_CPCIsSetEvtMask(uint16_t nEvtMask);
//#endif


void ICPCPCHook_Init(void);
	// Called at reception of the Reset command.

uint8_t ICPCPCHook_VendRequest(CreditCpcValue nPrice, uint16_t nSelNumber, uint16_t nCCode, CreditCpcValue *ptVendAmount, uint16_t *ptVendCurrency);
	// Called to require a vend.
	// Return value: Vend deny or Vend Approved with VendAmount or no response ready at this moment

uint8_t ICPCPCHook_NegVendRequest(CreditCpcValue nPrice, uint16_t nSelNumber, uint16_t nCCode, CreditCpcValue *ptVendAmount, uint16_t *ptVendCurrency);
	// Called to require a negative vend (the price is added to the key).
	// Return value: Vend deny or Vend Approved with VendAmount or no response ready at this moment
	// (if "negative vend" option enabled)

uint8_t ICPCPCHook_RevalueRequest(CreditCpcValue nRevalue, uint16_t nCCode);
	// Called to require a revalue of credit on the key.
	// Return value: Revalue deny or Revalue Approved or no response ready at this moment

void ICPCPCHook_VendCancel(void);
	// Called to abort a vend request not yet approved or denied.

void ICPCPCHook_VendSuccess(uint16_t nSelNumber);
	// Called to signal that the vend session is completed successfully.

void ICPCPCHook_VendFailure(void);
	// Called to signal that the vend session is failed.
	// Try to refund the credit on the key.
	// (if "refund or revalue" option enabled)

void ICPCPCHook_VendComplete(void);
	// Called to signal that the vend session is completed
	// (the key must be re-inserted to start another vend session).

void ICPCPCHook_VendCashSale(CreditCpcValue nPrice, uint16_t nSelNumber, uint16_t nCCode);
	// Called to audit vend sessions...
	// (if "cash sale" option enabled)

void ICPCPCHook_ReaderEnable(bool fEnable);
	// Called to enable/disable the reader
	// (when the reader is disabled no key is accepted for vending)

void ICPCPCHook_ReaderCancel(void);
	// Called to abort reader activities at Enabled state (!?!)


bool ICPCPCHook_IsAvailable(void);
	// Return true if Cashless Payment peripheral is used

bool ICPCPCHook_IsInitCompleted(void);
	// Return true when initialisation is completed

uint8_t ICPCPCHook_GetFeatureLevel(void);
	// Return the feature level of the reader

uint16_t ICPCPCHook_GetCountryCode(void);
	// Return reader country code

uint8_t ICPCPCHook_GetScalingFactor(void);
	// Return the scaling factor used to scale all monetary values transactions

uint8_t ICPCPCHook_GetDecimalPoint(void);
	// Return the decimal places used to comunicate all monetary values transactions

uint8_t ICPCPCHook_GetMaxNoResponse(void);
	// Return the maximum time the reader provide a response to any command from the Vmc

ET_CPCFOPTIONS ICPCPCHook_GetFeatures(void);
	// Return reader miscellaneous options: refund, multivend, display, cash sale

void ICPCPCHook_GetRevalueLimit(CreditCpcValue *ptLimitAmount, uint16_t *ptCCode);
	// Called to get the maximum amount the key will accepts

void ICPCPCHook_GetMediaInfo(CreditCpcValue *ptFunds, uint32_t *ptPaymentID, \
							 uint8_t *ptPaymentType, uint16_t *ptPaymentData, \
							 uint16_t *ptUserLanguage, uint16_t *ptUserCurrency, uint8_t *ptUserOptions);
	// Called to get Media informations to send as BeginSession response.
	// ptUserOptions: see ET_CPCPOLL03OPT

void ICPCPCHook_GetVendAppAmount(CreditCpcValue *ptVendAmount, uint16_t *ptVendCurrency);
	// Called to get VendAmount approved.

void ICPCPCHook_GetDispInfo(uint8_t *ptMsg, uint8_t *ptMsgTime);
	// Called to get the "Message" that the reader needs/wants to display.
	// msg length = 32, msgTime = requested display time (0.1 second units)


void ICPCPCHook_SetMaxMinPrices(CreditCpcValue nMaxPrice, CreditCpcValue nMinPrice, uint16_t nCCode);
	// Called to save the price range of the Vmc

void ICPCPCHook_SetFTLEnable(bool fEnable);
	// Called to advise the reader that the "File Transport Layer" feature is available

void ICPCPCHook_SetCredit32Enable(bool fEnable);
	// Called to advise the reader that the "32 bit monetary format" feature is using

void ICPCPCHook_SetMultiCurrencyEnable(bool fEnable);
	// Called to advise the reader that the "multi currency/lingual" feature is available

void ICPCPCHook_SetNegVendEnable(bool fEnable);
	// Called to advise the reader that the "Negative Vend" feature is available

void ICPCPCHook_SetDataEntryEnable(bool fEnable);
	// Called to advise the reader that the "Data Entry" feature is available

bool  ICPCPC_IsStateEnable(void);													// MR 06.03.2014
	// Check the state level of the reader against Enable Level

bool  ICPCPC_IsStateIdle(void);														// MR 27.03.2014
	// Check the state level of the reader against Idle Level

void Set_CPC_Id_Data(uint8_t ix, uint8_t l, uint8_t *point);

bool Is_CPC_Level_1(void);

#endif
