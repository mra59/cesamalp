/****************************************************************************************
File:
    VMC_CPU_HW.h

Description:
    Include file con definizioni e strutture per l'HW CPU VMC

History:
    Date       Aut  Note
    Mar 2021	MR   

 *****************************************************************************************/

#ifndef __VMC_CPU_HW_h
#define __VMC_CPU_HW_h

bool  IsResetDaPWDown(void);
bool  IsResetDaSWReset(void);
void  ClrResetFlags(void);


void  Executive_COMM_Init(void);
void  ExecSlaveTxChar(uint8_t Dato);

void  Master_Exec_Comm_Init(void);
void  Master_Exec_TxChar(uint8_t ch);

void  Master_MDB_Comm_Init(void);
void  Master_MDB_TxChar(uint8_t Dato, uint8_t ModeBit);

void  MDB_Slave_COMM_Init(void);
void  MDB_Slave_TxChar(uint8_t Dato, uint8_t ModeBit);

void  		RS485_Comm_Init(void);
void  		RS485_Comm_ReceiveBlock(uint8_t *DataBuff, uint8_t BuffLenght);
void  		RS485_Comm_Enable_Rx_Chr(void);
void  		RS485_Comm_DisableReceive(void);
void  		RS485_Comm_SendBreak(void);
uint8_t		RS485_Comm_SendBlock(uint8_t *DataBuff, uint8_t BuffLenght);


bool  Is_LCD_NoAnswerSet(void);
void  Clear_LCD_NoAnswer(void);
void  LCD_POWER(bool On_Off);
void  Refresh_LCD(void);
void  SendCommand_LCD(uint8_t CMD);
void  SendMessageToLCD(char *srcpnt, uint8_t msgsize);
void  DelayTimeLCD(uint32_t delayLCD);
bool  EEPROM_Write (uint16_t device, uint16_t MemAddr, uint8_t* EEBuff, uint8_t size);
bool  EEPROM_Read (uint16_t device, uint16_t EE_Address, uint8_t *DestBuff, uint8_t size);
void  RTx_IO(void);
void  MIFARE_COMM_Init(void);
void  MIFARE_COMM_DeInit(void);
void  MIFARE_ChangeBaudRate(uint32_t BaudRate);
void  MIFARE_SendBlock(uint8_t *DataBuff, uint8_t BuffLenght);
void  MIFARE_Enable_Rx_Chr(void);
void  MIFARE_OFF(void);
void  MIFARE_ON(void);
void  GreenLedCPU_Toggle(void);
void  GreenLedCPU(uint8_t On_Off);
void  RedLedCPU_Toggle(void);
void  RedLedCPU(uint8_t On_Off);
void  BlueLedCPU_Toggle(void);
void  BlueLedCPU(uint8_t On_Off);
void  YellowLedCPU_Toggle(void);
void  YellowLedCPU(uint8_t On_Off);
void  Led_Vano(uint8_t On_Off, uint32_t DurataON);
void  Start_IrDA(void);
void  IrDA_COMM_TxChar(uint8_t Dato);
void  IrDA_COMM_ChangeBaudRate(uint8_t BdRate);
void  IrDA_COMM_Disable(void);

/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  IrDA_COMM_TxBegin(void);
void  IrDA_COMM_RxBegin(void);
void  IrDA_COMM_RxEnd(void);
*/


void  EnaDisCoinValidator(uint8_t enable);

void  Init_CRC(uint16_t Seme);
uint16_t  Calc_CRC_GetCRC8(uint8_t Dato);
uint16_t  Calc_CRC_GetCRC16(uint16_t Dato);
uint16_t  Calc_CRC_GetCRC32(uint32_t Dato);

void  All_595Outputs_OFF(void);
void  Check_PowerSupply_Level(void);
bool  Check_MIF_Voltage(void);

void  InitRTC(void);
void  RTC_Calibration(void);
void  RTC_GetTimeAndDate(LDD_RTC_TTime *DateTime);
void  RTC_SetTimeAndDate(LDD_RTC_TTime *DateTime);
void  RTC_SetDayLightTime(LDD_RTC_TTime *DateTime, bool BKP);
bool  RTC_Read_BKP(void);

void  	SetPC_Comm(uint32_t BaudRate);
uint32_t  	ReadAnalog(uint8_t InputName);
uint32_t  	Read_HW_Type(void);
void  	Read_UniqueID(uint32_t *DestCodes);
bool  	Test_uP(uint8_t *codici);

void  	Buz_start(uint32_t time, uint32_t tono1, uint32_t tono2);
void  	BuzFreqChange(uint32_t tono);
void  	Buz_stop(void);
bool  	IsBuzzerOFF(void);
void  	BlinkErrRedLed_HALT(uint32_t frequenza, BlinkErrType errore);
bool  	Is_5V_USB_HOST_PC(void);
bool  	Is_5V_USB_Present(void);
void  	USB_Key_Voltage(uint8_t On_Off);

void  RTC_Write_BKP(bool fBKP);

void  Disable_LowSideMosfetSwitch(void);
bool  Is_5V_USB_Key_Active(void);

void  Scan_Temperatures(void);
void  AnalogMUX_Select(uint8_t AnalogInput);

void	WatchDogReset(void);

#if IOT_PRESENCE
	void  IoT_COMM_Init(void);
	uint8_t  IoT_SendBlock(uint8_t *DataBuff, uint16_t BuffLenght);
	void  IoT_Enable_Rx_Chr(void);
	void  Reset_WiFi_Module(void);
#endif

void  		AnalogInputs_ADC_Init(void);	
void  		AnalogsIntegration(void);
static void	RestartAnalogsIntegration(void);

// =================================================================================================

#define TASTO_DELAY 	 	Milli35									// Integrazione in msec dei tasti
#define MICRO_DELAY			Milli50									// Integrazione in msec dei microswitch
#define DOOR_MICRO_DELAY	Milli500								// Integrazione in msec dei microswitch
#define EMRGNC_SWITCH_DELAY	Milli300								// Integrazione in msec del pulsante Emergency
#define VNTLN_INTEGR	 	Milli3									// Integrazione in msec dei ventolini
#define	MinMifareVoltage 	2400									// Valore minimo tensione lettore Mifare per non essere considerato in CC 
#define RIGHE_TASTIERA 	 	0x0f									// Numero di righe della tastiera (4)
#define	NUMCNTVOLUM		 	1										// Numero dei contatori volumetrici
#define	NUMPULSEROG		 	4										// Numero dei pulsanti di erogazione servizio
#define	INTEGR_CORTO_CIRC	Milli75


// ============================================
// ===== Defines kEYBOARD a matrice   =========
// ============================================
#define	COL1			   0										// Prima colonna tastiera a matrice
#define	COL2			   1										// Seconda colonna tastiera a matrice
#define	COL3			   2										// Terza colonna tastiera a matrice
#define	COL4			   3										// Quarta colonna tastiera a matrice
#define	MAX_COL_NUM		COL4+1


#define	SCAN_COL1		0x80
#define	SCAN_COL2		0x40
#define	SCAN_COL3		0x20
#define	SCAN_COL4		0x10
#define	KEYB_ROWS		(0x80 | 0x40 | 0x20 | 0x10)
#define	KEYB_ROWS_SHIFT	4
	

#define	FOUR_TASTI		0x80										// Flag che indica alla "Gestione_Touch_Tastiera" che e' un tasto della tastiera a 4 tasti
#define MAXVALTASTOPMT	(ROW4_Pin << 1)								// Con 4 righe il valore massimo premendo il tasto di peso maggiore e' (ROW4 << 1)
#define FIRSTCOLUMN		SCAN_COL1									// Indice della prima colonna da scandire
#define LASTCOLUMN		SCAN_COL4									// Indice dell'ultima colonna da scandire
#define SCAN_OVERFLOW	(SCAN_COL4 >> 1)							// Indice oltre l'ultima colonna da scandire: occorre ripartire dalla prima

// ==============================================

#define	InpAux1			0x01(SCAN_COL4 >> 1)
#define	InpAux2			0x02
#define	InpAux3			0x04
#define	InpAux4			0x08

#define	TIMEOUT_EEP		Milli50										// Timeout esecuzione comando su EEProm
#define	TIMEOUT_SPI2	Milli2										// Timeout esecuzione comando su SPI2

#define	LCD_MAX_TIME_BUSY	250

#define	RS485_DE		GPIO_PIN_12

#define	BITS_CC_9_12	0x0f


// ============================================
// ===== Defines 4 Tasti e 4 Led      =========
// ============================================

#define	BIT_TASTO1		0x01
#define	BIT_TASTO2		0x02
#define	BIT_TASTO3		0x04
#define	BIT_TASTO4		0x08

#define	KEYB_COMMON		0x01										// Uscita da attivare sul TPI6C595 Keyboard per dare il GND comune ai tasti

#define	LED_SELEZ1		0x10
#define	LED_SELEZ2		0x08
#define	LED_SELEZ3		0x04
#define	LED_SELEZ4		0x02

#define	NO_TAST_PMT			(BIT_TASTO1 | BIT_TASTO2 | BIT_TASTO3 | BIT_TASTO4)
#define	TASTI_EROGAZIONE	(BIT_TASTO1 | BIT_TASTO2 | BIT_TASTO3 | BIT_TASTO4)

// ============================================
// =====   Defines  Analogiche        =========
// ============================================

#if  CESAM
	//--  CPU   CESAM   ---
	#define	HW_REVISION			0										// Ingresso ADC12_IN11 per Hardware Revision
	#define	VIN_VOLTAGE			1										// Ingresso ADC12_IN12 per la misura della tensione di alimentazione della scheda CPU
	#define	ALL_ADC1_INPUTS		2										// Numero totale degli ingressi ADC1 da convertire
	#define	ALL_ADC2_INPUTS		2										// Numero totale degli ingressi ADC2 da convertire
	#define	ANALOGS_1			1										// Ingresso ADC12_IN9: ingressi NTC2
	#define	ANALOGS_2			0										// Ingresso ADC12_IN10: ingressi NTC1

#else
	#define	EV_CURRENT			0										// Ingresso ADC12_IN11 unico per tutti le uscite di potenza
	#define	VIN_VOLTAGE			1										// Ingresso ADC12_IN12 per la misura della tensione di alimentazione della scheda CPU
	#define	ANALOGS_1			1										// Ingresso ADC12_IN9: ingressi NTC2, AREF e Hardware Revision
	#define	ANALOGS_2			0										// Ingresso ADC12_IN10: ingressi NTC1, e Conduttivimetro (NTC3)
	#define	ALL_ADC1_INPUTS		2										// Numero totale degli ingressi ADC1 da convertire
	#define	ALL_ADC2_INPUTS		2										// Numero totale degli ingressi ADC2 da convertire

	#define	HW_REVISION			2										// Ingresso ADC12_IN9 per Hardware Revision

	#define	MUX_NTC1_NTC2		0
	#define	MUX_NTC3_AREF		1
	#define	MUX_TIPOHW			2
#endif

// --  Ingressi A e B dell'HC4052 - BA = 00 per selezionare Analog1 e Analog2 da inviare all'ADC1 --
#define	NTC1_NTC2_SELECT	{																	\
							(HAL_GPIO_WritePin(AnSW_A_GPIO_Port, AnSW_A_Pin, GPIO_PIN_RESET));	\
							(HAL_GPIO_WritePin(AnSW_B_GPIO_Port, AnSW_B_Pin, GPIO_PIN_RESET));	\
}

// --  Ingressi A e B dell'HC4052 - BA = 01 per selezionare Aref e Analog3 da inviare all'ADC1 --
#define	NTC3_VREF_SELECT	{																	\
							(HAL_GPIO_WritePin(AnSW_A_GPIO_Port, AnSW_A_Pin, GPIO_PIN_SET));	\
							(HAL_GPIO_WritePin(AnSW_B_GPIO_Port, AnSW_B_Pin, GPIO_PIN_RESET));	\
}

// --  Ingressi A e B dell'HC4052 - BA = 10 per selezionare il partitore che determina il TipoHW o di sotto-montaggio --
#define	TIPOHW_SELECT		{																	\
							(HAL_GPIO_WritePin(AnSW_A_GPIO_Port, AnSW_A_Pin, GPIO_PIN_RESET));	\
							(HAL_GPIO_WritePin(AnSW_B_GPIO_Port, AnSW_B_Pin, GPIO_PIN_SET));	\
}

#define	NUM_TEMPER_READINGS			8								// Numero di letture ADC per calcolare la media della temperatura
#define	TEMPER_READ_BUFF_SIZE		NUM_TEMPER_READINGS+2			// Grandezza del buffer letture NTC
	
// --   Definizioni conversione temperatura sonde --
#define	VENTICINQUE_KELVIN	298.15									// Zero Kelvin + 25 perche' la resistenza della NTC e' data a 25 gradi
#define	NTC_NOMINAL_RES		10000									// Resistenza Nominale della NTC a 25 gradi
#define	BETA_NTC			3977									// Beta sonda NTC AS2348/00500 
#define	BIAS_RESISTOR		4990									// Resistenza serie alla sonda NTC che ne determina il partitore
#define	ADC_BIT_NUM			4096									// Numero di bit di risoluzione del convertitore
#define	ZERO_KELVIN			273.15

#define	ADC_HIGH_LIMIT			4000								// Limite superiore di conversione: 208KOhm --> Temper < -30 Gradi Centigradi = sonda aperta o non collegata
#define	ADC_LOW_LIMIT			50									// Limite inferiore di conversione: 60 Ohm --> Temper > 180 Gradi Centigradi = sonda in corto circuito
#define	NTC_ERROR_NO_CONNECTED	0xFE								// Codice errore sonda apetta
#define	NTC_ERROR_SHORT_CIRCUIT	0xF8								// Codice errore sonda in CC


#define	HIGH_TEMPER_LIMIT	3900
#define	LOW_TEMPER_LIMIT	700

	
// ============================================
// =====   Defines  Soglie Tipo  HW   =========
// ============================================

//---  CPU   CESAM   ----

// Le soglie LOW e HIGH sono calcolate considerando un errore totale del 5%
// La tensione VREF+ dell'ADC e' di 3.3V nominali, misurati 3.29V su una scheda.
// La conversione continua con DMA probabilmente influenza i canali adiacenti
// dell'ADC:
// sul partitore da 10K+10K per il tipo HW misuro 1.64V ADC-OFF e 1.82V ADC-ON
// sul partitore controllo Vin misuro 1.61V ADC-OFF e 1.56V ADC-ON

// Perche' la resistenza di ingresso dell'ADC non condizioni la lettura,
// utilizzare resistenze di basso valore


// --- Partitore 1K High - 1K Low: misurata 1,66 V  --> valore convertito 2100 bit
#define	BUILD_0_LOW				2050
#define	BUILD_0_HIGH			2150

/*
// --- Partitore 10K High - 10K Low: misurata 1,82 V  --> valore convertito 2435 bit
#define	RANGE_0_LOW				2375
#define	RANGE_0_HIGH			2495
*/

// --- Partitore 1K High - 3K3 Low: misurata 2,55 V  --> valore convertito 3188 bit
#define	BUILD_1_LOW				3108
#define	BUILD_1_HIGH			3268

// --- Partitore 1K High - 2K2 Low: misurata 2,3 V  --> valore convertito 2880 bit
#define	BUILD_2_LOW				2810
#define	BUILD_2_HIGH			2950

// ============================================
// =====   Defines  I2C   EEPROM      =========
// ============================================

#define	PREV_WRITE_WAIT		Milli20									// Tempo attesa fine write precedente per inviare nuovo comando di scrittura

// ============================================
// =====   Defines  Real Time Clock RTC   =====
// ============================================

#define	RTC_BKP_FLAG		0x0001									// Flag che indica se il calcolo del DayLight saving time e' gia' stato eseguito

// ============================================
// =====   Defines  Byte "Micro1_8"   =========
// ============================================

#define	PULS_EROGAZ_1				0x01							// Gli ingressi PULS_EROGAZ_x sono elaborati singolarmente e non sono memorizzati in Micro1_8
#define	PULS_EROGAZ_2				0x02
#define	PULS_EROGAZ_3				0x04
#define	PULS_EROGAZ_4				0x08
#define	EMERGENCY					0x10
#define	FINE_PRODOTTO_1				0x20
#define	DOOR_SWITCH					0x40
//#define	TASTO_PROG					0x80
#define	FINE_PRODOTTO_2				0x80

#define	INTEGR_TASTO_PROG			0								// Offset array MicroIntegr
#define	INTEGR_EMERGENCY			1								// Offset array MicroIntegr
#define	INTEGR_FINE_PRODOTTO_1		2								// Offset array MicroIntegr
#define	INTEGR_DOORSWITCH			3								// Offset array MicroIntegr
#define	INTEGR_FINE_PRODOTTO_2		4								// Offset array MicroIntegr
								

// ============================================
#define	Tasto_Meno					253											// 0xfd
#define	Tasto_Piu					254											// 0xfe
#define	Tasto_NessunTasto			255											// 0xff

#define	MATRIX_ROWS					9											// Numero righe matrice tastiera
#define	MATRIX_COLS					4											// Numero colonne matrice tastiera LCD remoto
#define	ARRAY_MATRIX_SIZE			(MATRIX_ROWS * MATRIX_COLS)

#define	LCD_HNIBBLE_BUS_SHIFT		4											// Shift del Nibble High del Bus LCD
#define	LCD_LNIBBLE_BUS_SHIFT		8											// Shift del Nibble Low del Bus LCD

#define	LCD_BUS_AS_INPUT_PIN_MASK	0x000000ff									// Mask bits PE0-PE3 per mettere in input LCD DataBus (da complementare)
#define	LCD_BUS_AS_OUTPUT_PIN_MASK	0x00000055									// Mask bits PE0-PE3 per mettere in output LCD DataBus

#define	LCD_BUSY_FLAG				0x0008

#if DBG_LCD_BOLYMIN
	#define	NIBBLE_DELAY			50											// Valore trovato sperimentalmente
	#define	DELAY_BETW_CHARS		560											// Il display Bolymin richiede almeno 37 usec tra un chr e l'altro
#else
	#define	NIBBLE_DELAY			50											// Valore trovato sperimentalmente
	#define	DELAY_BETW_CHARS		500
#endif

#define	E_HIGH_TIME					100
#define	E_HIGH_BF_READ				30											// Durata E high durante lettura BF
#define	E_LOW_BF_READ				30											// Durata E high durante lettura BF
#define	CMD_FIXED_DELAY				100											// Delay fisso quando flag BF non ancora disponibile

// ============================================
// =====   Defines  USB gpio&enum     =========
// ============================================

#define DEVICE_MODE_PIN				GPIO_PIN_9
#define DEVICE_MODE_PIN_GPIO_Port 	GPIOA

	
// ============================================
// =====   Defines  Matrice Tastiere  =========
// ============================================
#define	Tasto_0				0x00
#define	Tasto_1				0x01
#define	Tasto_2				0x02
#define	Tasto_3				0x03
#define	Tasto_4				0x04
#define	Tasto_5				0x05
#define	Tasto_6				0x06
#define	Tasto_7				0x07
#define	Tasto_8				0x08
#define	Tasto_9				0x09

/*--------------------------------------------------------------------------------------*\
Variable definition	
\*-------------------------------------------------------------------------------------*/

extern	uint16_t	Ventolino[NUMCNTVOLUM];



#endif		// __VMC_CPU_HW_h
