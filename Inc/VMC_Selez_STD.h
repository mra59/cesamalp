/****************************************************************************************
File:
    VMC_Selez_STD.h

Description:
    Include file con definizioni e strutture per eseguire le selezioni.

History:
    Date       Aut  Note
    Apr 2019	MR   

 *****************************************************************************************/

#ifndef __VMC_Selez_STD__H
#define __VMC_Selez_STD__H


void  		 Esegui_Selezione(void);

#endif		// __VMC_Selez_STD__H


