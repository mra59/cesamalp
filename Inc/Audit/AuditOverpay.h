/****************************************************************************************
File:
    AuditOverpay.h

Description:
    nclude file contenente definizioni per il file AuditOverpay.c

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __AuditOverpay__H
#define __AuditOverpay__H

static void ResOverpay(void);
static void OverpayCaricamentoBlocchi(void);
static void OverpayCash(void);
static void OverpayPayout(void);
void 		GestioneAuditOverpay(void);
void 		Overpay(uint8_t);
static void OverpayCarta(void);


#endif
