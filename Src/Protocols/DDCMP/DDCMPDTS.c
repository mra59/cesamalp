/****************************************************************************************
File:
    DDCMPDTS.c

Description:
    Implementazione standard EVA-DTS con i comandi del DDCMP.
    puo' attivare il protocollo sia in Master che in Slave mode.

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#include "string.h"
#include "stdio.h"

#include "ORION.H"
#include "DevSerial.h"
#include "DDCMPProtocol.h"
#include "DDCMP.h"
#include "DDCMPDTS.h"
#include "OrionTimers.h"





/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void  UpdateTimeDate(uint8_t* ClockDaWAY);


/*--------------------------------------------------------------------------------------*\
Private variables
\*--------------------------------------------------------------------------------------*/

DDCMPDTSVars sDTSVars;


/*--------------------------------------------------------------------------------------*\
Template packets and strings
\*--------------------------------------------------------------------------------------*/
#if kDDCMPDTSMasterSupport && kDDCMPDTSSlaveSupport
static const uint8_t sIdentPC[] = {
  "PCCONF00"
};
#endif


#if kDDCMPDTSMasterSupport
//static const uint8_t sDDCMPDTSWAYReq[]= {
//  kDDCMPDataMsgCmd, kDDCMPCmdWhoAreYou, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C
//};

static const uint8_t sDDCMPReadReq[]= {
  kDDCMPDataMsgCmd, kDDCMPCmdReadData, 0x00, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff
};

static const uint8_t sDDCMPWriteReq[]= {
  kDDCMPDataMsgCmd, kDDCMPCmdWriteData, 0x00, 0x01, 0xff, 0xff
};

static const uint8_t sDDCMPDeleteReq[]= {
  kDDCMPDataMsgCmd, kDDCMPCmdDeleteData, 0x00, 0x00, 0x00
};
#endif

#if kDDCMPDTSSlaveSupport
static const uint8_t sDDCMPWAYRspAccept[]= {
  kDDCMPDataMsgCmdResp, kDDCMPCmdWhoAreYou, 0x01,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  WAYSwVersion,
  0x00, 0x00, 0x00, 
  (kDDCMPDTSBuffSize+kDDCMPDTSBuffSizeExt)/256, (kDDCMPDTSBuffSize+kDDCMPDTSBuffSizeExt)%256
};
#endif
#if kDDCMPDTSSlaveSupport
static const uint8_t sDDCMPWAYRspReject[]= {
  kDDCMPDataMsgCmdResp, kDDCMPCmdWhoAreYou, 0x0, 0x01
};

//static const uint8_t sDDCMPReadRspAccept[]= {
//  kDDCMPDataMsgCmdResp, kDDCMPCmdReadData, 0x1, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff
//};
static const uint8_t sDDCMPReadRspReject[]= {
  kDDCMPDataMsgCmdResp, kDDCMPCmdReadData, 0x0
};

static const uint8_t sDDCMPWriteRspAccept[]= {
  kDDCMPDataMsgCmdResp, kDDCMPCmdWriteData, 0x1
};
static const uint8_t sDDCMPWriteRspReject[]= {
  kDDCMPDataMsgCmdResp, kDDCMPCmdWriteData, 0x0
};

static const uint8_t sDDCMPDeleteRspAccept[]= {
  kDDCMPDataMsgCmdResp, kDDCMPCmdDeleteData, 0x1
};
static const uint8_t sDDCMPDeleteRspReject[]= {
  kDDCMPDataMsgCmdResp, kDDCMPCmdDeleteData, 0x0
};
#endif

#define DDCMPDTSPkt(pkt)  pkt

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSInit
  Initializes DDCMPManager
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSInit(void) {
  DDCMPStateSet(kDDCMPDTSClosed);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSOpen
  Starts DDCMPManager
Parameters:
  open    ->  open mode
  ddcmpBuff ->  ptr to buffer to be used by ddcmp, or nil
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSOpen(DDCMPDTSMode mode, char* ddcmpBuff) {
	if(sDTSVars.state!=kDDCMPDTSClosed) return;
	sDTSVars.buff= ddcmpBuff;

	#if !kDDCMPDTSMasterSupport
		mode&= ~kDDCMPDTSModeMaster;
  	#endif

	#if !kDDCMPDTSSlaveSupport
		mode&= ~kDDCMPDTSModeSlave;
  	#endif
	
	sDTSVars.mode= mode;
	DDCMPStateSet(kDDCMPDTSInit);		// Macro DDCMPStateSet(newState) (sDTSVars.state= (newState))
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSClose
  Close DDCMPManager and abort any data transfer in progress
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSClose(void) {
  if(sDTSVars.state!=kDDCMPDTSClosed) {
    DDCMPClose(kDDCMPRelease);
    #if kKernelMaxTask>0
    TaskDelete(sDTSVars.taskId);
    #endif
    DDCMPStateSet(kDDCMPDTSClosed);
  }
}

#if kDDCMPDTSMasterSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSCmdReadData
  Send ReadData command.

Parameters:
  listNumber  ->  list number
  offset    ->  offset
  length    ->  length
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSCmdReadData(uint8_t listNumber, uint16_t offset, uint16_t length) {
  memcpy(sDTSVars.buff, sDDCMPReadReq, sizeof(sDDCMPReadReq));
  sDTSVars.buff[3]= listNumber;
  sDTSVars.buff[5]= offset>>8;
  sDTSVars.buff[6]= offset;
  sDTSVars.buff[7]= length>>8;
  sDTSVars.buff[8]= length;
  DDCMPFrameSend(sDTSVars.buff, sizeof(sDDCMPReadReq), false);
  DDCMPStateSet(kDDCMPDTSMaster_WaitReadRsp);
  DDCMPDTSReceive();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSCmdWriteData
  Send WriteData command.

Parameters:
  listNumber  ->  list number
  length    ->  length
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSCmdWriteData(uint8_t listNumber, uint16_t length) {
  memcpy(sDTSVars.buff, sDDCMPWriteReq, sizeof(sDDCMPWriteReq));
  sDTSVars.buff[3]= listNumber;
  sDTSVars.buff[4]= length>>8;
  sDTSVars.buff[5]= length;
  DDCMPFrameSend(sDTSVars.buff, sizeof(sDDCMPWriteReq), false);
  DDCMPStateSet(kDDCMPDTSMaster_WaitWriteRsp);
  DDCMPDTSReceive();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSCmdDeleteData
  Send DeleteData command
Parameters:
  listNumber  ->  list to delete
  recordNumber->  record to delete
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSCmdDeleteData(uint8_t listNumber, uint8_t recordNumber) {
  memcpy(sDTSVars.buff, sDDCMPDeleteReq, sizeof(sDDCMPDeleteReq));
  sDTSVars.buff[3]= listNumber;
  sDTSVars.buff[4]= recordNumber;
  DDCMPFrameSend(sDTSVars.buff, sizeof(sDDCMPDeleteReq), false);
  DDCMPStateSet(kDDCMPDTSMaster_WaitDeleteRsp);
  DDCMPDTSReceive();
}

#endif

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSCmdEnd
  Terminate current command without sending a new one
  In master mode, or if no command in progress, also closes connection
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSCmdEnd(void) {
  switch(sDTSVars.state) {
  case kDDCMPDTSSlave_TxData:
  case kDDCMPDTSSlave_RxData:
    DDCMPStateSet(kDDCMPDTSSlave_WaitCmd);
    DDCMPDTSReceive();
    break;
  default:
    DDCMPDTSConnEnd();
    break;
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSCmdDone
  Current rx command completed but we still don't know if we have another cmd.
  [Master mode only]
  Must be followed by DDCMPDTSCmdXXX() or DDCMPDTSCmdEnd()
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSCmdDone(void) {
  // Just send back an ack so that the other end knows we have received the last 
  // packet correctly
  DDCMPFrameReceive(nil, 0);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSDataTx
  Send a data message

Parameters:
  dataLen     ->  length of data
  selFlag     ->  select flag
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSDataTx(uint16_t dataLen, bool selFlag) {
  sDTSVars.buff[0]= kDDCMPDataMsgDataBlock;
  sDTSVars.buff[1]= sDTSVars.dataBlockNum++;
  DDCMPFrameSend(sDTSVars.buff, dataLen+kDDCMPDTSDataHdrSize, selFlag);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSDataRx
  Confirm that the last data packet received has been processed
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSDataRx(void) {
  DDCMPDTSReceive();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSConnect
  Start DDCMP connection
Parameters:
  ddcmpMode -> master/slave mode
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSConnect(DDCMPMode ddcmpMode)
{
	DDCMPOpen((DDCMPMode)((sDTSVars.mode&kDDCMPDTSModeRemote)? ddcmpMode+kDDCMPModeRemote:ddcmpMode));
	
#if kDDCMPExtendedAddrSupport
	if(sDTSVars.useExtAddr) DDCMPExtendedAddrSet(sDTSVars.extAddr);
#endif

	DDCMPConnect();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSTask
  Handles DDCMP communication
Parameters:
  ignored
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSTask(void) {

  switch(sDTSVars.state) {
  // Connection establishment
  case kDDCMPDTSInit:
    // If master mode not supported, start in slave mode
  case kDDCMPDTSStartSlave:
  case kDDCMPDTSStartSlaveSwap:
  #if kDDCMPDTSSlaveSupport
    if( sDTSVars.state == kDDCMPDTSStartSlaveSwap || DDCMPHookLineRequest()){
      DDCMPStateSet(kDDCMPDTSStartingSlave);
      DDCMPDTSConnect(kDDCMPModeSlave);
      TmrStart(sDTSVars.tmr, (sDTSVars.mode&kDDCMPDTSModeRemote)? kDDCMPDTSSlaveConnAbortRemTO:kDDCMPDTSSlaveConnAbortTO);
      sDTSVars.buffSize= kDDCMPDTSBuffSize+kDDCMPDTSBuffSizeExt;		// MR aggiunto per inizializzare il buffsize
    }
  #endif
    break;
  case kDDCMPDTSStartMaster:
    sDTSVars.buffSize= kDDCMPDTSBuffSize+kDDCMPDTSBuffSizeExt;
    #if kDDCMPDTSMasterSupport
    if(sDTSVars.mode&kDDCMPDTSModeMaster) {
      if(DDCMPHookLineRequest()){
        DDCMPStateSet(kDDCMPDTSStartingMaster);
        DDCMPDTSConnect(kDDCMPModeMaster);
      }
      break;
    }
    #endif
	case kDDCMPDTSStartingSlave:
    // Avoid closing and reopening DDCMP without
    // reason if open in slave-only mode.

		if(!(sDTSVars.mode&kDDCMPDTSModeMaster)) {
			if(!DDCMPDTSHookSlaveRelease())
				break;
			if(!DDCMPClosePossible())
				break;
			// close and reopen for release line CU
		}


  case kDDCMPDTSConnClosed:
	if(TmrTimeout(sDTSVars.tmr)) {
		DDCMPClose(kDDCMPRelease);
		DDCMPStateSet(kDDCMPDTSStartMaster);
    }
    break;

  // Master mode
  #if kDDCMPDTSMasterSupport
  case kDDCMPDTSMaster:
    DDCMPStateSet(kDDCMPDTSMaster_TxWAY);
    break;
  case kDDCMPDTSMaster_TxWAY:
    DDCMPDTSWAYSend();
    DDCMPStateSet(kDDCMPDTSMaster_WaitWAYRsp);
    DDCMPDTSReceive();
    break;
  #endif

  // Slave mode
  #if kDDCMPDTSSlaveSupport
  case kDDCMPDTSSlave:
    DDCMPStateSet(kDDCMPDTSSlave_WaitWAYReq);
    break;
  case kDDCMPDTSSlave_WaitCmd:
    break;
  #endif
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSConnEndInd
  Called when DDCMP connection terminates
Parameters:
  finished  -> true if finished normally, false if aborted
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSConnEndInd(bool finished) {
  uint8_t state= sDTSVars.state;
  DDCMPStateSet(kDDCMPDTSConnClosed);
  switch(state) {
  #if kDDCMPDTSMasterSupport
  case kDDCMPDTSMaster_ConnRejected:
    TmrStart(sDTSVars.tmr, kDDCMPDTSConnRejectedRetryTO);
    DDCMPDTSHookConnRejectedInd();
    break;
  #endif
  case kDDCMPDTSSlave_WaitWAYReq:
  case kDDCMPDTSMaster_WaitWAYRsp:
    // Connection open event was not yet indicated to user, so do nothing here
    break;
  default:
    if(finished) DDCMPDTSHookConnFinishedInd();
    else DDCMPDTSHookConnFailedInd();
    break;
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookConnEstablishedInd
  Called when DDCMP connection becomes established
\*--------------------------------------------------------------------------------------*/
void DDCMPHookConnEstablishedInd(void) {
  switch(sDTSVars.state) {
  case kDDCMPDTSStartingMaster:
    DDCMPStateSet(kDDCMPDTSMaster);				//MACRO DDCMPStateSet(newState) (sDTSVars.state= (newState))
    break;
  case kDDCMPDTSStartingSlave:
    DDCMPStateSet(kDDCMPDTSSlave);				//MACRO DDCMPStateSet(newState) (sDTSVars.state= (newState))
    DDCMPDTSReceive();
    break;
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookConnFinishedInd
  Called when DDCMP connection is completed normally
\*--------------------------------------------------------------------------------------*/
void DDCMPHookConnFinishedInd(void) {
  if(sDTSVars.state==kDDCMPDTSSwapMode) {
    DDCMPClose(kDDCMPNotRelease);
    DDCMPStateSet(kDDCMPDTSStartSlaveSwap);
  }
  else {
    TmrStart(sDTSVars.tmr, DDCMPDTSConnIsSlave()? 0:kDDCMPDTSConnFinishRetryTO);
      // Finish Timeout is 0 if we are slave
    DDCMPDTSConnEndInd(true);
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookConnFailedInd
  Called when DDCMP connection is aborted/failed.
  Connection will be retried later.
  If we were trying to establish a connection in master mode retry in a short time
\*--------------------------------------------------------------------------------------*/
void DDCMPHookConnFailedInd(void)
{
  	if(sDTSVars.state==kDDCMPDTSStartingMaster)
	{
		if(sDTSVars.mode&kDDCMPDTSModeSlave)
	  	{
			DDCMPClose(kDDCMPRelease);
			DDCMPStateSet(kDDCMPDTSStartSlave);
    	}
    	else 
		{
      		DDCMPStateSet(kDDCMPDTSConnClosed);
      		TmrStart(sDTSVars.tmr, kDDCMPDTSMasterConnectRetryTO);
    	}
  	}
  	else 
	{
    	TmrStart(sDTSVars.tmr, DDCMPDTSConnIsSlave()? 0 : kDDCMPDTSConnFailRetryTO);
      	// Finish Timeout is 0 if we are slave
    	DDCMPDTSConnEndInd(false);
  	}
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookFrameRcvdInd
  Called when a DDCMP frame has been received
\*--------------------------------------------------------------------------------------*/
void DDCMPHookFrameRcvdInd(void* frameData, uint16_t frameLen, bool selectFlag) {
  uint8_t* data= (uint8_t*)frameData;

  if(frameLen<kDDCMPDTSDataHdrSize) return;       // Invalid packet. Ignore it

  switch(data[0]) {
  case kDDCMPDataMsgCmd:
    DDCMPDTSCmdReqInd(data, frameLen);
    break;
  case kDDCMPDataMsgCmdResp:
    DDCMPDTSCmdRspInd(data, frameLen);
    break;
  case kDDCMPDataMsgDataBlock:
    DDCMPDTSDataInd(data, frameLen, selectFlag);
    break;
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookFrameSentInd
  Transmission of previous frame has been completed succesfully
\*--------------------------------------------------------------------------------------*/
void DDCMPHookFrameSentInd(void) {
  DDCMPDTSSendNextDataFrame();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSSendNextDataFrame
  Transmit next data frame
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSSendNextDataFrame(void) {
  char* dataBuffPt= sDTSVars.buff+kDDCMPDTSDataHdrSize;

  if(sDTSVars.state!=kDDCMPDTSMaster_TxData && sDTSVars.state!=kDDCMPDTSSlave_TxData) return;
    // Unexpected packet, ignore it
    
  DDCMPDTSHookDataTxInd(sDTSVars.dataBlockNum, dataBuffPt, sDTSVars.buffSize-kDDCMPDTSBuffSizeExt-kDDCMPDTSDataHdrSize);
    // Data handled. 
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSCmdReqInd
  Handle command request
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSCmdReqInd(uint8_t* frameData, uint16_t frameLen) {


	switch(frameData[1]) {

#if kDDCMPDTSSlaveSupport

	case kDDCMPCmdWhoAreYou:
		DDCMPDTSWAYReqInd(frameData, frameLen);
		break;
	
	case kDDCMPCmdReadData:   
		DDCMPDTSReadReqInd(frameData, frameLen);
		break;

	case kDDCMPCmdWriteData:
		DDCMPDTSWriteReqInd(frameData, frameLen);
		break;

	case kDDCMPCmdDeleteData:
		DDCMPDTSDeleteReqInd(frameData, frameLen);
		break;
      
#endif

	default:
		DDCMPDTSBadCmdInd((DDCMPCmd)frameData[1]);  // ignore unkonwn command
		break;
  }
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSCmdRspInd
  Handle command response
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSCmdRspInd(uint8_t* frameData, uint16_t frameLen) {
  switch(frameData[1]) {
  #if kDDCMPDTSMasterSupport
  case kDDCMPCmdWhoAreYou:
    DDCMPDTSWAYRspInd(frameData, frameLen);
    break;
  case kDDCMPCmdReadData:   
    DDCMPDTSReadRspInd(frameData, frameLen);
    break;
  case kDDCMPCmdWriteData:
    DDCMPDTSWriteRspInd(frameData, frameLen);
    break;
  case kDDCMPCmdDeleteData:
    DDCMPDTSDeleteRspInd(frameData, frameLen);
    break;
  #endif
  default:
    (void)(frameLen);
    DDCMPDTSBadRspInd((DDCMPCmd)frameData[1]);
    break;
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSDataInd
  Handle data packet
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSDataInd(uint8_t* frameData, uint16_t frameLen, bool selectFlag) {
  if(sDTSVars.state!=kDDCMPDTSMaster_RxData && sDTSVars.state!=kDDCMPDTSSlave_RxData) {
    // Unexpected packet, ignore it
    DDCMPDTSReceive();    // NB: this will generate an ack, even if a NAck would be more appropriate
    return;
  }

//  if(sDTSVars.dataBlockNum!=frameData[1]) return; sDTSVars.dataBlockNum++;
//    // Bad data block number, ignore it

  DDCMPDTSHookDataRxInd(frameData[1], &frameData[kDDCMPDTSDataHdrSize], frameLen-kDDCMPDTSDataHdrSize, selectFlag);
    // Data handled. 
  
}

#if kDDCMPDTSMasterSupport

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSWAYSend
  Format and sends a WAY packet
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSWAYSend(void) {

typedef unsigned short us;
	DDCMPDTSMasterInfo mi;
	RTCDateTime dateTime;
	MemClear(&mi, sizeof(mi));
	mi.userClass= 0x0c;

  if(RTCGet(&dateTime)) {
    mi.year= dateTime.year%100 ;
    mi.month= dateTime.month;
    mi.day= dateTime.day;
    mi.hours= dateTime.hours;
    mi.minutes= dateTime.minutes;
    mi.seconds= dateTime.seconds;
  }

  #if kDDCMPDTSConnReqSupport
  DDCMPDTSHookMasterInfoGet(&mi);
  #else
  mi.securityCode= DDCMPDTSHookPwdGet();
  #endif

  sDTSVars.buff[0]= kDDCMPDataMsgCmd;
  sDTSVars.buff[1]= kDDCMPCmdWhoAreYou;
  sDTSVars.buff[2]= 0;
  sDTSVars.buff[3]= mi.securityCode/256;
  sDTSVars.buff[4]= mi.securityCode;
  sDTSVars.buff[5]= mi.passCode2/256;
  sDTSVars.buff[6]= mi.passCode2;
  sDTSVars.buff[7]= BinToBcd(mi.day);
  sDTSVars.buff[8]= BinToBcd(mi.month);
  sDTSVars.buff[9]= BinToBcd(mi.year%100);
  sDTSVars.buff[10]= BinToBcd(mi.hours);
  sDTSVars.buff[11]= BinToBcd(mi.minutes);
  sDTSVars.buff[12]= BinToBcd(mi.seconds);
  sDTSVars.buff[13]= mi.userId/256;
  sDTSVars.buff[14]= mi.userId;
  sDTSVars.buff[15]= mi.userClass;

  DDCMPFrameSend(sDTSVars.buff, 16, false);
}
#endif

#if kDDCMPDTSSlaveSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSWAYReqInd
  Handle a Who Are You request (77 E0)
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSWAYReqInd(uint8_t* frameData, uint16_t frameLen)
{
  
	uint16_t pwd;
  
#if kDDCMPDTSConnReqSupport
	DDCMPDTSMasterInfo mi;
	DDCMPDTSSlaveInfo si;
	static const DDCMPDTSSlaveInfo sDefaultSlaveInfo= {
	  0, 0, 0, 0xff, kDDCMPDTSBuffSize, { 8, 7, 6, 5, 4, 3, 2, 1 }
	};
#endif

	if(!(sDTSVars.mode&kDDCMPDTSModeSlave)) goto badPkt;
	if(sDTSVars.state!=kDDCMPDTSSlave_WaitWAYReq) goto badPkt;
	if(frameLen!=16) goto badPkt;
	pwd= frameData[3]*256+frameData[4];
	
	UpdateTimeDate(frameData);

	#if kDDCMPDTSConnReqSupport

	#if kDDCMPDTSConnReqMiBCDPacketSupport
	// Master Information Data transferred as bcd packet format
	// (used where application needs to reduce the code at max possible)
	  mi.securityCode= pwd;
	  //mi.passCode2= frameData[5]*256+frameData[6];        // if necessary
	  mi.year= frameData[9];
	  mi.month= frameData[8];
	  mi.day= frameData[7];
	  mi.hours= frameData[10];
	  mi.minutes= frameData[11];
	  //mi.seconds= frameData[12];                // if necessary
	#else
	  mi.securityCode= pwd;
	  mi.passCode2= frameData[5]*256+frameData[6];
	  mi.userId= frameData[13]*256+frameData[14];
	  mi.userClass= frameData[15];
	  mi.year= BcdToBin(frameData[9]);
	  mi.month= BcdToBin(frameData[8]);
	  mi.day= BcdToBin(frameData[7]);
	  mi.hours= BcdToBin(frameData[10]);
	  mi.minutes= BcdToBin(frameData[11]);
	  mi.seconds= BcdToBin(frameData[12]);
	#endif

	si= sDefaultSlaveInfo;

	if(DDCMPDTSHookConnReqInd(&mi, &si)) {
	// This is a WAY request and we were waiting for it. Pass request to application.
	#if kDDCMPDTSConnReqSiSupport
		memcpy(sDTSVars.buff, sDDCMPWAYRspAccept, sizeof(sDDCMPWAYRspAccept));
		sDTSVars.buff[3]= (char)(si.securityCode>>8);
		sDTSVars.buff[4]= (char)si.securityCode;
		sDTSVars.buff[5]= (char)(si.passCode2>>8);
		sDTSVars.buff[6]= (char)si.passCode2;
		memcpy(&sDTSVars.buff[7], si.serialNumber, sizeof(si.serialNumber));
		sDTSVars.buff[15]= (char)si.swVersion;
		sDTSVars.buff[16]= (char)si.manufacturerId;
		si.buffSize+= kDDCMPDTSBuffSizeExt;
		sDTSVars.buff[19]= (char)(si.buffSize>>8);
		sDTSVars.buff[20]= (char)si.buffSize;
		DDCMPFrameSend(sDTSVars.buff, sizeof(sDDCMPWAYRspAccept), false);
		#else
		DDCMPFrameSend(DDCMPDTSPkt(sDDCMPWAYRspAccept), sizeof(sDDCMPWAYRspAccept), false);
		#endif
	#else

	if(pwd==DDCMPDTSHookPwdGet() || pwd==kDDCMPDTSUniversalPwd)
	{
		// Slave WAY response (88 E0 01) This is an authorized WAY request and we were waiting for it. Send accept response
	  	DDCMPFrameSend(DDCMPDTSPkt(sDDCMPWAYRspAccept), sizeof(sDDCMPWAYRspAccept), false);

	#endif

		DDCMPStateSet(kDDCMPDTSSlave_WaitCmd);
    	DDCMPDTSHookConnEstablishedSlaveInd();
  	}
  	else 
	{
    	// Access denied
    	DDCMPFrameSend(DDCMPDTSPkt(sDDCMPWAYRspReject), sizeof(sDDCMPWAYRspReject), false);
  	}

	DDCMPDTSReceive();
	return;

badPkt:
	DDCMPDTSBadCmdInd((DDCMPCmd)frameData[1]);
}
#endif

#if kDDCMPDTSMasterSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSWAYRspInd
  Handle a Who Are You response
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSWAYRspInd(uint8_t* frameData, uint16_t frameLen) {
  DDCMPDTSSlaveInfo slaveInfo;
  uint16_t  slaveBuffSize;
  if(!(sDTSVars.mode&kDDCMPDTSModeMaster)) goto badPkt;
  if(sDTSVars.state!=kDDCMPDTSMaster_WaitWAYRsp) goto badPkt;
  if(frameData[2]==0) {
    // WAY rejected. Close connection
    DDCMPDTSConnEnd();
    DDCMPStateSet(kDDCMPDTSMaster_ConnRejected);
    return;
  }
  if(frameLen!=21) goto badPkt;

  #if kDDCMPDTSMasterSupport && kDDCMPDTSSlaveSupport
  if((sDTSVars.mode&kDDCMPDTSModeMasterSlave)==kDDCMPDTSModeMasterSlave) {
    // Now check slave identity
    if(memcmp(&frameData[7], sIdentPC, sizeof(sIdentPC))==0) {
      // This is FAGE Configurator: turn in slave mode
      DDCMPDTSConnEnd();
      DDCMPStateSet(kDDCMPDTSSwapMode);
      return;
    }
  }
  #endif

  // Go on as DDCMP master
  DDCMPStateSet(kDDCMPDTSMaster_WaitCmd);
  slaveBuffSize= frameData[19]*256+frameData[20];
  if(slaveBuffSize==0) slaveBuffSize= 256;
    // Fix buff size if 0 (default) 
  if(slaveBuffSize<kDDCMPDTSBuffSizeExt+kDDCMPDTSDataHdrSize) slaveBuffSize= kDDCMPDTSBuffSizeExt+kDDCMPDTSDataHdrSize;
    // Fix buff size if invalid
  sDTSVars.buffSize= slaveBuffSize;
    // Remember slave buffer size
  if(sDTSVars.buffSize>kDDCMPDTSBuffSizeExt+kDDCMPDTSBuffSize) sDTSVars.buffSize= kDDCMPDTSBuffSizeExt+kDDCMPDTSBuffSize;
    // Fix buff size if too big

  #if kDDCMPDTSConnReqMiSupport
  slaveInfo.securityCode= frameData[3]*256+frameData[4];
  slaveInfo.passCode2= frameData[5]*256+frameData[6];
  slaveInfo.swVersion= frameData[15];
  slaveInfo.manufacturerId= frameData[16];
  memcpy(slaveInfo.serialNumber, &frameData[7], 8);
  slaveInfo.buffSize= slaveBuffSize-kDDCMPDTSBuffSizeExt;
  DDCMPDTSHookConnEstablishedMasterInd(&slaveInfo);
  #else
  DDCMPDTSHookConnEstablishedMasterInd();
  #endif
  
  return;

badPkt:
  DDCMPDTSBadRspInd(frameData[1]);
}
#endif


#if kDDCMPDTSSlaveSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSReadReqInd
  Gestione Comando "Read data request" (77 E2)
  
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSReadReqInd(uint8_t* frameData, uint16_t frameLen) {
  
	uint16_t byteOffset, length, actualSegmentLen = kDDCMPDTSLenUnknown;
	
	if (sDTSVars.state != kDDCMPDTSSlave_WaitCmd) goto badPkt;
	if (frameLen != 9) goto badPkt;
	byteOffset= frameData[6]*256+frameData[5];
	length= frameData[8]*256+frameData[7];
  
	sDTSVars.dataBlockNum= 0;
	if (DDCMPDTSHookReadDataReqInd(frameData[3], byteOffset, length, &actualSegmentLen)) {		// Controllo se posso rispondere al cmd Read 77 E2
		// Read command accepted
		DDCMPStateSet(kDDCMPDTSSlave_TxData);
		frameData[0]= kDDCMPDataMsgCmdResp;
		frameData[2]= 1;
		frameData[7]= (char)actualSegmentLen;
		frameData[8]= (char)(actualSegmentLen>>8);
		DDCMPFrameSend(frameData, 9, false);
 	} else {
 		// Read command rejected
 		DDCMPFrameSend(DDCMPDTSPkt(sDDCMPReadRspReject), sizeof(sDDCMPReadRspReject), false);
 		DDCMPDTSReceive();
 	}
	return;

badPkt:
  	  DDCMPDTSBadCmdInd((DDCMPCmd)frameData[1]);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSWriteReqInd
  Gestione Comando "Write data request"(77 E3).
  
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSWriteReqInd(uint8_t* frameData, uint16_t frameLen) {
  
	uint16_t length;
  
	if (sDTSVars.state != kDDCMPDTSSlave_WaitCmd) goto badPkt;
	if (frameLen != 6) goto badPkt;
	length= frameData[5]*256+frameData[4];
  
	if (DDCMPDTSHookWriteDataReqInd(frameData[3], length)) {
		// Write command accepted
		DDCMPStateSet(kDDCMPDTSSlave_RxData);
		DDCMPFrameSend(DDCMPDTSPkt(sDDCMPWriteRspAccept), sizeof(sDDCMPWriteRspAccept), false);
		DDCMPDTSReceive();
	}  else {
		// Write command rejected
		DDCMPFrameSend(DDCMPDTSPkt(sDDCMPWriteRspReject), sizeof(sDDCMPWriteRspReject), false);
		DDCMPDTSReceive();
	}
	return;

badPkt:
  	  DDCMPDTSBadCmdInd((DDCMPCmd)frameData[1]);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSDeleteReqInd
  Gestione Comando "Delete data request"(77 E4).
  
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSDeleteReqInd(uint8_t* frameData, uint16_t frameLen) {
  
	if (sDTSVars.state != kDDCMPDTSSlave_WaitCmd) goto badPkt;
	if (frameLen !=5) goto badPkt;
	if (DDCMPDTSHookDeleteDataReqInd(frameData[3], frameData[4])) {
		// Delete command accepted
		DDCMPFrameSend(DDCMPDTSPkt(sDDCMPDeleteRspAccept), sizeof(sDDCMPDeleteRspAccept), false);
		DDCMPDTSReceive();
	} else {
		// Delete command rejected
		DDCMPFrameSend(DDCMPDTSPkt(sDDCMPDeleteRspReject), sizeof(sDDCMPDeleteRspReject), false);
		DDCMPDTSReceive();
	}
	DDCMPStateSet(kDDCMPDTSSlave_WaitCmd);
	return;

badPkt:
  	  DDCMPDTSBadCmdInd((DDCMPCmd)frameData[1]);
}
#endif

#if kDDCMPDTSMasterSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSReadRspInd
  Handle a Read response
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSReadRspInd(uint8_t* frameData, uint16_t frameLen) {
  if(sDTSVars.state!=kDDCMPDTSMaster_WaitReadRsp) goto badPkt;
  if(frameLen<3) goto badPkt;
  if(frameData[2]==1) {
    // Read accepted
    if(frameLen!=9) goto badPkt;
    DDCMPStateSet(kDDCMPDTSMaster_RxData);
    DDCMPDTSHookReadDataRspInd(true, (uint16_t)frameData[8]*256+frameData[7]);
  }
  else {
    // Read rejected
    if(frameLen!=3) goto badPkt;
    DDCMPStateSet(kDDCMPDTSMaster_WaitCmd);
    DDCMPDTSHookReadDataRspInd(false, 0);
  }
  DDCMPDTSRspEnRx();
  return;

badPkt:
  DDCMPDTSBadRspInd(frameData[1]);
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSWriteRspInd
  Handle a Write response
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSWriteRspInd(uint8_t* frameData, uint16_t frameLen) {
  if(sDTSVars.state!=kDDCMPDTSMaster_WaitWriteRsp) goto badPkt;
  if(frameLen!=3) goto badPkt;
  sDTSVars.dataBlockNum= 0;

  if(frameData[2]==1) {
    // write accepted, start sending data
    DDCMPStateSet(kDDCMPDTSMaster_TxData);
    DDCMPDTSHookWriteDataRspInd(true);
    DDCMPDTSSendNextDataFrame();
  }
  else {
    // write rejected
    DDCMPStateSet(kDDCMPDTSMaster_WaitCmd);
    DDCMPDTSHookWriteDataRspInd(false);
  }

  DDCMPDTSRspEnRx();
  return;

badPkt:
	DDCMPDTSBadRspInd(frameData[1]);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSDeleteRspInd
  Handle a Delete response
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSDeleteRspInd(uint8_t* frameData, uint16_t frameLen) {
  if(sDTSVars.state!=kDDCMPDTSMaster_WaitDeleteRsp) goto badPkt;
  if(frameLen!=3) goto badPkt;
  DDCMPStateSet(kDDCMPDTSMaster_WaitCmd);
  DDCMPDTSHookDeleteDataRspInd(frameData[2]);
  DDCMPDTSRspEnRx();
  return;

badPkt:
  DDCMPDTSBadRspInd(frameData[1]);
}
#endif


/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSBadCmdInd
  Called when bad/unexpected command request received
Parameters:
  cmd ->  command code  
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSBadCmdInd(DDCMPCmd cmd) {
  (void)(cmd);

  // Send reject response?
  DDCMPDTSReceive();      // Prepare to receive next packet
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSBadCmdInd
  Called when bad/unexpected command response received
Parameters:
  cmd ->  command code  
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSBadRspInd(DDCMPCmd cmd) {
  (void)(cmd);

  // Ignore this?
  DDCMPDTSReceive();      // Prepare to receive next packet
}

#if kDDCMPDTSMasterSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSRspEnRx
  Called after reception of a command response to check if we must
  re-enable reception
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSRspEnRx(void) {
  switch(sDTSVars.state) {
  case kDDCMPDTSMaster_RxData:
  case kDDCMPDTSMaster_RxMemory:
  case kDDCMPDTSMaster_WaitCmd:
    DDCMPDTSReceive();
    break;
  }
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSConnEnd
  Terminate DDCMP connection
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSConnEnd(void) {
  DDCMPDisconnect();
  //if(DDCMPDTSConnIsOpen())        // test, just to be sure..
  DDCMPStateSet(kDDCMPDTSConnClosing);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSReceive
  Prepare to receive a new packet
\*--------------------------------------------------------------------------------------*/
static void DDCMPDTSReceive(void) {
  DDCMPFrameReceive(sDTSVars.buff, kDDCMPDTSBuffSize);
}

#if kDDCMPExtendedAddrSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSExtendedAddrSet
  Set DDCMP extended address
Parameters:
  extendedAddress ->  ddcmp extended address
\*--------------------------------------------------------------------------------------*/
//void DDCMPDTSExtendedAddrSet(const uint8_t extendedAddress[8]) {
void DDCMPDTSExtendedAddrSet(const DDCMPExtendedAddr extendedAddress) {
  if(extendedAddress) {
    memcpy(sDTSVars.extAddr, extendedAddress, sizeof(DDCMPExtendedAddr));
    sDTSVars.useExtAddr= true;
  }
  else
    sDTSVars.useExtAddr= false;
}

#endif

