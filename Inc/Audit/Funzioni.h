/****************************************************************************************
File:
    Funzioni.h

Description:
    Include file con definizioni e strutture per il file Funzioni

History:
    Date       Aut  Note
    Giu 2019	MR   

 *****************************************************************************************/

#ifndef __Funzioni_h
#define __Funzioni_h

/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/
extern	const char	gSWRevCode[];

#ifndef MCU_STM32_RAMBegin
#define MCU_STM32_RAMBegin		0x20000000
#endif

// Dichiarazione  Funzioni  

void ReadMessage(char *pnt, uint8_t lang, uint8_t mess, uint8_t msgLen);
bool SystemStBy(void);
uint16_t IntListGetMin(uint16_t *ptr, uint16_t num);
uint IntListGetMax(uint *ptr, uint num);
void  SetMinMaxPrice();
void  ResetDaWDOG_Events(void);
void  GestioneFasceSconto(void);
uint8_t TestFasce(tFasce *fasce, uint8_t maxfasce);
void  TestFasceGiornaliere(void);
void ReadMess(char *pnt, uint mess);
uint32_t AuditReadData(uint32_t pnt, uint8_t size);
void AuditByteInc(uint32_t pnt, uint8_t value);
void AuditByteDec(uint32_t pnt, uint8_t value);
void  AuditUintInc(uint32_t pnt);
void AuditUintAdd(uint32_t pnt, uint8_t value);
void  AuditUlongInc(uint32_t pnt);
void  AuditUlongAdd(uint32_t pnt, uint valore);
void  AuditVeryUlongAdd(uint32_t pnt, uint32_t valore);
void  AuditUlongSub(IICEEPOffsetType pnt, uint valore);
void SetDatePrice(void);
void SetDateInit(void);
bool SetDataTime(uint32_t *data, uint *time);
void   AzzeraTuttaEEPROM(void);
void   FillEEPROMS(uint8_t FillVal);
void   FillEE_LogBuffer(uint8_t FillVal);
void  Check_Clear_Audit_LR(void);
void  Set_DDCMP_EnableAuditClear(void);
void  ClearAudit(void);
void  ClrMarcheAudit(void);
void AuditLRClear(void);
void AuditINClear(void);
void	MemoDateTimeActualAuditRead();
void 	CT5AuditAdjust(void);
void  ReadDato(char *pntDestinazione, char *pntSorgente, uint nBytes);
uint8_t IICEEPF(uint32_t pntDestinazione, uint8_t Value, IICEEPSizeType nBytes);
uint8_t  IICEEPR(uint8_t *pntDestinazione, uint32_t pntSorgente, uint32_t nBytes);
uint8_t  IICEEPW(uint32_t pntDestinazione, uint8_t *pntSorgente, uint32_t nBytes);
void SetConfigReceiveStart(void);
void ResetConfigReceiveStart(void);
void  PowerOutageAudit(void);
void  InsertEvent (uint8_t tipo, uint32_t date, uint time, uint8_t TestPWDN_Flag);
void  ClearBackupRAM_Events(void);
void  SwRevUpdate_Events(void);

  	

#endif		// End __Funzioni_H
