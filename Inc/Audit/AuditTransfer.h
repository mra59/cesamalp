/*--------------------------------------------------------------------------------------*\
File:
    AuditTransfer.h

Description:
    Handle formatting of audit data

History:
    Date       Aut  Note
    ---------- ---  ----


\*--------------------------------------------------------------------------------------*/

#ifndef __AuditTransfer__
#define __AuditTransfer__

#include "BaseTypes.h"

/*--------------------------------------------------------------------------------------*\
AuditTransfer Customization Constant
\*--------------------------------------------------------------------------------------*/

#ifndef kTransSetHeader
#define kTransSetHeader         1
    // Transaction Set Header Default Value (001 indicate Vending Industry)
#endif

#ifndef kTransSetCtrlNum
#define kTransSetCtrlNum        1
    // Transaction Set Control Number Defaut Value 
    // For a single transfer to/from a VMD, this number will always be 0001
    // Incrementing if multiple transaction sets.
#endif


/*--------------------------------------------------------------------------------------*\
AuditTransfer interface 
\*--------------------------------------------------------------------------------------*/

void AuditTransferBegin(const void* auditTable);
    // Start a new audit data transfer operation
    // Specify beginning of audit table

void AuditTransferEnd(void);
    // Terminates an audit data transfer operation

int8 AuditTransferDataGet(char* buff, uint16 size);
    // Get next block of audit data
    // Return number of data bytes inserted in buffer.
    // <0 if buffer too small for next data item
    // 0 if no more audit data

/*--------------------------------------------------------------------------------------*\
External hooks
    These optional routines are product-specific, so must be provided in some other module
\*--------------------------------------------------------------------------------------*/
void AuditHookStringRead(char* destStr, const char* srcStr, uint8 strLen, uint8 fileId);
    // Read an audit string

uint32 AuditHookDataRead(uint16 dataOffset, uint8 dataSize, uint8 fileId);
    // Read an audit data


#endif // __AuditTransfer__


