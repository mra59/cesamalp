/*
 * ICPGlobal.h
 *
 *  Created on: Sep 30, 2013
 *      Author: Abe
 */

#ifndef ICPGLOBAL_H_
#define ICPGLOBAL_H_

#define SIZE_MANUFACTURER 	3
#define SIZE_SERIAL 		12
#define SIZE_MODEL 			12

extern uint16_t CoinEnMask;
extern uint16_t BillEnMask;

extern uint8_t num_icp_bill;
extern uint8_t num_icp_coin;

extern bool send_tube_status;

// Pay Out Status
typedef enum {
  kPOSNone,
  kPOSRequest,
  kPOSRequested
} CMDrvICPPOS;

// Pay Out route
typedef enum {
  Pay_Change,
  Pay_Manual,
  Pay_POS
} CMDrvPAYPOS;

typedef struct {
  CreditValue   	acceptableAmount;
  CreditValue   	changeToPayout;
  CMDrvICPPOS   	payoutState;
  CreditCashTypes 	cashTypes;
  uint8_t      		acceptAmountCoinChgd 	:1;  	// true if acceptable coins amount has changed
  uint8_t      		acceptAmountBillChgd 	:1;  	// true if acceptable bills amount has changed
  uint8_t      		inExactChange 			:1;
  uint8_t      		inFillMode 				:1;		// In fill mode il cash non e' utilizzato come credito
  uint8_t        		chgReady 				:1;		// true se rendiresto ready e available
  uint16_t      		billEnableMask;
  uint16_t      		coinEnableMask;
} CMDrvICPCashVars;


extern CMDrvICPCashVars sCMDICPCash;

#endif /* ICPGLOBAL_H_ */
