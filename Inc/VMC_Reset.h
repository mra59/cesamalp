/****************************************************************************************
File:
    VMC_Reset.h

Description:
    Include file con definizioni e strutture per il Reset della CPU VMC.

History:
    Date       Aut  Note
    Apr 2019	MR   

 *****************************************************************************************/

#ifndef __VMC_Reset__H
#define __VMC_Reset__H


void 	Reset_CPU(void);

#endif
