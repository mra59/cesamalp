/****************************************************************************************
File:
    ICPDrv.c

Description:
    Driver ICP 

History:
    Date       Aut  Note
    Set 2012 	Abe   

*****************************************************************************************/


#include "ORION.H"
#include "icpDrv.h"
#include "icp.h"
#include "DevSerial.h"
#include "icpProt.h"
#include "icpHook.h"
#include "icpVMCProt.h"
#include "icpCHGProt.h"
#include "icpBILLProt.h"
#include "icpCPCProt.h"
#include "icpUSDProt.h"

#include "OrionTimers.h"

extern	bool ICPUSDHook_IsAvailable(void);
extern	bool ICPUSDHook_IsEnabled(uint8_t usdType);

/*---------------------------------------------------------------------------------------------*\
Method: ICPTxEmptyInd
	Funzione di trasmissione buffer o di un chr (es. Ack) chiamata ad ogni Transmit Complete.
	Funzione eseguita in Irq.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ICPTxEmptyInd(void)
{
	if (gVars.fTxFrame == 0) return;
	if (ICPInfo.iTX == ICPInfo.nModebitPos) {
		#if(_IcpSetModeBit_ == false)
			if(icpRS232Mode) ICPHookModeBitClear();
		else
		#endif
			ICPHookModeBitSet();
	} else {
		if(icpMasterMode && (ICPInfo.iTX <= 1)) {
			ICPHookModeBitClear();
		}
	}	
	if(ICPInfo.nFrameLen) {
		ICPInfo.nFrameLen--;
		ICPHookPutChar(ICPInfo.buff[ICPInfo.iTX++]);
	}
	if (!ICPInfo.nFrameLen) {
		gVars.fTxFrame = 0;
		//MR19 ICPHookTxEnd();						// Non piu' usata
		//MR19 ICPHookRxBegin();					// Non piu' usata
		ICPInfo.nComDrvState = COM_TXFRAME;														// Serve per passare subito nello stato di COM_WAITRESP
																								// in origine questa funzione e' chiamata solo in ICPTask
																								// per� si verificano casi di scarto di dati in ricezione
																								// se nell'applicazione ci sono task che occupano piu' tempo
																								// rallentando la gestione di ICPTask.
		ICPDrvHandleComState();																	// Predispongo ricezione risposta
#if (_IcpSlaveCode_)
		ICPInfo.WaitAckNackRet = true;															// In Slave Mode attendo solo Ack, Nack o RET
#endif
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPTxEmptyInd
	Funzione chiamata dall'Irq di Transmit Complete.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ICPTxComplete(void) {
	ICPTxEmptyInd();
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDataInd
	Elaborazione chr ricevuto dalla seriale.
	Funzione eseguita in Irq.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ICPDataInd(uint8_t nData)
{

	//MR19  bool fStartCMD;					// Never referenced
	
// ======================================================
// ========    MASTER     MODE   Rx  ====================
// ======================================================
#if (_IcpMaster_ == true)
	if(icpMasterMode) {

		switch(ICPInfo.nComDrvState) {
			case COM_WAITRESP:
				ICPInfo.nComDrvState = COM_RX;
			case COM_RXERR:
			case COM_RX:
				if(ICPInfo.iRX < ICP_MAX_BUFFSIZE) {
					ICPInfo.buff[ICPInfo.iRX] = nData;
					ICPInfo.iRX++;
					//#if (_IcpMasterNoModeBit_ == true)
					#if(_IcpChkModeBit_ == false)
						ICPInfo.fSlaveRW = false;
						if(ICPInfo.iRX == 1) ICPInfo.fSlaveRW = true;
					#endif
					ICPDrvTestEndFrame(icpMasterMode);
				}
				//#if (_IcpMasterNoModeBit_ == true)
				/*#if (_IcpChkModeBit_ == false)
					ICPTimerStart(ICP_TMID_INTERBYTE);
				#endif*/
				ICPTimerStart(ICP_TMID_INTERBYTE);
				break;
			default:
				// ?!?!
				break;
		}
	return;
	}
#endif

// ======================================================
// ========    SLAVE   MODE  Rx  ========================
// ======================================================
#if (_IcpSlaveCode_)

	// Ignore received character if we are not in the correct state
	if(ICPInfo.nComDrvState==COM_RXEOF || ICPInfo.nComDrvState==COM_RXFRAME || ICPInfo.nComDrvState==COM_TX || ICPInfo.nComDrvState==COM_SUSPEND) {
		return;
	}

	#if (_IcpSlaveInterByteTm_ == true)
		if(ICPHookModeBitTest()) {
			ICPInfo.iRX = 0;
			//} else {
			// Check Master ACK, NAK, RET...
		}
		if(ICPTimerTimeout(ICP_TMID_INTERBYTE)) {
			ICPInfo.iRX = 0;
		}
		switch(ICPInfo.nComDrvState) {
		case COM_WAITRESP:
			ICPInfo.nMasterRW = nData;
			switch(nData) {
			case ICP_RW_RET:
				ICPDrvSetComState(COM_RETRYTX);
				return;
			case ICP_RW_ACK:
			case ICP_RW_NAK:
			default:
				ICPDrvSetComState(COM_IDLE);
				return;
			}
			break;
		default:
			break;
		}

		if(ICPInfo.iRX < ICP_MAX_BUFFSIZE) {
			ICPInfo.buff[ICPInfo.iRX] = nData;
			ICPInfo.iRX++;
		}

		ICPInfo.nComDrvState = COM_RX;
		ICPTimerStart(ICP_TMID_INTERBYTE);
	#else

		#if(kIcpRs232Support == true)
		fStartCMD = false;
		if(!icpRS232Mode) {
			if(ICPHookModeBitTest()) fStartCMD = true;
		} else {
			if(ICPTimerTimeout(ICP_TMID_INTERBYTE)) {
				if(ICPInfo.nComDrvState != COM_WAITRESP) {
					fStartCMD = true;
				}
			}

		}
	 	ICPTimerStart(ICP_TMID_INTERBYTE);
		if(fStartCMD) {
		#else
		if(ICPHookModeBitTest()) {
		#endif
			// ========================================================================
			// ========    Ricevuto  Comando  (Mode bit Set)   ========================
			// ========================================================================

			ICPInfo.iRX = 0;
			ICPInfo.nFrameLen = 0;																// Predispongo invalid command!
			if (ICPInfo.WaitAckNackRet) {														// Ero in attesa di Ack, Nack o Ret ma il Master ha inviato un comando
				ICPInfo.WaitAckNackRet = false;
				ICPDrvSetComState(COM_IDLE);
			}
			// Is it my frame?
			switch(nData & ICP_ADDR_MASK) {
			#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
			case ICP_USD1_ADDR:
			case ICP_USD2_ADDR:
			case ICP_USD3_ADDR:
				if(ICPUSDHook_IsAvailable() && ICPUSDHook_IsEnabled(nData & ICP_ADDR_MASK)) {
					switch(nData & ICP_CMD_MASK) {
					case ICP_USD_CMD_RESET:
					case ICP_USD_CMD_POLL:
						ICPInfo.nFrameLen = sizeof(T_USDCMD_RESET);
						break;
					case ICP_USD_CMD_SETUP:
						ICPInfo.nFrameLen = sizeof(T_USDCMD_SETUP);
						break;
					case ICP_USD_CMD_CONTROL:
						ICPInfo.nFrameLen = sizeof(T_USDCMD_CONTROL00);
						break;
					case ICP_USD_CMD_VEND:
					case ICP_USD_CMD_FUNDS:
					case ICP_USD_CMD_EXP:
						ICPInfo.nFrameLen = ICP_MAX_FRAMELEN;
						break;
					}
				}
				break;
			#endif

			#if (_IcpSlaveCode_ & ICP_CHANGER)
			case ICP_CHG_ADDR:
				if(ICPCHGHook_IsAvailable()) {
					switch(nData & ICP_CMD_MASK) {
					case ICP_CHG_CMD_RESET:
					case ICP_CHG_CMD_STATUS:
					case ICP_CHG_CMD_TUBESTATUS:
					case ICP_CHG_CMD_POLL:
						ICPInfo.nFrameLen = sizeof(T_CHGCMD_RESET);
						break;
					case ICP_CHG_CMD_COINTYPE:
						ICPInfo.nFrameLen = sizeof(T_CHGCMD_COINTYPE);
						break;
					case ICP_CHG_CMD_DISPENSE:
						ICPInfo.nFrameLen = sizeof(T_CHGCMD_DISPENSE);
						break;
					case ICP_CHG_CMD_EXP:
						ICPInfo.nFrameLen = ICP_MAX_FRAMELEN;
						break;
					}
				}
				break;
			#endif

			#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
			case ICP_BILL_ADDR:
				if(ICPBILLHook_IsAvailable()) {
					switch(nData & ICP_CMD_MASK) {
					case ICP_BILL_CMD_RESET:
					case ICP_BILL_CMD_STATUS:
					case ICP_BILL_CMD_POLL:
					case ICP_BILL_CMD_STACKER:
						ICPInfo.nFrameLen = sizeof(T_BILLCMD_RESET);
						break;
					case ICP_BILL_CMD_SECURITY:
						ICPInfo.nFrameLen = sizeof(T_BILLCMD_SECURITY);
						break;
					case ICP_BILL_CMD_BILLTYPE:
						ICPInfo.nFrameLen = sizeof(T_BILLCMD_BILLTYPE);
						break;
					case ICP_BILL_CMD_ESCROW:
						ICPInfo.nFrameLen = sizeof(T_BILLCMD_ESCROW);
						break;
					case ICP_BILL_CMD_EXP:
						ICPInfo.nFrameLen = ICP_MAX_FRAMELEN;
						break;
					default:
						break;
					}
				}
				break;
			#endif

			#if (_IcpSlaveCode_ & ICP_CARDREADER)
			case ICP_CPC_ADDR:
				if(ICPCPCHook_IsAvailable()) {
					switch(nData & ICP_CMD_MASK) {
					case ICP_CPC_CMD_RESET:
					case ICP_CPC_CMD_POLL:
						ICPInfo.nFrameLen = sizeof(T_CPCCMD_RESET);								// Carica ICPInfo.nFrameLen con 1
						break;
					case ICP_CPC_CMD_SETUP:
					case ICP_CPC_CMD_VEND:
					case ICP_CPC_CMD_READER:
					case ICP_CPC_CMD_REVALUE:
					case ICP_CPC_CMD_EXP:
						ICPInfo.nFrameLen = ICP_MAX_FRAMELEN;									// Carica ICPInfo.nFrameLen con 35 da usare come flag nella ICPDrvTestEndFrame
						break;
					default:
						break;
					}
				}
				break;
			#endif
			}
			if(ICPInfo.nFrameLen) {																// Se il comando era per una periferica abilitata, ICPInfo.nFrameLen e' > 0
				ICPInfo.buff[0] = nData;														// e lo memorizza in ICPInfo.buff[0]
				ICPInfo.iRX++;
				ICPInfo.nFrameLen++;
			}
		} else {
			// ========================================================================
			// ========    Ricevuto  Dato  (Mode bit Clear)    ========================
			// ========================================================================

			// L'errore che si e' eliminato con questa modifica (28.08.15) avveniva sulla Paypoint
			// Microhard la quale, spesso, non risponde Ack al pacchetto dati inviato dal PK3.
			// Nel caso di un Revalue succedeva che la risposta Revalue Approved (0x0D) fosse trasmessa
			// ma il Master non rispondeva e iniziava l'invio di un comando BILL TYPE al Bill Validator.
			// In questo comando erano presenti due chr 0x00 che erano interpretati dal PK3 come
			// l'Ack alla risposta Revalue Approved.
			// Quando il Master richiamava il PK3, questi rispondeva con l'Ack a tutti i Poll ricevuti
			// da li' in avanti: la ricarica era stata effettuata ma la Paypoint si fermava in "ATTENDERE"..
			// La modifica consiste nell'aggiunta della flag WaitAckNackRet, set alla fine trasmissione
			// di un blocco dati.

			// Se il dato e' destinato a una periferica abilitata, nFrameLen e' stato inizializzato
			// alla ricezione del comando (Mode bit set), altrimenti nFrameLen e' zero e non
			// inserisce nel buffer ICPInfo.buff i dati ricevuti.
			// Se sono in attesa di risposta al blocco dati appena inviato (WaitAckNackRet = 1),
			// nFrameLen dovrebbe essere zero: i dati ricevuti ma non destinati a me sono scartati
			// e SOLO IL PRIMO chr e' controllato come risposta del Master al blocco dati inviato.
			// La modifica serve a scartare tutti i chr ricevuti come dati ma indirizzati ad altre
			// periferiche ed evitare che questi dati possano essere interpretati erroneamente come
			// Ack, Nack o RET.
			
			if (ICPInfo.nFrameLen) {															// Se ho ricevuto un comando per me nFrameLen e' > 0 e memorizzo
				if (ICPInfo.iRX < ICP_MAX_BUFFSIZE) {											// i dati in ICPInfo.buff
					ICPInfo.buff[ICPInfo.iRX] = nData;
					ICPInfo.iRX++;
					ICPDrvTestEndFrame(icpMasterMode);
				}
			} else {
				ICPTimerStart(ICP_TMID_INTERBYTE0);												// Serve per gestire subito il prossimo carattere come un possibile CMD!
				if (ICPInfo.WaitAckNackRet) {													// Sono in attesa della risposta al blocco dati inviato al Master
					ICPInfo.WaitAckNackRet = false;												// Ricevo risposta al blocco dati inviato, clr flag attesa
					switch(nData) {
						case ICP_RW_RET:
							ICPDrvSetComState(COM_RETRYTX);
							break;																// 26.08.15 Aggiunto break altrimenti ricevendo RET era come ricevere ACK
						case ICP_RW_ACK:
							ICPSetFrameAcked(true);
							ICPDrvSetComState(COM_IDLE);
							break;
						case ICP_RW_NAK:
						default:
							ICPDrvSetComState(COM_IDLE);
							break;
					}
				}
			}
		}
	#endif 	// End (_IcpSlaveInterByteTm_ == true)
#endif		// End (_IcpSlaveCode_)
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPErrInd
	Funzione chiamata dall'Irq di Rx Chr.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ICPErrInd(uint8_t nData, uint8_t rxErr) {
	ICPDataInd(nData);
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvSendRW
	Funzione di invio dei comandi Ack, Nack e RET.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ICPDrvSendRW(uint8_t nReservedWord) {

	switch(nReservedWord) {
        case ICP_RW_ACK:
        case ICP_RW_NAK:
            break;
        case ICP_RW_RET:
            if(!icpMasterMode)
                return;
            break;
        default:
            return;
    }

	if(!icpMasterMode) {
		ICPHookModeBitSet();
	} else {
		ICPHookModeBitClear();
	}
	ICPHookPutChar(nReservedWord);
	ICPDrvSetComState(COM_IDLE);
	gVars.fTxFrame = 0;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvSendMsg
	Funzione di calcolo CRC e set flags varie per l'invio di un buffer dati.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ICPDrvSendMsg(uint8_t nFrameLen) {
    
	uint8_t i, nChk;

    if(nFrameLen > ICP_MAX_FRAMELEN)
        nFrameLen = ICP_MAX_FRAMELEN;

#if (_IcpMaster_ == true)
//    ICPInfo.aPeriphInfo[nPeriphID].nLastCmd = ICPAddrCommand;
//    ICPInfo.aPeriphInfo[nPeriphID].nLastSubCmd = ICPSubCommand;
#endif

	nChk = 0;
    for(i = 0; i < nFrameLen; i++) {
    	nChk += ICPInfo.buff[i];
    }
	ICPInfo.buff[nFrameLen] = nChk;
	ICPInfo.nFrameLen = nFrameLen + 1;
	ICPInfo.iTX = 0;

	if(!icpMasterMode) {
		ICPInfo.nModebitPos = nFrameLen;
		ICPHookModeBitClear();
	} else {
		ICPInfo.nModebitPos = 0;
		ICPHookModeBitSet();
	}
	ICPInfo.nComDrvState = COM_TX;
	//MR19 ICPHookRxEnd();			// Non piu' usata
	//MR19 ICPHookTxBegin();		// Non piu' usata
	
	// TODO: controllare...
	gVars.fTxFrame = 1;
	ICPHookPutChar(ICPInfo.buff[ICPInfo.iTX++]);
	ICPInfo.nFrameLen--;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvHandleComState
	Funzione chiamata al termine di una trasmissione buffer dati e alla ricezione di un comando.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ICPDrvHandleComState(void) {

	uint8_t nComState = ICPDrvGetComState();

	switch(nComState) {
	case COM_TXFRAME:
	#if (_IcpMaster_ == true)
		if(icpMasterMode) {
			if(ICPInfo.iTX > 1) ICPDrvStartRX();
			else ICPDrvSetComState(COM_IDLE);
			break;
		}
	#endif
	#if (_IcpSlaveCode_)
		if(ICPInfo.nModebitPos) {
			ICPDrvStartRX();																	// Vado in WAITRESP solo se ho trasmesso dati
		}
	#endif
		break;

	case COM_WAITRESP:
		if(ICPTimerTimeout(ICP_TMID_RESPONSE)) {
			#if (_IcpMaster_ == true)
			if(icpSlvCheckNoRespState == 0) *pticpSlvCheckNoRespState = 1;
			#endif
			ICPDrvSetComState(COM_IDLE);														// Timeout scaduto in attesa di risposta: torno a COM_IDLE
		}
		break;

	case COM_RX:
		if(ICPTimerTimeout(ICP_TMID_INTERBYTE)) {
			ICPDrvSetComState(COM_RXEOF);
			ICPTimerStart(ICP_TMID_RESPONSE);													// Ricarico tm per capire se rispondero' in tempo
		}
		break;

	case COM_RXEOF:
		if(ICPDrvTestChecksum()) {
			ICPDrvSetComState(COM_RXFRAME);
			#if (_IcpMaster_ == true)
			if(icpMasterMode) {
				*pticpSlvCheckNoRespState = 0;
				break;
			}
			#endif
			#if (_IcpSlaveCode_)
				if(!ICPInfo.fLink) {
					ICPInfo.fLink = true;
					ICPHook_SetLink(true);
					#if (_IcpSlaveCode_ & ICP_CHANGER)
					ICPProt_CHGSetCoinTypeState();
					#endif
					#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
					ICPProt_BILLSetBillTypeState();
					#endif
				}
				ICPTimerStart(ICP_TMID_NOLINK);
				if(ICPInfo.fSuspendRequest) ICPDrvSetComState(COM_SUSPEND);
				else {
					if (ICPTimerTimeout(ICP_TMID_RESPONSE)) {									// Tempo per preparare e inviare la risposta scaduto
						ICPDrvSetComState(COM_RXCHKERR);										// Rispondo alla prossima chiamata
					}
				}
			#endif
		} else {
/*MR19
	#if (_IcpMaster_ == true)
		#if _IcpTestRetry_
Goto_TxRetry:
		ICPDrvSendRW(ICP_RW_RET);
		ICPDrvSetComState(COM_TXFRAME);
		goto Goto_ComTxFrame;
		#endif
		#endif
*/
		ICPDrvSetComState(COM_IDLE);
		}
		break;

	case COM_SUSPEND:
	#if (_IcpSlaveCode_)
		if(!icpMasterMode) {
			if(!ICPInfo.fSuspendRequest) ICPDrvSetComState(COM_TXFRAME);
		}
	#endif
		break;
	
	case COM_RXTMRESPONSE:
	case COM_RXTMNOLINK:
	case COM_RXCHKERR:
	case COM_RXFRAME:
	case COM_IDLE:
	case COM_MSTIDLE:
	default:
			break;
	}

	#if (_IcpSlaveCode_)
	if(!icpMasterMode) {
		if(ICPInfo.fLink && ICPTimerTimeout(ICP_TMID_NOLINK)) {
			ICPInfo.fLink = false;
			ICPHook_SetLink(FALSE);
		}
		#if (_IcpSmsAlarms_ == true)
		if(!ICPInfo.fMstNoLink && !ICPInfo.fLink && ICPTimerTimeout(ICPInfo.nTmMstNoLink)) {
			ICPInfo.fMstNoLink = true;
			ICPHook_SetMstNoLink(ICPInfo.fMstNoLink);
		}
		if(!ICPInfo.fMstInhibit && (ICPInfo.nSlaveInhMask == ICPInfo.nSlvInhMask) && ICPTimerTimeout(ICPInfo.nTmMstInhibit)) {
			ICPInfo.fMstInhibit = true;
			ICPHook_SetMstInhibit(ICPInfo.fMstInhibit);
		}
		#endif
	}
	#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvGetComState
	Funzione che ritorna lo stato attuale del driver.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
uint8_t ICPDrvGetComState(void) {
	
	return ICPInfo.nComDrvState;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvSetComState
	Funzione che aggiorna lo stato attuale del driver.
	
	IN:	 - nuovo stato driver 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPDrvSetComState(uint8_t nComState) {

	ICPInfo.nComDrvState = nComState;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvStartRX
	Funzione che predispone il driver in ricezione.
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPDrvStartRX(void) {
	
	ICPInfo.nComDrvState = COM_WAITRESP;
	ICPInfo.iRX = 0;
	ICPTimerStart(ICP_TMID_RESPONSE);
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvTestChecksum
	
	IN:	 - 
	OUT: - true se CRC ok, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool ICPDrvTestChecksum(void) {

	uint16_t i, nChkPos;
	uint8_t nChk;
	bool fChkOk = FALSE;

	if(ICPInfo.iRX > 1) {
		nChkPos = ICPInfo.iRX - 1;
		nChk = 0;
		for(i = 0; i < nChkPos; i++) {
			nChk += ICPInfo.buff[i];
		}
		if(nChk == ICPInfo.buff[nChkPos]) {
			fChkOk = TRUE;
			#if (_IcpMaster_ == true)
			//ICPDrvSendRW(ICP_RW_ACK);
			#endif
		} else {
			fChkOk = FALSE;
		}
	} else {
		switch(ICPInfo.nMasterRW) {
		case ICP_RW_ACK:
		case ICP_RW_RET:
			ICPSetFrameAcked(true);
		case ICP_RW_NAK:
			fChkOk = TRUE;
			break;
		default:
			break;
		}
	}
	return fChkOk;
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPDrvTestEndFrame
	Questa funzione, in Slave Mode, e' chiamata ad ogni chr ricevuto dopo il Comando.
	Essa determina il numero di chr (ICPInfo.nFrameLen) che devono ancora essere attesi per 
	completare il comando ricevuto.
	Nella "ICPDataInd", alla ricezione del Comando, si carica "ICPInfo.nFrameLen" con 2 se 
	ricevuto Reset o Poll, mentre si carica ICP_MAX_BUFFSIZE per gli altri comandi che prevedono
	parametri da ricevere.
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPDrvTestEndFrame(bool icpMasterMode) {

	if(icpMasterMode) {
		// ======================================================
		// ========    MASTER     MODE    =======================
		// ======================================================
		// Master: end of frame = modebit set
		if (ICPHookModeBitTest()) {
			ICPInfo.nComDrvState = COM_RXEOF;
			#if (_IcpMaster_ == true)
			/////////ICPInfo.fSlaveRW = (ICPInfo.iRX == 1);
			if(ICPInfo.iRX <= 1) {
				ICPInfo.fSlaveRW = true;
				if(ICPInfo.buff[0] == ICP_RW_NAK)
					ICPInfo.nComDrvState = COM_IDLE;
			} else ICPInfo.fSlaveRW = false;
			#endif
		}
	} else {
		// ======================================================
		// ========    SLAVE   MODE    ==========================
		// ======================================================
	#if (_IcpSlaveCode_)
	#if (_IcpSlaveInterByteTm_ == false)

		if(ICPInfo.nFrameLen >= ICP_MAX_BUFFSIZE) {
			ICPInfo.nFrameLen = 0;
			//																					// Si predispone nFrameLen per ricevere l'esatto numero di chr
			// Sub-Command																		// determinata dal Sub-Command
			//
			if(ICPInfo.iRX == 2) {
				switch(ICPAddr) {																// ICPCommand = (ICPInfo.buff[0] & 0xF8) = test dei 5 bit dell'address 
				#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
					case ICP_USD1_ADDR:
					case ICP_USD2_ADDR:
					case ICP_USD3_ADDR:
						switch(ICPCommand) {
						case ICP_USD_CMD_VEND:
							switch(ICPSubCommand) {
							case ICP_USD_CMD_VENDAPPROVED:
							case ICP_USD_CMD_VENDDENIED:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_VEND00);
								break;
							case ICP_USD_CMD_VENDSEL:
							case ICP_USD_CMD_VENDHOMESEL:
							case ICP_USD_CMD_VENDSELSTATUS:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_VEND02);
								break;
							default:
								break;
							}
							break;
						case ICP_USD_CMD_FUNDS:
							switch(ICPSubCommand) {
							case ICP_USD_CMD_FUNDSCREDIT:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_FUNDS00);
								break;
							case ICP_USD_CMD_FUNDSSELPRICE:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_FUNDS01);
								break;
							default:
								break;
							}
							break;
						case ICP_USD_CMD_EXP:
							switch(ICPSubCommand) {
							case ICP_USD_CMD_EXPREQUESTID:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_EXP00);
								break;
							case ICP_USD_CMD_EXPENABLEOPT:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_EXP01);
								break;
							case ICP_USD_CMD_EXPSENDBLOCKS:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_EXP02);
								break;
							case ICP_USD_CMD_EXPREQBLOCKN:
								ICPInfo.nFrameLen = sizeof(T_USDCMD_EXP04);
								break;
							case ICP_USD_CMD_EXPSENDBLOCKN:
							case ICP_USD_CMD_EXPSENDBLOCK:
							case ICP_USD_CMD_EXPDIAG:
								ICPInfo.nFrameLen = 2 + kUSDExpFFDataSize;
								if(ICPInfo.nFrameLen > (ICP_MAX_BUFFSIZE-1))
									ICPInfo.nFrameLen = ICP_MAX_BUFFSIZE-1;
								break;
							default:
								break;
							}
							break;
						default:
							break;
						}
						break;
				#endif

				#if (_IcpSlaveCode_ & ICP_CHANGER)
					case ICP_CHG_ADDR:
						switch(ICPCommand) {
						case ICP_CHG_CMD_EXP:
							switch(ICPSubCommand) {
							case ICP_CHG_CMD_EXPREQUESTID:
							case ICP_CHG_CMD_EXPPAYOUTSTATUS:
							case ICP_CHG_CMD_EXPPAYOUTPOLL:
							case ICP_CHG_CMD_EXPDIAGSTATUS:
							case ICP_CHG_CMD_EXPMANFILLREP:
							case ICP_CHG_CMD_EXPMANPAYOUTREP:
								ICPInfo.nFrameLen = sizeof(T_CHGCMD_EXP00);
								break;
							case ICP_CHG_CMD_EXPENABLEOPT:
								ICPInfo.nFrameLen = sizeof(T_CHGCMD_EXP01);
								break;
							case ICP_CHG_CMD_EXPPAYOUTVAL:
								ICPInfo.nFrameLen = sizeof(T_CHGCMD_EXP02);
								break;
							case ICP_CHG_CMD_EXPDIAG:
								ICPInfo.nFrameLen = ICP_MAX_BUFFSIZE;		//?!
								#if kModifMDB_RestomatExpFFCHG
								ICPInfo.nFrameLen = 2 + 1 + sizeof(uint16_t);
								#endif	//kModifMDB_RestomatExpFFCHG
								break;
							default:
								break;
							}
							break;
						default:
							break;
						}
						break;
				#endif

				#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
					case ICP_BILL_ADDR:
						switch(ICPCommand) {
						case ICP_BILL_CMD_EXP:
							switch(ICPSubCommand) {
							case ICP_BILL_CMD_EXPREQUESTID1:
								ICPInfo.nFrameLen = sizeof(T_BILLCMD_EXP00);
								break;
							case ICP_BILL_CMD_EXPREQUESTID2:
								ICPInfo.nFrameLen = sizeof(T_BILLCMD_EXP02);
								break;
							case ICP_BILL_CMD_EXPENABLEOPT:
								ICPInfo.nFrameLen = sizeof(T_BILLCMD_EXP01);
								break;
							case ICP_BILL_CMD_EXPDIAG:
								ICPInfo.nFrameLen = ICP_MAX_BUFFSIZE;		//?!
								break;
							default:
								break;
							}
							break;
						default:
							break;
						}
						break;
				#endif

				#if (_IcpSlaveCode_ & ICP_CARDREADER)
					case ICP_CPC_ADDR:
						switch(ICPCommand) {													// ICPCommand = (ICPInfo.buff[0] & 0x07) = test dei 3 bit del comando 
						case ICP_CPC_CMD_SETUP:
							switch(ICPSubCommand) {												// ICPSubCommand = (ICPInfo.buff[1]
							case ICP_CPC_CMD_SETUPID:
								ICPInfo.nFrameLen = sizeof(T_CPCCMD_SETUP00);
								break;
							case ICP_CPC_CMD_SETUPMAXMINPRICE:
								{
								T_CPCCMD_SETUP01 *ptCmd;
								ICPInfo.nFrameLen = 2;
								if(ICPProt_DrvGetLevel(ICP_CPC_ADDR) <= 2)
									ICPInfo.nFrameLen += sizeof(ptCmd->Setup01.L1);
								else
									ICPInfo.nFrameLen += sizeof(ptCmd->Setup01.L3);
								}
								break;
							default:
								break;
							}
							break;

						case ICP_CPC_CMD_VEND:
							switch(ICPSubCommand) {
							case ICP_CPC_CMD_VENDREQUEST:
							case ICP_CPC_CMD_VENDCASHSALE:
							case ICP_CPC_CMD_NEGVENDREQUEST:
								{
								T_CPCCMD_VENDxx *ptCmd;
								ICPInfo.nFrameLen = 2;
								if(ICPProt_DrvGetLevel(ICP_CPC_ADDR) <= 2)
									ICPInfo.nFrameLen += sizeof(ptCmd->Vend.L1);
								else
									ICPInfo.nFrameLen += sizeof(ptCmd->Vend.L3);
								}
								break;
							case ICP_CPC_CMD_VENDCANCEL:
							case ICP_CPC_CMD_VENDFAILURE:
							case ICP_CPC_CMD_VENDCOMPLETE:										// Session Cancel Request
								ICPInfo.nFrameLen = sizeof(T_CPCCMD_VEND01);
								break;
							case ICP_CPC_CMD_VENDSUCCESS:
								ICPInfo.nFrameLen = sizeof(T_CPCCMD_VEND02);
								break;
							default:
								break;
							}
							break;

						case ICP_CPC_CMD_READER:
							switch(ICPSubCommand) {
							case ICP_CPC_CMD_READERDISABLE:
							case ICP_CPC_CMD_READERENABLE:
							case ICP_CPC_CMD_READERCANCEL:
								ICPInfo.nFrameLen = sizeof(T_CPCCMD_READER00);
								break;
							case ICP_CPC_CMD_READERDATAENTRY:
								//ICPInfo.nFrameLen = sizeof(T_CPCCMD_READER03);
								//break;
							default:
								break;
							}
							break;

						case ICP_CPC_CMD_REVALUE:
							switch(ICPSubCommand) {
							case ICP_CPC_CMD_REVALUEREQUEST:
								{
								T_CPCCMD_REVAL00 *ptCmd;
								ICPInfo.nFrameLen = 2;
								if(ICPProt_DrvGetLevel(ICP_CPC_ADDR) <= 2)
									ICPInfo.nFrameLen += sizeof(ptCmd->Revalue.L1);
								else
									ICPInfo.nFrameLen += sizeof(ptCmd->Revalue.L3);
								}
								break;
							case ICP_CPC_CMD_REVALUELIMIT:
								ICPInfo.nFrameLen = sizeof(T_CPCCMD_REVAL01);
								break;
							default:
								break;
							}
							break;

						case ICP_CPC_CMD_EXP:
							switch(ICPSubCommand) {
							case ICP_CPC_CMD_EXPREQUESTID:
								{
								T_CPCCMD_EXP00 *ptCmd;
								ICPInfo.nFrameLen = 2;
								if(ICPProt_DrvGetLevel(ICP_CPC_ADDR) <= 2)
									ICPInfo.nFrameLen += sizeof(ptCmd->Exp00.L1);
								else
									ICPInfo.nFrameLen += sizeof(ptCmd->Exp00.L3);
								}
								break;
							case ICP_CPC_CMD_EXPWRITETIMEDATE:
								ICPInfo.nFrameLen = sizeof(T_CPCCMD_EXP03);
								break;
							case ICP_CPC_CMD_EXPENABLEOPT:
								ICPInfo.nFrameLen = sizeof(T_CPCCMD_EXP04);
								break;
							// FTL commands...
							case ICP_CPC_CMD_EXPDIAG:
								ICPInfo.nFrameLen = ICP_MAX_BUFFSIZE;							// ?!
								break;
							case ICP_CPC_CMD_EXPREADUSERFILE:									// obsolete cmd
							case ICP_CPC_CMD_EXPWRITEUSERFILE:									// obsolete cmd
							default:
								break;
							}
							break;

						default:
							break;
						}
						break;
				#endif	// End (_IcpSlaveCode_ & ICP_CARDREADER)

					default:																	// Default se non e' indirizzo di una periferica abilitata
						break;
					}  // End switch(ICPAddr)
					if(ICPInfo.nFrameLen) ICPInfo.nFrameLen++;
			}
			
		} // End Test (ICPInfo.nFrameLen >= ICP_MAX_BUFFSIZE)
		if(ICPInfo.iRX && (ICPInfo.iRX == ICPInfo.nFrameLen)) {									// Ricevuto ultimo dei "ICPInfo.nFrameLen" attesi: se cardreader eseguo la ICPProt_DispatchCmds
			ICPInfo.nComDrvState = COM_RXEOF;
			ICPTimerStart(ICP_TMID_RESPONSE);													// Ricarico "ICPInfo.nTmResponse" per capire se rispondero' in tempo
			
#if (_IcpSlaveCode_ != ICP_CARDREADER)
			ICPProt_DispatchCmds();																// Elaboro comando e dati ricevuti dal Master
#endif
		}
#endif 	// End (_IcpSlaveInterByteTm_ == false)
#endif
	}	// End Test Slave/master
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPTimerStart
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void ICPTimerStart(uint16_t nTimerID) {

	switch(nTimerID) {

	case ICP_TMID_INTERBYTE0:
		Tmr32Start(ICPInfo.nTmInterByte, 0);
		break;

	case ICP_TMID_INTERBYTE:
		TmrStart(ICPInfo.nTmInterByte, ICP_TM_INTERBYTE);
		#if(kIcpRs232Support == true)
			if(icpRS232Mode) {
				TmrStart(ICPInfo.nTmInterByte, kICPRs232_TM_INTERBYTE);
			}
		#endif
		break;

	case ICP_TMID_LINEBREAK:
		break;

	case ICP_TMID_RESPONSE:
		if(icpMasterMode)
			TmrStart(ICPInfo.nTmResponse, ICP_TM_RESPONSE);
		else
			TmrStart(ICPInfo.nTmResponse, ICP_TM_RESPONSESLV);
		#if(kIcpRs232Support == true)
			if(icpRS232Mode) {
				TmrStart(ICPInfo.nTmResponse, kICPRs232_TM_RESPONSE);
			}
		#endif
		break;

	#if (_IcpMaster_ == true)
	case ICP_TMID_SETUP:
		TmrStart(ICPInfo.nTmSetup, ICP_TM_SETUP);
		break;
	case ICP_TMID_POLLPERIPH:
		TmrStart(ICPInfo.nTmPoll, ICP_TM_POLLPERIPH);
		break;
	case ICP_TMID_CHGMAXNORESP:
		Tmr32Start(pticpSlvInfo->nTmMaxNoResp, ICP_TM_CHGMAXNORESP);
		break;
	case ICP_TMID_BILLMAXNORESP:
		Tmr32Start(pticpSlvInfo->nTmMaxNoResp, ICP_TM_BILLMAXNORESP);
		break;
	case ICP_TMID_CPCMAXNORESP:
		Tmr32Start(pticpSlvInfo->nTmMaxNoResp, ICP_TM_CPCMAXNORESP);
		break;
	case ICP_TMID_USDMAXNORESP:
		Tmr32Start(pticpSlvInfo->nTmMaxNoResp, ICP_TM_USDMAXNORESP);
		break;
	case ICP_TMID_CHGRESET:
	case ICP_TMID_BILLRESET:
	case ICP_TMID_CPCRESET:
	case ICP_TMID_USDRESET:
		Tmr32Start(pticpSlvInfo->nTmMaxNoResp, ICP_TM_NORESPRESET);
		break;
	//$USD$#else
	#endif
	
#if (_IcpSlaveCode_)
	case ICP_TMID_NOLINK:
		TmrStart(ICPInfo.nTmNoLink, ICP_TM_NOLINK);												// Timeout No Link
		break;
#endif

	default:
		break;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ICPTimerTimeout
	Funzione che controlla se un timer e' scaduto.
	
	IN:	 - Timer da controllare
	OUT: - fElapsed true se timer scaduto, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool ICPTimerTimeout(uint16_t nTimerID) {
	
	bool fElapsed = FALSE;

	switch(nTimerID) {
	case ICP_TMID_INTERBYTE:
		fElapsed= TmrTimeout(ICPInfo.nTmInterByte);
		break;
	case ICP_TMID_LINEBREAK:
		break;
	case ICP_TMID_RESPONSE:
		fElapsed= TmrTimeout(ICPInfo.nTmResponse);
		break;
	#if (_IcpMaster_ == true)
	case ICP_TMID_SETUP:
		fElapsed= TmrTimeout(ICPInfo.nTmSetup);
		break;
	case ICP_TMID_POLLPERIPH:
		fElapsed= TmrTimeout(ICPInfo.nTmPoll);
		break;
	case ICP_TMID_CHGMAXNORESP:
	case ICP_TMID_BILLMAXNORESP:
	case ICP_TMID_CPCMAXNORESP:
	case ICP_TMID_USDMAXNORESP:
	case ICP_TMID_CHGRESET:
	case ICP_TMID_BILLRESET:
	case ICP_TMID_CPCRESET:
	case ICP_TMID_USDRESET:
		fElapsed=  TmrTimeout(pticpSlvInfo->nTmMaxNoResp);
		break;
	//$USD$#else
	#endif
	#if (_IcpSlaveCode_)
	case ICP_TMID_NOLINK:
		fElapsed= TmrTimeout(ICPInfo.nTmNoLink);
		break;
	#endif
		default:
			break;
	}

	return fElapsed;
}


/*---------------------------------------------------------------------------------------------*\
Method: TestTimeoutNoLink
	Controlla se e' scaduto il Timeout di comunicazione col Master.
 	Da utilizzare solo in MDB Slave.

	IN:		- 
	OUT:	-  Set "fVMC_NoLink" se timeout scaduto
\*----------------------------------------------------------------------------------------------*/
void  TestTimeoutNoLink(void) {

#if (_IcpSlaveCode_)
	if(!icpMasterMode) {
		if(ICPInfo.fLink && ICPTimerTimeout(ICP_TMID_NOLINK)) {
			ICPInfo.fLink = false;
			ICPHook_SetLink(FALSE);
		}
	}
#endif
}


