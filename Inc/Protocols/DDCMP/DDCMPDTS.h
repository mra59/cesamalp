/*--------------------------------------------------------------------------------------*\
File:
	DDCMPDTS.h

Description:
	Handles EVA-DTS standard commands using DDCMP protocol

History:
	Date	   Aut	Note
	---------- ---	----


\*--------------------------------------------------------------------------------------*/

#ifndef __DDCMPDTS__
#define __DDCMPDTS__

/*--------------------------------------------------------------------------------------*\
Customization constants
\*--------------------------------------------------------------------------------------*/
#ifndef kDDCMPDTSBuffSize
#define kDDCMPDTSBuffSize		130
	// DDCMP messages buffer. The maximum number of applicative data bytes that can be
	// sent/received in a single packet is kDDCMPDTSBuffSize-2
#endif

#ifndef kDDCMPDTSUniversalPwd
#define kDDCMPDTSUniversalPwd	0xffff
	// This password is always accepted as valid
#endif

#ifndef kDDCMPDTSRTCSupport
#define kDDCMPDTSRTCSupport		true
	// Support for date and time
#endif

#if !defined(kDDCMPDTSMasterSupport) && !defined(kDDCMPDTSSlaveSupport)
#define kDDCMPDTSMasterSupport	false		
#define kDDCMPDTSSlaveSupport	true
	// If nothing defined, support transfers in master and slave mode
#endif

#ifndef kDDCMPDTSMasterSupport
#define kDDCMPDTSMasterSupport	false
	// Support transfers in master mode
#endif

#ifndef kDDCMPDTSSlaveSupport
#define kDDCMPDTSSlaveSupport	false
	// Support transfers in slave mode
#endif

#ifndef kDDCMPDTSConnReqSupport
#define kDDCMPDTSConnReqSupport	false
	// Allows complete control over a connection request from master or slave
#endif

#ifndef kDDCMPDTSConnReqMiBCDPacketSupport
#define kDDCMPDTSConnReqMiBCDPacketSupport	false
	// Allows complete control over a connection request from master by data 
	// transferred as bcd packet format (used where application needs to reduce
	// the code at max possible)
#endif

#ifndef kDDCMPDTSConnReqMiSupport
#define kDDCMPDTSConnReqMiSupport	kDDCMPDTSConnReqSupport
	// Allows complete control over a connection request from master
#endif

#ifndef kDDCMPDTSConnReqSiSupport
#define kDDCMPDTSConnReqSiSupport	kDDCMPDTSConnReqSupport
	// Allows complete control over a connection request from slave
#endif


/*--------------------------------------------------------------------------------------*\
Private types & constants
\*--------------------------------------------------------------------------------------*/
typedef enum {
  kDDCMPDTSClosed,						//	0
  kDDCMPDTSInit,						//	1
  kDDCMPDTSStartMaster,					//	2
  kDDCMPDTSStartSlave,					//	3
  kDDCMPDTSStartSlaveSwap,				//	4
  kDDCMPDTSStartingMaster,				//	5
  kDDCMPDTSStartingSlave,				//	6
  kDDCMPDTSSwapMode,					//	7
  kDDCMPDTSMaster,						//	8
  kDDCMPDTSMaster_WaitCmd,				//	9
  kDDCMPDTSMaster_TxWAY,				//	10	0x0A
  kDDCMPDTSMaster_WaitWAYRsp,			//	11	0X0B
  kDDCMPDTSMaster_ConnRejected,			//	12	0X0C
  kDDCMPDTSMaster_WaitWriteRsp,			//	13	0X0D
  kDDCMPDTSMaster_WaitReadRsp,			//	14	0X0E
  kDDCMPDTSMaster_WaitDeleteRsp,		//	15	0X0F
  kDDCMPDTSMaster_TxData,				//	16	0X10
  kDDCMPDTSMaster_RxData,				//	17	0X11
  kDDCMPDTSMaster_RxMemory,				//	18	0X12

  kDDCMPDTSSlave,						//	19	0X13
  kDDCMPDTSSlave_WaitWAYReq,			//	20	0X14
  kDDCMPDTSSlave_WaitCmd,				//	21	0X15
  kDDCMPDTSSlave_TxData,				//	22	0X16
  kDDCMPDTSSlave_RxData,				//	23	0X17

  kDDCMPDTSConnClosing,					//	24	0X18
  kDDCMPDTSConnClosed 					//	25	0X19
} DDCMPDTSState;



#define DDCMPDTSConnIsOpen()  (sDTSVars.state>=kDDCMPDTSSwapMode && sDTSVars.state<kDDCMPDTSConnClosed)
  // Return true if current state corresponds to an open DDCMP connection

#define DDCMPDTSConnIsSlave() (sDTSVars.state>=kDDCMPDTSSlave)
  // Return true if current open connection is a Slave connection

typedef enum {
//  kDDCMPDTSMasterConnectRetryTO=  1500,
// FP 18/12/2006 ridotto per problemi su OSCAR CU
  kDDCMPDTSMasterConnectRetryTO=  700,
    // Time between two master connection requests

  kDDCMPDTSConnFailRetryTO=   5000,
    // Time between following connection attempt when connection fails
  kDDCMPDTSSlaveConnAbortTO=    1000,
  kDDCMPDTSSlaveConnAbortRemTO= 7000,
    // Max time allowed to establish a slave connection. Afterwards returns to master mode
  kDDCMPDTSConnFinishRetryTO=   5000,
    // Time between following connection attempt when connection completed succesfully
  kDDCMPDTSConnRejectedRetryTO= 5000
    // Time between following connection attempt when connection rejected
} kDDCMPDTSTO;

enum {
  kDDCMPDTSDataHdrSize= 2,
  kDDCMPDTSBuffSizeExt= 8+2
    // Our DDCMP implementation put packet header (8) and CRC (2) in a separate area, so
    // in effect our buffer is a bit larger than kDDCMPDTSBuffSize
};

#define kDDCMPDTSLenUnknown	0xffff
	// Use this constant to specify an unknown data len

#define kDDCMPDTSLenAll		0x0000
	// Use this constant as data len to specify the complete file

// DDCMPDTS lists
typedef enum {
	kDDCMPDTSListNull=				0,
	kDDCMPDTSListAuditCollection=	1,
	kDDCMPDTSListSecurityRead=		2,
	kDDCMPDTSListDump=				50,
	kDDCMPDTSListConfiguration=		64
} DDCMPDTSList;

// DDCMPDTS open mode
typedef enum {
	kDDCMPDTSModeMaster=			0x1,
	kDDCMPDTSModeSlave=	 			0x2,
	kDDCMPDTSModeMasterSlave=		kDDCMPDTSModeMaster+kDDCMPDTSModeSlave,
	kDDCMPDTSModeRemote=	 		0x10
} DDCMPDTSMode;

// DDCMP master information
typedef struct {
	uint16_t	securityCode;
	uint16_t	passCode2;
	uint16_t	userId;
	uint8_t	userClass;
	uint8_t	year;
	uint8_t	month;
	uint8_t	day;
	uint8_t	hours;
	uint8_t	minutes;
	uint8_t	seconds;
} DDCMPDTSMasterInfo;

// DDCMP slave information
typedef struct {
	uint16_t	securityCode;
	uint16_t	passCode2;
	uint8_t	swVersion;
	uint8_t	manufacturerId;
	uint16_t	buffSize;			// This is the kDDCMPDTSBuffSize on the slave
	char	serialNumber[8];
} DDCMPDTSSlaveInfo;

typedef struct {
  DDCMPDTSMode  mode;
  uint8_t     	state;
  Tmr32Local    tmr;
  char*     	buff;
  TaskId      	taskId; 
  uint16_t      	buffSize;
  uint8_t     	dataBlockNum;
  #if kDDCMPExtendedAddrSupport
  bool      useExtAddr;     // Use extended address
  DDCMPExtendedAddr extAddr;      // Extended address
  #endif
} DDCMPDTSVars;

/*--------------------------------------------------------------------------------------*\
Interface methods
\*--------------------------------------------------------------------------------------*/

void DDCMPDTSTask(void);

void DDCMPDTSInit(void);
	// Initialize DDCMP DTS handler

void DDCMPDTSOpen(DDCMPDTSMode mode, char* ddcmpBuff);
	// Starts DDCMP DTS handler.
	// ddcmpBuff must point to a buffer of at least kDDCMPDTSBuffSize bytes.

void DDCMPDTSClose(void);
	// Close DDCMP DTS handler. Any data transfer in progress is aborted, and memory is released

void DDCMPDTSRun(void);
	// If tasks not used call this periodically to run DDCMPDTS

void DDCMPDTSCmdReadData(uint8_t listNumber, uint16_t byteOffset, uint16_t length);
	// Send ReadData command.

void DDCMPDTSCmdWriteData(uint8_t listNumber, uint16_t length);
	// Send WriteData command.

void DDCMPDTSCmdDeleteData(uint8_t listNumber, uint8_t recordNumber);
	// Send DeleteData command.

void DDCMPDTSCmdEnd(void);
	// Terminate current command without sending a new one
	// In master mode, or if no command in progress, also closes connection

void DDCMPDTSCmdDone();
	// Current rx command completed but we still don't know if we have another cmd. [Master mode only]
	// Must be followed by DDCMPDTSCmdXXX() or DDCMPDTSCmdEnd()


void DDCMPDTSDataTx(uint16_t dataLen, bool dataEnd);
	// Can be called inside or after a DDCMPDTSHookDataTxInd() to send a data message.
	// dataLen is the size of the application data to transmit

void DDCMPDTSDataRx(void);
	// Must be called inside or after a DDCMPDTSHookDataRxInd() to indicate that
	// we have processed the message received and we are ready to accept a new one.


void DDCMPDTSExtendedAddrSet(const uint8_t extendedAddress[8]);
	// Available only if kDDCMPExtendedAddrSupport
	// Can be called before DDCMPDTSOpen() to set DDCMP extended address.
	// This is used as the remote address for master connections,
	// as the local address for slave connections.
	// If extendedAddress is nil, use standard addressing

/*--------------------------------------------------------------------------------------*\
Private methods
\*--------------------------------------------------------------------------------------*/
/// static TaskPhase DDCMPDTSTask(TaskPhase fase, TaskArg arg);
static void DDCMPDTSCmdReqInd(uint8_t* frameData, uint16_t frameLen);
static void DDCMPDTSCmdRspInd(uint8_t* frameData, uint16_t frameLen);
static void DDCMPDTSDataInd(uint8_t* frameData, uint16_t frameLen, bool selectFlag);

// Master mode
#if kDDCMPDTSMasterSupport
	static void DDCMPDTSWAYSend(void);
	static void DDCMPDTSWAYRspInd(uint8_t* frameData, uint16_t frameLen);
	static void DDCMPDTSReadRspInd(uint8_t* frameData, uint16_t frameLen);
	static void DDCMPDTSWriteRspInd(uint8_t* frameData, uint16_t frameLen);
	static void DDCMPDTSDeleteRspInd(uint8_t* frameData, uint16_t frameLen);
	static void DDCMPDTSRspEnRx(void);
#endif

static void DDCMPDTSWAYReqInd(uint8_t* frameData, uint16_t frameLen);

static void DDCMPDTSReadReqInd(uint8_t* frameData, uint16_t frameLen);
static void DDCMPDTSWriteReqInd(uint8_t* frameData, uint16_t frameLen);


static void DDCMPDTSSendNextDataFrame(void);

static void DDCMPDTSBadCmdInd(DDCMPCmd cmd);
static void DDCMPDTSBadRspInd(DDCMPCmd cmd);

static void DDCMPDTSConnEnd(void);

static void DDCMPDTSReceive(void);
static void DDCMPDTSDeleteReqInd(uint8_t* frameData, uint16_t frameLen);


#define DDCMPStateSet(newState) (sDTSVars.state= (newState))


/*--------------------------------------------------------------------------------------*\
External hooks methods
	These routines are product-specific, so must be provided in some other module
	They are the interface between this module and the rest of the system
\*--------------------------------------------------------------------------------------*/
#if kDDCMPDTSConnReqMiSupport
extern void DDCMPDTSHookConnEstablishedMasterInd(const DDCMPDTSSlaveInfo* slaveInfo);
#else
extern void DDCMPDTSHookConnEstablishedMasterInd(void);
#endif
	// Connection established in master mode

extern void DDCMPDTSHookConnEstablishedSlaveInd(void);
	// Connection established in slave mode

extern void DDCMPDTSHookConnRejectedInd(void);
	// Connection was rejected by slave device

extern void DDCMPDTSHookConnFinishedInd(void);
	// Connection completed succesfully

extern void DDCMPDTSHookConnFailedInd(void);
	// Connection aborted/failed

extern uint16_t DDCMPDTSHookPwdGet(void);
	// Return access password (needed if !kDDCMPDTSConnReqSupport)

extern void DDCMPDTSHookMasterInfoGet(DDCMPDTSMasterInfo* mi);
	// Get master information when sending a WhoAreYou (needed if kDDCMPDTSConnReqSupport)
	// The mi struct is initialized with default information. Custom values can provided by this routine

extern bool DDCMPDTSHookConnReqInd(const DDCMPDTSMasterInfo* mi, DDCMPDTSSlaveInfo* si);
	// Connection request indication (needed only if kDDCMPDTSConnReqSupport)
	// Called in slave mode when a connection request is received from master
	// The si struct is initialized with default information. Custom values can provided by this routine
	// Must return true to accept the connection, false to reject it

extern bool DDCMPDTSHookReadDataReqInd(uint8_t listNumber, uint16_t byteOffset, uint16_t length, uint16_t* actualLen);
	// Handle reception of a ReadData request
	// actualLen <- actual data len if known, else do not modify
	// Returns true if read command accepted

extern void DDCMPDTSHookReadDataRspInd(bool accepted, uint16_t length);
	// Handle reception of a ReadData response

extern void DDCMPDTSHookDataRxInd(uint8_t dataBlockNum, uint8_t* applData, uint16_t applDataLen, bool dataEnd);
	// Handle reception of next data frame
	// Reply with:				If
	//	DDCMPDTSDataRx()		We are ready to accept next data packet
	//	DDCMPDTSCmdXXX()		Rx command completed and we have another command to send [Master mode only]
	//	DDCMPDTSCmdEnd()		Rx command completed and nothing else to do
	//	DDCMPDTSCmdDone()		Rx command completed but we still don't know if we have another cmd. [Master mode only]
	//							Must be followed by DDCMPDTSCmdXXX() or DDCMPDTSCmdEnd()
	// The reply routine must be called after the received data has been processed.
	// It can be called either inside the hook routine, or later if data processing is done in background


extern bool DDCMPDTSHookWriteDataReqInd(uint8_t listNumber, uint16_t length);
	// Handle reception of a WriteData request
	// Returns true if write command accepted

extern void DDCMPDTSHookWriteDataRspInd(bool accepted);
	// Handle reception of a WriteData response

extern void DDCMPDTSHookDataTxInd(uint8_t dataBlockNum, char* applDataBuff, uint16_t dataBuffSize);
	// Handle transmission of next data frame, or next command
	// Reply with:				If
	//	DDCMPDTSDataTx(len,end)	We have another data packet to send
	//	DDCMPDTSCmdXXX()		Tx command completed and we have another command to send [Master mode only]
	//	DDCMPDTSCmdEnd()		Tx command completed and nothing else to do
	// The reply routine can be called either inside the hook routine,
	// or later on if data packet is built in background


extern bool DDCMPDTSHookDeleteDataReqInd(uint8_t listNumber, uint8_t recordNumber);
	// Handle reception of a DeleteData request
	// Returns true if delete command accepted

extern void DDCMPDTSHookDeleteDataRspInd(bool accepted);
	// Handle reception of a DeleteData response

extern bool DDCMPDTSHookSlaveRelease(void);
	// returns true if need to close and reopen to release line CU

#endif

