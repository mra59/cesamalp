#ifndef __IcpProt__
#define __IcpProt__

#define ICP_ORION
#define kReq456Support true

#define ICP_MASTER			0x00
#define ICP_CHANGER			0x01
#define ICP_CARDREADER		0x02
#define ICP_BILLVALIDATOR	0x04
#define ICP_USD1			0x08
#define ICP_USD2			0x10
#define ICP_USD3			0x20


#ifndef _IcpSmsAlarms_
#define _IcpSmsAlarms_ false
#endif

#define _IcpMaster_ kICPMasterSupport

#define _IcpSlave_ 	(							\
	kICPSlaveCGSupport*ICP_CHANGER |			\
	kICPSlaveBVSupport*ICP_BILLVALIDATOR |		\
	kICPSlaveCPCSupport*ICP_CARDREADER | 		\
	kICPSlaveUSDSupport*ICP_USD1 |				\
	kICPSlaveUSDSupport*ICP_USD2 |				\
	kICPSlaveUSDSupport*ICP_USD3 				\
)


#ifndef _IcpChgLevel_
#define _IcpChgLevel_ 3
#endif
#ifndef _IcpBillLevel_
#define _IcpBillLevel_ 1
#endif
#ifndef _IcpCpcLevel_
#define _IcpCpcLevel_ 2
#endif
#ifndef _IcpUsdLevel_
#define _IcpUsdLevel_ 1
#endif
#ifndef _IcpVmcLevel_
#define _IcpVmcLevel_ _IcpCpcLevel_
#endif
#ifndef _IcpVmcDisplayCol_
#define _IcpVmcDisplayCol_ 0
#endif
#ifndef _IcpVmcDisplayRow_
#define _IcpVmcDisplayRow_ 0
#endif
#ifndef _IcpVmcDisplayType_
#define _IcpVmcDisplayType_ 0
#endif

#define _IcpChgFTL_ 0
#define _IcpBillFTL_ 0
#define _IcpCpcFTL_ 0
#define _IcpVmcFTL_ 0
#define _IcpUsdFTL_ 0

#ifndef kICPSlaveUsd1Mode1
#define kICPSlaveUsd1Mode1 true
#endif
#ifndef kICPSlaveUsd1Mode2
#define kICPSlaveUsd1Mode2 true
#endif
#ifndef kICPSlaveUsd2Mode1
#define kICPSlaveUsd2Mode1 true
#endif
#ifndef kICPSlaveUsd2Mode2
#define kICPSlaveUsd2Mode2 true
#endif
#ifndef kICPSlaveUsd3Mode1
#define kICPSlaveUsd3Mode1 true
#endif
#ifndef kICPSlaveUsd3Mode2
#define kICPSlaveUsd3Mode2 true
#endif
#define _IcpUsd1Mode1_ (kICPSlaveUsd1Mode1*0x01)
#define _IcpUsd1Mode2_ (kICPSlaveUsd1Mode2*0x02)
#define _IcpUsd2Mode1_ (kICPSlaveUsd2Mode1*0x01)
#define _IcpUsd2Mode2_ (kICPSlaveUsd2Mode2*0x02)
#define _IcpUsd3Mode1_ (kICPSlaveUsd3Mode1*0x01)
#define _IcpUsd3Mode2_ (kICPSlaveUsd3Mode2*0x02)


#ifndef kICPSlaveCGCode
#define kICPSlaveCGCode true
#endif
#ifndef kICPSlaveBVCode
#define kICPSlaveBVCode true
#endif
#ifndef kICPSlaveCPCCode
#define kICPSlaveCPCCode true
#endif
#ifndef kICPSlaveUSDCode
#define kICPSlaveUSDCode true
#endif
#define _IcpSlaveCode_ 	(						\
	kICPSlaveCGSupport*kICPSlaveCGCode*ICP_CHANGER |				\
	kICPSlaveBVSupport*kICPSlaveBVCode*ICP_BILLVALIDATOR |			\
	kICPSlaveCPCSupport*kICPSlaveCPCCode*ICP_CARDREADER | 			\
	kICPSlaveUSDSupport*kICPSlaveUSDCode*ICP_USD1 |					\
	kICPSlaveUSDSupport*kICPSlaveUSDCode*ICP_USD2 |					\
	kICPSlaveUSDSupport*kICPSlaveUSDCode*ICP_USD3 					\
)


typedef uint16_t CHGCreditType;
typedef uint16_t BILLCreditType;
typedef uint16_t CPCCreditType_L1;
typedef CPCCreditType_L1 CPCCreditType_L2;
typedef uint32_t CPCCreditType_L3;
typedef uint16_t USDCreditType_L1;
typedef uint32_t USDCreditType_L3;


typedef void (*T_PSLVFUNCT)(void);

//TODO: gi� definiti in PE_Types.h
/**
#define TRUE 1
#define FALSE 0
#define NULL 0
**/

#define ICP_ADDR_MASK 0xF8
#define ICP_CMD_MASK 0x07


#define ICP_MAX_BUFFSIZE 36
#define ICP_MAX_FRAMELEN (ICP_MAX_BUFFSIZE - 1)
#define ICP_MAX_COMMANDS 8

#define ICPAddrCommand ICPInfo.buff[0]
#define ICPSubCommand ICPInfo.buff[1]
#define ICPAddr ICPInfo.buff[0] & ICP_ADDR_MASK
#define ICPCommand ICPInfo.buff[0] & ICP_CMD_MASK


#if (kICPSlaveUSDSupport)
#define ICP_MAX_PERIPH (4+3)
#else
#define ICP_MAX_PERIPH 4
#endif




#define ICP_TM_SLAVETASK 20

#define ICP_TM_LINEBREAK 100
#define ICP_TM_SETUP 200
#define ICP_TM_NORESPRESET 10000

#ifndef ICP_TM_INTERBYTE
#define ICP_TM_INTERBYTE 20
#endif
#ifndef ICP_TM_RESPONSE
#define ICP_TM_RESPONSE 50
#endif

#ifndef ICP_TM_RESPONSESLV
#define ICP_TM_RESPONSESLV 5
#endif

#ifndef ICP_TM_NOLINK
//#define ICP_TM_NOLINK 2000
#define ICP_TM_NOLINK 10000							// 09.17 Portato da 2 sec originali a 10 sec
#endif

#ifndef ICP_TM_POLLPERIPH
#define ICP_TM_POLLPERIPH 50						// 09.17 Portato da 65 originali a 50 msec
#endif

#ifndef T_POLL_EXP05		//tempo invio (*0,1 sec) EXP 0F-05 --> Request Send Diagnostic Status per CHG livello 3 con opzione enable
#define T_POLL_EXP05 20
#endif

#ifndef ICP_TM_CHGWAITINIT
#define ICP_TM_CHGWAITINIT 500
#endif
#ifndef ICP_TM_BILLWAITINIT
#define ICP_TM_BILLWAITINIT 500
#endif
#ifndef ICP_TM_CPCWAITINIT
#define ICP_TM_CPCWAITINIT 500
#endif
#ifndef ICP_TM_USDWAITINIT
#define ICP_TM_USDWAITINIT 500
#endif

#ifndef ICP_TM_CHGMAXNORESP
#define ICP_TM_CHGMAXNORESP 5000
#endif
#ifndef ICP_TM_BILLMAXNORESP
#define ICP_TM_BILLMAXNORESP 5000
#endif
#ifndef ICP_TM_CPCMAXNORESP
#define ICP_TM_CPCMAXNORESP 5000
#endif
#ifndef ICP_TM_USDMAXNORESP
#define ICP_TM_USDMAXNORESP 5000
#endif

#ifndef ICP_TM_MSTINH
#define ICP_TM_MSTINH (5*60000)
#endif
#ifndef ICP_TM_MSTNL
#define ICP_TM_MSTNL (5*60000)
#endif
#ifndef ICP_TM_SLVNL
#define ICP_TM_SLVNL (1*60000)
#endif


#define ICP_TMID_INTERBYTE 		0x0001
#define ICP_TMID_RESPONSE 		0x0002
#define ICP_TMID_LINEBREAK 		0x0004
#define ICP_TMID_SETUP 			0x0010
#define ICP_TMID_NOLINK 		0x0020
#define ICP_TMID_CHGMAXNORESP 	0x0040
#define ICP_TMID_CHGRESET 		0x0080
#define ICP_TMID_BILLMAXNORESP 	0x0100
#define ICP_TMID_BILLRESET 		0x0200
#define ICP_TMID_CPCMAXNORESP 	0x0400
#define ICP_TMID_CPCRESET 		0x0800
#define ICP_TMID_USDMAXNORESP 	0x1000
#define ICP_TMID_USDRESET 		0x2000
#define ICP_TMID_POLLPERIPH 	0x8000


//kModifMDB_OptRS232
#define ICP_TMID_INTERBYTE0 		0xFFFF
//kModifMDB_OptRS232


#define ICP_RW_ACK 					0x00
#define ICP_RW_NAK 					0xFF
#define ICP_RW_RET 					0xAA


#define ICP_RESPTYPE_NONE 			0
#define ICP_RESPTYPE_RW_ACK 		1
#define ICP_RESPTYPE_RW_NAK 		2
#define ICP_RESPTYPE_JUSTRESET 		3
#define ICP_CHG_RESPTYPE 			10
#define ICP_BILL_RESPTYPE 			(ICP_CHG_RESPTYPE 	+ 30)
#define ICP_CPC_RESPTYPE 			(ICP_BILL_RESPTYPE 	+ 30)
#define ICP_USD_RESPTYPE 			(ICP_CPC_RESPTYPE	+ 30)


#define I_SLVINFO_TBCMDS 			0x0
#define ICP_DRV_RXRESP 				0x01
#define ICP_DRV_RXCHKERR 			0x02
#define ICP_DRV_TMRESP 				0x04


//-------------------------------------------------
// I C P  Protocol Structures
//-------------------------------------------------

typedef struct {
	uint8_t nState;
	uint8_t nSubState;
	uint8_t nLevel;

//***********************
	uint8_t newState;
	uint8_t newSubState;
	bool fFrameSent;
	bool fFrameAcked;
	uint8_t slvCheckNoRespState;
	uint8_t frameSent[ICP_MAX_BUFFSIZE];
	uint8_t frameSentLen;
	uint8_t SlaveType;
//***********************

#if (_IcpMaster_ == true)
	Tmr32Local nTmMaxNoResp;
#endif
} T_ICP_PERIPINFO;

typedef struct {
	uint8_t buff[ICP_MAX_BUFFSIZE];
	uint8_t nFrameLen;
	uint8_t WaitAckNackRet;																		// Inviato un blocco dati, attendo uno dei 3 possibili comandi dal Master
	uint8_t nModebitPos;
	uint8_t iTX;
	uint8_t iRX;
	uint8_t nComDrvState;
	uint8_t nCurrPeriph;
	//uint8_t nCurrPeriphAddr;
	uint8_t nMasterRW;
	T_ICP_PERIPINFO aPeriphInfo[ICP_MAX_PERIPH];
	uint8_t nTotPeriph;
	Tmr32Local nTmInterByte;
	Tmr32Local nTmResponse;
	uint8_t fSuspendRequest :1;
#if (_IcpMaster_ == true)
	uint8_t fMstIdle :1;
	uint8_t fSlaveRW :1;
	uint8_t fTmPoll :1;
	Tmr32Local nTmSetup;
	Tmr32Local nTmPoll;
	uint8_t nSlaveJustResetMask;
	uint8_t nSlaveLinkMask;
	uint8_t nSlaveReadyMask;
//$USD$#else
#endif
#if (_IcpSlaveCode_)
	Tmr32Local nTmNoLink;
	#if (_IcpSmsAlarms_ == true)
		uint8_t nSlaveInhMask;
		uint8_t nSlvInhMask;
		Tmr32Local nTmMstInhibit;
		Tmr32Local nTmMstNoLink;
		uint8_t fMstInhibit :1;
		uint8_t fMstNoLink :1;
	#endif
	uint8_t fLink :1;
#endif
	TaskId nICPPollTaskID;
	bool SlaveReqReset;
	uint8_t freeuse[16];
} T_ICP_GLBINFO;


enum ET_COMSTATE {
	COM_IDLE = 0,
	COM_MSTIDLE = 1,
	COM_RX = 2,
	COM_RXEOF = 3,
	COM_RXFRAME = 4,
	COM_RXERR = 5,
	COM_RXCHKERR = 6,
	COM_RXTMRESPONSE = 7,
	COM_RXTMNOLINK = 8,
	COM_TX = 9,
	COM_TXFRAME = 10,
	COM_RETRYTX = 11,
	COM_WAITRESP = 12,
	COM_SUSPEND = 13
};


extern T_ICP_GLBINFO ICPInfo;
extern bool icpRS232Mode;
extern bool icpMasterMode;
extern uint8_t icpSlv, icpSlvAddr, icpSlvState, icpSlvSubState, icpSlvLinkMask;
extern T_ICP_PERIPINFO *pticpSlvInfo;
extern uint8_t *pticpSlvState;
extern uint8_t *pticpSlvSubState;
extern uint8_t *pticpSlvLevel;
#if (_IcpMaster_ == true)
extern uint8_t *pticpSlvCheckNoRespState;
extern uint8_t icpSlvCheckNoRespState;
#endif


//***********************
extern uint8_t *pticpSlvNewState;
extern uint8_t *pticpSlvNewSubState;
extern uint8_t *pticpSlvFrameSent;
extern uint8_t *pticpSlvFrameAcked;

#define ICPSetNewState(newState)				{if(pticpSlvNewState) *pticpSlvNewState = newState;}
#define ICPSetNewSubState(newSubState)			{if(pticpSlvNewSubState) *pticpSlvNewSubState = newSubState;}
#define ICPGetNewState							*pticpSlvNewState
#define ICPGetNewSubState						*pticpSlvNewSubState

#define ICPSetFrameSent(fSet)					{if(pticpSlvFrameSent) *pticpSlvFrameSent = fSet;}
#define ICPGetFrameSent							*pticpSlvFrameSent
#define ICPSetFrameAcked(fSet)					{if(pticpSlvFrameAcked) *pticpSlvFrameAcked = fSet;}
#define ICPGetFrameAcked						*pticpSlvFrameAcked

void 	ICPSaveSlaveFrameSent(uint8_t frameLen);
uint8_t 	ICPLoadSlaveFrameSent(void);
//***********************


extern const uint8_t tbPeriphID[];


void ICPTimerStart(uint16_t nTimerID);
bool ICPTimerTimeout(uint16_t nTimerID);


void ICPDrvSendRW(uint8_t nReservedWord);
void ICPDrvSendMsg(uint8_t nFrameLen);
void ICPDrvHandleComState(void);
uint8_t ICPDrvGetComState(void);
void ICPDrvSetComState(uint8_t nComState);
void ICPDrvStartRX(void);
bool ICPDrvTestChecksum(void);
void ICPDrvTestEndFrame(bool fMaster);


void ICPProt_InitSlave(uint8_t nSlaveType);
void ICPProt_ChangeState(uint8_t nSlaveID, int8 nState, int8 nSubState);
void ICPProt_HandleNoRespTm(uint8_t nPeriphID);
bool ICPProt_WaitInit(uint8_t nSlaveType);
void ICPProt_SetInhibit(uint8_t nSlaveType, bool fInh);
void ICPProt_SetLink(uint8_t nSlaveType, bool fLink);
void ICPProt_SetReady(uint8_t nSlaveType, bool fReady);
void ICPProt_SetLevel(uint8_t nSlaveType, uint8_t nSlaveLevel);
uint8_t ICPProt_DrvGetLevel(uint8_t nPeriphType);
uint8_t ICPProt_GetLevel(void);
bool ICPProt_IsMaster(void);
bool ICPProt_IsRS232(void);

bool ICPProt_IsMultiVend(void);
bool ICPProt_IsCredit32(void);
bool ICPProt_IsMultiCurr(void);
bool ICPProt_IsNegVend(void);
bool ICPProt_IsDataEntry(void);

void ICPProt_DispatchFrame(void);

void ICPProt_DispatchCmds(void);

#endif
