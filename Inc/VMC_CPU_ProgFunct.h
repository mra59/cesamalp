/****************************************************************************************
File:
    VMC_CPU_ProgFunct.h

Description:
    Include file con definizioni e strutture per le funzioni di elaborazione dati per
	la programmazione del DA NewDream.

History:
    Date       Aut  Note
    Set 2016	MR   

 *****************************************************************************************/

#ifndef __VMC_CPU_ProgFunct_H
#define __VMC_CPU_ProgFunct_H


/*--------------------------------------------------------------------------------------*\
Types definition
\*--------------------------------------------------------------------------------------*/

#define	TempBuff	gVars.ddcmpbuff[0]


/*--------------------------------------------------------------------------------------*\
Tipi funzioni di programmazione
\*--------------------------------------------------------------------------------------*/

void 		VisMessage(uint8_t NumMess_1, uint8_t NumRigaMsg_1, uint8_t NumMess_2, uint8_t NumRigaMsg_2, bool FillSpace_All_LCD);
void  		Elab_BCD(tProgBCD *sBCDLocal, bool UnaCifra);
uint8_t 	Elab_uint8(uint8_t EntryVal, uint8_t TastoVal, uint8_t maxval);
uint16_t 	Elab_uint16(uint16_t EntryVal, uint8_t TastoVal);
uint16_t 	Elab_uint16_wLimit(uint16_t EntryVal, uint8_t TastoVal, uint16_t MaxVal);
uint32_t 	Elab_uint32_wLimit(uint32_t EntryVal, uint8_t TastoVal, uint32_t MaxVal);
void   		TestPaymentSettings(void);
void  		AttesaTasto(bool checkTout);
void 		VisualNum_2cifre(uint8_t Numero, uint8_t NumRiga);
void 		VisualNum_1cifra(uint8_t Numero, uint8_t NumRiga);
bool  		Prog_Generic_BCD(uint8_t msgnum, uint16_t EEaddress, uint8_t min, uint8_t MAX, bool UnaCifra);
bool  		Prog_Generic_Boolean(uint8_t msgnum, uint16_t EEaddress, uint8_t mask);
bool  		Prog_Generic_uint8(uint8_t msgnum, uint16_t EEaddress, uint8_t min, uint8_t MAX);
bool  		Prog_Generic_uint16(uint8_t msgnum, uint16_t EEaddress, uint16_t min, uint16_t MAX, bool valuta);
bool  		Prog_Generic_uint32(uint8_t msgnum, uint16_t EEaddress, uint32_t min, uint32_t MAX, bool valuta);
bool  		Prog_PrezzoSelez(uint8_t numeroselez);
bool  		Prog_ScontoSelez(uint8_t numeroselez);
uint16_t  	AttesaPassword(uint8_t msg1, uint8_t rowmsg1, uint8_t msg2, uint8_t rowmsg2, uint8_t numcifre);
bool  		Visualizza_Valore(uint8_t rownum, uint32_t Valore, uint8_t SizeDato, bool valuta);
bool  		Prog_Orologio(void);
bool 		Prog_Fasce(uint8_t TipoFascia, uint8_t weekday, uint8_t numfascia);
void  		SetDefaultFabbrica(void);
uint8_t  	RicercaPosizione(uint8_t NumRiga, LocalAttrId chrRef);
bool  		ProgNewPSW(uint8_t AccLev);
uint8_t 	VisValore_32bit(char *DestBuff, uint32_t Valore, bool valuta, uint8_t DataLen);
bool  		Prog_Opzioni_Selezioni(uint8_t numeroselez, uint8_t DisplayMessageNum, uint8_t OptionBit);

static char  	GetRefChar(LocalAttrId attr);
static void 	Elab_Hex8(tProgHex8 *sHex8pnt);
static bool 	Elab_Hex16(tProgHex16 *sHex16local, bool valuta);
static bool  	Elab_Hex32(tProgHex32 *sHex32local, bool valuta);
static void    	Elab_Boolean(tProgBool *sBoolean);
bool  			Prog_Numero_ASCII(uint8_t msgnum, uint16_t EEaddress, uint16_t ParLen);

extern		LDD_RTC_TTime DataOra;



#endif		// __VMC_CPU_ProgFunct_H


