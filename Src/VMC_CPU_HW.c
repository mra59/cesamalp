/* ***************************************************************************************
File:
    VMC_CPU_HW.c

Description:
    File con funzioni di uso generale legate all'HW CPU VMC
    

History:
    Date       Aut  Note
    Mar 2021 	MR   

 *****************************************************************************************/


/* Includes ------------------------------------------------------------------*/
#include <time.h>
#include <time32.h>

#include "main.h"
#include "ORION.H"
#include "Gestore_Exec_MSTSLV.h"
#include "VMC_CPU_LCD.h"
#include "usart.h"
#include "Monitor.h"
#include "VMC_CPU_HW.h"
#include "crc.h"
#include "wwdg.h"
#include "tim.h"
#include "adc.h"
#include "VMC_CPU_Prog.h"
#include "OrionTimers.h"
#include "stm32l4xx_hal_adc.h"
#include "VMC_CPU_Motori.h"
#include "stm32l4xx_ll_rtc.h"
#include <math.h>
#include "IICEEP.h"
#include "I2C_EEPROM.h"

#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	I2C_HandleTypeDef 	hi2c1;
extern	SPI_HandleTypeDef	hspi2;
extern 	RTC_HandleTypeDef 	hrtc;
extern	TIM_HandleTypeDef 	htim3;


extern	uint32_t			adc_read_values[4];																		// Buffer ad uso del programma (aggiornato dalla callback in Event)
extern	LCD_Display			LCDdisp;																				// Struttura per la gestione del display LCD


extern	bool				LCD_SendHalfCmd, LCD_No_Test_BF;
extern	const 	uint32_t 	EVA_DTS_BaudRate[];
extern	uint32_t			HAL_RCC_GetSysClockFreq(void);
extern	char				LCD_DataBuff[LCD_DBUF_SIZE];
extern	void  				GestioneUSB_Host_Device(void);

uint32_t	NTC1_BuffIntegr[TEMPER_READ_BUFF_SIZE], NTC2_BuffIntegr[TEMPER_READ_BUFF_SIZE];							// Buffers integrazione e media letture NTC
uint32_t	NTC3_BuffIntegr[64];			

int8_t		NTC1_Unit, NTC2_Unit, NTC3_Unit;																		// Temperature sonde in gradi centigradi con segno
int8_t		NTC1_Decimal, NTC2_Decimal, NTC3_Decimal;



/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*-------------------------------------------------------------------------------------*/

#define	LCD_PWR_ON				HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_RESET)				// LCD Acceso
#define	LCD_PWR_OFF				HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, GPIO_PIN_SET)					// LCD Spento
		
#define	LCD_RS_LOW				HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_SET)						// Linea RS LOW
#define	LCD_RS_HIGH				HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET)						// Linea RS HIGH
		
#define	LCD_E_LOW				HAL_GPIO_WritePin(LCD_E_1_GPIO_Port, LCD_E_1_Pin, GPIO_PIN_SET)						// Linea E LOW  Display 1
#define	LCD_E_HIGH				HAL_GPIO_WritePin(LCD_E_1_GPIO_Port, LCD_E_1_Pin, GPIO_PIN_RESET)					// Linea E HIGH Display 1
		
#define	LCD_2_E_LOW				HAL_GPIO_WritePin(LCD_E_2_GPIO_Port, LCD_E_2_Pin, GPIO_PIN_SET)						// Linea E LOW  Display 2
#define	LCD_2_E_HIGH			HAL_GPIO_WritePin(LCD_E_2_GPIO_Port, LCD_E_2_Pin, GPIO_PIN_RESET)					// Linea E HIGH Display 2
		
#define	LCD_DB3_HIGH			HAL_GPIO_WritePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin, GPIO_PIN_RESET)					// DB3 HIGH to allow reading of Busy Flag from LCD

#define	STB_HCT595_LOW			HAL_GPIO_WritePin(STB595_GPIO_Port, STB595_Pin, GPIO_PIN_SET)
#define	STB_HCT595_HIGH			HAL_GPIO_WritePin(STB595_GPIO_Port, STB595_Pin, GPIO_PIN_RESET)

#define	OE_HCT595_LOW			HAL_GPIO_WritePin(OE595_GPIO_Port, OE595_Pin, GPIO_PIN_RESET)
#define	OE_HCT595_HIGH			HAL_GPIO_WritePin(OE595_GPIO_Port, OE595_Pin, GPIO_PIN_SET)

#define	PL165_LOW				HAL_GPIO_WritePin(PL165_GPIO_Port, PL165_Pin, GPIO_PIN_RESET)
#define	PL165_HIGH				HAL_GPIO_WritePin(PL165_GPIO_Port, PL165_Pin, GPIO_PIN_SET)
		
#define	LOCAL_IRDA_ENA			HAL_GPIO_WritePin(IRDA_ENA_GPIO_Port, IRDA_ENA_Pin, GPIO_PIN_RESET)
#define	LOCAL_IRDA_DIS			HAL_GPIO_WritePin(IRDA_ENA_GPIO_Port, IRDA_ENA_Pin, GPIO_PIN_SET)
		
#define	EEPROM_ENA				HAL_GPIO_WritePin(EEWP_GPIO_Port, EEWP_Pin, GPIO_PIN_RESET)							// Write Enabled
#define	EEPROM_DIS				HAL_GPIO_WritePin(EEWP_GPIO_Port, EEWP_Pin, GPIO_PIN_SET)							// Write Protect

#if (IOT_PRESENCE == true)
	//#define	WIFI_MODULE_RESET	HAL_GPIO_WritePin(GPIO_2_GPIO_Port, GPIO_2_Pin, GPIO_PIN_RESET);
	//#define	WIFI_MODULE_RUN		HAL_GPIO_WritePin(GPIO_2_GPIO_Port, GPIO_2_Pin, GPIO_PIN_RESET);
	#define	WIFI_MODULE_RESET	HAL_GPIO_WritePin(GtwyRES_GPIO_Port, GtwyRES_Pin, GPIO_PIN_RESET);					// PD11 su Cesam Rev-A
	#define	WIFI_MODULE_RUN		HAL_GPIO_WritePin(GtwyRES_GPIO_Port, GtwyRES_Pin, GPIO_PIN_SET);
	#define	WIFI_MOD_DIS_BOOT	HAL_GPIO_WritePin(GtwyBOOT_GPIO_Port, GtwyBOOT_Pin, GPIO_PIN_SET);					// PD10 su Cesam Rev-A
	#define	WIFI_MOD_ENA_BOOT	HAL_GPIO_WritePin(GtwyBOOT_GPIO_Port, GtwyBOOT_Pin, GPIO_PIN_RESET);
#endif	


/*--------------------------------------------------------------------------------------*\
Global variables and types definition	
\*--------------------------------------------------------------------------------------*/
static uint32_t	Analog1Value = 0, Analog2Value = 0;

HAL_StatusTypeDef	hlpuart1_devErr;
HAL_StatusTypeDef	huart2_devErr;
HAL_StatusTypeDef	huart3_devErr;
HAL_StatusTypeDef	huart4_devErr;
HAL_StatusTypeDef	huart5_devErr;
HAL_StatusTypeDef	hirda1_devErr;
HAL_StatusTypeDef	hrtc_devErr;
HAL_StatusTypeDef	hadc1_devErr;
HAL_StatusTypeDef	hadc2_devErr;
WWDG_HandleTypeDef	WatchDog;

uint32_t			adc1_DMA_buffer[4];																	// Buffer del DMA collegato all'ADC1
uint32_t			adc2_DMA_buffer[4];																	// Buffer del DMA collegato all'ADC2
uint32_t			NTC1_ReadBuffer[100], NTC2_ReadBuffer[100], NTC3_ReadBuffer[100];					// Buffer integrazioni letture temperature
static 	uint32_t	NumReadings = 0;
static	uint32_t	TempSumValue1 = 0, TempSumValue2 = 0;
static	bool		SuspendAnalogIntegration = false;

uint8_t 	hlpuart1_BuffRx[hlpuart1_BuffSize], hlpuart1_BuffTx[hlpuart1_BuffSize];
uint8_t 	huart2_BuffRx[huart2_BuffSize], huart2_BuffTx[huart2_BuffSize];
uint8_t 	huart3_BuffRx[huart3_BuffSize], huart3_BuffTx[huart3_BuffSize];
uint8_t 	huart4_BuffRx[huart4_BuffSize], huart4_BuffTx[huart4_BuffSize];
uint8_t 	huart5_BuffRx[huart5_BuffSize], huart5_BuffTx[huart5_BuffSize];
uint8_t 	hirda1_BuffRx[hirda1_BuffSize], hirda1_BuffTx[hirda1_BuffSize];

time_t		RTC_Counter;
RTC_TimeTypeDef 	sTime = {0};
RTC_DateTypeDef 	sDate = {0};

uint8_t 				spi2_BuffRx[SPI2_BUFFSIZE], spi2_BuffTx[SPI2_BUFFSIZE];
volatile 	uint8_t		Out1_8, Out9_16, Out_Keyb, Micro1_8, Micro9_12, Tasto, PAR_Inp, Pulse_Volum, CC_Out1_8, CC_Out9_12, TastoProgrammazione;

bool		fTastoPmt, fTastoElab;
bool		fTastoPmt1, fTastoElab1, fTastoPmt2, fTastoElab2, fTastoPmt3, fTastoElab3, fTastoPmt4, fTastoElab4;


static	uint8_t		KeyIntegr[MAX_COL_NUM] = {TASTO_DELAY, TASTO_DELAY, TASTO_DELAY, TASTO_DELAY};														// Counters integrazione tasti matrice + tastiera 4 tasti
static	uint8_t		KeyPrev[MAX_COL_NUM];																												// Tasto precedentemente premuto nella relativa colonna
static	uint8_t		ScanKeybCol = FIRSTCOLUMN;
static	uint16_t	MicroIntegr[6] = {MICRO_DELAY, MICRO_DELAY, MICRO_DELAY, DOOR_MICRO_DELAY, MICRO_DELAY};											// Integrazione microswitch	
static	uint8_t		VolumIntegr[2] = {VNTLN_INTEGR, VNTLN_INTEGR};																						// Integrazione contatori volumetrici
static	uint8_t		KeyErogIntegr[NUMPULSEROG] = {TASTO_DELAY, TASTO_DELAY, TASTO_DELAY, TASTO_DELAY};													// Counters integrazione pulsanti erogazione servizio
static	uint8_t		IntegrCortoCirc[MAX_OUTPUT];

uint8_t	AnalogMux_State;

uint16_t	Ventolino[NUMCNTVOLUM];

static	bool	LCD_NoAnswer = false;

	//---     C P U    C E S A M   ------
	const uint8_t  Tasti_METALTARGHE[MATRIX_ROWS][MATRIX_COLS] = {							// Tastiera METALTARGHE
					{0x00, 0x00, 		0x00, 	 	0x00 		},			// 0
					{0x00, Tasto_3, 	Tasto_2, 	Tasto_1		},			// 1
					{0x00, Tasto_6, 	Tasto_5, 	Tasto_4 	},			// 2
					{0x00, 0x00, 		0x00, 	 	0x00 		},			// 3
					{0x00, Tasto_9, 	Tasto_8, 	Tasto_7 	},			// 4
					{0x00, 0x00, 		0x00, 	 	0x00 		},			// 5
					{0x00, 0x00, 		0x00, 	 	0x00 		},			// 6
					{0x00, 0x00, 		0x00, 	 	0x00 		},			// 7
					{0x00, Tasto_Piu,	Tasto_0, 	Tasto_Meno	},			// 8
	};
					
	const uint8_t  Tasti_NOVA[MATRIX_ROWS][MATRIX_COLS] = {								// Tastiera Nova
					{0x00, 0x00, 		0x00, 		0x00 		},			// 0
					{0x00, Tasto_Meno, 	Tasto_0,	Tasto_Piu	},			// 1
					{0x00, Tasto_7, 	Tasto_8,	Tasto_9 	},			// 2
					{0x00, 0x00, 		0x00, 		0x00 		},			// 3
					{0x00, Tasto_4, 	Tasto_5, 	Tasto_6 	},			// 4
					{0x00, 0x00, 		0x00, 		0x00 		},			// 5
					{0x00, 0x00, 		0x00, 		0x00 		},			// 6
					{0x00, 0x00, 		0x00, 		0x00 		},			// 7
					{0x00, Tasto_1, 	Tasto_2, 	Tasto_3 	},			// 8
	};

	const uint8_t  Tasti_EUROTARGHE[MATRIX_ROWS][MATRIX_COLS] = {							// Tastiera NUOVA EUROTARGHE
					{0x00, 0x00, 		0x00, 		0x00 		},			// 0
					{0x00, Tasto_Meno, 	Tasto_0,	Tasto_Piu	},			// 1
					{0x00, Tasto_7, 	Tasto_8,	Tasto_9 	},			// 2
					{0x00, 0x00, 		0x00, 		0x00 		},			// 3
					{0x00, Tasto_4, 	Tasto_5, 	Tasto_6 	},			// 4
					{0x00, 0x00, 		0x00, 		0x00 		},			// 5
					{0x00, 0x00, 		0x00, 		0x00 		},			// 6
					{0x00, 0x00, 		0x00, 		0x00 		},			// 7
					{0x00, Tasto_1, 	Tasto_2, 	Tasto_3 	},			// 8
	};

#if DEB_NTC_ERROR
#define		ADC_ARRAY_SIZE		512
	uint32_t	ADC2_Array[ADC_ARRAY_SIZE];
#endif

// ========================================================================
// ========    RTx  HC165  e  HCT595              =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: RTx_IO
	Chiamata dall'IRQ del Timer ogni 1 msec.
	Scheda CESAM Rev-A: 
	- 	La catena e' costituita da N. 2 HCT595 (Out1-8 e Out9-12+ColKeyb) e da
		N. 4 HC165 (CC_Out9-12-Keyb, CC_Out1-8, Input1-8, CoinAcceptor).  

	- dagli HC165 ricevo la sequenza:
		spi2_BuffRx[0] = CoinAcceptor
		spi2_BuffRx[1] = Input1-8
		spi2_BuffRx[2] = CC_Out1-8
		spi2_BuffRx[3] = CC_Out9-12 + ROW1-ROW2-ROW3-ROW4
	 - nei casi di sottomontaggio si analizza la variabile "TipoHW"
	 - ai TPIC6C595 trasmetto:
		BuffTx[0] = Virtual - don't care
		BuffTx[1] = Virtual - don't care
		BuffTx[2] = Out9-12 + Colonne KeyBoard
		BuffTx[3] = Out1-8

	Il flussimetro e' letto direttamente dal pin PE11 (42) del uP.
	
	La durata dell'IRQ misurato e' di circa 60 usec.
	La velocita' del clock SPI2 e' impostata a 1,25MHz (800ns a bit).
	La trasmissione di un byte impiega 6 usec, l'interbyte time e' di 2,7usec.
	La trasmissione di tutti e 4 i bytes avviene in circa 32 usec
	Misure Rev-A 28.09.2021
	
 	IN :  - 
 	OUT:  -  
\*--------------------------------------------------------------------------------------*/
void  RTx_IO(void) 
{
static	uint8_t							j, k, TempTasto, InputsVari, InpMask;
static	volatile 			uint8_t		ElabTasto;
		HAL_StatusTypeDef				res;

#if DEB_TIME_PIT0
	HAL_GPIO_WritePin(GPIO_1_GPIO_Port, GPIO_1_Pin, GPIO_PIN_SET);			// Debug  PE10 HIGH  GPIO-1
#endif	

	// -- Tx TPIC6C595 + Read AUX InputsVari-Gettoniera ---
	PL165_HIGH;																					// PL High HC165: Serial Shift
	spi2_BuffTx[0] = (~0x0);																	// Dummy byte inviato ad un HCT595 inesistente per leggere tutti e 4 gli HC165
	spi2_BuffTx[1] = (~0x0);																	// Dummy byte inviato ad un HCT595 inesistente per leggere tutti e 4 gli HC165
	if (TipoHW == BUILD_0)
	{
		spi2_BuffTx[2] = ( (~Out9_16) & 0x0f) | (ScanKeybCol & 0xf0);							// Colonne Keyboard e Uscite 9-12
	}
	else if (TipoHW == BUILD_1)
	{
		spi2_BuffTx[2] = ( (~Out9_16) & 0x0f) | ((~ScanKeybCol) & 0xf0);						// Colonne Keyboard e Uscite 9-12
	}
	
	spi2_BuffTx[3] = (~Out1_8);
	
	res = HAL_SPI_TransmitReceive(&hspi2, &spi2_BuffTx[0], &spi2_BuffRx[0], 4U, TIMEOUT_SPI2);
	if (res != HAL_OK)
	{
		return;
	}
	PAR_Inp = ((spi2_BuffRx[0] & COIN_MASK) | INPDATAVALID);									// Lettura ingressi Coin Validator
	ElabTasto = (uint8_t)(spi2_BuffRx[3] >> KEYB_ROWS_SHIFT);									// In ElabTasto la lettura delle linee ROWs
	CC_Out1_8 = spi2_BuffRx[2];
	CC_Out9_12 = (spi2_BuffRx[3] & BITS_CC_9_12);
	
	STB_HCT595_HIGH;																			// Strobe HCT595 Keyboard: essendo un Open Drain prolungo il tempo di strobe per avere un buon impulso
	STB_HCT595_HIGH;																			// II Strobe come delay dell'impulso
	STB_HCT595_HIGH;																			// III Strobe come delay dell'impulso
	STB_HCT595_LOW;
	PL165_LOW;																					// PL Low HC165: Parallel Load
	
	//--------------------------------------------------------
	//--Controllo eventuali Corto Circuito sulle uscite     --
	//--La lettura di uno zero da un output a uno indica CC --
	//--------------------------------------------------------
	InpMask = 1;
	for (j=0; j < MAX_OUTPUT; j++)
	{
		if (j < 8)
		{
			if ((Out1_8 & InpMask) == 1)
			{
				if ((CC_Out1_8 & InpMask) == 0)
				{
					if (IntegrCortoCirc[j] > 0)
					{
						IntegrCortoCirc[j]--;
					}
					else
					{
						//-- Uscita in Corto Circuito --
						Out1_8 = (Out1_8 & (~InpMask));											// Disattivo l'uscita in Corto Circuito
						ICC_Alarm = true;
					}
				}
			}
			else
			{
				IntegrCortoCirc[j] = INTEGR_CORTO_CIRC;
			}
			
		}
		else
		{
			if ((Out9_16 & InpMask) == 1)
			{
				if ((CC_Out9_12 & InpMask) == 0)
				{
					if (IntegrCortoCirc[j] > 0)
					{
						IntegrCortoCirc[j]--;
					}
					else
					{
						//-- Uscita in Corto Circuito --
						Out1_8 = (Out1_8 & (~InpMask));											// Disattivo l'uscita in Corto Circuito
						ICC_Alarm = true;
					}
				}
			}
			else
			{
				IntegrCortoCirc[j] = INTEGR_CORTO_CIRC;
			}
		}
		InpMask <<= 1;
		if (InpMask == 0)
		{
			InpMask = 1;
		}
	}

	// ======================================================
	// ===== LCD, tastiera e pulsante PROG          =========
	// ======================================================
	//---------------------------------------
	//--  Elaborazione Tastiera a  Matrice --
	//---------------------------------------
	ElabTasto = ((~ElabTasto) & RIGHE_TASTIERA);												// Complemento righe per avere l'1 nel tasto premuto ed elimino i bit non della tastiera
	switch (ScanKeybCol)
	{
		case SCAN_COL1:
			j = COL1; 		// 0
			break;
		case SCAN_COL2:
			j = COL2; 		// 1
			break;
		case SCAN_COL3:
			j = COL3; 		// 2
			break;
		case SCAN_COL4:
			j = COL4; 		// 3
			break;
		default:
			j = COL1; 
	}
	if (ElabTasto > 0)
	{
		// Tasto premuto sulla colonna in scansione
		if (ElabTasto == KeyPrev[j])
		{
			// Stesso tasto scansione precedente o prima elaborazione tasto
			if (KeyIntegr[j] > 0)
			{
				KeyIntegr[j]--;
				if (KeyIntegr[j] == 0)
				{
					if (gOrionConfVars.KeyB_Type == KEYB_METALTARGHE)
					{
				  		TempTasto = Tasti_METALTARGHE[ElabTasto][j];							// j e' la colonna in elaborazione
					}
					//else if (gOrionConfVars.KeyB_Type == KEYB_ARDUINO)
					//{
				  	//	TempTasto = Tasti_ARDUINO[ElabTasto][j];								// j e' la colonna in elaborazione
					//}
					else 
					{
				  		TempTasto = Tasti_NOVA[ElabTasto][j];									// j e' la colonna in elaborazione
					}
				}
			}
		} else {
			// Premuto tasto diverso dal precedente
			if (KeyIntegr[j] < TASTO_DELAY) {													// Fino a che l'integratore non torna a riposo non considero il nuovo tasto
				KeyIntegr[j]++;
			} else {
				KeyPrev[j] = ElabTasto;
			}
		}
	}
	else
	{
		// Nessun tasto premuto sulla colonna in scansione
		if (KeyIntegr[j] < TASTO_DELAY) {
			KeyIntegr[j]++;
			if (KeyIntegr[j] >= TASTO_DELAY) {
				KeyPrev[j] = 0;
			}
		}
	}
	if (ScanKeybCol == LASTCOLUMN)
	{
		k = 0;																					// Determino se c'e' un tasto da considerare premuto (KeyIntegr[x] == 0)
		if (KeyIntegr[0] == 0) k = 0x01;
		if (KeyIntegr[1] == 0) k |= 0x02;
		if (KeyIntegr[2] == 0) k |= 0x04;
		if (KeyIntegr[3] == 0) k |= 0x08;
		if (k != 0)																				// Tasto premuto
		{
			if (fTastoPmt == false) {
				fTastoPmt = true;
				Tasto = TempTasto;																// Tasto premuto da elaborare
			}
		} 
		else 
		{																						// Tasti rilasciati
			if (fTastoElab == true) {															// Solo se elaborato accetto un nuovo tasto
				fTastoElab = false;
				fTastoPmt = false;
			}
		}
	}

	//---------------------------------------
	//--  Elaborazione Tasto  PROG         --
	//---------------------------------------
	
	//-- TastoProgrammazione: 0=rilasciato - 1=premuto --
	if (HAL_GPIO_ReadPin(PROG_GPIO_Port, PROG_Pin) != 0)
	{																							// Micro aperto (high)
		if (MicroIntegr[INTEGR_TASTO_PROG] < TASTO_DELAY)
		{
			MicroIntegr[INTEGR_TASTO_PROG]++;
		}
		if (MicroIntegr[INTEGR_TASTO_PROG] >= TASTO_DELAY)
		{
			MicroIntegr[INTEGR_TASTO_PROG] = TASTO_DELAY;
			TastoProgrammazione = 0x01;															// Tasto PROG rilasciato
		}
	}
	else
	{																							// Micro chiuso (Low)
		if (MicroIntegr[INTEGR_TASTO_PROG] > 0)
		{
			MicroIntegr[INTEGR_TASTO_PROG]--;
		}
		if (MicroIntegr[INTEGR_TASTO_PROG] == 0)
		{
			TastoProgrammazione = 0;															// Tasto PROG premuto
		}
	}
	
	if (Imene == false) goto EndRTx_IO;
	
	//-------------------------------------------
	//--  Elaborazione Ingressi Vari         ---
	//-------------------------------------------
/*
	if (TipoHW == BUILD_0)
	{
		InputsVari = spi2_BuffRx[0];															// Scheda completamente montata con N. 4 HC165
	}
	else if (TipoHW == BUILD_1)
	{
		InputsVari = spi2_BuffRx[0];															// Scheda sottomontata con N. 2 o 3 HC165
	}
*/
	
	//-----------------------------------------------------------
	// -- I bit [3-0] di InputsVari sono i pulsanti            --
	//-----------------------------------------------------------
	InputsVari = spi2_BuffRx[1];
	
	//-- Primo passaggio: loop per integrazione pulsanti --
	InpMask = 0x01;
	for (j=0; j < NUMPULSEROG; j++, (InpMask <<= 1))
	{
		if ((InputsVari & InpMask) != 0)
		{
			// ----   Pulsante Erogazione Rilasciato (HIGH) ----
			if (KeyErogIntegr[j] < TASTO_DELAY)
			{
				KeyErogIntegr[j]++;
			}
			else
			{
				if (KeyErogIntegr[j] > TASTO_DELAY)
				{
					KeyPrev[j] = TASTO_DELAY;
				}
			}
		}
		else 
		{
			// ----   Pulsante Erogazione Premuto (LOW) ----
			if (KeyErogIntegr[j] > 0)
			{
				KeyErogIntegr[j]--;
			}
		}
	}
	//-- Secondo passaggio: elaborazione pulsanti premuti/rilasciati --
	//----------   P u l s a n t e  1  ----------
	if (KeyErogIntegr[0] == 0)
	{
		//-- Pulsante Eogazione N. 1 Premuto --
		if (fTastoPmt1 == false)
		{	
			fTastoPmt1 = true;																	// Pulsante erogazione premuto da elaborare	
		}	
	}
	else
	{
		if (KeyErogIntegr[0] == TASTO_DELAY)
		{
			//-- Pulsante Eogazione N. 1 Rilasciato --
			if (fTastoElab1 == true)
			{																					// Solo se elaborato predispongo per una nuova pressione pulsante
				fTastoElab1 = false;
				fTastoPmt1 = false;
			}
		}
	}
	//----------   P u l s a n t e  2  ----------
	if (KeyErogIntegr[1] == 0)
	{
		//-- Pulsante Eogazione N. 2 Premuto --
		if (fTastoPmt2 == false)
		{	
			fTastoPmt2 = true;																	// Pulsante erogazione premuto da elaborare	
		}	
	}
	else
	{
		if (KeyErogIntegr[1] == TASTO_DELAY)
		{
			//-- Pulsante Eogazione N. 2 Rilasciato --
			if (fTastoElab2 == true)
			{																					// Solo se elaborato predispongo per una nuova pressione pulsante
				fTastoElab2 = false;
				fTastoPmt2 = false;
			}
		}
	}
	//----------   P u l s a n t e  3  ----------
	if (KeyErogIntegr[2] == 0)
	{
		//-- Pulsante Eogazione N. 3 Premuto --
		if (fTastoPmt3 == false)
		{	
			fTastoPmt3 = true;																	// Pulsante erogazione premuto da elaborare	
		}	
	}
	else
	{
		if (KeyErogIntegr[2] == TASTO_DELAY)
		{
			//-- Pulsante Eogazione N. 3 Rilasciato --
			if (fTastoElab3 == true)
			{																					// Solo se elaborato predispongo per una nuova pressione pulsante
				fTastoElab3 = false;
				fTastoPmt3 = false;
			}
		}
	}
	//----------   P u l s a n t e  4  ----------
	if (KeyErogIntegr[3] == 0)
	{
		//-- Pulsante Eogazione N. 4 Premuto --
		if (fTastoPmt4 == false)
		{	
			fTastoPmt4 = true;																	// Pulsante erogazione premuto da elaborare	
		}	
	}
	else
	{
		if (KeyErogIntegr[3] == TASTO_DELAY)
		{
			//-- Pulsante Eogazione N. 4 Rilasciato --
			if (fTastoElab4 == true)
			{																					// Solo se elaborato predispongo per una nuova pressione pulsante
				fTastoElab4 = false;
				fTastoPmt4 = false;
			}
		}
	}
	
	//-----------------------------------------------------
	// -- Lettura ingresso Flussimetro                   --
	// -- Incremento il counter ad ogni fronte di salita --
	//-----------------------------------------------------
	k = 0;
	InpMask = HAL_GPIO_ReadPin(FLUSS_1_GPIO_Port,FLUSS_1_Pin);
	if (InpMask == GPIO_PIN_SET)
	{
		// ----   Inp Volumetrico  Aperto  (HIGH) ----
	  	if (VolumIntegr[k] < VNTLN_INTEGR)
		{
			VolumIntegr[k]++;
			if (VolumIntegr[k] == VNTLN_INTEGR)
			{
				Ventolino[k]++;																	// Incremento numero impulsi ricevuti dal Flussimetro
			}
		}
		else
		{
			VolumIntegr[k] = VNTLN_INTEGR;
		}
	}
	else 
	{
		// ----   Inp Volumetrico LOW ----
		if (VolumIntegr[k] > 0)
		{
			VolumIntegr[k]--;
		}
	}

	//-----------------------------------------------------------
	// -- I bit [7-4] di InputsVari sono ingressi vari         --
	//-----------------------------------------------------------
	//InputsVari = spi2_BuffRx[0];
	
	//--     EMERGENCY - STOP      ---
	if ((InputsVari & EMERGENCY) != 0)
	{																							// Micro aperto (high)
		if (MicroIntegr[INTEGR_EMERGENCY] < EMRGNC_SWITCH_DELAY)
		{
			MicroIntegr[INTEGR_EMERGENCY]++;
		}
		if (MicroIntegr[INTEGR_EMERGENCY] >= EMRGNC_SWITCH_DELAY)
		{
			MicroIntegr[INTEGR_EMERGENCY] = EMRGNC_SWITCH_DELAY;
			Micro1_8 |= EMERGENCY;																// Aggiungo stato del pulsante Emergency in Micro1_8
		}
	}
	else
	{																							// Micro chiuso (Low)
		if (MicroIntegr[INTEGR_EMERGENCY] > 0)
		{
			MicroIntegr[INTEGR_EMERGENCY]--;
		}
		if (MicroIntegr[INTEGR_EMERGENCY] == 0)
		{
			Micro1_8 = (Micro1_8 & (~EMERGENCY));
		}
	}
	//-- Microswitch  Fine  Prodotto  1  ---
	if ((InputsVari & FINE_PRODOTTO_1) != 0)
	{																							// Micro aperto (high)
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_1] < MICRO_DELAY)
		{
			MicroIntegr[INTEGR_FINE_PRODOTTO_1]++;
		}
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_1] >= MICRO_DELAY)
		{
			MicroIntegr[INTEGR_FINE_PRODOTTO_1] = MICRO_DELAY;
			Micro1_8 |= FINE_PRODOTTO_1;														// Aggiungo stato del micro fine prodotto 1 in Micro1_8
		}
	}
	else
	{																							// Micro chiuso (Low)
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_1] > 0)
		{
			MicroIntegr[INTEGR_FINE_PRODOTTO_1]--;
		}
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_1] == 0)
		{
			Micro1_8 = (Micro1_8 & (~FINE_PRODOTTO_1));
		}
	}

	
	//-- Microswitch  Fine  Prodotto  2  ---
	if ((InputsVari & FINE_PRODOTTO_2) != 0)
	{																							// Micro aperto (high)
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_2] < MICRO_DELAY)
		{
			MicroIntegr[INTEGR_FINE_PRODOTTO_2]++;
		}
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_2] >= MICRO_DELAY)
		{
			MicroIntegr[INTEGR_FINE_PRODOTTO_2] = MICRO_DELAY;
			Micro1_8 |= FINE_PRODOTTO_2;														// Aggiungo stato del micro fine prodotto 2 in Micro1_8
		}
	}
	else
	{																							// Micro chiuso (Low)
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_2] > 0)
		{
			MicroIntegr[INTEGR_FINE_PRODOTTO_2]--;
		}
		if (MicroIntegr[INTEGR_FINE_PRODOTTO_2] == 0)
		{
			Micro1_8 = (Micro1_8 & (~FINE_PRODOTTO_2));
		}
	}

	//-- Reed Porta ---
	//-- L'integrazione e' molto piu' lunga rispetto agli altri microswitch --
	if ((InputsVari & DOOR_SWITCH) != 0)
	{																							// Micro aperto (high)
		if (MicroIntegr[INTEGR_DOORSWITCH] < DOOR_MICRO_DELAY)
		{
			MicroIntegr[INTEGR_DOORSWITCH]++;
		}
		if (MicroIntegr[INTEGR_DOORSWITCH] >= DOOR_MICRO_DELAY)
		{
			MicroIntegr[INTEGR_DOORSWITCH] = DOOR_MICRO_DELAY;
			Micro1_8 |= DOOR_SWITCH;															// Porta Aperta (High)
		}
	}
	else
	{																							// Micro chiuso (Low)
		if (MicroIntegr[INTEGR_DOORSWITCH] > 0)
		{
			MicroIntegr[INTEGR_DOORSWITCH]--;
		}
		if (MicroIntegr[INTEGR_DOORSWITCH] == 0)
		{
			Micro1_8 = (Micro1_8 & (~DOOR_SWITCH));												// Porta Chiusa (Low)
		}
	}
	
	
	// --------------------------------------------------------------------------------
	// -- Ciclo_Erogazione --
	// -- Controlla erogazioni in funzione per determinarne la fine --
	Ciclo_Erogazione();


EndRTx_IO:	
	//(ICC_Alarm == false) OE_HCT595_LOW;															// Output Enable solo se nessun motore e' in corto circuito
	OE_HCT595_LOW;																				// HC595 Output Enable 
	ScanKeybCol = ScanKeybCol >> 1;																// Scansione Colonna Successiva
	if (ScanKeybCol == SCAN_OVERFLOW) ScanKeybCol = FIRSTCOLUMN;

#if DEB_TIME_PIT0
	HAL_GPIO_WritePin(GPIO_1_GPIO_Port, GPIO_1_Pin, GPIO_PIN_RESET);			// Debug  PE10 LOW GPIO-1
#endif
	
	
#if (DEB_NO_MICROSW == true)	
	Micro1_8 = (Micro1_8 & (~EMERGENCY));
#endif	
	
}

// ========================================================================
// ========    RCC  Control/Status Register       =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: IsResetDaPWDown
	Ritorna la flag BOR (Power Down Flag) per aggiornare l'Audit registrando tutti
	gli OFF-ON
	
 	IN:	 - 
	OUT: - return BOR flag
\*--------------------------------------------------------------------------------------*/
bool  IsResetDaPWDown(void)
{
	return (HAL_IS_BIT_SET(RCC->CSR, RCC_CSR_BORRSTF));
}
	
/*--------------------------------------------------------------------------------------*\
Method: IsResetDaSWReset
	Ritorna la flag RCC_CSR_SFTRSTF (Software reset) per aggiornare l'Audit.
	
 	IN:	 - 
	OUT: - return RCC_CSR_SFTRSTF flag
\*--------------------------------------------------------------------------------------*/
bool  IsResetDaSWReset(void)
{
	return (HAL_IS_BIT_SET(RCC->CSR, RCC_CSR_SFTRSTF));
}

/*--------------------------------------------------------------------------------------*\
Method: ClrResetFlags
	Remove Reset flags
	
 	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
void  ClrResetFlags(void)
{
	RCC->CSR |= RCC_CSR_RMVF;
}


// ========================================================================
// ========    LPUART 1  per  Executive  Slave    =========================
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: Executive_COMM_Init
	Inizializza la COMM per i protocolli Slave (LPUART1).

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Executive_COMM_Init(void)
{
	MX_LPUART1_UART_Init();
	HAL_UART_Receive_IT(&hlpuart1, &hlpuart1_BuffRx[0], 1u);									// Start reception of one character
}

/*--------------------------------------------------------------------------------------*\
Method: ExecSlaveTxChar
	Risposta dello Slave Executive al Master RR.
	
 	IN:	 - Carattere da trasmettere 
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  ExecSlaveTxChar(uint8_t Dato) 
{
	hlpuart1_BuffTx[0] = Dato;
	hlpuart1_devErr = HAL_UART_Transmit_IT(&hlpuart1, &hlpuart1_BuffTx[0], 1u);
	Monitor_LogExecTxToRR(Dato);																// Se in Monitor, echo al PC del chr trasmesso dallo Slave Executive
}

// ========================================================================
// ========   LPUART 1  per  MDB  Slave  (USD)    =========================
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: MDB_Slave_COMM_Init
	Inizializza la COMM per il protocollo MDB Slave (LPUART1).

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  MDB_Slave_COMM_Init(void) {
	
	MX_LPUART1_UART_Init();
	HAL_UART_DeInit(&hlpuart1);
	hlpuart1.Init.Parity = UART_PARITY_NONE;
	if (HAL_UART_Init(&hlpuart1) != HAL_OK)
	{
	  Error_Handler();
	}
	HAL_UART_Receive_IT(&hlpuart1, &hlpuart1_BuffRx[0], 1u);									// Start reception of one character
}

/*--------------------------------------------------------------------------------------*\
Method: MDB_Slave_TxChar
	Invio un dato con protocollo MDB Slave.
	Se abilitato dal monitor, echo del carattere al PC.
	La UART invia autonomamente 9 bit utilizzando i due bytes del buffer: il primo
	e' il modebit, il secondo il dato.
	Si passa len=1u come counter di dati da trasmettere.
	
 	IN:	 - Carattere da trasmettere 
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  MDB_Slave_TxChar(uint8_t Dato, uint8_t ModeBit)
{
	hlpuart1_BuffRx[0] = ModeBit;
	hlpuart1_BuffRx[1] = Dato;
	huart2_devErr = HAL_UART_Transmit_IT(&huart2, &hlpuart1_BuffRx[0], 1u);
	Monitor_LogMDBTxData(Dato, ModeBit);													// Se in Monitor, echo al PC del chr trasmesso al Master MDB
}

// ========================================================================
// ========    USART 2 per  M I F A R E           =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: MIFARE_COMM_Init
	Inizializza la COMM per l'antenna MIFARE.

\*--------------------------------------------------------------------------------------*/
void  MIFARE_COMM_Init(void)
{
	MX_USART2_UART_Init();
	HAL_UART_Receive_IT(&huart2, &huart2_BuffRx[0], 1u);
}

/*--------------------------------------------------------------------------------------*\
 Method: MIFARE_ChangeBaudRate
 	Cambia la velocita' alla seriale utilizzata per il Mifare.

 	IN:	  - Baud Rate 
 	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  MIFARE_ChangeBaudRate(uint32_t BaudRate)
{
	uint32_t	NewPrescaler;
 	UART_HandleTypeDef	*pt;
	
	pt = &huart2;
	if (huart2.Init.BaudRate == BaudRate) return;												// Baud rate invariato
  	huart2.Init.BaudRate = BaudRate;															// New Baud Rate
	NewPrescaler = HAL_RCC_GetSysClockFreq() / BaudRate;
	pt->Instance->BRR = NewPrescaler;
}

/*--------------------------------------------------------------------------------------*\
Method: MIFARE_SendBlock
	Invia un blocco dati sulla UART5.
	
 	IN:	 - address buffer da trasmettere e sua lunghezza in bytes
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  MIFARE_SendBlock(uint8_t *DataBuff, uint8_t BuffLenght)
{
	huart2_devErr = HAL_UART_Transmit_IT(&huart2, DataBuff, BuffLenght);
}

/*---------------------------------------------------------------------------------------------*\
Method: MIFARE_Enable_Rx_Chr
	Abilita la ricezione di un carattere sulla UART Mifare.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  MIFARE_Enable_Rx_Chr(void)
{
	gVars.MifareRDRF = false;
	HAL_UART_Receive_IT(&huart2, &huart2_BuffRx[0], 1u);										// Start reception of one character
}

/*---------------------------------------------------------------------------------------------*\
Method: MIFARE_COMM_DeInit
	Disabilita la COMM per l'antenna MIFARE per evitare che la tensione sulla linea TxD possa
	ritornare, tramite l'antenna Microhard, sul pin +5Mifare e al reset sw all'uscita della
	programmazione far partire il uP in Boot Mode.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  MIFARE_COMM_DeInit(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

	HAL_UART_DeInit(&huart2);
	IrDA_COMM_Disable();
	
	HAL_GPIO_WritePin(GPIOA, (GPIO_2_Pin | GPIO_3_Pin), GPIO_PIN_RESET);						// PA2 and PA3 LOW
	GPIO_InitStruct.Pin = USB_PWR_ENA_Pin|GPIO_2_Pin|GPIO_3_Pin;								// PA2(USART2_Tx) and PA3(USART2_Rx) as output 
  	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  	GPIO_InitStruct.Pull = GPIO_NOPULL;
  	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB, (GPIO_PIN_6 | GPIO_PIN_7), GPIO_PIN_RESET);						// PB6 and PB7 LOW
  	GPIO_InitStruct.Pin = (GPIO_PIN_6|GPIO_PIN_7);												// PB6(USART1_Tx) and PB7(USART1_Rx) as output 
  	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  	GPIO_InitStruct.Pull = GPIO_NOPULL;
  	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
 	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}


// ========================================================================
// ========    USART 3  per  RS485  Display  Touch    =====================
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: RS485_Comm_Init
	Inizializza la COMM per il display Touch (UART3).

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RS485_Comm_Init(void)
{
#if TOUCH_STONE  	
	MX_USART3_UART_Init_STONE();
	HAL_UART_Receive_IT(&huart3, &huart3_BuffRx[0], 1u);										// Start reception of one character
#else	
	MX_USART3_UART_Init();
	if (TipoHW != BUILD_1)
	{
		HAL_UART_Receive_IT(&huart3, &huart3_BuffRx[0], 1u);									// Start reception of one character
	}
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: RS485_Comm_SendBlock
	Invia un blocco dati sulla USART3.
	
 	IN:	 - address buffer da trasmettere e sua lunghezza in bytes
	OUT: -
\*--------------------------------------------------------------------------------------*/
uint8_t RS485_Comm_SendBlock(uint8_t *DataBuff, uint8_t BuffLenght)
{
	huart3_devErr = HAL_UART_Transmit_IT(&huart3, DataBuff, BuffLenght);
	return (uint8_t)huart3_devErr;
}

/*---------------------------------------------------------------------------------------------*\
Method: RS485_Comm_Enable_Rx_Chr
	Abilita la ricezione di un carattere.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RS485_Comm_Enable_Rx_Chr(void)
{
	HAL_UART_Receive_IT(&huart3, &huart3_BuffRx[0], 1u);										// Start reception of one character
}

/*---------------------------------------------------------------------------------------------*\
Method: RS485_Comm_ReceiveBlock
	Abilita la ricezione di un buffer con DMA.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RS485_Comm_ReceiveBlock(uint8_t *DataBuff, uint8_t BuffLenght)
{
	huart3_devErr = HAL_UART_Receive_DMA(&huart3, DataBuff, BuffLenght);						// Start reception of a block
}

/*---------------------------------------------------------------------------------------------*\
Method: RS485_Comm_DisableReceive
	Disabilita la ricezione di un buffer con DMA.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RS485_Comm_DisableReceive(void)
{
	huart3_devErr = HAL_UART_DMAStop(&huart3);
	if (huart3_devErr != HAL_OK)
	{
		huart3_devErr = HAL_UART_DMAStop(&huart3);
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: RS485_Comm_SendBreak
	Invia un break.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RS485_Comm_SendBreak(void)
{
	huart3_devErr = HAL_LIN_SendBreak(&huart3);
	if (huart3_devErr != HAL_OK)
	{
		huart3_devErr = HAL_UART_DMAStop(&huart3);
	}
}

#if IOT_PRESENCE
// ========================================================================
// ========    USART 4 per  T E L E M E T R I A   =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: IoT_COMM_Init
	Inizializza la COMM per colloquiare con la scheda IoT.

 	IN:	 - 
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  IoT_COMM_Init(void)
{
/*
	if (gVMC_ConfVars.GatewayOnJ18 == true)
	{
		MX_UART4_Init_J18();
	}
	else
	{
		MX_UART4_Init();
	}
*/

	MX_UART4_Init();
	IoT_Enable_Rx_Chr();																		// Start reception of one character
}

/*--------------------------------------------------------------------------------------*\
Method: IoT_SendBlock
	Invia un blocco dati sulla UART4.
	
 	IN:	 - address buffer da trasmettere e sua lunghezza in bytes
	OUT: -
\*--------------------------------------------------------------------------------------*/
uint8_t  IoT_SendBlock(uint8_t *DataBuff, uint16_t BuffLenght)
{
	huart4_devErr = HAL_UART_Transmit_IT(&huart4, DataBuff, BuffLenght);
	return (uint8_t)huart4_devErr;
}

/*---------------------------------------------------------------------------------------------*\
Method: IoT_Enable_Rx_Chr
	Abilita la ricezione di un carattere sulla UART4 IoT.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  IoT_Enable_Rx_Chr(void)
{
	HAL_UART_Receive_IT(&huart4, &huart4_BuffRx[0], 1u);										// Start reception of one character
}

/*--------------------------------------------------------------------------------------*\
Method: Reset_WiFi_Module
	Attiva l'uscita OponDrain per resettare il solo Modulo WiFi Inventek.
	Il Reset del uP della MH620 non e' collegato ad alcun pin.
	
 	IN:	 - address buffer da trasmettere e sua lunghezza in bytes
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  Reset_WiFi_Module(void)
{
	WIFI_MODULE_RESET;
	WIFI_MOD_DIS_BOOT;
	HAL_Delay(Milli100);
	WIFI_MODULE_RUN;
}
#endif


// ========================================================================
// ========    UART 5   per  Executive    M A S T E R     =================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: Master_Exec_Comm_Init
	Inizializza la COMM per il protocollo Executive Master.

\*--------------------------------------------------------------------------------------*/
void  Master_Exec_Comm_Init(void)
{
	MX_UART5_ExecMaster_Init();
	HAL_UART_Receive_IT(&huart5, &huart5_BuffRx[0], 1u);
}

/*--------------------------------------------------------------------------------------*\
Method: Master_Exec_TxChar
	Invio un comando con protocollo Executive master allo Slave VMC2.
	Se abilitato dal monitor, echo del carattere al PC.
	
 	IN:	 - Carattere da trasmettere 
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  Master_Exec_TxChar(uint8_t Dato) 
{
	huart5_BuffTx[0] = Dato;
	huart5_devErr = HAL_UART_Transmit_IT(&huart5, &huart5_BuffTx[0], 1u);
	Monitor_LogExecTxToVMC2(Dato);																// Se in Monitor, echo al PC del chr trasmesso
}

// ========================================================================
// ========    UART 5   per     M D B       M A S T E R   =================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: Master_MDB_Comm_Init
	Inizializza la COMM per il protocollo MDB Master.

\*--------------------------------------------------------------------------------------*/
void  Master_MDB_Comm_Init(void)
{
	MX_UART5_Init();
	HAL_UART_Receive_IT(&huart5, &huart5_BuffRx[0], 1u);										// Start reception of one character
}

/*--------------------------------------------------------------------------------------*\
Method: Master_MDB_TxChar
	Invio un comando con protocollo MDB Master.
	Se abilitato dal monitor, echo del carattere al PC.
	La UART invia autonomamente 9 bit utilizzando i due bytes del buffer: il primo
	e' il modebit, il secondo il dato.
	Si passa len=1u come counter di dati da trasmettere.
	
 	IN:	 - Carattere da trasmettere 
	OUT: -
\*--------------------------------------------------------------------------------------*/
void  Master_MDB_TxChar(uint8_t Dato, uint8_t ModeBit)
{
	huart5_BuffTx[1] = ModeBit;
	huart5_BuffTx[0] = Dato;
	huart5_devErr = HAL_UART_Transmit_IT(&huart5, &huart5_BuffTx[0], 1u);
	Monitor_LogMDBTxData(Dato, ModeBit);														// Se in Monitor, echo al PC del chr trasmesso al VMC
}


// ========================================================================
// ========    UART per   I r D A                 =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: Start_IrDA
	Inizializza la COMM per le comunicazioni IrDA (USART1).

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Start_IrDA(void)
{
#if IRDA_PRESENTE
	if (gVMC_ConfVars.PresMifare == false)
	{																							// Senza MIFARE abilito IrDA Locale
		LOCAL_IRDA_ENA;
	}
	else
	{
		LOCAL_IRDA_DIS;
	}
	MX_USART1_IRDA_Init();
	HAL_IRDA_Receive_IT(&hirda1, &hirda1_BuffRx[0], 1);											// Start reception of one character
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: IrDA_COMM_TxChar
	Trasmetto un carattere sulla seriale usata per l'IrDA.
	
 	IN:	  - Carattere da trasmettere 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  IrDA_COMM_TxChar(uint8_t Dato) 
{
#if IRDA_PRESENTE
	hirda1_BuffTx[0] = Dato;
	hirda1_devErr = HAL_IRDA_Transmit_IT(&hirda1, &hirda1_BuffTx[0], 1u);
	if (hirda1_devErr != HAL_OK)
	{
    	Error_Handler();
	}
#endif
}

/*--------------------------------------------------------------------------------------*\
 Method: IrDA_COMM_ChangeBaudRate
 	Cambia la velocita' alla seriale utilizzata per l'IrDA.
 	Il Baud Rate passato come parametro e' quello riportato nell'EVA-DTS 6.1.1 a pag 51.
 	(2 = 2400; 6 = 38400).

 	IN:	  - Baud Rate 
 	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  IrDA_COMM_ChangeBaudRate(uint8_t BaudRate)
{
#if IRDA_PRESENTE

	uint32_t	NewPrescaler;
 	IRDA_HandleTypeDef	*pt;
	
	pt = &hirda1;
	if (hirda1.Init.BaudRate == EVA_DTS_BaudRate[BaudRate]) return;								// Baud rate invariato
  	NewPrescaler = HAL_RCC_GetSysClockFreq() / EVA_DTS_BaudRate[BaudRate];
	pt->Instance->BRR = NewPrescaler;

#endif
}

/*--------------------------------------------------------------------------------------*\
 Method: IrDA_COMM_Disable
 	Disattivo la seriale utilizzata per l'IrDA.

\*--------------------------------------------------------------------------------------*/
void  IrDA_COMM_Disable(void)
{
#if IRDA_PRESENTE
  	HAL_IRDA_DeInit(&hirda1);
#endif
}

// ************************************************************************
// ***********  NON  UTILIZZATE   CON   STM32   ***************************
// ************************************************************************
/*--------------------------------------------------------------------------------------*\
 Method: IrDA_COMM_TxBegin
	Attivo la trasmissione sulla seriale usata per l'IrDA e disattivo la ricezione per non
	avere il feedback della trasmissione stessa.

\*--------------------------------------------------------------------------------------*/
/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  IrDA_COMM_TxBegin(void)
{
	CLEAR_BIT(USART1->CR1, USART_CR1_RE);														// Receiver Disable
	SET_BIT(USART1->CR1, USART_CR1_TE);															// Transmitter Enable
}
*/

/*--------------------------------------------------------------------------------------*\
Method: IrDA_COMM_RxBegin
	Attivo la ricezione sulla seriale usata per l'IrDA.
	
\*--------------------------------------------------------------------------------------*/
/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  IrDA_COMM_RxBegin(void)
{
	SET_BIT(USART1->CR1, USART_CR1_RE);															// Receiver Enable
}
*/

/*--------------------------------------------------------------------------------------*\
Method: IrDA_COMM_RxEnd
	Disattivo la ricezione sulla seriale usata per l'IrDA.
	
\*--------------------------------------------------------------------------------------*/
/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  IrDA_COMM_RxEnd(void)
{
	CLEAR_BIT(USART1->CR1, USART_CR1_RE);														// Receiver Disable
}
*/


// ========================================================================
// ========    UART per comunicazione con PC      =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: SetPC_Comm
	Predispone la SCI per comunicare col PC
	
	IN:	  - BAUD rate di attivazione UART
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  SetPC_Comm(uint32_t BaudRate)
{
#if IRDA_PRESENTE

	HAL_IRDA_DeInit(&hirda1);
	
	hirda1.Instance = USART1;
	hirda1.Init.BaudRate = BaudRate;
	hirda1.Init.WordLength = IRDA_WORDLENGTH_8B;
	hirda1.Init.Parity = IRDA_PARITY_NONE;
	hirda1.Init.Mode = IRDA_MODE_TX_RX;
	hirda1.Init.Prescaler = 10;
	hirda1.Init.PowerMode = IRDA_POWERMODE_NORMAL;
	if (HAL_IRDA_Init(&hirda1) != HAL_OK)
	{
	  Error_Handler();
	}
	HAL_IRDA_Receive_IT(&hirda1, &hirda1_BuffRx[0], 1);											// Start reception of one character
#endif
}


// ========================================================================
// ========              Display  L C  D          =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\

Method: LCD_POWER
	Accende o spegne il Display LCD locale.

	IN:	  - Flag ON o OFF
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  LCD_POWER(bool On_Off)
{
	if (On_Off == ON)
	{
		LCD_PWR_ON;
	}
	else
	{
		LCD_PWR_OFF;
	}
}


/*--------------------------------------------------------------------------------------*\
Method: Is_LCD_NoAnswerSet
	Rende lo stato della flag "LCD_NoAnswer"

	IN:	  - 
	OUT:  - LCD_NoAnswer status
\*--------------------------------------------------------------------------------------*/
bool  Is_LCD_NoAnswerSet(void)
{
	return LCD_NoAnswer;
}

/*--------------------------------------------------------------------------------------*\
Method: Clear_LCD_NoAnswer

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Clear_LCD_NoAnswer(void)
{
	LCD_NoAnswer = false;
}

/*--------------------------------------------------------------------------------------*\
Method: Refresh_LCD
	Visualizza messaggi su display LCD.

	La funzione dura circa 7,4 msec @72MHz, col primo Delay=500 e gli altri 3 a 50.
	La "LCD_Home" dura 3 msec e la trasmissione di ogni chr dura circa 55 usec. 
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Refresh_LCD(void)
{
	LCD_Home();																					// Cursor Home (impiega 3 msec)
	SendMessageToLCD(&LCD_DataBuff[0], FOUR_LCD_NUMCOL);
}

/*--------------------------------------------------------------------------------------*\
Method: SendCommand_LCD
	Invia un comando al display LCD.
	La scrittura sul bus e' eseguita usando la funzione BitSet del uP che
	mette a 1 i bit desiderati: per mettere a zero quelli rimanenti si utilizza la
	funzione BitClr del uP usando il byte comando complementato.
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  SendCommand_LCD(uint8_t CMD)
{
// ===================================================================================
	// ---   Pilotaggio LCD classico con Delay times fissi ---

	uint16_t	wData_L, wData_H;
	uint16_t	wData_Compl_H, wData_Compl_L;


	wData_H = (uint16_t) ((~(((uint16_t)CMD) >> LCD_HNIBBLE_BUS_SHIFT)) & 0x000f);				// LCD BUS Nibble HIGH
	wData_L = (uint16_t) ((~((uint16_t)CMD)) & 0x000f);											// LCD BUS Nibble LOW
	wData_Compl_H = ((wData_H ^ 0xffff) & 0x000f);												// LCD BUS Nibble HIGH Complemented per clr bits che non sono a 1
	wData_Compl_L = ((wData_L ^ 0xffff) & 0x000f);												// LCD BUS Nibble LOW Complemented per clr clr bits che non sono a 1

	// ---   Start  Write  LCD  Module  ----
	LCD_RS_LOW;																					// Invio Comando
	//LCD_RW_LOW;																					// Write Data
	LCD_E_HIGH;																					// Strobe al display: Nibble High sul Bus LCD

	HAL_GPIO_WritePin(GPIOE, wData_H, GPIO_PIN_SET);											// Data High sul bus LCD
	HAL_GPIO_WritePin(GPIOE, wData_Compl_H, GPIO_PIN_RESET);									// Data High Complementedsul bus LCD per clr bits che non sono a 1
	HAL_Delay(Milli2);
	LCD_E_LOW;

	if (LCD_SendHalfCmd == false)
	{
		// Nibble Low all'LCD
		HAL_Delay(Milli2);
		LCD_E_HIGH;																				// Strobe al display

		HAL_GPIO_WritePin(GPIOE, wData_L, GPIO_PIN_SET);										// Data Low sul bus LCD
		HAL_GPIO_WritePin(GPIOE, wData_Compl_L, GPIO_PIN_RESET);								// Data Low Complemented sul bus LCD per clr bits che non sono a 1
		HAL_Delay(Milli2);
		LCD_E_LOW;
	}
	LCD_SendHalfCmd = false;
}

/*--------------------------------------------------------------------------------------*\
Method: SendMessageToLCD
	Visualizza un messaggio su display LCD.
	La scrittura sul bus si ottiene con la funzione BitSet del uP che
	mette a 1 i bit desiderati: per mettere a zero quelli rimanenti si utilizza la
	funzione BitClr del uP usando il byte dati complementato.

	IN:	  - pointer al messaggio e size messaggio
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  SendMessageToLCD(char *srcpnt, uint8_t msgsize) 
{
	uint8_t		LCD_Cnt/*, DataBus*/;
	uint16_t	wData_H, wData_L;
	uint16_t	wData_Compl_H, wData_Compl_L;
	char 		*msgpnt;
	
	msgpnt = srcpnt;
	//LCD_RW_LOW;																						// Write Data
	LCD_RS_HIGH;																					// Invio Dato
	for (LCD_Cnt=0; LCD_Cnt < msgsize; LCD_Cnt++)
	{
		DelayTimeLCD(DELAY_BETW_CHARS);									// 560 = circa 37 usec
		wData_H = (uint16_t) ((~(((uint16_t)*(msgpnt+LCD_Cnt)) >> LCD_HNIBBLE_BUS_SHIFT)) & 0x000f);	// LCD BUS Nibble HIGH
		wData_L = (uint16_t) ((~((uint16_t)*(msgpnt+LCD_Cnt))) & 0x000f);								// LCD BUS Nibble LOW
		wData_Compl_H = ((wData_H ^ 0xffff) & 0x000f);												// LCD BUS Nibble HIGH Complemented per clr bits che non sono a 1
		wData_Compl_L = ((wData_L ^ 0xffff) & 0x000f);												// LCD BUS Nibble LOW Complemented per clr clr bits che non sono a 1
		
	 	 // Nibble High all'LCD
		LCD_E_HIGH;																					// Strobe al display
		LCD_E_HIGH;																					// II Strobe usato come delay per evitare che il nuovo dato sul bus cambi mentre lo strobe sta ancora salendo
#if LCD2_PRESENTE	
	LCD_2_E_HIGH;																					// Strobe al display N. 2
#endif
		HAL_GPIO_WritePin(GPIOE, wData_H, GPIO_PIN_SET);											// Data High sul bus LCD
		HAL_GPIO_WritePin(GPIOE, wData_Compl_H, GPIO_PIN_RESET);									// Data High Complementedsul bus LCD per clr bits che non sono a 1
		DelayTimeLCD(E_HIGH_TIME);										// 100 = circa 6.6 usec
		LCD_E_LOW;
#if LCD2_PRESENTE	
	LCD_2_E_LOW;																					// Strobe al display N. 2
#endif
		// Nibble Low all'LCD
		DelayTimeLCD(NIBBLE_DELAY);										// 50 = circa 3.5 usec
		LCD_E_HIGH;																					// Strobe al display
		LCD_E_HIGH;																					// II Strobe usato come delay per evitare che il nuovo dato sul bus cambi mentre lo strobe sta ancora salendo
#if LCD2_PRESENTE	
	LCD_2_E_HIGH;																					// Strobe al display N. 2
	LCD_2_E_HIGH;
#endif
		HAL_GPIO_WritePin(GPIOE, wData_L, GPIO_PIN_SET);											// Data Low sul bus LCD
		HAL_GPIO_WritePin(GPIOE, wData_Compl_L, GPIO_PIN_RESET);									// Data Low Complemented sul bus LCD per clr bits che non sono a 1
		DelayTimeLCD(E_HIGH_TIME);										// 100 = circa 6.6 usec
		LCD_E_LOW;
#if LCD2_PRESENTE	
	LCD_2_E_LOW;																					// Strobe al display N. 2
#endif
	}
	HAL_Delay(Milli5);																				// Attendo elaborazione ultimo dato inviato
}

/*--------------------------------------------------------------------------------------*\
Method: DelayTimeLCD
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  DelayTimeLCD(uint32_t delayLCD)
{
	uint32_t	attesaLCD = delayLCD;
	while (attesaLCD)
	{
	  attesaLCD--;
	}
}

// ========================================================================
// ========          T E S T     V A R I          =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: EEPROM_Write
	Scrive nella EEprom il contenuto di EEBuff: i primi 2 bytes di EEBuff contengono
	il MemAddress che deve essere inviato come parametro alla HAL_I2C_Mem_Write e
	quindi scorporato dal resto del buffer dati.
	La EEPROM impiega circa 4-4,5 msec per scrivere sia un byte che piu' bytes se 
	inviati con una sola trasmissione (Page Write)
	Funzione bloccante, esce sol oal termine della scrittura.

Parameters:
	IN	 - numero di byte da scrivere.
	OUT	 - true se scrittura OK, False se scrittura fallita
\*--------------------------------------------------------------------------------------*/
bool  EEPROM_Write (uint16_t device, uint16_t MemAddr, uint8_t* EEBuff, uint8_t size) {

	HAL_StatusTypeDef	res;
	uint32_t			Tickstart, Timeout;
	
    Timeout = PREV_WRITE_WAIT;
	Tickstart = HAL_GetTick();
	EEPROM_ENA;																					// Enable Write
	do {
		res = HAL_I2C_Mem_Write(&hi2c1, device, MemAddr, I2C_MEMADD_SIZE_16BIT, &EEBuff[0], size, TIMEOUT_EEP);
		if (res == HAL_OK) 
		{
			EEPROM_DIS;																			// Write Protect
			return true;																		// Write OK
		}
	} while((HAL_GetTick() - Tickstart) < Timeout);
	EEPROM_DIS;																					// Write Protect
	return false;																				// Error
}

/*--------------------------------------------------------------------------------------*\
Method: EEPROM_Read
	Legge dalla EEPROM size bytes partendo dall'indirizzo contenuto in "EE_Address" e li 
	memorizza in "EE_Buff".
Parameters:
	IN	- Address dati da leggere 
	OUT	- True e DestBuff con i dati letti dalla EE se tutto ok, altrimenti false 
\*--------------------------------------------------------------------------------------*/
bool  EEPROM_Read (uint16_t device, uint16_t EE_Address, uint8_t *DestBuff, uint8_t size) {

	HAL_StatusTypeDef	res;
	uint32_t			Tickstart, Timeout;
	
    Timeout = PREV_WRITE_WAIT;
	Tickstart = HAL_GetTick();
	do {
		res = HAL_I2C_Mem_Read(&hi2c1, device, EE_Address, I2C_MEMADD_SIZE_16BIT, &DestBuff[0], size, TIMEOUT_EEP);
		if (res == HAL_OK) 
		{
			return true;																		// Write OK
		}
	} while((HAL_GetTick() - Tickstart) < Timeout);
	return false;																				// Read Error
}

/*---------------------------------------------------------------------------------------------*\
Method: YellowLedCPU
	Spegne o accende il led Giallo della CPU

	IN:	  - On_Off
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  YellowLedCPU(uint8_t On_Off)
{
	if (On_Off == ON)
	{
		HAL_GPIO_WritePin(LED_Y_GPIO_Port, LED_Y_Pin, GPIO_PIN_RESET);							// Led Giallo ON
	} else {
		HAL_GPIO_WritePin(LED_Y_GPIO_Port, LED_Y_Pin, GPIO_PIN_SET);							// Led Giallo OFF
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: YellowLedCPU_Toggle
	Lampeggia Led Giallo CPU

	IN:	  -
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  YellowLedCPU_Toggle(void)
{
	HAL_GPIO_TogglePin(LED_Y_GPIO_Port, LED_Y_Pin);
}

/*---------------------------------------------------------------------------------------------*\
Method: BlueLedCPU
	Spegne o accende il led Blu della CPU

	IN:	  - On_Off
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  BlueLedCPU(uint8_t On_Off)
{
	if (On_Off == ON)
	{
		HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);							// Led Blu ON
	} else {
		HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);							// Led Blu OFF
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: BlueLedCPU_Toggle
	Lampeggia Led Blu CPU

	IN:	  -
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  BlueLedCPU_Toggle(void)
{
	HAL_GPIO_TogglePin(LED_B_GPIO_Port, LED_B_Pin);
}

/*---------------------------------------------------------------------------------------------*\
Method: GreenLedCPU
	Spegne o accende il led Verde della CPU

	IN:	  - On_Off
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  GreenLedCPU(uint8_t On_Off)
{
	if (On_Off == ON)
	{
		HAL_GPIO_WritePin(LED_V_GPIO_Port, LED_V_Pin, GPIO_PIN_RESET);							// Led Verde ON
	} else {
		HAL_GPIO_WritePin(LED_V_GPIO_Port, LED_V_Pin, GPIO_PIN_SET);							// Led Verde OFF
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: GreenLedCPU_Toggle
	Lampeggia Led Verde CPU

	IN:	  -
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  GreenLedCPU_Toggle(void)
{
	HAL_GPIO_TogglePin(LED_V_GPIO_Port, LED_V_Pin);
}

/*---------------------------------------------------------------------------------------------*\
Method: RedLedCPU_Toggle
	Lampeggia Led Rosso CPU

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RedLedCPU_Toggle(void)
{
	HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin);
}

/*---------------------------------------------------------------------------------------------*\
Method: RedLedCPU
	Spegne o accende il led Rosso della CPU

	IN:	  - On_Off
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RedLedCPU(uint8_t On_Off)
{
	if (On_Off == ON)
	{
		HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);							// Led Rosso ON
	} else {
		HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);							// Led Rosso OFF
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Led_Vano
	Spegne o accende il led del vano prelievo prodotto.
	Quando lo accende carica anche il timeout accensione gestito poi nel main.

	IN:	  - On_Off
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Led_Vano(uint8_t On_Off, uint32_t DurataON) 
{
}

/*---------------------------------------------------------------------------------------------*\
Method: BlinkErrRedLed_HALT
	Routine che fa lampeggiare il led rosso della CPU alla frequenza desiderata.
	*********    ROUTINE SENZA USCITA ************    ATTESA  SPEGNIMENTO   *******
	Uso il timer "ToutVend" perche' sicuramente non in uso qui.

	IN:	  - frequenza di lampeggio in msec
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  BlinkErrRedLed_HALT(uint32_t frequenza, BlinkErrType errore)
{
	GreenLedCPU(OFF);																			// Led Verde CPU OFF
  	do {
  		WDT_Clear(gVars.devWDT);  
  		HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin);											// Inverto Led Rosso
  		Touts.ToutVend = frequenza;
		if (IsMonitorActive() == false)
		{
	  		if (IsBuzzerOFF()) 
			{
				Buz_start(Milli500*2, HZ_2000, 0);												// Modificato in tono continuo per distinguerlo dall'errore chiave USB
			}
		}
  		while(Touts.ToutVend != 0) 
		{
  			SystemMonitor();																	// Eseguo funzioni di Monitor del prodotto
			GestioneUSB_Host_Device();
  		}
  	} while (true);
}

/*---------------------------------------------------------------------------------------------*\
Method: EnaDisCoinValidator
	Abilito o Inibisco il Validatore parallelo (funzione chiamata continuamente dalla Sel24_task 
	nel Main).

	IN:	  - Enable o Disable
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  EnaDisCoinValidator(uint8_t enable)
{
	if (enable) {
		// Enable Coin Validator
		if (gOrionConfVars.ValidatorPolarity) {
		  	HAL_GPIO_WritePin(InhGett_1_GPIO_Port, InhGett_1_Pin, GPIO_PIN_RESET);				// Abilitazione Attivo Alto
		} else {
		  	HAL_GPIO_WritePin(InhGett_1_GPIO_Port, InhGett_1_Pin, GPIO_PIN_SET);				// Abilitazione Attivo Basso
		}
	} else {
		// Inhibit Coin Validator
		if (gOrionConfVars.ValidatorPolarity) {
		  	HAL_GPIO_WritePin(InhGett_1_GPIO_Port, InhGett_1_Pin, GPIO_PIN_SET);				// Abilitazione Attivo Basso
		} else {
		  	HAL_GPIO_WritePin(InhGett_1_GPIO_Port, InhGett_1_Pin, GPIO_PIN_RESET);				// Abilitazione Attivo Alto
		}
	}
}



/*---------------------------------------------------------------------------------------------*\
Method: All_595Outputs_OFF
	Disattivo tutte le uscite HC595
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  All_595Outputs_OFF(void)
{
	OE_HCT595_HIGH;																				// All HCT595 Output Disable
}

/*---------------------------------------------------------------------------------------------*\
Method: MIFARE_OFF
	Spegni PN512
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  MIFARE_OFF(void)
{
	HAL_GPIO_WritePin(MIF_1_PWS_ENA_GPIO_Port, MIF_1_PWS_ENA_Pin, GPIO_PIN_SET);				// Spengo PN512 MIFARE 1
}

/*---------------------------------------------------------------------------------------------*\
Method: MIFARE_ON
	Accendi PN512
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  MIFARE_ON(void)
{
	HAL_GPIO_WritePin(MIF_1_PWS_ENA_GPIO_Port, MIF_1_PWS_ENA_Pin, GPIO_PIN_RESET);				// Accendo PN512
}

// ========================================================================
// ========    Checksum   CRC 16                  =========================
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: Init_CRC
	Predispone i registri della periferica CRC.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Init_CRC(uint16_t Seme)
{
	hcrc.Init.InitValue = (uint32_t)Seme;
	if (HAL_CRC_Init(&hcrc) != HAL_OK)															// Clr CRC result Register
	{
	  Error_Handler();
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Calc_CRC_GetCRC8
	This method appends 1 data byte to current CRC calculation and returns new result. 

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
uint16_t  Calc_CRC_GetCRC8(uint8_t Dato)
{
	uint8_t		InpVal;
	//uint32_t	CRC_Result;

	hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
	InpVal = Dato;
	return (uint16_t)HAL_CRC_Accumulate(&hcrc, (uint32_t*)&InpVal, 1U);
	//CRC_Result = HAL_CRC_Accumulate(&hcrc, (uint32_t*)&InpVal, 1U);
	//return (uint16_t)CRC_Result;
}

/*---------------------------------------------------------------------------------------------*\
Method: Calc_CRC_GetCRC16
	This method appends 4 data byte to current CRC calculation and returns new result. 

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
uint16_t  Calc_CRC_GetCRC16(uint16_t Dato)
{
	uint16_t	InpVal;
	//uint32_t	CRC_Result;

	hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_HALFWORDS;
	InpVal = Dato;
	return (uint16_t)HAL_CRC_Accumulate(&hcrc, (uint32_t*)&InpVal, 1U);
	//CRC_Result = HAL_CRC_Accumulate(&hcrc, (uint32_t*)&InpVal, 1U);
	//return (uint16_t)CRC_Result;
}

/*---------------------------------------------------------------------------------------------*\
Method: Calc_CRC_GetCRC32
	This method appends 4 data byte to current CRC calculation and returns new result. 

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
uint16_t  Calc_CRC_GetCRC32(uint32_t Dato)
{
	uint32_t	InpVal;
	//uint32_t 	CRC_Result;

	hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_WORDS;
	InpVal = Dato;
	return (uint16_t)HAL_CRC_Accumulate(&hcrc, &InpVal, 1U);
	//CRC_Result = HAL_CRC_Accumulate(&hcrc, &InpVal, 1U);
	//return (uint16_t)CRC_Result;
}

// ========================================================================
// ========    Controllo Tensione Alimentazione Prodotto con ADC1   =======
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: Check_PowerSupply_Level
	Controlla tensione di alimentazione CPU: rimane in loop fino a che la tensione di ingresso
	non supera la soglia H_LEVEL_ADC predefinita.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Check_PowerSupply_Level(void)
{
	PowSup.PUP_WaitViHigh = true;
	
	MX_ADC1_Init();																				// INIT ADC 1 Converter e set Analog Watch dog 2
	hadc1_devErr = HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	HAL_ADC_Start_DMA(&hadc1, adc1_DMA_buffer, ALL_ADC1_INPUTS);
	RedLedCPU(OFF);																				// Led Rosso CPU OFF
	HAL_Delay(Milli500);																		// Attendo salita tensione per evitare la fermata nel SysMonitor se PC collegato e tensione in salita

#if (DEB_NO_VOLTAGE_TEST == false)
	// Attendo Vi>H_LEVEL_ADC lampeggiando LED Verde
	do {
		GreenLedCPU_Toggle();																	// Inverto Led Verde
		HAL_Delay(Milli100);
	
  #if SYSMONITOR_ONLY
		// -- Se Vin < H_LEVEL_ADC e presenza di Host CDC, attivo SystemMonitor --
		if (adc1_DMA_buffer[VIN_VOLTAGE] < H_LEVEL_ADC)
		{
        	if (USB_state != USB_MODE_DEVICE)
			{
				GestioneUSB_Host_Device();						        						// Controllo se CPU collegata a Host CDC (Terminal)
				InitRTC();																		// Inizializzo orologio per permetterne setting
			}
			else
			{
		  		SystemMonitor();																// Eseguo funzioni di Monitor del prodotto
			}
		}
  #endif	
	
	} while (adc1_DMA_buffer[VIN_VOLTAGE] < H_LEVEL_ADC);

	if (IsMonitorActive())																		// Se tensione regolare ma monitor attivo significa che dal +5V del PC si e' passati ad alimentazione piena
	{
		WatchDogReset();
	}
	GreenLedCPU(OFF);																			// Led Verde CPU OFF
    PowSup.PUP_WaitViHigh = false; 

#endif	// End DEB_NO_VOLTAGE_TEST
}

/*---------------------------------------------------------------------------------------------*\
Method: Check_MIF_Voltage
	Controlla il segnale Open Drain "FAULT" del Power Switch STMPS2141.
	Quando attivo indica un assorbimento che eccede il limite di 500 mA.
 
	IN:	 - 
	OUT: - true = Ok; False = FAIL;
\*----------------------------------------------------------------------------------------------*/
bool  Check_MIF_Voltage(void)
{
	return (HAL_GPIO_ReadPin(MIF_FAULT_GPIO_Port, MIF_FAULT_Pin));
}

/*---------------------------------------------------------------------------------------------*\
Method: ReadAnalog
	Rende la misura effettuata continuamente dall'ADC1 e memorizzata tramite DMA nel
	buffer adc_DMA_buffer.
 
	IN:	  - 
	OUT:  -
\*----------------------------------------------------------------------------------------------*/
uint32_t  ReadAnalog(uint8_t InputName)
{
	return adc1_DMA_buffer[InputName];
}

/*---------------------------------------------------------------------------------------------*\
Method: Read_HW_Type
	Rende la misura effettuata dall'ADC2 sull'ingresso ANALOGS-1 ADC12-IN11 che misura il
	partitore resistivo a indicazione del tipo di hardware.
 
	IN:	  - 
	OUT:  -
\*----------------------------------------------------------------------------------------------*/
uint32_t  Read_HW_Type(void)
{
	return adc1_DMA_buffer[HW_REVISION];
}

// ========================================================================
// ========    ADC2  per lettura Ingressi Analogici                 =======
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: AnalogInputs_ADC_Init
	Init ADC per lettura ingressi analogici.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  AnalogInputs_ADC_Init(void)
{
	MX_ADC2_Init();																				// INIT ADC 2 Converter
	HAL_ADCEx_Calibration_Start(&hadc2, ADC_SINGLE_ENDED);
	HAL_ADC_Start_DMA(&hadc2, adc2_DMA_buffer, ALL_ADC2_INPUTS);
}


/*---------------------------------------------------------------------------------------------*\
Method: RestartAnalogsIntegration
	Reset valori integrazione letture dell'ADC2 chiamata dalla "Scan_Temperatures" ad ogni 
	commutazione del MUX analogico, quindi ogni secondo dal main.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
/*
static void  RestartAnalogsIntegration(void)
{
	TempSumValue1 	= 0;																		// NTC1 e NTC3
	TempSumValue2 	= 0;																		// NTC2
	NumReadings		= 0;
	Analog1Value 	= 0;
	Analog2Value 	= 0;
}
*/

/*---------------------------------------------------------------------------------------------*\
Method: AnalogsIntegration
	Integra le letture dell'ADC2.
	Funzione chiamata ogni 1 msec dall'IRQ del timer PIT0.
	Somma TEMPER_INTEGR letture per farne la media.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  AnalogsIntegration(void)
{
#define	TEMPER_INTEGR	32
#define	DIVISOR_SHIFT	5

	if (SuspendAnalogIntegration == true) return;
	
#if DEB_NTC_ERROR
	ADC2_Array[NumReadings] = adc2_DMA_buffer[ANALOGS_2];					// NTC1
#endif		
	
	TempSumValue1 += adc2_DMA_buffer[ANALOGS_2];												// NTC1 e NTC3
	TempSumValue2 += adc2_DMA_buffer[ANALOGS_1];												// NTC2
	NumReadings++;
	if (NumReadings == TEMPER_INTEGR)
	{
		NumReadings = 0;
		Analog1Value = (TempSumValue1 >> DIVISOR_SHIFT);
		Analog2Value = (TempSumValue2 >> DIVISOR_SHIFT);
		TempSumValue1 = 0;
		TempSumValue2 = 0;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Scan_Temperatures
	Scansione letture ingressi NTC per determinazione temperature.
	Funzione chiamata ogni 100 msec dall'IRQ del timer PIT0.
	Dopo NUM_TEMP_READ letture si fa la media per determinare il valore della temperatura.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  Scan_Temperatures(void)
{
	static	uint32_t	Tot_ADC_Read1 = 0, Tot_ADC_Read2 = 0;
	//static	uint32_t	Previus_ADC_Read1_LOW = -1, Previus_ADC_Read1_HIGH = 0;
	//static	uint32_t	Previus_ADC_Read2_LOW = -1, Previus_ADC_Read2_HIGH = 0;
	
#if DEB_NTC_ERROR
	uint8_t		DecimalDiff;
	static uint32_t	Previus_ADC_Read1_LOW = -1, Previus_ADC_Read1_HIGH = 0;
	static uint32_t	Previus_HW_LOW = -1, Previus_HW_HIGH = 0;
#endif

	//uint32_t	ADC_Read1, ADC_Read2;
	float		T_ZERO = VENTICINQUE_KELVIN;
	float		R_ZERO = NTC_NOMINAL_RES;
	float		BETA = BETA_NTC;
	float		RESISTOR_VALUE = BIAS_RESISTOR;
	long		loTemp;
	float		fTemp, fDenum, TempNTC, fTemp1;
	bool		Negativa;
	
		// ======================================================
		// =====  Lettura  NTC1  e  NTC2                =========
		// ======================================================
		//******   Calcolo valore temperatura NTC1  **********
		SuspendAnalogIntegration = true;
		Tot_ADC_Read1 = Analog1Value;															// Valori NTC1 integrati dalla "AnalogsIntegration"
		Tot_ADC_Read2 = Analog2Value;															// Valori NTC2 integrati dalla "AnalogsIntegration"

	/*
		if (Tot_ADC_Read1 < Previus_ADC_Read1_LOW)
		{
			Previus_ADC_Read1_LOW = Tot_ADC_Read1;
		}
		if (Tot_ADC_Read1 > Previus_ADC_Read1_HIGH)
		{
			Previus_ADC_Read1_HIGH = Tot_ADC_Read1;
		}

		if (Tot_ADC_Read2 < Previus_ADC_Read2_LOW)
		{
			Previus_ADC_Read2_LOW = Tot_ADC_Read2;
		}
		if (Tot_ADC_Read2 > Previus_ADC_Read2_HIGH)
		{
			Previus_ADC_Read2_HIGH = Tot_ADC_Read2;
		}
	*/

#if (DEB_NTC_ERROR == false)
		SuspendAnalogIntegration = false;
#endif
			
		if ((Tot_ADC_Read1 < HIGH_TEMPER_LIMIT) && (Tot_ADC_Read1 > LOW_TEMPER_LIMIT))
		{
			Negativa = false;
			loTemp = (long)(((float)Tot_ADC_Read1 * RESISTOR_VALUE) / (ADC_BIT_NUM - (float)Tot_ADC_Read1));
			fTemp = (float)(log(loTemp / R_ZERO));
			fDenum = BETA + (fTemp * T_ZERO);
			TempNTC = (float)((BETA*T_ZERO)/(fDenum));
			TempNTC = (TempNTC - ZERO_KELVIN);
			if (TempNTC < 0) Negativa = true;
			NTC1_Unit = (int8_t)TempNTC;															// Gradi di temperatura sonda NTC1

			fTemp = (float)NTC1_Unit;
			fTemp1 = ((TempNTC - fTemp) * 10);														// Decimali in floating point
			if (Negativa == true) fTemp1 = (0x00 - fTemp1);
			NTC1_Decimal = (uint8_t)fTemp1;															// Decimi di grado temperatura sonda NTC1
			fTemp = (float)NTC1_Decimal;															// Arrotondo i decimi di grado 
			fTemp1 = ((fTemp1 - fTemp) * 10);
			if (fTemp1 > 5)
			{
				NTC1_Decimal++;
				if (NTC1_Decimal == 10)
				{
					NTC1_Decimal = 0;
					if (Negativa == true)
					{
						NTC1_Unit--;
					} 
					else
					{
						NTC1_Unit++;
					}
				}
			}
		}
		else
		{
			NTC1_Unit = 99;																			// Sonda guasta o scollegata
			NTC1_Decimal = 9;
		}
		//******   Calcolo valore temperatura NTC2  **********
		if ((Tot_ADC_Read2 < HIGH_TEMPER_LIMIT) && (Tot_ADC_Read2 > LOW_TEMPER_LIMIT))
		{
			Negativa = false;
			loTemp = (long)(((float)Tot_ADC_Read2 * RESISTOR_VALUE) / (ADC_BIT_NUM - (float)Tot_ADC_Read2));
			fTemp = (float)(log(loTemp / R_ZERO));
			fDenum = BETA + (fTemp * T_ZERO);
			TempNTC = (float)((BETA*T_ZERO)/(fDenum));
			TempNTC = (TempNTC - ZERO_KELVIN);
			if (TempNTC < 0) Negativa = true;
			NTC2_Unit = (int8_t)TempNTC;															// Gradi di temperatura sonda NTC1
			
			fTemp = (float)NTC2_Unit;
			fTemp1 = ((TempNTC - fTemp) * 10);														// Decimali in floating point
			if (Negativa == true) fTemp1 = (0x00 - fTemp1);
			NTC2_Decimal = (uint8_t)fTemp1;															// Decimi di grado temperatura sonda NTC1
			fTemp = (float)NTC2_Decimal;															// Arrotondo i decimi di grado 
			fTemp1 = ((fTemp1 - fTemp) * 10);
			if (fTemp1 > 5)
			{
				NTC2_Decimal++;
				if (NTC2_Decimal == 10)
				{
					NTC2_Decimal = 0;
					if (Negativa == true)
					{
						NTC2_Unit--;
					} 
					else
					{
						NTC2_Unit++;
					}
				}
			}
		}
		else
		{
			NTC2_Unit = 99;																			// Sonda guasta o scollegata
			NTC2_Decimal = 9;
		}
		
#if DEB_NTC_ERROR
	//-- N T C     1 --
	if (gVmcStatusIoT.NTC1 != NTC1_Unit)
	{
		if (gVmcStatusIoT.NTC1 > NTC1_Unit)
		{
			DecimalDiff = ((gVmcStatusIoT.NTC1Decimal+10) - NTC1_Decimal);
		}
		else
		{
			DecimalDiff = ((NTC1_Decimal+10) - gVmcStatusIoT.NTC1Decimal);
		}
		if (DecimalDiff > 5)
		{
			Negativa = false;					// DEB Solo per mettere il break
			//AddLogDataToEEPromBufferLog((uint8_t*)&ADC2_Array[0], ADC_ARRAY_SIZE, false);
		}
	}
	else
	{
		if (Tot_ADC_Read1 < Previus_ADC_Read1_LOW)
		{
			Previus_ADC_Read1_LOW = Tot_ADC_Read1;
		}
		if (Tot_ADC_Read1 > Previus_ADC_Read1_HIGH)
		{
			Previus_ADC_Read1_HIGH = Tot_ADC_Read1;
		}
	}
	SuspendAnalogIntegration = false;
#endif		

	
		
	#if DEB_TIME_PIT0
			HAL_GPIO_WritePin(GPIO_2_GPIO_Port, GPIO_2_Pin, GPIO_PIN_RESET);					// Debug  PB2 LOW GPIO-2
	#endif	
}

/*---------------------------------------------------------------------------------------------*\
Method: AnalogMUX_Select
	Predispone le uscite desiderate del multiplexer analogico HC4052.
	Memorizza stato del MUX nel byte AnalogMux_State per controlli prima di utilizzare le
	conversioni automatiche dell'ADC.
 
	IN:	  - 
	OUT:  - byte AnalogMux_State aggiornato
\*----------------------------------------------------------------------------------------------*/
void  AnalogMUX_Select(uint8_t AnalogInput)
{
/*
	switch(AnalogInput)
	{
		case MUX_NTC1_NTC2:
			NTC1_NTC2_SELECT;
			AnalogMux_State = MUX_NTC1_NTC2;
			break;
		
		case MUX_NTC3_AREF:
			NTC3_VREF_SELECT;
			AnalogMux_State = MUX_NTC3_AREF;
			break;
		
		case MUX_TIPOHW:
			TIPOHW_SELECT;
			AnalogMux_State = MUX_TIPOHW;
			break;
			
		default:
			NTC1_NTC2_SELECT;
			AnalogMux_State = MUX_NTC1_NTC2;
			break;		
	}
*/
}

// ========================================================================
// ========    Real   Time    Clock               =========================
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: InitRTC
	Inizializzazione RTC: se anno >2099 o <2013 � la prima accensione, quindi devo 
	attivare il conteggio.
	Sarebbe opportuno leggere la VBAT per verifiare che sia attiva, altrimenti errore.
 
	IN:	  - 
	OUT:  - Data e ora nella struttura DataOra
\*----------------------------------------------------------------------------------------------*/
void  InitRTC(void)
{
 	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 127;
	hrtc.Init.SynchPrediv = 255;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
	  Error_Handler();
	}
  	hrtc_devErr = HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	if (hrtc_devErr != HAL_OK)
	{
	  Error_Handler();
	}
	hrtc_devErr = HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	if (hrtc_devErr != HAL_OK)
	{
	  Error_Handler();
	}
	if ((sDate.Year < (ANNO_INIZIALE-STM32_REF_YEAR)) || (sDate.Year > (ANNO_FINALE-STM32_REF_YEAR)))	// RTC gia' inizializzato (anno compreso tra 2020 e 2099)?
	{
		//--- Inizializzo il RTC al 01.01.2020 ore 00:00:00 ---
		sTime.Hours 			= 0;
		sTime.Minutes 			= 0;
		sTime.Seconds 			= 0;
		sDate.WeekDay 			= RTC_WEEKDAY_MONDAY;
		sDate.Month 			= RTC_MONTH_JANUARY;
		sDate.Date 				= 1;
		sDate.Year 				= (ANNO_INIZIALE - STM32_REF_YEAR);
		sTime.DayLightSaving 	= RTC_DAYLIGHTSAVING_NONE;
		sTime.StoreOperation 	= RTC_STOREOPERATION_RESET;												// Reset bit BKP (Backup) del registro RTC_CR a indicazione che il DayLightSaving non � stato calcolato e memorizzato
		if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) == HAL_OK)
		{
			if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
			{
		  		Error_Handler();
			}
		}
		else
		{
			Error_Handler();
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: RTC_Calibration
	Calibrazione del RTC.
	Tramite configurazione si programmano CALM e CALP che contengono i clock di correzione in
	piu' o in meno da applicare al RTC.
	Si aggiorna il registro RTC_CALR quando diverso dal contenuto di CALM e CALP.

	CALP is intended to be used in conjunction with CALM, which lowers the frequency of
	the calendar with a fine resolution. if the input frequency is 32768 Hz, the number of RTCCLK
	pulses added during a 32-second window is calculated as follows: (512 * CALP) - CALM.	
	CALP e' un bit: set a 1 se uso RTC_SMOOTHCALIB_PLUSPULSES_SET, clear a 0 se uso RTC_SMOOTHCALIB_PLUSPULSES_RESET
	CALM e' costituito da 9 bit [8:0]
	Il calcolo degli impulsi di calibrazione e' il seguente:
	(secondi_ritardo/anticipo*32768)/(secondi_perido_di_misura/32) --> CALP - (512-risultato)
	Esempio: il RTC ritarda 4 sec ogni 16 ore (Quarzo LSE da 32.768KHz)
	(16ore * 3600) = 57600 sec
	(4*32768)/(57600/32)= +72,82-->72   CALP=1 e CALM=(512-72)->437
	Oppure, piu' semplicemente:
	(32 * 32768) * (secondi_indietro/avanti / secondi_perido_di_misura)
	(32*32768) * (4/57600) = 72,81
	Se il RTC corre si deve scrivere il risultato in CALM e CALP = 0.
 
	IN:	  - CALP e CALM
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RTC_Calibration(void)
{
#define	RTC_ritarda		1
#define	ORE_IN_ANNO		8784
#define	SECONDI_IN_ORA	3600
	
	bool		RTCError_Sign;
	uint8_t		RTC_CalibOutput;
	uint32_t	CALP, CALM;
	uint32_t	TDiff = 0, TSample = 0;
	RTC_HandleTypeDef	*pnt = &hrtc;

	
	//-- Attivazione uscita impulsi di 512 Hz ----
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Opt_RTC_COE]), (uint8_t *)(&RTC_CalibOutput), 1U);										// RTC Calibration Output Enable
	if (RTC_CalibOutput)
	{
		if (HAL_RTCEx_SetCalibrationOutPut(&hrtc, 0) != HAL_OK)
		{
			Error_Handler();
		}
	}
	else
	{
		if (HAL_RTCEx_DeactivateCalibrationOutPut(&hrtc) != HAL_OK)
		{
			Error_Handler();
		}
	}
	//-- Controllo se eseguire aggiornamento registro calibrazione RTC_CALR ----
	ReadEEPI2C(IICEEPConfigAddr(EETimeDiff), (uint8_t *)(&TDiff), sizeof(SizeEETimeDiff));											// Secondi di ritardo/anticipo RTC
	if (TDiff < 130000)																												// Il valore massimo di differenza programmabile e' di (2^32 / 32768) = 131072
	{
		ReadEEPI2C(IICEEPConfigAddr(EETimeSample), (uint8_t *)(&TSample), sizeof(SizeEETimeSample));								// Tempo di campionamento RTC da ore trasformato in secondi
		if (TSample < ORE_IN_ANNO)																									// Il numero massimo di ore accettato e' quello delle oe in un anno bisestile (24*366=8784)
		{	
			TSample = (TSample * SECONDI_IN_ORA);
			ReadEEPI2C(IICEEPConfigAddr(EEDiffSign), (uint8_t *)(&RTCError_Sign), sizeof(SizeEEDiffSign));							// Segno dell'errore del RTC: 1 = RTC resta indietro - 0 = RTC corre
			RTCError_Sign = (RTCError_Sign) ? true : false;
			CALM = 0;
			CALP = 0;
			if (TDiff != 0)
			{
				CALM = (TDiff*32768);
				CALP = (TSample / 32);				// DEBUG
				CALM = (CALM / CALP);
				CALP = RTCError_Sign;
				if (CALP == RTC_ritarda)
				{
					CALM = (512 - CALM);
				}
			}
			if ((pnt->Instance->CALR & RTC_CALR_CALM) != (CALM << RTC_CALR_CALM_Pos) ||
				(pnt->Instance->CALR & RTC_CALR_CALP) != (CALP << RTC_CALR_CALP_Pos))
			{																														// Il registro RTC_CALR e' diverso da CALM e CALP: lo aggiorno
				if (CALP == RTC_ritarda)
				{			
					if (HAL_RTCEx_SetSmoothCalib(&hrtc, RTC_SMOOTHCALIB_PERIOD_32SEC, RTC_SMOOTHCALIB_PLUSPULSES_SET, CALM) != HAL_OK)
					{
						Error_Handler();
					}
				}
				else
				{
					if (HAL_RTCEx_SetSmoothCalib(&hrtc, RTC_SMOOTHCALIB_PERIOD_32SEC, RTC_SMOOTHCALIB_PLUSPULSES_RESET, CALM) != HAL_OK)
					{
						Error_Handler();
					}
				}
			}
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: RTC_GetTimeAndDate
	Legge l'orologio interno alla MPU e memorizza i valori nella struttura passata come
	parametro.
	RTC_Counter: serve a risparmiare bytes per salvare data/ora in un uint32.

	IN:	  - pnt a struttura destinazione data/ora
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RTC_GetTimeAndDate(LDD_RTC_TTime *DateTime)
{
  	struct tm NuovaLettura;

	hrtc_devErr = HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	if (hrtc_devErr != HAL_OK)
	{
	  Error_Handler();
	}
	hrtc_devErr = HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	if (hrtc_devErr != HAL_OK)
	{
	  Error_Handler();
	}
	
	// Aggiorno struttura destinazione
	DateTime->Second 	= (uint32_t)sTime.Seconds;
	DateTime->Minute	= (uint32_t)sTime.Minutes;
	DateTime->Hour 		= (uint32_t)sTime.Hours;
	DateTime->Day 		= (uint32_t)sDate.Date;
	DateTime->DayOfWeek = (uint32_t)sDate.WeekDay;
	DateTime->Month 	= (uint32_t)sDate.Month;
	DateTime->Year 		= (uint32_t)(sDate.Year + STM32_REF_YEAR);								// Nell'RTC l'anno e' un offset dal 1900, quindi aggiungo 1900 per avere l'anno corrente

	// Trasformo data/ora in secondi da 01.01.2000 per Audit Estesa
	NuovaLettura.tm_sec = sTime.Seconds;
	NuovaLettura.tm_min = sTime.Minutes;
	NuovaLettura.tm_hour = sTime.Hours;
	NuovaLettura.tm_mday = sDate.Date;
	NuovaLettura.tm_mon = (sDate.Month -1);
	NuovaLettura.tm_year = sDate.Year;
	NuovaLettura.tm_wday = (sDate.WeekDay -1);
	NuovaLettura.tm_isdst = (int)DateTime->DayLightSaving;
	RTC_Counter = mktime(&NuovaLettura);

	/*
	if (RTC_Counter > SECS_20000101)
	{
		RTC_Counter -= SECS_20000101;															// Offset dal 01.01.2000
	}
	*/
}

/*---------------------------------------------------------------------------------------------*\
Method: RTC_SetTimeAndDate
	Aggiorna l'orologio interno alla MPU.
 
	IN:	  - pnt a struttura data/ora
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RTC_SetTimeAndDate(LDD_RTC_TTime *DateTime)
{
  	struct tm NuovaLettura;

	sTime.Hours 	= (uint8_t)DateTime->Hour;
	sTime.Minutes 	= (uint8_t)DateTime->Minute;
	sTime.Seconds 	= (uint8_t)DateTime->Second;
	sDate.Month 	= (uint8_t)DateTime->Month;
	sDate.Date 		= (uint8_t)DateTime->Day;
	sDate.WeekDay 	= (uint8_t)DateTime->DayOfWeek;
	sDate.Year 		= (uint8_t)(DateTime->Year - STM32_REF_YEAR);							// Nell'RTC l'anno e' un offset dal 1900, quindi sottraggo 1900 per avere il corretto offset per l'RTC
	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) == HAL_OK)
	{
		if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) == HAL_OK)
		{
			// Trasformo data/ora in secondi da 01.01.2000 per Audit Estesa
			NuovaLettura.tm_sec = sTime.Seconds;
			NuovaLettura.tm_min = sTime.Minutes;
			NuovaLettura.tm_hour = sTime.Hours;
			NuovaLettura.tm_mday = sDate.Date;
			NuovaLettura.tm_mon = (sDate.Month -1);
			NuovaLettura.tm_year = sDate.Year;
			NuovaLettura.tm_wday = (sDate.WeekDay -1);
			RTC_Counter = mktime(&NuovaLettura);
			if (RTC_Counter > SECS_20000101)
			{
				RTC_Counter -= SECS_20000101;													// Offset dal 01.01.2000
			}
		}
		else
		{
	  		Error_Handler();
		}
	}
	else
	{
		Error_Handler();
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ReadDateTime
	Funzione chiamata per frequenti letture del RTC.
 
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
/*
void  ReadDateTime(void)
{
	do
	{
		hrtc_devErr = HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
		if (hrtc_devErr == HAL_OK)
		{
		  	hrtc_devErr = HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
			if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
	  Error_Handler();
	}
		  
		}
	} while(hrtc_devErr != HAL_OK);
	
	
	if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
	  Error_Handler();
	}
	hrtc_devErr = HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
	  Error_Handler();
	}
}
*/


/*---------------------------------------------------------------------------------------------*\
Method: RTC_SetDayLightTime
	Aggiorna l'orologio interno alla CPU con i valori nella struttura passata come
	parametro, determinando l'ora legale o solare.
 
	IN:	  - pnt a struttura destinazione data/ora
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RTC_SetDayLightTime(LDD_RTC_TTime *DateTime, bool BKP)
{
	RTC_SetTimeAndDate(DateTime);
	RTC_Write_BKP(BKP);
}

/*---------------------------------------------------------------------------------------------*\
Method: RTC_Read_BKP
	Legge la flag RTC_BKP_FLAG contenuta nel registro BKP0R per sapere se e' gia' stata
	eseguita la correzione dell'ora.
	Il registro BKP0R e' uno dei 32 registri soggetti a backup con batteria del RTC quando
	manca alimentazione al uP.
 
	IN:	  - 
	OUT:  - bit BKP del Reg CR
\*----------------------------------------------------------------------------------------------*/
bool  RTC_Read_BKP(void)
{
 	RTC_HandleTypeDef	*pt;
	
	pt = &hrtc;
	return ((pt->Instance->BKP0R & RTC_BKP_FLAG)? true: false);
}

/*---------------------------------------------------------------------------------------------*\
Method: RTC_Write_BKP
	Scrive la flag BKP (Reg CR) per registrare correzione dell'ora.
 
	IN:	  - bit BKP
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RTC_Write_BKP(bool fBKP)
{
 	RTC_HandleTypeDef	*pt;
	
	pt = &hrtc;
	if (fBKP == true)
	{
    	pt->Instance->BKP0R |= ((uint32_t)RTC_BKP_FLAG);
	}
	else
	{
	    pt->Instance->BKP0R &= ((uint32_t)~RTC_BKP_FLAG);
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Read_UniqueID
	Return the three words of the unique device identifier (UID based on 96 bits)
	La first word (32 bit) e' definita nel datasheet come "UID[31:0]: X and Y coordinates on the wafer"
	La second word e' definita come "UID[63:40]: LOT_NUM[23:0]" e "UID[39:32]: WAF_NUM[7:0]"
	La third word e' definita  come "UID[95:64]: LOT_NUM[55:24]"

	IN:	  - 
	OUT:  - ritorna la first word
\*----------------------------------------------------------------------------------------------*/
void  Read_UniqueID(uint32_t *DestCodes) 
{
  	*DestCodes = HAL_GetUIDw0();
  	*(DestCodes+1) = HAL_GetUIDw1();
  	*(DestCodes+2) = HAL_GetUIDw2();
}

// ========================================================================
// ========    TIM3  P W M    B U Z Z E R         =========================
// ========================================================================
/*--------------------------------------------------------------------------------------*\
Method: Buz_start
	Attiva il Buzzer per la durata e con i toni (max 2) passati come parametri. 
	
 	IN:	  - Tempo di attivazione e tono/i
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Buz_start(uint32_t time, uint32_t tono1, uint32_t tono2)
{
#if DEB_NO_BUZZ

	return;

#else
  	uint32_t	temp;

	Touts.Buzz_time1 = time;
	Buz.freq1 = tono1;
	Buz.freq2 = 0;
	if (tono2 > 0) 
	{																							// Per il secondo tono inizializzo la frequenza 2
		Buz.freq2 = tono2;																		// e divido a meta' il tempo di attivazione buzzer: meta' per il tono 1 e meta'
		Touts.Buzz_time1 /= 2;																	// per il tono 2
		Touts.Buzz_time2 = Touts.Buzz_time1;
	} 
	// --- Start  PWM  con TIM3 ----
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	temp = SystemCoreClock;
	temp = (SystemCoreClock / htim3.Init.Prescaler);
	temp = (temp / tono1);
	htim3.Init.Period = temp;																	// Period = (SystemCoreClock / htim3.Init.Prescaler / Frequenza desiderata)
	if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
	{
		Error_Handler();
	}
	TIM3->CCR1 = (htim3.Init.Period / 2);														// Duty Cycle 50% = Period / 2
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: BuzFreqChange
	
 	IN:	  - Tono 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  BuzFreqChange(uint32_t tono)
{
  	uint32_t	temp;

	HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	//temp = SystemCoreClock;
	temp = (SystemCoreClock / htim3.Init.Prescaler);
	temp = (temp / tono);
	htim3.Init.Period = temp;																	// Period = (SystemCoreClock / htim3.Init.Prescaler / Frequenza desiderata)
	if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
	{
		Error_Handler();
	}
	TIM3->CCR1 = (htim3.Init.Period / 2);														// Duty Cycle 50% = Period / 2
}

/*--------------------------------------------------------------------------------------*\
Method: Buz_stop
	Disattiva il PWM Buzzer
	
 	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Buz_stop(void)
{
	HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
}

/*--------------------------------------------------------------------------------------*\
Method: IsBuzzerOFF
	Rende lo stato del buzzer per permettere una sua eventuale riattivazione. 
	
 	IN:	  - 
 	OUT:  - True se Buzzer OFF, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  IsBuzzerOFF(void)
{
	return (((Touts.Buzz_time1 + Touts.Buzz_time2) == 0) ? true : false );
}

/*--------------------------------------------------------------------------------------*\
Method: Is_5V_USB_HOST_PC
	Verifica la presenza di tensione +5V proveniente dall'USB Host (PC) eventualmente
	connesso.
	
 	IN:	  - 
 	OUT:  - True con +5V presente, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  Is_5V_USB_HOST_PC(void)
{
	GPIO_PinState TestResult;
  
  	TestResult = HAL_GPIO_ReadPin(VDD_USB_GPIO_Port, VDD_USB_Pin);
	return (TestResult == GPIO_PIN_SET? true : false);
}

/*---------------------------------------------------------------------------------------------*\
Method: USB_Key_Voltage
	Spegne o accende il +5V alla chiave USB

	IN:	  - On_Off
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  USB_Key_Voltage(uint8_t On_Off)
{
	if (On_Off == ON)
	{
		HAL_GPIO_WritePin(USB_PWR_ENA_GPIO_Port, USB_PWR_ENA_Pin, GPIO_PIN_RESET);				// +5V ON
	} else {
		HAL_GPIO_WritePin(USB_PWR_ENA_GPIO_Port, USB_PWR_ENA_Pin, GPIO_PIN_SET);				// +5V OFF
	}
}

/*--------------------------------------------------------------------------------------*\
Method: Is_5V_USB_Key_Active
	Verifica se attiva l'uscita che alimenta la Key USB.
	
 	IN:	  - 
 	OUT:  - True con uscita +5V ON come Host verso chiavi USB, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  Is_5V_USB_Key_Active(void)
{
	GPIO_PinState TestResult;
  
  	TestResult = HAL_GPIO_ReadPin(USB_PWR_ENA_GPIO_Port, USB_PWR_ENA_Pin);
	return (TestResult == GPIO_PIN_RESET? true : false);
}


// ========================================================================
// ========    WWDG  W A T C H       D O G        =========================
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: WatchDogReset
	Attiva il WatchDog e attende reset uP.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  WatchDogReset(void)
{
	MIFARE_OFF();																				// Tolgo tensione +5V Mifare altrimenti il uP riparte in Boot dopo il reset in uscita programmazione
	MIFARE_COMM_DeInit();																		// Per togliere tensione al pin +5 MIF collegato al Boot del uP
	HAL_Delay(TwoSec);																			// Attendo riduzione tensione pin +5V MIfare connesso al pin Boot del uP
	MX_WWDG_Init();
	while(true){};
}