/****************************************************************************************
File:
    ExecMaster.c

Description:
    File con le funzioni del Protocollo Executive Master, da utilizzare in un VMC 
    Executive Slave per pilotare anche un secondo VMC Executive Slave.
    

History:
    Date       Aut  Note
    Giu 2017	MR   

*****************************************************************************************/

#include "ORION.H"
#include "Gestore_Exec_MSTSLV.h"
#include "ExecMaster.h"
#include "OrionTimers.h"
#include "VMC_CPU_HW.h"



#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void  			Monitor_ExecVendReq(uint8_t);

/*--------------------------------------------------------------------------------------*\
Global Defines
\*--------------------------------------------------------------------------------------*/
uint8_t		VMC_Command;

static uint16_t  		VMC_CreditoInviato;
static ExecMasterVar	sExecMastVar;

const uint8_t DPP_Converse[] = {1,2,4,8,0};

/*---------------------------------------------------------------------------------------------*\
Method: EXEC_Master_Task
	Funzione chiamata continuamente dal MAIN
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void EXEC_Master_Task(void) {
	
	if ((gOrionKeyVars.ExecState == MasterSuspended) || (fVMC_NoLink)) return;					// Esce se disabilitato o c'e' NoLink con la RR
	
/*	
	if (gOrionKeyVars.ExecState == RR_Busy_Vend_Nova) {
		gOrionKeyVars.ExecResponseCondition = TX_COMANDO_NO_RESPONSE;
		gOrionKeyVars.ExecTxBuffer[0] = EXEC_VMC_STATUS;
		ExecSendMessage(COMMAND_LEN, kExecToutNovaInVend, kExecToutCMD);						// Tx 1 chr (LEN) tra 1 sec e tout wait risp di 2 minuti
		return;
	}
*/	
	Check_VMC_Answer();
	DelayBeforeTxCMD();																			// Se ci sono comandi da inviare e il tempo attesa e' scaduto, invia il comando

	if (gOrionKeyVars.ExecResponseCondition != TX_COMANDO_NO_RESPONSE) {
	  if (gOrionKeyVars.ExecResponseCondition == NO_LINK_SERIALE) {
		Set_fVMC2_NoLink;
		Set_fSendCreditToVMC;																	// Per rivisualizzare correttamente quando torna il link
	  } else {
		  if (gOrionKeyVars.ExecResponseCondition == TX_COMANDO_FAIL_PNACK) {
			  Clear_fVMC2_NoLink;                                								// Clr tranne che con Timeout
		  } else {
			  if (gOrionKeyVars.ExecResponseCondition == TX_COMANDO_OK) {
				  Clear_fVMC2_NoLink;
			  }
		  }
		  switch(gOrionKeyVars.ExecState) {
	
		  	  case VMC_STATUS :
		  	  case RR_Busy_Vend_Nova :
				  GestioneVMCStatusState();
				  break;
		
			  case VMC_CREDIT :
				  GestioneVMCCreditState();
				  break;
		
			  case WAIT_VISUAL_NEW_CREDIT :
					Send_VMC_Command(EXEC_VMC_STATUS);
			    	gOrionKeyVars.ExecState = VMC_STATUS;                  
				  break;
		
			  }
		  }
	}
}

/**********************************************************/
/*  Gestione  STATUS State:                               */
/**********************************************************/
// Se il VMC Master e' in selezione si inviano solo STATUS e
// il Credito se modificato dalla RR.

void GestioneVMCStatusState(void) {
	
	if (gOrionKeyVars.ExecResponseCondition == TX_COMANDO_OK) {
		if (gOrionKeyVars.ExecState == RR_Busy_Vend_Nova) {
			gOrionKeyVars.ExecResponseCondition = TX_COMANDO_NO_RESPONSE;
			gOrionKeyVars.ExecTxBuffer[0] = EXEC_VMC_STATUS;
			ExecSendMessage((uint8_t)COMMAND_LEN, (uint8_t)kExecToutNovaInVend, (uint32_t)kExecToutCMD);						// Tx 1 chr (LEN) tra 1 sec e tout wait risp di 2 minuti
			return;
		}

		if ((gOrionKeyVars.ExecResponseData & VMC_INIBIT_FLAG) == FALSE) {
			Clear_fVMC2_Inibito;
			if (((gOrionKeyVars.ExecResponseData & VMC_FV_REQ_FLAG) == FALSE) || gOrionConfVars.PriceHolding) {  				// Ignora free-vend request in price holding
				Clear_fVMC2FreeVendRequired;
				if ((VMC_CreditoDaVisualizzare != VMC_CreditoInviato) || fSendCreditToVMC || sExecMastVar.reSendCredit ) {
					Clear_fSendCreditToVMC;
					sExecMastVar.reSendCredit= false;
					VMC_Credito(VMC_CreditoDaVisualizzare);
					gOrionKeyVars.ExecState = WAIT_VISUAL_NEW_CREDIT;                  
				} else {
					if (RR_ReadyForVMC2Vend() == 2) {												// VMC Master in selezione e RR attende Fine vend: invio solo STATUS
						Send_VMC_Command(EXEC_VMC_STATUS);
					} else {
						if (gOrionConfVars.PriceDisplay || gOrionConfVars.PriceHolding) {
							Send_VMC_Command(EXEC_VMC_CREDIT);
							gOrionKeyVars.ExecState = VMC_CREDIT;                  
						} else {
							if (VMC_CreditoDaVisualizzare == 0) {
								Send_VMC_Command(EXEC_VMC_STATUS);
							} else {
								Send_VMC_Command(EXEC_VMC_CREDIT);
								gOrionKeyVars.ExecState = VMC_CREDIT;                  
							}
						}
					}
				}
			} else {
				// -- Richiesta una Free Vend dal VMC2 ---
				if (RR_ReadyForVMC2Vend() == 0) {													// RR Ready
					Set_fVMC2FreeVendRequired;
					gOrionKeyVars.ExecState = MasterSuspended;										// Disattiva esecuzione Master
				} else {
					Send_VMC_Command(EXEC_VMC_STATUS);												// Ignora la FV Req del VMC2
				}
			}
		} else {
			// ********   VMC2  Inhibit   **************
			Set_fVMC2_Inibito;
			if ((VMC_CreditoDaVisualizzare != VMC_CreditoInviato) || fSendCreditToVMC) {
    			#if kTxCreditAgainWhenInh
					sExecMastVar.reSendCredit= true;												// Invia ancora il credito quando non piu' inibito
				#endif
				Clear_fSendCreditToVMC;
				VMC_Credito(VMC_CreditoDaVisualizzare);
				gOrionKeyVars.ExecState = WAIT_VISUAL_NEW_CREDIT;                  
			} else {
				Send_VMC_Command(EXEC_VMC_STATUS);
			}
		}
	} else {
		Send_VMC_Command(EXEC_VMC_STATUS);
	}
}


/**********************************************************/
/*  Gestione  CREDIT State:                               */
/**********************************************************/

void GestioneVMCCreditState(void) {

	if (gOrionKeyVars.ExecResponseCondition == TX_COMANDO_OK) {
		if (gOrionKeyVars.ExecResponseData == RX_VMC_NO_VEND_REQUEST) {
			
			// ---  VMC2  NO VEND  REQUEST  -----
			if ((VMC_CreditoDaVisualizzare != VMC_CreditoInviato) || fSendCreditToVMC) { 
				Clear_fSendCreditToVMC;
				VMC_Credito(VMC_CreditoDaVisualizzare);
				gOrionKeyVars.ExecState = WAIT_VISUAL_NEW_CREDIT;                  
			} else {
				Send_VMC_Command(EXEC_VMC_STATUS);
				gOrionKeyVars.ExecState = VMC_STATUS;                  
			}
		} else {
			
			// ---  VMC2  VEND   REQUEST  -----
			VMC_Response = VMC_RESP_NO_RESPONSE;
			Monitor_ExecVendReq(gOrionKeyVars.ExecResponseData);								// Se in Monitor, echo al PC la richiesta di vendita del VMC
			if (RR_ReadyForVMC2Vend() == 0) {													// RR Ready
				Set_VMC2_VendReq(gOrionKeyVars.ExecResponseData);								// Vend Request e valore selezione da inviare alla RR
				gOrionKeyVars.ExecState = MasterSuspended;										// Disattiva esecuzione Master
			} else {
				Send_VMC_Command(EXEC_VMC_STATUS);												// VEND DENIED per RR non Ready o VMC Master in Selezione
				gOrionKeyVars.ExecState = VMC_STATUS;                  
			}
/*
			if (GestMS_VMC2_VendReq(gOrionKeyVars.ExecResponseData) == TRUE) {					
				gOrionKeyVars.ExecState = MasterSuspended;										// Disattiva esecuzione Master
			} else {
				Send_VMC_Command(EXEC_VMC_STATUS);												// VEND  DENIED da GestMS
				gOrionKeyVars.ExecState = VMC_STATUS;                  
			}
*/			
		}
	} else {
		Send_VMC_Command(EXEC_VMC_STATUS);
		gOrionKeyVars.ExecState = VMC_STATUS;                  
	}
}

/*----------------------------------------------------------------------------------------------*\
 Method: Send_VMC_Command
   Predispone per invio comando al VMC2.

   IN:  - 
  OUT:  -  
\*----------------------------------------------------------------------------------------------*/
static void Send_VMC_Command(uint8_t comando) {
	
	gOrionKeyVars.ExecResponseCondition = TX_COMANDO_NO_RESPONSE;
	gOrionKeyVars.ExecTxBuffer[0] = comando;
	if (comando == EXEC_VMC_VEND) {
		ExecSendMessage(COMMAND_LEN, kExecInterCommand, kExecToutVend);							// Tx 1 chr (LEN) tra 50 msec (TIME_COMANDO) e tout wait risp di 2 minuti
	} else {
		ExecSendMessage(COMMAND_LEN, kExecInterCommand, kExecToutCMD);							// Tx 1 chr (LEN) tra 50 msec (TIME_COMANDO) e tout wait risp di 10 secondi
	}
}

/*----------------------------------------------------------------------------------------------*\
 Method: VMC_Credito
   Predispone nuova visualizzazione di un credito al VMC.

   IN:  -  valore da visualizzare
  OUT:  -  
\*----------------------------------------------------------------------------------------------*/
void VMC_Credito(uint16_t valore) {
	
	sExecMastVar.TimeoutSendPriceToVMC.tmrEnd = 0;
	VMC_CreditoInviato = valore;                                								// Salvo valore credito inviato
	Send_VMC_Credito(valore);
}

/*
* gOrionKeyVars.ExecTxBuffer 0  :  Credito  nibble 0 (LSB)
* gOrionKeyVars.ExecTxBuffer 1  :  Credito  nibble 1
* gOrionKeyVars.ExecTxBuffer 2  :  Credito  nibble 2
* gOrionKeyVars.ExecTxBuffer 3  :  Credito  nibble 3 (MSB)
* gOrionKeyVars.ExecTxBuffer 4  :  USF      nibble L (LSB)
* gOrionKeyVars.ExecTxBuffer 5  :  USF      nibble H (MSB)
* gOrionKeyVars.ExecTxBuffer 6  :  DPP
* gOrionKeyVars.ExecTxBuffer 7  :  EXC
* gOrionKeyVars.ExecTxBuffer 8  :  SYNC
*/
/*----------------------------------------------------------------------------------------------*\
 Method: Send_VMC_Credito
   Invia la visualizzazione di un credito al VMC.

   IN:  -  valore da visualizzare
  OUT:  -  
\*----------------------------------------------------------------------------------------------*/
void Send_VMC_Credito(uint16_t valore) {
	
	uint16_t credito_scalato;
	uint16_t dpp,usf;

	usf = UnitScalingFactor;
	dpp = DecimalPointPosition;
 
	gOrionKeyVars.ExecResponseCondition = TX_COMANDO_NO_RESPONSE;
	credito_scalato = valore / usf;
	gOrionKeyVars.ExecTxBuffer[0] = EXEC_VMC_ACCEPT_DATA;
	gOrionKeyVars.ExecTxBuffer[1] = ((credito_scalato & 0x000f) | EXEC_VMC_ADDRESS);
	gOrionKeyVars.ExecTxBuffer[2] = (((credito_scalato & 0x00f0) >> 4) | EXEC_VMC_ADDRESS);
	gOrionKeyVars.ExecTxBuffer[3] = (((credito_scalato & 0x0f00) >> 8) | EXEC_VMC_ADDRESS);
	gOrionKeyVars.ExecTxBuffer[4] = (((credito_scalato & 0xf000) >> 12) | EXEC_VMC_ADDRESS);
	gOrionKeyVars.ExecTxBuffer[5] = ((usf & 0x0f) | EXEC_VMC_ADDRESS);
	gOrionKeyVars.ExecTxBuffer[6] = (((usf & 0xf0) >> 4) | EXEC_VMC_ADDRESS);
	if (dpp <= 4) {
		gOrionKeyVars.ExecTxBuffer[7] = (DPP_Converse[dpp] | EXEC_VMC_ADDRESS);
	} else {
		gOrionKeyVars.ExecTxBuffer[7] = (EXEC_VMC_ADDRESS);
	}

	gOrionKeyVars.ExecTxBuffer[8] = (EXEC_VMC_ADDRESS);											// Predispongo NO Exact-change
	if (fExactChange) gOrionKeyVars.ExecTxBuffer[8] = (0x01 | EXEC_VMC_ADDRESS);
	gOrionKeyVars.ExecTxBuffer[9] = EXEC_VMC_SYNC;
	ExecSendMessage(CREDIT_LEN, kExecInterByteDisplayData, kExecToutCMD);						// Tx 10 chr (LEN) ogni 2 msec (TIME_STRINGA) e tout wait risp di 10 sec
}

/*----------------------------------------------------------------------------------------------*\
 Method: ExecSendChar
	Funzione per predisporre l'invio di un comando: copia il comando in byteToSend, e carica
	il timeout toInterByteTime trascorso il quale, nella DelayBeforeTxCMD, si invia il byte
	alla UART per la trasmissione.
   
   IN:  -  sExecMastVar.txPointer as pointer al comando da inviare e sExecMastVar.toInterByteTime as 
		   timeout
  OUT:  -  
\*----------------------------------------------------------------------------------------------*/
static void ExecSendChar(void) {

	sExecMastVar.byteToSend 	= *sExecMastVar.txPointer;
	sExecMastVar.fByteToSend  	= true;																// Abilita test fine attesa in "DelayBeforeTxCMD" per invio nuovo comando
	sExecMastVar.fWaitResponse  = false;
	Tmr32Start(sExecMastVar.tmrWaitResponse, sExecMastVar.TimeAttesaRisposta);						// Carico timeout attesa risposta dal VMC (10 sec o 2 min)
	Tmr32Start(sExecMastVar.tmrInterByteTime, sExecMastVar.toInterByteTime);						// Carico tempo di attesa prima di inviare il comando al VMC
	Tmr32Start(sExecMastVar.TimeoutNoLink, kExecToutNoLink);										// Carico timeout per il NoLink (5 sec)
}

/*----------------------------------------------------------------------------------------------*\
 Method: ExecSendMessage
	Funzione per predisporre l'invio di un comando.
   
   IN:  -  
  OUT:  -  
\*----------------------------------------------------------------------------------------------*/
void ExecSendMessage(uint8_t messageLenght, uint8_t Delay_To_Tx, uint32_t ToutWaitResponse)
{
	sExecMastVar.TimeAttesaRisposta  = ToutWaitResponse;											// Timeout attesa risposta dal DA: 10 sec a riposo, 2 minuti durante VEND
	sExecMastVar.toInterByteTime 	 = Delay_To_Tx;													// Tempo di attesa prima di inviare il comando (50 msec se CMD, 2 msec se Dato)
	sExecMastVar.messageLenght   	 = messageLenght;
	sExecMastVar.txPointer		 	 = gOrionKeyVars.ExecTxBuffer;
	sExecMastVar.retryCnt 		 	 = kRetryCnt;													// Numero di retry
	sExecMastVar.ToutRetryCnt 	 	 = kNumToutWaitVMCAnswer;										// Numero di volte che scade il timeput attesa risposta dal VMC
	ExecSendChar();
}

/*------------------------------------------------------------------------------------------------*\
 Method: Check_VMC_Answer
	Funzione che attende la risposta del DA controllando lo scadere del timeout TimeAttesaRisposta
 	impostato in tmrWaitResponse e il numero di volte che tale timeout puo' scadere (ToutRetryCnt).
 	Dopo avere ritrasmesso "ToutRetryCnt" volte l'ultimo chr inviato, si cancella la flag attesa
	risposta (fWaitResponse) e la ExecResponseCondition assume il valore "RetryCounterTOExpired".
	Tale valore, pero', non e' testato come tale nella "EXEC_Master_Task" ma dara' comunque origine alla 
	trasmissione del comando STATUS perche' nello stato in cui si trova l'Executive si accorge di
	non avere avuto risposta dal DA e quindi, di default, si procede con l'invio di STATUS.

	IN:  -  
	OUT: -  
\*------------------------------------------------------------------------------------------------*/
static void Check_VMC_Answer(void) {

	if (sExecMastVar.fWaitResponse == true ) {
		if (TmrTimeout(sExecMastVar.tmrWaitResponse)) {													// Timeout attesa risposta scaduto
			sExecMastVar.ToutRetryCnt--;																// Numero di volte che il timeout attesa risposta puo' scadere
			if (sExecMastVar.ToutRetryCnt == 0) {
				sExecMastVar.fWaitResponse = false;														// Esauriti tentativi di avere risposta dal VMC: ricomincio con STATUS
				gOrionKeyVars.ExecResponseCondition = RetryCounterTOExpired;
			} else { 
				//sExecMastVar.tmrWaitResponse = gVars.gKernelFreeCounter;								// Precedente
				Tmr32Start(sExecMastVar.tmrWaitResponse, sExecMastVar.TimeAttesaRisposta);				// Ricarico il timeout attesa risposta
				sExecMastVar.byteToSend = *sExecMastVar.txPointer;										// Ricarico ultimo byte inviato al VMC
				Master_Exec_TxChar(sExecMastVar.byteToSend);											// Ritrasmetto al VMC ultimo byte inviato
			}
		} else {
			//if (sExecMastVar.TimeAttesaRisposta >= (kExecToutWaitVMCAnswer/2) && (gVars.gKernelFreeCounter-sExecMastVar.tmrWaitResponse) >= (kExecToutWaitVMCAnswer/2)) {
			if (TmrTimeout(sExecMastVar.TimeoutNoLink)) {												// Timeout NoLink scaduto
				gOrionKeyVars.ExecResponseCondition = NO_LINK_SERIALE;
			}
		}
	}
}

/*----------------------------------------------------------------------------------------------*\
 Method: DelayBeforeTxCMD
 	 Funzione per l'invio di un chr al DA.
 	 Se c'e' un byte da trasmettere (fByteToSend=true) ed e' trascorso il tempo di attesa dopo
 	 il chr precedente (sExecMastVar.tmrInterByteTime), si chiama la MasterExecTxChar per l'invio
 	 vero e proprio.
   
   IN:  -  sExecMastVar.fByteToSend e sExecMastVar.tmrInterByteTime
  OUT:  -  
\*----------------------------------------------------------------------------------------------*/
static void DelayBeforeTxCMD(void) {

	if (sExecMastVar.fByteToSend == true && TmrTimeout(sExecMastVar.tmrInterByteTime)) {			// Attesa dal Tx CMD precedente scaduta: posso inviare nuovo comando
		sExecMastVar.fByteToSend 	= false;														// Clear flag comando in attesa di trasmissione
		sExecMastVar.fWaitResponse  = true;															// Set flag attesa risposta dal VMC
		Master_Exec_TxChar(sExecMastVar.byteToSend);												// Trasmetto nuovo comando
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ExecMasterRxErr
	IRQ di ricezione di un errore nella risposta dal VMC2.
 
	IN:		- SCI Status Register 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void ExecMasterRxErr(void) {

	sExecMastVar.byteToSend 	= EXEC_Master_NACK;
	sExecMastVar.fWaitResponse  = false;														// Clr flag attesa risposta dal VMC: scarto eventuale chr ricevuto con l'errore
	sExecMastVar.fByteToSend  	= true;
	Tmr32Start(sExecMastVar.tmrInterByteTime, sExecMastVar.toInterByteTime);					// Carico tempo di attesa prima di inviare il comando al VMC
}

/*---------------------------------------------------------------------------------------------*\
Method: ExecMasterRxDataVMC2
	IRQ di ricezione risposta dal VMC2.
 
	IN:	  - Byte ricevuto dallo slave VMC2
	OUT:  -
\*----------------------------------------------------------------------------------------------*/
void ExecMasterRxDataVMC2(uint8_t responseData) {

	if (sExecMastVar.fWaitResponse == true ) {
		sExecMastVar.fWaitResponse = false;
		gOrionKeyVars.ExecResponseData = responseData;

		if (gOrionKeyVars.ExecResponseData == PNACK) {											// Ricevuto NACK (0xFF) dal VMC
			sExecMastVar.retryCnt--;															// Counter tentativi di ritrasmissione
			if (sExecMastVar.retryCnt > 0) {													// Ritrasmetto ultimo byte inviato
				ExecSendChar();
				sExecMastVar.ToutRetryCnt = kNumToutWaitVMCAnswer;								// Ricarico numero di volte che puo' scadere il timeout attesa risposta
			} else {
				gOrionKeyVars.ExecResponseCondition = RetryCounterNAckExpired;
			}
		} else {
			sExecMastVar.messageLenght--;
			if (sExecMastVar.messageLenght > 0) {												// Trasmetto nuovo byte del messaggio da inviare
				sExecMastVar.txPointer++;
				ExecSendChar();
				sExecMastVar.retryCnt = kRetryCnt;												// Ricarico Counter tentativi di ritrasmissione in caso di PNACK
				sExecMastVar.ToutRetryCnt = kNumToutWaitVMCAnswer;								// Ricarico numero di volte che puo' scadere il timeout attesa risposta
			}  else {
				gOrionKeyVars.ExecResponseCondition = TX_COMANDO_OK;							// Fine invio messaggio
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------

/*---------------------------------------------------------------------------------------------*\
Method: ExecInit
	Funzione chiamata la prima volta che si esegue il MAIN
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void ExecMasterInit(void) {

	sExecMastVar.fByteToSend = false;
	if (UnitScalingFactor == 0) {
		UnitScalingFactor = 1;
	}
	Master_Exec_Comm_Init();
	gOrionKeyVars.ExecState = VMC_STATUS;                  
	Set_fSendCreditToVMC;
	Set_fVMC2_Inibito;
	Send_VMC_Command(EXEC_VMC_STATUS);

}

/*--------------------------------------------------------------------------------------*\
Method: RestartExecMaster
	Ricomincia il colloquio con il VMC2 con STATUS
	
Parameters:
	IN	- 
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  RestartExecMaster(void) {
	
	Send_VMC_Command(EXEC_VMC_STATUS);
	gOrionKeyVars.ExecState = VMC_STATUS;
	Set_fSendCreditToVMC;																		// Aggiorna il Credito sul VMC2
}

/*--------------------------------------------------------------------------------------*\
Method: ClrMasterSuspend
	Riabilita Master Executive se in sospeso.
	
Parameters:
	IN	- 
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  ClrMasterSuspend(void) {
	
	if (gOrionKeyVars.ExecState == MasterSuspended) {
		Send_VMC_Command(EXEC_VMC_STATUS);
		gOrionKeyVars.ExecState = VMC_STATUS;                  
	}
}

/*--------------------------------------------------------------------------------------*\
Method: SetMasterTxStatusOnly
	Predispone il Master ad inviare il solo STATUS al VMC2 perche' siamo in VMC Master Vend 
	senza Resto subito, quindi RR busy in attesa di fine Vend.
	Il VMC2 quindi non puo' richiedere vendite, basta lo STATUS trasmesso ogni secondo
	per non disattivare il VMC2 da NoLink.
	
Parameters:
	IN	- 
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  SetMasterTxStatusOnly(void) {
	
	gOrionKeyVars.ExecState = RR_Busy_Vend_Nova;
}

/*--------------------------------------------------------------------------------------*\
Method: ClrMasterTxStatusOnly
	Riabilita il Master al normale funzionamento. 
	
Parameters:
	IN	- 
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  ClrMasterTxStatusOnly(void) {
	
	if (gOrionKeyVars.ExecState == RR_Busy_Vend_Nova) {
		Send_VMC_Command(EXEC_VMC_STATUS);
		gOrionKeyVars.ExecState = VMC_STATUS;                  
	}
}
/*--------------------------------------------------------------------------------------*\
Method: ExecReaderVendApp

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void ExecReaderVendApp(bool approved) {
	
	VMC_Command = approved ? VMC_CMD_VEND_APPROVED : VMC_CMD_VEND_DENIED;
}

/*--------------------------------------------------------------------------------------*\
Method: ExecReaderVendEnd

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/
bool ExecReaderVendEnd(void) {
	
	return (VMC_Response != VMC_RESP_NO_RESPONSE);
}







