/****************************************************************************************
File:
    AWS_subscribe_publish.c

Description:
    File con funzioni di elaborazione messaggi scambiati con AWS.
    

History:
    Date       Aut  Note
    Apr 2021	MR   

*****************************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>


#include "ORION.H" 
#include "IICEEP.h"
#include "IoT_Files.h"
#include "I2C_EEPROM.h"
#include "stm32l4xx_hal.h"						// Contiene il prototipo della "HAL_Delay"
#include "Monitor.h"
#include "IoT_Comm.h"
#include "VMC_Main.h"
#include "VMC_CPU_Motori.h"
#include "AWS_subscribe_publish.h"
#include "general_utility.h"
#include "VMC_CPU_Prog.h"



#if (IOT_PRESENCE == true)

/*--------------------------------------------------------------------------------------*\
Private Declarations 
\*--------------------------------------------------------------------------------------*/

char* 			pnt_transID;
char 			Stringtransactionid[]	= "transactionid";
static uint16_t	RxConf_EEAdr = 0;
static uint16_t	RxConf_Start_Address = 0;


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void  StartErogazioneDaRemoto(uint8_t NumErogaz, uint16_t FlowMeterCount);
extern	bool  IsSelParameterCorrect(uint8_t NumErogaz, uint32_t Param);

/*--------------------------------------------------------------------------------------*\
Method: MQTTcallbackHandler
	Funzione chiamata dalla "Gtwy_Poll_Answ" che passa il buffer ricevuto da AWS.

Parameters:
	IN	- messaggio in "params.payload"
	OUT	- 
\*--------------------------------------------------------------------------------------*/
IOT_COMM_STATE   MQTTcallbackHandler(char *params)
{

// ------  Private Variables -----
uint8_t			len, response, ActualBlockNum;
uint16_t		j, CommandNum;
uint32_t		Temp;
char			resultBuff[RESULT_BUF_LEN];
char* 			pnt_param;
IOT_COMM_STATE	NextBuffTypeToAWS;

char 			puntovendita[]	= "pointofsaleid";
char 			code_string[]	= "code";
char 			parametri[]		= "parameters";
char 			numeroblocco[]	= "blocknum";
uint16_t		LenghtCnt;
char* 			pnt_param2;


	NextBuffTypeToAWS = IoT_POLL;																		// Predispongo invio POLL alla Gateway se ricevuto file sconosciuto
	response = AWS_CMD_REJECTED;
	
	//-- Ricerca Codice Macchina --
	pnt_param = (strstr(params, puntovendita));															// Ricerca stringa "pointofsaleid"
	if (pnt_param == NULL)
	{
		//msg_info("\nComando senza codice macchina\n");
	}	
	else																								// Trovata stringa "pointofsaleid"
	{
		//-- Estrazione e conversione Codice Macchina --
		len = strlen(puntovendita);																		// Lunghezza stringa "pointofsaleid"
		pnt_param += len;																				// "pnt_param" punta alla fine della stringa "pointofsaleid"
		pnt_param = Find_ParamValue(pnt_param, ASCII_NUMERIC, MAX_NUM_CHR);								// Cerca il primo valore numerico del parametro partendo dal pointer indicato
		if (pnt_param == NULL) 
		{
			//msg_info("\nComando senza codice macchina\n");
		}
		else
		{
			memset(&resultBuff[0], 0x0, RESULT_BUF_LEN);
			for (j=0; j < MACHINE_CODE_MAX_LEN; j++)
			{
				resultBuff[j] = *(pnt_param + j);
				if (is_ascii_num(*(pnt_param + j)) == false)											// Chr non numerico: termino estrazione Codice Macchina
				{
					resultBuff[j] = NULL;
					break;
				}
			}
			Temp = atoi(&resultBuff[0]);
			if (Temp != CodiceMacchina)
			{
				//msg_info("\nMessaggio non indirizzato a questa macchina\n");
			}
			else
			{
				// -- Il comando e' per questa macchina --
				CommandNum = 0;
				pnt_param = (strstr(params, code_string));												// Ricerca stringa "code"
				if (pnt_param != NULL)
				{																						// Trovata stringa "code"
					len = strlen(code_string);															// Lunghezza stringa "code"
					pnt_param += len;																	// "pnt_param" punta alla fine della stringa "code"
					pnt_param = Find_ParamValue(pnt_param, ASCII_NUMERIC, MAX_NUM_CHR);					// Cerca il primo valore numerico del parametro partendo dal pointer indicato
					if (pnt_param == NULL) 
					{
						//msg_info("\nCodice Comando Errato\n");
					}
					else
					{
						//-- Estraggo il comando valido dal buffer --
						if (is_ascii_num(*(pnt_param + 1)) == false)
						{
							CommandNum = (uint16_t) ascii_to_hex_num(*(pnt_param));						// Comando ad una cifra (1-9)
						}
						else
						{
							CommandNum = (uint16_t)														// Comando a 2 cifre (10-99)
							(	
								((ascii_to_hex_num(*(pnt_param + 0))) << 4) |		
								((ascii_to_hex_num(*(pnt_param + 1))) << 0)
							);
							CommandNum = bcd_to_hex(CommandNum);
						}
					}
				}
				switch(CommandNum)
				{

					case CMD_AUD_REQ_WOUT_RESET:
						// *********************************************************************
						// *******   C O M A N D O   1:    AUDIT REQUEST WithOUT RESET *********
						// *********************************************************************
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						if ((IsErogazInProgress() == false) && (gVMCState.InProgrammaz == false) &&
							(Is_Machine_StBy() == true))
						{
							if (Export_Audit_to_AWS() == false)
							{
								NextBuffTypeToAWS = IoT_POLL;																	// La "Export_Audit_to_AWS" ha inviato direttamente l'Audit ad AWS: non invio alcun buffer
							}
							else
							{
								Response_AWS_OK_Rejected(response, CMD_AUD_REQ_WOUT_RESET);										// Predispongo msg di risposta "REJECTED" 
								NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;														// Quando esce invia il buffer Status 
							}
						}					
						else
						{
								Response_AWS_OK_Rejected(response, CMD_AUD_REQ_WOUT_RESET);										// Predispongo msg di risposta "REJECTED" 
								NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;														// Quando esce invia il buffer Status 
						}
						break;	
					
					case CMD_AUD_REQ_W_RESET:
						// *********************************************************************
						// *******   C O M A N D O   2:    AUDIT REQUEST WITH RESET    *********
						// *********************************************************************
						//GTW_command = PK3_AUDREQ_W_RES;
						//command_requested = true;
						break;
					
					case CMD_NEW_CONFIG:
						// **********************************************************************************
						// *******   C O M A N D O   3: - Versione 1:   NEW CONFIG FROM REMOTE      *********
						// **********************************************************************************
						// L'invio della configurazione da parte della dashboard puo' essere frazionato in blocchi quando il size
						// totale supera il size del buffer di comunicazione con AWS (2K al Apr 2021).
						// Memo tutti i blocchi ricevuti in EE senza alcun controllo del numero del blocco, memorizzando il
						// nuovo address EE dove salvare il blocco successivo.
						// Controllo solo se e' il blocco N. 1 che inizializza l'Address pointer alla EE.
						// Il restart della CPU per acquisire la nuova configurazione deve avvenire tramite apposito comando 90.
						// L'Address pointer alla EE e' resettato inoltre ad ogni restart.
						// Rispondo "OK" ad ogni blocco ricevuto regolarmente, rispondo REJECT se la EE e' full o non ci sono le
						// condizioni per salvare il blocco.
						if ((IsErogazInProgress() == false) && (Is_Machine_StBy() == true) && (gVMCState.InProgrammaz == false))
						{
							//--  Ricerca e controllo parametro blocknumber --
							len = (EstraeParametroNumerico(params, &numeroblocco[0], &ActualBlockNum, MAX_NUM_BLOCKS_CHR));	// In len result funzione
							if ((len == ERR_OK) && (ActualBlockNum > 0))
							{
								ActualBlockNum = bcd_to_hex(ActualBlockNum);
								Init_EEAddr_Config_Block_Save(ActualBlockNum);
								//--  Ricerca e controllo stringa in "parameters" --
								pnt_param = (strstr(params, parametri));
								if (pnt_param != NULL)
								{
									len = strlen(parametri);															// Lunghezza stringa "parameters"
									pnt_param += len;																	// "pnt_param" punta alla fine della stringa "parameters"
									pnt_param = Find_ParamValue(pnt_param, ASCII_LITERAL_A_z, MAX_NUM_CHR);				// Cerca la prima lettera partendo dal pointer indicato
									if (pnt_param != NULL) 
									{																					// Trasferisco la nuova configurazione in EE fino a quando trovo il chr '"'
										//-- Controllo limite di memoria EE ancora disponibile --
										pnt_param2 = (strstr(params, code_string));										// Memory Address inizio stringa "code" che termina i parametri
										LenghtCnt = ((char*)pnt_param2) - ((char*)pnt_param);							// Lunghezza buffer parametri
										if (((RxConf_EEAdr - RxConf_Start_Address) + LenghtCnt) < EE_TXCONFBUF_LEN)
										{
											j = 0;
											memset(&gVars.tempRTx1, 0x0, sizeof(gVars.tempRTx1));
											do
											{
												gVars.tempRTx1[j++] = *(pnt_param++);
												if (j > (sizeof(gVars.tempRTx1) - 10))
												{
													WriteEEPI2C(RxConf_EEAdr, (uint8_t *)(&gVars.tempRTx1), j);
													RxConf_EEAdr += j;
													j = 0;
													memset(&gVars.tempRTx1, 0x0, sizeof(gVars.tempRTx1));
												}
											} while(*pnt_param != '\"');
											if (j > 0)
											{
												WriteEEPI2C(RxConf_EEAdr, (uint8_t *)(&gVars.tempRTx1), j);
												RxConf_EEAdr += j;
											}
											gVars.tempRTx1[0] = NULL;
											WriteEEPI2C(RxConf_EEAdr, (uint8_t *)(&gVars.tempRTx1), 1U);				// Chr chiusura buffer ricevuto
											BankSelect(0x00);
											response = AWS_CMD_OK;														// Rispondo ACK ad AWS
										}
										else
										{
											///* DEBUG
											//-- Il buffer supera lo spazio di EE ancora disponibile--
											//msg_info("\nBuffer EE overflow\n");
											len = 2;
										}
									}
									else
									{
										///* DEBUG
										//msg_info("\nNessun parametro valido\n");
										len = 2;
										//*/
									}
								}
								else
								{
									///* DEBUG
									//msg_info("\nComando senza membro \"parameters\"\n");
									len = 1;
									//*/
								}
							}
							else
							{
								///* DEBUG
								if (len == ERR_FIRST_PARAM_NOT_FOUND)
								{
									//msg_info("\nComando senza membro \"blocknumber\"\n");
									len = ERR_FIRST_PARAM_NOT_FOUND;
								}
								else if (len == ERR_SECND_PARAM_NOT_FOUND)
								{
									//msg_info("\nNessun numero di blocco\n");
									len = ERR_SECND_PARAM_NOT_FOUND;
								}
								else if (ActualBlockNum == 0)
								{
									//msg_info("\nErr: Blocco numero zero\n");
									len = ERR_WRONG_PARAM_VALUE;
								}
								//*/
							}
						}
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CMD_NEW_CONFIG);														// Predispongo msg di risposta ad AWS 
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer Status 
						break;

					case CMD_STATUS_REQUEST:
						// *********************************************************************
						// *******   C O M A N D O   4:     REQUEST  to  send  STATUS    *******
						// *********************************************************************
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Prepare_MachineStatus_Buffer();																			// Aggiorna lo Status Buffer
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer Status 
						break;

#if (CESAM == false)
					case CMD_STOP_ALARM:
						// *********************************************************************
						// *******   C O M A N D O   5:     DISATTIVA  SIRENA  ALLARME   *******
						// *********************************************************************
						SetDoorOpenDelay();																						// Disattiva sirena allarme e predispone ritardo allarme
						response = AWS_CMD_OK;
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CMD_STOP_ALARM);														// Predispongo msg di risposta ad AWS 
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer  
						break;
#endif
						
					case CMD_DIS_GETT:
						// *********************************************************************
						// *******   C O M A N D O    6:    INHBIT  Gettoniera          *********
						// *********************************************************************
						if ((IsErogazInProgress() == false) && (Is_Machine_StBy() == true) && (gVMCState.InProgrammaz == false) &&
							(gOrionConfVars.Remote_INH == false))
						{
							response = AWS_CMD_OK;
							GTW_command = CMD_DIS_GETT;																			// Command Code da passare alla CPU
						}
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CMD_DIS_GETT);														// Predispongo msg di risposta ad AWS 
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer  
						break;
		
					case CMD_ENA_GETT:
						// *********************************************************************
						// *******   C O M A N D O    7:    ENABLE  Gettoniera          *********
						// *********************************************************************
						if (gOrionConfVars.Remote_INH == true)
						{
							response = AWS_CMD_OK;
							GTW_command = CMD_ENA_GETT;																			// Command Code da passare alla CPU
						}
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CMD_ENA_GETT);														// Predispongo msg di risposta ad AWS 
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer  
						break;

					case CMD_CONFIG_REQ:
						// *********************************************************************
						// *******   C O M A N D O   8:     CONFIG    REQUEST          *********
						// *********************************************************************
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						if ((IsErogazInProgress() == false) && (gVMCState.InProgrammaz == false) &&
							(Is_Machine_StBy() == true))
						{
							if (Export_Config_to_AWS() == false)
							{
								NextBuffTypeToAWS = IoT_POLL;																	// La "Export_Config_to_AWS" ha inviato direttamente la configurazione ad AWS: non invio alcun buffer
							}
							else
							{
								Response_AWS_OK_Rejected(response, CMD_CONFIG_REQ);												// Predispongo msg di risposta "REJECTED" 
								NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;														// Quando esce invia il buffer Status 
							}
						}					
						else
						{
								Response_AWS_OK_Rejected(response, CMD_CONFIG_REQ);												// Predispongo msg di risposta "REJECTED" 
								NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;														// Quando esce invia il buffer Status 
						}
						break;	
					
			/*
					case CMD_EROGAZ_REQ:
						// *********************************************************************
						// *******   C O M A N D O   9:     EROGAZ    REQUEST          *********
						// *********************************************************************
						len = (EstraeParametroNumerico(params, &numeroblocco[0], &ActualBlockNum, MAX_NUM_BLOCKS_CHR));			// In len result funzione
						if (len == ERR_OK)
						{
							ActualBlockNum = bcd_to_hex(ActualBlockNum);
							if ((ActualBlockNum > 0) && (ActualBlockNum <= MAXNUMSEL))
							{
								if ((Is_Machine_StBy() == true) && (gVMCState.InProgrammaz == false) &&
								(IsErogazAvailable(ActualBlockNum) == true) && (gOrionConfVars.Remote_INH == false))
								{
									
									//--  Ricerca e controllo stringa in "parameters" --
									pnt_param = (strstr(params, parametri));
									if (pnt_param != NULL)
									{
										len = strlen(parametri);																// Lunghezza stringa "parameters"
										pnt_param += len;																		// "pnt_param" punta alla fine della stringa "parameters"
										pnt_param = Find_ParamValue(pnt_param, ASCII_NUMERIC, MAX_NUM_CHR);						// Cerca la prima cifra partendo dal pointer indicato
										if (pnt_param != NULL) 
										{
											j = 0;
											memset(&gVars.tempRTx1, 0x0, sizeof(gVars.tempRTx1));
											do
											{
												gVars.tempRTx1[j] = *(pnt_param++);
												if (is_ascii_num(gVars.tempRTx1[j]) != true)
												{
													gVars.tempRTx1[j] = 0;
													break;
												}
												if (j++ > CMD_9_MAX_PARAM_LEN)
												{
													gVars.tempRTx1[0] = 0;														// Parametro numerico in "params" inesistente
													break;
												}
												
											}while (true);
											len = strlen((char const*)&gVars.tempRTx1[0]);
											if (len == 0) break;																// Errato parametro numerico in "params"
											Temp = atouint32(&gVars.tempRTx1[0], len);											// In Temp il parametro dell'erogazione
											//if ((Temp == 0) || (Temp > MAX_FLOW_SEL)) break;									// Il numero di impulsi non puo' essere zero e maggiore di MAX_FLOW_SEL
											//if (IsSelParameterCorrect(ActualBlockNum, Temp) == false) break;					// Se parametro non nel range corretto esco
											
											if (IsSelParameterCorrect(ActualBlockNum, Temp) == true)							// Se parametro non nel range corretto esco
											{
												GetTransactionID(params);															// Copia in TransactionIDValue per ritrasmetterlo
												for (j=0; j < TRANSAC_ID_LEN; j++)
												{
													CopyOfTransID[j] = TransactionIDValue[j];										// In CopyOfTransID il valore da ristrasmettere nei messaggi alla dashboard
												}
												StartErogazioneDaRemoto(ActualBlockNum, (uint16_t)Temp);
												break;																				// Esco senza rispondere, la risposta sara' inviata al termine dell'erogazione
											}
											else
											{
												GetTransactionID(params);															// Copia in TransactionIDValue per ritrasmetterlo
												for (j=0; j < TRANSAC_ID_LEN; j++)
												{
													CopyOfTransID[j] = TransactionIDValue[j];										// In CopyOfTransID il valore da ristrasmettere nei messaggi alla dashboard
												}
											}
										}
									}
								}
							}
						}
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CMD_EROGAZ_REQ);														// Predispongo msg di risposta ad AWS 
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer Status 
						break;
			*/
						
						
					case CMD_CPU_RESET:
						// *********************************************************************
						// *******   C O M A N D O   9 0       CPU  RESET     ******************
						// *********************************************************************
						if ((IsErogazInProgress() == false) && (Is_Machine_StBy() == true) && (gVMCState.InProgrammaz == false))
						{
							response = AWS_CMD_OK;
							GTW_command = CMD_CPU_RESET;																		// Command Code da passare alla CPU
						}
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CMD_CPU_RESET);														// Predispongo msg di risposta ad AWS 
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer  
						break;

				
					case CMD_ACTIVE_OUT:
						// *********************************************************************
						// *******   C O M A N D O       9 9        ****************************
						// *********************************************************************
						
					/*
						if ((IsErogazInProgress() == false) && (Is_Machine_StBy() == true) && (gVMCState.InProgrammaz == false))
						{
							response = AWS_CMD_OK;
						}
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CMD_ACTIVE_OUT);														// Predispongo msg di risposta ad AWS 
						break;
					*/
					
					//-- Comando errato o inesistente --
					default:
						GetTransactionID(params);																				// Copia in TransactionIDValue per ritrasmetterlo
						Response_AWS_OK_Rejected(response, CommandNum);															// Predispongo msg di risposta ad AWS 
						NextBuffTypeToAWS = IoT_TX_BUFFER_TO_AWS;																// Quando esce invia il buffer  
						break;
				}
			}
		}
	}

//FineSubscribeCallback:
	pnt_param = (char *)params;
	for (j=0; j < RX_GTWY_BUF_SIZE; j++) 																						// Clr rxBuffer per evitare che un successivo buffer piu' corto sia elaborato erroneamente
	{	
		*(pnt_param + j) = 0;
	}
	return NextBuffTypeToAWS;
}

/*--------------------------------------------------------------------------------------*\
Method: Find_ParamValue
	Cerca il valore di un membro del JSON buffer: puo' cercare sia valori numerici che
	alfanumerici, saltando sempre i chr tipo doppi apici, slash, etc.
	
Parameters:
	IN	- buffer e pointer al parametro del quale si vuole estrarre il valore
		  e massimo numero di caratteri nei quali cercare
	OUT	- pointer alla prima cifra del param value oppure NULL
\*--------------------------------------------------------------------------------------*/
char* Find_ParamValue(char* paramPnt, SEARCH_TYPE ValueType, uint32_t MaxNumChar)
{
	uint32_t  j;

	switch(ValueType)
	{
		case ASCII_NUMERIC:
			for(j=0; j < MaxNumChar; j++)
			{
				if (is_ascii_num(*(paramPnt+j))) return (paramPnt+j);							// Trovato chr numerico
			}
			break;
		
		case ASCII_HEX:
			for(j=0; j < MaxNumChar; j++)
			{
				if (is_ascii_hex(*(paramPnt+j))) return (paramPnt+j);							// Trovato chr Hex
			}
			break;
		
		case ASCII_LITERAL_HEX:
			for(j=0; j < MaxNumChar; j++)
			{
				if (is_ascii_literal(*(paramPnt+j))) return (paramPnt+j);						// Trovato chr letterale (non numero)
			}
			break;

		case ASCII_ALFANUMERIC:
			for(j=0; j < MaxNumChar; j++)
			{
				if (is_ascii_num_literal(*(paramPnt+j))) return (paramPnt+j);					// Trovato chr alfanumerico
			}
			break;
			
		case ASCII_LITERAL_A_z:
			for(j=0; j < MaxNumChar; j++)
			{
				if (is_ascii_literal_A_z(*(paramPnt+j))) return (paramPnt+j);					// Trovato chr alfanumerico
			}
			break;
	}
	return NULL;
}


/*---------------------------------------------------------------------------------------------*\
Method: GetTransactionID
	Ricerca la stringa "transactionid" nel buffer ricevuto da AWS.
	Termina la ricerca quando trova il chr " oppure ha raggiunto il massimo numero di chr
	corrispondenti al size di TransactionIDValue.

	IN:	  - buffer ricevuto da AWS
	OUT:  - valore del transactionid nel buffer "TransactionIDValue"
\*----------------------------------------------------------------------------------------------*/
static void  GetTransactionID(char *params)
{
uint8_t	len, j;
	
	memset(&TransactionIDValue[0], 0, sizeof(TransactionIDValue));								// Clear buffer destinazione TransactionIDValue
	TransactionIDValue[0] = ASCII_ZERO;
	pnt_transID = (strstr((char *) params, Stringtransactionid));
	if (pnt_transID != NULL)
	{
		len = strlen(Stringtransactionid);
		pnt_transID += len;
		pnt_transID = Find_ParamValue(pnt_transID, ASCII_ALFANUMERIC, 5);						// Cerca il primo valore alfanumerico del parametro partendo dal pointer indicato
		if (pnt_transID != NULL) 
		{
			for (j=0; j < (TRANSAC_ID_LEN-1); j++)
			{
				TransactionIDValue[j] = *(pnt_transID + j);
				if (TransactionIDValue[j] == '"')												// Trovato chr " di chiusura stringa
				{
					TransactionIDValue[j] = NULL;												// Fine stringa prezzo transactionID
					break;
				}
				else if (j >= TRANSAC_ID_LEN)
				{
					TransactionIDValue[TRANSAC_ID_LEN-1] = NULL;								// Raggiunto limite size di TransactionIDValue
					break;
				}
			}
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: EstraeParametroNumerico
	Ricerca un parametro in un buffer JSON ed estrae il suo valore numerico 

	IN:	  - pointer al sourcebuffer, alla stringa da cercare, al valore di ritorno e numero
			massimo di caratteri da ricercare come valore numerico nel buffer sorgente
	OUT:  - ERR_OK se valore trovato ed estratto, altrimenti codice di errore
\*----------------------------------------------------------------------------------------------*/
static uint8_t  EstraeParametroNumerico(char* sourcebuffer, char* stringToFind, uint8_t* resvalue, uint8_t SearchCharNum)
{
#pragma diag_suppress=Pe550														//Warning[Pe550]: variable "result" was set but never used
#pragma diag_suppress=Pa079														//Warning[Pa079]: undefined behavior: variable "result" (declared at line 565) is modified more than once without an intervening sequence point in this statement C:\Work\Ecoline\Src\AWS_subscribe_publish.c 578 


uint8_t len, result, result_h;
char*	localpnt;

	localpnt = (strstr(sourcebuffer, stringToFind));
	if (localpnt == NULL) return result = ERR_FIRST_PARAM_NOT_FOUND;
	len = strlen(stringToFind);																		// Lunghezza stringa da cercare
	localpnt += len;																				// "localpnt" punta alla fine della stringa trovata
	localpnt = Find_ParamValue(localpnt, ASCII_NUMERIC, SearchCharNum);								// Cerca il primo valore numerico del parametro partendo dal pointer indicato
	if (localpnt == NULL) return result = ERR_SECND_PARAM_NOT_FOUND;
	
	if (is_ascii_num(*(localpnt + 1)) == false)
	{
		result = (uint8_t) *(localpnt);
		result = (uint8_t) ascii_to_hex_num(result);												// Valore ad una cifra (1-9)
	}
	else
	{
		result = (uint8_t) *(localpnt);
		result_h = (uint8_t) *(localpnt+1);
		result = (uint8_t) ((ascii_to_hex_num(result) << 4) | (ascii_to_hex_num(result_h) << 0));	// Valore a due cifre (01-99)
	}
	*resvalue = result;
	return result = ERR_OK;

#pragma diag_default=Pa079
#pragma diag_default=Pe550
}								

/*---------------------------------------------------------------------------------------------*\
Method: Init_EEAddr_Config_Block_Save
	Predispone l'address EE per salvare i blocchi di configurazione ricevuti da AWS.
	La funzione Rd_EEBuffStartAddr_And_BankSelect e' sempre chiamata per predisporre il Bank
	della EE.
	RxConf_Start_Address contiene l'address di inizio del buffer in EE e serve per evitare di
	superare il limite di memoria disponibile.

	IN:	  - true per inizializzare l'RxConf_EEAdr 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Init_EEAddr_Config_Block_Save(uint8_t NumBlock)
{
	Rd_EEBuffStartAddr_And_BankSelect(BUF_RXCONFIG);												// Determino address inizio Buffer EEProm e set Bank EE dove memorizzare il file config ricevuto
	RxConf_Start_Address = 0;				
	RxConf_Start_Address += MemoryBytes[1];															// LSB
	RxConf_Start_Address += (MemoryBytes[0]<<8);													// MSB
	if (NumBlock == 1)
	{
		RxConf_EEAdr = RxConf_Start_Address;				
	}
}

#endif	// IOT_PRESENCE
	
