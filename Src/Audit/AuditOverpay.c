/****************************************************************************************
File:
    AuditOverpay.c

Description:
    Funzioni Audit Overpay
    

History:
    Date       Aut  Note
    Set 2012	MR   

*****************************************************************************************/


#include "ORION.H"
#include "AuditOverpay.h"
#include "AuditExt.h"
#include "IICEEP.h"
#include "Funzioni.h"
#include "OrionCredit.h"
#include "Totalizzazioni.h"
#include "Power.h"

#include "Dummy.h"

/*------------------------------------------------------------------------------------------*\
 Method: ResOverpay
	Funzione di azzeramento stato Overpay.
	
Parameters:
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void ResOverpay(void) {
  
	PSHProcessingData();
	TipoOverpay = NO_OVERPAY;
	StatoOverpay = 0;
	PSHCheckPowerDown();
}

/*------------------------------------------------------------------------------------------*\
 Method: OverpayCaricamentoBlocchi
	Audit ID aggiornati quando il blocco scalato da una carta di caricamento a blocchi e
	salvato in "RefundCashCB" dopo il mancato uso, non e' piu' attribuibile alla stessa 
	carta perche' questa e' stata riprogrammata con una valore del blocco diverso da quello
	che aveva quando era stato scalato.
	Es. Carta CB con blocco=5 Euro, il blocco non e' utilizzato e va in "RefundCashCB", la
	carta non e' piu' usata e viene successivamente riconfigurata con blocco=10 Euro.
	Al suo primo reinserimento il sistema trova il suo UID in "RefundCashCB" ma, essendo il
	valore del blocco cambiato, il credito in "RefundCashCB" viene azzerato e registrato qui.
	Lo scopo e' quello di poter tracciare soldi pagati per la carta CB, mai trasferiti su 
	carte utente, e non piu' presenti sulla carta CB.
	
Parameters:
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void OverpayCaricamentoBlocchi(void) {
	
	uint16_t blocchi;

	blocchi = 1;																				// Nell'Orion scalo solo un blocco alla volta dalla carta di caricamento
	switch(StatoOverpay) {

    	case 1 :
    		AuditUlongAdd(IICEEPAuditAddr(LREv8Ovp), CashCaricamentoBlocchi);					// "EA2*EK_09" "Overpay Caricamento a Blocchi"
    		StatoOverpay++;
    		PSHCheckPowerDown();

    	case 2 :
    		AuditUintAdd(IICEEPAuditAddr(LREvent8), blocchi);									// "EA2*EK_04"  "Caricamento a Blocchi" EA202
    		StatoOverpay++;
    		PSHCheckPowerDown();

    	case 3 :
    		AuditUlongAdd(IICEEPAuditAddr(INEvent8), blocchi);                 					// "EA2*EK_04"  "Caricamento a Blocchi" EA203
    		StatoOverpay++;
    		PSHCheckPowerDown();

    	case 4 :
    		AuditUlongAdd(IICEEPAuditAddr(LREv8Val), CashCaricamentoBlocchi);					// "EA2*EK_04"  "Caricamento a Blocchi" EA204
    		StatoOverpay++;

    	default:
    		PSHProcessingData();
    		CashCaricamentoBlocchi = NULL_VALUE;
    		ValoreBloccoCaricamento = NULL_VALUE;
    		ResOverpay();
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: OverpayCash
	Funzione di registrazione in Overpay Cash e successivo azzeramento del valore di cash
	da monete e da banconote.
	
Parameters:
   IN:  - StatoOverpay, CashDaSelettore, ValidateCashDaSelettore
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void OverpayCash(void) {

	uint  valore;

	valore = GetValidatedCash();
	switch(StatoOverpay) {

    case 1 :
    	AuditUlongAdd(IICEEPAuditAddr(LROverpay), valore);										// CA801
    	StatoOverpay++;
    	PSHCheckPowerDown();

    case 2 :
    	AuditUlongAdd(IICEEPAuditAddr(INOverpay), valore);										// CA802
    	StatoOverpay++;
    	PSHCheckPowerDown();

#if	kApplTokenAudit
	case 3:
		if (CGMngrHookTokenInserted() && !CGMngrHookCoinTokenMix()) {
			AuditUlongAdd(IICEEPAuditAddr(INTokenOverPay), valore);								// TA501
			AuditUlongAdd(IICEEPAuditAddr(LRTokenOverPay), valore);								// TA502
			StatoOverpay++;
			PSHCheckPowerDown();
		}
#endif
    default:
    	AzzeraCash(NocheckPWD);
    	ResOverpay();
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: OverpayPayout
	Funzione di registrazione in Overpay Cash del valore di cash non reso causato da una
	interruzione della restituzione.
	
Parameters:
   IN:  - CashDaRendere
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void OverpayPayout(void) {

	switch(StatoOverpay) {

    case 1 :
    	AuditUlongAdd(IICEEPAuditAddr(LROverpay), CashDaRendere);								// CA801
    	StatoOverpay++;
    	PSHCheckPowerDown();

    case 2 :
    	AuditUlongAdd(IICEEPAuditAddr(INOverpay), CashDaRendere);								// CA802
    	StatoOverpay++;
    	PSHCheckPowerDown();

    case 3 :
		AuditUintInc(IICEEPAuditAddr(LR_EA_01_Event));											// EA_01	"EA2*EA_01" "Payout Interrotto"
		StatoOverpay++;
		PSHCheckPowerDown();

	case 4 :
		AuditUlongInc(IICEEPAuditAddr(IN_EA_01_Event));											// EA_01	"EA2*EA_01" "Payout Interrotto"
		StatoOverpay++;
		PSHCheckPowerDown();

	case 5 :
		AuditUlongAdd(IICEEPAuditAddr(LR_EA_01_Val), CashDaRendere);							// EA_01	"EA2*EA_01" "Payout Interrotto"
		StatoOverpay++;
		PSHCheckPowerDown();

    default:
    	CashDaRendere = 0;
    	ResOverpay();
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: OverpayCarta
	Funzione di registrazione in Overpay Carta del credito azzerato dalla tabella Refund.
	Aggiorno anche il valore addebitato alle carte perche', essendo un credito derivante
	da vendita fallita, non era mai stato conteggiato nell'Audit.
	
Parameters:
   IN:  - ValoreReinstate
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void OverpayCarta(void) {

	switch(StatoOverpay) {
	
	case 1 :
		AuditUlongAdd(IICEEPAuditAddr(LRCardDeb), ValoreReinstate);                      		// DA302 Value Debited from card LR
    	StatoOverpay++;
		PSHCheckPowerDown();

	case 2 :
		AuditUlongAdd(IICEEPAuditAddr(INCardDeb), ValoreReinstate);                      		// DA301 Value Debited from card IN
    	StatoOverpay++;
		PSHCheckPowerDown();

    case 3 :
    	AuditUlongAdd(IICEEPAuditAddr(LROverpayCard), ValoreReinstate);							// DA901 Overpay Cashless LR
    	StatoOverpay++;
    	PSHCheckPowerDown();

    case 4 :
    	AuditUlongAdd(IICEEPAuditAddr(INOverpayCard), ValoreReinstate);							// DA902 Overpay Cashless IN
    	StatoOverpay++;
    	PSHCheckPowerDown();

    default:
    	ValoreReinstate = 0;
    	ResOverpay();
	}
}
/*---------------------------------------------------------------------------------------------*\
Method: GestioneAuditOverpay
	Gestione Audit Overpay

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void GestioneAuditOverpay(void) {

  switch (TipoOverpay) {

    case OVERPAY_CASH :
      OverpayCash();
    break;

    case OVERPAY_CARICAMENTO_BLOCCHI :
      OverpayCaricamentoBlocchi();
    break;

    case OVERPAY_CASH_PAYOUT :
    	OverpayPayout();
    break;
    
    case OVERPAY_KR:
    	OverpayCarta();
    break;

    default:
      ResOverpay();
  }
}


/*---------------------------------------------------------------------------------------------*\
Method: Overpay
	Attiva la registrazione Audit Overpay

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void Overpay(uint8_t tipo) {

	PSHProcessingData();
	TipoOverpay = tipo;
	StatoOverpay = 1;
	PSHCheckPowerDown();
}
