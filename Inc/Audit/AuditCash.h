/*
 * AuditCash.h
 *
 *  Created on: Oct 1, 2013
 *      Author: Abe
 */

#ifndef AUDITCASH_H_
#define AUDITCASH_H_

#define EXP_AUDIT 0x80


void BillAuditStart(ET_BILLPOLLDEST billDest, uint8_t billType, CreditCoinValue billValue);
void CoinAuditStart(ET_CHGPOLLCOINDEST coinDest, uint8_t coinType, CreditCoinValue coinValue);
void CoinFillManualAuditStart(uint32_t fill_amount);
void CoinPayoutAuditStart (CreditCoinValue ChangeAmount, CMDrvPAYPOS route);
void CoinPayoutExpAuditStart(CreditCoinValue ChangeAmount, CMDrvPAYPOS route);

typedef enum {
	NO_AUD_BILL = 0,
	IN_AUD_BILL,
	LR_AUD_BILL,
	TOT_NUM_BILL,
	END_AUD_BILL
} AUD_BILL_PHASE;

typedef enum {
	NO_AUD_COIN = 0,
	IN_AUD_COIN,
	LR_AUD_COIN,
	TOT_NUM_COIN,
	ROUTE_NUM_COIN,
	INC_NUM_TUBE,
	END_AUD_COIN
} AUD_COIN_PHASE;

typedef enum {
	NO_MANFILL_COIN = 0,
	IN_MANFILL_COIN,
	LR_MANFILL_COIN,
	INC_TUBESTATUS_NUM,
	END_MANFILL_COIN
} MANFILL_COIN_PHASE;

typedef enum {
	NO_CHANGE_COIN = 0,
	IN_CH_PAY_COIN,
	LR_CH_PAY_COIN,
	DEC_TUBESTATUS_NUM,
	END_CHANGE_COIN
} CHANGE_COIN_PHASE;



#endif /* AUDITCASH_H_ */
