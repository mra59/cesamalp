/****************************************************************************************
File:
    VMC_CPU_Motori.h

Description:
    Include file con definizioni e strutture per l'attivazione degli attuatori selezione

History:
    Date       Aut  Note
    Lug 2020	MR   

 *****************************************************************************************/

#ifndef __VMC_CPU_Motori_H
#define __VMC_CPU_Motori_H


// ----  Defines  VMC  ----

#define ADC_FAIL				0xffffffff
#define ADC_FAIL_Cnt			10						// Errori ADC prima di dichiararlo guasto
#define	NUM_LETTURE_CORRENTE	200						// Integratore letture corrente
#define	STOP_I_NUMREAD			10						// Numero di volte che la corrente (integrata con NumLettureCorrente) e' al di sotto della soglia di Stop

#define DA_VUOTO				true
#define DA_PIENO				false

#define	DOOR_STATUS_CLOSED		0
#define	DOOR_STATUS_OPEN		1
#define	DOOR_ALARM_DISABLED		false
#define	DOOR_ALARM_ENABLED		true

// =====================================================
// =====   Defines  Correnti  Elettrovalvole   =========
// =====================================================

// -- Determinazione soglia di tensione ADC corrispondente alla I di Corto Circuito desiderata --

#define	V_ICC			90000						// 0,09V = Tensione ADC oltre la quale si considera I Corto Circuito (*1,000,000)

#define	VREF_ADC		2950000						// 3,0V nominali = Vref del Convertitore ADC (*1,000,000)
#define VOLT_ONE_BIT	(VREF_ADC / 4095)			// Tensione x 1000 rappresentata da 1 bit (conversione a 12 bit)
#define V_ICC_12BIT		(V_ICC / VOLT_ONE_BIT)		// Valore di CC espresso a 12 bit 

#define	RAPPORTO		((V_ICC_12BIT*1000) / 4096)	// Rapporto tra bit di ICC e valore max con 12 bit (4096)
#define	ICC_LIM_H		((RAPPORTO*256) / 1000)		// Valore a 8 bit equivalente a V_ICC_12BIT su 12 bit
#define	AWD2_ICC_LIM_H	(ICC_LIM_H << 4)			// Il valore da impostare nel registro AWD2 e' a 12 bit allineato a sx

// ============================================
// =====   Defines  Tempi  Vari       =========
// ============================================

#define	TOUT_EROGAZ					TwoMin				// Timeout erogazione
#define	TIME_CHECK_EV				Milli100			// Tempo attivazione EV per controllarne esistenza e funzionalita' *** ATTENZIONE: NON SUPERARE IL TEMPO DI "INTEGR_CORTO_CIRC" *****
#define	TIME_LED_VANO				EIGHTSEC			// Temporizzazione accensione led vano prelievo prodotti
#define	TIME_TEST_COOLER_TEMPER		OneMin				// Temporizzazione test temperatura ambiente per attivare le ventole
//#define	TIME_TEST_COOLER_TEMPER		FIVESEC				// Temporizzazione test temperatura ambiente per attivare le ventole


// ----  Strutture  Cicli  Attuatori  ------
typedef struct {
	Tmr32Local    WaitStop;
	Tmr32Local    ResetMotori;
	Tmr32Local    TestCoolerPeriod;
} TimeOutMot;


typedef struct {
	bool				InProgress;						// 1=in erogazione - 0=st-by
	bool				DelayBeforeFilterClean;			// 1=dopo erogazione, in attesa partenza pulizia filtro
	bool				FilterCleaning;					// 1=pulizia filtro in corso
	uint16_t			FlowCnt;						// Counter contatore volumetrico
	Tmr32Local    		ToutErogaz;						// Timer erogazione
} Erogaz;


/* Private defines -----------------------------------------------------------*/

// ==========================================
// ==  Definizione Inputs  MicroSwitch  =====
// ==========================================

/*  Definiti in "VMC_CPU_HW.h" */

// ==============================================
// ==  Definizioni  Uscite                  =====
// ==============================================
// --  Byte "Out1_8" inviato all'HCT595 --
#define	OUT1				0x01
#define	OUT2				0x02
#define	OUT3				0x04
#define	OUT4				0x08
#define	OUT5				0x10
#define	OUT6				0x20
#define	OUT7				0x40
#define	OUT8				0x80

// --  Byte "Out9_16" inviato all'HCT595 --
#define	OUT9				0x01
#define	OUT10				0x02
#define	OUT11				0x04
#define	OUT12				0x08
#define	OUT13				0x10
#define	OUT14				0x20
#define	OUT15				0x40
#define	OUT16				0x80


// -------  Defines per EV Erogatori Acqua  ----------

#define	ALL_OUTS_OFF	{					\
							Out1_8 = 0x00;	\
							Out9_16 = 0x00;	\
						}

#define	EV1_ON					Out1_8 |= OUT1
#define	EV1_OFF					Out1_8 &= (~OUT1)
#define	EV2_ON					Out1_8 |= OUT2
#define	EV2_OFF					Out1_8 &= (~OUT2)
#define	EV3_ON					Out1_8 |= OUT3
#define	EV3_OFF					Out1_8 &= (~OUT3)
#define	EV4_ON					Out1_8 |= OUT4
#define	EV4_OFF					Out1_8 &= (~OUT4)
	
#define	LUCI_ON					Out1_8 |= OUT6
#define	LUCI_OFF				Out1_8 &= (~OUT6)
	
#define	Is_FINE_PRODOTTO_1		(((Micro1_8 & FINE_PRODOTTO_1) == false) ? false : true)

/*
#define	OUT_FINE_PRODOTTO_ON	Out1_8 |= OUT8
#define	OUT_FINE_PRODOTTO_OFF	Out1_8 &= (~OUT8)
#define	Is_FINE_PRODOTTO_ON		(((Out1_8 & OUT8) == false) ? false : true)
*/

#if (CESAM == false)
	#define	OZONO_ON				Out1_8 |= OUT7
	#define	OZONO_OFF				Out1_8 &= (~OUT7)
	#define	Is_OZONO_ON				(((Out1_8 & OUT7) == false) ? false : true)
	#define	SIRENA_ON				Out1_8 |= OUT8
	#define	SIRENA_OFF				Out1_8 &= (~OUT8)
	#define	Is_SIRENA_ON			(((Out1_8 & OUT8) == false) ? false : true)
	#define	COOLER_ON				Out1_8 |= OUT5
	#define	COOLER_OFF				Out1_8 &= (~OUT5)
#endif

#define	ALL_EV_OFF			0x00

// -------  Defines Cesam    ----------
#define	FLUSS1					1
#define	BIT_CC_Out_1			0x01
#define	BIT_CC_Out_2			0x02
#define	BIT_CC_Out_3			0x04
#define	BIT_CC_Out_4			0x08
	
#define	FLUSS_STEP				0x09
#define	MULTIPLO				10
	
#define	IMPULSI_RESIDUI				250
#define	TIME_RESIDUO				FIVESEC
#define	TIME_FILTER_CLEAN			ThreeSec
#define	DELAY_BEFORE_FILTER_START	FIVESEC
#define	WAIT_AFTER_OUT_OFF			Milli200

/*--------------------------------------------------------------------------------------*\
    Functions  Prototypes 
\*--------------------------------------------------------------------------------------*/
void  	Check_Timer_Vari_e_Allarmi(void);
void  	Luci(bool Switch);
void  	Motori_OFF(void);
void  	AttesaStopMotore(uint32_t TimeAttesa);
void  	ResetGuasti(void);
void  	TestPresenzaAttuatori(void);
bool  	Ciclo_Selezione(uint8_t NumeroSelez);
bool  	Is_DA_Vuoto(void);
bool  	Is_VMC_Fail(void);
bool  	Start_Erogazione(uint8_t NumeroSelez, bool Percentage);
void  	Add_Time_Pulses(uint8_t NumeroSelez);
void  	Ciclo_Erogazione(void);
void  	UpdateStatusErogazioni(void);
void  	ON_OFF_ELV(uint8_t EV_Num, bool Switch);
bool  	IsErogazComplete(uint8_t ErogazNum);
bool  	IsErogazInProgress(void);
bool  	IsFilerCleaningInProgress(void);
bool  	IsErogazAvailable(uint8_t NumErogaz);
bool  	IsProdottoEsaurito(uint8_t SelNum);
uint8_t ReadErogazStatus(uint8_t NumErogaz);
void  	Check_Timer_Ozono(void);
static void  ClrErogazParameters(uint8_t FlussNum);
void  	UpdateVMCStatusIoT(void);
void  	Check_Allarme_Porta(void);
void  	DoorOpenInitState(void);
void  	GestioneErogazioneDaRemoto(void);
void  	Force_Tx_Status(void);
bool  	ErogazTimeRemaining(uint32_t *TimeResiduo, uint8_t NumErogaz);
uint8_t GetSelectionInProgress(void);
void  	DisableRemainingOutputs(uint8_t ErogazInProgress);;
void  	SetOutputs(uint8_t SelezNum, bool Switch);
bool    IsProd_Esaurito(uint8_t NumProdotto);


#if (CESAM == false)
	void	SetDoorOpenDelay(void);


#endif

#if DEB_NO_VMC
	void  SimulSelez(uint32_t TimeAttesa);
#endif


#endif		// __VMC_CPU_Motori_H


