/****************************************************************************************
File:
    VMC_CPU_FlashMsg.c

Description:
    File messaggi in Flash per la programmazione del VMC.

History:
    Date       Aut  Note
    Mag 2019	MR   

 *****************************************************************************************/

//#include "ORION.H"

/*--------------------------------------------------------------------------------------*\
Constants definition
\*--------------------------------------------------------------------------------------*/
//                |---  Riga  3 -----|----  Riga  4 -----|
//                1234567890123456789012345678901234567890
//                1111111111111111111133333333333333333333
const char Lang0_ProgMsgLCD[230][20] = {
#if LCDmsg_Lingua
				{"   A L P   S R L    "},									// mInitMsgLCD			1
				{" CPU  Revision xx.xx"},
				{" Boot Revision xx.xx"},
				{"                    "},

				{"Macchina in servizio"},									// mStByNoZucNoResto	2
				{"                    "},
				{"Non da resto        "},
				{"Credito Euro       0"},

				{"Macchina in servizio"},									// mStByNoZucDaResto	3
				{"                    "},
				{"                    "},
				{"Credito Euro       0"},
		
				{"Macchina in servizio"},									// mStByZucNoResto		4
				{"                    "},
				{"Non da resto        "},
				{"Credito Euro       0"},
		
				{"Macchina in servizio"},									// mStByZucDaResto		5
				{"                    "},
				{"                    "},
				{"Credito Euro       0"},

				{"    Attendere       "},									// mLCDWait				6
				{"     capsula        "},
				{"    erogazione      "},
				{"                    "},
				
				{"    Erogazione      "},									// mLCDGetBev			7
				{"    terminata       "},
				{"     capsula        "},
				{"                    "},
				
				{"                    "},									// mCostoSelez			8
				{" SELEZIONE    N.  X "},
				{"                    "},
				{"Prezzo  Euro        "},
				
				{"     MACCHINA       "},									// mNoVMC				9
				{"     SERVIZIO       "},
				{"      FUORI         "},
				{"                    "},
				
				{"                    "},									// mProdEsaur			10
				{"      Capsula       "},
				{"                    "},
				{"  Non  Disponibile  "},
				
				{"   A L P   S R L    "},									// mNewConfig			11
				{"       Fine         "},
				{"     Attendere      "},
				{"   Programmazione   "},
				
				{"   A L P   S R L    "},									// mWaitReset			12
				{"     Reset          "},
				{"     Attendere      "},
				{"     Macchina       "},
				
				{"     ATTENDERE      "},									// mRiscaldam			13
				{"        in          "},
				{"     Macchina       "},
				{"   Riscaldamento    "},
				
				{"     Attendere      "},									// mLavGrup				14
				{"    in   corso      "},
				{" Lavaggio   Gruppo  "},
				{"                    "},

				{"     Attendere      "},									// mLavMixer			15
				{"    in   corso      "},
				{" Lavaggio   Mixer   "},
				{"                    "},
				
				{"   A L P   S R L    "},									// mWaitInit			16
				{"      Attendere     "},
				{"                    "},
				{"  Inizializzazione  "},
				
				{"   A L P   S R L    "},									// mProgrammaz			17
				{"   Macchina    in   "},
				{"                    "},
				{"   Programmazione   "},

				{"     Erogazione     "},									// mLCDVendFail			18
				{"                    "},
				{"      Prodotto      "},
				{"      Fallita       "},

				{"     Distributore   "},									// mDAVuoto				19
				{"                    "},
				{"       Vuoto        "},
				{"                    "},
				
				{"IN_CONF :     ----  "},									// mUSB_KEY				20
				{"  AUDIT :     ----  "},
				{"OUT_CON :     ----  "},
				{"   LOG  :     ----  "},

				{"  Tipo Chiave  USB  "},									// mUSB_KEY_Withdraw	21
				{"  Estrarre Chiave   "},
				{"   non  rilevabile  "},
				{"                    "},
				
				{"Erogaz 1            "},									// mStBy_Msg			22
				{"Erogaz 3            "},
				{"Erogaz 2            "},
				{"Erogaz 4            "},

#endif	// End LCDmsg_Lingua

				{"INSERIRE            "},									// mInserire
				{"PASSWORD:       ****"},									// mPassword
				{"Nuova Password |||||"},									// mNewPassword
				{"     Azzerare       "},									// mAzzerare
				{"   Memoria  dati?  |"},									// mMemoriaDati
				{"        Menu        "},									// mMenu
				{" Parametri Selez    "},									// mTempiDosi
				{"Prezzo Selezioni    "},									// mPrezzoSelez
				{"Sconto Selezioni    "},									// mScontoSelez
				{"Temperature Macchina"},									// mTemperMacchina
				{"       Audit        "},									// mMenuAudit
				{"Sistemi di Pagamento"},									// mSistemiPagamento
				{"Selezione->Prezzo   "},									// mSelezPrezzo
				{"  Opzioni   Varie   "},									// mOpzioniVarie
				{"Impostazioni ALP    "},									// mImpostazioniProduttore
				{"    Collaudo        "},									// mCollaudo
				{"    Lavaggi         "},									// mLavaggi
				{"  Fasce   Orarie    "},									// mRisparmio
				{"Parametri  Fabbrica "},									// menuParamFabbrica
				{"Selezione N.      ||"},									// mSelezioneNumero
				{"Selezione N.       |"},									// mSelezioneUnaCifra
				{"Modificare ?        "},									// mReq_DaProgrammare
				{"Selez. Disattivata |"},									// mSel_Disable    
				{" Impulsi ?         |"},									// mSel_Impulsi    
				{"    Prezzo    ||||||"},									// mSel_Prezzo    
				{"Prezzo Scont. ||||||"},									// mSel_Sconto    
				{"Prezzo Numero    |||"},									// mTab_SelPrez
				{"123456789 10 11 12  "},									// mSel_ColCaps   
				{"Executive ?        |"},									// mPay_Executive    
				{"Price Holding ?    |"},									// mPay_ExecPrHold   
				{"Price Display ?    |"},									// mPay_ExecPrDisplay
				{"Resto Subito ?     |"},									// mPay_ExecMDBRestoSubito
				{"Mifare+Selettore?  |"},									// mPay_Mifare   
				{"Mifare PIN 1   *****"},									// mPay_MifarePin1
				{"Mifare PIN 2   *****"},									// mPay_MifarePin2  
				{"New Mifare PIN |||||"},									// mPay_NewMifarePin
				{"Mifare Gestore |||||"},									// mPay_MifareGest  
				{"Cod Macch.  ||||||||"},									// mPay_CodiceMacchina  
				{"Cod Macch ||||||||||"},									// mPay_CodiceMacchina10Cifre  
				{"Codice  Macchina    "},									// msg_CodiceMacchina  
				{"Cod Locazione  |||||"},									// mPay_CodiceLocazione 
				{"Codice  Locazione   "},									// msg_CodiceLocazione 
				{"Abilito Sconti?    |"},									// mPay_EnaDiscount  
				{"Moneta N.         ||"},									// mPay_CoinNum
				{"Valore        ||||||"},									// mPay_CoinVal
				{"Max Cred Card ||||||"},									// mPay_MaxCard
				{"Max Cred Cash ||||||"},									// mPay_MaxCash
				{"Gratuito ?         |"},									// mPay_Gratis   
				{"MDB ?              |"},									// mPay_MDB
				{"MDB Slave ?        |"},									// mPay_MDBSLV
				{"MultiVend ?        |"},									// mPay_MDBMultiVend
				{"Resto Massimo ||||||"},									// mPay_MDBMaxChange
				{"Cambiamonete?      |"},									// mPay_MDBCambiaMonete
				{"Bank Sempre Abil.? |"},									// mPay_MDBBillEnaWithCard
				{"Abilitata?         |"},									// mPay_MDBCoinBillEna
				{"Banconota N.      ||"},									// mPay_MDBBillNum
				{"Monete in Tubo   |||"},									// mPay_MDBCoinsInTube
				{"Decimal Point    |||"},									// mOpt_DPP  
				{"Distribut. Tipo  |||"},									// mOpt_DaType
				{"Tastiera   Tipo  |||"},									// mOpt_KeyBoardType
				{"Deconto ?          |"},									// mOpt_EnaDeconto
				{"Deconto attuale     "},									// msg_DecontoAttuale
				{"  Deconto      |||||"},									// mOpt_DecontoMassimo
				{"Riserv deconto |||||"},									// mOpt_DecontoRiserva
				{"Ricarica Deconto?  |"},									// mCMD_ResetDeconto
				{"Service             "},									// msg_RiservaDeconto
				{"Decalcificatore?   |"},									// mOpt_EnaDecalcific
				{"Val decalcif.  |||||"},									// mOpt_DecalcifMassimo
				{"Azzera Memoria?    |"},									// mOpt_ClearEE  
				{"Azzera Audit?      |"},									// mOpt_ClearAuditEE  
				{"Sei Sicuro ?       |"},									// mOpt_Sicuro  
				{"Comando Numero   |||"},									// mOpt_CmdCollaudo
				{"Attesa Sel (sec) |||"},									// mOpt_CmdAttesaTraSelezioni
				{" Collaudo Terminato "},									// mOpt_CollaudoTerminato
				{"Totale Battute LR   "},									// mAud_TotBattuteLR
				{"Totale Battute IN   "},									// mAud_TotBattuteIN
				{"Totale Battute Test "},									// mAud_TotBattuteTest
				{"Totale Venduto LR   "},									// mAud_TotVendutoLR  
				{"Totale Venduto IN   "},									// mAud_TotVendutoIN  
				{"Imposta Orologio?  |"},									// mOrologio
				{"  Data  e  Ora      "},									// mDataOra
				{"  G  M  A g   H  M  "},									// mClockTipoDato
				{"Fasce orarie Luci? |"},									// mOpt_EnaONLuci
				{"Giorno   (1 = Lun) |"},									// mDayOfTheWeek
				{"Fascia          N. |"},									// mFasciaNum
				{"Imposta Fasce ?    |"},									// mImpostaFasce
				{"Param. Fabbrica?   |"},									// mSetDefaultFabbrica  
//				{"Lingua Numero    |||"},									// mLanguageNum
				{"Seconda Lingua?    |"},									// mLanguageNum
				//{"Exec Master/Slave? |"},									// mPay_ExecMstSlv  
				{"Start Automatico ? |"},									// mOpt_AutomaticStart  
				{"LCD 16x2 ?         |"},									// mLCD_16x2 
				{"  Impulsi   Motore  "},									// mOpzNumRotaz
//				{"Ozono Presente?    |"},									// mOpz_Ozono
				{"Prod1 Aperto=Fine? |"},									// mOpt_SwitchFineProdotto_N_1
				{"Tempo Secondi  |||||"},									// mVal_TimeSel
				{"Numero Impulsi |||||"},									// mVal_CounterSel
				{"Senza Pulsanti ?   |"},									// mOpt_SenzaPulsanti
				{"Sel-1 I  uscita  |||"},									// mVal_Sel_1_Out1
				{"Sel-1 II uscita  |||"},									// mVal_Sel_1_Out2
				{"Sel-2 I  uscita  |||"},									// mVal_Sel_2_Out1
				{"Sel-2 II uscita  |||"},									// mVal_Sel_2_Out2
				{"Sel-3 I  uscita  |||"},									// mVal_Sel_3_Out1
				{"Sel-3 II uscita  |||"},									// mVal_Sel_3_Out2
				{"Sel-4 I  uscita  |||"},									// mVal_Sel_4_Out1
				{"Sel-4 II uscita  |||"},									// mVal_Sel_4_Out2
				{"CleanFilter out  |||"},									// mVal_CleanFilterOutNum
				{"Erogaz 1 Fluss n |||"},									// mVal_FlussimErogaz_1
				{"Erogaz 2 Fluss n |||"},									// mVal_FlussimErogaz_2
				{"Erogaz 3 Fluss n |||"},									// mVal_FlussimErogaz_3
				{"Erogaz 4 Fluss n |||"},									// mVal_FlussimErogaz_4
				{"Temper Ventola ON ||"},									// mVal_TemperVentolaON  
				{"CPU s/n   ||||||||||"},									// mPay_CPU_SerialNumber  
				{"Remote Inhibit?    |"},									// mLCD_RemoteInhibit 
				{"Erogaz. Ozono  |||||"},									// mVal_ErogazOzono
				{"Attesa  Ozono  |||||"},									// mVal_AttesaOzono
				{"Inibizione Sirena? |"},									// mOpz_Sirena
				{"Durata Sirena  |||||"},									// mVal_AttivazSirena
				{"Ritardo Sirena |||||"},									// mVal_RitardoSirena
				{"MultiVend Autom. ? |"},									// mOpt_AutomaticMultiVend
				{"Tipo Valuta    |||||"},									// mTipoValuta
				{"Errore Collaudo N ||"},									// mErrCollaudo
				{"Prod2 Aperto=Fine? |"},									// mOpt_SwitchFineProdotto_N_2
				{"FineProd1->INH Sel |"},									// mOpt_InhSelAfterEndProd_N_1
				{"FineProd2->INH Sel |"},									// mOpt_InhSelAfterEndProd_N_2
				{"Multiciclo ?       |"}									// mMultiCiclo
};		

//************************************************************************************
//************************************************************************************
// ***********************************************************************************
// **********      S E C O N D A       L I N G U A                              ******
// ***********************************************************************************
//************************************************************************************
//************************************************************************************


const char Lang1_ProgMsgLCD[230][20] = {
#if LCDmsg_Lingua
				{"   A L P   S R L    "},									// mInitMsgLCD			1
				{" CPU  Revision xx.xx"},
				{" Boot Revision xx.xx"},
				{"                    "},
		
				{"  Machine   ON      "},									// mStByNoZucNoResto	2
				{"                    "},
				{"No Change           "},
				{"Credit  Euro       0"},
		
				{"  Machine   ON      "},									// mStByNoZucDaResto	3
				{"                    "},
				{"                    "},
				{"Credit  Euro       0"},
		
				{"  Machine   ON      "},									// mStByZucNoResto		4
				{" Sugar    -XXXOO+   "},
				{"No Change           "},
				{"Credit  Euro       0"},
		
				{"  Machine   ON      "},									// mStByZucDaResto		5
				{" Sugar    -XXXOO+   "},
				{"                    "},
				{"Credit  Euro       0"},

				{"  Please   Wait     "},									// mLCDWait				6
				{"    progress        "},
				{"   dispensing in    "},
				{"                    "},
				
				{"    Dispensing      "},									// mLCDGetBev			7
				{"                    "},
				{"    Finished        "},
				{"                    "},
				
				{"                    "},									// mCostoSelez			8
				{" SELECTION    N.  X "},
				{"                    "},
				{"Price   Euro        "},
				
				{"     MACHINE        "},									// mNoVMC				9
				{"      ORDER         "},
				{"     OUT  OF        "},
				{"                    "},
				
				{"                    "},									// mProdEsaur			10
				{"     Capsule        "},
				{"                    "},
				{"  Not  Available    "},
				
				{"   A L P   S R L    "},									// mNewConfig			11
				{"    End   of        "},
				{"   Please  Wait     "},
				{"   Programming      "},
				
				{"   A L P   S R L    "},									// mWaitReset			12
				{"                    "},
				{"   Please  Wait     "},
				{"     Restart        "},
				
				{"   Please  Wait     "},									// mRiscaldam			13
				{"     Heating        "},
				{"     Machine        "},
				{"                    "},
				
				{"   Please    Wait   "},									// mLavGrup				14
				{"   in    progress   "},
				{"   Group  Washing   "},
				{"                    "},

				{"   Please    Wait   "},									// mLavMixer			15
				{"   in    progress   "},
				{"   Mixer  Washing   "},
				{"                    "},
				
				{"   A L P   S R L    "},									// mWaitInit			16
				{"   Please  Wait     "},
				{"                    "},
				{"  Machine   Init    "},
				
				{"   A L P   S R L    "},									// mProgrammaz			17
				{"     Machine        "},
				{"                    "},
				{"   Programming      "},

				{"      Product       "},									// mLCDVendFail			18
				{"                    "},
				{"      Delivery      "},
				{"      Failed        "},

				{"     Machine        "},									// mDAVuoto				19
				{"                    "},
				{"      Empty         "},
				{"                    "},
				
				{"IN_CONF :     ----  "},									// mUSB_KEY				20
				{"  AUDIT :     ----  "},
				{"OUT_CON :     ----  "},
				{"   LOG  :     ----  "},

				{"  USB  Key  Type    "},									// mUSB_KEY_Withdraw	21
				{"  Please  withdraw  "},
				{"   not  detected    "},
				{"                    "},
				
				{"Erogaz 1            "},									// mStBy_Msg			22
				{"Erogaz 3            "},
				{"Erogaz 2            "},
				{"Erogaz 4            "},

#endif	// End LCDmsg_Lingua


				{"  PLEASE   INSERT   "},									// mInserire
				{"PASSWORD:       ****"},									// mPassword
				{"New Password   |||||"},									// mNewPassword
				{"   CLEAR    ALL     "},									// mAzzerare
				{"   Memory   data?  |"},									// mMemoriaDati
				{"      Menu          "},									// mMenu
				{" Time /Doses /Caps  "},									// mTempiDosi
				{"Selection  Prices   "},									// mPrezzoSelez
				{"Selection  Discount "},									// mScontoSelez
				{"Machine Temperature "},									// mTemperMacchina
				{"       Audit        "},									// mMenuAudit
				{"Payment Systems     "},									// mSistemiPagamento
				{"Selection->Num Price"},									// mSelezPrezzo
				{"  Misc  Options     "},									// mOpzioniVarie
				{"A L P     Settings  "},									// mImpostazioniProduttore
				{"      Test          "},									// mCollaudo
				{"    Washings        "},									// mLavaggi
				{"  Time  Slots       "},									// mRisparmio
				{" Factory   Settings "},									// menuParamFabbrica
				{"Selection N.      ||"},									// mSelezioneNumero
				{"Selection N.       |"},									// mSelezioneUnaCifra
				{"Modify ?            "},									// mReq_DaProgrammare
				{"Selection Disabled |"},									// mSel_Disable    
				{" Impulsi ?         |"},									// mSel_Impulsi    
				{"    Price     ||||||"},									// mSel_Prezzo    
				{"OFF Price     ||||||"},									// mSel_Sconto    
				{"Price Number     |||"},									// mTab_SelPrez
				{"123456789 10 11 12  "},									// mSel_ColCaps   
				{"Executive ?        |"},									// mPay_Executive    
				{"Price Holding ?    |"},									// mPay_ExecPrHold   
				{"Price Display ?    |"},									// mPay_ExecPrDisplay
				{"Change Immediately?|"},									// mPay_ExecMDBRestoSubito
				{"Mifare+Validator?  |"},									// mPay_Mifare   
				{"Mifare PIN 1   *****"},									// mPay_MifarePin1
				{"Mifare PIN 2   *****"},									// mPay_MifarePin2  
				{"New Mifare PIN |||||"},									// mPay_NewMifarePin
				{"MifareOperator |||||"},									// mPay_MifareGest  
				{"Mach. Code  ||||||||"},									// mPay_CodiceMacchina  
				{"Mach Code ||||||||||"},									// mPay_CodiceMacchina10Cifre  
				{"Machine  Code       "},									// msg_CodiceMacchina  
				{"Location Code  |||||"},									// mPay_CodiceLocazione 
				{"Location Code       "},									// msg_CodiceLocazione 
				{"Discount Enabled?  |"},									// mPay_EnaDiscount  
				{"Coin N.           ||"},									// mPay_CoinNum
				{"Value         ||||||"},									// mPay_CoinVal
				{"Max Card Cred ||||||"},									// mPay_MaxCard
				{"Max Cash Cred ||||||"},									// mPay_MaxCash
				{"Selections Free ?  |"},									// mPay_Gratis   
				{"MDB ?              |"},									// mPay_MDB
				{"MDB Slave ?        |"},									// mPay_MDBSLV
				{"MultiVend ?        |"},									// mPay_MDBMultiVend
				{"Max Change    ||||||"},									// mPay_MDBMaxChange
				{"Change W/out Vend? |"},									// mPay_MDBCambiaMonete
				{"Bank Always Enab.? |"},									// mPay_MDBBillEnaWithCard
				{"Enabled ?          |"},									// mPay_MDBCoinBillEna
				{"Banknote  N.      ||"},									// mPay_MDBBillNum
				{"Coins in Tube    |||"},									// mPay_MDBCoinsInTube
				{"Decimal Point    |||"},									// mOpt_DPP  
				{"Machine  Type N  |||"},									// mOpt_DaType
				{"KeyBoard Type N  |||"},									// mOpt_KeyBoardType
				{"Counter Down?      |"},									// mOpt_EnaDeconto
				{"Actual counter      "},									// msg_DecontoAttuale
				{"Counter Val    |||||"},									// mOpt_DecontoMassimo
				{"Counter Low    |||||"},									// mOpt_DecontoRiserva
				{"Reload  Counter ?  |"},									// mCMD_ResetDeconto
				{"Service             "},									// msg_RiservaDeconto
				{"Water softener ?   |"},									// mOpt_EnaDecalcific
				{"Val softener   |||||"},									// mOpt_DecalcifMassimo
				{"Clear Memory ?     |"},									// mOpt_ClearEE  
				{"Clear Audit?       |"},									// mOpt_ClearAuditEE  
				{"Sure To Clear?     |"},									// mOpt_Sicuro  
				{"Command Number   |||"},									// mOpt_CmdCollaudo
				{"Delay Selez (sec)|||"},									// mOpt_CmdAttesaTraSelezioni
				{"   Test   Over      "},									// mOpt_CollaudoTerminato
				{"Vend Sales Numb LR  "},									// mAud_TotBattuteLR
				{"Vend Sales Numb IN  "},									// mAud_TotBattuteIN
				{"Test Sales Numb     "},									// mAud_TotBattuteTest
				{"Vend Sales Value LR "},									// mAud_TotVendutoLR  
				{"Vend Sales Value IN "},									// mAud_TotVendutoIN  
				{"  Clock  Setting?  |"},									// mOrologio
				{"  Date   and  Time  "},									// mDataOra
				{"  D  M  Y W   h  m  "},									// mClockTipoDato
				{"Lights time range? |"},									// mOpt_EnaONLuci
				{"Day (1=Monday)     |"},									// mDayOfTheWeek
				{"Time Slot      N.  |"},									// mFasciaNum
				{"Time Slot Setting? |"},									// mImpostaFasce
				{"Factory Params ?   |"},									// mSetDefaultFabbrica  
//				{"Language Number  |||"},									// mLanguageNum
				{"Second Language?   |"},									// mLanguageNum
//				{"Exec Master/Slave? |"},									// mPay_ExecMstSlv
				{"Start Automatico ? |"},									// mOpt_AutomaticStart  
				{"LCD 16x2 ?         |"},									// mLCD_16x2
				{"   Motor   Pulses   "},									// mOpzNumRotaz
//				{"Ozone Installed?   |"},									// mOpz_Ozono
				{"Prod1 End=SwOpen?  |"},									// mOpt_SwitchFineProdotto_N_1
				{"Time Seconds   |||||"},									// mVal_TimeSel
				{"FlowMeter Count|||||"},									// mVal_CounterSel
				{"No Pushbuttons ?   |"},									// mOpt_SenzaPulsanti
				{"Sel-1 I  output  |||"},									// mVal_Sel_1_Out1
				{"Sel-1 II output  |||"},									// mVal_Sel_1_Out2
				{"Sel-2 I  output  |||"},									// mVal_Sel_2_Out1
				{"Sel-2 II output  |||"},									// mVal_Sel_2_Out2
				{"Sel-3 I  output  |||"},									// mVal_Sel_3_Out1
				{"Sel-3 II output  |||"},									// mVal_Sel_3_Out2
				{"Sel-4 I  output  |||"},									// mVal_Sel_4_Out1
				{"Sel-4 II output  |||"},									// mVal_Sel_4_Out2
				{"CleanFilter out  |||"},									// mVal_CleanFilterOutNum
				{"Dispen 1: Flow n |||"},									// mVal_FlussimErogaz_1
				{"Dispen 2: Flow n |||"},									// mVal_FlussimErogaz_2
				{"Dispen 3: Flow n |||"},									// mVal_FlussimErogaz_3
				{"Dispen 4: Flow n |||"},									// mVal_FlussimErogaz_4
				{"Cooler ON Temper  ||"},									// mVal_TemperVentolaON  
				{"CPU s/n   ||||||||||"},									// mPay_CPU_SerialNumber  
				{"Remote Inhibit?    |"},									// mLCD_RemoteInhibit 
				{"Time Ozone ON  |||||"},									// mVal_ErogazOzono
				{"Delay Ozone    |||||"},									// mVal_AttesaOzono
				{"Inibizione Sirena? |"},									// mOpz_Sirena
				{"Durata Sirena  |||||"},									// mVal_AttivazSirena
				{"Ritardo Sirena |||||"},									// mVal_RitardoSirena
				{"Autom. MultiVend ? |"},									// mOpt_AutomaticMultiVend
				{"Currency       |||||"},									// mTipoValuta
				{"Errore Collaudo N ||"},									// mErrCollaudo
				{"Prod2 End=SwOpen?  |"},									// mOpt_SwitchFineProdotto_N_2
				{"EndProd1-> INH Sel |"},									// mOpt_InhSelAfterEndProd_N_1
				{"EndProd2-> INH Sel |"},									// mOpt_InhSelAfterEndProd_N_2
				{"Multiciclo ?       |"}									// mMultiCiclo
};



							// *****************************************************
							// ****   M E S S A G G I    L C D     4 x 20    *******
							// *****************************************************

//                                    		  |---  Riga  1 ------|---  Riga  3 ------|---  Riga  2 ------|---  Riga  4  ----|
//                                    		  12345678901234567890123456789012345678901234567890123456789012345678901234567890
//                                    		  11111111111111111111333333333333333333332222222222222222222244444444444444444444
#if LCDmsg_Lingua == 0
const  char mInitMsgLCD[] 				=	{" A L P   SrL                             CPU Rev. xx.xx                         "};
const  char mStByNoZucNoResto[] 		= 	{"Macchina in servizio                       Non  da  resto   Credito            0"};
const  char mStByNoZucDaResto[] 		= 	{"Macchina in servizio                                        Credito            0"};
const  char mCostoSelez[] 				= 	{"                     EROGAZIONE   N.  X                     Prezzo              "};
const  char mNoVMC[]  	  				=	{"    MACCHINA                            FUORI SERVIZIO                          "};
const  char mProdEsaur[]  				=	{"                         Erogazione                           Non  Disponibile  "};
const  char mNewConfig[]  				=	{"    A L P   SrL            Fine              Attendere         Programmazione   "};
const  char mWaitReset[]  				=	{"    A L P   SrL          Reset               Attendere           Macchina       "};
const  char mProgrammaz[] 				=	{"    A L P   SrL        Macchina    in                          Programmazione   "};
const  char mLCDVendFail[]				=	{"     Erogazione                              Prodotto             Fallita       "};
const  char mUSB_KEY[] 					=	{"IN_CONF :   ----      AUDIT :   ----    OUT_CONF:   ----       LOG  :   ----    "};
const  char mUSB_KEY_Withdraw[] 		=	{" Tipo Chiave  USB    Estrarre Chiave      non rilevabile                        "};
const  char mStBy_Msg[]    				= 	{"Erogaz 1            Erogaz 3            Erogaz 2            Erogaz 4            "};
const  char mIN_CORSO[] 				= 	{"In Corso"};
const  char mIN_PAUSA[] 				= 	{"In Pausa"};
const  char mPRONTA[] 					= 	{"  Pronta"};
const  char mNON_DISPO[]				= 	{"--------"};
//                                    		  12345678901234567890123456789012345678901234567890123456789012345678901234567890
const  char mCollInCorso[]  			=	{"    A L P   SrL              in              Collaudo             Corso         "};
const  char mRadConfig[]  				=	{"     A t t e s a            Chiave           Lettura              U S B         "};
const  char mDownloadIrda[]  			=	{"      Prelevare             da              A u d i t            IrDa    C P U  "};
const  char mClearMemory[]  			=	{"     Azzeramento             in                Memoria             corso        "};
const  char mEndCollaudo_OK[]  			=	{"    A L P   SrL           Terminato         Collaudo           Con   Successo    "};


#endif


							// *****************************************************
							// ****   M E S S A G G I    L C D     2 x 16    *******
							// *****************************************************

//                                    		  |---  Riga  1 ------|---  Riga  3 ------|---  Riga  2 ------|---  Riga  4  ----|
//                                    		  12345678901234567890123456789012345678901234567890123456789012345678901234567890
//                                    		  11111111111111111111333333333333333333332222222222222222222244444444444444444444
#if LCDmsg_Lingua == 0
const  char mInitMsgLCD_2x16[] 			=	{" A L P SrL                               CPU Rev. xx.xx                         "};
const  char mCostoSelez_2x16[] 			= 	{"EROGAZIONE N. X                         Prezzo       0                          "};
const  char mWaitReset_2x16[]  			=	{"   Attendere                                 Reset                              "};
const  char mNewConfig_2x16[]  			=	{" Attendere Fine                          Programmazione                         "};
const  char mUSB_KEY_2x16[] 			=	{"CFG_W:--  AUD:--                        CFG_R:--  LOG:--                        "};
const  char mUSB_KEY_Withdraw_2x16[]	=	{"  Errore  USB                           Estrarre  Chiave                        "};
const  char mLCDVendFail_2x16[]			=	{"  Erogazione                                Fallita                             "};
const  char mNoVMC_2x16[]  	  			=	{"    MACCHINA                            FUORI SERVIZIO                          "};
const  char mProdEsaur_2x16[]  			=	{"  Erogazione                            Non Disponibile                         "};
const  char mStBy_Msg_2x16[]    		= 	{"1:* 2:* 3:* 4:*                         Credito       0                         "};
const  char mIN_CORSO_2x16[] 			= 	{"E"};
const  char mIN_PAUSA_2x16[] 			= 	{"P"};
const  char mPRONTA_2x16[] 				= 	{"R"};
const  char mNON_DISPO_2x16[]			= 	{"-"};
// -----  Messaggi  Chiave  USB   2x16  --------
const  char mKeyOK_2x16[]				= {"OK"};							// Operazione correttamente riuscita
const  char mFileErr_2x16[]				= {"FE"};							// Errore lettura/scrittura chiave USB
const  char mNoFileCfw_2x16[]			= {"NF"};							// Non ci sono file .cfw di aggiornamento fw sulla chiave
const  char mNoDIR_2x16[]				= {"ND"};							// Manca la directory sulla chiave
const  char mPSWErr_2x16[]				= {"PE"};							// Password errata
const  char mDirFull_2x16[]				= {"DF"};							// Presenza di un numero di file nella directory che non permette di registrarne altri 
const  char mAudDenied_2x16[]			= {"DN"};							// Negata autorizzazione al prelievo Audit (probabile presenza credito)
const  char mNoLogData_2x16[]			= {"NL"};							// Negata autorizzazione al prelievo Logs (probabile presenza credito)
const  char mErrEEProm_2x16[]			= {"ME"};							// Errore accesso memoria EEProm

#endif

				
// ***************  Messaggi   di  VMC   Guasto   ******************	
	
const  char mErrMotFAIL[] = 	{"Nessuna Colonna Disp"};					// {"Motori Caps Guasti  "};
const  char mICCFail[] 	  = 	{"ICC  Motors         "};					// {"Corto Circuito Motor"};
const  char mNoParam[]	  = 	{"  NO  Parameters    "};					// {"Mancano Parametri   "};
const  char mMotCapsGuasto[]= 	{"Fail Mot Spira/Caps "};					// {"Fail Mot Spira/Caps "};
const  char mErrNoLink[]  = 	{"Master No Link      "};
const  char mFineDeconto[] = 	{"CNT0                "};					// {"Deconto in riserva  "};
const  char mErrNoCodes[] = 	{"   No  Codes        "};
const  char mTempOutRange[] = 	{"TLim                "};					// {"NTC Fuori Range     "};

const  char mFINE_PRODOTTO[]		= {"FP"};								// {"Fine Prodotto"};
const  char mEMERGENCY_PRESSED[]	= {"EM"};								// {"Emergency Pressed"};

// -----  Messaggi  Chiave  USB   4x20  --------
const  char mKeyOK[]		= {"   OK     "};								// Operazione correttamente riuscita
const  char mFileErr[]		= {" FILE ERR "};								// Errore lettura/scrittura chiave USB
const  char mNoFileCfw[]	= {" NO FILE  "};								// Non ci sono file .cfw di aggiornamento fw sulla chiave
const  char mNoDIR[]		= {" NO DIR   "};								// Manca la directory sulla chiave
const  char mPSWErr[]		= {" PSW ERR  "};								// Password errata
const  char mDirFull[]		= {" DIR FULL "};								// Presenza di un numero di file nella directory che non permette di registrarne altri 
const  char mAudDenied[]	= {"  DENIED  "};								// Negata autorizzazione al prelievo Audit (probabile presenza credito)
const  char mNoLogData[]	= {" NO LOGS  "};								// Negata autorizzazione al prelievo Logs (probabile presenza credito)
const  char mErrEEProm[]	= {"Memory Err"};								// Errore accesso memoria EEProm













