
#ifndef __icpVMC__
#define __icpVMC__

#include "IcpCHG.h"
#include "IcpBILL.h"
#include "IcpCPC.h"
#include "IcpUSD.h" 


typedef enum  {
	ICP_VMCCMDRES_NONE = 0,
	ICP_VMCCMDRES_JUSTRESET,
	ICP_VMCCMDRES_REJECTED,
	ICP_VMCCMDRES_INQUEUE,
	ICP_VMCCMDRES_RUNNING,
	ICP_VMCCMDRES_DONE
} ET_VMCCMDRES;


void ICPCHGHook_SetReady(bool fReady);
void ICPCHGHook_SetDPP(uint8_t nDPP);
void ICPCHGHook_SetCoinsDeposited(ET_CHGPOLLCOINDEST nDestType, uint8_t nCoinType, CreditCoinValue nCoinValue);
void ICPCHGHook_SetCoinsDispMan(uint8_t nCoinsDispensed, uint8_t nCoinType); 
void ICPCHGHook_SetEscrow(void);
void ICPCHGHook_SetPaidoutAmount(CreditCoinValue nPayoutVal);	//solo aggiornamento display
void ICPCHGHook_SetPayoutComplete(CreditCoinValue nPaidoutVal);
void ICPCHGHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen);
void ICPCHGHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPCHGHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPCHGHook_SetSWVersion(uint16_t nSwVersion);
void ICPCHGHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen);

bool ICPCHGHook_SetExactChange(uint8_t);
void ICPCHGHook_SetTubesFull(bool fTubesFull);


void 			ICPCHGSetCoinEnableMask(uint16_t nMask);
CreditCoinValue ICPCHGGetCoinValue(uint8_t nCoinType);
ET_VMCCMDRES 	ICPCHGGetCmdResult(void);
bool 			ICPCHGReset(void);
bool			ICPCHGEnable(CreditValue nCashLimitAmount);
bool 			ICPCHGEnableCoinInTube(void);
bool 			ICPCHGInhibit(void);
bool 			ICPCHGDispense(uint8_t nCoinType, uint8_t nCoins);
bool 			ICPCHGPayout(CreditCoinValue nPayoutVal);
bool 			ICPCHGDiag(uint8_t *ptData, uint8_t nDataLen);
void  			DisattivaEnDisCHG(void);
void  			DisattivaEnDisBill(void);


void ICPBILLHook_SetReady(bool fReady);
void ICPBILLHook_SetDPP(uint8_t nDPP);
void ICPBILLHook_SetBillsDeposited(ET_BILLPOLLDEST nDestType, uint8_t nBillType, CreditCoinValue nBillValue);
void ICPBILLHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen);
void ICPBILLHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPBILLHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPBILLHook_SetSWVersion(uint16_t nSwVersion);
void ICPBILLHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen);

void ICPBILLHook_SetStackerFull(bool fStackerFull);


void ICPBILLSetBillEnableMask(uint16_t nMask);
CreditCoinValue ICPBILLGetBillValue(uint8_t nBillType);
ET_VMCCMDRES ICPBILLGetCmdResult(void);
bool ICPBILLReset(void);
bool ICPBILLEnable(CreditValue nCashLimitAmount);
bool ICPBILLInhibit(void);
//bool ICPBILLEscrow(bool fEscrow);
bool ICPBILLDiag(uint8_t *ptData, uint8_t nDataLen);




void ICPCPCHook_SetReady(bool fReady);
void ICPCPCHook_SetDPP(uint8_t nDPP);
void ICPCPCHook_RechargeOption(bool enable);
void ICPCPCHookEnDis(void);
void ICPCPCHook_MediaCredit(CreditCpcValue nCpcValue, bool fDisplayCredit);
void ICPCPCHook_MediaID(uint32_t nMediaID);
//void ICPCPCHook_MediaUsage(uint8_t nUsageType, ...);
void ICPCPCHook_MediaLanguage(uint16_t nLanguageCode);
void ICPCPCHook_MediaCurrency(uint16_t nCurrencyCode);
void ICPCPCHook_VendApproved(CreditCpcValue nVendAmount, uint16_t nCurrencyCode);
void ICPCPCHook_VendDenied(void);
void ICPCPCHook_EndSession(void);
void ICPCPCHook_OpCancelled(void);
void ICPCPCHook_RefundError(bool fRefundFail);
void ICPCPCHook_RevalueApproved(void);
void ICPCPCHook_RevalueDenied(void);
void ICPCPCHook_RevalueLimit(CreditCpcValue nRevalueAmount, uint16_t nCurrencyCode);
void ICPCPCHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen);
void ICPCPCHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPCPCHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPCPCHook_SetSWVersion(uint16_t nSwVersion);
void ICPCPCHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen);
void ICPCPCHookSetMaxRevalue(CreditCpcValue CostoSelezione);
void ICPCPCHook_CheckOrionState(void);
bool  CheckZeroCPC_USF(void);
bool  Is_CPC_Idle_State(void);
bool  Is_CPC_EQ_LESS_Enable_State(void);
uint32_t  GetLastCPC_MediaID(void);
void ICPCPCHook_SetMaxNonResponseTime(uint8_t nTmMNR);



ET_VMCCMDRES ICPCPCGetCmdResult(void);
bool ICPCPCReset(void);
bool ICPCPCVendReq(CreditCpcValue nSelPrice, uint16_t nSelNumber, uint16_t nCurrencyCode);
bool ICPCPCNegVendReq(CreditCpcValue nSelPrice, uint16_t nSelNumber, uint16_t nCurrencyCode);
bool ICPCPCVendSuccess(uint16_t nSelNumber);
bool ICPCPCVendFailure(void);
bool ICPCPCVendCashSale(CreditCpcValue nSelPrice, uint16_t nSelNumber, uint16_t nCurrencyCode);
bool ICPCPCPullMedia(void);
bool ICPCPCEnable(void);
bool ICPCPCDisable(void);
bool ICPCPCRevalueRequest(CreditCpcValue nRevalueAmount, uint16_t nCurrencyCode);
bool ICPCPCRevalueLimit(void);
bool ICPCPCDiag(uint8_t *ptData, uint8_t nDataLen);


ET_VMCCMDRES ICPUSDGetCmdResult(uint8_t usdSlave);
bool ICPUSDReset(uint8_t usdSlave);
bool ICPUSDDisable(uint8_t usdSlave);
bool ICPUSDEnable(uint8_t usdSlave);
bool ICPUSDVendApproved(uint8_t usdSlave);
bool ICPUSDVendDenied(uint8_t usdSlave);
bool ICPUSDVendSel(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn);
bool ICPUSDVendHomeSel(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn);
bool ICPUSDVendSelStatus(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn);
bool ICPUSDFundsCredit(uint8_t usdSlave, CreditCpcValue creditAmount);
bool ICPUSDFundsSelPrice(uint8_t usdSlave, uint8_t nSelRow, uint8_t nSelColumn, CreditCpcValue nSelPrice, uint16_t nSelID);
bool ICPUSDDiag(uint8_t usdSlave, uint8_t *ptData, uint8_t nDataLen);


void ICPVMCHook_GetManufactureCode(uint8_t *ptData, uint8_t nDataLen);
void ICPVMCHook_GetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPVMCHook_GetModelNumber(uint8_t *ptData, uint8_t nDataLen);
uint16_t ICPVMCHook_GetSwVersion(void);
void ICPVMCHook_GetDate(uint8_t *ptYears, uint8_t *ptMonths, uint8_t *ptDays);
void ICPVMCHook_GetTime(uint8_t *ptHours, uint8_t *ptMinutes, uint8_t *ptSeconds);
void ICPVMCHook_GetDateInfo(uint8_t *ptDayOfWeek, uint8_t *ptWeekNumber, uint8_t *ptSummertime, uint8_t *ptHoliday);
bool ICPVMCHook_GetMultiVend(void);
bool ICPVMCHook_GetSlaveUSD(void);
bool ICPVMCHook_GetSlaveMode(void);
//uint16_t ICPVMCHook_GetScalingFactor(void);				Spostata in ICPUSDHook
//uint8_t ICPVMCHook_GetDecimalPoint(void);				Spostata in ICPUSDHook
CreditCpcValue ICPVMCHook_GetMaxPrice(void);
CreditCpcValue ICPVMCHook_GetMinPrice(void);

void ICPVMCHook_DisplayMsg(uint8_t *ptMsg, uint8_t nMsgLen, uint16_t nMsecDisplayTime);


//18/12/2007 kModifMdb_NewExactChange (richiesta per mdbmdb)
#ifndef ICPProt_VMCCheckExactChange
#define ICPProt_VMCCheckExactChange ICPProt_VMCCheckExactChange
#endif
bool ICPCHGCoinsInTubeGet(uint8_t nCoinType, uint8_t *nCoins);
//18/12/2007 kModifMdb_NewExactChange (richiesta per mdbmdb)

#endif
