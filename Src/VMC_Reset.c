/****************************************************************************************
File:
    VMC_Reset.c

Description:
    Funzioni di inizializzazione della CPU VMC
    

History:
    Date       Aut  Note
    Apr 2019 	MR   

*****************************************************************************************/

#include <string.h>
//#include <stdio.h>
#include "main.h"

#include "ORION.H"
#include "IICEEP.h"
#include "VMC_CPU_LCD.h"
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"
#include "Power.h"
#include "Funzioni.h"
#include "Sel24.h"
#include "phcsBflStatus.h"
#include "stm32l4xx_hal_def.h"
#include "VMC_CPU_Prog.h"
#include "VMC_CPU_Motori.h"
#include "VMC_CPU_Tasti.h"
#include "VMC_Reset.h"


#if IOT_PRESENCE
	#include "IoT_Comm.h"
#endif

#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void  		TestMonitor(void);
extern	uint8_t		CheckNewConfig(void);
extern	uint8_t		SendConfToEEprom(uint8_t);
extern	void		LeggiOrologio(void);
extern	void   		TestPaymentSettings(void);
extern	void  		CheckDeconto(void);
extern	void		Init_DDCMP_Protocol(void);
extern	void 		TestAuditExtFullStatus(void);
extern	void  		UpDateDeconto(bool SelezOK);
extern	void 		AuditExtInitData(void);
extern	void 		HAL_NVIC_SystemReset(void);
extern	bool  		Test_uP(uint8_t *codici);
extern	void  		ResetGuasti(void);
extern	void  		ValidateCashDaSelettore(void);
extern	void  		ValidateCashDaBankReader(void);
extern	void  		AzzeraCash(uint8_t TestPWDN_Flag);
extern	void 		Overpay(uint8_t);
extern	bool  		IsResetDaPWDown(void);
extern	bool  		IsResetDaSWReset(void);
extern	void  		ClrResetFlags(void);
extern	void  		RS485_Comm_Init(void);
extern	void 		USB_ff_files_init(void);
extern	void  		RS485_Expansion_Comm_Init(void);
extern	void  		InitLogBufferPointers(bool MemClr);
extern	void  		Init_EEAddr_Config_Block_Save(uint8_t NumBlock);
extern	void 		Testing(void);
extern	void 		Collaudo_Part_1(void);


#if IOT_PRESENCE
extern	void  IoT_COMM_Init(void);
	tFasce  		FasciaAuditSales;														// Fascia orarie per inviare Audit Sales al cloud AWS
	uint8_t			NumCifratura;
#endif

#if CALC_SICUR_CODES
extern	void cifra(void);
#endif

extern	phcsBfl_Status_t 	PN512_mif_init(uint32_t speed, uint8_t mask);

extern	char			LCD_DataBuff[LCD_DBUF_SIZE];
//extern	const uint8_t  	Tasti_METALTARGHE_LCD_Remoto[MATRIX_ROWS][MATRIX_COLS];
//extern	const uint8_t  	Tasti_NOVA_LCD_Remoto[MATRIX_ROWS][MATRIX_COLS];
//extern	const uint8_t  	Tasti_ARDUINO_LCD_Remoto[MATRIX_ROWS][MATRIX_COLS];

extern	uint32_t		TDiff, TSample;
extern	bool			RTCError_Sign;
extern	bool			NewCPU;



/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

static 	uint8_t	CheckMachine_HW_Config(bool AfterConfigCheck);

tVars				gVars = {0};
tOrionKeyVars		gOrionKeyVars = {0};
tVMCConfVars		gVMC_ConfVars = {0};
SelStruct	 		sSelParam_A[MAXNUMSEL];
tVMCState 			gVMCState;
REFUNDS				RefundValues = {0};
REFUNDS				RefundFreeCredit = {0};
REFUNDS				RefundFreeVend = {0};
REFUNDS				RefundCashCB = {0};
DATEREFUNDS			DataRefundValues = {0};
DATEREFUNDS			DataRefundFC = {0};
DATEREFUNDS			DataRefundFV = {0};
DATEREFUNDS			DataRefundCashCB = {0};
PwUP_DWN 			PowSup = {0};
tOrionConfVars		gOrionConfVars = {0};
tOrionCashVars		gOrionCashVars = {0};
tOrionFreeCrVars 	gOrionFreeCrVars = {0};
tOrionFreeVendVars 	gOrionFreeVendVars = {0};
uPrices 			MRPPrices = {0};
tFasce 				FasceSconto[MaxFasceSconto];
Piezo 				Buz;

bool				Inp_Active, Imene, IRQ_TickTimerFunctEnabled, External_LCD_Keyb;
bool				Filtro1_Riserva, Filtro2_Riserva, Filtro3_Riserva, Filtro4_Riserva;


CreditCpcValue		MaxRecharge, MaxChange, MinResidue, MaxCredGetCard;
uint8_t				MultiVend, OpMode, TipoHW;
uint8_t				gVmcState_0, gVmcState_1, gVmcState_2;
uint8_t				gInhibitState, gSysState01, gControlFlagInhibit;
uint8_t				EnableAuditClr, FileAudReady, AuditOptions;
uint8_t				DecimalPointPosition, UnitScalingFactor;
uint8_t				SystOpt3, SystOpt4;
uint8_t				MinCoinsTube,FreeCredAllaSelez, FreeVendAllaSelez;
uint8_t				TabellaSelezPrezzo[MAXNUMSEL+2];
uint8_t				DecontoState;
uint8_t				KeyBoard_Matrix_Array[(ARRAY_MATRIX_SIZE)];

uint16_t			TimeExChEvent, SaveTime;
uint16_t			Gestore, Locazione, Pin1, Pin2;
uint16_t			Locaz1, Locaz2, Locaz3, Locaz4;
uint16_t			MaxCash;
uint16_t			CurrencyCode;
uint16_t			SystOpt1, SystOpt2;
uint16_t			CoinEnMask, BillEnMask;
uint16_t			DecontoVal, DecontoVal_1, DecontoVal_2, DecontoVal_3, DecontoVal_4, DecontoRiserva;
uint16_t 			CntVolum_1, CntVolum_2, CntVolum_3, CntVolum_4;

uint32_t			SerialNumber, CodiceMacchina;
uint32_t			DataExChEvent, SaveData;
uint32_t			Utente, AccPsw, AudPsw;
uint32_t			RispEnergTime;
uint32_t			Ozono_TimeON, Ozono_TimeWait;
uint32_t			AWS_StatusSend_Interval_Time;
uint32_t			TimeAllarmeSirena, TimeDelayAllarmeSirena, TimeFlushing;

char				ID106_AssetNumberCodeMacchina[20];											// ID106 (EVA-DTS Machine Asset Number)

#if (IOT_PRESENCE == true)
	uint8_t			SSID_WiFi[36], Psw_SSID_WiFi[36];
	uint16_t		PrezzoPA[1];
	MachineStat 	gVmcStatusIoT;																// Stato VMC per telemetria
#endif

	
//************************************************************************
//******  Stringhe di controllo funzioni chiave USB               ********
//************************************************************************
const char FileNameRxConf_String[]		= {"Alp.cfr\0"};
const char FileNameTxConf_String[]		= {"Alp_\0"};
const char FileNameAudit_String[]   	= {"AlpAudit_\0"};
const char FileNameLOG_String[]   		= {"AlpLog_\0"};

/*---------------------------------------------------------------------*\
Private constants and types definition	
\*---------------------------------------------------------------------*/


//	==============================================================================
//	==================    R E S E T    C P U    VMC              =================
//	==============================================================================
void Reset_CPU(void)  {
	
/* Private variables ---------------------------------------------------------*/
	bool		fBOR, fSFTRSTF;																	// Reset da BOR o da SFTRSTF (Software reset Flag)
  	uint8_t 	res, r, datalen;
	uint16_t	addrbuff;
	uint32_t	Temp, Temp1, Temp2, SerNum[3];
	tFasce*		memofasce;

#if (IOT_PRESENCE == true)
	//uint8_t		j;
#endif
	
#if CodMacch_10Cifre
	char*		StrPnt;
#endif

#if  DebugOptions	
	uint8_t 	InitEE = 0;																		// Usata per Debug
#endif
  
// ========================================================================
// ========    Inizializzazioni Varie   ===================================
// ========================================================================

	TastoProgrammazione = 0x01;																	// Parto impostando tasto PROG rilasciato
	memset(&gOrionConfVars, 0, sizeof(gOrionConfVars));
	Motori_OFF();
	Imene = false;
	IRQ_TickTimerFunctEnabled = false;															// Non eseguire Tick Timer Function in Events per non utilizzare periferiche non ancora inizializzate
	memset(&gVMCState, 0, sizeof(gVMCState));
  	Inp_Active = true;																			// E' necessario vedere tutte le linee high prima di considerare il validatore attivo
	DoorOpenInitState();
	HAL_Delay(OneSec);																			// Attesa prima della partenza del firmware

	Read_UniqueID(&SerNum[0]);
	SerialNumber = SerNum[0];
	Tipo_EE = Atmel;
	//Imene = true;																				// Spostato piu' sotto
	IRQ_TickTimerFunctEnabled = true;															// Enable Tick Timer Function in Events 

#if DEBUG_CLR_EEPROM
	//-- Clear  EEProm --
  	BankSelect(0x00);																			// Select Bank0 eeprom 1
	GreenLedCPU(OFF);																			// Led Verde CPU OFF
	RedLedCPU(ON);																				// Led Rosso CPU ON
	AzzeraTuttaEEPROM();
	RedLedCPU(OFF);																				// Led Rosso CPU OFF
	GreenLedCPU(ON);																			// Led Verde CPU ON
#endif

	/*
#if CALC_SICUR_CODES
	//-- Calcola Codici di sicurezza --
	BankSelect(0x00);																			// Select Bank0 eeprom 1
	GreenLedCPU(OFF);																			// Led Verde CPU OFF
	RedLedCPU(ON);																				// Led Rosso CPU ON
	AzzeraTuttaEEPROM();
	FillEEPI2C((uint16_t)GP_CODES, 0, (EE_FREE1 - GP_CODES));									// Azzera Area Codici prima di inserire il codice di sicurezza
	cifra();
	RedLedCPU(OFF);																				// Led Rosso CPU OFF
	GreenLedCPU(ON);																			// Led Verde CPU ON
#endif
	*/
	
	
	BlueLedCPU(OFF);
	YellowLedCPU(OFF);

	// ---------------------------------------------------------------
	// Controllo Tensione Alimentazione Prodotto con ADC1
	Check_PowerSupply_Level();
	AnalogInputs_ADC_Init();
	HAL_Delay(Milli100);																		// Attendo letture ADC2 in IRQ da 1 msec e relativa integrazione
	

	// ---------------------------------------------------------------
	// -- Inizializzazione variabile Tipo Hardware CPU --

	Temp = Read_HW_Type();																		// La ReadAnalog rende la lettura di un canale dell'ADC: in questo caso leggo un partitore sulla CPU
	TipoHW = 0;																					// che identifica il tipo di scheda e i componenti presenti/non montati
	if ((Temp >= BUILD_0_LOW) && (Temp < BUILD_0_HIGH))
	{
		TipoHW = BUILD_0;
	}
	else if ((Temp >= BUILD_1_LOW) && (Temp < BUILD_1_HIGH))
	{
		TipoHW = BUILD_1;
	}
	else if ((Temp >= BUILD_2_LOW) && (Temp < BUILD_2_HIGH))									// Mag 2022: prodotte N. 50 CPU con partitore 1K-2K2 equivalenti alle N. 2 CPU che ho modificato io a mano montando i corretti Mosfet
	{																							// keyboard ma con partitore 1K-10K//4K7
		TipoHW = BUILD_1;
	}
	Imene = true;																				// Set dopo lettura tipo HW altrimenti, nella versione senza Gettoniera, accreditava la prima moneta al power-up
	
	
	// ---------------------------------------------------------------
	// -- Configurazione  Hardware  della  Macchina --
	CheckMachine_HW_Config(false);
	
	// ---------------------------------------------------------------
	// -- Inizializzazione Real Time Clock --
	InitRTC();
	LeggiOrologio();	
	
	// ---------------------------------------------------------------
	// --  Inizializzazione Lingua Messaggi  --- 
  	BankSelect(0x00);																			// Select Bank0 eeprom 1 perche' nella LCD_Init si legge dalla EE
#if LCDmsg_Lingua
  	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptLinguaMessaggi]), (byte *)(&Lingua), 1U);
	if (Lingua > MaxNumLanguage) Lingua = 0;													// Default 0 se parametro non valido
#endif

	// ---------------------------------------------------------------
	// --  Inizializzazione Display  LCD  --- 
	LCD_Init(true);																				// Nella LCD_Init c'e' accesso EE per leggere rev boot
	if (MessMax > 254) BlinkErrRedLed_HALT(Milli50, numMessageLimit);							// Controllo limite numero messaggi in EEprom

	// ------------------------------------------------------------
	// --  Test EEPROM N. 1   --- 
	// ------------------------------------------------------------

	// Test funzionamento EEPROM: Bank 0
  	BankSelect(0x00);																										// Select Bank0 eeprom 1
  	WDT_Clear(gVars.devWDT);
  	Temp = RTC_Counter;
  	WriteEEPI2C(RFUAddr(TestArea), (uint8_t *)(&Temp), sizeof(Temp));
  	if (!ReadEEPI2C(RFUAddr(TestArea), (uint8_t *)(&Temp1), sizeof(Temp1))) BlinkErrRedLed_HALT(Milli50, EEPROM_Failure);	// Memoria difettosa o inesistente
  	if (Temp1 != Temp)  BlinkErrRedLed_HALT(Milli50, EEPROM_Failure);														// Memoria difettosa o inesistente

  	// Test funzionamento EEPROM: Bank 1
  	BankSelect(0x01);																										// Select Bank1 eeprom 1
	addrbuff = (uint16_t)TestAreaBank1;
  	ReadEEPI2C(addrbuff, (uint8_t *)(&Temp2), sizeof(Temp2));																// Save contenuto area di test 
  	Temp = ~Temp;
  	WriteEEPI2C(addrbuff, (uint8_t *)(&Temp), sizeof(Temp));
  	if (!ReadEEPI2C(addrbuff, (uint8_t *)(&Temp1), sizeof(Temp1))) BlinkErrRedLed_HALT(Milli50, EEPROM_Failure);			// Memoria difettosa o inesistente
  	if (Temp1 != Temp)  BlinkErrRedLed_HALT(Milli50, EEPROM_Failure);														// Memoria difettosa o inesistente
  	WriteEEPI2C(addrbuff, (uint8_t *)(&Temp2), sizeof(Temp2));																// Restore area di test

	// ------------------------------------------------------------
	// --  Test EEPROM N. 2   --- 
	// ------------------------------------------------------------
  	WDT_Clear(gVars.devWDT);
  	BankSelect(0x02);																										// Select Bank0 eeprom 2
  	Temp += 0x3f4528;
  	WriteEEPI2C(0x00, (uint8_t *)(&Temp), sizeof(Temp));
  	if (ReadEEPI2C(0x00, (uint8_t *)(&Temp1), sizeof(Temp1))) {
  		if (Temp1 == Temp) {
  			gVars.ee2 = true;																								// Memoria eeprom N. 2 presente e funzionante
  	  	} else {
  	  		gVars.ee2 = false;																								// Memoria eeprom N 2 inesistente
  	  	}
  	}
  	BankSelect(0x00);																										// Default Bank0 eeprom 1

	// ========================================================================
	// =====    Test Codici Sicurezza ed eventuale Collaudo  CPU         ======
	// ========================================================================

	Testing();
	
// ========================================================================
// ========    BACKUP-RAM  e  Power Outage Audit     ======================
// ========================================================================

	// Restore Dati in BackupRAM solo se Power ON Reset
	fBOR 	 = IsResetDaPWDown();																// Reset da BOR ?
	fSFTRSTF = IsResetDaSWReset();																// Software Reset ?
	ClrResetFlags();
		
	Restore();		//MR23 Non so perche' si chiama sempre la Restore

	if (fBOR)
	{
	  	//Restore();

#if kOnOffEvents
		//--  Audit OFF-ON --
		CntEventi_PowerOutage++;																// Update "EA*701 ed EA*702" Aud OFF-ON tramite la "Check_Clear_Audit_LR" chiamata nel MAIN
#endif
	}
    
    // Audit Evento Reset da WDOG
	// Incremento il counter di questo evento e lo registro in EE nel Main altrimenti, 
	// usando la chiave USB per prelevare l'Audit, qui registra l'ID EA1 di WD Reset 
	// ma nel Main lo azzera quando esegue la "ClearAudit" in conseguenza del prelievo 
    // Audit appena eseguito.
    if (fSFTRSTF)
	{
		//--  Audit SW Reset --
		CntEventi_WD_Reset++;																	// "EK_05": Reset da Watch-Dog
    	ResetDaWDOG_Events();																	// (03.04.15: spostato nel Main) Reset da WDOG: inserisco Evento
    }

// ========================================================================
// ====  Controllo Bytes Civetta e Credito Cash recuperato da EEprom   ====
// ========================================================================

    // Se i bytes civetta sono corrotti eseguo la Restore perche' potrei averla saltata:
    // se anche i dati della EEPROM sono corrotti la Restore azzerera' tutto.
  	if (Civetta1 != 0x27 || Civetta2 != 0x35 || Civetta3 != 0x5961 || Civetta4 != 0x9496) {
		// Dati EEPROM corrotti: azzera EEPROM e RAM
		Restore();
  	}
  	
	ValidateCashDaSelettore();																	// Controllo correttezza eventuale credito cash
	ValidateCashDaBankReader();
	
	Temp = CashDaSelettore + CashDaBankReader;													// Sommo 2 valori a 16 bit in uno a 32 bit
	if (Temp > MAX_SYSTEM_CREDIT) {																// Overflow sulla somma dei crediti cash
		AzzeraCash(true);
	}
#if ResetClrCash
	Overpay(OVERPAY_CASH);
	AzzeraCash(true);
#endif	


#if CESAM
	ResetGuasti();
#endif
	
	
// --------  Clear Allarmi Non Bloccanti  ----------------	
	

// ========================================================================
// ========    Routines    DEBUG     ======================================
// ========================================================================

#if  DebugOptions	
	  InitEE = 0;
// ========================================================================
// ========  Controllo Limiti Audit Address in EEPROM                  ====
// ========================================================================
//	Test inteso come debug durante lo sviluppo
  	addrbuff = (uint16_t)(IICEEPAuditAddr(DatePrice));																		// Deve essere 0x0900
  	if (addrbuff != AuditIN_Begin)								BlinkErrRedLed_HALT(Milli50, Addr_DatePrice_Error);
  	addrbuff = (uint16_t) (IICEEPAuditAddr(freeINspace));																		// Deve essere 0x0CF0
  	addrbuff = (uint16_t) (IICEEPAuditAddr(INEnd));																			// Deve essere 0x10FE
	if (addrbuff >= AuditLR_Begin)								BlinkErrRedLed_HALT(Milli50, Addr_INEnd_Error);
  	addrbuff = (uint16_t) (IICEEPAuditAddr(LRCardVnd));																		// Deve essere 0x1100
	if (addrbuff != AuditLR_Begin)								BlinkErrRedLed_HALT(Milli50, Addr_LRCardVnd_Error);
  	addrbuff = (uint16_t) (IICEEPAuditAddr(TypeEventList));
  	addrbuff = (uint16_t) (IICEEPAuditAddr(DateEventList));
  	addrbuff = (uint16_t) (IICEEPAuditAddr(TimeEventList));
  	addrbuff = (uint16_t) (IICEEPAuditAddr(IndexDataEvent));
  	addrbuff = (uint16_t) (IICEEPAuditAddr(freeLRspace));
  	addrbuff = (uint16_t) (IICEEPAuditAddr(LREnd));																			// Deve essere 0x20FE
	if (addrbuff >= BeginRefundData)							BlinkErrRedLed_HALT(Milli50, Addr_LREnd_Error);
	
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EEfCoinEnabled[0]));
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EEfBillEnabled[0]));
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EETimeDiff));
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EETimeSample));
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EEDiffSign));
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EEByteFree[0]));
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EEPriceDisplay));
	
	
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EE_GP[0]));
	if (addrbuff != 0x04F0)										BlinkErrRedLed_HALT(Milli50, ConfigLenght_Error3);
	//if (addrbuff != 0x03F0)										BlinkErrRedLed_HALT(Milli50, ConfigLenght_Error3);
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EEPrezzoPR[0]));
	if (addrbuff != 0x0518)										BlinkErrRedLed_HALT(Milli50, ConfigLenght_Error1);
  	addrbuff = (uint16_t) (IICEEPConfigAddr(EEPrezzoPD[100-1]));																// Deve essere 0x8FE
	if (addrbuff > BeginAuditData)								BlinkErrRedLed_HALT(Milli50, ConfigLenght_Error2);
	
  InitEE = 0;
	
  // Debug5: Azzeramento Buffer Eventi
  if (InitEE) {
	FillEEPI2C((uint16_t)(uint32_t)OnOffEventsStart, 0, OnOffEventsLenght);
	InitEE = 0;
  }
  
#endif		// ********* END DEBUG OPTIONS ********* 
  

// ==========================================================================
// ========    Controllo Product  Serial  Number              ===============
// ==========================================================================
  	  
  ReadEEPI2C(ProductSerialNum, (uint8_t *)(&Temp), sizeof(uint32_t));							// Leggo Serial Number dall'area di interscambio
  if (Temp != SerialNumber) {
	  WriteEEPI2C(ProductSerialNum, (uint8_t *)(&SerialNumber), sizeof(uint32_t));				// Registro Serial Number nella eeprom
  }
  
// ==========================================================================
// ========    Controllo disponibilita' nuova Configurazione  ===============
// ==========================================================================
  // Controllo se e' stata memorizzata una nuova configurazione tramite chiave USB

	res = CheckNewConfig();
  	if (res == UpdateCfg_OK)
	{
	  	SendConfToEEprom(false);																// Dopo importazione nuova config. memo tutta la config in EEPROM
		CheckMachine_HW_Config(true);															// Dopo check NewConfig rileggo configurazione macchina
	}

// ========================================================================
// ========    Calibrazione   R T C    ====================================
// ========================================================================
	RTC_Calibration();	

  
// ========================================================================
// ========    Carico Marche Audit USB in RAM  ============================
// ========================================================================
  
	ReadEEPI2C(AuditClearEnabled, (uint8_t *)(&EnableAuditClr), sizeof(EnableAuditClr));		// Marca Azzeramento Audit LR
	ReadEEPI2C(AuditFileReady, (uint8_t *)(&FileAudReady), sizeof(FileAudReady));				// Marca File Audit pronto per essere copiato su chiave USB
	ReadEEPI2C(IICEEPAuditAddr(DateAuditRead), (uint8_t *)(&SaveData), sizeof(SaveData));		// Leggo Data/Ora prelievo Audit
	ReadEEPI2C(IICEEPAuditAddr(TimeAuditRead), (uint8_t *)(&SaveTime), sizeof(SaveTime));
  
  
// ========================================================================
// ========    Carico configurazione in RAM   =============================
// ========================================================================

	ReadEEPI2C(IICEEPConfigAddr(EEAccessPsw), (uint8_t *)(&AccPsw), sizeof(AccPsw));			// Password di Accesso
	ReadEEPI2C(IICEEPConfigAddr(EEAuditPsw), (uint8_t *)(&AudPsw), sizeof(SizeEEAuditPsw));		// Password Audit
	ReadEEPI2C(IICEEPConfigAddr(EEGestore), (uint8_t *)(&Gestore), sizeof(Gestore));			// Gestore
	ReadEEPI2C(IICEEPConfigAddr(EELocazione), (uint8_t *)(&Locazione), sizeof(Locazione));		// Codice Locazione

	ReadEEPI2C(IICEEPConfigAddr(EELocazEnable), (uint8_t *)(&res), sizeof(res));				// Enable/Disable Location Code Test
		gOrionConfVars.EnDisLocationCode = (res) ? true : false;
	//if (gOrionConfVars.EnDisLocationCode == 0) Locazione = 0;									// Se codice locazione non usato lo metto a 0 per l'ID104
	
	ReadEEPI2C(IICEEPConfigAddr(EELocaz1), (uint8_t *)(&Locaz1), sizeof(Locaz1));				// Codice Locazione 1
	ReadEEPI2C(IICEEPConfigAddr(EELocaz2), (uint8_t *)(&Locaz2), sizeof(Locaz2));				// Codice Locazione 2
	ReadEEPI2C(IICEEPConfigAddr(EELocaz3), (uint8_t *)(&Locaz3), sizeof(Locaz3));				// Codice Locazione 3
	ReadEEPI2C(IICEEPConfigAddr(EELocaz4), (uint8_t *)(&Locaz4), sizeof(Locaz4));				// Codice Locazione 4
	ReadEEPI2C(IICEEPConfigAddr(EEDPP), &DecimalPointPosition, sizeof(DecimalPointPosition));	// DPP
	if (DecimalPointPosition > 3) DecimalPointPosition = 2;
	ReadEEPI2C(IICEEPConfigAddr(EEUSF), &UnitScalingFactor, sizeof(UnitScalingFactor));			// USF
	if (UnitScalingFactor == 0) UnitScalingFactor = 1;											// Set Default = 1 per USF
	ReadEEPI2C(IICEEPConfigAddr(EEMultiVendCard), (uint8_t *)(&MultiVend), sizeof(MultiVend));	// Multivend
		MultiVend = (MultiVend) ? true : false;													// 1 = Multivend - 0 = Single Vend
	ReadEEPI2C(IICEEPConfigAddr(EEPin1), (uint8_t *)(&Pin1), sizeof(Pin1));						// Pin1
	ReadEEPI2C(IICEEPConfigAddr(EEPin2), (uint8_t *)(&Pin2), sizeof(Pin2));						// Pin2

#if (CodMacch_10Cifre == false)
	ReadEEPI2C(IICEEPConfigAddr(EECodiceMacchina), (uint8_t *)(&CodiceMacchina), sizeof(CodiceMacchina));		// Codice Macchina
	if (CodiceMacchina > Max_CodiceMacchina) CodiceMacchina = Max_CodiceMacchina;
#endif
	
	ReadEEPI2C(IICEEPConfigAddr(EECreditoMaxCarta), (uint8_t *)(&MaxRecharge), sizeof(MaxRecharge));			// MAXCRED carta
	ReadEEPI2C(IICEEPConfigAddr(EECreditoMaxCash), (uint8_t *)(&MaxCash), sizeof(MaxCash));						// MAXCRED cash
	ReadEEPI2C(IICEEPConfigAddr(EEMaxAcceptCreditCard), (uint8_t *)(&MaxCredGetCard), sizeof(MaxCredGetCard));	// Credito massimo per accettare una carta
	ReadEEPI2C(IICEEPConfigAddr(EEBillValidWCard), (uint8_t *)(&res), sizeof(res));								// Bill Val Enabled w/card
		gOrionKeyVars.BillValidEnaW_Card = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EERechargeTestCard), (uint8_t *)(&res), sizeof(res));							// Carta vendite di test ricaricabile
		gOrionConfVars.RechTestVendCard = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEDescrValuta), (uint8_t *)(&CurrencyCode), sizeof(CurrencyCode));				// Currency Code (Hex) (per l'Euro 0x1978 --> 0x7BA in CurrencyHex)
	if (CurrencyCode < 1000) {																					// Aggiungo "1" nel left most digit, come specificato da EVA-DTS per ID402
		CurrencyCode += 1000;
	}
	ReadEEPI2C(IICEEPConfigAddr(EEFree4), (uint8_t *)(&SystOpt4), sizeof(SystOpt4));							// SystOpt4
	if (SystOpt4 == 0xff) SystOpt4 = 0;

	ReadEEPI2C(IICEEPConfigAddr(EEPriceHolding), (uint8_t *)(&res), sizeof(SizeEEPriceHolding));				// PriceHolding
		gOrionConfVars.PriceHolding = (res) ? true : false;
	if (gOrionConfVars.PriceHolding) {
		ReadEEPI2C(IICEEPConfigAddr(EEPriceDisplay), (uint8_t *)(&res), sizeof(SizeEEPriceDisplay));			// PriceDisplay solo se in Price Holding
		gOrionConfVars.PriceDisplay = (res) ? true : false;
	}
	ReadEEPI2C(IICEEPConfigAddr(EECardLevelsEna), (uint8_t *)(&res), sizeof(SizeEE_CardLevels));				// Enable Card Levels
		gOrionConfVars.CardLevelsEnabled = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEDiscountEna), (uint8_t *)(&res), sizeof(SizeEE_Discount));					// Discount Enable
		gOrionConfVars.DiscountEnabled = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEListaPrezzo), (uint8_t *)(&res), sizeof(SizeEE_ListaPrezzo));					// Lista Prezzi/Sconti
		gOrionConfVars.ListaPrezzo = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEFasciaSconti1Inizio), (uint8_t *)(&Temp), (sizeof(SizeEE_TimeFascia)*2));		// Ore/Min inizio e fine Fascia sconti 1
	memofasce = &FasceSconto[0];
	memofasce->Inizio = Temp;
	memofasce->Fine = (Temp>>16);
	ReadEEPI2C(IICEEPConfigAddr(EEAuditOptions), (uint8_t *)(&AuditOptions), sizeof(SizeEEAuditOption));		// Audit options flag

	ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPR), (uint8_t *)(&MRPPrices.Prices99.MRPPricesList99[0]), (LenListaPrezzi*2));				// Leggo Price List in RAM
	SetMinMaxPrice();																												// Calcolo MinPrice e MaxPrice
	
	ReadEEPI2C(IICEEPConfigAddr(EEFreeCred1), (uint8_t *)(&gOrionFreeCrVars.FreeCr1), sizeof(gOrionFreeCrVars.FreeCr1));			// Valore FreeCredit1
	ReadEEPI2C(IICEEPConfigAddr(EEFreeCred2), (uint8_t *)(&gOrionFreeCrVars.FreeCr2), sizeof(gOrionFreeCrVars.FreeCr2));			// Valore FreeCredit2
	ReadEEPI2C(IICEEPConfigAddr(EEFreeCred3), (uint8_t *)(&gOrionFreeCrVars.FreeCr3), sizeof(gOrionFreeCrVars.FreeCr3));			// Valore FreeCredit3
	ReadEEPI2C(IICEEPConfigAddr(EEFreeCred4), (uint8_t *)(&gOrionFreeCrVars.FreeCr4), sizeof(gOrionFreeCrVars.FreeCr4));			// Valore FreeCredit4
	ReadEEPI2C(IICEEPConfigAddr(EEFreeCrMode), &gOrionFreeCrVars.FreeCrMode, sizeof(gOrionFreeCrVars.FreeCrMode));					// Modo FreeCredit
	ReadEEPI2C(IICEEPConfigAddr(EEFreeCrPeriod), (uint8_t *)&gOrionFreeCrVars.FreeCrPeriod, sizeof(gOrionFreeCrVars.FreeCrPeriod));	// Periodo FreeCredit

	ReadEEPI2C(IICEEPConfigAddr(EEFreeVend1), (uint8_t *)(&gOrionFreeVendVars.FreeVend1), sizeof(gOrionFreeVendVars.FreeVend1));	// Valore FreeVend1
	ReadEEPI2C(IICEEPConfigAddr(EEFreeVend2), (uint8_t *)(&gOrionFreeVendVars.FreeVend2), sizeof(gOrionFreeVendVars.FreeVend2));	// Valore FreeVend2
	ReadEEPI2C(IICEEPConfigAddr(EEFreeVend3), (uint8_t *)(&gOrionFreeVendVars.FreeVend3), sizeof(gOrionFreeVendVars.FreeVend3));	// Valore FreeVend3
	ReadEEPI2C(IICEEPConfigAddr(EEFreeVend4), (uint8_t *)(&gOrionFreeVendVars.FreeVend4), sizeof(gOrionFreeVendVars.FreeVend4));	// Valore FreeVend4
	ReadEEPI2C(IICEEPConfigAddr(EEFreeVendMode), &gOrionFreeVendVars.FreeVendMode, sizeof(gOrionFreeVendVars.FreeVendMode));		// Modo FreeVend
	ReadEEPI2C(IICEEPConfigAddr(EEFreeVendPeriod), (uint8_t *)&gOrionFreeVendVars.FreeVendPeriod, sizeof(gOrionFreeVendVars.FreeVendPeriod));	// Periodo FreeVend
	ReadEEPI2C(IICEEPConfigAddr(EEVega), (uint8_t *)(&res), sizeof(SizeEE_Vega));													// Vega Digisoft
		gOrionConfVars.LA1_Digisoft = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEUser), (uint8_t *)(&res), sizeof(SizeEE_User));													// Nuovo Codice Utente su carte vergini
		gOrionConfVars.NewUserCode = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEInhp), (uint8_t *)(&res), sizeof(SizeEE_INHP));													// Polarita' selettore parallelo
		gOrionConfVars.ValidatorPolarity = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEMaxChange), (uint8_t *)(&MaxChange), sizeof(MaxChange));											// MaxChange
	ReadEEPI2C(IICEEPConfigAddr(EEPrelevaAudExt), (uint8_t *)(&res), sizeof(res));													// Enable/Disable prelievo Audit Estesa
		gOrionConfVars.AuditExt = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EETestAudExtFull), (uint8_t *)(&res), sizeof(res));													// Enable/Disable Test se Audit Estesa FULL
		gOrionConfVars.TestAudExtFull = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEPermanentCash), (uint8_t *)(&res), sizeof(res));													// Cash Permanente
		gOrionConfVars.NoTimeCash = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEMinimoCoinTube), (uint8_t *)(&MinCoinsTube), sizeof(MinCoinsTube));								// Per test ExactChange: livello minimo di monete nei tubi
		if (MinCoinsTube == 0) MinCoinsTube = 3;
	ReadEEPI2C(IICEEPConfigAddr(EEEquazExChng), (uint8_t *)(&res), sizeof(res));													// Opzione ExactChange: 0=qualsiasi tubo vuoto, 1=vuoto tubo valore inferiore
		gOrionConfVars.EquazExCh = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEFreeCredAllaSelez), (uint8_t *)(&FreeCredAllaSelez), sizeof(FreeCredAllaSelez));					// Flag Free-Credit subito (0) o alla selezione (1)
	ReadEEPI2C(IICEEPConfigAddr(EEFreeVendAllaSelez), (uint8_t *)(&FreeVendAllaSelez), sizeof(FreeVendAllaSelez));					// Flag Free-Credit subito (0) o alla selezione (1)
	ReadEEPI2C(IICEEPConfigAddr(EEInhRechargeCard), (uint8_t *)(&res), sizeof(res));												// Ricarica carta: 0=permessa, 1=inibita
		gOrionConfVars.InhRechargeCard = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EECambiaMonete), (uint8_t *)(&res), sizeof(res));													// Opzione Cambiamonete: 0=fare una selez. per avere il resto, 1=cambiamonete
		gOrionConfVars.CambiaMonete = (res) ? true : false;
	ReadEEPI2C(IICEEPConfigAddr(EEGestSel), (uint8_t *)(&res), sizeof(res));														// Opzione Gestione Selezioni
		gOrionConfVars.GestSel = (res) ? true : false;

		MinResidue = 0;				//minimo residuo al di sotto del quale non viene dato il resto --> previsto ma per ora fisso a valore 0 = funzione disabilitata
	// ==================================================================================================================================================

// ========================================================================
// ========    I m p o s t a z i o n i    I o T      ======================
// ========================================================================
#if (IOT_PRESENCE == true)
	
	Init_EEAddr_Config_Block_Save(1);																							// Al restart predispongo sempre address EE per eventuale ricezione new config
	BankSelect(0x00);
	
	ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[TxStatusIntervalTime]), (uint8_t *)(&AWS_StatusSend_Interval_Time), 2U);
	if (AWS_StatusSend_Interval_Time == 0)
	{
		AWS_StatusSend_Interval_Time = 0x7fff0000;
	}
	else
	{
		AWS_StatusSend_Interval_Time = (AWS_StatusSend_Interval_Time * OneMin);
	}
		
	memset(&SSID_WiFi[0], 0x00, sizeof(SSID_WiFi));
	ReadEEPI2C(IICEEPConfigAddr(EESSID[0]), (byte *)(&SSID_WiFi[0]), LEN_SSID);														// Leggo SSID ASCII in RAM
	
	/*
	memset(&SSID_WiFi[0], 0x00, sizeof(SSID_WiFi));
	SSID_WiFi[0] = 'E';
	SSID_WiFi[1] = 'V';
	SSID_WiFi[2] = '9';
	SSID_WiFi[3] = '7';
	*/

	
/*	
	// La stringa con l'SSID non deve essere completata con SPACE ma con zeri
	// Non sono ammessi SPACE nella stringa
	for (j=0; j < LEN_SSID; j++) {																									// Fill zero nei bytes diversi da ASCII
		if ((SSID_WiFi[j] <= 0x20) || (SSID_WiFi[j] > 0x7e))
		{
			SSID_WiFi[j] = 0x00;
		}
	}
*/
	
	
	memset(&Psw_SSID_WiFi[0], 0x00, sizeof(Psw_SSID_WiFi));
	ReadEEPI2C(IICEEPConfigAddr(EE_Psw_SSID[0]), (byte *)(&Psw_SSID_WiFi[0]), LEN_PSW_SSID);										// Leggo Password SSID ASCII in RAM
	
	/*
	memset(&Psw_SSID_WiFi[0], 0x00, sizeof(Psw_SSID_WiFi));
	Psw_SSID_WiFi[0] = 'E';
	Psw_SSID_WiFi[1] = 'V';
	Psw_SSID_WiFi[2] = '9';
	Psw_SSID_WiFi[3] = '7';
	Psw_SSID_WiFi[4] = '-';
	Psw_SSID_WiFi[5] = 'I';
	Psw_SSID_WiFi[6] = '8';
	Psw_SSID_WiFi[7] = '9';
	Psw_SSID_WiFi[8] = '3';
	Psw_SSID_WiFi[9] = '9';
	*/
	
	
/*	
	// La stringa con la Psw_SSID_WiFi non deve essere completata con SPACE ma con zeri
	// Non sono ammessi SPACE nella stringa
	for (j=0; j < LEN_PSW_SSID; j++) {																								// Fill zero nei bytes diversi da ASCII
		if ((Psw_SSID_WiFi[j] <= 0x20) || (Psw_SSID_WiFi[j] > 0x7e))
		{
			Psw_SSID_WiFi[j] = 0x00;
		}
	}
*/
	
	ReadEEPI2C(IICEEPConfigAddr(EECifratura), (byte *)(&NumCifratura), 1U);															// Leggo Numero Cifratura in RAM
	
	//-- Controllo inibizione da remoto --
	ReadEEPI2C(IICEEPConfigAddr(EERemoteINH), (byte *)(&res), sizeof(res));															// Inibizione da remoto
		gOrionConfVars.Remote_INH = (res) ? true : false;
		
	ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPA[0]), (byte *)(&PrezzoPA[0]), sizeof(SizeEEPrezzo));
	ReadEEPI2C(IICEEPConfigAddr(EEFasciaAuditSalesInizio), (byte *)(&Temp), (sizeof(SizeEE_TimeFascia)*2));							// Ore/Min inizio invio Audit Sales
	memofasce = &FasciaAuditSales;
	memofasce->Inizio = Temp;
	memofasce->Fine = (Temp>>16);

#endif
// *********************************************************************************************************		

	// ==================================================================================================================================================
		
	// ===================   Sistemi   di  Pagamento   ======================
	
#if CESAM
	gVMC_ConfVars.ExecutiveSLV = false;																								// Nella CPU ALP non c'e' il Protocollo Executive
	gVMC_ConfVars.ExecMSTSLV = false;
	gOrionConfVars.PriceHolding = false;
#else
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[ExecSlave]), 		(uint8_t *)(&res),							1U);					// Executive SLAVE
		gVMC_ConfVars.ExecutiveSLV = (res) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptExecMstSlav]), 	(uint8_t *)(&res),							1U);					// Executive Master/SLAVE
		gVMC_ConfVars.ExecMSTSLV = (res) ? true : false;
		if (gVMC_ConfVars.ExecutiveSLV == false) {																					// Protocollo Non Executive, clr Master/Slave
			gVMC_ConfVars.ExecMSTSLV = false;
		}
#endif
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]), 	(uint8_t *)(&res),							1U);					// Presenza Lettore Mifare
		gVMC_ConfVars.PresMifare = (res) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]), 	(uint8_t *)(&res),							1U);					// Macchina in gratuito
		gVMC_ConfVars.Gratuito = (res) ? true : false;

	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MDBPaymSyst]), 		(uint8_t *)(&res),							1U);					// MDB
		gVMC_ConfVars.ProtMDB = (res) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MDB_SLV]), 			(uint8_t *)(&res),							1U);					// MDB SLAVE
		gVMC_ConfVars.ProtMDBSLV = (res) ? true : false;
		if (gVMC_ConfVars.ProtMDB == false) {																						// Protocollo Non MDB, clr MDB SLAVE
			gVMC_ConfVars.ProtMDBSLV = false;
		}
	ReadEEPI2C(IICEEPConfigAddr(EEfCoinEnabled), (uint8_t *)(&CoinEnMask), 2U);														// Coins Enabled flags
	ReadEEPI2C(IICEEPConfigAddr(EEfBillEnabled), (uint8_t *)(&BillEnMask), 2U);														// Bills Enabled flags
	
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptExecRestoSubito]), (uint8_t *)(&res),							1U);					// Executive/MDB Resto Subito
		gOrionConfVars.RestoSubito = (res) ? true : false;
	ReadEEPI2C(EEPConfVMCAddr(EE_SelezPrezzo), (uint8_t *)(&TabellaSelezPrezzo[0]), (MAXNUMSEL));									// Leggo Tabella Selezione/Prezzo in RAM
		
	// =======================================================================

	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptEnaONLuci]), 	(uint8_t *)(&res),								1U);					// Abilitazione Fasce Luci
		gVMCState.EnaFasceLuci = (res) ? true : false;
		
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptPresOzono]), 	(uint8_t *)(&res),								1U);					// Abilitazione Ozono
		gVMC_ConfVars.PresOzono = (res) ? true : false;
	ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[OZONO_Timer_WAIT]), (uint8_t *)(&Ozono_TimeWait), 			2U);
	Ozono_TimeWait = (Ozono_TimeWait * OneMin);
	ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[OZONO_Timer_ON]), (uint8_t *)(&Ozono_TimeON), 				2U);
	Ozono_TimeON = (Ozono_TimeON * OneMin);

	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptSirena]), 	(uint8_t *)(&res),								1U);					// Inibizione Sirena Allarme porta aperta
		gVMC_ConfVars.SirenaPortaAperta = (res) ? false : true;
	ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[SIRENA_Timer_Delay]), (uint8_t *)(&TimeDelayAllarmeSirena), 	2U);
	TimeDelayAllarmeSirena = (TimeDelayAllarmeSirena * OneMin);
	ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[SIRENA_Timer_ON]), (uint8_t *)(&TimeAllarmeSirena), 			2U);
	TimeAllarmeSirena = (TimeAllarmeSirena * OneMin);

	ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[EETimeFlushing]), (uint8_t *)(&TimeFlushing), 				2U);
	TimeFlushing = (TimeFlushing * OneMin);
	
	//------ Debug  connessione Gateway al connettore J18 ---	
	//ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GatewayCablata]), 	(uint8_t *)(&res),							1U);					// Gateway collegata con cablaggio a J18
	//gVMC_ConfVars.GatewayOnJ18 = (res) ? true : false;
	//-------------------------------------------------------
	
#if CodMacch_10Cifre
	ReadEEPI2C(IICEEPConfigAddr(EE_CodeMacchina[0]), (uint8_t *)(&ID106_AssetNumberCodeMacchina[0]), EE_CodeMacchinaLen);			// Leggo Cod Macchina ASCII in RAM
	StrPnt = &ID106_AssetNumberCodeMacchina[0];
	k = 0;																															// Flag memorizzazione valore modificato
	for (j=0; j<EE_CodeMacchinaLen; j++)
	{																																// Fill ASCII Zero nei bytes diversi da ASCII
		if ((*(StrPnt+j) < '0') || (*(StrPnt+j) > '9'))
		{
			*(StrPnt+j) = '0';
			k = 1;
		}
	}
	if (k == 1)
	{
		WriteEEPI2C(IICEEPConfigAddr(EE_CodeMacchina[0]), (uint8_t *)(&ID106_AssetNumberCodeMacchina[0]), EE_CodeMacchinaLen);		// Nuovo Cod Macc ASCII in EE
	}
#endif

	// ========================================================================
	// ========  Set  Unused  Options  and   Values     =======================
	// ========================================================================
	// Predispongo alcune opzioni e impostazioni perche' non configurabili da
	// programmazione e da chive USB
	

	
	
	
	// ========================================================================
	// ========  Configurazione    Macchina   Alp       =======================
	// ========================================================================
	
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[StartAutomatico]), (uint8_t *)(&res), 1U);												// Opzione "Start automatica selezione 1 all'inserimento del credito anche con macchine con pulsanti"
		gOrionConfVars.AutomaticStart = (res) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MultiVendAutomatica]), (uint8_t *)(&res), 1U);											// Opzione "Scala automaticamante il credito per prolungare la selezione in corso"
		gOrionConfVars.AutomaticMultiVend = (res) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[PolaritaSwitchProdotto_1]), (uint8_t *)(&res), 1U);										// Polarita' microswitch Fine Prodotto 1
		gOrionConfVars.SwitchFineProdotto_1_Open = (res) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[PolaritaSwitchProdotto_2]), (uint8_t *)(&res), 1U);										// Polarita' microswitch Fine Prodotto 2
		gOrionConfVars.SwitchFineProdotto_2_Open = (res) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[NumSelInhDaFineProd_1]), (uint8_t *)(&gOrionConfVars.SelInhEndProd_1), 1U);				// Numero selezione da inibire quando fine prodotto N. 1
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[NumSelInhDaFineProd_2]), (uint8_t *)(&gOrionConfVars.SelInhEndProd_2), 1U);				// Numero selezione da inibire quando fine prodotto N. 2
		
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[SenzaPulsanti]), (uint8_t *)(&res), 1U);													// Opzione Macchina senza pulsanti
		gOrionConfVars.NO_Pulsanti = (res) ? true : false;
		
#if MULTI_CYCLE
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MultiCiclo]), (uint8_t *)(&res), 1U);													// Multi ciclo cash in macchina a pulsanti
		gOrionConfVars.MultiCycle = (res) ? true : false;
	if (gOrionConfVars.NO_Pulsanti == true) gOrionConfVars.MultiCycle = false;
#endif
		
	for (r = (FIRSTSELEZ - 1); r < LASTSELEZ; r++)
	{
		ReadEEPI2C(EEPConfVMCAddr(EE_SelOptA[r]), (uint8_t *)(&sSelParam_A[r].SelFlags), 1U);										// Read Byte opzioni per ogni erogazione
	}
	r = 0;
	if ((sSelParam_A[0].SelFlags & BIT_INH_SEL) == BIT_INH_SEL)
	{
		gVmcStatusIoT.Erogaz_1_Inh = true;
		r++;
	}
	if ((sSelParam_A[1].SelFlags & BIT_INH_SEL) == BIT_INH_SEL)
	{
		gVmcStatusIoT.Erogaz_2_Inh = true;
		r++;
	}
	if ((sSelParam_A[2].SelFlags & BIT_INH_SEL) == BIT_INH_SEL)
	{
		gVmcStatusIoT.Erogaz_3_Inh = true;
		r++;
	}
	if ((sSelParam_A[3].SelFlags & BIT_INH_SEL) == BIT_INH_SEL)
	{
		gVmcStatusIoT.Erogaz_4_Inh = true;
		r++;
	}
	if (r == 4)
	{
		//-- Visualizza Nessuna Erogazione Abilitata --
		Set_fVMC_Inibito;
	}	
	else
	{
		ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz1_Out1_Num]),	(uint8_t *)(&gVars.ddcmpbuff[0]), 8U);								// Outputs associati alle erogazioni
		sSelParam_A[0].OutNum_1 = gVars.ddcmpbuff[0];
		sSelParam_A[0].OutNum_2 = gVars.ddcmpbuff[1];
		sSelParam_A[1].OutNum_1 = gVars.ddcmpbuff[2];
		sSelParam_A[1].OutNum_2 = gVars.ddcmpbuff[3];
		sSelParam_A[2].OutNum_1 = gVars.ddcmpbuff[4];
		sSelParam_A[2].OutNum_2 = gVars.ddcmpbuff[5];
		sSelParam_A[3].OutNum_1 = gVars.ddcmpbuff[6];
		sSelParam_A[3].OutNum_2 = gVars.ddcmpbuff[7];
		ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[CleanFilter_OutNum]), (uint8_t *)(&sSelParam_A[0].CleanFilterOut), 1U);					// Output associato alla pulizia filtro

		//ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz1_FlussimNum]), (uint8_t *)(&gVars.ddcmpbuff[0]), 4U);								// Flussimetro associato all'erogazione

		sSelParam_A[0].FlussNum = MIN_FLUSSNUM;																							// Flussimetro associato all'erogazione
		sSelParam_A[1].FlussNum = MIN_FLUSSNUM;
		sSelParam_A[2].FlussNum = MIN_FLUSSNUM;
		sSelParam_A[3].FlussNum = MIN_FLUSSNUM;
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_1]), (uint8_t *)(&sSelParam_A[0].FlowCounterNominale), 2U);
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_2]), (uint8_t *)(&sSelParam_A[1].FlowCounterNominale), 2U);
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_3]), (uint8_t *)(&sSelParam_A[2].FlowCounterNominale), 2U);
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_4]), (uint8_t *)(&sSelParam_A[3].FlowCounterNominale), 2U);
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[TIMESelez_1]), (uint8_t *)(&sSelParam_A[0].TimeErogazione), 2U);
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[TIMESelez_2]), (uint8_t *)(&sSelParam_A[1].TimeErogazione), 2U);
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[TIMESelez_3]), (uint8_t *)(&sSelParam_A[2].TimeErogazione), 2U);
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[TIMESelez_4]), (uint8_t *)(&sSelParam_A[3].TimeErogazione), 2U);
		sSelParam_A[0].TimeErogazione = (sSelParam_A[0].TimeErogazione * OneSec);
		sSelParam_A[1].TimeErogazione = (sSelParam_A[1].TimeErogazione * OneSec);
		sSelParam_A[2].TimeErogazione = (sSelParam_A[2].TimeErogazione * OneSec);
		sSelParam_A[3].TimeErogazione = (sSelParam_A[3].TimeErogazione * OneSec);
		r = 0;
		if (gOrionConfVars.NO_Pulsanti == true)
		{
			if ((sSelParam_A[0].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if ((sSelParam_A[0].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[0].FlowCounterNominale > MAX_FLOW_SEL))
				{
					sSelParam_A[0].SelFlags |= BIT_INH_SEL;
					r = 0x0f;																// Macchina Fuori Servizio: impulsi non programmati
				}
			}
			else
			{
				if (sSelParam_A[0].TimeErogazione == 0)
				{
					sSelParam_A[0].SelFlags |= BIT_INH_SEL;
					r = 0x0f;																// Macchina Fuori Servizio: : timer erogazione non programmato
				}
			}
			if ( (sSelParam_A[0].OutNum_1 == 0) && (sSelParam_A[0].OutNum_2 == 0))
			{
				sSelParam_A[0].SelFlags |= BIT_INH_SEL;
				r = 0x0f;																	// Macchina Fuori Servizio: : uscite non programmate
			}
			//-- Nella macchina senza pulsanti inibite Selez 2, 3 e 4 --
			sSelParam_A[1].SelFlags |= BIT_INH_SEL;
			sSelParam_A[2].SelFlags |= BIT_INH_SEL;
			sSelParam_A[3].SelFlags |= BIT_INH_SEL;
		}
		else
		{
			//--    Selez  1 ---
			if ( ((sSelParam_A[0].OutNum_1 == 0) && (sSelParam_A[0].OutNum_2 == 0))	)
			{
				sSelParam_A[0].SelFlags |= BIT_INH_SEL;
				r |= 0x01;
			}
			if ((sSelParam_A[0].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if ((sSelParam_A[0].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[0].FlowCounterNominale > MAX_FLOW_SEL))
				{
					sSelParam_A[0].SelFlags |= BIT_INH_SEL;
					r |= 0x01;
				}
			}
			else
			{
				if (sSelParam_A[0].TimeErogazione == 0)
				{
					sSelParam_A[0].SelFlags |= BIT_INH_SEL;
					r |= 0x01;
				}
			}
			//--    Selez  2 ---
			if ( ((sSelParam_A[1].OutNum_1 == 0) && (sSelParam_A[1].OutNum_2 == 0))	)
			{
				sSelParam_A[1].SelFlags |= BIT_INH_SEL;
				r |= 0x02;
			}
			if ((sSelParam_A[1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if ((sSelParam_A[1].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[0].FlowCounterNominale > MAX_FLOW_SEL))
				{
					sSelParam_A[1].SelFlags |= BIT_INH_SEL;
					r |= 0x02;
				}
			}
			else
			{
				if (sSelParam_A[1].TimeErogazione == 0)
				{
					sSelParam_A[1].SelFlags |= BIT_INH_SEL;
					r |= 0x02;
				}
			}
			//--    Selez  3 ---
			if ( ((sSelParam_A[2].OutNum_1 == 0) && (sSelParam_A[2].OutNum_2 == 0))	)
			{
				sSelParam_A[2].SelFlags |= BIT_INH_SEL;
				r |= 0x04;
			}
			if ((sSelParam_A[2].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if ((sSelParam_A[2].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[0].FlowCounterNominale > MAX_FLOW_SEL))
				{
					sSelParam_A[2].SelFlags |= BIT_INH_SEL;
					r |= 0x04;
				}
			}
			else
			{
				if (sSelParam_A[2].TimeErogazione == 0)
				{
					sSelParam_A[2].SelFlags |= BIT_INH_SEL;
					r |= 0x04;
				}
			}
			//--    Selez  4 ---
			if ( ((sSelParam_A[3].OutNum_1 == 0) && (sSelParam_A[3].OutNum_2 == 0))	)
			{
				sSelParam_A[3].SelFlags |= BIT_INH_SEL;
				r |= 0x08;
			}
			if ((sSelParam_A[3].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if ((sSelParam_A[3].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[0].FlowCounterNominale > MAX_FLOW_SEL))
				{
					sSelParam_A[3].SelFlags |= BIT_INH_SEL;
					r |= 0x08;
				}
			}
			else
			{
				if (sSelParam_A[3].TimeErogazione == 0)
				{
					sSelParam_A[3].SelFlags |= BIT_INH_SEL;
					r |= 0x08;
				}
			}
		}
		if (r == 0x0f)
		{
	#if DBG_NO_PARAM
		if (gOrionConfVars.NO_Pulsanti == true)
		{
			if ((sSelParam_A[0].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if ((sSelParam_A[0].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[0].FlowCounterNominale > MAX_FLOW_SEL))
				{
					sSelParam_A[0].FlowCounterNominale = 10;																		// DEBUG
				}
			}
			else
			{
				if (sSelParam_A[0].TimeErogazione == 0)
				{
					sSelParam_A[0].TimeErogazione = THIRTYSEC;																		// DEBUG
				}
			}
			if ( (sSelParam_A[0].OutNum_1 == 0) && (sSelParam_A[0].OutNum_2 == 0))
			{
				sSelParam_A[0].OutNum_1 = 1;																						// DEBUG
				sSelParam_A[0].OutNum_2 = 0;																						// DEBUG
			}
			sSelParam_A[0].SelFlags &= (~BIT_INH_SEL);

		}
		else
		{
			if ((sSelParam_A[0].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[0].FlowCounterNominale > MAX_FLOW_SEL)) sSelParam_A[0].FlowCounterNominale = 10;				// DEBUG
			if ((sSelParam_A[1].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[1].FlowCounterNominale > MAX_FLOW_SEL)) sSelParam_A[1].FlowCounterNominale = 20;				// DEBUG
			if ((sSelParam_A[2].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[2].FlowCounterNominale > MAX_FLOW_SEL)) sSelParam_A[2].FlowCounterNominale = 30;				// DEBUG
			if ((sSelParam_A[3].FlowCounterNominale < MIN_FLOW_SEL) || (sSelParam_A[3].FlowCounterNominale > MAX_FLOW_SEL)) sSelParam_A[3].FlowCounterNominale = 40;				// DEBUG
			sSelParam_A[0].OutNum_1 = 1;
			sSelParam_A[0].OutNum_2 = 0;
			sSelParam_A[1].OutNum_1 = 2;
			sSelParam_A[1].OutNum_2 = 0;
			sSelParam_A[2].OutNum_1 = 3;
			sSelParam_A[2].OutNum_2 = 0;
			sSelParam_A[3].OutNum_1 = 4;
			sSelParam_A[3].OutNum_2 = 0;
			sSelParam_A[0].FlussNum = FLUSS1;
			sSelParam_A[1].FlussNum = FLUSS1;
			sSelParam_A[2].FlussNum = FLUSS1;
			sSelParam_A[3].FlussNum = FLUSS1;
			sSelParam_A[0].SelFlags &= (~BIT_INH_SEL);
			sSelParam_A[1].SelFlags &= (~BIT_INH_SEL);
			sSelParam_A[2].SelFlags &= (~BIT_INH_SEL);
			sSelParam_A[3].SelFlags &= (~BIT_INH_SEL);
		}
	#else
			//--  Allarme macchina senza parametri ---
			gVmcStatusIoT.NoParam = true;
			gVmcStatusIoT.StatusChang_IN++;
			Set_VMC_NoParam;
			Set_fVMC_Inibito;
	#endif
		}
	}
	
	
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	// ------------------------------------------------------------
	// --  DEFAULT    VARI   --- 
	// ------------------------------------------------------------
	TestPaymentSettings();																		// Verifica congruita' impostazioni e imposta valori predefiniti
	
	
	// Le PSW dei Liv 2 e 3 non possono essere 0 o > 9999
	addrbuff = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_1]);
	ReadEEPI2C(addrbuff, (uint8_t *)(&Temp), 2U);												// Leggo PSW Liv 1 dalla EE
	if ((Temp > 9999)) {
		Temp = 0;																				// Default PSW Liv 0
		WriteEEPI2C(addrbuff, (uint8_t *)(&Temp), 2U);											// Scrivo nuovo valore PSW in EE
	}
	addrbuff = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_2]);
	ReadEEPI2C(addrbuff, (uint8_t *)(&Temp), 2U);												// Leggo PSW Liv 2 dalla EE
	if ((Temp == 0) || (Temp > 9999)) {
		Temp = 1111;																			// Default PSW Liv 2
		WriteEEPI2C(addrbuff, (uint8_t *)(&Temp), 2U);											// Scrivo nuovo valore PSW in EE
	}
	addrbuff = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_3]);
	ReadEEPI2C(addrbuff, (uint8_t *)(&Temp), 2U);												// Leggo PSW Liv 3 dalla EE
	if ((Temp == 0) || (Temp > 9999)) {
		Temp = 1234;																			// Default PSW Liv 3
		WriteEEPI2C(addrbuff, (uint8_t *)(&Temp), 2U);											// Scrivo nuovo valore PSW in EE
	}
	
#if PIN_RMFATECH	
	Pin1 = 11111;
	Pin2 = 22222;
	Gestore = 11111;
#endif	

#if DBG_DISABLE_LOG
	SystOpt4 = 0;
#endif
	
	
	// Controllo stato Audit Estesa per controllo memoria Full
	TestAuditExtFullStatus();
	
	//-- Controllo validita' dati per LOG File --
	ReadEEPI2C(LogFileSize, (uint8_t *)(&MemoryBytes[0]), 3U);									// Verifico che in LogFileSize ci sia la dimensione del buffer LOG in EE
	Temp = (MemoryBytes[0] | MemoryBytes[1] << 8);												// Mantengo questi bytes per utilizzare quanto gia' implementato per la serie Orion
	if ((Temp != LOG_FILE_BUF_LEN) || (MemoryBytes[2] != 0))
	{
		InitLogBufferPointers(true);															// Set Size Buffer LOG in EE azzerandone il contenuto
	}

	
	OpMode = PAR;																				// Per compatibilita' con fw esistente della serie Orion PK3
	if (gVMC_ConfVars.ProtMDB == true) {
		OpMode = MDB;
	}
	
	ReadEEPI2C(IICEEPConfigAddr(EEOpMode), (uint8_t *)(&res), sizeof(SizeEEOpMode));			// Leggo attuale contenuto di EEOpMode
	if (res != OpMode) {
		WriteEEPI2C(IICEEPConfigAddr(EEOpMode), &OpMode, sizeof(OpMode));						// Registro OpMode nella EEPROM
		SendConfToEEprom(true);																	// Aggiorno solo la parte di Rev sw della config in EEPROM
	}

	
	// Controllo versione software del prodotto
	datalen = 4;
	strncpy((char*)&gVars.ddcmpbuff[0], kProductSWVersionStr, datalen);							// Copio in ddcmpbuff la revisione sw attuale
	ReadEEPI2C(ProductSwRevision, (uint8_t *)(&gVars.ddcmpbuff[datalen]), datalen);				// Leggo Area con la revisione software
	for (r=0; r<datalen; r++) {
		if (gVars.ddcmpbuff[r] != gVars.ddcmpbuff[r+datalen]) {
			WriteEEPI2C(ProductSwRevision, (uint8_t *)(&gVars.ddcmpbuff), datalen);				// Registro revisione sw nella eeprom
			SendConfToEEprom(true);																// Aggiorno solo la parte di Rev sw della config in EEPROM
			SwRevUpdate_Events();																// Nuova Revisione Software: inserisco Evento
			break;
		}
	}

	memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));								// Azzero buffer ddcmp

	
// ========================================================================
// ========    Carico Revalue in RAM          =============================
// ========================================================================

	//MR19 ReadRefundValues(&RefundValues, &DataRefundValues, TipoRefundCredito);
	//MR19 ReadRefundValues(&RefundFreeCredit, &DataRefundFC, TipoRefundFreeCred);
	//MR19 ReadRefundValues(&RefundFreeVend, &DataRefundFV, TipoRefundFreeVend);
	//MR19 ReadRefundValues(&RefundCashCB, &DataRefundCashCB, TipoRefundCaricamBlk);
  
// ========================================================================
// ========    Inizializzazione Pointers Memo Log =========================
// ========================================================================

	if (SystOpt4 & TxIrDALog)
	{
		if ( (SystOpt4 & MaskTipoLog) != LogTipo0 &&  (SystOpt4 & MaskTipoLog) != LogTipo1 
				&& (SystOpt4 & MaskTipoLog) != LogTipo2)
		{
			SystOpt4 &= ~TxIrDALog;																// Se impostato tipo log non previsto non effettuo alcuna registrazione
		}
		gVars.pnt_data = 0;
		gVars.pnt_timing = OffsetDatiLog;
		SystOpt4 |= DiscardFirstRx;																// Scarta il primo chr ricevuto dall'IrDA appena si attiva la seriale
	}

#if DEB_NTC_ERROR
	if (SystOpt4 & MEM_GENERIC_LOG)
	{
		InitLogBufferPointers(true);
	}
#endif	

// ========================================================================
// ========    Inizializzazione DDCMP             =========================
// ========================================================================
	
	if ((gVMC_ConfVars.ExecutiveSLV | gVMC_ConfVars.ProtMDBSLV) == false)						// IrDA attivabile quando NON e' ne' Executive Slave ne' MDB Slave 
	{
		Start_IrDA();
		Init_DDCMP_Protocol();
	}
	
	
// ========================================================================
// ========    Inizializzazione UART  Varie   =============================
// ========================================================================
/*
---------------------------------------------------------------------------------------------------------| 
 UART Fisica   |    LPUART1   |  USART-1 |   USART-2    |   USART-3    |      UART-4    |    UART-5      |
---------------|-----------------------------------------------------------------------------------------|
  CPU  CESAM   | Espansione   |  IrDA    |    MIFARE    | RS485 Touch  | Telemetria-EXP |   MDB Master   | 
  Rev. A       |              |          |              |              |                |                |
---------------|-----------------------------------------------------------------------------------------|
*/

	if (gVMC_ConfVars.ProtMDBSLV == false)
	{
		MIFARE_COMM_Init();																		// Inizializza la COMM per l'antenna MIFARE
	}
	if (gVMC_ConfVars.ExecutiveSLV == true)
	{
		// ---   Executive Slave   -----
	    MIFARE_OFF();																			// Spengo PN512
		gVMC_ConfVars.LocalIrDA = false;														// NON usare IrDA locale
		OpMode = EXEC_Slave;
	} 
	else if (gVMC_ConfVars.Gratuito == true)
	{
		OpMode = Gratis;
		gVMC_ConfVars.LocalIrDA	= true;															// Posso usare IrDA locale
		MIFARE_COMM_DeInit();																	// Tolgo l'ulteriore tensione proveniente dai pin UART 
	}
	else if (gVMC_ConfVars.PresMifare == true)													// Configurazione prevede presenza Mifare reader
	{
		Set_fExactChange;
		gOrionKeyVars.VariazExCh = true;
		//EnableMifare();
		response = PN512_mif_init(MIFARE_BAUD_FAST, DDRPN);  									// Reinizializzo Mifare Reader
		if (response != Resp_OK)
		{
			gOrionKeyVars.AntennaRespFail = true;
		}
		if (gVMC_ConfVars.ProtMDB == false)
		{
			OpMode = EXECUTIVE;																	// Funziona tutto anche con OpMode = 0 che e' Executive Master
		}
	}
	else
	{
		if (gVMC_ConfVars.ProtMDBSLV == false) 
		{
			gVMC_ConfVars.LocalIrDA	= true;														// Posso usare IrDA locale se non sono USD Slave
		}
	}

// ========================================================================
// ========    Inizializzazione Selettore  Parallelo          =============
// ========================================================================

	if ((gVMC_ConfVars.PresMifare == true) || (gVMC_ConfVars.ProtMDB == true))
	{
		Sel24_init();																			// Init Selettore Parallelo
	}
	

// ========================================================================
// ========    Inizializzazione UART 3 RS485  Touch           =============
// ========================================================================

	RS485_Comm_Init();
	//SetPC_Comm(Baud57600);
	//Touch_Visual_Init();
	
// ========================================================================
// ========    Inizializzazione UART 2  Master EXECUTIVE/MDB   ============
// ========================================================================

	if ((gVMC_ConfVars.ProtMDB == true) && (gVMC_ConfVars.ProtMDBSLV == false))
	{
		Master_MDB_Comm_Init();																	// Seriale per Master MDB
	}
	else
	{
		Master_Exec_Comm_Init();																// Seriale per Master Executive
	}
	gVars.valWaitRTx = TwoSec;



#if IOT_PRESENCE
// ========================================================================
// ========    Inizializzazione UART 4      I o T              ============
// ========================================================================

	Reset_WiFi_Module();
	IoT_COMM_Init();
	Gtwy_Comm_Init();
	//IoT_SendBlock(&gVars.ddcmpbuff[0], 1U);			// DEBUG Invio un chr

#endif
	
	
// ========================================================================
// ========    Sessione Monitor                   =========================
// ========================================================================
//	Controllo se attivare la funzione Monitor
  
	//TestMonitor();		MR19 Spostato controllo nel Main perche' con USB CDC e' piu' complesso far ripartire il log protocolli al power up
	
// ========================================================================
// ========           Collaudo  CPU e I/O  PASVENS             ============
// ========================================================================
	if (NewCPU == true)
	{
		Collaudo_Part_1();
	}
}

/*--------------------------------------------------------------------------------------*\
Method: CheckMachine_HW_Config
	Configurazione  Hardware  della  Macchina.	
	Inizializzazione Parametri per espansione LCD_Keyboard.
	Non e' ancora stato fatto il test della EE per sapere se i param letti sono corretti,
	ma non posso fare altro per poter utilizzare l'LCD.
	Chiamata all'inizo del Reset e successivamente alla lettura dalla EE di una eventuale
	nuova configurazione macchina.
	In questo secondo caso abilito comunicazione con LCD remoto se la configurazione e'
	stata modificata da Locale a remoto; se e' stata cambiata da remoto a locale eseguo la
	LCD_Init perch' prima non era stata effettuata 
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
static uint8_t	CheckMachine_HW_Config(bool AfterConfigCheck)
{
	bool 	resp;
	uint8_t	Error;
	
	BankSelect(0x00);																									// Select Bank0 eeprom 1
	Error = ERR_OK;
	
	//--  LCD Type ---
	gOrionConfVars.LCD_Type = LCD_2x16;																					// Fisso 2x16 perche' sono abituati a vedere questo durante il funzionamento
	
/*
	resp = ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptLCD_Type]), (uint8_t *)(&gOrionConfVars.LCD_Type), 1U);			// Tipo LCD
	if (resp == false)
	{
		Error = ERR_READ_EE;
		return Error;
	}
	else
	{
		if (gOrionConfVars.LCD_Type > MAX_LCD_TYPE)
		{
			gOrionConfVars.LCD_Type = LCD_4x20;
			Error |= ERR_NO_KEYBOARD;
		}
	}
*/

	//--  KeyBoard Type ---
	resp = ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[EE_Keyb_Type]), (uint8_t *)(&gOrionConfVars.KeyB_Type), 1U);			// Tipo Tastiera
	if (resp == false)
	{
		Error = ERR_READ_EE;
	}
	else
	{
		if (gOrionConfVars.KeyB_Type > MAX_TASTIERA)
		{
			//gOrionConfVars.KeyB_Type = KEYB_METALTARGHE;
			gOrionConfVars.KeyB_Type = KEYB_NOVA;																		// Default Nova perche' e' la tastiera che ho dato ad ALP per tests
			Error |= ERR_NO_KEYBOARD;
		}
	}
return Error;
}