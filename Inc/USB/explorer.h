/**
  ******************************************************************************
  * @file    USB_Host/MSC_Standalone/Src/explorer.h 
  * @author  MCD Application Team
  * @brief   header file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license SLA0044,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        http://www.st.com/SLA0044
  *
  ******************************************************************************
  */

#ifndef __EXPOLRER_H__
#define __EXPOLRER_H__

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>

#include "stdio.h"
#include "usbh_core.h"
#include "usbh_msc.h" 
#include "ff.h"
#include "ff_gen_drv.h"
#include "usbh_diskio_dma.h"
#include <stdint.h> 

FRESULT Explore_Disk(char *path, uint8_t recu_level);

#endif

