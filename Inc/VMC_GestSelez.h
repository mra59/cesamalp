/****************************************************************************************
File:
    VMC_GestSelez.h

Description:
    Include file con definizioni e strutture per gestire la richiesta di selezioni

History:
    Date       Aut  Note
    Mag 2019	MR   

 *****************************************************************************************/

#ifndef __VMC_GestSelez_H
#define __VMC_GestSelez_H

#define		TIMEBUZ_VEND_OK		(OneSec+Milli500)
#define		TIMEBUZ_VEND_FAIL	FourSec

void  			Gestione_Selezioni(void);
bool			DA_in_Stby(void);
void 			VendAuthorResult(bool Approved);
static	bool  	IfExecSlaveSelezEnabled(uint8_t NumeroSelez);
void  			SelezPrenotata(void);
void  			ClearSelezPrenotata(void);
void 			SetPaymentConfig(void);
void  			CheckCreditForSelection(void);
bool  			IsRemainingLow(uint8_t NumSel);

#endif		// __VMC_GestSelez_H


