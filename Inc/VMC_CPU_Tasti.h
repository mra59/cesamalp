/****************************************************************************************
File:
    VMC_CPU_Tasti.h

Description:
    Include file con definizioni e strutture per la gestione dei tasti da 
    display touch Stone e da Tastiera a membrana.

History:
    Date       Aut  Note
    Apr 2016	Abe/MR  

 *****************************************************************************************/

#ifndef __VMC_CPU_Tasti_H_
#define __VMC_CPU_Tasti_H_

#include "Orion.h"

void  	scan_buffer(uint8_t chr);
void  	Gestione_Touch_Tastiera(void);
void  	VisParamTouch(void);
void 	Restart_Da_Prog(void);
bool  	Preselez_Zucchero(bool increm);
bool 	Preselez_NoBicc(bool NoBicc);
void 	CheckPulsSelezGratuita(void);
void 	ResetToutProgram(void);
bool  	IsToutProgramElapsed(void);

#if DEB_VISTIME
	bool  Is_First_Puls_Selez_Premuto(void);
#endif


/****************************************************************/
//macro definitions include file

#define MAX_numchar		32
#define MAX_TASTI		32
#define MAX_WAIT_TOUCH	Milli250

/****************************************************************/
//typedef include file
//scan string AA 78 <Touch_Code> CC 33 C3 3C
typedef enum{
	SCAN_IDLE,
	SCAN_START_RX,
	SCAN_WAIT_MSB_TOUCH,
	SCAN_WAIT_LSB_TOUCH,
	SCAN_END_1,
	SCAN_END_2,
	SCAN_END_3,
	SCAN_END_4,
	SCAN_VALID,
	NEWSCAN_Idle,
	NEWSCAN_Second_SyncChr,
	NEWSCAN_MsgCounter,
	NEWSCAN_MsgCommand,
	NEWSCAN_Data,
	NEWSCAN_Valid
}SCAN_STAT;

typedef struct{
	uint32_t time_tasto;
	uint16_t valore_tasto;
}TASTI_TOUCH;

extern volatile TASTI_TOUCH touch_button[MAX_TASTI];

// --- Definizioni Vecchio Touch ----
#define OldHeader_NumByte	6																	// Numero bytes header vecchio touch
#define Pos_OldFirstChr		6																	// Posizione primo carattere della stringa da inviare
#define OldTrailer_NumByte	4																	// Numero bytes chiusura stringa vecchio touch


// --- Comandi New Touch ----
#define	Write_REG			0x80																// Scrittura Registro Touch
#define	Read_REG			0x81																// Lettura   Registro Touch
#define	Write_VAR			0x82																// Scrittura Variabile Touch
#define	Read_VAR			0x83																// Lettura 	 Variabile Touch
#define	PIC_ID				0x03																// Registro  Pagina attualmente visualizzata
#define	RTC_NOW				0x1f																// Registro  Orologio

#define	NumByte_Header		3																	// Lunghezza dell'header del buffer da trasmettere 
#define	PIC_Swicth_CMD_Len	4																	// Lunghezza del comando per cambaire pagina
#define	Button_Addr			0x0001																// Address Variabile che contiene il Codice del tasto premuto


// -- Stringa  ricevuta dal New touch ---
#define PIC_ID_MSB			2																	// Posizione nel buffer ricevuto del MSB Pagina attualmente visualizzata
#define PIC_ID_LSB			3																	// Posizione nel buffer ricevuto del LSB Pagina attualmente visualizzata
#define Butt_MSB			3																	// Posizione nel buffer ricevuto del MSB Codice Pulsante premuto
#define Butt_LSB			4																	// Posizione nel buffer ricevuto del LSB Codice Pulsante premuto

// -- Stringa  trasmessa al New touch ---
#define Pos_R3				0																	// Posizione Byte 1 address Display touch
#define Pos_RA				1																	// Posizione Byte 2 address Display touch
#define Pos_ByteCount		2																	// Posizione counter numero di bytes da trasmettere
#define Pos_CMD				3																	// Posizione del Comando da trasmettere
#define Pos_VarAddr_MSB		4																	// Posizione del MSB dell'address Variant
#define Pos_VarAddr_LSB		5																	// Posizione del LSB dell'address Variant
#define Pos_FirstChrStrng	6																	// Posizione primo carattere della stringa

#define Pos_REG_Addr		4																	// Posizione dell'indirizzo del registro da elaborare
#define Pos_FirstChrREG		5																	// Posizione del primo chr da inviare al registro


typedef struct {
	Tmr32Local	DelayTastoTastiera;																// Timer di scansione Main
} TempiTouch;

#define	TOUTPRIMOTASTO	FIVESEC

#define	Tasto_Meno_Touch 	124
#define	Tasto_Piu_Touch 	125

#define	  FIRSTSELEZ  		Selez_01
#define	  LASTSELEZ  		Selez_04




#endif		// __VMC_CPU_Tasti_H_
