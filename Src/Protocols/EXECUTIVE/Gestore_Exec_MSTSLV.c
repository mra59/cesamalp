/****************************************************************************************
File:
    Gestore_Exec_MSTSLV.c

Description:
    File con le funzioni di controllo per le funzioni di Master/Slave Executive.
    

History:
    Date       Aut  Note
    Giu 2017	MR   

*****************************************************************************************/

#include <stdbool.h>
#include <string.h>

#include "ORION.H"
#include "ExecSlave.h"
#include "ExecMaster.h"
#include "Gestore_Exec_MSTSLV.h"
#include "VMC_CPU_HW.h"
#include "Monitor.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/




/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

MSVars 	sGestMSVars;

/*---------------------------------------------------------------------------------------------*\
Method: GestMS_Init
	Funzione chiamata la prima volta che si esegue il MAIN
 
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void GestMS_Init(void) {
	
	memset(&sGestMSVars, 0, sizeof(sGestMSVars));
	sGestMSVars.cd.scalingFact 	= 1;
	UnitScalingFactor 			= sGestMSVars.cd.scalingFact;
	sGestMSVars.masterUnavail 	= false;
	sGestMSVars.Phase 			= kCredBeforeVendReq;
}

// ========================================================================
// ========    GESTORE  EXECUTIVE  MASTER / SLAVE  TASK     ===============
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: GestoreMS_Task
	Funzione chiamata continuamente dal MAIN.
	Elabora le risposte degli Slave VMC Locale e VMC2 in base al comando inviato dalla RR.
	Ritrasmette la risposta dello Slave alla RR.
	
	In st-by RR comunica con Slave Locale mentre il VMC2 e' pilotato dal Master Executive.
	
	Quando il VMC Locale e' in selezione, se Resto Subito = N, il Master Executive tiene vivo il VMC2
	con il solo STATUS (anche nuovo Credito se dovesse variare).
	Se il VMC Locale e' in Resto Subito, durante la selezione anche il VMC2 puo' richiedere una
	selezione e la RR viene "collegata" al VMC2 fino al termine.
	Il GestoreMS analizza la comunicazione RR-VMC2 per capire quando e' terminata e ritorna alla
	comunicazione con il VMC Locale, riattivando il Master Executive verso il VMC2.
	
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void GestoreMS_Task(void) 
{
	if (sGestMSVars.fRxChrDaVMC == false) {														// Nessuna risposta dai VMC (Locale o VMC2)
		if (sGestMSVars.RRCmd_Cnt > MaxRRNoResp) {												// Counter CMD da RR Over senza dare risposte (dovrebbe accadere solo con VMC2)
			SwitchToLocalVMC();																	// Reindirizzo RR a VMC Locale
			sGestMSVars.Phase = kCredBeforeVendReq;
		}
		return;
	}		
	// -- Elaboro risposta dello Slave alla RR ---
	sGestMSVars.fRxChrDaVMC = false;
	sGestMSVars.RRCmd_Cnt = 0;
	if (sGestMSVars.ByteDaVMC == kSlave_PNACK) {												// PNACK dal VMC: lo ritrasmetto alla RR ed esco
		ExecSlaveTxChar(sGestMSVars.ByteDaVMC);
		return;
	}
	if (sGestMSVars.fRRToVMC2 == false) {														// Comunicazioni RR-VMC Locale
		
		// ========================================================================
		// ========    Comunicazioni  RR <---> VMC  Locale ========================
		// ========================================================================

		switch(sGestMSVars.CMD) {																// Elaboro comando della RR dopo che il VMC Locale ha risposto

			// ==================================================================================
			case kExecRRCmdStatus:
				if (sGestMSVars.Phase == kWaitVend) {											// Ricevuto STATUS dalla RR dopo una Vend Req, equivale a VEND DENIED
					sGestMSVars.Phase = kCredBeforeVendReq;
				} else {
					if (sGestMSVars.fVMC2_FV_Req == true) {										// Free Vend Request dal VMC2
						sGestMSVars.fVMC2_FV_Req = false;
						SwitchToVMC2();
						sGestMSVars.Phase = kWaitVend;
						sGestMSVars.ByteDaVMC = kExecVMCStatFreeVendRq;
					} else {																	// Determina risposta INH o ENABLE dei VMC
						if (fVMC_Inibito && (fVMC2_Inibito || fVMC2_NoLink)) {					// Inibire RR
							sGestMSVars.ByteDaVMC = kExecVMCStatVendInhibit;
						} else {
							sGestMSVars.ByteDaVMC = kExecACK;									// Abilitare RR
						}
					}
				}
				break;
		
			// ==================================================================================
			case kExecRRCmdCredit:
				if (sGestMSVars.ByteDaVMC == kExecNoVendRq) {									// VMC Locale NO Vend Req (0xFE)
					if (sGestMSVars.fVMC2_SelezReq == true) {									// Vend Request da VMC2
						sGestMSVars.fVMC2_SelezReq = false;
						SwitchToVMC2();
						sGestMSVars.Phase = kWaitVend;
						sGestMSVars.ByteDaVMC = sGestMSVars.VMC2_SelValue;
					}
				} else {																		// VMC Locale Vend Request
					sGestMSVars.Phase = kWaitVend;
				}
				break;
		
			// ==================================================================================
			case kExecRRCmdVend:
				sGestMSVars.Phase = kCredBeforeVendReq;											// Qui arrivo con la risposta al Vend del VMC (OK o FAIL), anche con Resto Subito
				ClrMasterTxStatusOnly();
				break;
		
			// ==================================================================================
			case kExecRRCmdDataSync:
				// Todo vedere se fare aggiornare qui il credito o solo nella ExecSlave
				MSExecSlaveUpdateCreditInfo();													// Aggiorna VMC_CreditoDaVisualizzare, DPP, USF e Ex-Ch
				break;
				
			// ==================================================================================
			case kExecVMCCmdAcceptData:
			case kExecRRCmdNACK:
			case kExecVMCCmdUnknown:
				break;		

			default:
				break;
		}
		
	} else {
		// ========================================================================
		// ========    Comunicazioni  RR <---> VMC 2      =========================
		// ========================================================================
	
		switch(sGestMSVars.CMD) {																// Elaboro comando della RR dopo che il VMC Locale ha risposto

			// ==================================================================================
			case kExecRRCmdStatus:
				if (sGestMSVars.Phase == kWaitVend) {											// Ricevuto STATUS dalla RR dopo una Vend Req, equivale a VEND DENIED
					SwitchToLocalVMC();
					sGestMSVars.Phase = kCredBeforeVendReq;
					sGestMSVars.ByteDaVMC = kExecACK;											// Rispondo ACK alla RR
				}
				break;
		
			// ==================================================================================
			case kExecRRCmdCredit:
				if (sGestMSVars.ByteDaVMC != kExecNoVendRq) {									// VMC2 Vend Req
					sGestMSVars.Phase = kWaitVend;												// Vend Request da VMC2
				}
				break;
	
			// ==================================================================================
			case kExecRRCmdVend:
				sGestMSVars.Phase = kCredBeforeVendReq;											// Qui arrivo alla risposta al Vend del VMC (OK o FAIL)
				SwitchToLocalVMC();																// RR --> VMC Locale e restart STATUS master exec interno verso VMC2
				break;
			
			// ==================================================================================
			case kExecRRCmdDataSync:
				// Todo vedere se fare aggiornare qui il credito o nella ExecSlave
				MSExecSlaveUpdateCreditInfo();													// Aggiorna VMC_CreditoDaVisualizzare, DPP, USF e Ex-Ch
				break;
					
			// ==================================================================================
			case kExecVMCCmdAcceptData:
			case kExecRRCmdNACK:
			case kExecVMCCmdUnknown:
				break;		

			default:
				break;
		}	
	}
	ExecSlaveTxChar(sGestMSVars.ByteDaVMC);													// Ritrasmetto alla RR la risposta del VMC
}

/*--------------------------------------------------------------------------------------*\
Method:	TraceCMDRR
	Rx data dalla RR (funzione chiamata in IRQ).
	Si salva il chr ricevuto in sGestMSVars.ByteDaGettRR perche' sia ritrasmesso al
	VMC di destinazione nel Main (funzione Slave_Exec_Task).
	Traccia i comandi dalla RR per analizzare inizio/fine vend con VMC2.
	
Parameters:
	data	->	character received
\*--------------------------------------------------------------------------------------*/

static void TraceCMDRR(uint8_t data) {
	
	if(ExecCharIsCmd(data) == false) {
		data&= kExecDataMask;
		switch(sGestMSVars.dataCount++) {
			case 0:	sGestMSVars.cd.baseUnitValue= data;								break;
			case 1: sGestMSVars.cd.baseUnitValue+= data*16;							break;
			case 2: sGestMSVars.cd.baseUnitValue+= data*256;						break;
			case 3: sGestMSVars.cd.baseUnitValue+= data*256*16;						break;
			case 4:	sGestMSVars.cd.decimalPoint= data;								break;
			case 5: sGestMSVars.cd.scalingFact=data*16+sGestMSVars.cd.decimalPoint;	break;
			case 6:	sGestMSVars.cd.decimalPoint= data;								break;
			case 7:	sGestMSVars.cd.exactChange= data;								break;
		}
		return;
	}

	// Ricevo un Comando dal Master
	switch (data & kExecDataMask) {
	
		//  ------- 0x31 S T A T U S  -------
		case kExecVMCCmdStatus:
			sGestMSVars.CMD = kExecRRCmdStatus;
			break;

		//  ------- 0x32  C R E D I T  -------
		case kExecVMCCmdCredit:
			sGestMSVars.CMD = kExecRRCmdCredit;
			break;

		//  ------- 0x33  V E N D    -------
		case kExecVMCCmdVend:
			sGestMSVars.CMD = kExecRRCmdVend;
			break;

		case kExecRRCmdAudit: 																	// Non implementato
		case kExecRRCmdSendAuditAddr:															// Non implementato
		case kExecRRCmdSendAuditData:															// Non implementato
		case kExecRRCmdIdentify:																// Non implementato
			break;

		//  ------- 0x38  A C C E P T   D A T A -------
		case kExecRRCmdAcceptData:
			sGestMSVars.CMD = kExecRRCmdAcceptData;
			sGestMSVars.dataCount = 0;
			break;

		//  ------- 0x39    S Y N C   -------
		case kExecRRCmdDataSync:
			sGestMSVars.CMD = kExecRRCmdDataSync;
			if (sGestMSVars.dataCount >= 8) {
				sGestMSVars.cdChanged = true;													// Ricevuta una nuova visualizzata, aggiorno la struttura cd
			}
			break;

		//  ------- 0x3F    N A C K   -------
		case kExecRRCmdNACK:
			sGestMSVars.CMD = kExecRRCmdNACK;
			break;

		default:
			sGestMSVars.CMD = kExecRRCmdUnknown;
			break;																				// Comando sconosciuto
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	MSExecSlaveUpdateCreditInfo
	Aggiorna struttura ci tramite struttura cd se quest'ultima e' stata modificata
	da una nuova visualizzazione dal Master.

	IN:	  - 
	OUT:  - true e aggiornamento struttura "sGestMSVars.ci" se ricevuto nuovo credito,
			altrimenti false
\*--------------------------------------------------------------------------------------*/
static bool MSExecSlaveUpdateCreditInfo(void) {
	
	if (sGestMSVars.cdChanged == false) return false;											// Credito invariato
	if (sGestMSVars.dataCount < 8) return false;												// Buffer credito non corretto

	sGestMSVars.ci.creditVal = sGestMSVars.cd.baseUnitValue*sGestMSVars.cd.scalingFact;
	sGestMSVars.ci.exactChange = sGestMSVars.cd.exactChange;
	if (sGestMSVars.cd.decimalPoint == 0x2) sGestMSVars.ci.dpPosition = 1;
	else if (sGestMSVars.cd.decimalPoint == 0x4) sGestMSVars.ci.dpPosition = 2;
	else if (sGestMSVars.cd.decimalPoint == 0x8) sGestMSVars.ci.dpPosition = 3;
	else sGestMSVars.ci.dpPosition = 0;
	DecimalPointPosition = sGestMSVars.ci.dpPosition;
	UnitScalingFactor = sGestMSVars.cd.scalingFact;

	VMC_CreditoDaVisualizzare = sGestMSVars.ci.creditVal;
	if (sGestMSVars.ci.exactChange) {															// Predispone la flag Exact change
		Set_fExactChange;
	} else {
		Clear_fExactChange;
	}
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method:	RR_ReadyForVMC2Vend
	Restituisce lo stato di RR e VMC Locale per sapere se autorizzare una Vend Req dal 
	VMC2, e inviare solo STATUS quando il VMC Locale e' in selezione e la RR e' in attesa
	della Fine Vend (NO Resto Subito).
	Controlla anche lo stato della RR quando il VMC Locale e' in selezione ma e' impostato
	Resto Subito e la RR e' quindi disponibile per una selezione contemporanea sul VMC2. 
	
	IN:	  - 
	OUT:  - 0: RR ready per accettare Vend Req da VMC2.
			1: Deny Vend Req da VMC2, VMC Locale in attesa autorizzazione alla selezione
			2: VMC Locale in selezione e RR attende fine vend (NO resto subito)
\*--------------------------------------------------------------------------------------*/
uint8_t  RR_ReadyForVMC2Vend(void) {
	
	uint8_t	resp = 0;
	
	if (gVMCState.SelezReq == false) {															// Nessuna Vend Req in corso nel VMC Locale
		return resp;
	} else {
		if (gVMCState.InSelezione == false) {													// SelezReq=1 e InSelezione=0: sono in attesa di VEND/DENIED dalla RR
			return (resp = 1);
		} else {
			if (sGestMSVars.Phase == kWaitVend) {												// SelezReq=1, InSelezione=1 e RR=in Vend: VMC Locale in erogaz e RR attende fine Vend
				return (resp = 2);
			} else {
				return resp;
			}
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	IsSlaveTxSuspendStatus
	Restituisce lo stato della flag di inibizione alla trasmissione dei chr da parte
	dello Slave Locale alla RR.
	
	IN:	  - 
	OUT:  -
\*--------------------------------------------------------------------------------------*/
bool  IsSlaveTxSuspendStatus(void) {
	
	return(sGestMSVars.fSlaveSuspend);
}

/*--------------------------------------------------------------------------------------*\
Method:	GestMS_IsVMC2InVend
	Restituisce un'info sullo stato del VMC2.
	
	IN:	 - 
	OUT: - true se VMC2 ha richiesto una vendita
\*--------------------------------------------------------------------------------------*/
bool  GestMS_IsVMC2InVend(void) {
	
	if (gVMC_ConfVars.ExecMSTSLV == false) return false;										// Executive normale, NO MST/SLV
	if (sGestMSVars.fVMC2_SelezReq || sGestMSVars.fVMC2_FV_Req || sGestMSVars.fRRToVMC2) {		// VMC2 ha richiesto una selezione
		return true;
	} else {
		return  false;
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	GestMS_VMC2_FV_Req
	Funzione chiamata da "ExecMaster" da MAIN: il VMC2 ha richiesto una Free Vend.
	Se il VMC Localer non e' gia' in selezione, disabilito comunicazioni del Master e 
	attendo il primo comando STATUS dalla RR per comunicare la richiesta del VMC2.
	
	IN:		- 
	OUT:	-
\*--------------------------------------------------------------------------------------*/
bool  GestMS_VMC2_FV_Req(void) {

	if (gVMCState.SelezReq == true) return false;												// VMC Locale in selezione
	Set_fVMC_Inibito;																			// Inhibit VMC Locale
	sGestMSVars.fVMC2_FV_Req = true;															// VMC2 richiede una Free Vend alla RR
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method:	GestMS_VMC2_VendReq
	Funzione chiamata da "ExecMaster" da MAIN: il VMC2 ha richiesto una Selezione.
	Se il VMC Locale non e' gia' in selezione, disabilito comunicazioni del Master e 
	attendo il primo comando CREDIT dalla RR per comunicare la richiesta del VMC2.
	
	IN:	 - 
	OUT: - false se selezione da VMC2 non puo' essere trasmessa alla RR
\*--------------------------------------------------------------------------------------*/
bool  GestMS_VMC2_VendReq(uint8_t Selezione) {
	
	if (gVMCState.SelezReq == true) return false;												// VMC Locale in selezione
	Set_fVMC_Inibito;																			// Inhibit VMC Locale
	sGestMSVars.fVMC2_SelezReq 	= true;															// VMC2 richiede una Vend alla RR
	sGestMSVars.VMC2_SelValue 	= Selezione;													// Valore/Numero selezione richiesta dal VMC2
	return  true;
}

/*--------------------------------------------------------------------------------------*\
Method:	Set_VMC2_FV_Req
	Attiva la richiesta di una Free Vend Req dal VMC2.
	
	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
void  Set_VMC2_FV_Req(void) {

	Set_fVMC_Inibito;																			// Inhibit VMC Locale
	sGestMSVars.fVMC2_FV_Req = true;															// VMC2 richiede una Free Vend alla RR
}

/*--------------------------------------------------------------------------------------*\
Method:	Set_VMC2_VendReq
	Attiva la richiesta di Vend Req dal VMC2.
	
	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
void  Set_VMC2_VendReq(uint8_t Selezione) {

	Set_fVMC_Inibito;																			// Inhibit VMC Locale
	sGestMSVars.fVMC2_SelezReq 	= true;															// VMC2 richiede una Vend alla RR
	sGestMSVars.VMC2_SelValue 	= Selezione;													// Valore/Numero selezione richiesta dal VMC2
}

/*--------------------------------------------------------------------------------------*\
Method:	SwitchToVMC2
	Predispone per la comunicazione RR --> VMC2.
	
	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
static void  SwitchToVMC2(void) {

	sGestMSVars.fSlaveSuspend	= true;
	sGestMSVars.fRRToVMC2		= true;
}

/*--------------------------------------------------------------------------------------*\
Method:	SwitchToLocalVMC
	Predispone per la comunicazione RR --> VMC Locale.
	Riabilita il master Executive verso il VMC2.
	
	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
static void  SwitchToLocalVMC(void) {

	sGestMSVars.fSlaveSuspend	= false;
	sGestMSVars.fRRToVMC2		= false;
	RestartExecMaster();																		// Riabilita Master e aggiorna Credito su VMC2
}

/*--------------------------------------------------------------------------------------*\
Method:	GestioneMS_Exec
	Chiamata durante il ciclo selezione per mantenere il collegamento col VMC2 da parte
	del Master Executive interno se in protocollo Exec MST/SLV.
	
	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
void  GestioneMS_Exec(void) {
	
	if (gVMC_ConfVars.ExecMSTSLV == true) {														// MST/SLV
		GestoreMS_Task();
		EXEC_Master_Task();

#if (MonitorInMSTSLV == true)
		SystemMonitor();
#endif
	}
}

// =============================================================================================
// =============================================================================================
// ========   R I C E T R A S M I S S I O N I         S E R I A L I    =========================
// =============================================================================================
// =============================================================================================

/*--------------------------------------------------------------------------------------*\
Method:	GestMS_RR_CMD
	Rx CMD dalla RR sulla UART seriale (funzione chiamata ed eseguita in IRQ).
	
	IN:	  - Chr ricevuto dalla RR sulla seriale
	OUT:  -
\*--------------------------------------------------------------------------------------*/
void  GestMS_RR_CMD(uint8_t RRdata) {
	
    Monitor_LogExecRxRR(RRdata);
	if (gVMC_ConfVars.ExecMSTSLV == true) {														// MST/SLV
		TraceCMDRR(RRdata);																		// Analisi CMD dalla RR
		if (sGestMSVars.fRRToVMC2 == true) {													// RR ---> VMC2
			Master_Exec_TxChar(RRdata);															// Trasmetto CMD dalla RR al VMC2
			sGestMSVars.RRCmd_Cnt++;
		} else {
			ExecSlaveDataInd(RRdata);															// RR ---> VMC in MST/SLV
		}
	} else {
		ExecSlaveDataInd(RRdata);																// RR ---> VMC senza MST/SLV
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: VMC_Locale_Reply
	Risposta del VMC Locale al Comando RR.
	Funzione chiamata in IRQ dalla ExecSlave.
	
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void  VMC_Locale_Reply(uint8_t Reply) {

	sGestMSVars.ByteDaVMC 	= Reply;															// Risposta VMC Locale in ByteDaVMC
	sGestMSVars.fRxChrDaVMC = true;																// Set ricevuta risposta dallo slave
}

/*---------------------------------------------------------------------------------------------*\
Method: GestMS_RxVMC2
	Risposta del VMC2 al Comando del Master che puo' essere la RR o l'ExecMaster interno.
	Funzione chiamata in IRQ dalla "TWRSER4_OnBlockReceived".
	
	IN:		- 
	OUT:	-
\*----------------------------------------------------------------------------------------------*/
void  GestMS_RxVMC2(uint8_t VMC2Data) {
	
	Monitor_LogExecRxVMC2(VMC2Data);
	if (sGestMSVars.fRRToVMC2 == true) {
		sGestMSVars.ByteDaVMC 	= VMC2Data;														// Risposta VMC2 in ByteDaVMC
		sGestMSVars.fRxChrDaVMC = true;															// Set ricevuta risposta dallo slave
	} else {
		ExecMasterRxDataVMC2(VMC2Data);															// Risposta VMC2 al Master Executive interno
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	GestMS_RxVMC2_Error
	Funzione chiamata dalla Uart ed eseguita in IRQ.
	Ricevuto errore sulla seriale dal VMC2.
	
	IN:		- 
	OUT:	-
\*--------------------------------------------------------------------------------------*/
void  GestMS_RxVMC2_Error(uint8_t data) {
	
	if (sGestMSVars.fRRToVMC2 == true) {														// VMC2 in collegamento con la RR
		Master_Exec_TxChar(EXEC_RR_NACK);														// Trasmetto immediatamente NACK al VMC2
	} else {
		ExecMasterRxErr();																		// VMC2 in collegamento con il Master interno, inviera' lui NACK
	}	
}

/*--------------------------------------------------------------------------------------*\
Method:	GestMS_RxError
	Funzione chiamata ed eseguita in IRQ.
	Ricevuto errore sulla seriale ricevendo un CMD dalla RR.
	
	IN:		- 
	OUT:	-
\*--------------------------------------------------------------------------------------*/
void  GestMS_RxRR_Error(uint8_t data) {
	
	ExecSlaveErrInd(data);																		// Uso sempre ExecSlave per rispondere all'errore seriale dalla RR
}

