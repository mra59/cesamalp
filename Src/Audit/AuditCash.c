/*
 * AuditCash.c
 *
 *  Created on: Oct 1, 2013
 *      Author: Abe
 */

#include "ORION.H"
#include "iCPGlobal.h"
#include "icpBILL.h"
#include "icpCHG.h"
#include "IICEEP.h"
#include "Funzioni.h"
#include "AuditCash.h"
#include "Power.h"



#include "Dummy.h"

CreditCoinValue CoinValuesList[ICP_MAX_COINTYPES];
CreditCoinValue BVHBillValuesList[ICP_MAX_BILLTYPES];


void BillAuditStart(ET_BILLPOLLDEST billDest, uint8_t billType, CreditCoinValue billValue)
{
	PSHProcessingData();
	BillRouting = billDest;
	BillIdx = billType;
	BillVal = billValue;
	FaseAuditBill = IN_AUD_BILL;
	PSHCheckPowerDown();
}


void CoinAuditStart(ET_CHGPOLLCOINDEST coinDest, uint8_t coinType, CreditCoinValue coinValue)
{
	PSHProcessingData();
	CoinRouting = coinDest;
	CoinIdx = coinType;
	CoinVal = coinValue;
	FaseAuditCoin = IN_AUD_COIN;
	PSHCheckPowerDown();
}

void CoinFillManualAuditStart(uint32_t fill_amount)
{
	if (fill_amount == 0) return;
//	AuditFillManual[ix] = coin;
	PSHProcessingData();
	IndexManFill = 0;
	AmountAuditFillCoin = fill_amount;
	FaseAuditFillManual = IN_MANFILL_COIN;
	PSHCheckPowerDown();
}

void CoinPayoutExpAuditStart(CreditCoinValue ChangeAmount, CMDrvPAYPOS route)
{
	if (ChangeAmount == 0) return;
	PSHProcessingData();
	IndexManPayout = EXP_AUDIT;
	ChangeVal = ChangeAmount;
	ChangeRouting = route;
	FaseAuditChange = IN_CH_PAY_COIN;
	PSHCheckPowerDown();
}

void CoinPayoutAuditStart(CreditCoinValue ChangeAmount, CMDrvPAYPOS route)
{
	if (ChangeAmount == 0) return;
	PSHProcessingData();
	IndexManPayout = 0;
	ChangeVal = ChangeAmount;
	ChangeRouting = route;
	FaseAuditChange = IN_CH_PAY_COIN;
	PSHCheckPowerDown();
}


/************************************************************/
/*  Gestione  Audit del credito da cash ChangeGiver o Bill  */
/************************************************************/

void GestioneAuditCash(void) {

    //*********************************
    // genera audit monete

	do {
	    switch(FaseAuditCoin) {
		      case IN_AUD_COIN :
	    	  switch(CoinRouting) {
	    	  	  case ICP_CHG_CASHBOX:  
	    	  		  AuditUlongAdd(IICEEPAuditAddr(INCashBox), CoinVal);     				// CA306
	    	  		  break;
	    	  	  case ICP_CHG_TUBES:
	    	  		  AuditUlongAdd(IICEEPAuditAddr(INCashTub), CoinVal);     				// CA307
	    	  		  break;
	    	  	  default:
	    	  		  break;
	    	  }
			  PSHProcessingData();
	    	  FaseAuditCoin++;
	    	  PSHCheckPowerDown();

	      case LR_AUD_COIN :
	    	  switch(CoinRouting){
	    	  	  case ICP_CHG_CASHBOX:  
	    	  		  AuditUlongAdd(IICEEPAuditAddr(LRCashBox), CoinVal);     				// CA302
	    	  		  break;
	    	  	  case ICP_CHG_TUBES:
	    	  		  AuditUlongAdd(IICEEPAuditAddr(LRCashTub), CoinVal);     				// CA303
	    	  		  break;
	    	  	  default:
	    	  		  break;
	    	  }
			  PSHProcessingData();
	    	  FaseAuditCoin++;
	    	  PSHCheckPowerDown();

	      case TOT_NUM_COIN :
	    	  AuditUintInc(IICEEPAuditAddr(numCoin[CoinIdx]));                				// CA1102
			  FaseAuditCoin++;
		      PSHCheckPowerDown();

	      case ROUTE_NUM_COIN :
	    	  switch(CoinRouting) {
	    	  	  case ICP_CHG_CASHBOX:  
	    	  		  AuditUintInc(IICEEPAuditAddr(CashCoin[CoinIdx]));     				// CA1103
	    	  		  break;
	    	  	  case ICP_CHG_TUBES:
	    	  		  AuditUintInc(IICEEPAuditAddr(TubCoin[CoinIdx]));     					// CA1104
	    	  		  // aggiornare anche numero delle monete presente nei tubi LR CA1703
	    	  		  break;
	    	  	  default:
	    	  		  break;
	    	  }
			  PSHProcessingData();
	    	  FaseAuditCoin++;
		      PSHCheckPowerDown();
		    	  
	      case INC_NUM_TUBE :
	    	  if (CoinRouting == ICP_CHG_TUBES){
	    		  AuditByteInc(IICEEPAuditAddr(TubCoinStatus[CoinIdx]), 1);     			// CA1501 calcolo locale
	    	  }
			  PSHProcessingData();
	    	  FaseAuditCoin++;
	    	  PSHCheckPowerDown();

		  case END_AUD_COIN :
			  PSHProcessingData();
			  CoinRouting = 0;
			  CoinIdx = 0;
			  CoinVal = 0;
	    	  PSHCheckPowerDown();

		  default:
			  FaseAuditCoin = NO_AUD_COIN;
	    }
	} while (FaseAuditCoin != NO_AUD_COIN);

		
    //*********************************
    // genera audit banconote
    PSHCheckPowerDown();
    
    do {
 	    switch(FaseAuditBill) {

 	      case IN_AUD_BILL :
 	    	  AuditUlongAdd(IICEEPAuditAddr(INBankInp), BillVal);     							// CA308
 	    	  FaseAuditBill++;
 	    	  PSHCheckPowerDown();

 	      case LR_AUD_BILL :
 	    	  AuditUlongAdd(IICEEPAuditAddr(LRBankInp), BillVal);     							// CA304
 	    	  FaseAuditBill++;
 	    	  PSHCheckPowerDown();

 	      case TOT_NUM_BILL :
 	    	  AuditUintInc(IICEEPAuditAddr(numBank[BillIdx]));          						// CA1402
 			  FaseAuditBill++;
 		      PSHCheckPowerDown();

 		  case END_AUD_BILL :
			  PSHProcessingData();
 			  BillRouting = 0;
 			  BillIdx = 0;
 			  BillVal = 0;
	    	  PSHCheckPowerDown();

 	      default:
 	    	  FaseAuditBill = NO_AUD_BILL;
 	    }
 	} while (FaseAuditBill != NO_AUD_BILL);
}


/************************************************************/
/*  Gestione Audit da caricamento manuale tubi  */
/************************************************************/

void GestioneAuditManualFill(void){
	
	// AGGIORNARE IDENTIFICATORE AUDIT CA1704
	do {
	    switch(FaseAuditFillManual) {

	    	case IN_MANFILL_COIN:
	    		AuditUlongAdd(IICEEPAuditAddr(INCashFill), AmountAuditFillCoin); 
	    		FaseAuditFillManual = LR_MANFILL_COIN; 	
	    		PSHCheckPowerDown();
	    		  
	    	case LR_MANFILL_COIN:  
	    		AuditUlongAdd(IICEEPAuditAddr(CashFill), AmountAuditFillCoin); 
	    		FaseAuditFillManual = INC_TUBESTATUS_NUM; 	
	    		PSHCheckPowerDown();
	
	    	case INC_TUBESTATUS_NUM:
	    		//MR warning: expression has no effect for (IndexManFill; IndexManFill<ICP_MAX_COINTYPES; IndexManFill++) {							// incrementa numero monete derivanti dal manual fill money expansion command 0F-06
	    		for (; IndexManFill<ICP_MAX_COINTYPES; IndexManFill++) {							// incrementa numero monete derivanti dal manual fill money expansion command 0F-06
	    			AuditByteInc(IICEEPAuditAddr(TubCoinStatus[IndexManFill]), AuditFillManual[IndexManFill]);	// CA1501 calcolo locale
	    			PSHCheckPowerDown();																		// Check PWD perche' IndexManFill e' l'indice in BackupRAM del loop
	    		}
				PSHProcessingData();
	    		FaseAuditFillManual = END_MANFILL_COIN; 	
	    		PSHCheckPowerDown();
    		  
	    	case END_MANFILL_COIN:
	    		PSHProcessingData();
				AmountAuditFillCoin = 0;
				FaseAuditFillManual = NO_MANFILL_COIN;
	    		PSHCheckPowerDown();
		    	  
	    	default:
	    		FaseAuditFillManual = NO_MANFILL_COIN;
		}
	} while (FaseAuditFillManual != NO_MANFILL_COIN);
}

/************************************************************/
/*  Gestione Audit monete rese come resto                   */
/************************************************************/

void GestioneAuditCoinPayout(void) {
	
	do {
		switch(FaseAuditChange) {

			case IN_CH_PAY_COIN:
				if (ChangeRouting == Pay_Change) {
					AuditUlongAdd(IICEEPAuditAddr(INDispensed), ChangeVal);     				// CA403
				} else if(ChangeRouting == Pay_Manual) {
					AuditUlongAdd(IICEEPAuditAddr(INManDispen), ChangeVal);     				// CA404
				}
				FaseAuditChange = LR_CH_PAY_COIN; 
				PSHCheckPowerDown();
		    	  	  
		    case LR_CH_PAY_COIN:	
		    	if(ChangeRouting == Pay_Change) {
		    		AuditUlongAdd(IICEEPAuditAddr(Dispensed), ChangeVal);     					// CA401
		    	} else if(ChangeRouting == Pay_Manual) {
		    		AuditUlongAdd(IICEEPAuditAddr(ManDispen), ChangeVal);     					// CA402
		    	}
				FaseAuditChange = DEC_TUBESTATUS_NUM; 
	    		PSHCheckPowerDown();
		    	  	  
		    case DEC_TUBESTATUS_NUM:
		    	if ((IndexManPayout & EXP_AUDIT) == EXP_AUDIT) {								//test flag per singola/multipla moneta resa -->risultato exp command 0F-07
		    		//MR warning: expression has no effect for ((IndexManPayout&~EXP_AUDIT); (IndexManPayout&~EXP_AUDIT)<ICP_MAX_COINTYPES; IndexManPayout++) {
  					for (; (IndexManPayout&~EXP_AUDIT)<ICP_MAX_COINTYPES; IndexManPayout++) {
		    			AuditByteDec(IICEEPAuditAddr(TubCoinStatus[(IndexManPayout&~EXP_AUDIT)]), AuditPaydManual[(IndexManPayout&~EXP_AUDIT)] );	// CA1501 calcolo locale monete multiple
		    			PSHCheckPowerDown();																										// Check PWD perche' IndexManFill e' l'indice in BackupRAM del loop
		    		}
		    	} else {
		    		AuditByteDec(IICEEPAuditAddr(TubCoinStatus[AuditPaydManual[0]]), AuditPaydManual[1] );    										// CA1501 calcolo locale
		    	}
				PSHProcessingData();
		    	FaseAuditChange = END_CHANGE_COIN; 
	    		PSHCheckPowerDown();
		    	  	  
		    case END_CHANGE_COIN:  	  
		    	PSHProcessingData();
		    	ChangeRouting = 0;
		    	ChangeVal = 0;
		    	FaseAuditChange = 0;
	    		PSHCheckPowerDown();

		    default:
		    	FaseAuditChange = NO_CHANGE_COIN;
		}
	} while (FaseAuditChange != NO_CHANGE_COIN);
}	


