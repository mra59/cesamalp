/****************************************************************************************
File:
    DDCMPHook.c

Description:
    Funzioni di aggancio protocllo DDCMP
    

History:
    Date       Aut  Note
    Giu 2019 	MR   

*****************************************************************************************/

#include <string.h>
#include <stdio.h>

#include "ORION.H"
#include "DevSerial.h"
#include "AuditDataDescr.h"
#include "DDCMPHook.h"
#include "DDCMPProtocol.h"
#include "DDCMPDTSHook.h"

#include "Funzioni.h"
#include "Audit.h"
#include "VMC_CPU_HW.h"


/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

bool 	DDCMPMngrHookConnEstablishedInd(void);
void 	DDCMPMngrHookEnd(void);
void  	UpdateTimeDate(uint8_t*);

uint8_t	gAuditListNumber;

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	bool  		IsMonitorActive(void);
extern	void 		ReadExtAudDataBlock(uint16_t, uint16_t, char*);
extern	uint8_t 	BcdToBin(uint8_t bcd);
extern	LDD_RTC_TTime 	DataOra;
//extern	void 			Set_ora_LS(LDD_RTC_TTime *DataOra);

/*---------------------------------------------------------------------------------------------*\
Method: UpdateTimeDate
	Aggiorna orologio con i dati ricevuti dal Carrier
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  UpdateTimeDate(uint8_t* ClockDaWAY) {

	uint8_t	anno, gg, mm, hh, min, ss;
	
	//MR19 RTC1_GetTime(NULL,&DataOra);

	gg = BcdToBin(ClockDaWAY[7]);
	if (gg == 0 | gg > 31) return;								// Giorno da 1 a 31
	mm = BcdToBin(ClockDaWAY[8]);
	if (mm > 12) return;										// Mese da 0 a 12
	hh = BcdToBin(ClockDaWAY[10]);
	if (hh > 23) return;										// Ora da 0 a 23
	min = BcdToBin(ClockDaWAY[11]);
	if (min > 59) return;										// Minuti da 0 a 59
	ss = BcdToBin(ClockDaWAY[12]);
	if (ss > 59) return;										// Secondi da 0 a 59
	anno = BcdToBin(ClockDaWAY[9]);								// Anno

	DataOra.Year = anno + MILLENNIO_2000;
	DataOra.Day = gg;
	DataOra.Month = mm;
	DataOra.Hour = hh;
	DataOra.Minute = min;
	DataOra.Second = ss;
	//Set_ora_LS(&DataOra);
	RTC_SetTimeAndDate(&DataOra);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookBaudRateChange
	Cambio Baud Rate alla Seriale usata per l'IrDA.
	
\*--------------------------------------------------------------------------------------*/
void  DDCMPHookBaudRateChange(uint8_t BdRate) {

	IrDA_COMM_ChangeBaudRate(BdRate);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookCommClose
	Disattivo la Seriale usata per l'IrDA.
	
\*--------------------------------------------------------------------------------------*/
void  DDCMPHookCommClose(void) {
	
	IrDA_COMM_Disable();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookTxBegin
	Attivo la trasmissione sulla seriale usata per l'IrDA e disattivo la ricezione per non
	avere il feedback della trasmissione stessa.
	
\*--------------------------------------------------------------------------------------*/
/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  DDCMPHookTxBegin(void) {
	
	IrDA_COMM_TxBegin();
} 
*/

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookRxBegin
	Attivo la ricezione sulla seriale usata per l'IrDA.
	
\*--------------------------------------------------------------------------------------*/
/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  DDCMPHookRxBegin(void) {
	
	IrDA_COMM_RxBegin();												// visto che per errore hanno messo un pull-up nel circuito dell'Orion MDB
}
*/

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookRxEnd
	Disattivo la ricezione sulla seriale usata per l'IrDA.
	
\*--------------------------------------------------------------------------------------*/
/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  DDCMPHookRxEnd(void) {
	
	IrDA_COMM_RxEnd();
}
*/

/*---------------------------------------------------------------------------------------------*\
Method: DDCMPHookIrDAComm
	Ritorna true se in connessione IrDA, false se in connessione con PC
 
	IN:	  - 
	OUT:  -  true se in connessione IrDA, false se in connessione con PC
\*----------------------------------------------------------------------------------------------*/
bool DDCMPHookIrDAComm(void)
{
	return !IsMonitorActive();
}

/*---------------------------------------------------------------------------------------------*\
Method: DDCMPHookBaudRateMaxGet
	Ritorna la velocita' massima IrDA consentita in questo prodotto 
 
	IN:	  - 
	OUT:  -  kMaxBaudRateDDCMP
\*----------------------------------------------------------------------------------------------*/
uint8_t DDCMPHookBaudRateMaxGet(void)
{
	return kMaxBaudRateDDCMP;
}

/*---------------------------------------------------------------------------------------------*\
Method: DDCMPHookTxEnd
	Disattiva la trasmissione IrDA
 
	IN:	  - 
	OUT:  -  
\*----------------------------------------------------------------------------------------------*/
void DDCMPHookTxEnd(void) 
{
}


/*--------------------------------------------------------------------------------------*\
Method: DEXUCSHookCommOpen
Parameters:
\*--------------------------------------------------------------------------------------*/
//void DEXUCSHookCommOpen(void) {
//	//TODO ->    DEXUCSSCIOpen(serOpt, hnd);
//} 


void DEXUCSHookCommClose(void) {
	//TODO ->  DEXUCSSCIClose();
}

void DEXUCSHookTxBegin(void) {
	//TODO ->  DEXUCSSCIRxEnd();
	//TODO ->  DEXUCSSCITxBegin();
} 

void DEXUCSHookTxEnd(void) {
	//TODO ->  DEXUCSSCITxEnd();
}
 
void DEXUCSHookRxBegin(void) {
	//TODO ->  DEXUCSSCIRxBegin();
}

void DEXUCSHookRxEnd(void) {
	//TODO ->  DEXUCSSCIRxEnd();
}

void DEXUCSHookCharTx(char ch) {
	//TODO ->  DEXUCSSCICharTx(ch);
}


bool DDCMPDTSHookSlaveRelease(void) {
  return false;
}


void  DDCMPHookLineRelease(void) {
}

bool  DDCMPHookLineRequest(void) {
  return true;
}

void  DDCMPHookSendStart(void) {
// TODO: controllare...
/**
**/
}

void  DDCMPHookWaitStart(void) {
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookCheckFineTxAudit
	Se e' l'ACK all'ultimo blocco dati Audit trasmesso, predispongo azzeramento Audit LR
	alla ricezione del successivo FINISH.

Parameters:	stato di "uData.audit.EndTxAudit"
\*--------------------------------------------------------------------------------------*/
void  DDCMPHookCheckFineTxAudit(void) {
	if (uData.audit.EndTxAudit == TxLastAuditBlk) {
		uData.audit.EndTxAudit = AckedLastAuditBlk;
	}
}
/*--------------------------------------------------------------------------------------*\
Method: DDCMPHookRxFinish
	Controllo se e' un Finish seguente al prelievo Audit: se ho ricevuto l'ACK all'ultimo
	blocco dati Audit trasmesso ed ora ricevo il Finish, predispongo l'azzeramento
	Audit LR.
	La scrittura in EEPROM della marca "ENA_AUDIT_LR_CLEAR" funziona anche se qui siamo
	in IRQ ricezione UART perche' la priorita' dell'IRQ I2C EEPROM e' maggiore.

Parameters:	stato di "uData.audit.EndTxAudit"
\*--------------------------------------------------------------------------------------*/
void  DDCMPHookRxFinish(void) {
	if (uData.audit.EndTxAudit == AckedLastAuditBlk) {
		Set_DDCMP_EnableAuditClear();
	}
}
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookConnEstablishedSlaveInd
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSHookConnEstablishedSlaveInd(void) {
  DDCMPMngrHookConnEstablishedInd();
    // Notify our user, then wait for master commands
}
void DDCMPDTS2HookConnEstablishedSlaveInd(void) {
  DDCMPMngrHookConnEstablishedInd();
    // Notify our user, then wait for master commands
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookConnFinishedInd
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSHookConnFinishedInd(void) {
	
	DDCMPMngrHookEnd();
	Clear_fDDCMPTransferInProgress;
}

void DDCMPDTS2HookConnFinishedInd(void) {

	DDCMPMngrHookEnd();
	Clear_fDDCMPTransferInProgress;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookConnFailedInd
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSHookConnFailedInd(void) {

	DDCMPHookLineRelease();		// MR Vuota
/*
  if(uData.audit.configReceived) {
    CT5Restart();
  }
  else {
    Clear_fDDCMPTransferInProgress;
  }
*/
  Clear_fDDCMPTransferInProgress;
}

void DDCMPDTS2HookConnFailedInd(void) {

  DDCMPHookLineRelease();		// MR Vuota

  /**
  if(uData.audit.configReceived) {
    CT5Restart();
  }
  else {
    Clear_fDDCMPTransferInProgress;
  }
  **/
  Clear_fDDCMPTransferInProgress;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookDataRxInd
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSHookDataRxInd(uint8_t dataBlockNum, uint8_t* applData, uint16_t applDataLen, bool dataEnd) {
	// TODO: controllare...

	/**
  GestioneConfigurazione(applData-2);

  if(dataEnd){
    ResetConfigReceiveStart(); 
    DDCMPDTS2CmdEnd();
  }
  else {
	  DDCMPDTS2DataRx();
  }
  **/
}

void DDCMPDTS2HookDataRxInd(uint8_t dataBlockNum, uint8_t* applData, uint16_t applDataLen, bool dataEnd) {
	// TODO: controllare...

	/**
  GestioneConfigurazione(applData-2);

  if(dataEnd){
    ResetConfigReceiveStart(); 
    DDCMPDTS2CmdEnd();
  }
  else {
	  DDCMPDTS2DataRx();
  }
  **/
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookDataTxInd
\*--------------------------------------------------------------------------------------*/
void DDCMPDTSHookDataTxInd(uint8_t dataBlockNum, char* applDataBuff, uint16_t dataBuffSize) {
	
	//uint8_t r;
	uint16_t length;

  uData.audit.applDataBuff = applDataBuff;
  uData.audit.dataBuffFree = dataBuffSize;
  uData.audit.dataBuffUsed = 0;
  if(uData.audit.endTx) {
    if(uData.audit.listNumber==1){
      uData.audit.auditDataRead= true;
      uData.audit.auditExtDataRead= true;
    }
    else if(uData.audit.listNumber==3)
      uData.audit.auditExtDataRead= true;

      DataTranferEnd();

  }
  else{
    if( uData.audit.localBufferSize != 0 )
      DDCMPMngrHookCopyLocalBuffer();

/*
	// Test se produrre Log Buffer
	if (SystOpt4 & TxIrDALog) {													// Opzione registra Log Audit Irda
		if (Transfer_EE) {
			TranferLogToEE(false, true);										// Trasferisce il log buffer RAM in EE
		}
	}
*/

    do{
      if( uData.audit.loopBodyResponse == false )
        break;

      if (ReadTipoLista()){ 
        if (uData.audit.txAuditExt) {
        	//  Send Ext Audit
        	if( uData.audit.numBlocks == uData.audit.block ) {
        		uData.audit.loopBodyResponse = false;
        		break;
          } else {
        	  ReadExtAudDataBlock(uData.audit.block, uData.audit.numBlocks, &uData.audit.localBuffer[0]);
        	  uData.audit.block++;
        	  DDCMPMngrHookBufferAudit(&uData.audit.localBuffer[0], strlen(uData.audit.localBuffer));
        	  if (uData.audit.localBufferSize != 0) {
        		  break;
        	  }
          }
        }
        else {
  //  Send Audit
          uData.audit.loopBodyResponse = AuditLoopBody();
          if (uData.audit.localBufferSize != 0) {
        	  break;
          }
          if (uData.audit.loopBodyResponse == false) {
        	  if (uData.audit.numBlocks != 0) {
        		  uData.audit.txAuditExt = true;
        		  uData.audit.loopBodyResponse = true;
        	  } else {
        		  break;
        	  }
          }
        }
      } else {
    	  //  Send Configuration: tolto il 15.07.2013 avendo messo false l'opzione "kApplConfigTxSupport"
    	  //uData.audit.loopBodyResponse = ConfTxLoop();
    	  //uData.audit.txBuffer = true;
    	  break;
      }
    }while(1);

    if( uData.audit.txBuffer == true || uData.audit.localBufferSize != 0 ){
      uData.audit.txBuffer = false;
      DataTranferTx(uData.audit.dataBuffUsed, false);
    }
    else {
      if(uData.audit.loopBodyResponse == false) {
    	if(ReadTipoLista()) {
    		  sprintf(uData.audit.localBuffer,"DXE*1*1\r\n\r\n\r\n");

/* -------------  Da mettere come campo 1 nell'ID di chiusura "SE" --------------
        		sprintf(uData.audit.localBuffer,"DXE*%5u*1\r\n\r\n\r\n", RigheAudit);
        		// Gli spazi davanti alle cifre di RigheAudit diventano zeri
        		for (r=0; r<5; r++) {
        			if (uData.audit.localBuffer[4+r] != ' ') break;									// Finiti gli spazi
        			if (uData.audit.localBuffer[4+r] <'0') uData.audit.localBuffer[4+r] = '0';
        		}
*/
        } else {
        		sprintf(uData.audit.localBuffer,"\r\n\r\n");
        }
        	length = strlen(uData.audit.localBuffer);
        	if ((uData.audit.dataBuffTransmitted % 128) == 0) length -= 2;
        	if (uData.audit.dataBuffFree < length) {
        		DataTranferTx(uData.audit.dataBuffUsed, false);
        	} else {
        		DDCMPMngrHookBufferAudit(&uData.audit.localBuffer[0], length);
       			DataTranferTx(uData.audit.dataBuffUsed, true);
       			uData.audit.endTx = true;
       			uData.audit.EndTxAudit = TxLastAuditBlk;
        	}
      }
    }
  }

}

void DDCMPDTS2HookDataTxInd(uint8_t dataBlockNum, uint8_t* applDataBuff, uint16_t dataBuffSize) {}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookDeleteDataReqInd
\*--------------------------------------------------------------------------------------*/
bool DDCMPDTSHookDeleteDataReqInd(uint8_t listNumber, uint8_t recordNumber)
{
	return false;		//MR19 aggiunto
}

bool DDCMPDTS2HookDeleteDataReqInd(uint8_t listNumber, uint8_t recordNumber)
{
	return false;		//MR19 aggiunto
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookReadDataReqInd
\*--------------------------------------------------------------------------------------*/

bool DDCMPDTSHookReadDataReqInd (uint8_t listNumber, uint16_t byteOffset, uint16_t length, uint16_t* actualLen)
{
	switch(listNumber)
	{
		case 1:					// AUDIT_LIST
		case 2:
		case 3:
			if (!AuditTransferAllowed()) return false;
			Set_fDDCMPTransferInProgress; 															// Inhibit payment system while we are sending audit

		case 64:
			gAuditListNumber = listNumber;
			uData.audit.listNumber = listNumber;
			DDCMPMngrTxBegin(); 																	// read audit list
			return true;
	}
	return false;
}

bool DDCMPDTS2HookReadDataReqInd(uint8_t listNumber, uint16_t byteOffset, uint16_t length, uint16_t* actualLen) {

/*
  switch(listNumber) {
  case 1:
  case 2:
  case 3:
    if(!AuditTransferAllowed()) return false;
    Set_fDDCMPTransferInProgress; // Inhibit payment system while we are sending audit
  case 64:

	gAuditListNumber = listNumber;
    uData.audit.listNumber = listNumber;
    DDCMPMngrTxBegin(); // read audit list
    return true;
  }
*/	
  return false;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookWriteDataReqInd
\*--------------------------------------------------------------------------------------*/
bool DDCMPDTSHookWriteDataReqInd(uint8_t listNumber, uint16_t length)
{
	if(listNumber == 64) 
	{
		Set_fDDCMPTransferInProgress; // Inhibit payment system while we are receiving cfg
		gAuditListNumber = listNumber;
		uData.audit.listNumber = listNumber;
		uData.audit.configReceived= true;
		SetConfigReceiveStart();       
		DDCMPMngrTxBegin();
		return true;
	}
	else
	{
		  return false;
	}
}

bool DDCMPDTS2HookWriteDataReqInd(uint8_t listNumber, uint16_t length)
{
	// TODO: controllare...

	/**
  if(listNumber == 64) {
    Set_fDDCMPTransferInProgress; // Inhibit payment system while we are receiving cfg
		gAuditListNumber = listNumber;
    uData.audit.listNumber = listNumber;
    uData.audit.configReceived= true;
      SetConfigReceiveStart();       
    DDCMPMngrTxBegin();
    return true;
  }
  else
  **/
    return false;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookPwdGet
\*--------------------------------------------------------------------------------------*/
uint16_t DDCMPDTSHookPwdGet(void)
{
	return AudPsw;
}

uint16_t DDCMPDTS2HookPwdGet(void)
{
	return AudPsw;
}


bool DDCMPMngrHookConnEstablishedInd(void) {
/**TODO integrare!!!
  gAuditListNumber= 0; // This will be set when audit list is actually requested (avoid clearing UP audit when credit present)
  if(!AuditTransferAllowed()) return false;
  Set_fDDCMPTransferInProgress; // Inhibit payment system while we are sending audit
  uData.audit.listNumber= 0;
  uData.audit.auditDataRead= false;
  uData.audit.auditExtDataRead= false;
  uData.audit.configReceived= false;
**/
  return true;
}

void  DDCMPMngrHookEnd() {
//	Come da protocollo ho spostato l'azzeramento Audit
//	alla ricezione del FINISH 08.08.13
}

/*--------------------------------------------------------------------------------------*\
Method: Set_Audit_Buf
	Inizializza pointer e size Audit buffer e azzera contatori.
	Chiamata dalla Export_Audit USB

Parameters:
	IN	- pointer al buffer destinazione e suo size
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void Set_Audit_Buf(char *bufpnt, uint16_t size)
{
	uData.audit.applDataBuff = bufpnt;
	uData.audit.dataBuffFree = size;
	uData.audit.dataBuffUsed = 0;
	uData.audit.dataBuffTransmitted = 0;
}

/*--------------------------------------------------------------------------------------*\
Method: Get_dataBuffTransmitted
	Rende il numero di chr inseriti nel buffer Audit.

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/
uint16_t Get_dataBuffTransmitted(void)
{
	return uData.audit.dataBuffTransmitted;
}

/*--------------------------------------------------------------------------------------*\
Method: Get_dataBuffFree
	Rende lo spazio ancora disponibile nel buffer Audit destinazione

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/
uint16_t Get_dataBuffFree(void)
{
	return uData.audit.dataBuffFree;
}

/*--------------------------------------------------------------------------------------*\
Method: Get_localBufferSize
	Rende il dato "uData.audit.localBufferSize" che indica quando >0 che il buffer
	Audit e' pieno e va inviato.
	Normalmente = 0 ad indicare che c'e' ancora spazio per altri ID.

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/
uint8_t Get_localBufferSize(void)
{
	return uData.audit.localBufferSize;
}

/*--------------------------------------------------------------------------------------*\
Method: Extended_Audit_LoopBody
	Preleva i record di Audit Estesa dalla EEprom.

Parameters:
	IN	-
	OUT	- false quando non ci sono altri record in EEprom
\*--------------------------------------------------------------------------------------*/
bool Extended_Audit_LoopBody(void)
{
	do {
			if (uData.audit.numBlocks == uData.audit.block)
			{
				return false;																	// Prelievo Audit Estesa terminato
			}
			else
			{
				ReadExtAudDataBlock(uData.audit.block, uData.audit.numBlocks, &uData.audit.localBuffer[0]);
				uData.audit.block++;
				DDCMPMngrHookBufferAudit(&uData.audit.localBuffer[0], strlen(uData.audit.localBuffer));
				if (uData.audit.localBufferSize != 0)
				{
					break;																		// Buffer pieno, copiarlo su USB
				}
			}
	} while(true);
	
	return true;
}



