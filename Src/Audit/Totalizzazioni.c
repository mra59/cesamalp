/*--------------------------------------------------------------------------------------*\
File:
    Totalizzazioni.c

Description:
    File con funzioni per il calcolo di totali vari.
    

History:
    Date       Aut  Note
    Set 2012 	MR   
\*--------------------------------------------------------------------------------------*/

#include "ORION.H"
#include "Totalizzazioni.h"
#include "AuditExt.h"
#include "IICEEP.h"
#include "OrionCredit.h"
#include "Funzioni.h"
#include "Power.h"


#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
Extern References 
\*--------------------------------------------------------------------------------------*/

extern CreditValue KeyCreditRevalue;

/*--------------------------------------------------------------------------------------*\
Method:	AzzeraCash

\*--------------------------------------------------------------------------------------*/
void  AzzeraCash(uint8_t TestPWDN_Flag) {
	
	PSHProcessingData();
	CashDaSelettore = 0;
	CashDaBankReader = 0;
	CRC_CashDaSelettore = 0;
	CRC_CashDaBankReader = 0;
	if (TestPWDN_Flag) {
		PSHCheckPowerDown();
	}
}

/*-----------------------------------------------------------------------------------------*\
Method:	SottraeCash
	Sottrae il credito cash da CashDaSelettore e/o da CashDaBankReader.
	Utilizzata prima di ricaricare il credito sulla carta o prima di attivare la gettoniera
	rendiresto

Parameters:
	IN	- valore da sottrarre a CashDaSelettore/CashDaBankReader
	OUT - 
\*-----------------------------------------------------------------------------------------*/
void  SottraeCash(uint16_t val) {
	
	PSHProcessingData();
	if (val <= CashDaSelettore) {
		CashDaSelettore -= val;
		MemoCashAdd(0, kCreditCashTypeCoin, NocheckPWD);							// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
	} else {
		val -= CashDaSelettore;
		CashDaSelettore = 0;
		MemoCashAdd(0, kCreditCashTypeCoin, NocheckPWD);							// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
		if (val <= CashDaBankReader) {
			CashDaBankReader -= val;
		} else {
			CashDaBankReader = 0;
		}
		MemoCashAdd(0, kCreditCashTypeBill, NocheckPWD);							// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
	}
	PSHCheckPowerDown();
}

/*-----------------------------------------------------------------------------------------*\
Method:	GestioneAuditFreeCredit
	Aggiorna l'Audit da Free-Credit.

Parameters:
	IN	- 
	OUT - 
\*-----------------------------------------------------------------------------------------*/
void GestioneAuditFreeCredit(void) {

	if (StatusFreeCredit == 0) return;
	switch(StatusFreeCredit) {

    case 1 :
    	AuditUlongAdd(IICEEPAuditAddr(LRCreFRCR), FreeCreditAccreditato);           			// DA602 (Revaluation Incentive on Cashless 1 LR) + DA403 User Defined Field
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 2 :
    	AuditUlongAdd(IICEEPAuditAddr(INCreFRCR), FreeCreditAccreditato);           			// DA601 Revaluation Incentive on Cashless 1 IN
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 3 :
    	AuditUlongAdd(IICEEPAuditAddr(LRDebFRCR), FreeCreditAddebitato);           				// DA9902 FreeCredit debited from Cashless LR
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 4 :
    	AuditUlongAdd(IICEEPAuditAddr(INDebFRCR), FreeCreditAddebitato);            			// DA9901 FreeCredit debited from Cashless IN
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 5 :
    	AuditUlongAdd(IICEEPAuditAddr(LRCardCre), FreeCreditAccreditato);           			// DA402 Value credited to Cashless 1 LR
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 6 :
    	AuditUlongAdd(IICEEPAuditAddr(INCardCre), FreeCreditAccreditato);          				// DA401 Value credited to Cashless 1 IN
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 7 :
    	AuditUlongAdd(IICEEPAuditAddr(LRCardDeb), FreeCreditAddebitato);           				// DA302 Value debited from Cashless 1 LR
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 8 :
    	AuditUlongAdd(IICEEPAuditAddr(INCardDeb), FreeCreditAddebitato);            			// DA301 Value debited from Cashless 1 IN
    	StatusFreeCredit++;
    	PSHCheckPowerDown();

    case 9 :
		if (FreeCreditAccreditato > 0 || FreeCreditAddebitato > 0) {
			AuditExtFreeCredit();
		}
    	PSHProcessingData();
		StatusFreeCredit++;

    default:
    	PSHProcessingData();
    	FreeCreditAccreditato = 0;              
    	FreeCreditAddebitato = 0;             
    	StatusFreeCredit = 0;               
    	PSHCheckPowerDown();
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	PresetAudFreeCredit
	Predispone variabili per la generazione dell'Audit dopo l'attribuzione del 
	Free credit alla carta.
	Solo quando StatusFreeCredit sara' 1, il sistema registrerÓ i dati Audit.
	In questo modo possono essere registrati sia quando il Free-credit e' attribuito
	subito, sia quando attribuito al momento della selezione.

\*--------------------------------------------------------------------------------------*/
void PresetAudFreeCredit(void) {
	
	PSHProcessingData();
	NewCreditKR 		= (uint16_t) gOrionKeyVars.Credit;										// Credito carta
	NewFreeCreditKR 	= (uint16_t) gOrionKeyVars.FreeCredit;									// Free Credit carta
	CodiceUtenteKR 		= (uint16_t) gOrionKeyVars.CodiceUtente;									// Codice Utente carta
	PSHCheckPowerDown();
}

/*--------------------------------------------------------------------------------------*\
Method:	PresetAudFreeVend
	Predispone variabili per la generazione dell'Audit dopo l'attribuzione delle
	FreeVend alla carta.
	In questo modo possono essere registrati sia quando il FreeVend e' attribuito
	subito, sia quando attribuito al momento della selezione.

\*--------------------------------------------------------------------------------------*/
void PresetAudFreeVend(void) {
	
	PSHProcessingData();
	NewCreditKR 		= (uint16_t) gOrionKeyVars.Credit;										// Credito carta
	NewFreeVendKR 		= (uint16_t) gOrionKeyVars.FreeVend;										// Free Vend carta
	CodiceUtenteKR 		= (uint16_t) gOrionKeyVars.CodiceUtente;									// Codice Utente carta
	PSHCheckPowerDown();
}
/*--------------------------------------------------------------------------------------*\
Method:	TotaleCash
	Restituisce il totale del credito cash in gestione

\*--------------------------------------------------------------------------------------*/
uint16_t  TotaleCash(void) {
	
	return (CashDaSelettore + CashDaBankReader);
}

/*--------------------------------------------------------------------------------------*\
Method:	TotaleCreditoAttuale
	Restituisce tutto il Credito in gestione escluso il credito Carta.
	Se la somma dei crediti supera il valore massimo ammissibile, rende il valore
	massimo ammissibile (MAX_SYSTEM_CREDIT).
	
	IN:	  - 
	OUT:  - Totale crediti o MAX_SYSTEM_CREDIT
\*--------------------------------------------------------------------------------------*/
uint16_t TotaleCreditoAttuale(void) {
	
	uint32_t	TotCredit;
	
	TotCredit = (CashDaSelettore + CashDaBankReader + CashCaricamentoBlocchi);
	if (TotCredit > MAX_SYSTEM_CREDIT) {
		return MAX_SYSTEM_CREDIT;
	} else {
		return TotCredit;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: AuditResetNoSyncMDB
	Audit ID aggiornati quando avviene un reset da WD provocato dal mancato sincronismo
	traa il Main e l'MDB. 
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void AuditResetNoSyncMDB(void) {
	
	PSHProcessingData();
	AuditUintInc(IICEEPAuditAddr(IN_EK_07_Event));                 								// "EA2*EK_07"
	AuditUintInc(IICEEPAuditAddr(LR_EK_07_Event));                 								// "EA2*EK_07"
    PSHCheckPowerDown();
}

/*------------------------------------------------------------------------------------------*\
 Method: Audit_WD_Event_Da_ChiaveUSB
	Audit ID aggiornati quando avviene un reset da WD provocato dall'inserimento di una 
	chiave USB.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Audit_WD_Event_Da_ChiaveUSB(void) {
	
	PSHProcessingData();
	AuditUintInc(IICEEPAuditAddr(IN_EK_08_Event));                 								// "EA2*EK_08" "USBKeyInserted"
	AuditUintInc(IICEEPAuditAddr(LR_EK_08_Event));                 								// "EA2*EK_08" "USBKeyInserted"
    PSHCheckPowerDown();
}




