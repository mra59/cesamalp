/****************************************************************************************
File:
    Dummy.c

Description:
    File con le funzioni vuote per compilare senza errori.

History:
    Date       Aut  Note
    Mag 2019	MR   

*****************************************************************************************/

#include "ORION.H"
#include "Dummy.h"


void  		Touch_Msg_PrelevaBevanda(void){}
void  		SetTouchNewViscredit(void){}
void 		Touch_Msg_VendFail(void){}
void  		WDT_Clear(LDD_TDeviceData *devWDT){}
void		Touch_Gest_Visualizzazioni(void){}
void 		SetActualTouchPage(uint16_t pagina) {/*TouchDisp.ActualPageOnDisplay = pagina;*/}











