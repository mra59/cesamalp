/****************************************************************************************
File:
    Sel24.h

Description:
    Include file con definizioni e strutture per la gestione del selettore parallelo.

History:
    Date       Aut  Note
    Dic 2013	Abe   

 *****************************************************************************************/

#ifndef __Sel24_h
#define __Sel24_h

#define COIN_DELAY 		30											// 25.02.15: portato da 20 msec a 30 msec
#define COIN_RELEASE 	5
#define COIN_FLAG 		0x80

#define WARMUP_TIME 	5*OneSec  									//warmup time per selettore 24 --> attesa eventuale rendiresto collegata

#define	Disable 0
#define	Enable  1

#define NumOfCoins	6

void Sel24_init(void);
void Sel24_read_coin(void);
void Sel24_task(void);

#endif		// End Sel24