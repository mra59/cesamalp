/*--------------------------------------------------------------------------------------*\
File:
	ExecSlave.h

Description:
    Dichiarazione strutture e routines Protocollo Executive Slave

History:
	Date	   Aut	Note
    Apr 2016	MR   
\*--------------------------------------------------------------------------------------*/

#ifndef ExecSlave_H_
#define ExecSlave_H_

/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

// Timeout NoLink Executive Slave
#define		kProtExecSlaveTimeout			FIVESEC
	// Counter messaggi ricevuti con richiesta pendente, poi
	// no credito e termino la richiesta
#define		kProtExecSlaveMaxStatNoCredit	3
#define		kProtExecSlaveAuditBuffSize		5
	// Delay (msec) prima della risposta al Master
#define		kProtExecSlaveReplyDelay		0

typedef enum {
	kVendNone,							// No vend in progress
	kVendRq,							// We would like to vend something
	kVendRqSent,						// We have sent the vend request to Exec Master
	kVendGranted,						// Exec master has replied with a CmdVend confirmation
	kVendRejected						// Exec master has replied with a CmdStatus (vend deny)
} VendState;

typedef enum {
    kCreditPermDenied,                  // Vend permission denied
    kCreditPermGranted,                 // Vend permission granted
    kCreditPermWaiting                  // Vend permission being negotiated
} CreditPerm;                           // Credit vend permission

// Valori speciali di credito
enum {
    kCreditValueInvalid = 0xffff,
};

/*--------------------------------------------------------------------------------------*\
Strutture
\*--------------------------------------------------------------------------------------*/


typedef struct {
	uint8_t			addr;				// Address
	uint16_t			value;				// Value (credit values already converted in real units)
} ExecAuditDataEl;						// Audit data element


typedef struct {
    CreditValue     creditVal;			// Credit value
    uint8_t           dpPosition;         // Decimal point position to use to display credit value
    uint8_t           exactChange;    	// Exact change required (could become obsolete)
} CreditInfo;

typedef struct {
	uint16_t			baseUnitValue;
	uint8_t			scalingFact;
	uint8_t			decimalPoint;
	bool			exactChange;
} ExecCreditData;						// Credit data information

typedef struct {
	uint8_t			addr;				// Address
	uint8_t			value;				// Value (credit values in USF units)
} ExecAuditPrivEl;						// Audit private data element

typedef struct {
	uint8_t			scalingFact;		// Audit scaling factor
	uint8_t			putIx;				// Audit data put index
	uint8_t			getIx;				// Audit data get index
	ExecAuditPrivEl	data[kProtExecSlaveAuditBuffSize];
} ExecAuditData;

typedef struct {
	CreditValue		creditomancante;
	uint8_t			result;
} ExecVendReqResp;
	

typedef struct {
	VendState		vendState;				// State of the vend
	char			lastCharSent;			// Last char sent
	uint8_t			priceCode;				// Selection code or selnum
	uint8_t			dataCount;				// Counter data bytes ricevuti
	uint8_t			statusCnt;				// Count status messages received
	uint8_t			vendEnabled;			// false if vends are disabled
	uint8_t			masterUnavail;			// true if master unavailable
	uint8_t			cdChanged;				// true if cd changed
	uint8_t			haveCreditData;			// asserted after the 1st credit data received
	uint8_t			commActivity;			// used to determine if master is available
	uint8_t			identifyReq;			// used by ASU to request peripheral identify
	CreditInfo		ci;						// Current credit information
	ExecCreditData	cd;						// Last credit data received
	Tmr32Local		commTmr;				// Used to check comm line activity
	uint8_t			ExecSlvState;
	CreditInfo 		prDispCreditInfo;		// Credito usato per la funzione di Price Display
	Tmr32Local 		prDispTimer;			// Price Display Timer
	uint8_t			prDispStart;			// Price Display Start Function Flag 
	//uint8_t			RestoSubito;			Per ora usata flag Restosubito in gOrionConfVars
	uint8_t 			waitingRealVendEnd;		// In resto subito attendo la fine vendita reale
	CreditValue		lastSelValue;
	bool			VendInProgress;
	bool			EsitoVend;
	
	#if kProtExecSlaveAuditUnitSupport
	uint8_t			auditDataCount;			// Used to count audit data bytes received
	ExecAuditData	ad;						// last audit data received
	#endif
	#if	  kProtExecSlaveReplyDelay
	bool			sendIt;
	Tmr32Local		delayReplyTmr;			// Used to delay replies
	#endif

} ExecSlaveVars;



// Peripheral identifiers
typedef enum {
	kExecPeripheralIdVMC	= (1<<5),				// Vending machine controller
	kExecPeripheralIdAU		= (2<<5),				// Audit unit
	kExecPeripheralIdCPU	= (3<<5)				// Cashless payment unit
} ExecPeripheralId;

// Masks to extract fields from an executive char
typedef enum {
	kExecDataMask			= 0x0f,
	kExecModeMask			= 0x10,
	kExecPeripheralIdMask	= 0xe0
} ExecCharMask;

typedef enum {
	kExecACK			= 0x00,						// Positive acknowledge / default response
	kExec1stReserved	= 0xfb,						// 251
	kExecCPP			= 0xfb,						// Command failed
	kExecRsrv1			= 0xfc,
	kExecRsrv2			= 0xfd,
	kExecSyncLost		= 0xfe,						// master/slave sync lost
	kExecNoVendRq		= 0xfe,						// no vend requested
	kExecPNAK			= 0xff						// Peripheral negative ack
} ExecReservedChar;

#define ExecCharIsCmd(ch) ((ch)&kExecModeMask)
	// Check if a char represents a command

#define ExecCharIsRsrv(ch) ((uint8_t)(ch)>=kExec1stReserved)
	// Check if a char represents reserved character


//--------------------------------------------------------------------------------------
//VMC Application layer	
//--------------------------------------------------------------------------------------

// VMC command codes
typedef enum {
	kExecVMCCmdUnused0,
	kExecVMCCmdStatus,
	kExecVMCCmdCredit,
	kExecVMCCmdVend,
	kExecVMCCmdAudit,
	kExecVMCCmdSendAuditAddr,
	kExecVMCCmdSendAuditData,
	kExecVMCCmdIdentify,
	kExecVMCCmdAcceptData,
	kExecVMCCmdDataSync,
	kExecVMCCmdUnused10,
	kExecVMCCmdUnused11,
	kExecVMCCmdUnused12,
	kExecVMCCmdUnused13,
	kExecVMCCmdUnused14,
	kExecVMCCmdNACK,
	kExecVMCCmdUnknown	= 0xf0
} ExecVMCCmdCode;

// VMC status bits
typedef enum {
	kExecVMCStatAuditMask	= 0x1f,
	kExecVMCStatVendInhibit	= 0x40,
	kExecVMCStatFreeVendRq	= 0x80
} ExecVMCStatMask;

// VMC status bits
typedef enum {
	kExecVMCVReplyAuditMask		= 0x1f,
	kExecVMCVReplyVendFailed	= 0x80
} ExecVMCVReplyMask;


//--------------------------------------------------------------------------------------
// Audit Storage Unit Application layer	
//--------------------------------------------------------------------------------------

// ASU command codes
typedef enum {
	kExecASUCmdUnused0,
	kExecASUCmdStatus,
	kExecASUCmdDataSync,
	kExecASUCmdNACK= 15
} ExecASUCmdCode;

// ASU status bits
typedef enum {
	kExecASUReplyAuditReqMask= 0x01,
	kExecASUReplyIdentifyReqMask= 0x02
} ExecVMCReplyMask;

//--------------------------------------------------------------------------------------
//Cashless Payment Peripheral Application layer	
//--------------------------------------------------------------------------------------

// CPP command codes
typedef enum {
	kExecCPPCmdUnused0,
	kExecCPPCmdStatus,
	kExecCPPCmdReadcard,
	kExecCPPCmdSendData,
	kExecCPPCmdAcceptData,
	kExecCPPCmdDecrement,
	kExecCPPCmdReInstate,
	kExecCPPCmdReturnCard,
	kExecCPPCmdDataSync,
	kExecCPPCmdAudit,
	kExecCPPCmdSendAddr,
	kExecCPPCmdSendData_,
	kExecCPPCmdIdentify,
	kExecCPPCmdDispData,
	kExecCPPCmdUnused14,
	kExecCPPCmdNACK
} ExecCPPCmdCode;


/*--------------------------------------------------------------------------------------*\
Executive slave routines
\*--------------------------------------------------------------------------------------*/

static void ExecSlaveTx(char ch);
static void SendCharToRR(uint8_t data);
static void ExecSlaveUpdateCreditInfo(void);
static void ExecSlaveCommCheck(void);
static uint8_t ExecSlaveCvtPrice(CreditValue price);
static void ExecSlaveCommTmrStart(void);
static CreditPerm CreditHndExecVendAllowed(CreditValue* value, CreditInfo* creditInfo);

void	ExecSlave_VendSuccess(void);
void	ExecSlave_VendFail(void);
void 	ExecSlaveInit(void);
void	ExecSlaveDataInd(uint8_t);
void 	ExecSlaveErrInd(uint8_t);
void 	EXEC_Slave_Task(void);


bool ExecSlaveGetCreditInfo(CreditInfo* creditInfo);
	/* Get info about current credit.
	Returns true if credit info changed since last call. */

bool ExecSlaveAuditDataGet(ExecAuditDataEl* ade);
	// Get next audit data element received
	// Return true if a data element has been copied in *ade
	// Return false in no more data elements

void ExecSlaveAuditUSFSet(uint8_t usf);
	// Set default Unit Scaling Factor for Audit Storage Unit 
	// May be used to set a default USF which is used until the actual USF is received

//bool ExecSlaveVendRequest(uint8_t Tasto);
void ExecSlaveVendRequest(ExecVendReqResp* risul, uint8_t numtasto);
	// Predispone una vend request se i parametri sono corretti

CreditPerm ExecSlaveVendAllowed(CreditInfo* creditInfo);
	/* Check if the last vend request has been accepted. 
	In creditInfo (if not nil) put current credit information */

void ExecSlaveVendCompleted(void);
	/* Confirm success of the last vend requested */

void ExecSlaveVendFailed(void);
	/* Signal failure of the last vend requested */

void ExecSlaveVendEnable(bool inEnableVend);
	/* Enable/disable vends */

CreditValue ExecSlaveMaxCredit(void);
	/* Return maximum credit value representable with current protocol options */


#endif		// ExecSlave_H_


