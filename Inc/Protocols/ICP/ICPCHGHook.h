/*--------------------------------------------------------------------------------------*\
File:
    ICPCHGHook.h

Description:
    Include file con definizioni e strutture per il file ICPCHGHook.
    

History:
    Date       Aut  Note
    Set 2012 	MR   
\*--------------------------------------------------------------------------------------*/

#ifndef __ICPCHGHook_h
#define __ICPCHGHook_h

#define CHG_EN 		1
#define CHG_DIS 	2

void ICPChgHookEnDis(void);
void CheckChgCashType(void);
void  ICPChgHookTaskInit(void);
void  Chg_Task(void);
bool ICPCHGHook_Change(void);
void ICPCHGHook_SetEscrow(void);
void  ICPCHGHook_SetPaidoutAmount(CreditCoinValue payoutVal);
void ICPCHGHook_SetPayoutComplete(CreditCoinValue paidoutVal);
void ICPCHGHook_SetCoinsDeposited(ET_CHGPOLLCOINDEST coinDest, uint8_t coinType, CreditCoinValue coinValue);
void ICPCHGHook_SetDPP(uint8_t inDPP);
bool ICPCHGHook_SetExactChange(uint8_t MinCoinNum);
void ICPCHGHook_SetReady(bool avail);
uint32_t CHGHook_GetCoinTubeAmount(uint8_t NumCoin, uint8_t ix);
uint8_t  CHGHook_GetNumCoinInTube(uint8_t CoinType);
void ICPCHGHook_SetCoinsDispMan(uint8_t nCoinsDispensed, uint8_t coinType);
void ICPCHGHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen);
void ICPCHGHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPCHGHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPCHGHook_SetSWVersion(uint16_t nSwVersion);
void  DisattivaEnDisCHG(void);

void ICPCHGHook_SetCountryCode(uint16_t countryCode);
void ICPCHGHook_SetCurrency(uint16_t currency);
/////void ICPCHGHook_SetCoinTypeInTube(uint16_t coinInTubeMask);
/////void ICPCHGHook_SetTubeStatus(uint8_t *ptTubeStatus);
void ICPCHGHook_SetPayout(bool fAvailable);
void ICPCHGHook_SetExtDiag(bool fAvailable);
void ICPCHGHook_SetManualFill(bool fAvailable);
void ICPCHGHook_SetFTL(bool fAvailable);
void ICPCHGHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen);
void ICPCHGHook_SetTubesFull(bool fTubesFull);

bool ICPCHGHook_GetPayout(void);
bool ICPCHGHook_GetExtDiag(void);
bool ICPCHGHook_GetManualFill(void);
bool ICPCHGHook_GetFTL(void);

#endif	// __ICPCHGHook_h

