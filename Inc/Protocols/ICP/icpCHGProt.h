#ifndef __icpCHGProt__
#define __icpCHGProt__

#include "icpCHG.h"
#include "ICPGlobal.h"
#include "ICPProt.h"

#pragma pack(1)


#define ICP_CHG_CMD_RESET 0
#define ICP_CHG_CMD_STATUS 1
#define ICP_CHG_CMD_TUBESTATUS 2
#define ICP_CHG_CMD_POLL 3
#define ICP_CHG_CMD_COINTYPE 4
#define ICP_CHG_CMD_DISPENSE 5
#define ICP_CHG_CMD_EXP 7

#define ICP_CHG_CMD_EXPREQUESTID 0
#define ICP_CHG_CMD_EXPENABLEOPT 1
#define ICP_CHG_CMD_EXPPAYOUTVAL 2
#define ICP_CHG_CMD_EXPPAYOUTSTATUS 3
#define ICP_CHG_CMD_EXPPAYOUTPOLL 4
#define ICP_CHG_CMD_EXPDIAGSTATUS 5
#define ICP_CHG_CMD_EXPMANFILLREP 6
#define ICP_CHG_CMD_EXPMANPAYOUTREP 7
#define ICP_CHG_CMD_EXPDIAG 0xFF

#if (_IcpMaster_ == true)
	#define ICP_CHG_INACTIVE 0
	#define ICP_CHG_DISABLED 1
	#define ICP_CHG_ENABLED 2
	#define ICP_CHG_PAYOUT 3

	#define ICP_CHG_INACTIVE_TXRESET 0
	#define ICP_CHG_INACTIVE_WAITINIT 1
	#define ICP_CHG_INACTIVE_TXPOLL_1 2
	#define ICP_CHG_INACTIVE_TXPOLL_2 3

	#define ICP_CHG_DISABLED_TXSTATUS 0
	#define ICP_CHG_DISABLED_TXEXP00 1
	#define ICP_CHG_DISABLED_TXEXP01 2
	#define ICP_CHG_DISABLED_TXEXP05 3
	#define ICP_CHG_DISABLED_TXTUBESTATUS 4
	#define ICP_CHG_DISABLED_TXCOINTYPE 5
	#define ICP_CHG_DISABLED_TXPOLL 6
	#define ICP_CHG_DISABLED_TXDISPENSE 7
	#define ICP_CHG_DISABLED_TXCOINTYPE1 8
	#define ICP_CHG_DISABLED_TXEXPDIAG 9
	#define ICP_CHG_DISABLED_TXCOINTYPE2 10

	#define ICP_CHG_ENABLED_TXPOLL 0
	#define ICP_CHG_ENABLED_TXTUBESTATUS 1
	#define ICP_CHG_ENABLED_TXCOINTYPE 2
	#define ICP_CHG_ENABLED_TXDISPENSE 3
	#define ICP_CHG_ENABLED_WAITENDDISPENSE 4
	#define ICP_CHG_ENABLED_TXCOINTYPE1 5
	#define ICP_CHG_ENABLED_TXEXPDIAG 6
	#define ICP_CHG_ENABLED_TXEXP05 7
	#define ICP_CHG_ENABLED_TXEXP06 8
	#define ICP_CHG_ENABLED_TXEXP07 9


	#define ICP_CHG_PAYOUT_TXEXP02 0
	#define ICP_CHG_PAYOUT_TXEXP03 1
	#define ICP_CHG_PAYOUT_TXEXP04 2
	#define ICP_CHG_PAYOUT_TXTUBESTATUS1 3
	#define ICP_CHG_PAYOUT_CHECKTXDISPENSE 4
	#define ICP_CHG_PAYOUT_TXDISPENSE 5
	#define ICP_CHG_PAYOUT_TXPOLL 6
	#define ICP_CHG_PAYOUT_TXPOLL2 7
	#define ICP_CHG_PAYOUT_TXPOLL3 8
	#define ICP_CHG_PAYOUT_TXTUBESTATUS2 9
	#define ICP_CHG_PAYOUT_ENDDISPENSE 10
	#define ICP_CHG_PAYOUT_TXTUBESTATUS3 11

#define ICP_CHG_PAYOUT_TXTUBESTATUS4 12

	#define ICP_CHG_NCOINSEXCH 3

	typedef enum  {
		ICP_CHGINFO_NONE = 0,
		ICP_CHGINFO_EXCH = 0x01,
		ICP_CHGINFO_ESCROW = 0x02,
		ICP_CHGINFO_KEY_STATE = 0x04
	} ET_CHGINFO;

#else
	#define ICP_CHG_ENABLED 0
#endif

#define ICP_CHG_RESPTYPE_STATUS (ICP_CHG_RESPTYPE + 1)
#define ICP_CHG_RESPTYPE_EXP00 (ICP_CHG_RESPTYPE + 2)
#define ICP_CHG_RESPTYPE_EXP05 (ICP_CHG_RESPTYPE + 3)
#define ICP_CHG_RESPTYPE_TUBESTATUS (ICP_CHG_RESPTYPE + 4)
#define ICP_CHG_RESPTYPE_ESCROW (ICP_CHG_RESPTYPE + 5)
#define ICP_CHG_RESPTYPE_BUSY (ICP_CHG_RESPTYPE + 6)
#define ICP_CHG_RESPTYPE_INHIBIT (ICP_CHG_RESPTYPE + 7)
#define ICP_CHG_RESPTYPE_EXP03 (ICP_CHG_RESPTYPE + 8)
#define ICP_CHG_RESPTYPE_EXP04 (ICP_CHG_RESPTYPE + 9)
#define ICP_CHG_RESPTYPE_EXPFF (ICP_CHG_RESPTYPE + 10)
#define ICP_CHG_RESPTYPE_EXP06 (ICP_CHG_RESPTYPE + 11)
#define ICP_CHG_RESPTYPE_EXP07 (ICP_CHG_RESPTYPE + 12)
#define	ICP_CHG_RESPTYPE_EXP05_10_20 (ICP_CHG_RESPTYPE + 13)


//-------------------------------------------------
// C H G  Info
//-------------------------------------------------

typedef enum {
	kSlvChgInfoMask_None =			0,
	kSlvChgInfoMask_InitCompleted = 0x01
} SlvChgInfoMask;

typedef struct {
	uint8_t nFLevel;
	uint16_t nCCode;
	uint8_t nUSF;
	uint8_t nDPP;
	uint16_t nCoinTypeInTubes;
	uint8_t aCoinTypeVal[ICP_MAX_COINTYPES];
} T_CHG_STATUS;

typedef struct {
	uint16_t nCoinTypeTubeFull;
	uint8_t aCoinsInTube[ICP_MAX_COINTYPES];
} T_CHG_TUBESTATUS;

typedef struct {
	uint16_t nCoinEnable;
	uint16_t nManDispenseEnable;
} T_CHG_COINTYPE;

typedef struct {
	uint8_t aManufactCode[SIZE_MANUFACTURER];
	uint8_t aSerialNumber[SIZE_SERIAL];
	uint8_t aModelNumber[SIZE_MODEL];
	uint16_t nSwVersion;
	uint32_t nFOptions;
} T_CHG_PERIPHID;

typedef struct {
	uint32_t nFOptions;
	T_CHG_STATUS Status;
	T_CHG_TUBESTATUS TubeStatus;
	T_CHG_COINTYPE CoinTypeState;
	T_CHG_PERIPHID ChgInfo;
#if (_IcpMaster_ == true)
	uint8_t nWaitInitCnt;
	uint8_t nCmdState;
	uint8_t nCmdType;
	uint8_t aCmdParameters[6];
#endif
	uint8_t nInfoMask;
	uint8_t chg_state;	
	uint8_t chg_exp_state;
	uint8_t send_exp_count;			//ABELE se livello 3, ogni 10 poll invia anche EXP Send Diagnostic Status (0F-05)
} T_ICP_CHGINFO;



//-------------------------------------------------
// C H G  Command/Response Structures
//-------------------------------------------------

typedef struct {
	uint8_t nCmd;
} T_CHGCMD_RESET;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CHGRESP_RESET;

typedef struct {
	uint8_t nCmd;
} T_CHGCMD_STATUS;

typedef union {
	uint8_t nodata;
	T_CHG_STATUS Status;
} T_CHGRESP_STATUS;

typedef struct {
	uint8_t nCmd;
} T_CHGCMD_TUBESTATUS;

typedef union {
	uint8_t nodata;
	T_CHG_TUBESTATUS TubeStatus;
} T_CHGRESP_TUBESTATUS;

typedef struct {
	uint8_t nCmd;
} T_CHGCMD_POLL;

typedef union {
	uint8_t nodata;
	uint8_t aPollData[ICP_MAX_FRAMELEN];
} T_CHGRESP_POLL;

typedef struct {
	uint8_t nCmd;
	T_CHG_COINTYPE CoinTypeState;
} T_CHGCMD_COINTYPE;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CHGRESP_COINTYPE;

typedef struct {
	uint8_t nCmd;
	uint8_t nCoinsCoinType;
} T_CHGCMD_DISPENSE;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CHGRESP_DISPENSE;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CHGCMD_EXP00;

typedef union {
	uint8_t nodata;
	T_CHG_PERIPHID PeriphID;
} T_CHGRESP_EXP00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint32_t nFOptions;
} T_CHGCMD_EXP01;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CHGRESP_EXP01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nPayoutVal;
} T_CHGCMD_EXP02;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CHGRESP_EXP02;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CHGCMD_EXP03;

typedef union {
	uint8_t nodata;
	uint8_t aCoinsPaidOut[ICP_MAX_COINTYPES];
} T_CHGRESP_EXP03;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CHGCMD_EXP04;

typedef union {
	uint8_t nodata;
	uint8_t nPaidOutVal;
} T_CHGRESP_EXP04;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CHGCMD_EXP05;

typedef union {
	uint8_t nodata;
	uint8_t aStatusCode[16];
} T_CHGRESP_EXP05;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CHGCMD_EXP06;

typedef union {
	uint8_t nodata;
	uint8_t aCoinsManFilled[ICP_MAX_COINTYPES];
} T_CHGRESP_EXP06;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CHGCMD_EXP07;

typedef union {
	uint8_t nodata;
	uint8_t aCoinsManPayout[ICP_MAX_COINTYPES];
} T_CHGRESP_EXP07;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t aDiagData[ICP_MAX_FRAMELEN - 2];
} T_CHGCMD_EXPFF;

typedef union {
	uint8_t nodata;
	uint8_t aDiagData[ICP_MAX_FRAMELEN];
} T_CHGRESP_EXPFF;


void ICPProt_CHGReset(void);
void ICPProt_CHGStatus(void);
void ICPProt_CHGTubeStatus(void);
void ICPProt_CHGPoll(void);
void ICPProt_CHGCoinType(void);
void ICPProt_CHGDispense(void);
void ICPProt_CHGExp00(void);
void ICPProt_CHGExp01(void);
void ICPProt_CHGExp02(void);
void ICPProt_CHGExp03(void);
void ICPProt_CHGExp04(void);
void ICPProt_CHGExp05(void);
void ICPProt_CHGExp06(void);
void ICPProt_CHGExp07(void);
void ICPProt_CHGExpFF(void);

uint8_t ICPProt_CHGResetResp(void);
uint8_t ICPProt_CHGStatusResp(void);
uint8_t ICPProt_CHGTubeStatusResp(void);
uint8_t ICPProt_CHGPollResp(void);
uint8_t ICPProt_CHGCoinTypeResp(void);
uint8_t ICPProt_CHGDispenseResp(void);
uint8_t ICPProt_CHGExp00Resp(void);
uint8_t ICPProt_CHGExp01Resp(void);
uint8_t ICPProt_CHGExp02Resp(void);
uint8_t ICPProt_CHGExp03Resp(void);
uint8_t ICPProt_CHGExp04Resp(void);
uint8_t ICPProt_CHGExp05Resp(void);
uint8_t ICPProt_CHGExp06Resp(void);
uint8_t ICPProt_CHGExp07Resp(void);
uint8_t ICPProt_CHGExpFFResp(void);

void CHGInit(uint8_t nSlaveType);
void CHGInactive(void);
void CHGDisabled(void);
void CHGEnabled(void);
void CHGPayout(void);



#if (_IcpMaster_ == true)
bool ICPProt_CHGGetPayout(void);
bool ICPProt_CHGGetExtDiag(void);
bool ICPProt_CHGGetManualFill(void);
bool ICPProt_CHGGetFTL(void);
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
typedef struct {
	uint8_t aPollData[16];
	uint8_t nRdData;
	uint8_t nWrData;
	uint8_t nData;
} T_ICP_CHGPOLLDATA;

void ICPProt_CHGClearPollData(void);
void ICPProt_CHGResetPollData(void);
uint8_t ICPProt_CHGGetPollData(uint8_t *ptData, bool fAllData);
void ICPProt_CHGSetCoinTypeState(void);

bool ICPProt_CHGIsPayoutEnable(void);
bool ICPProt_CHGIsExtDiagEnable(void);
bool ICPProt_CHGIsManFillEnable(void);
#endif


extern T_ICP_CHGINFO ICPCHGInfo;
#endif
