#ifndef __icpCPCProt__
#define __icpCPCProt__

#include "icpCPC.h"
#include "icpProt.h"
#include "icpVMCProt.h"


//#pragma pack(1)


#define ICP_CPC_CMD_RESET 0
#define ICP_CPC_CMD_SETUP 1
#define ICP_CPC_CMD_POLL 2
#define ICP_CPC_CMD_VEND 3
#define ICP_CPC_CMD_READER 4
#define ICP_CPC_CMD_REVALUE 5
#define ICP_CPC_CMD_EXP 7

//----  Questi sono SubCommands ----
#define ICP_CPC_CMD_SETUPID 0
#define ICP_CPC_CMD_SETUPMAXMINPRICE 1
#define ICP_CPC_CMD_VENDREQUEST 0
#define ICP_CPC_CMD_VENDCANCEL 1
#define ICP_CPC_CMD_VENDSUCCESS 2
#define ICP_CPC_CMD_VENDFAILURE 3
#define ICP_CPC_CMD_VENDCOMPLETE 4
#define ICP_CPC_CMD_VENDCASHSALE 5
#define ICP_CPC_CMD_NEGVENDREQUEST 6
#define ICP_CPC_CMD_READERDISABLE 0
#define ICP_CPC_CMD_READERENABLE 1
#define ICP_CPC_CMD_READERCANCEL 2
#define ICP_CPC_CMD_READERDATAENTRY 3
#define ICP_CPC_CMD_REVALUEREQUEST 0
#define ICP_CPC_CMD_REVALUELIMIT 1
#define ICP_CPC_CMD_EXPDIAG 0xFF
#define ICP_CPC_CMD_EXPREQUESTID 0
#define ICP_CPC_CMD_EXPREADUSERFILE 1
#define ICP_CPC_CMD_EXPWRITEUSERFILE 2
#define ICP_CPC_CMD_EXPWRITETIMEDATE 3
#define ICP_CPC_CMD_EXPENABLEOPT 4


#define ICP_CPC_INACTIVE 0
#define ICP_CPC_DISABLED 1
#define ICP_CPC_ENABLED 2
#define ICP_CPC_SESSIDLE 3
#define ICP_CPC_VEND 4
#define ICP_CPC_REVALUE 5

typedef enum {
	ICP_CPC_JUSTRESET = 0,
	ICP_CPC_SETUPID = 0x01,
	ICP_CPC_DISPREQ = 0x02,
	ICP_CPC_BEGSESS = 0x03,
	ICP_CPC_SESSCANC = 0x04,
	ICP_CPC_VENDAPP = 0x05,
	ICP_CPC_VENDDENY = 0x06,
	ICP_CPC_ENDSESS = 0x07,
	ICP_CPC_CANCELLED = 0x08,
	ICP_CPC_PERIPHID = 0x09,
	ICP_CPC_MALFUNCT = 0x0A,
	ICP_CPC_OUTSEQ = 0x0B,
	ICP_CPC_BUSY = 0x0C,
	ICP_CPC_REVALAPP = 0x0D,
	ICP_CPC_REVALDENY = 0x0E,
	ICP_CPC_REVALLIMIT = 0x0F,
	ICP_CPC_USERFILE = 0x10,
	ICP_CPC_TIMEREQ = 0x11,
	ICP_CPC_DATAENTRYREQ = 0x12,
	ICP_CPC_DATAENTRYCANCEL = 0x13,
	ICP_CPC_FTLREQTORCV = 0x1B,
	ICP_CPC_FTLRETRYDENY= 0x1C,
	ICP_CPC_FTLSENDBLOCK = 0x1D,
	ICP_CPC_FTLOKTOSEND = 0x1E,
	ICP_CPC_FTLREQTOSEND = 0x1F,
	ICP_CPC_DIAG = 0xFF
} ET_CPCPOLLSTATUS;

typedef enum  {
	ICP_CPCINFO_NONE = 0,
	ICP_CPCINFO_SEQCMDERR = 1,
	ICP_CPCINFO_OUTOFSEQ = 2
} ET_CPCINFO;

#define IsSetCPCInfoMask(mask)	(ICPCPCInfo.nInfoMask & mask)
//#define SetCPCInfoMask(mask)	ICPCPCInfo.nInfoMask |= mask
#define SetCPCInfoMask(mask)	{ __DI(); ICPCPCInfo.nInfoMask |= mask; __EI(); }
#define ResetCPCInfoMask(mask)	ICPCPCInfo.nInfoMask &= ~mask


//***********************
//	RM81.2005_RPSOLUTIONS
//			MSTNOACK_SLVNORETRY (COINCO_2P / OSCARMDBONLY)
//***********************
#define CPCSetLastEvent(mask)	{ICPCPCInfo.lastEventMask = mask;}
#define CPCGetLastEvent				ICPCPCInfo.lastEventMask
//	RM81.2005_RPSOLUTIONS
//***********************


//#if (_IcpMaster_ == true)
	#define ICP_CPC_INACTIVE_TXRESET 				0
	#define ICP_CPC_INACTIVE_WAITINIT 				1
	#define ICP_CPC_INACTIVE_TXPOLL_1 				2
	#define ICP_CPC_INACTIVE_TXPOLL_2 				3

	#define ICP_CPC_DISABLED_TXSETUPID 				0
	#define ICP_CPC_DISABLED_WAITSETUPID 			1
	#define ICP_CPC_DISABLED_TXSETUPMMPRICE_1 		2
	#define ICP_CPC_DISABLED_TXEXPID 				3
	#define ICP_CPC_DISABLED_WAITEXPID 				4
	#define ICP_CPC_DISABLED_TXEXPFOPT 				5
	#define ICP_CPC_DISABLED_WAITEXPFOPT 			6
	#define ICP_CPC_DISABLED_TXSETUPMMPRICE_2 		7
	#define ICP_CPC_DISABLED_TXPOLL 				8
	#define ICP_CPC_DISABLED_TXVENDCASH 			9
	#define ICP_CPC_DISABLED_TXREADERENABLE_1 		10
	#define ICP_CPC_DISABLED_TXREADERENABLE_2 		11
	#define ICP_CPC_DISABLED_TXEXPTIMEDATE 			12

	#define ICP_CPC_ENABLED_TXPOLL 0
	#define ICP_CPC_ENABLED_WAITENDSESSION 1
	#define ICP_CPC_ENABLED_TXSESSCOMP 2
	#define ICP_CPC_ENABLED_TXVENDCASH 3
	#define ICP_CPC_ENABLED_TXREADERDISABLE 4
	#define ICP_CPC_ENABLED_TXREVALLIM 5
	#define ICP_CPC_ENABLED_TXEXPTIMEDATE 6
	#define ICP_CPC_ENABLED_WAITREVALUELIMIT 7
	#define ICP_CPC_ENABLED_TXREADERCANCEL 8
	#define ICP_CPC_ENABLED_WAITCANCELLED 9

	#define ICP_CPC_SESSIDLE_TXPOLL 0
	#define ICP_CPC_SESSIDLE_WAITENDSESSION 1
	#define ICP_CPC_SESSIDLE_TXSESSCOMP 2
	#define ICP_CPC_SESSIDLE_TXREADERDISABLE 3
	#define ICP_CPC_SESSIDLE_TXEXPTIMEDATE 4
	#define ICP_CPC_SESSIDLE_TXREVALLIM 5
	#define ICP_CPC_SESSIDLE_TXVENDCASH 6
	#define ICP_CPC_SESSIDLE_WAITREVALUELIMIT 7
	#define ICP_CPC_SESSIDLE_CHKMULTIVEND 8

	#define ICP_CPC_REVALUE_TXREVALREQ 0
	#define ICP_CPC_REVALUE_TXPOLL 1
	#define ICP_CPC_REVALUE_TXSESSCOMP 2
	#define ICP_CPC_REVALUE_WAITENDSESSION 3

	#define ICP_CPC_VEND_TXVENDREQ 0
	#define ICP_CPC_VEND_TXNEGVENDREQ 1
	#define ICP_CPC_VEND_TXPOLL 2
	#define ICP_CPC_VEND_WAITVENDAPPDENY 3
	#define ICP_CPC_VEND_WAITVENDSUCCFAIL 4
	#define ICP_CPC_VEND_WAITENDSESSION 5
	#define ICP_CPC_VEND_TXVENDSUCC 6
	#define ICP_CPC_VEND_TXVENDFAILED 7
	#define ICP_CPC_VEND_TXVENDCANCEL 8
	#define ICP_CPC_VEND_TXSESSCOMP 9
	#define ICP_CPC_VEND_TXVENDSUCC1 10
	#define ICP_CPC_VEND_WAITENDVENDFAIL 11
	#define ICP_CPC_VEND_WAITENDVENDSUCC 12
	#define ICP_CPC_VEND_WAITENDVENDSUCC1 13
//#else
	#define ICP_CPC_INACTIVE_WAITSETUP 0
	#define ICP_CPC_INACTIVE_RXSETUPID 1
	#define ICP_CPC_INACTIVE_RXSETUPMMP 2

	#define ICP_CPC_VEND_RXVENDREQ 0
	#define ICP_CPC_VEND_RXVENDCANCEL 1
	#define ICP_CPC_VEND_RXVENDSUCC 2
	#define ICP_CPC_VEND_RXVENDFAILED 3
	#define ICP_CPC_VEND_RXSESSCOMP 4
	#define ICP_CPC_VEND_RXCASHSALE 5
	#define ICP_CPC_VEND_RXNEGVENDREQ 6
	//#define ICP_CPC_VEND_WAITVENDSUCCFAIL 7
	#define ICP_CPC_VEND_ENDVEND 8
//#endif


#define ICP_CPC_RESPTYPE_SETUPID (ICP_CPC_RESPTYPE + 1)
#define ICP_CPC_RESPTYPE_DISPREQ (ICP_CPC_RESPTYPE + 2)
#define ICP_CPC_RESPTYPE_BEGSESS (ICP_CPC_RESPTYPE + 3)
#define ICP_CPC_RESPTYPE_SESSCANC (ICP_CPC_RESPTYPE + 4)
#define ICP_CPC_RESPTYPE_VENDAPP (ICP_CPC_RESPTYPE + 5)
#define ICP_CPC_RESPTYPE_VENDDENY (ICP_CPC_RESPTYPE + 6)
#define ICP_CPC_RESPTYPE_ENDSESS (ICP_CPC_RESPTYPE + 7)
#define ICP_CPC_RESPTYPE_CANCELLED (ICP_CPC_RESPTYPE + 8)
#define ICP_CPC_RESPTYPE_PERIPHID (ICP_CPC_RESPTYPE + 9)
#define ICP_CPC_RESPTYPE_MALFUNCT (ICP_CPC_RESPTYPE + 10)
#define ICP_CPC_RESPTYPE_REFUNDERR (ICP_CPC_RESPTYPE + 11)
#define ICP_CPC_RESPTYPE_OUTSEQ (ICP_CPC_RESPTYPE + 12)
#define ICP_CPC_RESPTYPE_BUSY (ICP_CPC_RESPTYPE + 13)
#define ICP_CPC_RESPTYPE_REVALAPP (ICP_CPC_RESPTYPE + 14)
#define ICP_CPC_RESPTYPE_REVALDENY (ICP_CPC_RESPTYPE + 15)
#define ICP_CPC_RESPTYPE_REVALLIMIT (ICP_CPC_RESPTYPE + 16)
#define ICP_CPC_RESPTYPE_USERFILE (ICP_CPC_RESPTYPE + 17)
#define ICP_CPC_RESPTYPE_TIMEREQ (ICP_CPC_RESPTYPE + 18)
#define ICP_CPC_RESPTYPE_EXPFF (ICP_CPC_RESPTYPE + 19)


//-------------------------------------------------
// C P C  Info
//-------------------------------------------------

typedef enum  {
	ICP_CPCOPT_NONE 			= 0x00,
	ICP_CPCOPT3_FTL 			= 0x01,
	ICP_CPCOPT3_CREDIT32 		= 0x02,
	ICP_CPCOPT3_MULTICURRENCY 	= 0x04,
	ICP_CPCOPT3_NEGVEND 		= 0x08,
	ICP_CPCOPT3_DATAENTRY 		= 0x10
} ET_CPCPOLL09OPT;

typedef struct {
	uint8_t 	nFLevel;
	uint16_t 	nCCode; 
	uint8_t 	nUSF; 
	uint8_t 	nDPP; 
	uint8_t 	nTmMNR; 
	uint8_t 	nFOptions; 
} T_CPC_SETUP00;

typedef struct {
	uint8_t 	nMsgTime;
	uint8_t 	aMsg[32];
} T_CPC_DISPREQ;

typedef struct {
	CPCCreditType_L1 	nFunds;
} T_CPC_FUNDINFO_L1;

typedef struct {
	CPCCreditType_L2 	nFunds;
	uint32_t 				nPaymentID;
	uint8_t 				nPaymentType;
	uint16_t 				nPaymentData;
} T_CPC_FUNDINFO_L2;

typedef struct {
	CPCCreditType_L3 	nFunds;
	uint32_t 				nPaymentID;
	uint8_t 				nPaymentType;
	uint16_t 				nPaymentData;
	uint16_t 				nUserLanguage;
	uint16_t 				nUserCurrency;
	uint8_t 				nUserOptions;
} T_CPC_FUNDINFO_L3;

typedef struct {
	CPCCreditType_L1 	nVendAmount;
} T_CPC_VENDAPP_L1;

typedef struct {
	CPCCreditType_L3 	nVendAmount;
	uint16_t 				nVendCurrency;
} T_CPC_VENDAPP_L3;

typedef struct {
	uint8_t 	aManufactCode[3];				// Len SIZE_MANUFACTURER
	uint8_t 	aSerialNumber[12];				// Len SIZE_SERIAL
	uint8_t 	aModelNumber[12];				// Len SIZE_MODEL
	uint16_t 	nSwVersion;
} T_CPC_PERIPHID_L1;

typedef struct {
	uint8_t 	aManufactCode[3];
	uint8_t 	aSerialNumber[12];
	uint8_t 	aModelNumber[12];
	uint16_t 	nSwVersion;
	uint32_t 	nFOptions;
} T_CPC_PERIPHID_L3;

typedef struct {
	CPCCreditType_L1 nLimitAmount;
} T_CPC_REVALLIM_L1;

typedef struct {
	CPCCreditType_L3 nLimitAmount;
	uint16_t nCurrencyCode;
} T_CPC_REVALLIM_L3;

typedef struct {
	uint8_t nUserFile;
	uint8_t nUserFileID;
	uint8_t nUserFileLen;
	uint8_t aUserData[ICP_MAX_FRAMELEN-3];
} T_CPC_USERFILE;

typedef struct {
	uint32_t 				nFOptions;
	T_CPC_SETUP00 		Setup00;
//	T_CPC_SETUP01 		Setup01;
	T_CPC_FUNDINFO_L3 	FundInfo;
	T_CPC_VENDAPP_L3 	VendApproved;
	T_CPC_REVALLIM_L3	RevalueLimit;
	uint8_t 				nWaitInitCnt;
	uint8_t 				nCmdState;
	uint8_t 				nCmdType;
	uint8_t 				aCmdParameters[10];
	//uint16_t 			nCurrencyCode;
	uint8_t 				cpc_state;
	uint32_t 				nInfoMask;
	uint32_t 				lastEventMask;
	T_CPC_PERIPHID_L1	CPCInfo;
} T_ICP_CPCINFO;



//-------------------------------------------------
// C P C  Command/Response Structures
//-------------------------------------------------

typedef struct {
	uint8_t nCmd;
} T_CPCCMD_RESET;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CPCRESP_RESET;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	T_VMC_SETUP00 Setup00;
} T_CPCCMD_SETUP00;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nSetup00;
		T_CPC_SETUP00 Setup00;
	} setup00;
} T_CPCRESP_SETUP00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	union {
		T_VMC_SETUP01_L1 L1;
		T_VMC_SETUP01_L3 L3;
	} Setup01;
} T_CPCCMD_SETUP01;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CPCRESP_SETUP01;

// T_CPCCMD_SETUP02 + T_CPCRESP_SETUP02 + ICP_CPC_RESPTYPE_CURRTYPES: Currency negoziation

typedef struct {
	uint8_t nCmd;
} T_CPCCMD_POLL;

typedef union {
	uint8_t nodata;
	uint8_t aPollData[ICP_MAX_FRAMELEN];
} T_CPCRESP_POLL;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nPoll02;
		T_CPC_DISPREQ DispReq;
	} poll02;
} T_CPCRESP_POLL02;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nPoll03;
		union {
			T_CPC_FUNDINFO_L1 L1;
			T_CPC_FUNDINFO_L2 L2;
			T_CPC_FUNDINFO_L3 L3;
		} FunfInfo;
	} poll03;
} T_CPCRESP_POLL03;

typedef union {
	uint8_t nodata;
	uint8_t nPoll04;
} T_CPCRESP_POLL04;

typedef union {
	uint8_t nodata;
	uint8_t nPoll07;
} T_CPCRESP_POLL07;

typedef union {
	uint8_t nodata;
	uint8_t nPoll08;
} T_CPCRESP_POLL08;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nPoll0A;
		uint8_t nErrorCode;
	} poll0a;
} T_CPCRESP_POLL0A;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nPoll0B;
#if _IcpCpcLevel_ >= 2
		uint8_t nCpcState;
#endif
	} poll0b;
} T_CPCRESP_POLL0B;

typedef union {
	uint8_t nodata;
	uint8_t nPoll0C;
} T_CPCRESP_POLL0C;

typedef union {
	uint8_t nodata;
	uint8_t nPoll11;
} T_CPCRESP_POLL11;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	union {
		T_VMC_VEND00_L1 L1;
		T_VMC_VEND00_L3 L3;
	} Vend;
} T_CPCCMD_VENDxx;

typedef union {
	uint8_t nodata;
	uint8_t nVendDenied;
	struct {
		uint8_t nVendApp;
		union {
			T_CPC_VENDAPP_L1 L1;
			T_CPC_VENDAPP_L3 L3;
		} VendApproved;
	} vendapp;
} T_CPCRESP_VEND00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CPCCMD_VEND01;

typedef union {
	uint8_t nodata;
	uint8_t nVendDenied;
} T_CPCRESP_VEND01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint16_t nSelNumber;
} T_CPCCMD_VEND02;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CPCRESP_VEND02;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CPCCMD_VEND03;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CPCRESP_VEND03;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CPCCMD_VEND04;

typedef union {
	uint8_t nodata;
	uint8_t nSessionComplete;
} T_CPCRESP_VEND04;

/*
typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	T_VMC_VEND00 Vend05;
} T_CPCCMD_VEND05;
*/

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CPCRESP_VEND05;

/*
typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	T_VMC_VEND00 Vend06;
} T_CPCCMD_VEND06;
*/

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CPCCMD_READER00;

typedef union {
	uint8_t nodata;
	uint8_t nReaderDisable;
} T_CPCRESP_READER00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CPCCMD_READER01;

typedef union {
	uint8_t nodata;
	uint8_t nReaderEnable;
} T_CPCRESP_READER01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CPCCMD_READER02;

typedef union {
	uint8_t nodata;
	uint8_t nReaderCancelled;
} T_CPCRESP_READER02;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	union {
		T_VMC_REVAL00_L1 L1;
		T_VMC_REVAL00_L3 L3;
	} Revalue;
} T_CPCCMD_REVAL00;

typedef union {
	uint8_t nodata;
	uint8_t nRevalDenied;
	uint8_t nRevalApp;
} T_CPCRESP_REVAL00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_CPCCMD_REVAL01;

typedef union {
	uint8_t nodata;
	uint8_t nRevalDenied;
	struct {
		uint8_t nRevalLimit;
		union {
			T_CPC_REVALLIM_L1 L1;
			T_CPC_REVALLIM_L3 L3;
		} RevalueLimit;
	} reval01;
} T_CPCRESP_REVAL01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	union {
		T_VMC_EXP00_L1 L1;
		T_VMC_EXP00_L3 L3;
	} Exp00;
} T_CPCCMD_EXP00;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nPeriphID;
		union {
			T_CPC_PERIPHID_L1 L1;
			T_CPC_PERIPHID_L3 L3;
		} PeriphID;
	} exp00;
} T_CPCRESP_EXP00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	T_VMC_EXP03 Exp03;
} T_CPCCMD_EXP03;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CPCRESP_EXP03;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint32_t nFOptions;
} T_CPCCMD_EXP04;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_CPCRESP_EXP04;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t aDiagData[ICP_MAX_FRAMELEN - 2];
} T_CPCCMD_EXPFF;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nExpFF;
		uint8_t aDiagData[ICP_MAX_FRAMELEN];
	} expff;
} T_CPCRESP_EXPFF;

/*
typedef struct {
	T_CPC_SETUP00 Setup00;
	T_CPC_FUNDINFO FundInfo;
	T_CPC_VENDAPP VendApproved;
	T_CPC_PERIPHID PeriphID;
	//T_CPC_MALFUNCTERR MalfunctErr;		// serve?!
	//T_CPC_CMDOUTSEQ CmdOutSequence;		// serve?!
	T_CPC_REVALLIM RevalueLimit;
	//T_CPC_USERFILE UserFile;				// serve?!
	T_CPC_DIAGINFO DiagInfo;
} T_ICP_CPCINFO;


enum ET_CPC_OPTFEATURES {
	CPC_OPT_REFUNDS,
	CPC_OPT_MULTIVEND,
	CPC_OPT_DISPLAY,
	CPC_OPT_CASHSALE,
	CPC_OPT_FTL,
	CPC_OPT_CREDIT32,
	CPC_OPT_MULTICURRENCY,
	CPC_OPT_NEGVEND,
	CPC_OPT_DATAENTRY
};
*/

void ICPProt_CPCReset(void);
void ICPProt_CPCSetup00(void);
void ICPProt_CPCSetup01(void);
void ICPProt_CPCPoll(void);
void ICPProt_CPCVend00(void);
void ICPProt_CPCVend01(void);
void ICPProt_CPCVend02(void);
void ICPProt_CPCVend03(void);
void ICPProt_CPCVend04(void);
void ICPProt_CPCVend05(void);
void ICPProt_CPCVend06(void);
void ICPProt_CPCReader00(void);
void ICPProt_CPCReader01(void);
void ICPProt_CPCReader02(void);
void ICPProt_CPCRevalue00(void);
void ICPProt_CPCRevalue01(void);
void ICPProt_CPCExpFF(void);
void ICPProt_CPCExp00(void);
void ICPProt_CPCExp01(void);
void ICPProt_CPCExp02(void);
void ICPProt_CPCExp03(void);
void ICPProt_CPCExp04(void);

uint8_t ICPProt_CPCResetResp(void);
uint8_t ICPProt_CPCSetup00Resp(void);
uint8_t ICPProt_CPCSetup01Resp(void);
uint8_t ICPProt_CPCPollResp(void);
uint8_t ICPProt_CPCPollResp0B(uint8_t nState);
uint8_t ICPProt_CPCVend00Resp(void);
uint8_t ICPProt_CPCVend01Resp(void);
uint8_t ICPProt_CPCVend02Resp(void);
uint8_t ICPProt_CPCVend03Resp(void);
uint8_t ICPProt_CPCVend04Resp(void);
uint8_t ICPProt_CPCVend05Resp(void);
uint8_t ICPProt_CPCVend06Resp(void);
uint8_t ICPProt_CPCReader00Resp(void);
uint8_t ICPProt_CPCReader01Resp(void);
uint8_t ICPProt_CPCReader02Resp(void);
uint8_t ICPProt_CPCRevalue00Resp(void);
uint8_t ICPProt_CPCRevalue01Resp(void);
uint8_t ICPProt_CPCExpFFResp(void);
uint8_t ICPProt_CPCExp00Resp(void);
uint8_t ICPProt_CPCExp01Resp(void);
uint8_t ICPProt_CPCExp02Resp(void);
uint8_t ICPProt_CPCExp03Resp(void);
uint8_t ICPProt_CPCExp04Resp(void);

void CPCInit(uint8_t nSlaveType);
void CPCInactive(void);
void CPCDisabled(void);
void CPCEnabled(void);
void CPCSessionIdle(void);
void CPCVend(void);
void CPCRevalue(void);

bool  CheckCPC_USF(void);

	bool ICPProt_CPCGetFTL(void);
	bool ICPProt_CPCGetCredit32(void);
	bool ICPProt_CPCGetMultiCurr(void);
	bool ICPProt_CPCGetNegVend(void);
	bool ICPProt_CPCGetDataEntry(void);

	bool ICPProt_CPCGetMultiVend(void);
	bool ICPProt_CPCGetDisplay(void);
	bool ICPProt_CPCGetCashSale(void);
	bool ICPProt_CPCGetRefund(void);
	bool ICPProt_CPCGetRevalue(void);


extern T_ICP_CPCINFO ICPCPCInfo;

#endif
