/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */



/*! \file MifareReaderLib.c
 *
 * Projekt: Object Oriented Reader Library Framework BAL component.
 *
 *  Source: MifareReaderLib.c
 * $Author: mha $
 * $Revision: 1.39 $
 * $Date: Fri Jun 30 11:02:45 2006 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */


/* Includes ------------------------------------------------------------------*/

/* For debugging purposes (printf) */
#include <stdio.h>		

#include "ORION.H"

/* Common headers */
#include <phcsBflHw1Reg.h>

/* Needed for any Operating Mode */
#include <phcsBflOpCtl.h>
#include <phcsBflOpCtl_Hw1Ordinals.h>


/* Needed for this Operating Mode */
#include <phcsBflI3P3A.h>
#include <phcsBflMfRd.h>


#include "MifareReaderLib.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	uint32_t	cryptuid;
extern	bool 		CompareBuffer (uint8_t *ptBuf1, uint8_t *ptBuf2, uint8_t bufLen);
extern	phcsBfl_Status_t CheckSiliconVersion(	phcsBflRegCtl_t* rc_reg_ctl,
								    uint8_t expectedVersion );

extern	phcsBfl_Status_t SetTimeOut(	phcsBflRegCtl_t *rc_reg_ctl, 
						    phcsBflOpCtl_t *rc_op_ctl, 
						    uint32_t aMicroSeconds, 
						    uint8_t aFlags);



/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

#define PH_ERR_BFL_NO_ANSWER_FROM_VERSION   (0x0055)

phcsBflBal_Hw1SerLinInitParams_t   bal_params;
phcsBflBal_t                       bal;
phcsBflRegCtl_ModRegParam_t        mrp;
phcsBflRegCtl_t                    rc_reg_ctl;
phcsBflOpCtl_t                     rc_op_ctl;

/* ISO14443-3A parameters       */
phcsBflI3P3A_ReqAParam_t           req_p;
phcsBflI3P3A_AnticollSelectParam_t sel_p;
phcsBflI3P3A_HaltAParam_t          hlt_p;
phcsBflI3P3A_t                     iso14443_3;
phcsBflMfRd_t                      mfrd;

/* Mifare parameters       */
phcsBflMfRd_CommandParam_t mfr_p;

/* Declaration of RCL Component Structures (auto), C variant: */
phcsBflIo_t             rcio;

/* Declaration of internal Component Structures: */
phcsBflRegCtl_SerHw1Params_t  rc_reg_ctl_params;
phcsBflIo_Hw1Params_t         rc_io_params;
phcsBflOpCtl_Hw1Params_t      rc_op_ctl_params;
phcsBflI3P3A_Hw1Params_t      iso_14443_3_params;
phcsBflMfRd_InternalParam_t   mifare_params;

/* 
* Declaration of used parameter structures 
*/
/* Register control parameters */
phcsBflRegCtl_SetRegParam_t         srp;
phcsBflRegCtl_GetRegParam_t         grp;

/* Operation control parameters */
phcsBflOpCtl_AttribParam_t          opp;

uint16_t 	MIFARE_Auth (uint8_t Sector);					// Dichiarata qui e non nel .h perche' utilizzata anche in "RBT_Mifare.c"
uint8_t		FaseWrite;



/* Buffer for data transfer (send/receive) */
uint8_t buffer[EXAMPLE_TRX_BUFFER_SIZE];
/* Buffer serial number						*/
uint8_t serial[12], ats[2];


/*  ************************************************************************/
void InitMifareAmbient(void)
{
	/* Declaration of variables, buffer, ... */
    /* Status variable */

	/* Initialisation of self parameters */
    srp.self = &rc_reg_ctl;
	grp.self = &rc_reg_ctl;
    mrp.self = &rc_reg_ctl;
    opp.self = &rc_op_ctl;
    req_p.self = &iso14443_3;
    sel_p.self = &iso14443_3;
    hlt_p.self = &iso14443_3;
    mfr_p.self = &mfrd;
    
    /* 
	 * Build up stack for serial communication. 
     * Start with the lowest layer, so that the upper ones may refer to this already. 
	 */
    /* Initialise Register Control Layer component */
///    phcsBflRegCtl_SpiHw1Init(&rc_reg_ctl, &rc_reg_ctl_params, &bal);
    
    phcsBflBal_Hw1SerTWRInit(&bal, &bal_params);
    phcsBflRegCtl_SerHw1Init(&rc_reg_ctl, &rc_reg_ctl_params, &bal);
    
    /* Initialise RcIo component */
    phcsBflIo_Hw1Init(&rcio, &rc_io_params, &rc_reg_ctl, PHCS_BFLIO_INITIATOR); 
    /* Initialise Operation Control component */
    phcsBflOpCtl_Hw1Init(&rc_op_ctl, &rc_op_ctl_params, &rc_reg_ctl);
    /* Initialise ISO14443-3 component */
    phcsBflI3P3A_Hw1Initialise(&iso14443_3, &iso_14443_3_params, &rc_reg_ctl, PHCS_BFLI3P3A_INITIATOR); 
    /* Initialise Mifare component */
    phcsBflMfRd_Init(&mfrd, &mifare_params, &rcio, buffer+20);                                       
}

/*  ************************************************************************/
uint16_t MifareCompareVersion(void)
{
	phcsBfl_Status_t status;
	status = CheckSiliconVersion(&rc_reg_ctl, 0x82);
	if (status != PH_ERR_BFL_SUCCESS) {
		status = PH_ERR_BFL_NO_ANSWER_FROM_VERSION;
	}
	return status;
}

/*  ************************************************************************/
uint16_t MF_Sw_Reset(void)
{
	phcsBfl_Status_t status;
    srp.address  = PHCS_BFL_JREG_COMMAND;
    srp.reg_data = PHCS_BFL_JCMD_SOFTRESET;
	status = rc_reg_ctl.SetRegister(&srp); 
#if kDebugInit
	if (status != PH_ERR_BFL_SUCCESS)
		printf("EInit1:sw reset\n");
#endif
	return status;
}
	
/*  ************************************************************************/
uint16_t MF_DisableDrivers(void)
{
    phcsBfl_Status_t     status = PH_ERR_BFL_SUCCESS;
    
	/* disable drivers */
    mrp.address  = PHCS_BFL_JREG_TXCONTROL;
    mrp.mask     = PHCS_BFL_JBIT_TX2RFEN | PHCS_BFL_JBIT_TX1RFEN;
    mrp.set      = 0;
    status = rc_reg_ctl.ModifyRegister(&mrp);
	return status;
}

#if kDebugMIFLock
/*---------------------------------------------------------------------------------------------*\
Method: MifareCheckRegisters
	
	IN:	  -
	OUT:  -
\*----------------------------------------------------------------------------------------------*/
void MifareCheckRegisters(void) {
	
    phcsBfl_Status_t    status = PH_ERR_BFL_SUCCESS;
    uint8_t				ParamVal = 0x33;
    uint8_t				ParamVal1 = 0x33;
    uint8_t				ParamVal2 = 0x33;
    uint8_t				ParamVal3 = 0x33;
    uint8_t				ParamVal4 = 0x33;
    
    grp.address = PHCS_BFL_JREG_COMMIRQ;						// Register 0x04
    status = rc_reg_ctl.GetRegister(&grp);
    if(status != PH_ERR_BFL_SUCCESS) goto EndCheck;
    ParamVal = grp.reg_data;

    grp.address = PHCS_BFL_JREG_TXAUTO;							// Register 0x15
    status = rc_reg_ctl.GetRegister(&grp);
    if(status != PH_ERR_BFL_SUCCESS) goto EndCheck;
    ParamVal1 = grp.reg_data;
    
    grp.address = PHCS_BFL_JREG_TXCONTROL;						// Register 0x14
    status = rc_reg_ctl.GetRegister(&grp);
    if(status != PH_ERR_BFL_SUCCESS) goto EndCheck;
    ParamVal2 = grp.reg_data;

    grp.address = PHCS_BFL_JREG_RXTRESHOLD;						// Register 0x18
    status = rc_reg_ctl.GetRegister(&grp);
    if(status != PH_ERR_BFL_SUCCESS) goto EndCheck;
    ParamVal3 = grp.reg_data;
    
    grp.address = PHCS_BFL_JREG_MODWIDTH;						// Register 0x24
    status = rc_reg_ctl.GetRegister(&grp);
    if(status != PH_ERR_BFL_SUCCESS) goto EndCheck;
    ParamVal4 = grp.reg_data;

        
    
EndCheck:
	grp.address = 0;
    					
}
#endif	// End kDebugMIFLock

/*---------------------------------------------------------------------------------------------*\
Method: MifareReaderInit
	Inizializzazione PN512.
	Se l'antenna non e' collegata esco con errore dopo i primi 3 comandi errati: inutile
	continuare anche perche' il primo comando predispone il PN512 a lavorare come Mifare reader
	e se non lo riceve non funziona anche se poi risponde correttamente a tutti i comandi
	successivi.
	
	IN:	  -
	OUT:  -
\*----------------------------------------------------------------------------------------------*/
uint16_t MifareReaderInit(void *comHandle, uint32_t aSettings)
{    
	
	uint8_t	Errori = 0;
	uint8_t	TotErrori = 3;

	/* Declaration of variables, buffer, ... */
    /* Status variable */
    phcsBfl_Status_t     status = PH_ERR_BFL_SUCCESS;
    //MR19 phcsBfl_Status_t     status1 = PH_ERR_BFL_SUCCESS;		// Non usata

	if (gOrionKeyVars.PN512_RapidSearch == true) {												// MR 29.11.16 Nella ricerca del del PN512 esco al primo errore per non rallentare il prodotto
		TotErrori = 1;
	}
    
	/* Configure PN51x to act as a Mifare Reader. All register which are set in other modes 
     * are configured to the right values. 
     * Remark: The operation control function does not perform a soft-reset and does not know anything 
     *         about the antenna configuration, so these things have to be handled separately!          
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_MFRD_A;
    opp.param_a          = PHCS_BFLOPCTL_VAL_RF106K;
    opp.param_b          = PHCS_BFLOPCTL_VAL_RF106K;
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
#if kDebugInit
    if(status != PH_ERR_BFL_SUCCESS)
        printf("EInit2:PN Config\n");
#endif
    
    if(status != PH_ERR_BFL_SUCCESS) Errori++;									// MR 04.12.2013 Aggiunta per uscire dopo i primi "TotErrori" col PN512
	if (Errori >= TotErrori) return status;

    /* Set Initiator bit of PN51x to act as a reader using register control functions. */
    srp.address = PHCS_BFL_JREG_CONTROL;
    srp.reg_data = PHCS_BFL_JBIT_INITIATOR;
    status = rc_reg_ctl.SetRegister(&srp);

    if(status != PH_ERR_BFL_SUCCESS) Errori++;									// MR 04.12.2013 Aggiunta per uscire dopo i primi "TotErrori" col PN512
	if (Errori >= TotErrori) return status;

    /* Set the timer to auto mode, 5ms using operation control commands before HF is switched on to 
     * guarantee Iso14443-3 compliance of Polling procedure 
	 */
    status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 1);
#if kDebugInit
	if (status != PH_ERR_BFL_SUCCESS)
    {
		printf("EInit3:set timer\n");
    }
#endif
	
    if(status != PH_ERR_BFL_SUCCESS) {											// MR 04.12.2013 Aggiunta per uscire dopo i primi "TotErrori" col PN512
    	if (++Errori == TotErrori) return status;
    }
    /* Activate the field (automatically if there is no external field detected) */
    mrp.address = PHCS_BFL_JREG_TXAUTO;
    mrp.mask    = PHCS_BFL_JBIT_INITIALRFON | PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
    mrp.set     = 1;
    status = rc_reg_ctl.ModifyRegister(&mrp);
#if kDebugInit
    if(status != PH_ERR_BFL_SUCCESS)
        printf("EInit4:RegTXAUTO\n");
#endif

    if(status != PH_ERR_BFL_SUCCESS) {											// MR 04.12.2013 Aggiunta per uscire dopo i primi "TotErrori" col PN512
    	if (++Errori == TotErrori) return status;
    }
    grp.address = PHCS_BFL_JREG_ERROR;
    //MR19 status1 = rc_reg_ctl.GetRegister(&grp);		// Non usata

#if kMifareOrion
    /* enable drivers */
    mrp.address  = PHCS_BFL_JREG_TXCONTROL;
    mrp.mask     = PHCS_BFL_JBIT_TX2RFEN | PHCS_BFL_JBIT_TX1RFEN;
    mrp.set      = 1;
    status = rc_reg_ctl.ModifyRegister(&mrp);
#endif


    /* 
	 * After switching on the drivers wait until the timer interrupt occures, so that 
     * the field is on and the 5ms delay have been passed. 
	 */
    grp.address = PHCS_BFL_JREG_COMMIRQ;
    do {
        status = rc_reg_ctl.GetRegister(&grp);
    } 
    while(!(grp.reg_data & PHCS_BFL_JBIT_TIMERI) && status == PH_ERR_BFL_SUCCESS);

    /* Check status and diplay error if something went wrong */
#if kDebugInit
    if(status != PH_ERR_BFL_SUCCESS)
        printf("EInit5:get timer\n");
#endif
    /* Clear the status flag afterwards */
    srp.address  = PHCS_BFL_JREG_COMMIRQ;
    srp.reg_data = PHCS_BFL_JBIT_TIMERI;
    status = rc_reg_ctl.SetRegister(&srp);

    status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
#if kDebugInit
	if (status != PH_ERR_BFL_SUCCESS)
    {
		printf("EInit6:Set timer\n");
    }
#endif

    if(status != PH_ERR_BFL_SUCCESS) {											// MR 04.12.2013 Aggiunta per uscire dopo i primi "TotErrori" col PN512
    	if (++Errori == TotErrori) return status;
    }

    /* Note that this is only needed if the SIGIN/SIGOUT feature is used! */
	if (aSettings & 0x03)
	{
		if ( aSettings == 1 )
			opp.param_b = PHCS_BFLOPCTL_VAL_NOSIGINOUT;
		else if ( aSettings == 2 )
			opp.param_b = PHCS_BFLOPCTL_VAL_ONLYSIGINOUT;
		else
		opp.param_b = PHCS_BFLOPCTL_VAL_SIGINOUT;
		opp.group_ordinal = PHCS_BFLOPCTL_GROUP_SIGNAL;
		opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_SIGINOUT;
		opp.param_a = PHCS_BFLOPCTL_VAL_READER; 
		status = rc_op_ctl.SetAttrib(&opp);
#if kDebugInit
		if(status != PH_ERR_BFL_SUCCESS)
			printf("[E] Failed to set SIGINOUT features. Status = %04X \n", status);
#endif
	}

	/* 
	 * Following two register accesses are only needed if the interrupt pin is used 
	 * It doesn't hurt if polling is used instead.
	 */
	/* Set IRQInv to zero -> interrupt signal won't be inverted */
	mrp.address = PHCS_BFL_JREG_COMMIEN;
	mrp.mask    = 0x80;
	mrp.set     = 0x00;
	status = rc_reg_ctl.ModifyRegister(&mrp);

	/* Enable IRQPushPull so that the PN51x actively drives the signal */
	mrp.address = PHCS_BFL_JREG_DIVIEN;
	mrp.mask    = 0x80;
	mrp.set     = 0xFF;
	status = rc_reg_ctl.ModifyRegister(&mrp);

    /* Activate receiver for communication
       The RcvOff bit and the PowerDown bit are cleared, the command is not changed. */
    srp.address  = PHCS_BFL_JREG_COMMAND;
    srp.reg_data = PHCS_BFL_JCMD_NOCMDCHANGE;
    status = rc_reg_ctl.SetRegister(&srp);
#if kDebugInit
    if(status != PH_ERR_BFL_SUCCESS)
        printf("EInit7:Activate PN Comm\n");
#endif

#if SIGOUT
    // Aggiunta modifica al registro 0x16 PHCS_BFL_JREG_TXSEL per
    // poter avere il segnale sul pin SIGOut e misurare la risposta
    // dell'antenna in termini di Q.
    // Una sonda dell'oscilloscopio va su SIGOUT, l'altra chiusa in loop
    // va messa davanti all'antenna
    grp.address = PHCS_BFL_JREG_TXSEL;
    //MR19 status1 = rc_reg_ctl.GetRegister(&grp);		// Non usata
    srp.address  = PHCS_BFL_JREG_TXSEL;
    srp.reg_data = 0x14;
    status = rc_reg_ctl.SetRegister(&srp);
#endif
    
    
    
    return status;
}
    

/*  *********************************************************************************
    Funzione aggiunta per modificare il BAUD Rate del PN512 
    *********************************************************************************/
phcsBfl_Status_t MifareReaderUARTSpeed(uint32_t baudrate)
{
    phcsBfl_Status_t  status = PH_ERR_BFL_SUCCESS;
    srp.address  = PHCS_BFL_JREG_SERIALSPEED; 													// register to set to change speed

	switch(baudrate)
    {
        case 9600:
            srp.reg_data = 0xEB;
            break;

		case 19200:
            srp.reg_data = 0xCB;
            break;

        case 38400:
            srp.reg_data = 0xAB;
            break;

        case 57600:
            srp.reg_data = 0x9A;
            break;

        case 115200:
            srp.reg_data = 0x7A;
            break;

        default:
            status = PH_ERR_BFL_INVALID_PARAMETER;
            break;
    }

	/* Set the appropriate value */
	if (status == PH_ERR_BFL_SUCCESS)
	{
		status = rc_reg_ctl.SetRegister(&srp);
	}
	return status;
}		
	

/*---------------------------------------------------------------------------------------------*\
Method: MifareReaderRequest
	Controlla la presenza di un chip Mifare nel campo del reader.

	IN:		- 
	OUT:	- true se chip Mifare nel campo, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool  MifareReaderRequest(void) {

	phcsBfl_Status_t   status = PH_ERR_BFL_SUCCESS;

    // disable drivers
    mrp.address  = PHCS_BFL_JREG_TXCONTROL;
    mrp.mask     = PHCS_BFL_JBIT_TX2RFEN | PHCS_BFL_JBIT_TX1RFEN;
    mrp.set      = 0;
    status = rc_reg_ctl.ModifyRegister(&mrp);
    
    //	vTaskDelay(6);
    // commentato per errore bloccante nel build
	Touts.tmRF = Milli6;
	while (Touts.tmRF) {
    }

	/* enable drivers */
    mrp.address  = PHCS_BFL_JREG_TXCONTROL;
    mrp.mask     = PHCS_BFL_JBIT_TX2RFEN | PHCS_BFL_JBIT_TX1RFEN;
    mrp.set      = 1;
    status = rc_reg_ctl.ModifyRegister(&mrp);
    
    //	vTaskDelay(6);
    // commentato per errore bloccante nel build
	Touts.tmRF = Milli6;
	while (Touts.tmRF) {
    }

	/* Set timeout for REQA, ANTICOLL, SELECT to 200us migliora portato a 400 */
//	status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 1000, 0);
	status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 500, 0);
#if kDebugPrintf
	if (status != PH_ERR_BFL_SUCCESS) {
		printf("E1_REQA in Set Tout\n");
    } 
#endif
	/* 
	 * Do activation according to ISO14443A Part3 
     * Prepare the Request command 
	 */
    buffer[0] = 0;              
    buffer[1] = 0;
    req_p.req_code = PHCS_BFLI3P3A_REQIDL;  /* Set Request command code (or Wakeup: ISO14443_3_REQALL) */
    req_p.atq      = buffer;                /* Let Request use the defined return buffer */
    status = iso14443_3.RequestA(&req_p);   /* Start Request command */
    /* 
	 * In normal operation this command returns one of the following three return codes:
	 * - PH_ERR_BFL_SUCCESS				At least one card has responded
	 * - PH_ERR_BFL_COLLISION_ERROR		At least two cards have responded
	 * - PH_ERR_BFL_IO_TIMEOUT			No response at all
	 * - PH_ERR_BFL_SETTINGLOOSE		MR 05.02.14 Se uno o piu' registri del PN512 hanno perso il setting
	 */
    if (status == PH_ERR_BFL_SETTINGLOOSE) {						// MR 05.02.14 Reinizializzare il PN512
		gOrionKeyVars.AntennaRespFail = true;
    }
    ats[0] = req_p.atq[0];
    ats[1] = req_p.atq[1];
    if(status == PH_ERR_BFL_SUCCESS || (status & PH_ERR_BFL_ERR_MASK) == PH_ERR_BFL_COLLISION_ERROR) {
#if kDebugPrintf
        /* There was at least one response */
        if((status & PH_ERR_BFL_ERR_MASK) == PH_ERR_BFL_COLLISION_ERROR)
            printf("E2_REQA Collision\n");
#endif
        return true;
	}
    return false;
}


/*  ************************************************************************/
bool MifareReaderAnticoll(uint8_t *ptSnum) {
    
	phcsBfl_Status_t	status = PH_ERR_BFL_SUCCESS;
    uint8_t 			i, index;

    for (i=0; i<12; i++) {																		// Prepare data for combined anticollision/select command to get one complete ID
        serial[i]      = 0x00;      
	}
	for (i=0; i < UID_MEDIA_BUF_LEN; i++) {															// Preparo il buffer dove memorizzare l'UID del media attivo
        ptSnum[i] = 0x00;  
	}
	sel_p.uid          = serial;                 												// Put serial buffer for ID
    sel_p.uid_length   = 0;                      												// No bits are known from ID, so set to 0
    status = iso14443_3.AnticollSelect(&sel_p);	 												// Start Anticollision/Select command 

    /* Check return code: If the command was OK, it's always returns PH_ERR_BFL_SUCCESS.
     * If nothing was received, parameter uid_length is 0. */
    if(status == PH_ERR_BFL_SUCCESS) {
        if ((sel_p.sak == 0x00) && (sel_p.uid_length/8 == 8)) {
			MF_DataExch.CardType = MIFARE_UL;													// MIFARE Ultra Light
			MF_DataExch.UID_Size = (sel_p.uid_length/8);
			/* Copio l'UID del media attivo nel buffer snum: nel buffer sorgente sel_p.uid
			* si trova (credo per errore) anche il byte CT (0x88) utilizzato dalla Mifare UL 
			* per inviare la prima parte del suo UID. Questo Byte deve essere scartato quindi
			* si parte da i=1. */
			index=0;
			for(i=1;i<(sel_p.uid_length/8);i++) {
				ptSnum[index++] = sel_p.uid[i];  
			}
		}
#if Card_SAK_88
        if ((sel_p.sak == 0x08 || sel_p.sak == 0x88) && (sel_p.uid_length/8 == 4)) {			// 03.09.15 Test sak=0x88 per carte campione Tesis da Baltom
#else
        	if ((sel_p.sak == 0x08) && (sel_p.uid_length/8 == 4)) {
#endif        
           	MF_DataExch.CardType = MIFARE_Std_1K;												// Mifare std o Mifare Tesis Baltom (0x88) 
			MF_DataExch.UID_Size = (sel_p.uid_length/8);
			for(i=0;i<(sel_p.uid_length/8);i++) {												// Copio l'UID del media attivo nel buffer snum
				ptSnum[i] = sel_p.uid[i];  
			}
		}
        return true;
    }
    
    return false;
}


//  ********************************************************************************
//  *******************   MIFARE   READ      ***************************************
//  ********************************************************************************

uint16_t  MifareReaderRead(uint8_t*rdbuff, uint8_t SectorToRead, uint8_t *BlockToRead) {
//uint16_t  MifareReaderRead(uint8_t*rdbuff, uint8_t SectorToRead, DATAMIFARE *BlockToRead) {

	// Declaration of used parameters and structures //
    phcsBfl_Status_t status = PH_ERR_BFL_SUCCESS;
    uint8_t i, block;

    //  Authentication  //
	status = MIFARE_Auth(SectorToRead);
	if (status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
	    printf("ERd_1:Auth\n");
#endif
		return status;
    }

	// Leggo tanti blocchi del settore SectorToRead quanti indicati nei bytes BlockToRead
	// fino al limite dei blocchi presenti in ogni settore
	for (i=0;i<BlockPerSector;i++) {
		if (BlockToRead[i] != NoBlock ) {
			status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);			// Response shall now be sent within 10ms //
			if (status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
				printf("ERd_2:Settout fail loop\n");
#endif
				return status;
			}
			block 		 		= BlockToRead[i];
			mfr_p.addr   		= ((SectorToRead*BlockPerSector) + block);	// Indirizzo assoluto del Blocco da leggere
			mfr_p.buffer 		= (rdbuff + (i * MIF_BLOCKLEN));			// set destination buffer
			mfr_p.buffer_length = 0;                						// nothing is handed over in the buffer
			mfr_p.cmd    		= PHCS_BFLMFRD_READ;       					// set Read command
			status = mfrd.Transaction(&mfr_p);		// Start Mifare Read
			if(status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
				printf("ERd_3:read fail loop\n");
#endif
				return status;
			}
		}
	}
#if	!SaltaSicurezza
	if (gOrionKeyVars.Inserted) {
		if (cryptuid != Snum_media_in[0]) {
			status = PH_ERR_BFL_AUTHENT_ERROR;
		}
	}
#endif
	
	return status;
}
/*--------------------------------------------------------------------------------------*\
Method: MifareWriteAndCompare
  Identica alla MifareReaderWrite ma con la rilettura e comparazione di ogni blocco scritto.
Parameters:
	IN	- pointer a wrbuff, buffer da scrivere sulla carta
		- Numero del settore da scrivere
		- pointer al blocco sulla carta da scrivere
		- flag di lettura dopo la scrittura
\*--------------------------------------------------------------------------------------*/
uint16_t  MifareWriteAndCompare(uint8_t *wrbuff, uint8_t SectorToWrite, uint8_t *BlockToWrite, bool ReadAfterWrite) {

	// Declaration of used parameter and structures
    phcsBfl_Status_t  status = PH_ERR_BFL_SUCCESS;
    uint8_t i, block;

    //  Authentication
	FaseWrite = 0;
	status = MIFARE_Auth(SectorToWrite);
	if (status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
	    printf("EWr_1:Auth\n");
#endif
		return status;
    }
	// Scrivo tanti blocchi del settore SectorToWrite quanti indicati nei bytes BlockToWrite
	// fino al limite dei blocchi presenti in ogni settore
	for (i=0; i<BlockPerSector; i++) {
		if (BlockToWrite[i] != NoBlock ) {
			FaseWrite ++;
			status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);			// Response shall now be sent within 10ms //
			if (status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
				printf("EWR_2:Settout write fail loop\n");
#endif
				return status;
			}
			block 		 		= BlockToWrite[i];
			mfr_p.addr   		= ((SectorToWrite * BlockPerSector) + block);	// Indirizzo assoluto del Blocco da scrivere
			mfr_p.buffer 		= (wrbuff + (i * MIF_BLOCKLEN));				// set source buffer
			mfr_p.buffer_length = PHCS_BFLMFRD_STD_BLOCK_SIZE;              	// 16 bytes in the buffer valid
			mfr_p.cmd    		= PHCS_BFLMFRD_WRITE;      						// set Write command
			FaseWrite ++;
			status = mfrd.Transaction(&mfr_p);									// Start Mifare Write
			if(status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
				printf("EWr_3:write fail loop\n");
#endif
				return status;
			} else {
				// **********  Controllo se rileggere il blocco scritto   **************
				if (ReadAfterWrite) {
					FaseWrite ++;
					status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);		// Response shall be sent within 5ms
					if (status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
						printf("EWR_4:Settout readAfterwrite fail loop\n");
#endif		
						return status;
					}
					mfr_p.cmd    		= PHCS_BFLMFRD_READ;       					// set Read command
					mfr_p.addr   		= ((SectorToWrite*BlockPerSector) + block);	// Blocco da leggere = ultimo blocco scritto
					mfr_p.buffer		= MF_DataExch.RdBuff;						// set destination buffer
					mfr_p.buffer_length = 0;                						// nothing is handed over in the buffer
					FaseWrite ++;
					status				= mfrd.Transaction(&mfr_p);					// Start Mifare Read
					if(status != PH_ERR_BFL_SUCCESS) {
#if kDebugPrintf
						printf("EWr_5:read after write fail\n");
#endif
						return status;
					} else {
						// **********  Comparo blocco appena scritto   **************
						if (!CompareBuffer(MF_DataExch.RdBuff, (wrbuff + (i * MIF_BLOCKLEN)), MIF_BLOCKLEN)) return (status = 0xFFFF);
					}
				}
			}
		}
	}

	return status;
}    

/*  ************************************************************************
	***  Prepare data for Mifare Authentication: KeyA o KeyB Command, ******
	***  UID nel buffer di trasmissione                               ******
	************************************************************************/
uint16_t MIFARE_Auth (uint8_t SectorToAuth) {

	/* Declaration of used parameter and structures */
    uint8_t 			j, index1;
	phcsBfl_Status_t 	status;

	/* Set timeout for Authentication */
	status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	if (status != PH_ERR_BFL_SUCCESS) return status;

	 // Inserisco KEYVal nel Buffer di trasmissione
	for(j=0; j<6; j++) {
		buffer[j] = MF_DataExch.KeyVal[j];		//KEY A/B nel buffer da trasmettere
	}
	// Copio UID da Snum_media_in al Buffer di trasmissione
	index1 = 6;
	for(j=0;j<MF_DataExch.UID_Size;j++) {
		buffer[index1] = Snum_media_in[j];				// UID nel buffer da trasmettere
        index1++;
     }
	// Determino se cmd Auth KeyA (0x60) o Auth KeyB (0x61)
	 if (MF_DataExch.KeyType) {
		mfr_p.cmd  = PHCS_BFLMFRD_AUTHENT_B;     		//Authent KeyB
	} else {
		mfr_p.cmd = PHCS_BFLMFRD_AUTHENT_A;      		//Authent KeyA
	}
	mfr_p.addr          = SectorToAuth*BlockPerSector;	// blocco
	mfr_p.buffer        = buffer;    					// Set the mifare buffer to the one initialised
	mfr_p.buffer_length = (MF_DataExch.UID_Size+0x06);  // 6 bytes of key plus UID bytes in buffer
	status = mfrd.Transaction(&mfr_p);					// Start Mifare Authentication
	return status;
}

/*  ************************************************************************/
phcsBfl_Status_t MifareReaderLedInit(uint8_t dato)
{
	phcsBfl_Status_t status = PH_ERR_BFL_SUCCESS;

	srp.address  = PHCS_BFL_JREG_TESTPINEN;
	srp.reg_data = dato | 0x80;
	status = rc_reg_ctl.SetRegister(&srp);
	
	dato = !dato & 0x07;
	srp.address  = PHCS_BFL_JREG_TESTPINVALUE;
	srp.reg_data = dato | 0x80;
	status = rc_reg_ctl.SetRegister(&srp);

	return status;
}

/*  ************************************************************************/
phcsBfl_Status_t PN512_getr (uint8_t addr, uint8_t* val)
{
	phcsBfl_Status_t status;
	
	grp.address  = addr;
	status = rc_reg_ctl.GetRegister(&grp);
	*val = grp.reg_data;
	return status;
}

/*  ************************************************************************/
phcsBfl_Status_t PN512_setr (uint8_t address, uint8_t dato)
{
	phcsBfl_Status_t status;
	srp.address  = address;
	srp.reg_data = dato;
	status = rc_reg_ctl.SetRegister(&srp);
	return status;
}

/*  ************************************************************************/
/*MR19  Non usata

phcsBfl_Status_t MifareReaderLedSet(pin, mask)
{
	uint8_t dato = 0;
	uint8_t temp;
	
	phcsBfl_Status_t status = PH_ERR_BFL_SUCCESS;
	
		grp.address  = PHCS_BFL_JREG_TESTPINVALUE;
		status = rc_reg_ctl.GetRegister(&grp);
		dato = grp.reg_data;

// controllo valore da assegnare a pin 1		
			temp = (pin & 0x1);
			if (temp != 0){
				temp = (temp & mask);
					if (temp == 0)
					dato = (dato & 0xfe);
					else
					dato = (dato | 0x1);}
			
// controllo valore da assegnare a pin 2		
			temp = (pin & 0x2);
			if (temp != 0){
				temp = (temp & mask);
					if (temp == 0)
					dato = (dato & 0xfd);
					else
					dato = (dato | 0x2);}			

// controllo valore da assegnare a pin 2		
			temp = (pin & 0x4);
			if (temp != 0){
				temp = (temp & mask);
					if (temp == 0)
					dato = (dato & 0xfb);
					else
					dato = (dato | 0x4);}
			
			srp.address  = PHCS_BFL_JREG_TESTPINVALUE;
			srp.reg_data = dato | 0x80;
			status = rc_reg_ctl.SetRegister(&srp);			
			
return status;			
				
}
*/		

