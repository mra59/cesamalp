/****************************************************************************************
File:
    VMC_GestSelez.c

Description:
    File con le funzioni di gestione della richiesta di selezione.

History:
    Date       Aut  Note
    Mag 2019	MR   

*****************************************************************************************/

#include "ORION.H" 
#include "VMC_GestSelez.h"
#include "Gestore_Exec_MSTSLV.h"
#include "ExecSlave.h"
#include "OrionTimers.h"
#include "OrionCredit.h"
#include "VMC_CPU_LCD.h"
#include "icpVMCProt.h"
#include "ICPUSDHook.h"
#include "ICPCHGHook.h"
#include "Vendita.h"
#include "Totalizzazioni.h"
#include "VMC_CPU_Motori.h"
#include "VMC_CPU_Prog.h"

#include "Dummy.h"



/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void  Led_Vano(uint8_t On_Off, uint32_t DurataON);
extern	void  Buz_start(uint32_t time, uint32_t tono1, uint32_t tono2);
extern	void  GestTastiSelez(uint16_t tasto);

extern	uint32_t	TimeRemaining;
extern	uint16_t	CntFlussRemaining;

//extern	bool ICPUSDHook_VendRequest(uint8_t numtasto);


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

uint8_t		NumVMCSelez;
bool		Norefresh;

static	ExecVendReqResp		VndReqResul;
static 	uint16_t			PreviousCredit;


/*---------------------------------------------------------------------------------------------*\
Method: Gestione_Selezioni
	Controlla se e' stata richiesta una selezione e la gestisce, sia con protocolli seriali 
	che in autonomia, con Mifare locale e selettore o gratuito.
	Se la selezione richiesta e' gia' attiva si abilita una nuova richiesta senza controlli
	di credito/Autorizzazioni.
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void  Gestione_Selezioni(void)
{
	//uint32_t	ParamVal;
	uint8_t		SelezInProgress;

	switch (gVMCState.Fase)
	{
		case NoSelez:
			if (gVMCState.SelezReq == true)
			{																						// Richiesta selezione
				SelezInProgress = GetSelectionInProgress();
				if ((SelezInProgress > 0) &&  (SelezInProgress != NumVMCSelez))
				{																					// Richiesta selezione diversa da quella in corso
					gVMCState.ChangeSelection = true;												// Abilito la commutazione della selezione senza scalare alcun credito
					gVMCState.InSelezione = true;
					gVMCState.Fase = WaitEndVend;
					break;
				}
				
				gVMCState.ChangeSelection = false;
				//MR19 Memo_Touch_NumSelez(NumVMCSelez-1);												// Memo NumSel per eventuale pagina di fail vend
				if ((gVMC_ConfVars.Gratuito == true)) {												// DA in gratuito
					SetPrice_e_Selnum(VMC_ValoreSelezione, 0);										// Set prezzo=0 e numero selezione per Audit
					VMC_ValoreSelezione = MRPPrices.Prices99.MRPPricesList99[NumVMCSelez-1];		// Determino valore selezione solo per l'audit vendite gratuite di test
					SetAuditVendInGratuito(VMC_ValoreSelezione);
					VendAuthorResult(true);															// Vendita sempre autorizzata
				}
				else if (gVMC_ConfVars.ExecutiveSLV == true) {
					// ===============================================
					// ========  Vend  Req  Executive Slave ==========
					// ===============================================
					if (IfExecSlaveSelezEnabled(NumVMCSelez)) {
						gVMCState.Fase = WaitVendAuthor;													// Attesa autorizzazione alla vendita dal Master Executive
					} else {
						gVMCState.SelezReq = false;
					}
				}
				else if (gVMC_ConfVars.PresMifare == true)
				{
					// ===============================================
					// ========    Mifare  Locale           ==========
					// ===============================================
					if (gOrionKeyVars.Inserted && (gOrionKeyVars.State != kOrionCPCStates_Idle))
					{																						// Vend Denied se c'e' una carta e non e' in Idle state
						LCD_Disp_NOSEL();																	// Visualizzo non disponibile su LCD
						gVMCState.SelezReq = false;
						return;
					}
					VMC_ValoreSelezione = MRPPrices.Prices99.MRPPricesList99[NumVMCSelez-1];
					if (SetPrice_e_Selnum(VMC_ValoreSelezione, NumVMCSelez))
					{																						// Determino prezzo e/o numero selezione
						gVMCState.Fase = WaitVendAuthor;													// Attesa autorizzazione alla vendita dalla gestione credito
						gOrionKeyVars.State = kOrionCPCStates_Vend;											// Nuovo stato per "CashlessTask" in OrionCredit
						TmrStart(gVMCState.ToutFaseGestSel, VEND_TOUT_MIFARE_LOCALE + OneSec);				// Tout attesa autorizzazione alla vendita dalla gestione credito
					}
					else
					{
						LCD_Disp_NOSEL();																	// Visualizzo non disponibile su LCD
						gVMCState.SelezReq = false;
					}
				}
				else if (gVMC_ConfVars.ProtMDB)
				{
					if (gVMC_ConfVars.ProtMDBSLV)
					{
						// ===============================================
						// ========   DA as USD Slave Locale    ==========
						// ===============================================
						if (ICPUSDHook_VendRequest(NumVMCSelez))
						{																					// Determino costo/numero selezione
							gVMCState.Fase = WaitVendAuthor;												// Attesa autorizzazione alla vendita dal Master MDB
						}
						else
						{
							LCD_Disp_NOSEL();																// Visualizzo non disponibile su LCD
							gVMCState.SelezReq = false;
						}
					}
					else
					{
						// ===============================================
						// ========    M D B   Master           ==========
						// ===============================================
						if (gVMCState.USD_Ext_SelezReq == false)
						{
							VMC_ValoreSelezione = MRPPrices.Prices99.MRPPricesList99[NumVMCSelez-1];
							if (SetPrice_e_Selnum(VMC_ValoreSelezione, NumVMCSelez))
							{																				// Determino prezzo e/o numero selezione
								gVMCState.Fase = WaitVendAuthor;											// Attesa autorizzazione alla vendita dalla gestione credito
								gOrionKeyVars.State = kOrionCPCStates_Vend;									// Nuovo stato per "CashlessTask" in OrionCredit
								TmrStart(gVMCState.ToutFaseGestSel, VEND_TOUT_PERIPH_MDB + TenSec);			// Tout attesa autorizzazione alla vendita dalla gestione credito
							}
							else
							{
								LCD_Disp_NOSEL();															// Visualizzo non disponibile su LCD
								gVMCState.SelezReq = false;
							}
						}
						else
						{																					// Vend Reqt da USD collegato al nostro DA che opera in Master Mode
							// ==================================================
							// ======  MDB Master: VendReq <-- USD Slave ========
							// ==================================================
							gVMCState.Fase = WaitVendAuthor;												// Attesa autorizzazione alla vendita dalla gestione credito
							gOrionKeyVars.State = kOrionCPCStates_Vend;										// Nuovo stato per "CashlessTask" in OrionCredit
							TmrStart(gVMCState.ToutFaseGestSel, TwoSec);									// Tout attesa autorizzazione alla vendita dalla gestione credito
						}
					}
				}
			}
			break;
				
// ========================================================================
// ========   W A I T    V E N D      A U T H O R I Z A T I O N    ========
// ========================================================================
		case WaitVendAuthor:
			
			if ((gVMC_ConfVars.PresMifare) ||
				((gVMC_ConfVars.ProtMDB) && (gVMC_ConfVars.ProtMDBSLV == false)))
			{																								// In MDB Master o con Mifare Locale controllo Timeout perche' sono Master
				if (TmrTimeout(gVMCState.ToutFaseGestSel))
				{																							// Scaduto Tout attesa risposta dalla gestione credito
					if (gOrionKeyVars.Inserted)
					{
						gOrionKeyVars.RFFieldFree = true;													// Reject card
					}
					if (gVMCState.USD_Ext_SelezReq == false)
					{
						gVMCState.Fase = NoSelez;															// Annullo richiesta
						LCD_Disp_NOSEL();																	// Visualizzo non disponibile su LCD
						gVMCState.SelezReq = false;
					}
					else
					{
						// ========  MDB Master: VEND DENIED --> USD Slave  =======	
						if (ICPUSDHook_VendDenied(NULL) == false) {											// USD  VEND DENIED predisposto con successo  per l'invio allo Slave USD
							gVMCState.Fase = NoSelez;														// Annullo richiesta
						}
					}
				}
			}
			else
			{
					// In Executive Slave e USD Slave non applico timeout
			}
			break;
			
// ========================================================================
// ========   V E N D        A P P R O V E D           ====================
// ========================================================================
		case VendApproved:
		
			if (gVMCState.NoDispCreditoInsuff == true)
			{
				gVMCState.NoDispCreditoInsuff = false;
			}
			if (gVMCState.USD_Ext_SelezReq == false)
			{																						// Selezione Locale, non richiesta da USD Esterno
				//====  Selezione Locale in Exec, o Mifare, o USD Slave o MDB Master  =====
				// --- Controllo condizioni per dare il resto subito ---
				// --- Le condizioni sono: Cash Vend + Resto Subito=Y + No gratuito ---
				// --- + MDB Master + RR collegata                                  ---
				if ((TipoVendita == VEND_CON_CASH) && (gOrionConfVars.RestoSubito))
				{																					// Controllo se ci sono le condizioni per dare il resto subito prima della selez.
					if (gVMC_ConfVars.Gratuito == false)
					{
						if ((gVMC_ConfVars.ProtMDB) && (ICPProt_CHGGetReady()))
						{
							if (ICPCHGHook_Change())
							{																		// Se c'e' cash residuo, do il resto poi inizio la selezione
								gVMCState.Fase = WaitEndPayout;
								break;
							}						
						}
					}
				}
				gVMCState.InSelezione = true;
				Norefresh = true;																	// Non aggiornare display LCD fino all'elaborazione di fine selezione
				gVMCState.Fase = WaitEndVend;		
				SetTouchNewViscredit();																// Per riattivare visualizzazione credito dopo la selezione
				SetDeconto();																		// Predispongo il deconto se funzione attivata
				if (gVMC_ConfVars.ExecutiveSLV == true)		
				{																					// In Executive predispongo aggiornamento solo battute costruttore
					SetDatiAuditVendita(VEND_DA_IN_EXEC);		
					VendAmountSEL = gOrionKeyVars.VendReq;											// Per registrare la selezione come vendita con cash senza sconto
				}		
				if ((gVMC_ConfVars.ProtMDB) && (gVMC_ConfVars.ProtMDBSLV))		
				{																					// Come USD Slave predispongo aggiornamento solo battute costruttore
					SetDatiAuditVendita(VEND_DA_AS_USD_SLAVE);		
				}		
			}		
			else		
			{																						// La Vend e' richiesta dallo Slave USD collegato a questo Master MDB
				// ========  MDB Master: VEND APPROVED --> USD Slave =======		
				if (ICPUSDHook_VendApproved(NULL) == false)		
				{																					// USD VEND APPROVED predisposto con successo per l'invio allo Slave USD
					gVMCState.Fase = WaitEndVend;		
					#if TimeFwBreve		
						TmrStart(gVMCState.ToutFaseGestSel, THIRTYSEC);								// Tout attesa breve per debug
					#else		
						TmrStart(gVMCState.ToutFaseGestSel, OneMin+THIRTYSEC);						// Tout attesa risposta fine vend dall'USD
					#endif
				}
			}
			break;
			
// ========================================================================
// ========   W A I T    E N D   P A Y O U T           ====================
// ========================================================================
		case WaitEndPayout:
			// ==================================================
			// =====  MDB Master: attesa fine payout da RR  =====
			// ==================================================
			if (gOrionCashVars.givingChange == false)
			{
				gVMCState.waitingRealVendEnd	= true;												// Resto Cash gia' reso, in caso di Fail Vend non ridare credito
				gVMCState.InSelezione 			= true;	
				Norefresh 						= true;												// Non aggiornare display LCD fino all'elaborazione di fine selezione
				gVMCState.Fase 					= WaitEndVend;	
				SetTouchNewViscredit();																// Per riattivare visualizzazione credito dopo la selezione
				SetDeconto();																		// Predispongo il deconto se funzione attivata
			}
			break;
		
// ========================================================================
// ========   V E N D        D E N I E D               ====================
// ========================================================================
		case VendDenied:
			if (gVMCState.USD_Ext_SelezReq)
			{																							// La Vend era richiesta da USD Slave esterno collegato a questo Master MDB
				// ========  MDB Master: VEND DENIED --> USD Slave Extern  =======
				if (ICPUSDHook_VendDenied(NULL) == false)
				{																						// USD VEND DENIED predisposto con successo  per l'invio allo Slave USD
					gVMCState.Fase = NoSelez;															// Clr richiesta selezione
				}
			}
			else
			{
				//====  Selezione Locale in Exec o Mifare o come USD Slave  =====
				gVMCState.SelezReq = false;
				gVMCState.Fase 	= NoSelez;																// Annullo richiesta
				if (gVMC_ConfVars.ExecutiveSLV == true) 
				{
					// ========  VEND  DENIED  Executive Slave =======

		/*  31.03.2017 Tentativo di soluzione delle varie situazioni di credito insufficiente
		 * 
					// La visualizzazione del prezzo selezione quando credito insufficiente funziona cosi':
					// a) Exec std senza credito: all'uscita della "IfExecSlaveSelezEnabled" si chiama la LCD_Disp_CredNoSuff perche' senza credito non
					//                            c'e' il comando per inviare la richiesta di selezione.
					// b) Exec std con credito insuff: il Master deve inviare il costo selezione che memorizzo in VMC_CreditoDaVisualizzare e lo visualizzo
					// c) Exec Price Hold: essendo il Price Display set di default, uso visualizzazione del master
					if (gOrionConfVars.PriceDisplay && gOrionConfVars.PriceHolding) {
						LCD_Disp_CredNoSuff(VMC_NumeroSelezione, VMC_CreditoDaVisualizzare);		// Visualizzo prezzo selezione richiesta su LCD
					} else if (gOrionConfVars.PriceHolding) {
						LCD_Disp_CredNoSuff(VMC_NumeroSelezione, VMC_ValoreSelezione);				// Visualizzo prezzo selezione richiesta su LCD
					}
		*/
				
					if (gOrionConfVars.PriceDisplay) {
						LCD_Disp_CredNoSuff(VMC_NumeroSelezione, VMC_CreditoDaVisualizzare);			// Visualizzo prezzo selezione richiesta su LCD
					} else if (gOrionConfVars.PriceHolding) {
						LCD_Disp_CredNoSuff(VMC_NumeroSelezione, VMC_ValoreSelezione);					// Visualizzo prezzo selezione richiesta su LCD
					} else {																			// Exec Std senza Price-Display: visualizzo valore selezione
						LCD_Disp_CredNoSuff(VMC_NumeroSelezione, VMC_ValoreSelezione);
					}
				}
				else
				{
					if (gVMC_ConfVars.PresMifare == true)
					{
						// ========  VEND  DENIED  MIFARE  o Selettore =======
						if (CreditoMancante != 0)
						{
							LCD_Disp_CredNoSuff(gOrionKeyVars.VendSel, gOrionKeyVars.VendReq);			// Visualizzo prezzo selezione richiesta su LCD
			#if PRENOTA_SELEZ
							// --- Prenotazione Selezione per eseguirla se entra carta ---
							if (gOrionKeyVars.Inserted == false) 
							{																			// Senza carta set timer per eseguire selez se entra carta entro il timeout
								if (TotaleCash() == 0)
								{																		// Se c'e' credito cash non parte selez inserendo carta perche' prima la ricarico
									TmrStart(gVMCState.PrenotaSelezTime, FIVESEC);						// Timer entrata carta per far partire la selezione richiesta prima
									gVMCState.SelezPrenotata = NumVMCSelez;								// Salvo numero selezione richiesta
								}
							}
			#endif
						}
						else
						{
							LCD_Disp_NOSEL();															// Visualizzo selezione non disponibile
							gVMCState.NoDispCreditoInsuff = false;
						}
					}
					else
					{
						if (gVMC_ConfVars.ProtMDBSLV) {													// Selezione Locale come USD Slave
							// ========  VEND DENIED USD Slave  Locale    =======
							//LCD_Disp_NOSEL();															// Visualizzo selezione non disponibile
							LCD_Disp_CredNoSuff(gOrionKeyVars.VendSel, gOrionKeyVars.VendReq);			// Visualizzo prezzo selezione richiesta su LCD
						} else {
							// ========  VEND DENIED  MDB  Master   =======
							if (CreditoMancante != 0)
							{
								LCD_Disp_CredNoSuff(gOrionKeyVars.VendSel, gOrionKeyVars.VendReq);		// Visualizzo prezzo selezione richiesta su LCD
							}
							else
							{
								LCD_Disp_NOSEL();														// Visualizzo selezione non disponibile
								gVMCState.NoDispCreditoInsuff = false;
							}
						}
					}
				}
			}
			break;
	
// ========================================================================
// ========     W A I T    E N D    V E N D            ====================
// ========================================================================
		case WaitEndVend:
			if (gVMCState.USD_Ext_SelezReq)
			{																							// Selezione effettuata da USD Slave esterno collegato a questo DA
				// =============================================================
				// ====  MDB Master: wait End Vend <-- USD Slave Extern   ======
				// =============================================================
				if (gVMCState.USD_Ext_In_Selezione == false)
				{
					VMC_Response = VMC_RESP_VEND_OK;													// Predispongo risposta "Vend OK"
					if (gVMCState.EsitoFineVend == VEND_FAIL)
					{																					// FAIL Vend
						if (gVMCState.waitingRealVendEnd == false)
						{																				// In caso di Resto Cash gia' erogato, lascio "Vend Ok" per non sballare l'Audit
							VMC_Response = VMC_RESP_VEND_FAILED;
						}
					}
					gVMCState.waitingRealVendEnd	= false;
					gVMCState.USD_Ext_SelezReq 		= false;
					gVMCState.SelezReq 				= false;
					NumVMCSelez						= 0;
					gVMCState.Fase 					= NoSelez;											// Ritorno in st-by
				}
				else
				{
					if (TmrTimeout(gVMCState.ToutFaseGestSel))
					{																					// Timeout attesa scaduto: reset USD Esterno
						gVMCState.SelezReq 				= false;
						gVMCState.USD_Ext_SelezReq		= false;
						gVMCState.USD_Ext_In_Selezione 	= false;
						if (gVMCState.waitingRealVendEnd)
						{																				// Resto Cash gia' erogato, rispondo "Vend Ok" per non sballare l'Audit
							VMC_Response = VMC_RESP_VEND_OK;											// Predispongo risposta "Vend OK"
						}
						else 
						{
							VMC_Response = VMC_RESP_VEND_FAILED;
						}
						ICPVMCHook_ResetUSD();
						gVMCState.waitingRealVendEnd	= false;
						NumVMCSelez						= 0;
						gVMCState.Fase 					= NoSelez;										// Ritorno in st-by
					}
				}
			}
			else
			{
				if (gVMCState.InSelezione == false)
				{
					// ========  SELEZIONE   TERMINATA   =======
					if (gVMCState.ChangeSelection == true)
					{
						// ==== CAMBIO  SELEZIONE   TERMINATA   =======
						gVMCState.SelezReq = false;
						gVMCState.Fase = NoSelez;														// Ritorno in st-by
						NumVMCSelez = 0;																// Riabilito DA per nuova selezione
						break;
					}

					UpDateDeconto(NumVMCSelez);															// Call UpDateDeconto prima di azzerare NumVMCSelez per aggiornare contatori usura filtri
					NumVMCSelez = 0;
					gVMCState.SelezReq = false;
					if (gVMCState.EnaRispEnerg == true)
					{
						TmrStart(gVMCState.ToutRispEnerg, RispEnergTime);								// Delay per risparmio energetico
					}
					TmrStart(gVMCState.TimeAutoFlushing, TimeFlushing);									// Carico time Autoflushing
					
					if (gVMC_ConfVars.Gratuito == true)
					{
						// ========  FINE  VEND   GRATUITO     =======
						if (gVMCState.EsitoFineVend == VEND_FAIL)
						{																				// FAIL Vend
							StatusVendita = VEND_FAILURE;
						}
						else
						{
							StatusVendita = VEND_SUCCESS;
						}
					}
					if (gVMC_ConfVars.ExecutiveSLV == true)
					{
						// ===============================================
						// ========  FINE  VEND Executive Slave ==========
						// ===============================================
						if (gVMCState.EsitoFineVend == VEND_FAIL)
						{																				// FAIL Vend
							LCD_Disp_VendFail();
							Norefresh = true;
							ExecSlave_VendFail();
							Buz_start(TIMEBUZ_VEND_FAIL, HZ_2000, 0);									// Buzzer lungo Segnala vendita FAIL
							StatusVendita = VEND_FAILURE;												// Annulla aggiornamento Battute Costruttore
						}
						else
						{
							ExecSlave_VendSuccess();													// Vendita OK
							Touch_Msg_PrelevaBevanda();													// Msg preleva bevanda su Touch
							//LCD_Disp_PrelevareBevanda();
							//Led_Vano(ON, TIME_LED_VANO);												// Accendo il led vano prelievo per la durata del messaggio prelevare				
							Norefresh = false;															// Riabilita refresh display LCD
							//Buz_start(TIMEBUZ_VEND_OK, HZ_2000, 0);										// Buzzer corto Segnala vendita OK
							StatusVendita = VEND_SUCCESS;												// Incrementa Battute Costruttore
						}
					}
					else
					{
						// =======================================================
						// ===  FINE  VEND  Mifare Locale, MDB o Gratuito   ======
						// =======================================================
						if (gVMC_ConfVars.ProtMDBSLV)
						{																				// DA in USD Slave Mode Locale
							if (gVMCState.EsitoFineVend == VEND_FAIL)
							{
								ICPUSDHook_SlaveVendFailed(ErrMotorJam);								// Errore Motor/Actuator Jam
								StatusVendita = VEND_FAILURE;											// Annulla aggiornamento Battute Costruttore
							}
							else
							{
								ICPUSDHook_SlaveVendSuccess();
								StatusVendita = VEND_SUCCESS;											// Incrementa Battute Costruttore
							}
						}
						if (gVMCState.EsitoFineVend == VEND_FAIL)
						{																				// FAIL Vend
							VMC_Response = VMC_RESP_VEND_FAILED;
							LCD_Disp_VendFail();
							Touch_Msg_VendFail();														// Msg Fail Vend su Touch
							Norefresh = true;
							Buz_start(TIMEBUZ_VEND_FAIL, HZ_2000, 0);									// Buzzer lungo Segnala vendita FAIL
						}
						else
						{
							VMC_Response = VMC_RESP_VEND_OK;											// Vend OK
							//Touch_Msg_PrelevaBevanda();													// Msg preleva bevanda su Touch
							//LCD_Disp_PrelevareBevanda();
							//Led_Vano(ON, TIME_LED_VANO);												// Accendo il led vano prelievo per la durata del messaggio prelevare				
							Norefresh = false;															// Riabilita refresh display LCD
							//Buz_start(TIMEBUZ_VEND_OK, HZ_2000, 0);										// Buzzer corto Segnala vendita OK
						}
						if (gVMC_ConfVars.Gratuito == true)
						{
							SetPaymentConfig();															// Ripristina setting dei sistemi di pagamento
						}
					}
					gVMCState.Fase = NoSelez;															// Ritorno in st-by
					NumVMCSelez = 0;																	// Riabilito DA per nuova selezione
				} 
			}
			break;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: DA_in_Stby
	Controlla se il DA e' in st-by, ovvero senza credito e senza selezioni in corso.
	
	IN:	 - 
	OUT: - true se DA in ST-by
\*----------------------------------------------------------------------------------------------*/
bool  DA_in_Stby(void) {
	
	if ((gOrionCashVars.creditCash != 0) || (gOrionKeyVars.Inserted == true)) {
		return false;
	}
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: VendAuthorResult
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void VendAuthorResult(bool Approved)
{
	if (Approved)
	{
		gVMCState.Fase = VendApproved;
	}
	else
	{
		gVMCState.Fase = VendDenied;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: IfExecSlaveSelezEnabled
	Controlla se la selezione richiesta e' disponibile e se possibile richiederla al master
	Executive.
	In Executive Standard visualizza subito il costo selezione quando ritorna 4 la funzione 
	"ExecSlaveVendRequest", cioe' selezione richiesta con credito = zero.

	IN:	 - Numero Selezione richiesta
	OUT: - true se disponibile (predispone la VendReq al Master)
\*----------------------------------------------------------------------------------------------*/
bool  IfExecSlaveSelezEnabled(uint8_t NumeroSelez) {
	
	ExecVendReqResp *pntreqres;
	pntreqres = &VndReqResul;
	
	if (GestMS_IsVMC2InVend() == true) {														// RR occupata nella vendita col VMC2
		LCD_Disp_NOSEL();																		// Annullo la richiesta di vendita VMC principale
		return false;
	}
	ExecSlaveVendRequest(pntreqres, NumeroSelez);												// Se parametri leciti predispone una Vend Request al Master
	if (pntreqres->result == 0) return true;
	
	if (pntreqres->result == 4) {
		LCD_Disp_CredNoSuff(NumeroSelez, pntreqres->creditomancante);							// Credito insufficiente: visualizzo prezzo selezione richiesta su LCD
	} else {
		LCD_Disp_NOSEL();																		// SelezNum zero o prezzo non valido o prezzo/usf non corretto: non disponibile su LCD
	}
	return false;
}	

/*---------------------------------------------------------------------------------------------*\
Method: SelezPrenotata
	Controlla se c'e' una selezione richiesta senza la presenza carta.
	Se si' viene riattivata la sequenza di richiesta selezione.

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void  SelezPrenotata(void) {
	
#if	PRENOTA_SELEZ
	
	if (gVMCState.PrenotaSelezTime.tmrEnd == 0) return;										// Nessuna selezione prenotata
	if (TmrTimeout(gVMCState.PrenotaSelezTime) == false) {										// Selezione prenotata da meno del timeout: rieseguo la richiesta
		gVMCState.PrenotaSelezTime.tmrEnd = 0;
		NumVMCSelez = gVMCState.SelezPrenotata;												// Restore numero selezione richiesta
		gVMCState.SelezReq = true;
	}
	
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: ClearSelezPrenotata
	Annulla eventuale selezione prenotata.

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void  ClearSelezPrenotata(void) {
	gVMCState.PrenotaSelezTime.tmrEnd = 0;
}

/*------------------------------------------------------------------------------------------*\
 Method: SetPaymentConfig
 	 Ripristina le opzioni di configurazione dei sistemi di pagamento dopo una vendita di
 	 test.
 	 
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void SetPaymentConfig(void) {
	
  /*
	uint8_t	res;
	
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[ExecSlave]), 		(byte *)(&res),	1U);		// Executive SLAVE
		gVMC_ConfVars.ExecutiveSLV = (res & true) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]),    (byte *)(&res), 1U);		// Presenza Lettore Mifare
		gVMC_ConfVars.PresMifare = (res & true) ? true : false;
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]),  (byte *)(&res),	1U);		// Macchina in gratuito
		gVMC_ConfVars.Gratuito = (res & true) ? true : false;
	
	TestPaymentSettings();		
*/																// Verifica congruita' impostazioni e imposta valori predefiniti
}	
	

/*---------------------------------------------------------------------------------------------*\
Method: CheckCreditForSelection
	
	Macchina SENZA pulsanti:
	- Nella macchina senza pulsanti fa partire la Selezione 1 quando inserito credito
	  sufficiente.
	- In presenza di credito cash residuo sufficiente e senza carta, si incrementa tempo/impulsi
	  poco prima del termine erogazione per non avere OFF-ON uscite.
	
	- Con credito da carta:
	  1) con opzione "AutomaticMultiVend = Y", si decrementa il credito carta e si aggiunge 
	     tempo/pulses poco prima del termine erogazione per non avere OFF-ON uscite.
	- 2) con opzione "AutomaticMultiVend = N" si deve togliere e rimettere la carta per 
	     avere un'altra erogazione.
	- L'inserimento di credito cash in presenza di una carta serve alla sua ricarica.

	Macchina CON pulsanti:
	- si attiva automaticamente la Selezione 1 quando si inserisce un credito con
	  l'opzione "AutomaticStart" set.
	  L'attivazione avviene solo se la macchina e' in st-by senza credito.
	- Feb 2023: con opzione "AutomaticMultiVend = Y", si decrementa il credito (solo cash) 
	  e si aggiunge tempo/pulses poco prima del termine erogazione per non avere OFF-ON uscite.

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void  CheckCreditForSelection(void)
{
static	bool		CreditInserted = false, VendReq_Sent = false;
		uint8_t		SelezInProgress;
			
	if (gVMCState.InSelezione == true)
	{
		VendReq_Sent = false;																	// "Gestione_Selezioni" ha approvato la selezione richiesta, clr flag per abilitare nuove richieste
	}
	if (gVMCState.SelezReq == true) return;														// Selezione appena richiesta: attendo sua elaborazione
	if (VMC_CreditoDaVisualizzare == 0)															// Se Credito esaurito esco senza fare nulla
	{
		PreviousCredit = 0;
		VendReq_Sent = false;																	// Abilito nuove richieste di selezione
		CreditInserted = false;
		return;
	}
	if (gOrionConfVars.NO_Pulsanti == true)
	{
		//--------------------------------------
		//----    Macchina senza pulsanti ------
		//--------------------------------------
		if (PreviousCredit != VMC_CreditoDaVisualizzare)
		{
			if (PreviousCredit == 0) CreditInserted = true;										// Inserito credito da zero
			PreviousCredit = VMC_CreditoDaVisualizzare;
			VendReq_Sent = false;																// Abilito nuove richieste di selezione
		}
		SelezInProgress = GetSelectionInProgress();
		if (SelezInProgress == Selez_01)
		{
			//-- Selezione gia' in corso --
			if ( (gOrionKeyVars.Inserted == false) || ((gOrionKeyVars.Inserted == true) && (gOrionConfVars.AutomaticMultiVend == true)) )
			{
				if (IsRemainingLow(Selez_01) == false) 
				{
					return;
				}
			}
			else
			{
				return;
			}
		}
		else
		{
			if ((gOrionKeyVars.Inserted == true) && (CreditInserted == false)) return;			// Carta gia' nel lettore, non inserita da credito zero
			CreditInserted = false;																// Credito da carta inserita con credito zero, o credito da cash
		}
		if (VendReq_Sent == false)
		{
			VendReq_Sent = true;
			GestTastiSelez(Selez_01);															// Richiesta selezione 1, come da pulsante premuto
			// Todo: verificare se ci sono impedimenti dinamici (fVMC_Inibito, fVMC_NoLink) per riproporre piu' tardi un'altra richiesta
		}
	}
	else
	{
		//--------------------------------------
		//----    Macchina con   pulsanti ------
		//--------------------------------------
		if (PreviousCredit != VMC_CreditoDaVisualizzare)
		{
			if (PreviousCredit == 0) CreditInserted = true;										// Inserito credito da zero
			PreviousCredit = VMC_CreditoDaVisualizzare;
			VendReq_Sent = false;																// Abilito nuove richieste di selezione
		}
		SelezInProgress = GetSelectionInProgress();
		if (gOrionConfVars.AutomaticStart == true)
		{
			//-- Start Automatico della Selezione N. 1 --
			if (SelezInProgress != 0)
			{
	#if MULTI_CYCLE
				if (gOrionConfVars.MultiCycle == true)
				{
					if ((gOrionKeyVars.Inserted == false) && (PreviousCredit != 0))				// Test time/pulses rimanenti solo con credito cash
					{
						if (IsRemainingLow(SelezInProgress) == true) 
						{
							CreditInserted = false;												// Inserito credito da zero con macchina in St-By
							if (VendReq_Sent == false)
							{																	// Possibile richiedere nuova selezione (nessuna vend request in corso VendReq_Sent=false)
								VendReq_Sent = true;
								GestTastiSelez(SelezInProgress);								// Richiesta nuovamente la selezione in corso
								if (gVMCState.SelezReq == true)
								{
									gVMCState.NoDispCreditoInsuff = true;						// Selezione disponibile ma non visualizzare eventuale credito insufficiente
								}
								return;
								// Todo: verificare se ci sono impedimenti dinamici (fVMC_Inibito, fVMC_NoLink) per riproporre piu' tardi un'altra richiesta
							}
						}
					}
				}
	#endif
				if (SelezInProgress == Selez_01)
				{																				// Clr "Credito Inserito" sia che non sia stato inserito altro credito, sia
					CreditInserted = false;														// che sia stato inserito ma non quando credito era zero e macchina in St-By
					return;
				}
			}
			else
			{
				//-- Nessuna  Selezione In Corso --
				if (CreditInserted == true)
				{
					CreditInserted = false;														// Inserito credito da zero con macchina in St-By
					if (VendReq_Sent == false)
					{																			// Possibile richiedere nuova selezione (nessuna vend request in corso VendReq_Sent=false)
						VendReq_Sent = true;
						GestTastiSelez(Selez_01);												// Richiesta selezione 1, come da pulsante premuto
						// Todo: verificare se ci sono impedimenti dinamici (fVMC_Inibito, fVMC_NoLink) per riproporre piu' tardi un'altra richiesta
					}
				}
			}
		}
		else
		{
			//-- Start Solo Manuale da pulsante premuto --
	#if MULTI_CYCLE
			if (gOrionConfVars.MultiCycle == true)
			{
				if (SelezInProgress != 0)
				{
					if ((gOrionKeyVars.Inserted == false) && (PreviousCredit != 0))				// Test time/pulses rimanenti solo con credito cash
					{
						if (IsRemainingLow(SelezInProgress) == true) 
						{
							CreditInserted = false;												// Inserito credito da zero con macchina in St-By
							if (VendReq_Sent == false)
							{																	// Possibile richiedere nuova selezione (nessuna vend request in corso VendReq_Sent=false)
								VendReq_Sent = true;
								GestTastiSelez(SelezInProgress);								// Richiesta nuovamente la selezione in corso
								if (gVMCState.SelezReq == true)
								{
									gVMCState.NoDispCreditoInsuff = true;						// Selezione disponibile ma non visualizzare eventuale credito insufficiente
								}
							}
						}
					}
				}
			}
	#endif
		}	// End "if AutomaticStart"
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: IsRemainingLow
	Restituisce true se il tempo o gli impulsi rimanenti nella selezione richiesta sono
	inferiori ai limiti previsti.

	IN:	 - numero selezione (1-n)
	OUT: - true se tempo o impulsi rimasti sono sotto il limite minimo
\*----------------------------------------------------------------------------------------------*/
bool  IsRemainingLow(uint8_t NumSel)
{
	if ((sSelParam_A[NumSel-1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
	{
		if (IMPULSI_RESIDUI > CntFlussRemaining) return true;
	}
	else
	{
		if (TIME_RESIDUO > TimeRemaining) return true;
	}
	return false;
}
