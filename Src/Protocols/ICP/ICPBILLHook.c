/****************************************************************************************
File:
    ICPBILLHook.c

Description:
    Funzioni di aggancio all'applicazione per protocollo ICP Bill Validator

History:
    Date       Aut  Note
    Set 2012 	Abe   

*****************************************************************************************/

#include "ORION.H"
#include "ICP.h"
#include "icpBILL.h"
#include "ICPBILLHook.h"
#include "ICPVMC.h"
#include "ICPGlobal.h"


/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/
uint8_t 	EnDis_Bill = 0;


extern 	CreditCoinValue BVHBillValuesList[];
extern	uint16_t	Dec16Touint16(uint16_t);
extern	void 		CreditMasterCashAdd(CreditValue amount, CreditCashTypes cashType);
extern	void 		BillAuditStart(ET_BILLPOLLDEST billDest, uint8_t billType, CreditCoinValue billValue);

extern  uint16_t	MaxCash;

//MR19 extern bool BillValueChanged;


/*------------------------------------------------------------------------------------------*\
 Method: ICPBillHookEnDis
	Controlla se abilitare/inibire il Bill Validator in base a diversi parametri quali lo
	stato dell'Orion, flags varie (NoLink, Inh, etc), il credito massimo cash raggiunto.
	La flag "EnDis_Chg" assume lo stato attuale che il BV potra' assumere: se = BILL_EN 
	significa che il BV e' inibito e potra' solo essere enabled, se = BILL_DIS significa
	che il BV e' enabled e potra' solo essere inibito.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ICPBillHookEnDis(void) {

	uint16_t CashLimit = 0;
	uint16_t CashMax = MaxCash;
	// Test se il BV deve essere inhibit: se il master non e' ENABLE o IDLE il BV deve essere inhibit
	//if (gOrionKeyVars.State != kOrionCPCStates_Enabled && gOrionKeyVars.State != kOrionCPCStates_Idle || fVMC_NoLink || fVMC_Inibito){
	if (fVMC_NoLink || fVMC_Inibito || gVars.AudExtFull ) {
		if (EnDis_Bill == BILL_DIS){
			if(!ICPBILLInhibit()){
				EnDis_Bill = BILL_EN;
			}
		}
		return;
	}	
	if (gOrionKeyVars.Inserted) CashMax = MaxRevalue;											// MaxRevalue se la carta e' inserita
	switch (EnDis_Bill){
		case BILL_EN:
			// --  Il Bill Validator e' inibito --
			if (!gOrionKeyVars.Inserted && !gOrionKeyVars.BillValidEnaW_Card ) break;
			if (CashMax >= gOrionCashVars.creditCash) CashLimit = CashMax - gOrionCashVars.creditCash;
			if (!ICPBILLEnable(CashLimit)) EnDis_Bill = BILL_DIS;
			break;
					
		case BILL_DIS:
			// --  Il Bill Validator e' abilitato --
			if (gOrionKeyVars.BillValidEnaW_Card) break;
			if (gOrionKeyVars.Inserted) break;
			if (!ICPBILLInhibit()) EnDis_Bill = BILL_EN;
			break;

		default:
			EnDis_Bill = BILL_EN;
			break;
		}
}

/*--------------------------------------------------------------------------------------*\
Method: CheckBillCashType
	Funzione chiamata al cambiamento del cash per verificare l'abilitazione/inibizione
	del bill validator.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void CheckBillCashType(void) {

	EnDis_Bill = BILL_EN;
	if (gOrionKeyVars.BillValidEnaW_Card) return;
	if (!gOrionKeyVars.Inserted) EnDis_Bill = BILL_DIS;
}

/*--------------------------------------------------------------------------------------*\
Method: ICPBillHookTaskInit
	.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void ICPBillHookTaskInit(void) {
	
	// Tolto il 08.08.17 BillEnMask = 0xffff;																		// Valore fisso per prova --> da sostituire con valore configurazione
	num_icp_bill = 0;

}

/*--------------------------------------------------------------------------------------*\
Method: Bill_Task
	Main Task Bill Validator: chiamata continuamente dal Main

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void Bill_Task(void) {
	
    if (!sCMDICPCash.acceptAmountBillChgd) return;
	ICPBillHookEnDis();
}


/*--------------------------------------------------------------------------------------*\
Method: ICPBILLHook_SetReady
  Indicate Bill validator state
  
Parameters:
  avail ->  true if ready and available
\*--------------------------------------------------------------------------------------*/
void ICPBILLHook_SetReady(bool avail) {
	
	uint8_t i;
	uint16_t cMask = 0;
	CreditCoinValue valbill;
	
	if(avail) {
		sCMDICPCash.acceptAmountBillChgd= true;
		for (i=0; i<ICP_MAX_BILLTYPES; i++) {
			if (ICPProt_BILLGetBillValue(i)) {
				cMask |= 1<<i;
				num_icp_bill++;
			}
			valbill = ICPBILLGetBillValue(i);
			if (valbill != BVHBillValuesList[i]) {
				BVHBillValuesList[i] = valbill;
				//MR19 BillValueChanged = TRUE;			//Non utilizzata da nessuno
			}
		}
    sCMDICPCash.billEnableMask = (BillEnMask & cMask);   										//BillEnableMask lettura da configurazione
    ICPBILLSetBillEnableMask(sCMDICPCash.billEnableMask);
	//(*sCMDICPCash.cashTypeAvail)(kCreditCashTypeBill);  										// Todo da verificare a cosa serve ABELE
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ICPBILLHook_SetBillsDeposited
  Called to indicate that a bill has been deposited

Parameters:
  billDest  ->  bill destination
  billType  ->  bill type
  billValue ->  bill value
Returns:
  
\*--------------------------------------------------------------------------------------*/
void ICPBILLHook_SetBillsDeposited(ET_BILLPOLLDEST billDest, uint8_t billType, CreditCoinValue billValue) {
	
    BillAuditStart(billDest, billType, billValue);
    
  switch(billDest) {
  case ICP_BILL_STACKED:
    CreditMasterCashAdd(billValue, kCreditCashTypeBill);
    // CheckBillCashType();   // Chiamata spostata in CreditMasterCashAdd //se ricevo valore banconota controllo lo stato per eventualmente inviare comando bill disable
    BillAuditStart(billDest, billType, billValue);
    break;
  case ICP_BILL_ESCROWPOS:
    // Boo... NYI
    break;
  case ICP_BILL_RETURNED:
  case ICP_BILL_DISBILLREJECTED:
    // Boo... ignore this indication
    break;
  }

}

/*--------------------------------------------------------------------------------------*\
Method: ICPBILLHook_SetDPP
  Remember DPP being used by Bill validator
Parameters:
  inDPP ->  decimal point position
\*--------------------------------------------------------------------------------------*/
void ICPBILLHook_SetDPP(uint8_t nDPP) {
//  sCHMDBVars.billDPP= inDPP;
}

/*--------------------------------------------------------------------------------------*\
Functions group to store bill validator general info

\*--------------------------------------------------------------------------------------*/

void ICPBILLHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen) {
	
	Set_Bill_Info_Data(0, nDataLen, ptData );
}
void ICPBILLHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen) {
	Set_Bill_Info_Data(SIZE_MANUFACTURER, nDataLen, ptData );
}
void ICPBILLHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen) {
	Set_Bill_Info_Data(SIZE_MANUFACTURER+SIZE_SERIAL, nDataLen, ptData );
}


/*------------------------------------------------------------------------------------------*\
 Method: ICPBILLHook_SetSWVersion
	Memorizza Bill Validator Sw Revision nella struttura Info del Bill.
	Modificata in giu 2019 perche' incompatibile il pnt a uint16_t di questa funzione
	con il pnt a uint8_t della Set_Bill_Info_Data (Col CodeWarrior funzionava!!!!)

   IN:  -   nSwVersion
  OUT:  -   
\*------------------------------------------------------------------------------------------*/
void ICPBILLHook_SetSWVersion(uint16_t nSwVersion) {
	
  	uint16_t	HexVal;

//MR19 Questa dava errore "Error[Pe167]: argument of type "uint16_t *" is incompatible with parameter of type "uint8_t *"
/*
	HexVal = Dec16Touint16(nSwVersion);																// nSwVersion e' gia' ASCII, lo trasformo in Hex perche' nell'Audit.c la sprintf lo
	Set_Bill_Info_Data(SIZE_MANUFACTURER+SIZE_SERIAL+SIZE_MODEL, sizeof(HexVal), &HexVal );	// ritrasforma nuovamente in ASCII prima di metterlo nel CA1
	//Set_Bill_Info_Data(SIZE_MANUFACTURER+SIZE_SERIAL+SIZE_MODEL, sizeof(nSwVersion), &nSwVersion );
*/	
	HexVal = Dec16Touint16(nSwVersion);
	Set_Bill_SwRev_Data(HexVal);

}

/*------------------------------------------------------------------------------------------*\
 Method: DisattivaEnDisBill
   Disattiva la variabile "EnDis_Bill" per far trasmettere la nuova abilitazione banconote
   al termine del ciclo di reset Bill Validator

   IN:  -   
  OUT:  -   
\*------------------------------------------------------------------------------------------*/
void  DisattivaEnDisBill(void) {
	EnDis_Bill = BILL_EN;
}

/*--------------------------------------------------------------------------------------*\
Unused ICP protocol BILL hooks  
\*--------------------------------------------------------------------------------------*/
void ICPBILLHook_SetCountryCode(uint16_t nCountryCode) {}
void ICPBILLHook_SetCurrency(uint16_t nCurrency) {}
void ICPBILLHook_SetFTL(bool fAvailable) {}
bool ICPBILLHook_GetFTL(void) { return false; }
void ICPBILLHook_SetDiagData(uint8_t *ptData, uint8_t nDataLen) {}

void ICPBILLHook_SetEscrow(bool fEscrow) {}
void ICPBILLHook_SetStackerFull(bool fStackerFull) {}



