/*****************************************************************************************************************************************************
*
*  File:
*   DDCMPDTSHook.h
*
*  Description:
*   Handle Hook Functions between DDCMP Communication Protocols and System Application.
*
*  History:
*   Date        Aut  Notes
*
*
*****************************************************************************************************************************************************/


#ifndef __DDCMPDTSHook__
#define __DDCMPDTSHook__


void 	DataTranferTx(uint16_t dataLen, bool selFlag);
void 	DataTranferEnd(void);
bool 	ReadTipoLista(void);
void 	DDCMPDTS2Initialization(void);
bool 	DDCMPDTS2HookConnIsEstablished(void);
void 	DDCMPMngrTxBegin(void);
bool 	DDCMPMngrHookConnEstablishedInd(void);
void 	DDCMPMngrHookBufferAudit(char *pnt, uint Size);
void 	DDCMPMngrHookCopyLocalBuffer(void);
bool  	AuditTransferAllowed(void);
void 	DDCMP2HookCommOpen(void);


#endif  // __DDCMPDTSHook__
