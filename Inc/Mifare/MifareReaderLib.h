/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file MifareReaderLib.h
 *
 * Projekt: Object Oriented Reader Library Framework BAL component.
 *
 *  Source: MifareReaderLib.h
 * $Author: mha $
 * $Revision: 1.10 $
 * $Date: Wed Jun  7 09:49:20 2006 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */

#ifndef __MIFARE_READER_LIB_H__
#define __MIFARE_READER_LIB_H__

//typedef unsigned char bool;

uint16_t	ActivateMifareInit(void *comHandle, uint32_t aSettings);
bool 		MifareReaderRequest(void);
bool 		MifareReaderAnticoll(uint8_t *ptSnum);
uint16_t	MifareReaderWrite(uint8_t *outbuf, uint8_t Sector, uint8_t BlockNum, bool ReadAfterWrite);
uint16_t 	MifareWriteAndCompare(uint8_t *wrbuff, uint8_t SectorToWrite, uint8_t *BlockToWrite, bool ReadAfterWrite);
uint16_t	MifareReaderRead(uint8_t *outbuf, uint8_t Sector, uint8_t *BlockNum);
//uint16_t	MifareReaderRead(uint8_t *outbuf, uint8_t Sector, DATAMIFARE *BlockNum);
uint16_t 	MifareCompareVersion(void);
void	 	InitMifareAmbient(void);
uint16_t 	MifareReaderInit(void *comHandle, uint32_t aSettings);
uint16_t 	MF_Sw_Reset(void);
uint16_t 	MF_DisableDrivers(void);


phcsBfl_Status_t PN512_getr (uint8_t addr, uint8_t* val);
phcsBfl_Status_t PN512_setr (uint8_t address, uint8_t dato);
phcsBfl_Status_t MifareReaderLedInit(uint8_t dato);
phcsBfl_Status_t MifareReaderUARTSpeed(uint32_t baudrate);


#define EXAMPLE_TRX_BUFFER_SIZE       64      


#endif /* __MIFARE_READER_C_H__ */
