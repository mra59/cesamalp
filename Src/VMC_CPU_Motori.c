/****************************************************************************************
File:
    VMC_CPU_Motori.c

Description:
    File con le funzioni per l'attivazione degli attuatori per le selezioni.

History:
    Date       Aut  Note
    Lug 2020	MR   

*****************************************************************************************/

#include <string.h>

#include "ORION.H" 
#include "VMC_CPU_Motori.h"
#include "OrionTimers.h"
#include "Gestore_Exec_MSTSLV.h"
#include "VMC_CPU_LCD.h"
#include "Power.h"
#include "VMC_CPU_Prog.h"
#include "VMC_CPU_HW.h"
#include "IICEEP.h"
#include "I2C_EEPROM.h"
#include "IoT_Files.h"
#include "main.h"

#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void  	Gestione_MDB(void);
extern	void  	HexToDec(uint8_t, uint8_t*);
extern	void	HAL_Delay(uint32_t Delay);
extern	void 	HexByteToAsciiInt(uint8_t, uint16_t*, uint8_t);
extern	void	HAL_SuspendTick(void);
extern	void	HAL_ResumeTick(void);
extern	void  	AzzeraCashInOverpay(void);

extern	const 		uint8_t SelezAvailable[];
extern	uint16_t	Ventolino[NUMCNTVOLUM];

/*--------------------------------------------------------------------------------------*\
	Variable Declarations 
\*--------------------------------------------------------------------------------------*/

bool				ICC_Alarm;																	// Flag Allarme CC uscite
static	Erogaz		sErogaz1, sErogaz2, sErogaz3, sErogaz4;

static  TimeOutMot 	ToutMotore;
//static	uint8_t		IntegrCortoCirc[MAX_OUTPUT];
uint32_t			PulseCounterSet;
uint32_t			TimeRemaining, PreviousTimeRemaining;
uint16_t			CntFlussRemaining, PreviousCntFluss;
uint32_t			TimeToDisplayFlussCnt;


/*---------------------------------------------------------------------------------------------*\
Method: Start_Erogazione
	Le macchine possono avere 4 pulsanti o nessun pulsante (es. aspirapolvere).
	Con i 4 pulsanti si erogano 4 servizi diversi che possono essere conteggiati
	a tempo (1-1.000 sec) o a impulsi da un flussimetro (1-10.000).
	Con credito carta o cash uguale o superiore al relativo Prezzo e attivando un pulsante si 
	attiva la relativa uscita; nella versione senza pulsanti si esegue la selezione 1.
	
	Al termine del servizio, se il credito � uguale o superiore, il suo Prezzo e' scalato 
	in modo automatico e riparte una nuova erogazione.

	Premendo un pulsante diverso da quello in corso, si disattiva l'attuale uscita e si attiva 
	la nuova uscita richiesta: il tempo o gli impulsi sono il residuo in percentuale di quanto 
	gia' utilizzato col pulsante precedente.
	Es: (Time pulsante 1 = 60 sec - Time Pulsante 2 = 40 sec) Dopo 30 secondi dalla pressione
	del pulsante 1 premo il pulsante 2 che attiva l'uscita 2 per 20 sec.

	E' tramite la "Ciclo_Erogazione" chiamata dall'IRQ da 1 msec che si controlla poi il termine
	effettivo della selezione.

	Per un certo tempo dall'attivazione delle uscite si controlla il relativo bit di CortoCircuito
	per disabilitare, se attivo, la selezione.


	IN:	  - numero selezione (1-n)
		  - flag usa i valori di configurazione o quelli di percentuale quando la selezione e'
		  - richiesta mentre era in corso un'altra selezione
		  - flag scambio selezione per disattivare le uscite della selezione in corso.
	OUT:  - false se attivazione OK
\*----------------------------------------------------------------------------------------------*/
bool  Start_Erogazione(uint8_t NumeroSelez, bool Percentage)
{
	ICC_Alarm = false;
	switch(NumeroSelez)
	{
		case Selez_01:
			ClrErogazParameters(sSelParam_A[0].FlussNum);
			memset (&sErogaz1, 0x0, sizeof(sErogaz1));
			if ((sSelParam_A[0].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if (Percentage == USE_PERCENTAGE)
				{
					PulseCounterSet = sSelParam_A[0].FlowCounterPercentuale;
				}
				else
				{
					PulseCounterSet = sSelParam_A[0].FlowCounterNominale;
				}
				TmrStart(sErogaz1.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
			}
			else
			{
				if (Percentage == USE_PERCENTAGE)
				{
					TmrStart(sErogaz1.ToutErogaz, sSelParam_A[0].TimeErogazionePercentuale);	// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				else
				{
					TmrStart(sErogaz1.ToutErogaz, sSelParam_A[0].TimeErogazione);				// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				TimeRemaining = (sErogaz1.ToutErogaz.tmrEnd);
			}
			sErogaz1.InProgress = true;
			DisableRemainingOutputs(Selez_01);													// Disattivo le altre uscite selezioni
			SetOutputs(Selez_01, ON);
			HAL_Delay(Milli5);																	// Attendo attivazione uscite
			TmrStart(ToutMotore.WaitStop, TIME_CHECK_EV);										// Controllo i primi "TIME_CHECK_EV" msec che l'EV da attivare non sia in CC
			while(TmrTimeout(ToutMotore.WaitStop) == false)
			{
				if (ICC_Alarm == true)
				{																				// RILEVATA CORRENTE DA CORTO CIRCUITO EV
					SetOutputs(Selez_01, OFF);			
					sSelParam_A[0].SelFlags |= BIT_INH_SEL;										// Selezione inibita
					sSelParam_A[0].EV_CC_Failure = true;										// EV guasta
					sErogaz1.InProgress = false;
					return true;
				}
			}
			break;

		case Selez_02:
			ClrErogazParameters(sSelParam_A[1].FlussNum);
			memset (&sErogaz2, 0x0, sizeof(sErogaz2));
			if ((sSelParam_A[1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if (Percentage == USE_PERCENTAGE)
				{
					PulseCounterSet = sSelParam_A[1].FlowCounterPercentuale;
				}
				else
				{
					PulseCounterSet = sSelParam_A[1].FlowCounterNominale;
				}
				TmrStart(sErogaz2.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
			}
			else
			{
				if (Percentage == USE_PERCENTAGE)
				{
					TmrStart(sErogaz2.ToutErogaz, sSelParam_A[1].TimeErogazionePercentuale);	// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				else
				{
					TmrStart(sErogaz2.ToutErogaz, sSelParam_A[1].TimeErogazione);				// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				TimeRemaining = (sErogaz2.ToutErogaz.tmrEnd);
			}
			sErogaz2.InProgress = true;
			DisableRemainingOutputs(Selez_02);													// Disattivo le altre uscite selezioni
			SetOutputs(Selez_02, ON);
			HAL_Delay(Milli5);																	// Attendo attivazione uscite
			TmrStart(ToutMotore.WaitStop, TIME_CHECK_EV);										// Controllo i primi "TIME_CHECK_EV" msec che l'EV da attivare non sia in CC 
			while(TmrTimeout(ToutMotore.WaitStop) == false)
			{
				if (ICC_Alarm == true)
				{																				// RILEVATA CORRENTE DA CORTO CIRCUITO EV
					SetOutputs(Selez_02, OFF);
					sSelParam_A[1].SelFlags |= BIT_INH_SEL;										// Selezione inibita
					sSelParam_A[1].EV_CC_Failure = true;										// EV guasta
					sErogaz2.InProgress = false;
					return true;
				}
			}
			break;

		case Selez_03:
			ClrErogazParameters(sSelParam_A[2].FlussNum);
			memset (&sErogaz3, 0x0, sizeof(sErogaz3));
			if ((sSelParam_A[2].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if (Percentage == USE_PERCENTAGE)
				{
					PulseCounterSet = sSelParam_A[2].FlowCounterPercentuale;
				}
				else
				{
					PulseCounterSet = sSelParam_A[2].FlowCounterNominale;
				}
				TmrStart(sErogaz3.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
			}
			else
			{
				if (Percentage == USE_PERCENTAGE)
				{
					TmrStart(sErogaz3.ToutErogaz, sSelParam_A[2].TimeErogazionePercentuale);	// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				else
				{
					TmrStart(sErogaz3.ToutErogaz, sSelParam_A[2].TimeErogazione);				// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				TimeRemaining = (sErogaz3.ToutErogaz.tmrEnd);
			}
			sErogaz3.InProgress = true;
			DisableRemainingOutputs(Selez_03);													// Disattivo le altre uscite selezioni
			SetOutputs(Selez_03, ON);
			HAL_Delay(Milli5);																	// Attendo attivazione uscite
			TmrStart(ToutMotore.WaitStop, TIME_CHECK_EV);										// Controllo i primi "TIME_CHECK_EV" msec che l'EV da attivare non sia in CC 
			while(TmrTimeout(ToutMotore.WaitStop) == false)
			{
				if (ICC_Alarm == true)
				{																				// RILEVATA CORRENTE DA CORTO CIRCUITO EV
					SetOutputs(Selez_03, OFF);
					sSelParam_A[2].SelFlags |= BIT_INH_SEL;										// Selezione inibita
					sSelParam_A[2].EV_CC_Failure = true;										// EV guasta
					sErogaz3.InProgress = false;
					return true;
				}
			}
			break;

		case Selez_04:
			ClrErogazParameters(sSelParam_A[3].FlussNum);
			memset (&sErogaz4, 0x0, sizeof(sErogaz4));
			if ((sSelParam_A[3].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				if (Percentage == USE_PERCENTAGE)
				{
					PulseCounterSet = sSelParam_A[3].FlowCounterPercentuale;
				}
				else
				{
					PulseCounterSet = sSelParam_A[3].FlowCounterNominale;
				}
				TmrStart(sErogaz4.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
			}
			else
			{
				if (Percentage == USE_PERCENTAGE)
				{
					TmrStart(sErogaz4.ToutErogaz, sSelParam_A[3].TimeErogazionePercentuale);	// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				else
				{
					TmrStart(sErogaz4.ToutErogaz, sSelParam_A[3].TimeErogazione);				// Selezione a tempo: il timer e' usato come tempo erogazione
				}
				TimeRemaining = (sErogaz4.ToutErogaz.tmrEnd);
			}
			sErogaz4.InProgress = true;
			DisableRemainingOutputs(Selez_04);													// Disattivo le altre uscite selezioni
			SetOutputs(Selez_04, ON);
			HAL_Delay(Milli5);																	// Attendo attivazione uscite
			TmrStart(ToutMotore.WaitStop, TIME_CHECK_EV);										// Controllo i primi "TIME_CHECK_EV" msec che l'EV da attivare non sia in CC 
			while(TmrTimeout(ToutMotore.WaitStop) == false)
			{
				if (ICC_Alarm == true)
				{																				// RILEVATA CORRENTE DA CORTO CIRCUITO EV
					SetOutputs(Selez_04, OFF);
					sSelParam_A[3].SelFlags |= BIT_INH_SEL;										// Selezione inibita
					sSelParam_A[3].EV_CC_Failure = true;										// EV guasta
					sErogaz4.InProgress = false;
					return true;
				}
			}
			break;

		default:
			break;				  
	}
	return false;
}	

/*---------------------------------------------------------------------------------------------*\
Method: Add_Time_Pulses
	Aggiunge tempo o impulsi alla erogazione in corso.

	IN:	  - numero selezione (1-n)
	OUT:  - false se attivazione OK
\*----------------------------------------------------------------------------------------------*/
void  Add_Time_Pulses(uint8_t NumeroSelez)
{
	switch(NumeroSelez)
	{
		case Selez_01:
			if ((sSelParam_A[0].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				HAL_SuspendTick();
				PulseCounterSet += sSelParam_A[0].FlowCounterNominale;
				TmrAdd(sErogaz1.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
				HAL_ResumeTick();
				PreviousCntFluss = 0;
			}
			else
			{
				HAL_SuspendTick();
				TmrAdd(sErogaz1.ToutErogaz, sSelParam_A[0].TimeErogazione);						// Selezione a tempo: il timer e' usato come tempo erogazione
				TimeRemaining = (sErogaz1.ToutErogaz.tmrEnd);
				HAL_ResumeTick();
				PreviousTimeRemaining = 0;														// Per visualizzare subito il nuovo tempo residuo
			}
			break;

		case Selez_02:
			if ((sSelParam_A[1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				HAL_SuspendTick();
				PulseCounterSet += sSelParam_A[1].FlowCounterNominale;
				TmrAdd(sErogaz2.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
				HAL_ResumeTick();
				PreviousCntFluss = 0;
			}
			else
			{
				HAL_SuspendTick();
				TmrAdd(sErogaz2.ToutErogaz, sSelParam_A[1].TimeErogazione);						// Selezione a tempo: il timer e' usato come tempo erogazione
				TimeRemaining = (sErogaz2.ToutErogaz.tmrEnd);
				HAL_ResumeTick();
				PreviousTimeRemaining = 0;														// Per visualizzare subito il nuovo tempo residuo
			}
			break;

		case Selez_03:
			if ((sSelParam_A[2].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				HAL_SuspendTick();
				PulseCounterSet += sSelParam_A[2].FlowCounterNominale;
				TmrAdd(sErogaz3.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
				HAL_ResumeTick();
				PreviousCntFluss = 0;
			}
			else
			{
				HAL_SuspendTick();
				TmrAdd(sErogaz3.ToutErogaz, sSelParam_A[2].TimeErogazione);						// Selezione a tempo: il timer e' usato come tempo erogazione
				TimeRemaining = (sErogaz3.ToutErogaz.tmrEnd);
				HAL_ResumeTick();
				PreviousTimeRemaining = 0;														// Per visualizzare subito il nuovo tempo residuo
			}
			break;

		case Selez_04:
			if ((sSelParam_A[3].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				HAL_SuspendTick();
				PulseCounterSet += sSelParam_A[3].FlowCounterNominale;
				TmrAdd(sErogaz4.ToutErogaz, TOUT_EROGAZ);										// Selezione a impulsi: il timer e' usato come timeout sicurezza
				HAL_ResumeTick();
				PreviousCntFluss = 0;
			}
			else
			{
				HAL_SuspendTick();
				TmrAdd(sErogaz4.ToutErogaz, sSelParam_A[3].TimeErogazione);						// Selezione a tempo: il timer e' usato come tempo erogazione
				TimeRemaining = (sErogaz4.ToutErogaz.tmrEnd);
				HAL_ResumeTick();
				PreviousTimeRemaining = 0;														// Per visualizzare subito il nuovo tempo residuo
			}
			break;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: Ciclo_Erogazione
	Funzione chiamata dall'IRQ del timer da 1 msec.	
	Controlla quale erogazione e' in funzione per determinarne la fine.
	Posso operare direttamente sui bytes di uscita perche' sono in IRQ del PIT0
	da 1 msec.

 	IN : - 
 	OUT: - 
\*--------------------------------------------------------------------------------------*/
void  Ciclo_Erogazione(void)
{
	if ((Micro1_8 & EMERGENCY) == EMERGENCY)
	{																							// Attivazione Emergency
		#if DEB_NO_MICROSW
			return;
		#else
			SetOutputs(Selez_01, OFF);																// Disattivo tutte le uscite selezioni
			SetOutputs(Selez_02, OFF);
			SetOutputs(Selez_03, OFF);
			SetOutputs(Selez_04, OFF);
	
			sErogaz1.InProgress = false;
			sErogaz2.InProgress = false;
			sErogaz3.InProgress = false;
			sErogaz4.InProgress = false;
			Set_fVMC_Inibito;
			return;
		#endif
	}
	// ==================================
	// ---- Ciclo Erogazione  Sel  1  ---
	// ==================================
	if (sErogaz1.InProgress == true)
	{		
		//-- Controllo Corto Circuito Uscite --
		if (ICC_Alarm == true)
		{
			SetOutputs(Selez_01, OFF);
			sSelParam_A[0].SelFlags |= BIT_INH_SEL;												// Selezione Disabilitata
			sSelParam_A[0].EV_CC_Failure = true;												// Selezione Disabilitata per CC Uscite
			sErogaz1.InProgress = false;
			return;
		}

		//-- Pulizia  Filtro --
		if (sErogaz1.FilterCleaning == true)
		{
			if (sErogaz1.DelayBeforeFilterClean == true)
			{
				if (TmrTimeout(sErogaz1.ToutErogaz))
				{
					TmrStart(sErogaz1.ToutErogaz, TIME_FILTER_CLEAN);
					sErogaz1.DelayBeforeFilterClean = false;
					SetOutputs(FILTER_CLEAN, ON);
				}
			}
			else
			{
				if (TmrTimeout(sErogaz1.ToutErogaz))
				{
					SetOutputs(FILTER_CLEAN, OFF);
					TimeRemaining = 0;
					sErogaz1.InProgress = false;
					sErogaz1.FilterCleaning = false;
					fTastoElab1 = true;															// Per evitare, in caso di pressione in questo istante, una nuova erogazione
				}
			}
			return;			
		}
		//-- Erogazione in Corso --
		if ((sSelParam_A[0].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
		{
			//--  Selezione a impulsi --
			if (Ventolino[(sSelParam_A[0].FlussNum) - 1] >= PulseCounterSet)
			{																					// Erogazione terminata
				SetOutputs(Selez_01, OFF);
				if (sSelParam_A[0].CleanFilterOut > 0)											// Programmata attivazione pulizia filtro
				{
					TmrStart(sErogaz1.ToutErogaz, DELAY_BEFORE_FILTER_START);
					sErogaz1.DelayBeforeFilterClean = true;
					sErogaz1.FilterCleaning = true;
				}
				else
				{
					sErogaz1.InProgress = false;
					fTastoElab1 = true;															// Per evitare, in caso di pressione in questo istante, una nuova erogazione
				}
			}
			else
			{
				CntFlussRemaining = (PulseCounterSet - Ventolino[sSelParam_A[0].FlussNum - 1]);
				if (TmrTimeout(sErogaz1.ToutErogaz))
				{
					//-- Timeout scaduto --
					SetOutputs(Selez_01, OFF);
					sSelParam_A[0].SelFlags |= BIT_INH_SEL;										// Selezione Disabilitata
					sSelParam_A[0].SEL_Timeout = true;											// Selezione Disabilitata per Timeout
					sErogaz1.InProgress = false;
				}
			}
		}
		else
		{
			//--  Selezione a Tempo --
			if (TmrTimeout(sErogaz1.ToutErogaz))												// Selezione a tempo: il timer e' usato come tempo erogazione
			{
				TimeRemaining = 0;
				SetOutputs(Selez_01, OFF);
				if (sSelParam_A[0].CleanFilterOut > 0)											// Programmata attivazione pulizia filtro
				{
					TmrStart(sErogaz1.ToutErogaz, DELAY_BEFORE_FILTER_START);
					sErogaz1.DelayBeforeFilterClean = true;
					sErogaz1.FilterCleaning = true;
				}
				else
				{
					sErogaz1.InProgress = false;
					fTastoElab1 = true;															// Per evitare, in caso di pressione in questo istante, una nuova erogazione
				}
			}
			else
			{
				TimeRemaining = (sErogaz1.ToutErogaz.tmrEnd - gVars.gKernelFreeCounter);
			}
		}
	}
	
	// ==================================
	// ---- Ciclo Erogazione  Sel  2  ---
	// ==================================
	if (sErogaz2.InProgress == true)
	{
		//-- Controllo Corto Circuito Uscite --
		if (ICC_Alarm == true)
		{
			SetOutputs(Selez_02, OFF);
			sSelParam_A[1].SelFlags |= BIT_INH_SEL;												// Selezione Disabilitata
			sSelParam_A[1].EV_CC_Failure = true;												// Selezione Disabilitata per CC Uscite
			sErogaz2.InProgress = false;
			return;
		}

		//-- Erogazione in Corso --
		if ((sSelParam_A[1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
		{
			//--  Selezione a impulsi --
			if (Ventolino[(sSelParam_A[1].FlussNum) - 1] >= PulseCounterSet)
			{																					// Erogazione terminata
				sErogaz2.InProgress = false;
				SetOutputs(Selez_02, OFF);
				fTastoElab2 = true;																// Per evitare, in caso di pressione in questo istante, una nuova erogazione
			}
			else
			{
				CntFlussRemaining = (PulseCounterSet - Ventolino[sSelParam_A[1].FlussNum - 1]);
				if (TmrTimeout(sErogaz2.ToutErogaz))
				{
					//-- Timeout scaduto --
					SetOutputs(Selez_02, OFF);
					sSelParam_A[1].SelFlags |= BIT_INH_SEL;										// Selezione Disabilitata
					sSelParam_A[1].SEL_Timeout = true;											// Selezione Disabilitata per Timeout
					sErogaz2.InProgress = false;
				}
			}
		}
		else
		{
			//--  Selezione a Tempo --
			if (TmrTimeout(sErogaz2.ToutErogaz))												// Selezione a tempo: il timer e' usato come tempo erogazione
			{
				TimeRemaining = 0;
				sErogaz2.InProgress = false;
				SetOutputs(Selez_02, OFF);
				fTastoElab2 = true;																// Per evitare, in caso di pressione in questo istante, una nuova erogazione
			}
			else
			{
				TimeRemaining = (sErogaz2.ToutErogaz.tmrEnd - gVars.gKernelFreeCounter);
			}
		}
	}

	// ==================================
	// ---- Ciclo Erogazione  Sel  3  ---
	// ==================================
	if (sErogaz3.InProgress == true)
	{
		//-- Controllo Corto Circuito Uscite --
		if (ICC_Alarm == true)
		{
			SetOutputs(Selez_03, OFF);
			sSelParam_A[2].SelFlags |= BIT_INH_SEL;												// Selezione Disabilitata
			sSelParam_A[2].EV_CC_Failure = true;												// Selezione Disabilitata per CC Uscite
			sErogaz3.InProgress = false;
			return;
		}

		//-- Erogazione in Corso --
		if ((sSelParam_A[2].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
		{
			//--  Selezione a impulsi --
			if (Ventolino[(sSelParam_A[2].FlussNum) - 1] >= PulseCounterSet)
			{																					// Erogazione terminata
				sErogaz3.InProgress = false;
				SetOutputs(Selez_03, OFF);
				fTastoElab3 = true;																// Per evitare, in caso di pressione in questo istante, una nuova erogazione
			}
			else
			{
				CntFlussRemaining = (PulseCounterSet - Ventolino[sSelParam_A[2].FlussNum - 1]);
				if (TmrTimeout(sErogaz3.ToutErogaz))
				{
					//-- Timeout scaduto --
					SetOutputs(Selez_03, OFF);
					sSelParam_A[2].SelFlags |= BIT_INH_SEL;										// Selezione Disabilitata
					sSelParam_A[2].SEL_Timeout = true;											// Selezione Disabilitata per Timeout
					sErogaz3.InProgress = false;
				}
			}
		}
		else
		{
			//--  Selezione a Tempo --
			if (TmrTimeout(sErogaz3.ToutErogaz))												// Selezione a tempo: il timer e' usato come tempo erogazione
			{
				TimeRemaining = 0;
				sErogaz3.InProgress = false;
				SetOutputs(Selez_03, OFF);
				fTastoElab3 = true;																// Per evitare, in caso di pressione in questo istante, una nuova erogazione
			}
			else
			{
				TimeRemaining = (sErogaz3.ToutErogaz.tmrEnd - gVars.gKernelFreeCounter);
			}
		}
	}

	// ==================================
	// ---- Ciclo Erogazione  Sel  4  ---
	// ==================================
	if (sErogaz4.InProgress == true)
	{
		//-- Controllo Corto Circuito Uscite --
		if (ICC_Alarm == true)
		{
			SetOutputs(Selez_04, OFF);
			sSelParam_A[3].SelFlags |= BIT_INH_SEL;												// Selezione Disabilitata
			sSelParam_A[3].EV_CC_Failure = true;												// Selezione Disabilitata per CC Uscite
			sErogaz4.InProgress = false;
			return;
		}

		//-- Erogazione in Corso --
		if ((sSelParam_A[3].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
		{
			//--  Selezione a impulsi --
			if (Ventolino[(sSelParam_A[3].FlussNum) - 1] >= PulseCounterSet)
			{																					// Erogazione terminata
				sErogaz4.InProgress = false;
				SetOutputs(Selez_04, OFF);
				fTastoElab4 = true;																// Per evitare, in caso di pressione in questo istante, una nuova erogazione
			}
			else
			{
				CntFlussRemaining = (PulseCounterSet - Ventolino[sSelParam_A[3].FlussNum - 1]);
				if (TmrTimeout(sErogaz4.ToutErogaz))
				{
					//-- Timeout scaduto --
					SetOutputs(Selez_04, OFF);
					sSelParam_A[3].SelFlags |= BIT_INH_SEL;										// Selezione Disabilitata
					sSelParam_A[3].SEL_Timeout = true;											// Selezione Disabilitata per Timeout
					sErogaz4.InProgress = false;
				}
			}
		}
		else
		{
			//--  Selezione a Tempo --
			if (TmrTimeout(sErogaz4.ToutErogaz))												// Selezione a tempo: il timer e' usato come tempo erogazione
			{
				TimeRemaining = 0;
				sErogaz4.InProgress = false;
				SetOutputs(Selez_04, OFF);
				fTastoElab4 = true;																// Per evitare, in caso di pressione in questo istante, una nuova erogazione
			}
			else
			{
				TimeRemaining = (sErogaz4.ToutErogaz.tmrEnd - gVars.gKernelFreeCounter);
			}
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: DisableRemainingOutputs
	Disattivo tutte le uscite delle selezioni diverse da quella in corso.
		
 	IN :  - numero selezione in corso (1-n)
 	OUT:  -
\*--------------------------------------------------------------------------------------*/
void  DisableRemainingOutputs(uint8_t ErogazInProgress)
{
	switch(ErogazInProgress)
	{
		case Selez_01:
			SetOutputs(Selez_02, OFF);
			SetOutputs(Selez_03, OFF);
			SetOutputs(Selez_04, OFF);
			sErogaz2.InProgress = false;
			sErogaz3.InProgress = false;
			sErogaz4.InProgress = false;
			break;
		
		case Selez_02:
			SetOutputs(Selez_01, OFF);
			SetOutputs(Selez_03, OFF);
			SetOutputs(Selez_04, OFF);
			sErogaz1.InProgress = false;
			sErogaz3.InProgress = false;
			sErogaz4.InProgress = false;
			break;

		case Selez_03:
			SetOutputs(Selez_01, OFF);
			SetOutputs(Selez_02, OFF);
			SetOutputs(Selez_04, OFF);
			sErogaz1.InProgress = false;
			sErogaz2.InProgress = false;
			sErogaz4.InProgress = false;
			break;
	
		case Selez_04:
			SetOutputs(Selez_01, OFF);
			SetOutputs(Selez_02, OFF);
			SetOutputs(Selez_03, OFF);
			sErogaz1.InProgress = false;
			sErogaz2.InProgress = false;
			sErogaz3.InProgress = false;
			break;
	}
	HAL_Delay(WAIT_AFTER_OUT_OFF);																// Attendo disattivazione uscite
}

/*--------------------------------------------------------------------------------------*\
Method: SetOutputs
	Attiva o disattiva la/e uscita/e di una selezione.
	Attiva o disattiva anche l'uscita pulizia filtro al termine della selezione 1.
	
 	IN :  - numero della selezione (1-n) e flag ON-OFF
 	OUT:  - Out1_8 e Out9_16 
\*--------------------------------------------------------------------------------------*/
void  SetOutputs(uint8_t SelezNum, bool Switch)
{
	uint8_t	mask, uscita1, uscita2, OutBit;
	
	if (SelezNum == FILTER_CLEAN)
	{
		uscita1 = sSelParam_A[0].CleanFilterOut;												// Uscita programmata nella configurazione della selezione 1
		uscita2 = 0;
	}
	else
	{
		uscita1 = sSelParam_A[SelezNum-1].OutNum_1;												// Numero I uscita programmato nella configurazione
		uscita2 = sSelParam_A[SelezNum-1].OutNum_2;												// Numero II uscita programmato nella configurazione
	}
	if ((uscita1 > 0) && (uscita1 <= MAX_OUTPUT))
	{
		mask = 1;
		if (uscita1 > 8)
		{
			uscita1 -= 9;
			OutBit = (mask << uscita1);
			if (Switch == ON)
			{
				Out9_16 |= OutBit;																// Uscita ON
			}
			else
			{
				Out9_16 &= (~OutBit);															// Uscita OFF
			}
		}
		else
		{
			OutBit = mask << (uscita1 - 1);
			if (Switch == ON)
			{
				Out1_8 |= OutBit;																// Uscita ON
			}
			else
			{
				Out1_8 &= (~OutBit);															// Uscita OFF
			}
		}
	}

	if ((uscita2 > 0) && (uscita2 <= MAX_OUTPUT))
	{
		mask = 1;
		if (uscita2 > 8)
		{
			uscita2 -= 9;
			OutBit = (mask << uscita2);
			if (Switch == ON)
			{
				Out9_16 |= OutBit;																// Uscita ON
			}
			else
			{
				Out9_16 &= (~OutBit);															// Uscita OFF
			}
		}
		else
		{
			OutBit = mask << (uscita2 - 1);
			if (Switch == ON)
			{
				Out1_8 |= OutBit;																// Uscita ON
			}
			else
			{
				Out1_8 &= (~OutBit);															// Uscita OFF
			}
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: DoorOpenInitState
	Predispongo flag e tempi per l'allarme porta aperta.
		
 	IN :  - 
 	OUT:  -
\*--------------------------------------------------------------------------------------*/
void  DoorOpenInitState(void)
{
	gVMCState.DoorPreviusStatus	= DOOR_STATUS_OPEN;
	gVMCState.DoorAlarmEnabled 	= DOOR_ALARM_ENABLED;
	gVMCState.TimeDelayDoorAlarm.tmrEnd = 0;
	gVMCState.TimeDoorAlarmON.tmrEnd = 0;

}

#if (CESAM == false)
/*--------------------------------------------------------------------------------------*\
Method: SetDoorOpenDelay
	Abilito, per un certo tempo, la possibilita' di apertura porta senza far scattare
	l'allarme.
		
 	IN :  - 
 	OUT:  -
\*--------------------------------------------------------------------------------------*/
void  SetDoorOpenDelay(void)
{
	if (gVMC_ConfVars.SirenaPortaAperta == false) return;
	SIRENA_OFF;
	//TmrStart(gVMCState.TimeDelayDoorAlarm, TimeDelayAllarmeSirena);
	TmrStart(gVMCState.TimeDelayDoorAlarm, TenSec);			// DEBUG
	gVMCState.DoorAlarmEnabled = DOOR_ALARM_DISABLED;
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: Check_Allarme_Porta
	Controlla lo stato del micro porta per la gestione dell'allarme sirena.
	L'allarme e' disabilitato per un certo dopo dopo l'inserimento della carta Mifare
	"Vendite di Test" per permettere al tecnico l'entrata.
	Mantenendo la porta aperta la CPU puo' essere accesa-spenta senza che scatti l'allarme.
	L'allarme e' riarmato quando la porta si chiude: da quel momento, se prima non si 
	inserisce la carta "Vendite di Test" per ritardare l'allarme, l'apertura della porta
	causa l'immediata partenza della sirena, per un periodo temporizzato.
	La funzionalita' dell'allarme sirena e' soggetta a disabilitazione da opzione di
	programmazione.
		
 	IN :  - 
 	OUT:  -
\*--------------------------------------------------------------------------------------*/
#if (CESAM == false)
void  Check_Allarme_Porta(void)
{
	if (gVMC_ConfVars.SirenaPortaAperta == false) return;
	if ((Micro1_8 & DOOR_SWITCH) == 0)
	{
		//--  Door  Closed  --
		if (gVMCState.DoorPreviusStatus == DOOR_STATUS_CLOSED)
		{
			//-- Now Closed, previus Close --
			if (TmrTimeout(gVMCState.TimeDelayDoorAlarm))
			{
				gVMCState.DoorAlarmEnabled = DOOR_ALARM_ENABLED;								// Delay scaduto, riarmo l'allarme
			}
			else
			{
				gVMCState.DoorAlarmEnabled = DOOR_ALARM_DISABLED;								// Delay in corso, allarme disabilitato
			}
		}
		else
		{
			//-- Now Closed, previus Open --
			if (Is_SIRENA_ON == true)															// Porta chiusa da aperta: se in allarme attendo fine time suono
			{
				if (TmrTimeout(gVMCState.TimeDoorAlarmON))
				{																				// Tempo attivazione sirena terminato
					SIRENA_OFF;
				}
			}
			else
			{
				gVMCState.DoorPreviusStatus = DOOR_STATUS_CLOSED;								// Abilito allarme e azzero delay entrata
				gVMCState.DoorAlarmEnabled = DOOR_ALARM_ENABLED;
				gVMCState.TimeDelayDoorAlarm.tmrEnd = 0;
			}
		}
	}
	else
	{
		//--  Door  Open  --
		if (gVMCState.DoorPreviusStatus == DOOR_STATUS_CLOSED)
		{
			gVMCState.DoorPreviusStatus = DOOR_STATUS_OPEN;										// Porta aperta da chiusa: se allarme abilitato attivo Sirena temporizzata
			if (gVMCState.DoorAlarmEnabled == DOOR_ALARM_ENABLED)
			{
				SIRENA_ON;
				TmrStart(gVMCState.TimeDoorAlarmON, TimeAllarmeSirena);
			}
		}
		else
		{
			//-- Now Open, previus Open --
			if (Is_SIRENA_ON == true)
			{
				if (TmrTimeout(gVMCState.TimeDoorAlarmON))
				{																				// Tempo attivazione sirena terminato
					SIRENA_OFF;
				}
			}
		}
	}
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: Check_Timer_Ozono
	La funzione Ozono utilizza due timers, il primo che conta il tempo dall'ultima
	erogazione, il secondo conta il tempo di attivazione ozono.
	L'erogazione ozono viene interrotta se e' richiesta un'erogazione.
	L'erogazione ozono non viene effettuata se il tempo scade in presenza di erogazione.
		
 	IN :  - 
 	OUT:  -
\*--------------------------------------------------------------------------------------*/
#if (CESAM == false)
void  Check_Timer_Ozono(void)
{
	if (gVMC_ConfVars.PresOzono == true)
	{																							// Funzione Ozono abilitata
		if ((IsErogazInProgress() == true) || (gVMCState.InProgrammaz == true))					// Prima di una selezione o in programmazione l'Ozono e' disattivato
		{
			//-- Arrivo qui prima di iniziare una erogazione --
			if (Is_OZONO_ON == true)
			{
				OZONO_OFF;																		// Uscita attiva prima di iniziare una selezione: disattivo uscita Ozono
				TmrStart(gVMCState.TimeWaitOzono, Ozono_TimeWait);								// Ricarico qui il tempo di attesa per la prossima attivazione
				TmrStart(gVMCState.TimeOzono_ON, (Ozono_TimeWait + Ozono_TimeON));				// Ricarico scadenza tempo di attivazione
			}
		}
		else
		{
			if (TmrTimeout(gVMCState.TimeWaitOzono))
			{																					// Tempo attesa trascorso
				if (TmrTimeout(gVMCState.TimeOzono_ON))
				{
					OZONO_OFF;																	// Tempo attivazione scaduto o ancora da caricare: disattivo comunque uscita Ozono
					TmrStart(gVMCState.TimeWaitOzono, Ozono_TimeWait);							// Ricarico il tempo di attesa per la prossima attivazione
					TmrStart(gVMCState.TimeOzono_ON, (Ozono_TimeWait + Ozono_TimeON));			// Ricarico scadenza tempo di attivazione
				}
				else																			// Time attesa scaduto e Time attivazione in corso: uscita attiva
				{
					OZONO_ON;
				}
			}
		}
	}
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: Check_Timer_Flushing
	Trascorso il tempo di attesa dall'ultima erogazione si attivano tutte le EV per
	2 secondi.
		
 	IN :  - 
 	OUT:  -
\*--------------------------------------------------------------------------------------*/
#if (CESAM == false)
void  Check_Timer_Flushing(void)
{
	bool		EV_In_Funzione;
	uint32_t	j;
	
	if (TimeFlushing == 0) return;															// Il Delay autoflushing = 0 disattiva la funzione autoflushing
	if ((IsErogazInProgress() == false) && (gVMCState.InProgrammaz == false))				// In erogazione o in programmazione il flushing e' disattivato
	{
		if (TmrTimeout(gVMCState.TimeAutoFlushing))
		{																					// Delay dopo ultima erogazione trascorso
			EV_In_Funzione = false;															// Flag almeno una EV in funzione
			for (j=0; j < MAXNUMSEL; j++)
			{
				if (sSelParam_A[j].EV_CC_Failure == false)
				{																			// Attivo EV perche' non e' guasta
					if (Start_Erogazione(Selez_01 + j) == false)
					{																		// EV attivata regolarmente
						EV_In_Funzione = true;
					}
				}
			}
			if (EV_In_Funzione == true)			
			{																				// C'e' almeno una EV in funzione
				HAL_Delay(TwoSec);
			}
			for (j=0; j < MAXNUMSEL; j++)
			{
				ON_OFF_ELV(sSelParam_A[j].EVNum, OFF);
			}
			TmrStart(gVMCState.TimeAutoFlushing, TimeFlushing);								// Carico time Autoflushing
		}
	}
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: Check_Timer_Vari_e_Allarmi
	Funzione chiamata dal Main.
	Il micro porta attiva la sirena di allarme se si apre dopo essere stato rilevato
	chiuso.
	L'allarme non si attiva se l'apertura della porta avviene nel tempo di ritardo
	concesso tramite inserimento della carta mifare Vendite di Test.
	La chiusura conseguente ad una apertura arma nuovamente l'allarme in caso di
	successiva apertura senza introduzione della carta di Test Mifare.
		
 	IN :  - 
 	OUT:  -
\*--------------------------------------------------------------------------------------*/
void  Check_Timer_Vari_e_Allarmi(void)
{
	
static		bool		ValoriSelezSuLCD = false;
//			uint8_t		Mask;
//			uint32_t	j;
static		uint32_t	TimeRemainingDec = 0, PreviusTimeRemainingDec = 0;
	
#if (CESAM == false)
	Check_Timer_Ozono();
	Check_Timer_Flushing();
	Check_Allarme_Porta();
	
	if (TmrTimeout(ToutMotore.TestCoolerPeriod))
	{
		TmrStart(ToutMotore.TestCoolerPeriod, TIME_TEST_COOLER_TEMPER);
		if (gVmcStatusIoT.NTC2 >= gOrionConfVars.TemperCooler)									// Utilizzo gVmcStatusIoT.NTC2 che risulta aggiornata solo quando la temper e' cambiata di almeno 0.5 gradi
		{																						// per evitare continui ON-OFF a cavallo di un grado
			COOLER_ON;
		}
		else
		{
			COOLER_OFF;
		}
	}
#else

/*
#if DEB_TIME_FUNZ_1
	HAL_GPIO_WritePin(GPIO_0_GPIO_Port, GPIO_0_Pin, GPIO_PIN_SET);			// Debug  PC13 HIGH  GPIO-0
#endif	

	//-- Controllo eventuali Corto Circuito sulle uscite --
	//-- La lettura di uno zero da un output a uno indica CC --
	Mask = 1;
	for (j=0; j < MAX_OUTPUT; j++)
	{
		if (j < 8)
		{
			if ((Out1_8 & Mask) == 1)
			{
				if ((CC_Out1_8 & Mask) == 0)
				{
					if (IntegrCortoCirc[j] > 0)
					{
						IntegrCortoCirc[j]--;
					}
					else
					{
						//-- Uscita in Corto Circuito --
						Out1_8 = (Out1_8 & (~Mask));											// Disattivo l'uscita in Corto Circuito
						ICC_Alarm = true;
					}
				}
			}
			else
			{
				IntegrCortoCirc[j] = INTEGR_CORTO_CIRC;
			}
		}
		else
		{
			if ((Out9_16 & Mask) == 1)
			{
				if ((CC_Out9_12 & Mask) == 0)
				{
					if (IntegrCortoCirc[j] > 0)
					{
						IntegrCortoCirc[j]--;
					}
					else
					{
						//-- Uscita in Corto Circuito --
						Out1_8 = (Out1_8 & (~Mask));											// Disattivo l'uscita in Corto Circuito
						ICC_Alarm = true;
					}
				}
			}
			else
			{
				IntegrCortoCirc[j] = INTEGR_CORTO_CIRC;
			}
		}
		Mask <<= 1;
		if (Mask == 0)
		{
			Mask = 1;
		}
	}
#if DEB_TIME_FUNZ_1
	HAL_GPIO_WritePin(GPIO_0_GPIO_Port, GPIO_0_Pin, GPIO_PIN_RESET);			// Debug  PC13 LOW  GPIO-0
#endif	
*/
	
	//-- L'attivazione del pulsante Emergency comporta --
	//-- l'azzeramento del credito inserito            --
	if ((Micro1_8 & EMERGENCY) == EMERGENCY)
	{
		if (gOrionKeyVars.Inserted == true)
		{
			gOrionKeyVars.Inserted = false;
			gOrionKeyVars.RFFieldFree = true;
		}
		// Controlla se c'e' del credito cash che deve andare in overpay
		if (gOrionCashVars.creditCash > 0)
		{
			if (gOrionCashVars.givingChange == false)											// Cash in overpay se payout non in corso
			{
				AzzeraCashInOverpay();
			}
		}
	}
	
	//--  Sezione solo per visualizzazione Time/Impulsi ----
	// A volte la differenza tra "PreviousTimeRemaining" e "TimeRemaining" 
	// supera i 100 msec e si traduce in una apparente velocita' superiore
	// del conteggio perche' si perde una unita': ad es si passa da 13 sec
	// a 11 sec. In realta' il conteggio effettivo del tempo e' perfetto.
	// Per ovviare visualizzo i secondi solo quando la differenza tra 
	// "PreviousTimeRemaining" e  "TimeRemaining" e' > un secondo, e solo
	// se la conversione in secondi di "TimeRemaining" ha un valore inferiore
	// di 1 rispetto a quanto visualizzato su display.
	
	if (IsErogazInProgress() == true)
	{
		ValoriSelezSuLCD = true;
		if (TimeRemaining > 0)
		{
			//-- Selezione a Tempo --
			if ((PreviousTimeRemaining - TimeRemaining) > 950)
			{
				TimeRemainingDec = (TimeRemaining/1000 + 1);
				//k2 = (PreviousTimeRemaining/1000 + 1);
				if (TimeRemainingDec != PreviusTimeRemainingDec)
				{
					PreviusTimeRemainingDec = TimeRemainingDec;
					ForceLCD_NewVisual();
				}
			}
		}
		else
		{
			if (TmrTimeout(ToutMotore.TestCoolerPeriod))
			{
				TmrStart(ToutMotore.TestCoolerPeriod, OneSec);								// Timer per aggiornare il display periodicamente
				if (PreviousCntFluss != CntFlussRemaining)
				{
					PreviousCntFluss = CntFlussRemaining;
					ForceLCD_NewVisual();
				}
			}
		}
	}
	else
	{
		if (ValoriSelezSuLCD == true)
		{
			ValoriSelezSuLCD = false;														// Visualizza msg st-by
			ForceLCD_NewVisual();
		}
	}
	//--------------------------------------------------------
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: Is_DA_Vuoto
	Feb 2023: ALP chiede di aggiungere un secondo ingresso per Fine Prodotto e impostare
	quale selezione deve essere inibita quando il relativo prodotto e' esaurito.
	NON si deve piu' inibire la macchina quando in prodotto siesaurisce ma si disabilita
	solo la selezione associata.
	
 	IN :  -
 	OUT:  -  true se prodotto esaurito, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  Is_DA_Vuoto(void)
{
	//return IsProdottoEsaurito();
	return false;
}

/*--------------------------------------------------------------------------------------*\
Method: Is_VMC_Fail
	Ritorna true se ci sono guasti che bloccano il funzionamento della macchina.

	IN:	 - 
	OUT: - True se VMC guasto o Emergency attivo, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  Is_VMC_Fail(void)
{

#if DEB_NO_MICROSW
	return ((sSelParam_A[0].SelFlags & BIT_INH_SEL) & 
			(sSelParam_A[1].SelFlags & BIT_INH_SEL) &
			(sSelParam_A[2].SelFlags & BIT_INH_SEL) &
			(sSelParam_A[3].SelFlags & BIT_INH_SEL) |
			(gOrionConfVars.Remote_INH == true)? true:false);
	
#else
	return ((sSelParam_A[0].SelFlags & BIT_INH_SEL) & 
			(sSelParam_A[1].SelFlags & BIT_INH_SEL) &
			(sSelParam_A[2].SelFlags & BIT_INH_SEL) &
			(sSelParam_A[3].SelFlags & BIT_INH_SEL) |
			(gOrionConfVars.Remote_INH == true)? true:false) |
			(((Micro1_8 & EMERGENCY) == EMERGENCY)? true:false);
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: UpdateStatusErogazioni
	Rileva variazioni nello stato delle erogazioni per update display LCD

	IN:	 - 
	OUT: - 
\*--------------------------------------------------------------------------------------*/
void  UpdateStatusErogazioni(void)
{
	static	uint8_t	 OldStatus[MAXNUMSEL] = {0, 0, 0, 0};
			uint8_t	 NewStatus;
			uint32_t	j;
	
	for (j=0; j < MAXNUMSEL; j++)
	{
		NewStatus = ReadErogazStatus(j);
		if ( NewStatus != OldStatus[j])
		{
			OldStatus[j] = NewStatus;
			ForceLCD_NewVisual();																// Aggiorna display LCD
			return;
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: IsProdottoEsaurito
	Verifica se la selezione richiesta come paramero di ingresso e' associata a un
	prodotto: se si' e se il prodotto e' esaurito si esce con selezione disabilitata.

	IN:	 - NumSel (0 - 3)
	OUT: - true se selezione disabilitata da fine prodotto, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  IsProdottoEsaurito(uint8_t NumSel)
{
	if (NumSel == (gOrionConfVars.SelInhEndProd_1 - 1))
	{
		//-- Selezione associata a Prodotto N. 1 --
		if (gOrionConfVars.SwitchFineProdotto_1_Open == true)
		{
			return ((Micro1_8 & FINE_PRODOTTO_1)? true : false);								// Open (High) quando prodotto 1 esurito
		}
		else
		{
			return (((Micro1_8 & FINE_PRODOTTO_1) == 0)? true : false);							// Closed (Low) quando prodotto 1 esurito
		}
	}
	else if (NumSel == (gOrionConfVars.SelInhEndProd_2 - 1))
	{
		//-- Selezione associata a Prodotto N. 2 --
		if (gOrionConfVars.SwitchFineProdotto_2_Open == true)
		{
			return ((Micro1_8 & FINE_PRODOTTO_2)? true : false);								// Open (High) quando prodotto 2 esurito
		}
		else
		{
			return (((Micro1_8 & FINE_PRODOTTO_2) == 0)? true : false);							// Closed (Low) quando prodotto 2 esurito
		}
	}
	return false;
}

/*--------------------------------------------------------------------------------------*\
Method: IsProd_Esaurito

	IN:	 - NumSel (1 - 2)
	OUT: - true se prodotto esaurito
\*--------------------------------------------------------------------------------------*/
bool  IsProd_Esaurito(uint8_t NumProdotto)
{
	if (NumProdotto == 1)
	{
		if (gOrionConfVars.SwitchFineProdotto_1_Open == true)
		{
			return ((Micro1_8 & FINE_PRODOTTO_1)? true : false);								// Open (High) quando prodotto 1 esurito
		}
		else
		{
			return (((Micro1_8 & FINE_PRODOTTO_1) == 0)? true : false);							// Closed (Low) quando prodotto 1 esurito
		}
	}
	else
	{
		if (gOrionConfVars.SwitchFineProdotto_2_Open == true)
		{
			return ((Micro1_8 & FINE_PRODOTTO_2)? true : false);								// Open (High) quando prodotto 2 esurito
		}
		else
		{
			return (((Micro1_8 & FINE_PRODOTTO_2) == 0)? true : false);							// Closed (Low) quando prodotto 2 esurito
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: IsErogazInProgress
	Ritorna true quando c'e' anche una sola erogazione in corso.
	Funzione usata con l'opzione "Erogazione Singola" per sapere se un'erogazione
	richiesta puo' essere eseguita o c'e' qualche altra erogazione in corso.

	IN:	 - 
	OUT: - true se erogazioni in corso
\*--------------------------------------------------------------------------------------*/
bool  IsErogazInProgress(void)
{
	return (sErogaz1.InProgress || sErogaz2.InProgress || sErogaz3.InProgress || sErogaz4.InProgress);
}

/*--------------------------------------------------------------------------------------*\
Method: IsFilerCleaningInProgress

	IN:	 - 
	OUT: - true durante la fase di pulizia filtro
\*--------------------------------------------------------------------------------------*/
bool  IsFilerCleaningInProgress(void)
{
	return (sErogaz1.FilterCleaning);
}

/*--------------------------------------------------------------------------------------*\
Method: ErogazTimeRemaining
	Ritorna il tempo residuo dell'erogazione.

	IN:	 - Numero erogazione (0-n)
	OUT: - true se numero erogazione fuori range altrimenti false e tempo residuo
\*--------------------------------------------------------------------------------------*/
bool  ErogazTimeRemaining(uint32_t *TimeResiduo, uint8_t NumErogaz)
{
	uint32_t	Val;
	
	if (NumErogaz > (MAXNUMSEL-1)) return true;
	switch(NumErogaz)
	{
		case 0:
			Val = sErogaz1.ToutErogaz.tmrEnd;
			break;
		case 1:
			Val = sErogaz2.ToutErogaz.tmrEnd;
			break;
		case 2:
			Val = sErogaz3.ToutErogaz.tmrEnd;
			break;
		case 3:
			Val = sErogaz4.ToutErogaz.tmrEnd;
			break;
	}
	*TimeResiduo = Val;
	return false;
}


/*--------------------------------------------------------------------------------------*\
Method: IsErogazAvailable
	Un'erogazione richiesta da remoto e' disponibile se non e' gia' in corso e se non
	e' gia' in corso quando macchina senza pulsanti.

	IN:	 - 
	OUT: - true se disponibile, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  IsErogazAvailable(uint8_t NumErogaz)
{
	if (ReadErogazStatus(NumErogaz-1) != 0) return false;										// Erogazione in corso
	if (gOrionConfVars.NO_Pulsanti == true)
	{
		if (IsErogazInProgress() == true) return false;
	}
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method: ReadErogazStatus
	Ritorna lo stato di erogazione (InProgress o st-by).

	IN:	 - numero dell'erogazione da monitorare (0-n)
	OUT: - InProgress (bit0)
\*--------------------------------------------------------------------------------------*/
uint8_t  ReadErogazStatus(uint8_t NumErogaz)
{
	uint8_t		status;
	
	switch(NumErogaz)
	{
		case 0:
			status = (sErogaz1.InProgress << INPROGR_STATUS_POS);
			break;
		
		case 1:
			status = (sErogaz2.InProgress << INPROGR_STATUS_POS);
			break;
		
		case 2:
			status = (sErogaz3.InProgress << INPROGR_STATUS_POS);
			break;

		case 3:
			status = (sErogaz4.InProgress << INPROGR_STATUS_POS);
			break;
	}
	return status;
}

/*--------------------------------------------------------------------------------------*\
Method: GetSelectionInProgress
	Ritorna il numero dell'erogazione in corso.

	IN:	 - 
	OUT: - erogazione in corso (1-n); 0 = nessuna erogazione
\*--------------------------------------------------------------------------------------*/
uint8_t  GetSelectionInProgress(void)
{
	if (sErogaz1.InProgress) return Selez_01;
	if (sErogaz2.InProgress) return Selez_02;
	if (sErogaz3.InProgress) return Selez_03;
	if (sErogaz4.InProgress) return Selez_04;
	return 0;
}

/*--------------------------------------------------------------------------------------*\
Method: Luci
	
 	IN :  - 
 	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Luci(bool Switch)
{
	if (Switch == ON)
	{
		LUCI_ON;
	}
	else if (Switch == OFF)
	{
		LUCI_OFF;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ResetGuasti
	Azzera guasti macchina. 
	
 	IN :  - 
 	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void ResetGuasti(void)
{
	PSHProcessingData();
	gVmcState_0 = 0;
	gVmcState_1 = 0;
	gVmcState_2 = 0;
	PSHCheckPowerDown();
}	


/*--------------------------------------------------------------------------------------*\
Method: IsErogazComplete
	
 	IN :  - numero dell'erogazione da monitorare
 	OUT:  - true quando il flussimetro ha contato gli impulsi dell'erogazione
            oppure e' terminato il tempo di attivazione uscita
\*--------------------------------------------------------------------------------------*/
bool  IsErogazComplete(uint8_t ErogazNum)
{
	if (Ventolino[(sSelParam_A[ErogazNum-1].FlussNum) - 1] >= sSelParam_A[ErogazNum-1].FlowCounterNominale)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: ClrErogazParameters
	
 	IN :  - 
 	OUT:  - 
\*--------------------------------------------------------------------------------------*/
static void  ClrErogazParameters(uint8_t FlussNum)
{
	if (FlussNum > 0) Ventolino[FlussNum - 1] = 0;
	TimeRemaining = 0;
	PreviousTimeRemaining = 0;
	CntFlussRemaining = 0;
	PreviousCntFluss = 0;
}

/*--------------------------------------------------------------------------------------*\
Method: Motori_OFF
	
 	IN :  - 
 	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Motori_OFF(void)
{
	HAL_SuspendTick();
	ALL_OUTS_OFF;
	HAL_ResumeTick();
}

/*-------------------------------------------------------------------------*\
Method: AttesaStopMotore
	Delay Time dopo aver spento una EV.
	
 	IN :  - 
 	OUT:  -  
\*-------------------------------------------------------------------------*/
void  AttesaStopMotore(uint32_t TimeAttesa)
{
#if WaitStopMot
	TmrStart(ToutMotore.WaitStop, TimeAttesa);												// Tempo attesa 
	while (!TmrTimeout(ToutMotore.WaitStop)) {
		WDT_Clear(gVars.devWDT);															// Trigger WDTimer
	}	
#endif
}


//******************************************************************************************************
//*************   F I N E   G E S T I O N E   I N -  O U T                          ********************
//******************************************************************************************************

#if DEB_NO_VMC
/*-------------------------------------------------------------------------*\
Method: SimulSelez
	Simulazione Selezione senza macchina collegata alla I/O.
	
 	IN :  - 
 	OUT:  -  
\*-------------------------------------------------------------------------*/
void  SimulSelez(uint32_t TimeAttesa) {
	
	TmrStart(ToutMotore.WaitStop, TimeAttesa);													// Tempo attesa 
	while (!TmrTimeout(ToutMotore.WaitStop)) {
		WDT_Clear(gVars.devWDT);
		GestioneMS_Exec();
		Gestione_MDB();
	}	
}
#endif	



#if (IOT_PRESENCE == true)
/*-------------------------------------------------------------------------*\
Method:	UpdateVMCStatusIoT
	Verifica se ci sono modifiche ai parametri IoT che determinano un
	invio immediato dello Status ad AWS.
	Funzione chimata continuamente dal Main.
	
 	IN :  - 
 	OUT:  -  
\*-------------------------------------------------------------------------*/
void  UpdateVMCStatusIoT(void)
{
	bool	newStatus = false;
	//uint8_t	DecimalDiff;

	//-- Erogazione  1  --
	if (gVmcStatusIoT.Erogaz_1_Inh != (bool)((sSelParam_A[0].SelFlags & BIT_INH_SEL)? true : false))
	{
		gVmcStatusIoT.Erogaz_1_Inh = (bool)((sSelParam_A[0].SelFlags & BIT_INH_SEL)? true : false);						// Aggiorno flag INH
		newStatus = true;
	}
	if (gVmcStatusIoT.Erogaz_1_Tout != sSelParam_A[0].SEL_Timeout)
	{
		gVmcStatusIoT.Erogaz_1_Tout = sSelParam_A[0].SEL_Timeout;														// Aggiorno flag Tout Fail
		newStatus = true;
	}
	if (gVmcStatusIoT.EV1_CC_Failure != sSelParam_A[0].EV_CC_Failure)
	{
		gVmcStatusIoT.EV1_CC_Failure = sSelParam_A[0].EV_CC_Failure;													// Aggiorno flag EV Fail
		newStatus = true;
	}
	
	//-- Erogazione  2  --
	if (gVmcStatusIoT.Erogaz_2_Inh != (bool)((sSelParam_A[1].SelFlags & BIT_INH_SEL)? true : false))
	{
		gVmcStatusIoT.Erogaz_2_Inh = (bool)((sSelParam_A[1].SelFlags & BIT_INH_SEL)? true : false);						// Aggiorno flag INH
		newStatus = true;
	}
	if (gVmcStatusIoT.Erogaz_2_Tout != sSelParam_A[1].SEL_Timeout)
	{
		gVmcStatusIoT.Erogaz_2_Tout = sSelParam_A[1].SEL_Timeout;														// Aggiorno flag Tout Fail
		newStatus = true;
	}
	if (gVmcStatusIoT.EV2_CC_Failure != sSelParam_A[1].EV_CC_Failure)
	{
		gVmcStatusIoT.EV2_CC_Failure = sSelParam_A[1].EV_CC_Failure;													// Aggiorno flag EV Fail
		newStatus = true;
	}

	//-- Erogazione  3  --
	if (gVmcStatusIoT.Erogaz_3_Inh != (bool)((sSelParam_A[2].SelFlags & BIT_INH_SEL)? true : false))
	{
		gVmcStatusIoT.Erogaz_3_Inh = (bool)((sSelParam_A[2].SelFlags & BIT_INH_SEL)? true : false);						// Aggiorno flag INH
		newStatus = true;
	}
	if (gVmcStatusIoT.Erogaz_3_Tout != sSelParam_A[2].SEL_Timeout)
	{
		gVmcStatusIoT.Erogaz_3_Tout = sSelParam_A[2].SEL_Timeout;														// Aggiorno flag Tout Fail
		newStatus = true;
	}
	if (gVmcStatusIoT.EV3_CC_Failure != sSelParam_A[2].EV_CC_Failure)
	{
		gVmcStatusIoT.EV3_CC_Failure = sSelParam_A[2].EV_CC_Failure;													// Aggiorno flag EV Fail
		newStatus = true;
	}

	//-- Erogazione  4  --
	if (gVmcStatusIoT.Erogaz_4_Inh != (bool)((sSelParam_A[3].SelFlags & BIT_INH_SEL)? true : false))
	{
		gVmcStatusIoT.Erogaz_4_Inh = (bool)((sSelParam_A[3].SelFlags & BIT_INH_SEL)? true : false);						// Aggiorno flag INH
		newStatus = true;
	}
	if (gVmcStatusIoT.Erogaz_4_Tout != sSelParam_A[3].SEL_Timeout)
	{
		gVmcStatusIoT.Erogaz_4_Tout = sSelParam_A[3].SEL_Timeout;														// Aggiorno flag Tout Fail
		newStatus = true;
	}
	if (gVmcStatusIoT.EV4_CC_Failure != sSelParam_A[3].EV_CC_Failure)
	{
		gVmcStatusIoT.EV4_CC_Failure = sSelParam_A[3].EV_CC_Failure;													// Aggiorno flag EV Fail
		newStatus = true;
	}

	/*
	//-- N T C     1 --
	if (gVmcStatusIoT.NTC1 != NTC1_Unit)
	{
		if (gVmcStatusIoT.NTC1 > NTC1_Unit)
		{
			DecimalDiff = ((gVmcStatusIoT.NTC1Decimal+10) - NTC1_Decimal);
		}
		else
		{
			DecimalDiff = ((NTC1_Decimal+10) - gVmcStatusIoT.NTC1Decimal);
		}
		if (DecimalDiff > 5)
		{
			gVmcStatusIoT.NTC1			= NTC1_Unit;
			gVmcStatusIoT.NTC1Decimal	= NTC1_Decimal;
			newStatus = true;
		}
	}
	//-- N T C     2 --
	if (gVmcStatusIoT.NTC2 != NTC2_Unit)
	{
		if (gVmcStatusIoT.NTC2 > NTC2_Unit)
		{
			DecimalDiff = ((gVmcStatusIoT.NTC2Decimal+10) - NTC2_Decimal);
		}
		else
		{
			DecimalDiff = ((NTC2_Decimal+10) - gVmcStatusIoT.NTC2Decimal);
		}
		if (DecimalDiff > 5)
		{
			gVmcStatusIoT.NTC2			= NTC2_Unit;
			gVmcStatusIoT.NTC2Decimal	= NTC2_Decimal;
			newStatus = true;
		}
	}
	
	//-- Pressostato  CO2  N.  1  --
	if (gVmcStatusIoT.PressCO2_1 != (bool)((Micro1_8 & PRESS_CO2_N_1)? true : false))
	{
		gVmcStatusIoT.PressCO2_1 = (bool)((Micro1_8 & PRESS_CO2_N_1)? true : false);
		newStatus = true;
	}

	//-- Pressostato  CO2  N.  2  --
	if (gVmcStatusIoT.PressCO2_2 != (bool)((Micro1_8 & PRESS_CO2_N_2)? true : false))
	{
		gVmcStatusIoT.PressCO2_2 = (bool)((Micro1_8 & PRESS_CO2_N_2)? true : false);
		newStatus = true;
	}

	//-- Alarm Status (Sirena)  --
	if (gVmcStatusIoT.AlarmStatus != (bool)(Is_SIRENA_ON))
	{
		gVmcStatusIoT.AlarmStatus = (bool)(Is_SIRENA_ON);
		newStatus = true;
	}
	*/

	//-- Door Switch Status  --
	if (gVmcStatusIoT.DoorStatus != (bool)((Micro1_8 & DOOR_SWITCH)? true : false))
	{
		gVmcStatusIoT.DoorStatus = (bool)((Micro1_8 & DOOR_SWITCH)? true : false);
		newStatus = true;
	}

	//-- Emergency Button Status  --
	if (gVmcStatusIoT.EmergencyStatus != (bool)((Micro1_8 & EMERGENCY)? true : false))
	{
		gVmcStatusIoT.EmergencyStatus = (bool)((Micro1_8 & EMERGENCY)? true : false);
		newStatus = true;
	}
	
	//-- Fine Prodotto 1 Status  --
	if (gVmcStatusIoT.ProductStatus != (bool)IsProd_Esaurito(1))
	{
		gVmcStatusIoT.ProductStatus = (bool)IsProd_Esaurito(1);
		newStatus = true;
	}

	//-- Fine Prodotto 2 Status  --
	if (gVmcStatusIoT.ProductStatus2 != (bool)IsProd_Esaurito(2))
	{
		gVmcStatusIoT.ProductStatus2 = (bool)IsProd_Esaurito(2);
		newStatus = true;
	}

	if (newStatus == true) gVmcStatusIoT.StatusChang_IN++;
}

/*-------------------------------------------------------------------------*\
Method:	GestioneErogazioneDaRemoto
	Controlla lo stato dell'erogazione richiesta da remoto per determinare
	il messaggio di risposta da inviare alla dashboard.
	
 	IN :  - 
 	OUT:  -  
\*-------------------------------------------------------------------------*/
void  GestioneErogazioneDaRemoto(void)
{
	uint8_t		ErrCode;
	
	if (gVmcStatusIoT.RemoteSelectionReq == true)
	{
		if (ReadErogazStatus(gVmcStatusIoT.RemoteSelezNum - 1) == 0)
		{																																	// Erogazione terminata
			// Ripristino flow-meter counter come da configurazione
			switch(gVmcStatusIoT.RemoteSelezNum)
			{
				case Selez_01:
					ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_1]), (uint8_t *)(&sSelParam_A[0].FlowCounterNominale), 2U);
					break;
			
				case Selez_02:
					ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_2]), (uint8_t *)(&sSelParam_A[1].FlowCounterNominale), 2U);
					break;
			
				case Selez_03:
					ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_3]), (uint8_t *)(&sSelParam_A[2].FlowCounterNominale), 2U);
					break;
			
				case Selez_04:
					ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_4]), (uint8_t *)(&sSelParam_A[3].FlowCounterNominale), 2U);
					break;
			}			
			if (((sSelParam_A[gVmcStatusIoT.RemoteSelezNum-1].SelFlags & BIT_INH_SEL) == true)	|| 
				((sSelParam_A[gVmcStatusIoT.RemoteSelezNum-1].EV_CC_Failure) == true)			||
				((sSelParam_A[gVmcStatusIoT.RemoteSelezNum-1].SEL_Timeout) == true))
			{
				//-- Erogazione Fallita --				
				if ((sSelParam_A[gVmcStatusIoT.RemoteSelezNum-1].EV_CC_Failure) == true)
				{
					ErrCode = REMOTE_EROGAZ_EV_FAIL;
				}
				else
				{
					ErrCode = REMOTE_EROGAZ_TOUT_FLOWMETER;
				}
			}
			else
			{
				//-- Erogazione OK --				
				ErrCode = REMOTE_EROGAZ_OK;
			}
			RemoteErogazAnswer(ErrCode);
			gVmcStatusIoT.RemoteSelezNum = 0;
			gVmcStatusIoT.RemoteSelectionReq = false;
		}
	}
}

/*-------------------------------------------------------------------------*\
Method:	Force_Tx_Status
	Set invio forzato dello Status ad AWS.
	Chiamata dal main in caso di Fail o Remote Inhibit.
	
 	IN :  - 
 	OUT:  -  
\*-------------------------------------------------------------------------*/
void  Force_Tx_Status(void)
{
	gVmcStatusIoT.StatusChang_IN++;
}
#endif


