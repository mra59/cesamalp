/****************************************************************************************
File:
    DDCMP.c

Description:
    Protocollo DDCMP

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#include <string.h>

#include "ORION.H"
#include "DevSerial.h"
#include "DDCMPProtocol.h"
#include "DDCMPHook.h"
#include "DDCMP.h"
#include "DDCMPDTS.h"
#include "OrionTimers.h"
#include "Monitor.h"



/*--------------------------------------------------------------------------------------*\
Global Defines References 
\*--------------------------------------------------------------------------------------*/

bool		MemoLogStartTime, Transfer_EE;
uint8_t		DatoTxDDCMP, BancoLogBuff, DataCntr;
DDCMPVars 	sDDCMPVars;
uint8_t		ErrorCounter;

// Valore da mettere nella struct IrDA USART1 per ottenere il baud rate desiderato
const uint32_t EVA_DTS_BaudRate[] = {0, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern  DDCMPDTSVars 	sDTSVars;
extern	void  			Start_IrDA(void);
extern	void  			IrDA_COMM_TxChar(uint8_t Dato);


/*--------------------------------------------------------------------------------------*\
Method: DDCMPClosePossible
  Test if it is possible to close DDCMP

Parameters:
\*--------------------------------------------------------------------------------------*/
bool DDCMPClosePossible(void) {
  return(sDDCMPVars.baudRate== kDDCMPbr2400);
//se sDDCMPVars.baudRate diverso da kDDCMPbr2400 ho gi� eseguito
//un cambio di bauds rate quindi non posso interrompere la sessione
}

/*----------------------------------------------------------------------------------------------------*\
Method: Init_DDCMP_Protocol
	Inizializza strutture e variabili DDCMP.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------------*/
void  Init_DDCMP_Protocol(void) {

	memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));								// Azzero buffer ddcmpbuff
	DDCMPDTSInit();
	DDCMPDTSOpen(kDDCMPDTSModeSlave, (char*)&gVars.ddcmpbuff[0]);
	DDCMPOpen(kDDCMPModeSlave);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPOpen
  Open and initializes DDCMP protocol

Parameters:
  mode  ->  open as DDCMP master or slave
\*--------------------------------------------------------------------------------------*/
void DDCMPOpen(DDCMPMode mode) {

  memset(&sDDCMPVars, 0, sizeof(sDDCMPVars));    // Init internal state
  sDDCMPVars.mode= (DDCMPMode)(mode&(kDDCMPModeMaster+kDDCMPModeSlave));
  sDDCMPVars.baudRate= kDDCMPbr2400;
  #if kDDCMPLineDelaySupport
  sDDCMPVars.isRemote= (mode&kDDCMPModeRemote)!=0;
  #endif
  DDCMPStateInit();
  DDCMPCommOpen();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPClose
  Close and terminate DDCMP protocol.
  Do nothing if ddcmp already closed
\*--------------------------------------------------------------------------------------*/
void DDCMPClose(DDCMPLineRelease release) {
	if(sDDCMPVars.state!=kDDCMPStateClosed) {
		DDCMPHookCommClose();					// Disabilita la UART
		sDDCMPVars.state= kDDCMPStateClosed;
		if(release == kDDCMPRelease)
			DDCMPHookLineRelease();				// Rilascia l'uso dello switch seriale
	}
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPConnect
  Must be called to establish a new connection.
  Ignored if connection already established
\*--------------------------------------------------------------------------------------*/
void DDCMPConnect(void) {
  if(sDDCMPVars.phase==kDDCMPPhaseFinished) {
    // No connection already in progress
    DDCMPStateInit();
    DDCMPConnectStart();
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDisconnect
  Must be called [in master mode] to terminate the connection.
  [Ignored in slave mode or if no connection]
\*--------------------------------------------------------------------------------------*/
void DDCMPDisconnect(void) {
//  if(sDDCMPVars.mode==kDDCMPModeMaster)
  DDCMPDataAckHandle();
  sDDCMPVars.closeConnReq= true;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPFrameSend
  Send a DDCMP frame with the specified content

Parameters:
  frameData ->  data to send
  frameLen  ->  data len
  selectFlag  ->  value of the select flag
Returns:
  True if send accepted, false if rejected
\*--------------------------------------------------------------------------------------*/
bool DDCMPFrameSend(const void* frameData, uint16_t frameLen, bool selectFlag) {
//  if(sDDCMPVars.tx.buff) return false;    // Tx already in progress
  DDCMPDataAckHandle();
  if(sDDCMPVars.phase==kDDCMPPhaseFinished) return false;   // No connection
  if(sDDCMPVars.txPhase!=kDDCMPTxPhaseDone) return false;   // Tx already in progress
  sDDCMPVars.tx.buff= (uint8_t*)frameData;
  sDDCMPVars.tx.buffLen= frameLen;
  sDDCMPVars.tx.selectFlag= selectFlag;
  sDDCMPVars.txRetryCnt= 0;
  sDDCMPVars.txPhase= kDDCMPTxPhaseSend;
  return true;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPFrameReceive
  Start reception of a new DDCMP frame

Parameters:
  rxBuff    ->  rx buffer where to put incoming data
          if nil, just acknowledge previous data frame received, if not already done
  rxBuffLen ->  rx buffer len
Returns:
  False if rx request rejected (i.e. another request already pending)
\*--------------------------------------------------------------------------------------*/
bool DDCMPFrameReceive(void* rxBuff, uint16_t rxBuffLen) {
  DDCMPDataAckHandle();
  if(rxBuff==nil) return false;
  if(sDDCMPVars.rxReqBuff) return false;    // another request still pending

  // Queue rx request
  sDDCMPVars.rxReqBuffLen= rxBuffLen;
  sDDCMPVars.rxReqBuff= rxBuff;

  // Process it immediately if possible
  DDCMPRxRequestActivate();
  return true;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPTxEmptyInd
  Chiamata dall'IRQ di Trasmissione per inviare un nuovo carattere
\*--------------------------------------------------------------------------------------*/
void DDCMPTxEmptyInd(void) {
  
	switch(sDDCMPVars.state) {
	// Tx frame header
	case kDDCMPStateTxHdr0:
	  DDCMPCRCInit(&sDDCMPVars.tx.crc);
	  sDDCMPVars.tx.count= 0;
	  ++sDDCMPVars.state;
	
	case kDDCMPStateTxHdrN:
	  DatoTxDDCMP = sDDCMPVars.frameHdr[sDDCMPVars.tx.count++];
	  DDCMPCRCUpdate(DatoTxDDCMP, &sDDCMPVars.tx.crc);
	  if(sDDCMPVars.tx.count>=kDDCMPFrameHdrCRC16) {
		++sDDCMPVars.state;
		#if kDDCMPExtendedAddrSupport
		if(sDDCMPVars.extAddr && DDCMPIsStartPkt(sDDCMPVars.frameHdr)) sDDCMPVars.tx.count= 0;
		else ++sDDCMPVars.state;          // Extended addr disabled: tx CRC
		#endif
	  }
	  break;
	
	#if kDDCMPExtendedAddrSupport
	case kDDCMPStateTxHdrExtAddr:
	  DatoTxDDCMP = sDDCMPVars.extAddr[sDDCMPVars.tx.count++];
	  DDCMPCRCUpdate(DatoTxDDCMP, &sDDCMPVars.tx.crc);
	  if(sDDCMPVars.tx.count>=kDDCMPExtendedAddrLen) ++sDDCMPVars.state;
	  break;
	#endif
	
	case kDDCMPStateTxHdrCRC16:
	  DatoTxDDCMP = (char)sDDCMPVars.tx.crc;
	  ++sDDCMPVars.state;
	  break;
	
	case kDDCMPStateTxHdrCRC16H:
	  DatoTxDDCMP = sDDCMPVars.tx.crc>>8;
	  if(sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]==kDDCMPMsgClassControl) {
		sDDCMPVars.state= kDDCMPStateTxDone;    												// Invio l'ultimo byte del Control frame
	  } else {            
		++sDDCMPVars.state;             														// Transmit frame data part
	  }  
	  break;

	// Tx frame data
	case kDDCMPStateTxBody0:
	  DDCMPCRCInit(&sDDCMPVars.tx.crc);
	  sDDCMPVars.tx.count= 0;
	  ++sDDCMPVars.state;
	
	case kDDCMPStateTxBodyN:
	  DatoTxDDCMP = sDDCMPVars.tx.buff[sDDCMPVars.tx.count++];
	  DDCMPCRCUpdate(DatoTxDDCMP, &sDDCMPVars.tx.crc);
	  if(sDDCMPVars.tx.count>=sDDCMPVars.tx.buffLen) ++sDDCMPVars.state;
	  break;
	
	case kDDCMPStateTxBodyCRC16:
	  DatoTxDDCMP = (char)sDDCMPVars.tx.crc;
	  ++sDDCMPVars.state;
	  break;
	
	case kDDCMPStateTxBodyCRC16H:
	  DatoTxDDCMP = sDDCMPVars.tx.crc>>8;
	  sDDCMPVars.state= kDDCMPStateTxDone;
	  break;

	case kDDCMPStateTxDone:
	// Trasmissione completata
	  if(sDDCMPVars.txPhase==kDDCMPTxPhaseSending) {
		//---------------------------------------------------------------DDCMPMsgClassData = 0x81---------------------kDDCMPDataMsgDataBlock = 0x99
		bool isDataMsgDataBlock = sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]==kDDCMPMsgClassData && sDDCMPVars.tx.buff[0]==kDDCMPDataMsgDataBlock;
		uint16_t ackTimeout= (isDataMsgDataBlock? kDDCMPDataMsgRxTO : DDCMPGetTO(kDDCMPAckCTO));
		  // NB: for data blocks Ack has an applicative meaning, so we can't expect it in 40 char time, but have
		  // to use an applicative timeout.
		#if kDDCMPLineDelaySupport
		if(sDDCMPVars.isRemote) ackTimeout+= kDDCMPLineDelay;
		#endif
		TmrStart(sDDCMPVars.ackTmr, ackTimeout);
		sDDCMPVars.txPhase= kDDCMPTxPhaseWaitAck;												// Set Attendere ACK (o NACK)
	  }
		  DDCMPFinishCheck();
		  DDCMPFrameRxStart();
	default:
	  DDCMPHookTxEnd();
	  return;
	}
	// -- Trasmissione dati al carrier ---
	LogTxData_DDCMP(DatoTxDDCMP);																// Save dato in Log File
	Monitor_EchoDDCMP_Slave(DatoTxDDCMP);														// Se in Monitor, echo al PC del chr trasmesso
	IrDA_COMM_TxChar(DatoTxDDCMP);																// Il dato da trasmettere e' in DatoTxDDCMP
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPTxDoneInd
  Chiamata quando la trasmissione e' stata completata
\*--------------------------------------------------------------------------------------*/
void DDCMPTxDoneInd(void) {
	DDCMPTxEmptyInd();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPRxIndData
  Funzione chiamata dall'IRQ RDRF.
  Ricezione seriale
  
Parameters:
  data  ->  byte ricevuto
\*--------------------------------------------------------------------------------------*/
void DDCMPRxIndData(uint8_t data)
{
	uint16_t interByteTimeout;
 
	#if kDDCMPExtendedAddrSupport
	uint8_t extAddrSize= 0;
	#else
	//enum { extAddrSize= 0 };
	uint8_t extAddrSize= 0;
	#endif

  	LogRxData(data, true);																	// True per segnalare dato valido
  	Monitor_EchoDDCMP_Master(data);															// Se in Monitor, echo al PC del chr ricevuto
	switch(sDDCMPVars.state) 
	{

//----------------------------------------------------------------------------------------------------------------------------
  	// Ricezione frame header
  	case kDDCMPStateRxHdr0:
		if (data != kDDCMPMsgClassControl && data != kDDCMPMsgClassData) break;				// Se data non e' ne' 05 ne' 81 esco
		if (data == kDDCMPMsgClassControl) {
			sDDCMPVars.InConnessioneDDCMP = true;											// Ricevuto 05: set flag InConnessione
			// Se attiva la registrazione Dati IrDA inizializzo i parametri
			/*MR19
			if (SystOpt4 & TxIrDALog) {														// Opzione registra Log Audit Irda
				if (!MemoLogStartTime) {													// Prima entrata, memorizzo log start time in EE
					GetStartLog();
					MemoLogStartTime = true;
					BancoLogBuff = 0;														// Predispongo il trasferimento del del log buffer partendo dal banco 0
					Transfer_EE = false;
					DataCntr = 1;
				}
			}
			*/
		}
		DDCMPCRCInit(&sDDCMPVars.rx.crc);
		sDDCMPVars.rx.count= 0;
		++sDDCMPVars.state;																	// Set state as "kDDCMPStateRxHdrN"

  	case kDDCMPStateRxHdrN:
		if (sDDCMPVars.rx.count == kDDCMPFrameHdrLen + extAddrSize) {
			sDDCMPVars.rx.count= 0;															// Ricevuti 8 chr: azzero counter per inserirli dall'inizio del buffer "sDDCMPVars.frameHdr"
		}
		// I primi 3 chr dello START devono essere 05-06-40
		if (sDDCMPVars.phase == kDDCMPPhaseWaitStart && sDDCMPVars.rx.count == 4) {
			  if (DDCMPIsStartPkt(sDDCMPVars.frameHdr)) {									// I primi 4 chr sono 05 06 40 00
				  sDDCMPVars.ShowAudit = true;												// Si puo' visualizzare "Aud" sul display
			  }
		}
		// Inserisco il dato nell'header buffer
		sDDCMPVars.frameHdr[sDDCMPVars.rx.count++]= data;
		#if kDDCMPExtendedAddrSupport
		// If this is a start packet, receive also extended address
		if(sDDCMPVars.extAddr && DDCMPIsStartPkt(sDDCMPVars.frameHdr) && sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd]==kDDCMPAddrExtended)
		  extAddrSize= kDDCMPExtendedAddrLen;
		#endif
		if (sDDCMPVars.rx.count <= kDDCMPFrameHdrCRC16+extAddrSize) {
			DDCMPCRCUpdate(data, &sDDCMPVars.rx.crc);
		}
		if (sDDCMPVars.rx.count == kDDCMPFrameHdrLen+extAddrSize) {
			DDCMPDecodeFrameHeader();														// Ho ricevuto 8 chr: eseguo "DDCMPDecodeFrameHeader"
		}
		break;

//----------------------------------------------------------------------------------------------------------------------------
  	// Ricevo il blocco dati seguente al data message header 0x81....
  	case kDDCMPStateRxBody0:
    
		DDCMPCRCInit(&sDDCMPVars.rx.crc);
		sDDCMPVars.rx.count= 0;
		sDDCMPVars.usingPrivRxBuff= sDDCMPVars.rx.buff==sDDCMPVars.privRxBuff;
	//    if(!sDDCMPVars.rx.buff) {
	//      // No rx buffer specified. Use private one. This allow us to receive Finish
	//      // frame also if no user rx is in progress, and to avoid tests in next phase.
	//      sDDCMPVars.rx.buff= sDDCMPVars.privRxBuff;
	//      sDDCMPVars.rx.buffLen= sizeof(sDDCMPVars.privRxBuff);
	//    }
		++sDDCMPVars.state;

  	case kDDCMPStateRxBodyN:
		if(sDDCMPVars.rx.count<sDDCMPVars.rx.frameLen) DDCMPCRCUpdate(data, &sDDCMPVars.rx.crc);
		if(sDDCMPVars.rx.count<sDDCMPVars.rx.buffLen) sDDCMPVars.rx.buff[sDDCMPVars.rx.count]= data;
		if(++sDDCMPVars.rx.count>=sDDCMPVars.rx.frameLen) ++sDDCMPVars.state;
		break;

  	case kDDCMPStateRxBodyCRC16:
		sDDCMPVars.rxDataCRC= data;
		++sDDCMPVars.state;
		break;

  	case kDDCMPStateRxBodyCRC16H:
		sDDCMPVars.rxDataCRC= sDDCMPVars.rxDataCRC+data*256;
		sDDCMPVars.state= /*kDDCMPStateIdle*/ kDDCMPStateRxHdr0;
		DDCMPDecodeFrameData();										// Ricevuto tutto il blocco dati: analizzo contenuto
		break;

  	case kDDCMPStateRxErr:
	  	if (++ErrorCounter == MAX_ERR_CHAR)
		{
			DDCMPConnectStart();
		}
    	// Throw away all
    	break;

  	default:
    	// Ignore char received
    	return;
  	}

	// Restart inter-byte timer
	interByteTimeout= DDCMPGetTO(kDDCMPInterByteCTO);
	#if kDDCMPLineDelaySupport
		if(sDDCMPVars.isRemote) interByteTimeout += kDDCMPInterByteExtraTO;
	#endif
	TmrStart(sDDCMPVars.interByteTmr, interByteTimeout);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPErrInd
	Errore dalla seriale: se sono in attesa del primo carattere dello START e sto 
	utilizzando l'IrDA, commuto il Baud Rate da 2400 a 9600, altrimenti imposto 
	"sDDCMPVars.state= kDDCMPStateRxErr".

Parameters:
  data  ->  dato ricevuto
  err   ->  tipo di errore (non usato)
\*--------------------------------------------------------------------------------------*/
void DDCMPErrInd(uint8_t data, uint8_t err) {
  
	(void)(err);
  
	if(DDCMPStateReceiving(sDDCMPVars.state)) {
		sDDCMPVars.state= kDDCMPStateRxErr;														// Non sono in attesa dello "05" dello START: set errore
		DDCMPRxIndData(data);																	// Chiamo comunque la "DDCMPRxIndData"
	} else {
		if(DDCMPHookIrDAComm() && (sDDCMPVars.phase == kDDCMPPhaseWaitStart)) {					// Se err seriale da IrDA in attesa dello START commuta da 2400 a 9600 Baud
			if(sDDCMPVars.baudRate == kDDCMPbr2400) {
				sDDCMPVars.baudRate = kDDCMPbr9600;
				sDDCMPVars.phase= kDDCMPPhaseChangeBaudRate;
			}
			TmrStart(sDDCMPVars.pollTmr, 0);
		}
	}
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPDataAckHandle
  Called when a new frame-tx, frame-rx or disconnect request arrives to check 
  if we have to acknowledge last data-packet received.
\*--------------------------------------------------------------------------------------*/
static void DDCMPDataAckHandle(void) {
  if(sDDCMPVars.sendDataAck) {
    sDDCMPVars.sendDataAck= false;
    DDCMPSendAck();
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPCommOpen
  Open DDCMP communication line
\*--------------------------------------------------------------------------------------*/
static void DDCMPCommOpen(void)
{
	DDCMPSerOptInit(sDDCMPVars.baudRate);
	DDCMPHookBaudRateChange(sDDCMPVars.baudRate);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPConnectStart
  Start DDCMP connection attempts
\*--------------------------------------------------------------------------------------*/
static void DDCMPConnectStart(void) {
  if(sDDCMPVars.mode==kDDCMPModeMaster) {
    sDDCMPVars.phase= kDDCMPPhaseStartConn;
    sDDCMPVars.startAttemptsCount= 0;
  }
  else {
    TmrStart(sDDCMPVars.pollTmr, 10000);
    sDDCMPVars.phase= kDDCMPPhaseWaitStart;
    DDCMPFrameRxStart();
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPStateInit
  Init DDCMP state variables
\*--------------------------------------------------------------------------------------*/
static void DDCMPStateInit(void) {
  sDDCMPVars.state= kDDCMPStateIdle;      // No tx/rx activity
  sDDCMPVars.phase= kDDCMPPhaseFinished;    // No connection
  sDDCMPVars.txPhase= kDDCMPTxPhaseDone;    // No frame tx in progress
  sDDCMPVars.rxReqBuff= nil;          // No frame rx request pending
  sDDCMPVars.rxData= nil;           // No frame has been received
  sDDCMPVars.rx.buff= sDDCMPVars.privRxBuff;        // No frame rx in progress
  sDDCMPVars.rx.buffLen= sizeof(sDDCMPVars.privRxBuff); // Private buffer len
//  sDDCMPVars.tx.buff= nil;

  sDDCMPVars.tx.frameNum= 1;
  sDDCMPVars.rx.frameNum= 1;
  sDDCMPVars.closeConnReq= false;
  #if kDDCMPExtendedAddrSupport
  sDDCMPVars.addr= sDDCMPVars.extAddr? kDDCMPAddrExtended:kDDCMPAddrAny;
  #endif
  #if kDDCMPStatisticsSupport
  MemClear(&sDDCMPVars.stats, sizeof(sDDCMPVars.stats));  // Clear connection statistics
  #endif
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPSerOptInit
  Inizializza la struttura DevSerOpt con i parametri DDCMP e il turn-around timeout.

Parameters:
  IN :    br->  baud rate richiesto
  OUT:   
\*--------------------------------------------------------------------------------------*/
static void DDCMPSerOptInit(DDCMPBaudRate br)
{
#if kDDCMPTurnRoundSupport
	#if kDDCMPBaudNegotiationSupport
  	  static const uint8_t sDDCMPTurnRounTime[kDDCMPbr_Count]= {
  			  40,   // 2400
  			  80,   // 1200
  			  40,   // 2400
  			  20,   // 4800
  			  10,   // 9600
  			  10,   // 19200  
  			  10,   // 38400
  			  8,    // 57600  
  			  8     // 115200
  	  };
  	  sDDCMPVars.turnRoundTimeout= sDDCMPTurnRounTime[br];
	#else
  	  sDDCMPVars.turnRoundTimeout= 40;  // 2400
	#endif
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPBaudRateMax
  Return maximum baud-rate supported by DDCMP communication port
  Convert DevSerial baud-rate value in DDCMP baud-rate value
\*--------------------------------------------------------------------------------------*/
static DDCMPBaudRate DDCMPBaudRateMax(void)
{
#if kDDCMPBaudNegotiationSupport
	// Enable support for Baud-Rate Negotiation (if false communication is performed at 2400)
	#if kDevSerBitRate115200 != 11
	  	#error kDevSerBitRate is changed. Adjust DDCMPBaudRateMax() 
	#endif
	uint8_t brMax= DDCMPHookBaudRateMaxGet();
	if(brMax<kDevSerBitRate2400) brMax= kDevSerBitRate2400;
	if(brMax>kDevSerBitRate115200) brMax= kDevSerBitRate115200;
	return (DDCMPBaudRate)(brMax-kDevSerBitRate2400+kDDCMPbr2400);
#else
	return kDDCMPbr2400;
#endif
}

/*-----------------------------------------------------------------------------------------------*\
Method: DDCMPDecodeFrameHeader
  Arrivo qui quando ricevo START, o ACK, o NACK, o Header File 81 xx xx: elaboro header richiesto
\*-----------------------------------------------------------------------------------------------*/
static void DDCMPDecodeFrameHeader(void) {

	uint16_t 	crc;
	bool 	seemsAStartFrame = false;
  
#if kDDCMPExtendedAddrSupport
	uint8_t extAddrSize = 0;
#else
	enum { extAddrSize = 0 };
#endif

	DDCMPTurnRoundTmrStart();

#if kDDCMPStatisticsSupport
	++sDDCMPVars.stats.rxPktCnt;
#endif
#if kDDCMPExtendedAddrSupport
	if(sDDCMPVars.extAddr && DDCMPIsStartPkt(sDDCMPVars.frameHdr) && sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd]==kDDCMPAddrExtended)
		extAddrSize= kDDCMPExtendedAddrLen;
#endif

  	  // Verify header CRC
  	  crc = (uint16_t)(((sDDCMPVars.frameHdr[kDDCMPFrameHdrCRC16+(uint8_t)extAddrSize])) + ((sDDCMPVars.frameHdr[kDDCMPFrameHdrCRC16H+(uint8_t)extAddrSize]*256)));
  	  if (DDCMPIsStartPkt(sDDCMPVars.frameHdr)) {												// I primi 4 chr sono 05-06-40-00
  		  seemsAStartFrame = true;																// E' uno START command
  	  }
  	  if (crc != sDDCMPVars.rx.crc && !seemsAStartFrame) {
  		  if (sDDCMPVars.phase == kDDCMPPhaseWaitStart)	{
  			  sDDCMPVars.state= kDDCMPStateRxErr;												// Sono in attesa dello START, ignoro il msg ricevuto e non rispondo 
  		  } else {
  			  if(sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]==kDDCMPMsgClassData) {				// Primo chr = 0x81 ?
  				  DDCMPSendNack(kDDCMPNackErrHdrCRC);
  			  }
  		  }
  		  if (sDDCMPVars.phase == kDDCMPPhaseFinishingReq) {
  			  goto goto_CrcErrSkipReturn;
  		  }
  		  return;
  	  }

goto_CrcErrSkipReturn:
	if (sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd] != kDDCMPAddrAny) {								// Controlla se "sadd" diverso da reserved
#if kDDCMPExtendedAddrSupport
		// Accept only messages with address kDDCMPAddrExtended (if extended address enabled)
		// Start messages are accepted only if extended address match
		if(!sDDCMPVars.extAddr || sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd]!=kDDCMPAddrExtended || 
				DDCMPIsStartPkt(sDDCMPVars.frameHdr) && memcmp(&sDDCMPVars.frameHdr[kDDCMPFrameHdrExtAddr], sDDCMPVars.extAddr, sizeof(DDCMPExtendedAddr))!=0)
#endif
		if (sDDCMPVars.phase == kDDCMPPhaseFinishingReq) {
			goto goto_AddrAnyErrSkipReturn;
		}
		// Messaggio non indirizzato a me: lo ignoro
		// NB: se e' l'header di un data message lo leggo e lo ignoro per rimanere sincronizzato
		sDDCMPVars.state= sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]==kDDCMPMsgClassControl? kDDCMPStateRxHdr0:kDDCMPStateRxErr;
		return;
	}

goto_AddrAnyErrSkipReturn:

	// Trigger connection-alive timer (? forse � applicativo)
	Tmr32Start(sDDCMPVars.activityTmr, kDDCMPConnectionTimeOut);

	// Check message class
  	switch(sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]) {
  		case kDDCMPMsgClassControl:
  			DDCMPDecodeCtrlHeader();															// Control Message (0x05): START, o STACK, o ACK o NACK
  			break;
  		case kDDCMPMsgClassData:
  			DDCMPDecodeDataHeader();															// Data Message Header: messaggi che iniziano con 0x81
  			break;
  		default:
  			DDCMPSendNack(kDDCMPNackErrHdrFmt);
  			return;
  	}
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDecodeCtrlHeader
  I Control Message sono: START, STACK, ACK, NACK
\*--------------------------------------------------------------------------------------*/
static void DDCMPDecodeCtrlHeader(void) {
  // Restart receiver
  sDDCMPVars.state= kDDCMPStateRxHdr0;

  // Controllo tipo di messaggio tramite il secondo byte (kDDCMPFrameHdrType_Len):
  // se 06 e' START - se 01 e' ACK - se 02 e' NACK - se 07 e' STACK
  switch(sDDCMPVars.frameHdr[kDDCMPFrameHdrType_Len]) {

  
//****************************************************************
	// Ricevuto START
  case kDDCMPCtrlStart:
    if (sDDCMPVars.phase == kDDCMPPhaseWaitStart) {
      DDCMPBaudRate masterBaudRate = (DDCMPBaudRate)sDDCMPVars.frameHdr[kDDCMPFrameHdrMBD_XX];
      #if kDDCMPExtendedAddrSupport
      // Use address kDDCMPAddrExtended or kDDCMPAddrAny depending on master request
      sDDCMPVars.addr= sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd];
      #endif
      DDCMPSendStack(masterBaudRate);
      sDDCMPVars.phase = kDDCMPPhaseConnJustEstablished;
      #if kDDCMPBaudNegotiationSupport
      DDCMPBaudRateRequest(masterBaudRate);
      #endif
    }
    else if(sDDCMPVars.phase==kDDCMPPhaseFinishingDone) {
    	// Possiamo terminare regolarmente lo stato di attesa che il Master abbia
    	// ricevuto l'ACK di chiusura, dato che ci sta inviando un nuovo START.
    	TmrStart(sDDCMPVars.pollTmr, 0);
    	} else {
    	// Qui arrivo se qualcosa e' andato storto: abort connessione.
    	sDDCMPVars.phase= kDDCMPPhaseAbort;
    	}
    break;

//****************************************************************
	// Ricevuto STACK
	case kDDCMPCtrlStack:
		if(sDDCMPVars.phase==kDDCMPPhaseWaitStack) {
			sDDCMPVars.phase= kDDCMPPhaseConnJustEstablished;
      	  #if kDDCMPBaudNegotiationSupport
			DDCMPBaudRateRequest((DDCMPBaudRate)(sDDCMPVars.frameHdr[kDDCMPFrameHdrSBD_RR]));
      	  #endif
		}
		break;

//****************************************************************
	// Ricevuto ACK
  case kDDCMPCtrlAck:
	DDCMPHookCheckFineTxAudit();
    if(sDDCMPVars.phase==kDDCMPPhaseFinishingReq) {
      // This is the connection-close-confirmation. Terminate connection.
      DDCMPFinishDone();
    }
    else {
      DDCMPConfirmTxFrameNum(sDDCMPVars.frameHdr[kDDCMPFrameHdrSBD_RR]);
    }
    break;

//****************************************************************
  	// Ricevuto NACK
case kDDCMPCtrlNack:

    #if kDDCMPStatisticsSupport
    ++sDDCMPVars.stats.rxNackCnt;
    #endif
    DDCMPConfirmTxFrameNum(sDDCMPVars.frameHdr[kDDCMPFrameHdrSBD_RR]);
    // Se il motivo del NACK e' 'NoBuff' faccio scattare il timeout per ritrasmettere subito
    if((sDDCMPVars.frameHdr[kDDCMPFrameHdrFlags]&kDDCMPNackErrMask)!=kDDCMPNackErrNoBuff)
      TmrStart(sDDCMPVars.ackTmr, 0);
    break;

//****************************************************************
  default:
    DDCMPSendNack(kDDCMPNackErrHdrFmt);
    break;
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDecodeDataHeader
  Ricevuto un "data message header" (0x81....)
\*--------------------------------------------------------------------------------------*/
static void DDCMPDecodeDataHeader(void) {
  
	uint16_t frameLen = sDDCMPVars.frameHdr[kDDCMPFrameHdrType_Len] + (sDDCMPVars.frameHdr[kDDCMPFrameHdrFlags]&kDDCMPDataLenMask)*256;
	
	sDDCMPVars.rx.frameLen = frameLen;
	sDDCMPVars.state = kDDCMPStateRxBody0;														// Inizio ricezione blocco dati che segue l'header 0x81
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDecodeFrameData
  Funzione chiamata dopo la ricezione del CRC di un blocco dati seguito all'header 0x81:
  si controlla la correttezza del blocco dati e si determina in cosa consiste.
\*--------------------------------------------------------------------------------------*/
static void DDCMPDecodeFrameData(void) {
	DDCMPTurnRoundTmrStart();

	// Controllo se il CRC e' corretto
	if (sDDCMPVars.rxDataCRC != sDDCMPVars.rx.crc) {
		DDCMPSendNack(kDDCMPNackErrDataCRC);
		return;
	}

	// Controllo se i contatori di blocchi Rx e Tx sono corretti
	if (sDDCMPVars.frameHdr[kDDCMPFrameHdrMBD_XX] != sDDCMPVars.rx.frameNum) {
		uint8_t prevFrameNum = sDDCMPVars.rx.frameNum-1;
		if(sDDCMPVars.frameHdr[kDDCMPFrameHdrMBD_XX]==prevFrameNum) {
			/*if(sDDCMPVars.rx.buff[0]!=kDDCMPDataMsgDataBlock || sDDCMPVars.sendDataAck==false)*/ {  // For data block send ack only if it was already sent
				--sDDCMPVars.rx.frameNum;     													// Restore counter which has been updated by send-ack 
				DDCMPSendAck();           														// Frame already received. Just ack it.
			}
		} else {
			DDCMPSendNack(kDDCMPNackErrHdrFmt);   // Bad sequence number!
		}
		return;
	}
  
	DDCMPConfirmTxFrameNum(sDDCMPVars.frameHdr[kDDCMPFrameHdrSBD_RR]);							// Potrebbe essere un ACK o NACK: comparo counter txframe con counter rx frame

	// Controllo se comando FINISH (77 FF)
	if (sDDCMPVars.rx.frameLen == 2 && sDDCMPVars.rx.buffLen >=2 && sDDCMPVars.rx.buff[0] == kDDCMPDataMsgCmd && sDDCMPVars.rx.buff[1] == kDDCMPCmdFinish) {
		// Ricevuto FINISH (77 FF). TERMINO LA CONNESSIONE
		DDCMPHookRxFinish();																// Controllo se predisporre eventuale Clear Audit LR
		DDCMPSendAck();
		if (sDDCMPVars.phase < kDDCMPPhaseFinishing) sDDCMPVars.phase = kDDCMPPhaseFinishing;
		return;
	}

  // Verify if we have received the complete packet in a real rx buffer
  if(sDDCMPVars.usingPrivRxBuff) {
    DDCMPSendNack(kDDCMPNackErrNoBuff);
    return;
  }

  // Verify buffer was large enough for packet
  if(sDDCMPVars.rx.frameLen>sDDCMPVars.rx.buffLen) {
    DDCMPSendNack(kDDCMPNackErrMsgTooLong);
    return;
  }

  // Messaggio ricevuto correttamente
  sDDCMPVars.rxData= sDDCMPVars.rx.buff;
  sDDCMPVars.rxDataLen= sDDCMPVars.rx.frameLen;
  sDDCMPVars.rxSelectFlag= (sDDCMPVars.frameHdr[kDDCMPFrameHdrFlags]&kDDCMPSelectFlagMask)!=0;

  // Invio ACK
  if(sDDCMPVars.rxData[0]!=kDDCMPDataMsgDataBlock) DDCMPSendAck();
    // Note: for data messages ACK has an applicative meaning, i.e. it seems to indicate
    // that the data packet has been received AND processed, so we cannot acknowledge it here,
    // but have to wait until user code has processed it. Of course in this case the 40 char
    // response time requested by the specs is not guaranteed.
  
  // Switch to private internal buffer until user provides another one.
  sDDCMPVars.rx.buffLen= sizeof(sDDCMPVars.privRxBuff);
  sDDCMPVars.rx.buff= sDDCMPVars.privRxBuff;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPConfirmTxFrameNum
  Controllo contatori se sono in attesa di ACK o NACK.
  Se il mio tx frames coincide con l'rx frame dell'altro lato, incremento tx frames.

Parameters:
  frameNum  ->  contatore dei frame ricevuti dall'altro lato
\*--------------------------------------------------------------------------------------*/
static void DDCMPConfirmTxFrameNum(uint8_t frameNum) {
	
	if(sDDCMPVars.txPhase==kDDCMPTxPhaseWaitAck) {
		if(sDDCMPVars.tx.frameNum == frameNum) {
			++sDDCMPVars.tx.frameNum;
			sDDCMPVars.txPhase= kDDCMPTxPhaseAckd;		// Stato successivo "kDDCMPTxPhaseAckd"
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPTask
  DDCMP control task chiamata dal MAIN.
  Gestisce connessioni in master o slave mode, ricetrasmissione frames e retry.
\*--------------------------------------------------------------------------------------*/
void DDCMPTask(void) {

	DDCMPHandleTurnRoundTmr();
	switch(sDDCMPVars.phase) {

//****************************************************************
	case kDDCMPPhaseStartConn:
//    // Wait a few ms before sending Start, so that if we have just changed
//    // baud-rate, also the slave has time to do this.
//    TmrStart(sDDCMPVars.pollTmr, 50);
//    sDDCMPVars.phase= kDDCMPPhaseWaitStack;
//    break;


//****************************************************************
	case kDDCMPPhaseSendStart:
		if(++sDDCMPVars.startAttemptsCount<=kDDCMPStartMaxCount) {
			uint16_t startTimeout;
			sDDCMPVars.phase= kDDCMPPhaseWaitStack;
			DDCMPSendStart();
			startTimeout= kDDCMPStartTO;
      	  	#if kDDCMPLineDelaySupport
			if(sDDCMPVars.isRemote) startTimeout+= kDDCMPLineDelay;
      	    #endif
			TmrStart(sDDCMPVars.pollTmr, startTimeout);
		}
		else {
			// Connection attempts failed! Stop retries and alert user.
			sDDCMPVars.phase= kDDCMPPhaseAbort;
		}
		break;

//****************************************************************
		
	case kDDCMPPhaseWaitStack:
		if(TmrTimeout(sDDCMPVars.pollTmr)) {
			DDCMPHookSendStart();
			sDDCMPVars.phase= kDDCMPPhaseSendStart;
		}
		break;

#if kDDCMPBaudNegotiationSupport
//****************************************************************
	case kDDCMPPhaseChangeBaudRate:
		if(!DDCMPStateSending(sDDCMPVars.state) && TmrTimeout(sDDCMPVars.pollTmr)) {
			//DDCMPHookCommClose();															// La seriale deve essere abilitata per poi poter cambiare il baud rate
			DDCMPCommOpen();
			DDCMPConnectStart();
		}
		break;
#endif

//****************************************************************
	case kDDCMPPhaseConnJustEstablished:
		// Signal to user that connection is established
		sDDCMPVars.phase= kDDCMPPhaseConnEstablished;
		DDCMPHookConnEstablishedInd();
		break;

//****************************************************************
	case kDDCMPPhaseConnEstablished:
		// Controlla se c'e' una richiesta di chiusura connessione
		if(sDDCMPVars.closeConnReq && DDCMPSendFinish()) {
			sDDCMPVars.phase= kDDCMPPhaseFinishingReq;
			sDDCMPVars.closeConnReq= false;
		}
		// Gestione trasmissione e ricezione frames
		DDCMPHandleFrameTx();
		DDCMPHandleFrameRx();
		break;

//****************************************************************
// In attesa di ricevere uno START
	case kDDCMPPhaseWaitStart:
		if (sDDCMPVars.state == kDDCMPStateRxHdr0 && sDDCMPVars.baudRate == kDDCMPbr2400) {
			sDDCMPVars.InConnessioneDDCMP = false;
			sDDCMPVars.ShowAudit = false;
		}
    
#if kDDCMPBaudNegotiationSupport
		if(sDDCMPVars.baudRate!=kDDCMPbr2400 && TmrTimeout(sDDCMPVars.pollTmr)) {
			sDDCMPVars.baudRate= kDDCMPbr2400;														// Scaduto timeout dopo il cambio velocita': ritorno a 2400 Baud
			sDDCMPVars.phase= kDDCMPPhaseChangeBaudRate;
		}
#endif
		if (DDCMPHandleFrameRx()) {																	// Nella "DDCMPHandleFrameRx" c'e' il controllo dell'Interbyte timeout
			// Non aggiungo nulla perche' dovrebbe gia' funzionare il log							// Timeout scaduto: non ho ricevuto correttamente lo START
		}
		break;


//****************************************************************
	case kDDCMPPhaseFinishingReq:
		// We should handle retransmission of finish frame as a normal frame
		DDCMPHandleFrameTx();
		if(DDCMPStateReceiving(sDDCMPVars.state)) {
			if(TmrTimeout(sDDCMPVars.interByteTmr)) {
				if((sDDCMPVars.rx.count>=3) &&
					 (sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]==kDDCMPMsgClassControl) &&
					 (sDDCMPVars.frameHdr[kDDCMPFrameHdrType_Len]==kDDCMPCtrlAck) &&
					 (sDDCMPVars.frameHdr[kDDCMPFrameHdrFlags]==kDDCMPSyncFlagMask)) {
				//if ((sDDCMPVars.rx.count>=3) && (sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]==kDDCMPMsgClassControl)
				//   && (sDDCMPVars.frameHdr[kDDCMPFrameHdrType_Len]==kDDCMPCtrlAck) && (sDDCMPVars.frameHdr[kDDCMPFrameHdrFlags]==kDDCMPSyncFlagMask)) {
						DDCMPDecodeCtrlHeader();
						Tmr32Start(sDDCMPVars.activityTmr, kDDCMPConnectionTimeOut);
				}
			}
		}
		DDCMPHandleFrameRx();
		break;

//****************************************************************
	case kDDCMPPhaseFinishing:
		// Attendo...
		break;

//****************************************************************

    case kDDCMPPhaseFinishingDone:
    	if( sDDCMPVars.txPhase == kDDCMPTxPhaseAckd ) {
    		DDCMPHandleFrameTx();		 														// Rx Finish ma non si � ancora comunicato all'applicativo la fine tx frame precedente
    	}
        if(!TmrTimeout(sDDCMPVars.pollTmr)) break;
        sDDCMPVars.phase= kDDCMPPhaseFinished;													// Connessione chiusa con successo
        DDCMPHookConnFinishedInd();
        break;

//****************************************************************
    case kDDCMPPhaseFinished:

    	if (SystOpt4 & TxIrDALog) {																// Opzione Tx Log Audit Irda al PC
    		//MR19 TranferLogToEE(true, true);															// Trasferisce tutto il rimanente log buffer RAM in  EE
    	}
    	DDCMPDTSInit();
    	DDCMPDTSOpen(kDDCMPDTSModeSlave, (char*)&gVars.ddcmpbuff[0]);
    	Start_IrDA();																			// Riattivo modalita' IrDA
//    	sDDCMPVars.phase =kDDCMPPhaseWaitStart;     // Nothing to do...
    	break;

//****************************************************************
    case kDDCMPPhaseAbort:
    	sDDCMPVars.phase= kDDCMPPhaseFinished;
    	DDCMPHookConnFailedInd();
    	break;
	}		// Fine Switchcase(sDDCMPVars.phase)

// ===================================================================================================================

	// Abortisco connessione se non c'e' attivita' 
	if(sDDCMPVars.phase>=kDDCMPPhaseConnEstablished && sDDCMPVars.phase<kDDCMPPhaseFinished && kDDCMPConnectionTimeOut && TmrTimeout(sDDCMPVars.activityTmr)) {
		sDDCMPVars.phase= kDDCMPPhaseAbort;
	}
	DDCMPDTSTask();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHandleTurnRoundTmr
	Gestione del turn-around time.
  
\*--------------------------------------------------------------------------------------*/
static void DDCMPHandleTurnRoundTmr(void) {
	
#if kDDCMPTurnRoundSupport
	if (sDDCMPVars.waitTurnRound && TmrTimeout(sDDCMPVars.turnRoundTmr)) {
		sDDCMPVars.waitTurnRound = false;
	}
	// Start tx dopo il turn-round time
	if (sDDCMPVars.state == kDDCMPStateTxReady && !sDDCMPVars.waitTurnRound) {
		sDDCMPVars.state = kDDCMPStateTxHdr0;
		//DDCMPHookTxBegin();																	// Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
		DDCMPTxEmptyInd();																		// Aggiunta per far partire la trasmissione dopo il turn around time
	}
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHandleFrameTx
  Gestione trasmissione frame
\*--------------------------------------------------------------------------------------*/
static void DDCMPHandleFrameTx(void) {
	
	switch(sDDCMPVars.txPhase) {
	case kDDCMPTxPhaseSend:
		// Nuovo Frame da trasmettere
		if (sDDCMPVars.state != kDDCMPStateRxHdr0) break;										// Se stato diverso da "kDDCMPStateRxHdr0" non trasmetto nulla perche' occupato
		DDCMPSendDataFrame();
		++sDDCMPVars.txPhase;																	// sDDCMPVars.txPhase = kDDCMPTxPhaseSending (Sending in progress)
		break;

//*************************************************************
// Sto inviando il blocco dati: rimango in questo stato 
// fino alla fine della trasmissione: lo stato successivo 
// sara' "kDDCMPTxPhaseWaitAck" settato nella "DDCMPTxEmptyInd"

	case kDDCMPTxPhaseSending:
		break;

//***************************************************
// Attendo ACK dopo aver effettuato una trasmissione:
// Non faccio nulla perche' alla ricezione dell'ACK
// 
	case kDDCMPTxPhaseWaitAck:
		if(TmrTimeout(sDDCMPVars.ackTmr)) {
			// Timeout attesa ACK scaduto: retry trasmissione o abort connessione
#if kDDCMPStatisticsSupport
			++sDDCMPVars.stats.rxTimeoutCnt;
#endif
			if(++sDDCMPVars.txRetryCnt<kDDCMPRetryMaxCount) {
				sDDCMPVars.txPhase= kDDCMPTxPhaseSend;
			} else {
				sDDCMPVars.phase= kDDCMPPhaseAbort;
			}
		}
		break;

//*******************************************
	case kDDCMPTxPhaseAckd:
		// Frame ricevuto dall'altro lato.
		sDDCMPVars.txPhase= kDDCMPTxPhaseDone;
		DDCMPHookFrameSentInd();
		break;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPHandleFrameRx
	Modificata il 24.09.2013 per uscire con True se e' scaduto il timeout: serve per
	poter monitorare se la ricezione dello START avviene correttamente.
  
  Handle frame reception  
\*--------------------------------------------------------------------------------------*/
static bool DDCMPHandleFrameRx(void) {
  uint8_t* rcvdData = sDDCMPVars.rxData;
  
	if(rcvdData && sDDCMPVars.phase != kDDCMPPhaseWaitStart) {									// Ricevuto un frame
		bool isDataMsg = rcvdData[0]==kDDCMPDataMsgDataBlock;
		if (isDataMsg) sDDCMPVars.sendDataAck = true;
		sDDCMPVars.rxData = 0;																	// Azzerare .rxData prima di chiamare la hook per evitare la riattivazione della
		DDCMPHookFrameRcvdInd(rcvdData, sDDCMPVars.rxDataLen, sDDCMPVars.rxSelectFlag);			// ricezione con possibilita' di ricevere un nuovo frame
	}
	if(DDCMPStateReceiving(sDDCMPVars.state)) {													// Se sono in attesa del primo chr NON controllo timeout
		if(TmrTimeout(sDDCMPVars.interByteTmr)) {												// Timeout ricezione scaduto: restart ricezione nuovo frame
			DDCMPFrameRxStart();
			return true;
		} else {
			return false;
		}
	} else {
		DDCMPRxRequestActivate();
		return false;
	}
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPRxRequestActivate
  Activate pending rx request, if any.
  Do nothing if we are not in the correct state or no rxRequest pending or another
  rx request already in progress
\*--------------------------------------------------------------------------------------*/
static void DDCMPRxRequestActivate(void) {
  if(sDDCMPVars.phase!=kDDCMPPhaseConnEstablished) return;
  if(sDDCMPVars.rxReqBuff && sDDCMPVars.rx.buff==sDDCMPVars.privRxBuff) {
    if(DDCMPStateReceiving(sDDCMPVars.state)) return;
      // Do not change buffer if rx operation in progress. It could happen that
      // the rx begins just after the above test. In this case if we receive the entire header
      // in a single burst and then change the buffer while we are receiving the body,
      // then the 'usingPrivRxBuff' variable is used to invalidate the packed received
      // and send a NAck

    // Process next rx request.
    sDDCMPVars.rx.buffLen= 0;
    sDDCMPVars.rx.buff= sDDCMPVars.rxReqBuff;
    sDDCMPVars.rx.buffLen= sDDCMPVars.rxReqBuffLen;
    sDDCMPVars.rxReqBuff= nil;
  }
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPBaudRateRequest
  Require DDCMP to run at the specified baud rate.
  Compute the minimum between requested baud-rate and supported baud-rate.
  If the result is different from currently used baud-rate, request a baud-rate change
  and restart DDCMP connection.

Parameters:
  reqBaudRate ->  requested baud rate
\*--------------------------------------------------------------------------------------*/
#if kDDCMPBaudNegotiationSupport
static void DDCMPBaudRateRequest(DDCMPBaudRate reqBaudRate)
{
  DDCMPBaudRate newBaudRate, myMaxBaudRate= DDCMPBaudRateMax();

  if(reqBaudRate==kDDCMPbrUnchanged) return;

  newBaudRate= myMaxBaudRate<reqBaudRate ? myMaxBaudRate:reqBaudRate;
  if(newBaudRate!=sDDCMPVars.baudRate) {
    // We have to change baud-rate
    TmrStart(sDDCMPVars.pollTmr, sDDCMPVars.mode==kDDCMPModeMaster? 100:DDCMPGetTO(kDDCMPFrameHdrLen)+10);
      // In master mode, leave time for slave to change its baud rate
      // In slave mode, leave time to complete transmission of STACK frame
    sDDCMPVars.baudRate= newBaudRate;
    sDDCMPVars.phase= kDDCMPPhaseChangeBaudRate;
  }
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: DDCMPGetTO
  Convert a timeout in characters in milliseconds

Parameters:
  cto ->  timeout expressed in characters
Returns:
  Timeout expressed in milliseconds
\*--------------------------------------------------------------------------------------*/
static uint16_t DDCMPGetTO(uint8_t cto) {
  #if kDDCMPBaudNegotiationSupport
  // Time (in 10us) required to transmit a byte at the specified baud-rate
  static const uint16_t sDDCMPByteDuration[kDDCMPbr_Count]= {
//    400, 800, 400, 200, 100, 50, 25, 18, 9
  // Times have been modified to avoid having very short timeouts at high baud-rates.
    400, 800, 400, 200, 100, 50, 50, 50, 50
  };

    return cto*sDDCMPByteDuration[sDDCMPVars.baudRate]/100      + 10;
  #else
  return cto*400                    /100      + 10;
  #endif
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPFinishCheck
  	  Must be called to check if connection finish completed
\*--------------------------------------------------------------------------------------*/
static void DDCMPFinishCheck(void) {
	
	if(sDDCMPVars.phase == kDDCMPPhaseFinishing || sDDCMPVars.phase == kDDCMPPhaseFinishingDone) {
		DDCMPFinishDone();																		// ACK gia' inviato, termino connessione
	}
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPFinishDone
	Funzione da chiamare quando la connessione e' terminata completamente.
	Si riattiva il timer perche' in master mode l'ultimo chr del Finish possa essere 
	trasmesso correttamente, in slave mode si puo' ritrasmettere l'ACK se il master
	dovesse inviare un ulteriore Finish.

\*--------------------------------------------------------------------------------------*/
static void DDCMPFinishDone(void) {

	sDDCMPVars.phase= kDDCMPPhaseFinishingDone;
	TmrStart(sDDCMPVars.pollTmr, sDDCMPVars.mode==kDDCMPModeMaster ? 10: 10);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPCRCInit
	inizializza CRC

Parameters:
	OUT : CRC = 0 
\*--------------------------------------------------------------------------------------*/
static void DDCMPCRCInit(uint16_t* crc) {
	
	*crc= 0;
  
}

/*--------------------------------------------------------------------------------------*\
Method: CrcEVAUpdate
  Update CRC with the given data byte [Ref1p5.2]

Parameters:
  data  ->  data byte to update CRC
  crc   ->  current CRC value
  crc   <-  updated CRC value
\*--------------------------------------------------------------------------------------*/
static void CrcEVAUpdate(uint8_t data, uint16_t* crc) {
  uint8_t bitCount= 8;
  uint16_t  crcVal= *crc;

  do {
    if((data^(uint8_t)crcVal) & 1) crcVal= ((crcVal^0x4002)>>1)|0x8000;
    else crcVal>>= 1;
    data>>= 1;
  } while(--bitCount>0);

  *crc= crcVal;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPCRCUpdate
  Aggiorna il CRC con il dato ricevuto.

Parameters:
  data  ->  data byte to update CRC
  crc   ->  current CRC value
  crc   <-  updated CRC value
\*--------------------------------------------------------------------------------------*/
static void DDCMPCRCUpdate(uint8_t data, uint16_t* crc) {

	CrcEVAUpdate(data, crc);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPFrameRxStart
  	Start frame reception
	L'ErroCounter e' usato per uscire dallo stato kDDCMPStateRxErr dovuto alle ricezioni
	spurie dell'irDA quando e' acceso il campo Mifare.

\*--------------------------------------------------------------------------------------*/
static void DDCMPFrameRxStart(void)
{
	sDDCMPVars.state= kDDCMPStateRxHdr0;
	//DDCMPHookRxBegin();										// Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
	ErrorCounter = 0;																			// Usato per uscire dallo stato kDDCMPStateRxErr
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPFrameTxPrapare
  Prepare to send a frame.
  After this routine is called all interferences from interrupt routines
  are inhibited and it is possible to set-up header data.
\*--------------------------------------------------------------------------------------*/
static void DDCMPFrameTxPrapare(void) {
  sDDCMPVars.state= kDDCMPStateIdle;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPFrameTxStart
  Start frame transmission of current frame
\*--------------------------------------------------------------------------------------*/
static void DDCMPFrameTxStart(void)
{
	//char dummybyte = 0;
#if kDDCMPStatisticsSupport
  	++sDDCMPVars.stats.txPktCnt;
#endif
	//MR19 DDCMPHookRxEnd();									// Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
#if !kDDCMPTurnRoundSupport
	sDDCMPVars.state= kDDCMPStateTxHdr0;
  	//DDCMPHookTxBegin();										// Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
  	DDCMPTxEmptyInd();											// Aggiunta per far partire la trasmissiomne
#else
  	sDDCMPVars.state= kDDCMPStateTxReady;
 #endif
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPSendDataFrame
  Start transmission of current data frame
\*--------------------------------------------------------------------------------------*/
static void DDCMPSendDataFrame(void) {
  uint8_t flags= sDDCMPVars.tx.selectFlag? (kDDCMPSyncFlagMask|kDDCMPSelectFlagMask) : kDDCMPSyncFlagMask;

  // Set-up header and send frame
  DDCMPFrameTxPrapare();
  sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]= kDDCMPMsgClassData;
  sDDCMPVars.frameHdr[kDDCMPFrameHdrType_Len]= (uint8_t)sDDCMPVars.tx.buffLen;

  sDDCMPVars.frameHdr[kDDCMPFrameHdrFlags]= flags | (sDDCMPVars.tx.buffLen>>8)&kDDCMPDataLenMask;
  sDDCMPVars.frameHdr[kDDCMPFrameHdrSBD_RR]= sDDCMPVars.rx.frameNum - 1;

  sDDCMPVars.frameHdr[kDDCMPFrameHdrMBD_XX]= sDDCMPVars.tx.frameNum;
  #if !kDDCMPExtendedAddrSupport
  sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd]= kDDCMPAddrAny;
  #else
  sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd]= sDDCMPVars.addr;
  #endif

  DDCMPFrameTxStart();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPSendCtrlFrame
  Start transmission of a control frame

Parameters:
  ctrlCode  ->  which control frame to send
  b2,b3,b4  ->  control frame content
\*--------------------------------------------------------------------------------------*/
static void DDCMPSendCtrlFrame(DDCMPCtrl ctrlCode, uint8_t b2, uint8_t b3, uint8_t b4) {
  // Set-up control frame
  DDCMPFrameTxPrapare();
  sDDCMPVars.frameHdr[kDDCMPFrameHdrClass]= kDDCMPMsgClassControl;
  sDDCMPVars.frameHdr[kDDCMPFrameHdrType_Len]= ctrlCode;
  sDDCMPVars.frameHdr[kDDCMPFrameHdrFlags]= b2;
  sDDCMPVars.frameHdr[kDDCMPFrameHdrSBD_RR]= b3;
  sDDCMPVars.frameHdr[kDDCMPFrameHdrMBD_XX]= b4;
  #if !kDDCMPExtendedAddrSupport
  sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd]= kDDCMPAddrAny;
  #else
  sDDCMPVars.frameHdr[kDDCMPFrameHdrSAdd]= sDDCMPVars.addr;
  #endif

  // Start tx of this frame
  DDCMPFrameTxStart();
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPSendStart
  Send a START control packet
\*--------------------------------------------------------------------------------------*/
static void DDCMPSendStart(void) {

	uint8_t baudRate;
	
	baudRate= DDCMPBaudRateMax();
	DDCMPSendCtrlFrame(kDDCMPCtrlStart, kDDCMPSyncFlagMask, 0, baudRate);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPSendStack
	Funzione per invaire STACK al master.
	Se lo START e' stato ricevuto a 2400 Baud e il byte mbd=0 ad indicazione che non
	vuole cambiare velocita', rispondiamo con sbd=0 nello STACK.
	Modifica effettuata per far funzionare il carrier MARK che non rispondeva se nello
	STACK l'sbd conteneva la nostra velocita' massima. 
  
Parameters:
  masterBaudRate  ->  baud rate del master
\*--------------------------------------------------------------------------------------*/
static void DDCMPSendStack(DDCMPBaudRate masterBaudRate) {

	uint8_t slaveBaudRate;

	// Se ricevo START a 2400 e mbd>0 rispondo con sbd=MaxBaudRate, altrimenti sbd=0
	if (sDDCMPVars.baudRate == kDDCMPbr2400 && sDDCMPVars.frameHdr[kDDCMPFrameHdrMBD_XX] != 0) {
		slaveBaudRate = DDCMPBaudRateMax();
	} else {
		slaveBaudRate = 0;
	}
	DDCMPSendCtrlFrame(kDDCMPCtrlStack, kDDCMPSyncFlagMask, slaveBaudRate, masterBaudRate);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPSendAck
  Send an ACK control packet and update last succesfull rx frame number
\*--------------------------------------------------------------------------------------*/
static void DDCMPSendAck(void) {
  DDCMPSendCtrlFrame(kDDCMPCtrlAck, kDDCMPSyncFlagMask, sDDCMPVars.rx.frameNum++, 0);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPSendNack
  Send a START control packet
Parameters:
  errCode ->  nack error code
\*--------------------------------------------------------------------------------------*/
static void DDCMPSendNack(DDCMPNackErr errCode) {
  #if kDDCMPStatisticsSupport
  ++sDDCMPVars.stats.txNackCnt;
  #endif
  DDCMPSendCtrlFrame(kDDCMPCtrlNack, kDDCMPSyncFlagMask|errCode, sDDCMPVars.rx.frameNum-1, 0);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPSendFinish
  Send a finish packet
Return:
  true if packet send accepted
\*--------------------------------------------------------------------------------------*/
static bool DDCMPSendFinish(void) {
  static const uint8_t sDDCMPFinishFrameData[]= { kDDCMPDataMsgCmd, kDDCMPCmdFinish};
  return DDCMPFrameSend(sDDCMPFinishFrameData, sizeof(sDDCMPFinishFrameData), false);
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPTurnRoundTmrStart
  Start Rx to Tx turn-round timer
\*--------------------------------------------------------------------------------------*/
static void DDCMPTurnRoundTmrStart(void) {
  #if kDDCMPTurnRoundSupport
  sDDCMPVars.waitTurnRound= true;
  TmrStart(sDDCMPVars.turnRoundTmr, sDDCMPVars.turnRoundTimeout);
  #endif
}


#if kDDCMPExtendedAddrSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPExtendedAddrSet
  Set extended address. This is used as the remote address for master connections,
  as the local address for slave connections.
  If extendedAddress is nil, use standard addressing

Parameters:
  extendedAddress ->  extended address to use
\*--------------------------------------------------------------------------------------*/
void DDCMPExtendedAddrSet(const DDCMPExtendedAddr extendedAddress) {
  sDDCMPVars.extAddr= extendedAddress;
}
#endif

#if kDDCMPStatisticsSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPConnStatisticsGet
  Read connection statistics
Parameters:
  connStats <-  statistics
\*--------------------------------------------------------------------------------------*/
void DDCMPConnStatisticsGet(DDCMPConnStats* connStats) {
  *connStats= sDDCMPVars.stats;
}
#endif


/*----------------------------------------------------------------------------------------------------*\
Method: ConnessioneDDCMP_IrDA
	Testa se sta per cominciare una una connessione DDCMP in IrDA o e' gia' iniziata.

	IN:	  - 
	OUT:  - sDDCMPVars.InConnessioneDDCMP
\*----------------------------------------------------------------------------------------------------*/
bool ConnessioneDDCMP_IrDA(void) {

	return (sDDCMPVars.InConnessioneDDCMP);
}

/*----------------------------------------------------------------------------------------------------*\
Method: IncomeSTART_DDCMP
	Testa se sono stati ricevuti almeno i primi 4 chr corretti dello START DDCMP.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------------*/
bool IncomeSTART_DDCMP(void) {

	return (sDDCMPVars.ShowAudit);

}

/*----------------------------------------------------------------------------------------------------*\
Method: Set_Phase_kDDCMPPhaseFinished
	Set phase "kDDCMPPhaseFinished" per riattivare la seriale IrDA dopo un invio del buffer di log
	al PC.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------------*/
void  Set_Phase_kDDCMPPhaseFinished(void) 
{
	sDDCMPVars.phase = kDDCMPPhaseFinished;
}


/*---------------------------------------------------------------------------------------------*\
Method: LogTxData_DDCMP
	Registra nel Buffer "tempRTx1" i caratteri trasmessi dal Master DDCMP.

	IN:	  - dato trasmesso dalla seriale
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  LogTxData_DDCMP(uint8_t txdata)
{
	//if ((SystOpt4 & TxIrDALog) && (!gVars.EndRecordLog)) {									// Se Tx Log Audit Irda abilitato registro chr trasmesso
	if (SystOpt4 & TxIrDALog) {																// Se Tx Log Audit Irda abilitato memorizzo chr trasmesso
		gVars.tempRTx1[gVars.pnt_data++] = txdata;											// Chr trasmesso
		gVars.tempRTx1[gVars.pnt_timing++] = 'T';											// Trasmissione
		RecordDati_DDCMP();
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: LogTxData_devCOM4
	Come la precedente LogTxData_devCOM4 ma vale per la devCOM4 (UART2 fisica).
	Registra nel Buffer "tempRTx1" i caratteri trasmessi dalla UART2 e altre informazioni
	dipendenti dal tipo di log richiesto.

	IN:	  - dato trasmesso dalla seriale
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
//void  LogTxData_devCOM4(uint8_t txdata) {
//}

/*---------------------------------------------------------------------------------------------*\
Method: LogRxData
	Registra nel Buffer "tempRTx1" i caratteri ricevuti dalla UART e altre informazioni
	dipendenti dal tipo di log richiesto.

	IN:	  - dato ricevuto dalla seriale e tipo di dato: chr valido o con Errore
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  LogRxData(uint8_t rxdata, bool tipo) 
{
	//if ( (SystOpt4 & TxIrDALog) && (!gVars.EndRecordLog) ) {								// Opzione Tx Log Audit Irda al PC e log abilitato
	if (SystOpt4 & TxIrDALog) {																// Opzione Tx Log Audit Irda al PC e log abilitato
		gVars.tempRTx1[gVars.pnt_data++] = rxdata;											// Chr Protocollo ricevuto
		if (tipo) {
			gVars.tempRTx1[gVars.pnt_timing++] = 'R';										// Ricezione
		} else {
			gVars.tempRTx1[gVars.pnt_timing++] = 'E';										// Errore in ricezione
		}
		RecordDati_DDCMP();
	}
}    

/*---------------------------------------------------------------------------------------------*\
Method: RecordDati_DDCMP
	Registro i dati complementari al DDCMP nel buffer RAM tempRTx1.
	Controllo se e' stata superata una PageSize di registrazione per procedere al loro trasfe-
	rimento in EEProm.
	
	IN:	  - buffer RAM gVars.tempRTx1, flag LastTransfer per indicare l'ultimo blocco dati
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  RecordDati_DDCMP(void)
{
/*
  uint16_t	temp;
	
	if ( (SystOpt4 & MaskTipoLog) == LogTipo0 ) {										// Log Tipo 0: solo Protocollo DDCMP
	} else {
		if ( (SystOpt4 & MaskTipoLog) == LogTipo1 ) {									// Log Tipo 1: Protocollo DDCMP + Time Stamp
			gVars.tempRTx1[gVars.pnt_timing++] = gVars.gKernelFreeCounter>>8;			// Time Stamp
			gVars.tempRTx1[gVars.pnt_timing++] = gVars.gKernelFreeCounter;
			temp = PIT0_GetCounterValue(gVars.devPIT0);									// Time Stamp Frazione
			if (Touts.TimerPrescaler) {													// Il timer e' caricato a 18.000 ogni 500usec e non piu' 36.000 ogni 1 msec
				temp += 18000;															// Per determinare gli 0,1 msec lo riporto come se fosse caricato a 36.000
			}
			gVars.tempRTx1[gVars.pnt_timing++] = temp>>8;
			gVars.tempRTx1[gVars.pnt_timing++] = temp;
		} else {																		// Log Tipo 2: Protocollo DDCMP + Flags sDDCMPVars	
			gVars.tempRTx1[gVars.pnt_timing++] = sDDCMPVars.state;						// sDDCMPVars.state
			gVars.tempRTx1[gVars.pnt_timing++] = sDDCMPVars.phase;						// sDDCMPVars.phase
			gVars.tempRTx1[gVars.pnt_timing++] = sDDCMPVars.rx.count;					// sDDCMPVars.rx.count (solo parte bassa)
			gVars.tempRTx1[gVars.pnt_timing++] = sDTSVars.state;						// sDTSVars.state
		}
	}
	DataCntr++;
	gVars.pnt_data %= DuePagineEE;														// Ogni due pagine di EE azzero pointers dati e timing a tempRTx1
	if (gVars.pnt_data == 0) {
		  gVars.pnt_timing = OffsetDatiLog;
	}
	if ( (DataCntr%PageSize) == 0) {
		Transfer_EE = true;
	}
*/
}

