/****************************************************************************************
File:
    VMC_Selezioni.c

Description:
    File con le funzioni per l'esecuzione delle selezioni.

History:
    Date       Aut  Note
    Lug 2020	MR   

*****************************************************************************************/

#include "ORION.H"
#include "VMC_Selez_STD.h"
#include "VMC_CPU_LCD.h"
#include "VMC_CPU_Motori.h"



/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	uint16_t	CntFlussRemaining;
extern	uint32_t	TimeRemaining;


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/



// ========================================================================
// ========    S   E   L   E   Z   I   O   N   E         ==================
// ========================================================================
/*---------------------------------------------------------------------------------------------*\
Method: Esegui_Selezione
	Esegue la selezione richiesta se non ci sono selezioni in corso, altrimenti commuta sulla
	nuova selezione richiesta o aggiunge Time/Impulsi a quella in corso.

	Si considera sempre il valore impostato come Time/Pulse come base 100, anche se il counter
	potrebbe essere superiore a causa di un credito inserito ulteriormente.
	(Info telefonica da Terruzzi del 21.09.2021).
	In realta' calcolo esattamente la percentuale residua che puo' anche essere superiore al
	valore nominale programmato se ho appena inserito un nuovo credito.

	L'algoritmo usato per gli impulsi e' il seguente (vale anche per i tempi):
	PercentualeImpulsiResidua = ((ImpulsiRimanenti * 1000) / CounterNominaleProgrammato)
	ValoreDaUtilizzare = ((PercentualeImpulsiResidua * CounterNominaleProgrammatoNuovaErogazione) / 1000)
	
	IN:	 - il numero della nuova selezione richiesta e' in NumVMCSelez (1-n)
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void  Esegui_Selezione(void)
{
	uint8_t		SelezInProgress;
	uint32_t	PercentageRemainingValue;
	
	if (gVMCState.InSelezione == false) return;

	gVMCState.EsitoFineVend = VEND_OK;															// Predispongo Vend OK
	SelezInProgress = GetSelectionInProgress();													// Numero dell'erogazione in corso in SelezInProgress (1-n)
	if (SelezInProgress > 0)
	{
		//-- Erogazione in corso --
		if (SelezInProgress == NumVMCSelez)
		{
			//-- Richiesta la stessa selezione gia' in corso --
			Add_Time_Pulses(SelezInProgress);													// Aggiunto Time/Pulses alla selezione in corso
			gVMCState.EsitoFineVend = VEND_OK;
			gVMCState.InSelezione = false;														// Fine selezione
			NumVMCSelez = 0;
			ForceLCD_NewVisual();
		}
		else
		{
			//-- Richiesta selezione diversa da quella in corso --
			//-- Calcolo percentuali di Time/Pulse da utilizzare --
			if ((sSelParam_A[SelezInProgress-1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
			{
				PercentageRemainingValue = (uint32_t)((CntFlussRemaining * 1000) / sSelParam_A[SelezInProgress-1].FlowCounterNominale);		// Percentuale impulsi residui su selezione in corso
				if ((sSelParam_A[NumVMCSelez-1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
				{
					sSelParam_A[NumVMCSelez-1].FlowCounterPercentuale = (uint16_t)(PercentageRemainingValue * ((uint32_t)sSelParam_A[NumVMCSelez-1].FlowCounterNominale) / 1000);	// Impulsi da utilizzare sulla nuova selezione richiesta
				}
				else
				{
					sSelParam_A[NumVMCSelez-1].TimeErogazionePercentuale = (PercentageRemainingValue * (sSelParam_A[NumVMCSelez-1].TimeErogazione) / 1000);							// Tempo da utilizzare sulla nuova selezione richiesta
				}
			}
			else
			{
				//-- Attuale selezione in corso e' a tempo --
				PercentageRemainingValue = ((TimeRemaining * 1000) / sSelParam_A[SelezInProgress-1].TimeErogazione);
				if ((sSelParam_A[NumVMCSelez-1].SelFlags & BIT_IMPULSI) == BIT_IMPULSI)
				{
					sSelParam_A[NumVMCSelez-1].FlowCounterPercentuale = (uint16_t)(PercentageRemainingValue * ((uint32_t)sSelParam_A[NumVMCSelez-1].FlowCounterNominale) / 1000);	// Impulsi da utilizzare sulla nuova selezione richiesta
				}
				else
				{
					sSelParam_A[NumVMCSelez-1].TimeErogazionePercentuale = (PercentageRemainingValue * (sSelParam_A[NumVMCSelez-1].TimeErogazione) / 1000);							// Tempo da utilizzare sulla nuova selezione richiesta
				}
			}
			if (Start_Erogazione(NumVMCSelez, USE_PERCENTAGE) == true)
			{
				gVMCState.EsitoFineVend = VEND_FAIL;												// FAIL VEND
			}
			gVMCState.InSelezione = false;															// Fine selezione
			NumVMCSelez = 0;
			ForceLCD_NewVisual();
		}
	}
	else
	{
		//-- Nessuna erogazione in corso--
		if (Start_Erogazione(NumVMCSelez, DO_NOT_USE_PERCENTAGE) == true)
		{
			gVMCState.EsitoFineVend = VEND_FAIL;												// FAIL VEND
		}
		gVMCState.InSelezione = false;															// Fine selezione
		NumVMCSelez = 0;
		ForceLCD_NewVisual();
		//LCD_Disp_SelInProgress(NumVMCSelez, gVMCState.EsitoFineVend);
		//MR19 StartAnimazione();
	}
}
