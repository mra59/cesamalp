/****************************************************************************************
File:
    ICPChg.c

Description:
    Protocollo ICP per rendiresto

History:
    Date       Aut  Note
    Set 2012 	Abe   

*****************************************************************************************/

#include <string.h>
#include <intrinsics.h>

#include "ORION.H"
#include "icpChg.h"
#include "icpProt.h"
#include "icpVMCProt.h"
#include "icpCHGProt.h"
#include "AuditCash.h"

#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

#if (_IcpSlave_ & ICP_CHANGER)

#ifndef kICPPatchRPSolutions_NoRxSetupCmd
#define kICPPatchRPSolutions_NoRxSetupCmd	true
#endif

#ifndef kICPPatchRPSolutions_NoRxPayoutOpt
#define kICPPatchRPSolutions_NoRxPayoutOpt	true
#endif

// ExpFF per Vmc Restomat
#ifndef kModifMDB_RestomatExpFFCHGDebug
#define kModifMDB_RestomatExpFFCHGDebug		false
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
T_ICP_CHGPOLLDATA ICPCHGPollInfo;
#endif

T_ICP_CHGINFO ICPCHGInfo;


// ExpFF per Vmc Restomat
#if kModifMDB_RestomatExpFFCHGDebug
static uint16_t mskCashBoxSet;
static uint16_t mskCashBoxGet;
#endif	//kModifMDB_RestomatExpFFCHGDebug



void CHGInit(uint8_t nSlaveType) {

	ICPProt_InitSlave(nSlaveType);
	memset(&ICPCHGInfo, 0, sizeof(T_ICP_CHGINFO));
	sCMDICPCash.acceptAmountCoinChgd = false;													// 26.06.15 Aggiunta perche' non c'era il suo clr
	DisattivaEnDisCHG();																		// 26.06.15 Per riabilitare la RR dopo un reset caldo
	#if (_IcpMaster_ == true)
	ICPCHGInfo.nWaitInitCnt = ICP_TM_CHGWAITINIT / ICP_TM_POLLPERIPH;
	ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
	ICPInfo.nSlaveLinkMask &= ~icpSlvLinkMask;
	#endif
	#if (_IcpSlaveCode_ & ICP_CHANGER)
	ICPProt_CHGResetPollData();
#if kReq456Support
//***********************
//	
//			MSTNOACK_SLVNORETRY (COINCO_2P)
//***********************
	ICPSetFrameSent(false);
	ICPSetFrameAcked(false);
	ICPSetNewState(-1);
	ICPSetNewSubState(0);
//***********************
#endif
	#endif

// ExpFF per Vmc Restomat
#if kModifMDB_RestomatExpFFCHGDebug
mskCashBoxSet = 0;
mskCashBoxGet = 0;
#endif	//kModifMDB_RestomatExpFFCHGDebug
}

bool ICPCHGIsInitCompleted(bool *initInProgress) {
	*initInProgress = ICPCHGInfo.nInfoMask ? true : false;
	return (ICPCHGInfo.nInfoMask >= 15) ? true : false;
}


//-------------------------------------------------
//
//					C H G	Slave Code
//
//-------------------------------------------------
#if (_IcpSlaveCode_ & ICP_CHANGER)

void CHGSlaveResp(void) {

#if kReq456Support
//***********************
//			MSTNOACK_SLVNORETRY (COINCO_2P)
//***********************
	//
	// Check if Retry have to do...
	//
	if(ICPGetFrameSent) {
		if(ICPGetFrameAcked) {
			//
			// Last frame sent is ok, reset last event && change to new state
			//
			if(ICPGetNewState != -1)
				ICPProt_ChangeState(icpSlv, ICPGetNewState, ICPGetNewSubState);
			ICPSetFrameSent(false);
			ICPSetFrameAcked(false);
			ICPSetNewState(-1);
			ICPSetNewSubState(0);
		}
	}
//***********************
#endif

	if(ICPDrvGetComState() == COM_RXFRAME) {

		// modifica x HArting sigarette
		/*if(!ICPCHGHook_IsInitCompleted())
			return;
		*/

		#if (_IcpSmsAlarms_ == true)
			//if(!ICPCHGHook_IsEnabled()) {
				switch(ICPCommand) {
				case ICP_CHG_CMD_RESET:
					ICPProt_CHGResetResp();
					break;
				case ICP_CHG_CMD_BILLTYPE:
					ICPProt_CHGCoinTypeResp();
					break;
				default:
					break;
				}
				return;
			//}
		#endif

		switch(ICPCommand) {
		case ICP_CHG_CMD_RESET:
			ICPProt_CHGResetResp();
			ICPCHGInfo.nInfoMask++;			// x compatibilitÓ inizializzaz. Vmc Vendo...
			break;
		case ICP_CHG_CMD_STATUS:
			ICPProt_CHGStatusResp();
			ICPCHGInfo.nInfoMask++;			// x compatibilitÓ inizializzaz. Vmc Vendo...
			break;
		case ICP_CHG_CMD_TUBESTATUS:
			ICPProt_CHGTubeStatusResp();
			break;
		case ICP_CHG_CMD_POLL:
			ICPProt_CHGPollResp();
			break;
		case ICP_CHG_CMD_COINTYPE:
			ICPProt_CHGCoinTypeResp();
			//ICPCHGInfo.nInfoMask |= kSlvChgInfoMask_InitCompleted;	// modifica x HArting sigarette
			if(ICPCHGInfo.nInfoMask < 15)
				ICPCHGInfo.nInfoMask++;		// x compatibilitÓ inizializzaz. Vmc Vendo...
			break;
		case ICP_CHG_CMD_DISPENSE:
			ICPProt_CHGDispenseResp();
			break;
		case ICP_CHG_CMD_EXP:
			switch(ICPSubCommand) {
			case ICP_CHG_CMD_EXPREQUESTID:
				ICPProt_CHGExp00Resp();
				break;
			case ICP_CHG_CMD_EXPENABLEOPT:
				ICPProt_CHGExp01Resp();
				break;
			case ICP_CHG_CMD_EXPPAYOUTVAL:
				if(ICPProt_CHGIsPayoutEnable())
					ICPProt_CHGExp02Resp();
				break;
			case ICP_CHG_CMD_EXPPAYOUTSTATUS:
				if(ICPProt_CHGIsPayoutEnable())
					ICPProt_CHGExp03Resp();
				break;
			case ICP_CHG_CMD_EXPPAYOUTPOLL:
				if(ICPProt_CHGIsPayoutEnable())
					ICPProt_CHGExp04Resp();
				break;
			case ICP_CHG_CMD_EXPDIAGSTATUS:
				if(ICPProt_CHGIsExtDiagEnable())
					ICPProt_CHGExp05Resp();
				break;
			case ICP_CHG_CMD_EXPMANFILLREP:
				if(ICPProt_CHGIsManFillEnable())
					ICPProt_CHGExp06Resp();
				break;
			case ICP_CHG_CMD_EXPMANPAYOUTREP:
				if(ICPProt_CHGIsManFillEnable())
					ICPProt_CHGExp07Resp();
				break;

			case ICP_CHG_CMD_EXPDIAG:
				ICPProt_CHGExpFFResp();
				break;

			default:
				ICPDrvSendRW(ICP_RW_ACK);
				break;
			}
			break;

		default:
			ICPDrvSendRW(ICP_RW_ACK);
			break;
		}

	
		//BUG: if command not handled no following commands recognized!
		if(ICPDrvGetComState() == COM_RXFRAME) {
			// if rxframe not handled, no tx data, then discard it
			ICPDrvSetComState(COM_IDLE);
		}

	}
}
#endif




//-------------------------------------------------
//
//					I N A C T I V E
//
//-------------------------------------------------
void CHGInactive(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint8_t nComState;
		bool fJustReset = false;

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CHG_INACTIVE)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CHG_INACTIVE_TXRESET:
					if(icpSlvCheckNoRespState <= 1) {
 						ICPProt_CHGReset();
					}
					break;
				case ICP_CHG_INACTIVE_WAITINIT:
					if(!ICPProt_WaitInit(icpSlvAddr))
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_INACTIVE_TXPOLL_1);
					break;
				case ICP_CHG_INACTIVE_TXPOLL_1:
				case ICP_CHG_INACTIVE_TXPOLL_2:
					ICPProt_CHGPoll();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CHG_INACTIVE_TXRESET:
				nRespType = ICPProt_CHGResetResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_INACTIVE_WAITINIT);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_INACTIVE_TXPOLL_1:
				nRespType = ICPProt_CHGPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				default:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_CHG_DISABLED, ICP_CHG_DISABLED_TXSTATUS);
					}
					break;
				case ICP_RESPTYPE_JUSTRESET:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_CHG_DISABLED, ICP_CHG_DISABLED_TXSTATUS);
					} else {
						ICPInfo.nSlaveJustResetMask |= icpSlvLinkMask;
					}
					break;
				}
				////ICPProt_ChangeState(icpSlv, -1, ICP_CHG_INACTIVE_TXPOLL_2);
				break;
			case ICP_CHG_INACTIVE_TXPOLL_2:
				nRespType = ICPProt_CHGPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					fJustReset = true;
					break;
				case ICP_RESPTYPE_RW_ACK:
				default:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask)
						fJustReset = true;
					break;
				}
				if(fJustReset) {
					ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
					ICPProt_ChangeState(icpSlv, ICP_CHG_DISABLED, ICP_CHG_DISABLED_TXSTATUS);
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	CHGSlaveResp();
#endif
}


//-------------------------------------------------
//
//					D I S A B L E D
//
//-------------------------------------------------
void CHGDisabled(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint8_t nComState;
		//MR19 bool fJustReset = false;		//Never referenced

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CHG_DISABLED)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CHG_DISABLED_TXSTATUS:
					ICPProt_SetReady(icpSlvAddr, false);
					ICPProt_CHGStatus();
					break;
				case ICP_CHG_DISABLED_TXEXP00:
					ICPProt_CHGExp00();
					break;
				case ICP_CHG_DISABLED_TXEXP01:
					ICPProt_CHGExp01();
					break;
				case ICP_CHG_DISABLED_TXEXP05:
					ICPProt_CHGExp05();
					break;
				case ICP_CHG_DISABLED_TXTUBESTATUS:
					ICPProt_CHGTubeStatus();
					break;
				case ICP_CHG_DISABLED_TXCOINTYPE:
				case ICP_CHG_DISABLED_TXCOINTYPE1:
				case ICP_CHG_DISABLED_TXCOINTYPE2:
					ICPProt_CHGCoinType();
					break;
				case ICP_CHG_DISABLED_TXPOLL:
					ICPProt_CHGPoll();
					break;
				case ICP_CHG_DISABLED_TXEXPDIAG:
					ICPProt_CHGExpFF();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
				case ICP_CHG_DISABLED_TXSTATUS:
					nRespType = ICPProt_CHGStatusResp();
					switch(nRespType) {
					case ICP_CHG_RESPTYPE_STATUS:
						switch(ICPProt_GetLevel()) {
						case 1:
						case 2:
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXTUBESTATUS);
							break;
						case 3:
						default:
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXEXP00);
							break;
						}
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXEXP00:
					nRespType = ICPProt_CHGExp00Resp();
					switch(nRespType) {
					case ICP_CHG_RESPTYPE_EXP00:
#ifdef kICPPatchProgemaMars_SkipEXP01
						if(ICPProt_GetLevel() < 3) {
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXTUBESTATUS);
						} else
#endif
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXEXP01);
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXEXP01:
					nRespType = ICPProt_CHGExp01Resp();
					switch(nRespType) {
					case ICP_RESPTYPE_RW_ACK:
//							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXEXP05);
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXTUBESTATUS);
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXEXP05:
					nRespType = ICPProt_CHGExp05Resp();
					switch(nRespType) {
					case ICP_CHG_RESPTYPE_EXP05:
					default:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXTUBESTATUS);
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXTUBESTATUS:
					nRespType = ICPProt_CHGTubeStatusResp();
					switch(nRespType) {
					case ICP_CHG_RESPTYPE_TUBESTATUS:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXCOINTYPE);
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXCOINTYPE:
					nRespType = ICPProt_CHGCoinTypeResp();
					switch(nRespType) {
					case ICP_RESPTYPE_RW_ACK:
						if(!ICPCHGInfo.CoinTypeState.nCoinEnable)
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXPOLL);
						else
							ICPProt_ChangeState(icpSlv, ICP_CHG_ENABLED, ICP_CHG_ENABLED_TXPOLL);
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_COINTYPEDONE);
						ICPProt_SetReady(icpSlvAddr, true);
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXCOINTYPE1:
					nRespType = ICPProt_CHGCoinTypeResp();
					switch(nRespType) {
					case ICP_RESPTYPE_RW_ACK:
						if(!ICPCHGInfo.CoinTypeState.nCoinEnable)
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXPOLL);
						else
							ICPProt_ChangeState(icpSlv, ICP_CHG_ENABLED, ICP_CHG_ENABLED_TXPOLL);
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_COINTYPEDONE);
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXCOINTYPE2:
					nRespType = ICPProt_CHGCoinTypeResp();
					switch(nRespType) {
					case ICP_RESPTYPE_RW_ACK:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXPOLL);
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_COINTYPEDONE);
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXPOLL:
					nRespType = ICPProt_CHGPollResp();
					switch(nRespType) {
					case ICP_RESPTYPE_JUSTRESET:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXSTATUS);
						break;
					case ICP_CHG_RESPTYPE_ESCROW:
						break;
					case ICP_CHG_RESPTYPE_BUSY:
						break;
					case ICP_CHG_RESPTYPE_INHIBIT:
						break;
					case ICP_RESPTYPE_RW_ACK:
						switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
						case ICP_VMCCMD_NONE:
							break;
						case ICP_VMCCMD_CHG_RESET:
							ICPProt_ChangeState(icpSlv, ICP_CHG_INACTIVE, ICP_CHG_INACTIVE_TXRESET);
							break;
						case ICP_VMCCMD_CHG_ENABLECOININTUBE:
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXCOINTYPE2);
							break;
						case ICP_VMCCMD_CHG_ENABLE:
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXCOINTYPE1);
							break;
						case ICP_VMCCMD_CHG_DIAG:
							ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXEXPDIAG);
							break;
						default:
							ICPProt_VMCCmdRejected(icpSlvAddr);
							break;
						}
						break;
					default:
						break;
					}
					break;
				case ICP_CHG_DISABLED_TXEXPDIAG:
					nRespType = ICPProt_CHGExpFFResp();
					switch(nRespType) {
					case ICP_RESPTYPE_RW_ACK:
					case ICP_CHG_RESPTYPE_EXPFF:
						ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_DIAGDONE);
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_DISABLED_TXPOLL);
						break;
					default:
						break;
					}
					break;
				default:
					break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	CHGSlaveResp();
#endif
}


//-------------------------------------------------
//
//					E N A B L E D
//
//-------------------------------------------------
void CHGEnabled(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint8_t nComState;
		//MR19 bool fJustReset = false;			//Never referenced

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CHG_ENABLED)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CHG_ENABLED_TXPOLL:
				case ICP_CHG_ENABLED_WAITENDDISPENSE:
					ICPProt_CHGPoll();
					break;
				case ICP_CHG_ENABLED_TXTUBESTATUS:
					ICPProt_CHGTubeStatus();
					break;
				case ICP_CHG_ENABLED_TXCOINTYPE:
				case ICP_CHG_ENABLED_TXCOINTYPE1:
					ICPProt_CHGCoinType();
					break;
				case ICP_CHG_ENABLED_TXDISPENSE:
					ICPProt_CHGDispense();
					break;
				case ICP_CHG_ENABLED_TXEXPDIAG:
					ICPProt_CHGExpFF();
					break;
				case ICP_CHG_ENABLED_TXEXP05:
					ICPProt_CHGExp05();
					break;
				case ICP_CHG_ENABLED_TXEXP06:
					ICPProt_CHGExp06();
					break;
				case ICP_CHG_ENABLED_TXEXP07:
					ICPProt_CHGExp07();
					break;
					
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CHG_ENABLED_TXPOLL:
				nRespType = ICPProt_CHGPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_CHG_DISABLED, ICP_CHG_DISABLED_TXSTATUS);
					break;
				case ICP_CHG_RESPTYPE_ESCROW:
					break;
				case ICP_CHG_RESPTYPE_BUSY:
					break;
				case ICP_CHG_RESPTYPE_INHIBIT:
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						if (ICPProt_CHGGetPayout() && (ICPCHGInfo.nFOptions & ICP_CHGOPT3_EXTDIAG)){
							ICPCHGInfo.send_exp_count++;
							if (ICPCHGInfo.send_exp_count > T_POLL_EXP05){
								ICPCHGInfo.send_exp_count = 0;
								ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXEXP05);
							}
						}
						break;
					case ICP_VMCCMD_CHG_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CHG_INACTIVE, ICP_CHG_INACTIVE_TXRESET);
						break;
					case ICP_VMCCMD_CHG_ENABLE:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXCOINTYPE);
						break;
					case ICP_VMCCMD_CHG_INHIBIT:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXCOINTYPE1);
						break;
					case ICP_VMCCMD_CHG_DISPENSE:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXDISPENSE);
						break;
					case ICP_VMCCMD_CHG_PAYOUT2:
						ICPProt_ChangeState(icpSlv, ICP_CHG_PAYOUT, ICP_CHG_PAYOUT_TXTUBESTATUS1);
						break;
					case ICP_VMCCMD_CHG_PAYOUT3:
						ICPProt_ChangeState(icpSlv, ICP_CHG_PAYOUT, ICP_CHG_PAYOUT_TXTUBESTATUS4);
						break;
					case ICP_VMCCMD_CHG_DIAG:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXEXPDIAG);
						break;
//					case ICP_VMCCMD_CHG_FILL:
//						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXEXP06);
//						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_ENABLED_TXTUBESTATUS:
				nRespType = ICPProt_CHGTubeStatusResp();
				switch(nRespType) {
				case ICP_CHG_RESPTYPE_TUBESTATUS:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_ENABLED_TXCOINTYPE:
				nRespType = ICPProt_CHGCoinTypeResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(ICPCHGInfo.CoinTypeState.nCoinEnable)
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXPOLL);
					else
						ICPProt_ChangeState(icpSlv, ICP_CHG_DISABLED, ICP_CHG_DISABLED_TXPOLL);
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_COINTYPEDONE);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_ENABLED_TXCOINTYPE1:
				nRespType = ICPProt_CHGCoinTypeResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXPOLL);
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_COINTYPEDONE);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_ENABLED_TXDISPENSE:
				nRespType = ICPProt_CHGDispenseResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_WAITENDDISPENSE);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_ENABLED_WAITENDDISPENSE:
				nRespType = ICPProt_CHGPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_CHG_DISABLED, ICP_CHG_DISABLED_TXSTATUS);
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_PAYOUT2DONE);
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXPOLL);
						break;
					case ICP_VMCCMD_CHG_RESET:
						ICPProt_ChangeState(icpSlv, ICP_CHG_INACTIVE, ICP_CHG_INACTIVE_TXRESET);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				default:
					break;
				}
			break;
				
			case ICP_CHG_ENABLED_TXEXP05:
				nRespType = ICPProt_CHGExp05Resp();
				switch(nRespType) {
					case ICP_CHG_RESPTYPE_EXP05:
						ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXEXP06);
						break;
					case ICP_CHG_RESPTYPE_TUBESTATUS:
						ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXTUBESTATUS);
						break;
					case ICP_CHG_RESPTYPE_EXP05_10_20:
						ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXEXP06);
						send_tube_status = TRUE;
						break;
					default:
						ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXPOLL);
						break;
					}
			break;
			
				case ICP_CHG_ENABLED_TXEXP06:
					nRespType = ICPProt_CHGExp06Resp();
					switch(nRespType) {
						case ICP_CHG_RESPTYPE_EXP06:
							ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXEXP07);
							break;
						default:
							ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXPOLL);
							break;
						}
				break;
				
					case ICP_CHG_ENABLED_TXEXP07:
						nRespType = ICPProt_CHGExp07Resp();
						switch(nRespType) {
							case ICP_CHG_RESPTYPE_EXP07:
								if (send_tube_status == TRUE){
									send_tube_status = FALSE;
									ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXTUBESTATUS);
								}else{
									ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXPOLL);
								}
								break;
							default:
								ICPProt_ChangeState(icpSlv,  -1, ICP_CHG_ENABLED_TXPOLL);
								break;
							}
					break;
			
			case ICP_CHG_ENABLED_TXEXPDIAG:
				nRespType = ICPProt_CHGExpFFResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_CHG_RESPTYPE_EXPFF:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_DIAGDONE);
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	CHGSlaveResp();
#endif
}


//-------------------------------------------------
//
//					P A Y O U T
//
//-------------------------------------------------
void CHGPayout(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint8_t nComState;
		//MR19 bool fJustReset = false;			//Never referenced

		/*icpSlv = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[icpSlv].nState != ICP_CHG_PAYOUT)
			return;
		icpSlvSubState = ICPInfo.aPeriphInfo[icpSlv].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_CHG_PAYOUT_TXEXP02:
					ICPProt_CHGExp02();
					break;
				case ICP_CHG_PAYOUT_TXEXP03:
					ICPProt_CHGExp03();
					break;
				case ICP_CHG_PAYOUT_TXEXP04:
					ICPProt_CHGExp04();
					break;
				case ICP_CHG_PAYOUT_TXTUBESTATUS1:
				case ICP_CHG_PAYOUT_TXTUBESTATUS2:
				case ICP_CHG_PAYOUT_TXTUBESTATUS3:
				case ICP_CHG_PAYOUT_TXTUBESTATUS4:
					ICPProt_CHGTubeStatus();
					break;
				case ICP_CHG_PAYOUT_TXDISPENSE:
					ICPProt_CHGDispense();
					break;
				case ICP_CHG_PAYOUT_TXPOLL:
				case ICP_CHG_PAYOUT_TXPOLL2:
				case ICP_CHG_PAYOUT_TXPOLL3:
					ICPProt_CHGPoll();
					break;
				case ICP_CHG_PAYOUT_CHECKTXDISPENSE:
					if(!ICPProt_VMCTxDispense())
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXTUBESTATUS2);
					else
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXDISPENSE);
					break;
				case ICP_CHG_PAYOUT_ENDDISPENSE:
					ICPProt_VMCEndDispense();
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_PAYOUT2DONE);
					ICPProt_ChangeState(icpSlv, ICP_CHG_ENABLED, ICP_CHG_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_CHG_PAYOUT_TXEXP02:
				nRespType = ICPProt_CHGExp02Resp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXEXP04);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXEXP03:
				nRespType = ICPProt_CHGExp03Resp();
				switch(nRespType) {
				case ICP_CHG_RESPTYPE_EXP03:
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXTUBESTATUS3);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXEXP04:
				nRespType = ICPProt_CHGExp04Resp();
#if DebugCHGReset
				CHGInit(0x08);
				CheckChgCashType();
			    sCMDICPCash.acceptAmountCoinChgd= false;
			    sCMDICPCash.chgReady = false;
				DisattivaEnDisCHG();
				nRespType = 0;
#endif				
				switch(nRespType) {
				case ICP_CHG_RESPTYPE_EXP04:
					break;
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXEXP03);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXTUBESTATUS1:
				nRespType = ICPProt_CHGTubeStatusResp();
				switch(nRespType) {
				case ICP_CHG_RESPTYPE_TUBESTATUS:
					if(!ICPProt_VMCDispense())
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_ENDDISPENSE);
					else
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_CHECKTXDISPENSE);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXTUBESTATUS2:
				nRespType = ICPProt_CHGTubeStatusResp();
				switch(nRespType) {
				case ICP_CHG_RESPTYPE_TUBESTATUS:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_ENDDISPENSE);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXTUBESTATUS3:
				nRespType = ICPProt_CHGTubeStatusResp();
				switch(nRespType) {
				case ICP_CHG_RESPTYPE_TUBESTATUS:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_CHG_PAYOUT3DONE);
					ICPProt_ChangeState(icpSlv, ICP_CHG_ENABLED, ICP_CHG_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXTUBESTATUS4:
				nRespType = ICPProt_CHGTubeStatusResp();
				switch(nRespType) {
				case ICP_CHG_RESPTYPE_TUBESTATUS:
					ICPProt_ChangeState(icpSlv, ICP_CHG_PAYOUT, ICP_CHG_PAYOUT_TXEXP02);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXDISPENSE:
				nRespType = ICPProt_CHGDispenseResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXPOLL);
					break;
				default:
					break;
				}
				break;
			case ICP_CHG_PAYOUT_TXPOLL:
			case ICP_CHG_PAYOUT_TXPOLL2:
			case ICP_CHG_PAYOUT_TXPOLL3:
				nRespType = ICPProt_CHGPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_CHG_DISABLED, ICP_CHG_DISABLED_TXSTATUS);
					break;
				case ICP_CHG_RESPTYPE_ESCROW:
					break;
				case ICP_CHG_RESPTYPE_BUSY:
					break;
				case ICP_CHG_RESPTYPE_INHIBIT:
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(icpSlvSubState) {
					case ICP_CHG_PAYOUT_TXPOLL:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXPOLL2);
						break;
					case ICP_CHG_PAYOUT_TXPOLL2:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_TXPOLL3);
						break;
					case ICP_CHG_PAYOUT_TXPOLL3:
					default:
						ICPProt_ChangeState(icpSlv, -1, ICP_CHG_PAYOUT_CHECKTXDISPENSE);
						break;
					}
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	CHGSlaveResp();
#endif
}


#if kICPPatchRPSolutions_NoRxSetupCmd
void ICPCHGHook_GetSetupInfo(void) {
#if (_IcpSlaveCode_ & ICP_CHANGER)
	uint8_t nCoinType;
	uint16_t nMask;
	uint16_t nCoinTypeInTubes;

	ICPCHGInfo.Status.nFLevel = ICPCHGHook_GetFeatureLevel();
	ICPCHGInfo.Status.nCCode = ICPCHGHook_GetCountryCode();
	ICPCHGInfo.Status.nUSF = ICPCHGHook_GetScalingFactor();
	ICPCHGInfo.Status.nDPP = ICPCHGHook_GetDecimalPoint();

	nCoinTypeInTubes = 0;
	for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {

		if(ICPCHGHook_IsCoinTypeInTube(nCoinType)) {
			nMask = 1 << nCoinType;
			nCoinTypeInTubes |= nMask;
		}

		ICPCHGInfo.Status.aCoinTypeVal[nCoinType] = ICPCHGHook_GetCoinTypeVal(nCoinType) / ICPCHGInfo.Status.nUSF;
	}
	ICPCHGInfo.Status.nCoinTypeInTubes = nCoinTypeInTubes;
#endif
}
#endif


//===============================================
//		RESET Command (08H)
//===============================================
void ICPProt_CHGReset(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_RESET *ptCmd = (T_CHGCMD_RESET *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	uint8_t nCmd, nCmdState;
	if((nCmd = ICPProt_VMCGetCmd(icpSlvAddr)) == ICP_VMCCMD_CHG_RESET) {
		ICPProt_VMCGetPar(icpSlvAddr);
		nCmdState = ICPCHGInfo.nCmdState;
	}
	CHGInit(icpSlvAddr);
	if(nCmd == ICP_VMCCMD_CHG_RESET) {
		ICPCHGInfo.nCmdType = nCmd;
		ICPCHGInfo.nCmdState = nCmdState;
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_RESET;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGResetResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_RESET *ptResp = (T_CHGRESP_RESET *)&ICPInfo.buff[0];

		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
		}
		ICPDrvSetComState(COM_IDLE);
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	CHGInit(icpSlvAddr);
	//ICPProt_CHGResetPollData();
	//if(ICPCHGHook_IsEnabled()) {
		ICPDrvSendRW(ICP_RW_ACK);
	//}
	ICPCHGHook_Init();
	ICPProt_SetInhibit(icpSlvAddr, true);


#if kICPPatchRPSolutions_NoRxSetupCmd
ICPCHGHook_GetSetupInfo();
#endif


#endif
	return nRespType;
}


//===============================================
//		STATUS Command (09H)
//===============================================
void ICPProt_CHGStatus(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_STATUS *ptCmd = (T_CHGCMD_STATUS *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_STATUS;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGStatusResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_STATUS *ptResp = (T_CHGRESP_STATUS *)&ICPInfo.buff[0];
		int8 nRespLen, n;

		if((!ICPInfo.fSlaveRW) && (ICPInfo.iRX > 1)) {
			ICPDrvSendRW(ICP_RW_ACK);
			nRespLen = sizeof(ptResp->Status);
			for(n = ICPInfo.iRX - 1; n < nRespLen; n++) {
				ICPInfo.buff[n] = 0;
			}
			ICPCHGInfo.Status = ptResp->Status;
			//MR20 Cvt16ToBigEndian(&ICPCHGInfo.Status.nCCode);
			ICPCHGInfo.Status.nCCode 			= __REV16(ICPCHGInfo.Status.nCCode);
			//MR20 Cvt16ToBigEndian(&ICPCHGInfo.Status.nCoinTypeInTubes);
			ICPCHGInfo.Status.nCoinTypeInTubes 	= __REV16(ICPCHGInfo.Status.nCoinTypeInTubes);
			ICPProt_SetLevel(icpSlvAddr, ICPCHGInfo.Status.nFLevel);
			ICPCHGHook_SetDPP(ptResp->Status.nDPP);
			nRespType = ICP_CHG_RESPTYPE_STATUS;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	uint8_t nCoinType;
	uint16_t nMask;
	uint16_t nCoinTypeInTubes;
	T_CHGRESP_STATUS *ptResp;

	ptResp = (T_CHGRESP_STATUS *)&ICPInfo.buff[0];

	ptResp->Status.nFLevel = ICPCHGHook_GetFeatureLevel();
	ptResp->Status.nCCode = ICPCHGHook_GetCountryCode();
	Cvt16ToBigEndian(&ptResp->Status.nCCode)
	ptResp->Status.nUSF = ICPCHGHook_GetScalingFactor();
	ptResp->Status.nDPP = ICPCHGHook_GetDecimalPoint();

	nCoinTypeInTubes = 0;
	for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {

		if(ICPCHGHook_IsCoinTypeInTube(nCoinType)) {
			nMask = 1 << nCoinType;
			nCoinTypeInTubes |= nMask;
		}

		ptResp->Status.aCoinTypeVal[nCoinType] = ICPCHGHook_GetCoinTypeVal(nCoinType) / ptResp->Status.nUSF;
	}
	ptResp->Status.nCoinTypeInTubes = nCoinTypeInTubes;
	Cvt16ToBigEndian(&ptResp->Status.nCoinTypeInTubes)

	// Ŕ giusto ?!!!!!
	ICPCHGInfo.Status = ptResp->Status;

	ICPInfo.nFrameLen = sizeof(ptResp->Status);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	ICPProt_CHGClearPollData();													// svuota eventuali "JustReset" in coda causato da comandi di Reset multipli
	}
#endif
	return nRespType;
}


//===============================================
//		TUBE STATUS Command (0AH)
//===============================================
void ICPProt_CHGTubeStatus(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_TUBESTATUS *ptCmd = (T_CHGCMD_TUBESTATUS *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CHG_TUBESTATUS) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_TUBESTATUS;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGTubeStatusResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_TUBESTATUS *ptResp = (T_CHGRESP_TUBESTATUS *)&ICPInfo.buff[0];
		int8 nRespLen, n;

		if((!ICPInfo.fSlaveRW) && (ICPInfo.iRX > 1)) {
			ICPDrvSendRW(ICP_RW_ACK);
			nRespLen = sizeof(ptResp->TubeStatus);
			for(n = ICPInfo.iRX - 1; n < nRespLen; n++) {
				ICPInfo.buff[n] = 0;
			}
			ICPCHGInfo.TubeStatus = ptResp->TubeStatus;
			//MR20 Cvt16ToBigEndian(&ICPCHGInfo.TubeStatus.nCoinTypeTubeFull);
			ICPCHGInfo.TubeStatus.nCoinTypeTubeFull = __REV16(ICPCHGInfo.TubeStatus.nCoinTypeTubeFull);
			nRespType = ICP_CHG_RESPTYPE_TUBESTATUS;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	uint8_t nCoinType;
	uint16_t nMask;
	uint16_t nCoinTypeTubeFull;
	T_CHGRESP_TUBESTATUS *ptResp;

	ptResp = (T_CHGRESP_TUBESTATUS *)&ICPInfo.buff[0];
	nCoinTypeTubeFull = 0;
	for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {
		if(ICPCHGHook_IsCoinTypeTubeFull(nCoinType)) {
			nMask = 1 << nCoinType;
			nCoinTypeTubeFull |= nMask;
		}
		ptResp->TubeStatus.aCoinsInTube[nCoinType] = ICPCHGHook_GetCoinsInTube(nCoinType);
	}
	ptResp->TubeStatus.nCoinTypeTubeFull = nCoinTypeTubeFull;
	Cvt16ToBigEndian(&ptResp->TubeStatus.nCoinTypeTubeFull)
	ICPInfo.nFrameLen = sizeof(ptResp->TubeStatus);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif
	return nRespType;
}


//===============================================
//		POLL Command (0BH)
//===============================================
void ICPProt_CHGPoll(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_POLL *ptCmd = (T_CHGCMD_POLL *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_POLL;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGPollResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_POLL *ptResp = (T_CHGRESP_POLL *)&ICPInfo.buff[0];
		uint8_t nCoinType;
		uint8_t nDestType;
		uint8_t nCoins, i, n;
		
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPDrvSendRW(ICP_RW_ACK);
			n = ICPInfo.iRX - 1;
			for(i = 0; i < n; i++) {
				nRespType = ptResp->aPollData[i];
				switch(nRespType & 0xC0) {
				case 0x00:
					switch(nRespType) {
					case ICP_CHG_ESCROWREQ:
						ICPCHGHook_SetEscrow();
						ICPCHGInfo.nInfoMask |= ICP_CHGINFO_ESCROW;
						nRespType = ICP_CHG_RESPTYPE_ESCROW;
						break;
					case ICP_CHG_PAYOUTBUSY:
					case ICP_CHG_BUSY:
						nRespType = ICP_CHG_RESPTYPE_BUSY;
						break;
					case ICP_CHG_ROMCHKERROR:
					case ICP_CHG_COINJAM:
						nRespType = ICP_CHG_RESPTYPE_INHIBIT;
						break;
					case ICP_CHG_JUSTRESET:
						nRespType = ICP_RESPTYPE_JUSTRESET;
						i = n;
						break;
					default:
						break;
					}
					//if(nRespType & 0x20)
					//	ICPCHGHook_SetSlug(nRespType & 0x1F);
					break;
				case 0x40:
					nDestType = (nRespType >> 4) & 0x03;
					nCoinType = nRespType & 0x0F;
					ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType] = ptResp->aPollData[++i];		// Registra il numero di monete che la RR dice di avere nel tubo
					ICPCHGHook_SetCoinsDeposited((ET_CHGPOLLCOINDEST)nDestType, nCoinType, (CreditCoinValue)(ICPCHGInfo.Status.aCoinTypeVal[nCoinType] * ICPCHGInfo.Status.nUSF));
					break;
				default:
					nCoins = (nRespType >> 4) & 0x07;
					nCoinType = nRespType & 0x0F;
					ICPCHGInfo.TubeStatus.aCoinsInTube[nCoinType] = ptResp->aPollData[++i];
					AuditPaydManual[0] = nCoinType; // per audit tube status
					AuditPaydManual[1] = nCoins;
					ICPCHGHook_SetCoinsDispMan(nCoins, nCoinType);

#if kModifMDB_RestomatExpFFCHG
#if kModifMDB_RestomatExpFFCHGDebug
{
	uint16_t msk;
	uint16_t *pMskCashBox;
	msk = 1 << nCoinType;
	pMskCashBox = (uint16_t*)&ICPInfo.freeuse[0];
	if(mskCashBoxSet & msk) {
		*pMskCashBox = mskCashBoxSet & ~msk;
	} else {
		*pMskCashBox = mskCashBoxSet | msk;
	}
	if(!ICPCHGDiag((uint8_t*)pMskCashBox, sizeof(mskCashBoxSet)))
		mskCashBoxSet = *pMskCashBox;
}
#endif	//kModifMDB_RestomatExpFFCHGDebug
#endif	//kModifMDB_RestomatExpFFCHG

					break;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)

#if kReq456Support
//***********************
//			MSTNOACK_SLVNORETRY (COINCO_2P)
//***********************
	//
	// Check if Retry have to do...
	//
	if(ICPGetFrameSent) {
		if(ICPGetFrameAcked == false) {
			//
			// Retry to send last frame if ACK not received from Master
			//
			ICPInfo.nFrameLen = ICPLoadSlaveFrameSent();
			ICPDrvSendMsg(ICPInfo.nFrameLen);
			return;
		} else {
			//
			// Last frame sent is ok, reset last event && change to new state
			//
			if(ICPGetNewState != -1)
				ICPProt_ChangeState(icpSlv, ICPGetNewState, ICPGetNewSubState);
		}
	}
	ICPSetFrameSent(false);
	ICPSetFrameAcked(false);
	ICPSetNewState(-1);
	ICPSetNewSubState(0);
//***********************
#endif

	if(ICPCHGHook_IsPayoutBusy())
		ICPProt_CHGSetStatus(ICP_CHG_PAYOUTBUSY);
	if(ICPCHGHook_IsAcceptorUnplugged())
		ICPProt_CHGSetStatus(ICP_CHG_ACCEPTORUNPLUGGED);
	ICPInfo.nFrameLen = ICPProt_CHGGetPollData(&ICPInfo.buff[0], true);
	if(ICPInfo.nFrameLen) {
		ICPDrvSendMsg(ICPInfo.nFrameLen);

#if kReq456Support
//***********************
//			MSTNOACK_SLVNORETRY (COINCO_2P)
//***********************
		//
		// Backup this frame needed in Retry process...
		//
		ICPSaveSlaveFrameSent(ICPInfo.nFrameLen);
		ICPSetFrameSent(true);
//***********************
#endif

	} else {
		ICPDrvSendRW(ICP_RW_ACK);
		if(ICPCHGInfo.nInfoMask >= 3)															// rx Reset+Status+CoinType commands
			if(ICPCHGInfo.nInfoMask < 15)
				ICPCHGInfo.nInfoMask++;
	}
#endif
	return nRespType;
}


//===============================================
//		COINTYPE Command (0CH)
//===============================================
void ICPProt_CHGCoinType(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_COINTYPE *ptCmd = (T_CHGCMD_COINTYPE *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar, nCmdType;
	uint8_t nFrameLen;
	uint16_t nCoinEnable;

	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_COINTYPE;
	ptCmdPar = NULL;
	nCmdType = ICPProt_VMCGetCmd(icpSlvAddr);
	switch(nCmdType) {
	case ICP_VMCCMD_CHG_ENABLE:
	case ICP_VMCCMD_CHG_INHIBIT:
	case ICP_VMCCMD_CHG_ENABLECOININTUBE:
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	default:
		break;
	}
	nCoinEnable = ICPCHGInfo.CoinTypeState.nCoinEnable;
	if(nCmdType == ICP_VMCCMD_CHG_ENABLECOININTUBE) {
		nCoinEnable = ICPCHGInfo.Status.nCoinTypeInTubes;
	}
	if(ptCmdPar) {
		if(nCoinEnable) {

			CreditValue nCashLimitAmount = *((CreditValue *)ptCmdPar);
			if(!nCashLimitAmount) nCoinEnable = 0;
			else {
				int16 nCoinType;
				uint16_t nMask;
				uint16_t nCoinInhibit = 0;
				// Abilito monete nei tubi in condizione di ExCh SOLO se c'e' almeno un coin type destinato nei tubi,
				// altrimenti accetto anche quelle che vanno in cassa.
				if((ICPCHGInfo.nInfoMask & ICP_CHGINFO_EXCH) && ICPCHGInfo.Status.nCoinTypeInTubes && !(ICPCHGInfo.nInfoMask & ICP_CHGINFO_KEY_STATE))
					nCoinEnable &= ICPCHGInfo.Status.nCoinTypeInTubes;
				nCashLimitAmount = nCashLimitAmount / ICPCHGInfo.Status.nUSF;
				for(nCoinType = ICP_MAX_COINTYPES - 1; nCoinType >= 0; nCoinType--) {
					if(!ICPCHGInfo.Status.aCoinTypeVal[nCoinType]) continue;
					if(ICPCHGInfo.Status.aCoinTypeVal[nCoinType] > nCashLimitAmount) {
						nMask = 1 << nCoinType;
						nCoinInhibit |= nMask;
					} else {
						break;
					}
				}
				if(nCoinInhibit) nCoinEnable &= ~nCoinInhibit;
			}
		}
	}
	ptCmd->CoinTypeState.nCoinEnable = nCoinEnable;
	ptCmd->CoinTypeState.nManDispenseEnable = 0xFFFF;
	//MR20 Cvt16ToBigEndian(&ptCmd->CoinTypeState.nCoinEnable);
	ptCmd->CoinTypeState.nCoinEnable 		= __REV16(ptCmd->CoinTypeState.nCoinEnable);
	//MR20 Cvt16ToBigEndian(&ptCmd->CoinTypeState.nManDispenseEnable);
	ptCmd->CoinTypeState.nManDispenseEnable = __REV16(ptCmd->CoinTypeState.nManDispenseEnable);
	ICPCHGInfo.CoinTypeState.nManDispenseEnable = ptCmd->CoinTypeState.nCoinEnable;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGCoinTypeResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_COINTYPE *ptResp = (T_CHGRESP_COINTYPE *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	T_CHGCMD_COINTYPE *ptCmd;
	bool fInh;
	//if(ICPCHGHook_IsEnabled()) {
		ICPDrvSendRW(ICP_RW_ACK);
	//}
	ptCmd = (T_CHGCMD_COINTYPE *)&ICPInfo.buff[0];
	Cvt16ToBigEndian(&ptCmd->CoinTypeState.nCoinEnable)
	Cvt16ToBigEndian(&ptCmd->CoinTypeState.nManDispenseEnable)
	ICPCHGInfo.CoinTypeState = ptCmd->CoinTypeState;
	ICPProt_CHGSetCoinTypeState();
	fInh = ptCmd->CoinTypeState.nCoinEnable ? false : true;
	ICPProt_SetInhibit(icpSlvAddr, fInh);
	}
#endif
	return nRespType;
}


//===============================================
//		DISPENSE Command (0DH)
//===============================================
void ICPProt_CHGDispense(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_DISPENSE *ptCmd = (T_CHGCMD_DISPENSE *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar, nCmdType;
	uint8_t nFrameLen;
	uint8_t nCoins;

	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_DISPENSE;
	ptCmdPar = NULL;
	nCmdType = ICPProt_VMCGetCmd(icpSlvAddr);
	if((nCmdType == ICP_VMCCMD_CHG_DISPENSE) || (nCmdType == ICP_VMCCMD_CHG_PAYOUT2)) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	if(ptCmdPar) {
		ptCmd->nCoinsCoinType = *((uint8_t *)ptCmdPar) & 0x0F;
		nCoins = *((uint8_t *)(ptCmdPar + sizeof(uint8_t)));
		ptCmd->nCoinsCoinType |= (nCoins << 4) & 0xF0;
	} else ptCmd->nCoinsCoinType = 0;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGDispenseResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_DISPENSE *ptResp = (T_CHGRESP_DISPENSE *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	uint8_t nCoinType;
	uint8_t nCoins;
	T_CHGCMD_DISPENSE *ptCmd;

	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_CHGCMD_DISPENSE *)&ICPInfo.buff[0];
	nCoinType = ptCmd->nCoinsCoinType & 0x0F;
	nCoins = (ptCmd->nCoinsCoinType >> 4) & 0x0F;
	ICPCHGHook_DispenseCoins(nCoinType, nCoins);
	}
#endif
	return nRespType;
}


//==================================================
//		EXP. ID. Command (0FH 00H) IDENTIFICATION
//==================================================
void ICPProt_CHGExp00(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_EXP00 *ptCmd = (T_CHGCMD_EXP00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPREQUESTID;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_EXP00 *ptResp = (T_CHGRESP_EXP00 *)&ICPInfo.buff[0];
		if((!ICPInfo.fSlaveRW) && (ICPInfo.iRX > 1)) {
			ICPDrvSendRW(ICP_RW_ACK);
			ICPCHGHook_SetManufactCode(&ptResp->PeriphID.aManufactCode[0], sizeof(ptResp->PeriphID.aManufactCode));
			ICPCHGHook_SetSerialNumber(&ptResp->PeriphID.aSerialNumber[0], sizeof(ptResp->PeriphID.aSerialNumber));
			ICPCHGHook_SetModelNumber(&ptResp->PeriphID.aModelNumber[0], sizeof(ptResp->PeriphID.aModelNumber));
			//MR20 Cvt16ToBigEndian(&ptResp->PeriphID.nSwVersion)
			ptResp->PeriphID.nSwVersion = __REV16(ptResp->PeriphID.nSwVersion);
			ICPCHGHook_SetSWVersion(ptResp->PeriphID.nSwVersion);
			ptResp->PeriphID.nFOptions = __REV(ptResp->PeriphID.nFOptions);
			ICPCHGInfo.nFOptions = ptResp->PeriphID.nFOptions;

#ifdef kICPPatchProgemaMars_SkipEXP01
			if(!ICPCHGInfo.nFOptions && ICPCHGInfo.Status.nFLevel >= 3) {
				ICPCHGInfo.Status.nFLevel = 2;
				ICPProt_SetLevel(icpSlvAddr, ICPCHGInfo.Status.nFLevel);
			}
#endif

			nRespType = ICP_CHG_RESPTYPE_EXP00;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	uint32_t nFOptions;
	T_CHGRESP_EXP00 *ptResp;

	ptResp = (T_CHGRESP_EXP00 *)&ICPInfo.buff[0];
	ICPHook_GetManufactCode(&ptResp->PeriphID.aManufactCode[0], sizeof(ptResp->PeriphID.aManufactCode));
	ICPHook_GetSerialNumber(&ptResp->PeriphID.aSerialNumber[0], sizeof(ptResp->PeriphID.aSerialNumber));
	ICPHook_GetModelNumber(&ptResp->PeriphID.aModelNumber[0], sizeof(ptResp->PeriphID.aModelNumber));
	ptResp->PeriphID.nSwVersion = ICPHook_GetSWVersion();
	Cvt16ToBigEndian(&ptResp->PeriphID.nSwVersion)
	nFOptions = ICP_CHGOPT_NONE;
	#if _IcpChgLevel_ >= 3
		nFOptions = ICPCHGHook_GetFeatures();
	#endif
	ptResp->PeriphID.nFOptions = nFOptions;
	Cvt32ToBigEndian(&ptResp->PeriphID.nFOptions)
	ICPCHGInfo.nFOptions = nFOptions;

	ICPInfo.nFrameLen = sizeof(ptResp->PeriphID);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif
	return nRespType;
}


//====================================================
//		EXP. OPT. Command (0FH 01H) FEATURE ENABLE
//====================================================
void ICPProt_CHGExp01(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_EXP01 *ptCmd = (T_CHGCMD_EXP01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;

	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPENABLEOPT;
	ptCmd->nFOptions = ICP_CHGOPT_NONE;
	if((ICPCHGInfo.nFOptions & ICP_CHGOPT3_PAYOUT) && ICPProt_CHGGetPayout())
		ptCmd->nFOptions |= ICP_CHGOPT3_PAYOUT;
	if((ICPCHGInfo.nFOptions & ICP_CHGOPT3_EXTDIAG) && ICPProt_CHGGetExtDiag())
		ptCmd->nFOptions |= ICP_CHGOPT3_EXTDIAG;
	if((ICPCHGInfo.nFOptions & ICP_CHGOPT3_MANFILL) && ICPProt_CHGGetManualFill())
		ptCmd->nFOptions |= ICP_CHGOPT3_MANFILL;
	if((ICPCHGInfo.nFOptions & ICP_CHGOPT3_FTL) && ICPProt_CHGGetFTL())
		ptCmd->nFOptions |= ICP_CHGOPT3_FTL;
	ICPCHGInfo.nFOptions = ptCmd->nFOptions;
	ptCmd->nFOptions = __REV(ptCmd->nFOptions);
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_EXP01 *ptResp = (T_CHGRESP_EXP01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	T_CHGCMD_EXP01 *ptCmd;
	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_CHGCMD_EXP01 *)&ICPInfo.buff[0];
	Cvt32ToBigEndian(&ptCmd->nFOptions)
	#if _IcpChgLevel_ >= 3
		ICPCHGInfo.nFOptions = ICPCHGHook_GetFeatures();
	#endif
	ICPCHGInfo.nFOptions &= ptCmd->nFOptions;
	}
#endif
	return nRespType;
}


//===============================================
//		PAYOUT VALUE Command (0FH 02H)
//===============================================
void ICPProt_CHGExp02(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_EXP02 *ptCmd = (T_CHGCMD_EXP02 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar, nCmdType;
	CreditCoinValue nPayoutVal;
	uint8_t nFrameLen;

	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPPAYOUTVAL;
	ptCmdPar = NULL;
	nCmdType = ICPProt_VMCGetCmd(icpSlvAddr);
	if(nCmdType == ICP_VMCCMD_CHG_PAYOUT3) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	if(ptCmdPar) {
		nPayoutVal = *((CreditCoinValue *)ptCmdPar);
		nPayoutVal /= ICPCHGInfo.Status.nUSF;
		if(nPayoutVal & 0xff00)
			ptCmd->nPayoutVal = 0xff;
		else
			ptCmd->nPayoutVal = nPayoutVal;
		//Cvt16ToBigEndian(&ptCmd->nPayoutVal);
	} else ptCmd->nPayoutVal = 0;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp02Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_EXP02 *ptResp = (T_CHGRESP_EXP02 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	#if _IcpChgLevel_ >= 3
		T_CHGCMD_EXP02 *ptCmd;
		CreditCoinValue nPayoutVal;

		ICPDrvSendRW(ICP_RW_ACK);
		ptCmd = (T_CHGCMD_EXP02 *)&ICPInfo.buff[0];
		nPayoutVal = ptCmd->nPayoutVal * ICPCHGInfo.Status.nUSF;
		ICPCHGHook_PayoutVal(nPayoutVal);
	#endif
	}
#endif
	return nRespType;
}


//===============================================
//		PAYOUT STATUS Command (0FH 03H)
//===============================================
void ICPProt_CHGExp03(void) {
#if (_IcpMaster_ == true)

	T_CHGCMD_EXP03 *ptCmd = (T_CHGCMD_EXP03 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;

	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPPAYOUTSTATUS;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp03Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		//MR19  T_CHGRESP_EXP03 *ptResp = (T_CHGRESP_EXP03 *)&ICPInfo.buff[0];				//Never referenced
		int16 nRespLen, n, nCoins;					//16/11/00 OSz int8 --> int16 (-11)
		CreditCoinValue nPaidoutVal = 0;

		if(!ICPInfo.fSlaveRW) {
			ICPDrvSendRW(ICP_RW_ACK);
			nRespLen = ICPInfo.iRX - 1;
			for(n = 0; n < nRespLen; n++) {
				nCoins = ICPInfo.buff[n];
				AuditPaydManual[n] = nCoins;
				if(nCoins)
					nPaidoutVal += (nCoins * ICPCHGInfo.Status.aCoinTypeVal[n]);
			}
			nPaidoutVal *= ICPCHGInfo.Status.nUSF;
//			Cvt16ToBigEndian(&nPaidoutVal);
			ICPCHGHook_SetPayoutComplete(nPaidoutVal);
			nRespType = ICP_CHG_RESPTYPE_EXP03;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	#if _IcpChgLevel_ >= 3
		if(!ICPCHGHook_IsPayoutCompleted()) {
			ICPDrvSendRW(ICP_RW_ACK);
		} else {
			uint8_t nCoinType;
			T_CHGRESP_EXP03 *ptResp;
			ptResp = (T_CHGRESP_EXP03 *)&ICPInfo.buff[0];
			for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {
				ptResp->aCoinsPaidOut[nCoinType] = ICPCHGHook_GetCoinsPaidOut(nCoinType);
			}
			ICPInfo.nFrameLen = sizeof(ptResp->aCoinsPaidOut);
			ICPDrvSendMsg(ICPInfo.nFrameLen);
		}
	#endif
	}
#endif

	return nRespType;
}


//===============================================
//		PAYOUT POLL Command (0FH 04H)
//===============================================
void ICPProt_CHGExp04(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_EXP04 *ptCmd = (T_CHGCMD_EXP04 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPPAYOUTPOLL;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp04Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_EXP04 *ptResp = (T_CHGRESP_EXP04 *)&ICPInfo.buff[0];
		CreditCoinValue nPaidoutVal;

		if(!ICPInfo.fSlaveRW) {
			ICPDrvSendRW(ICP_RW_ACK);
			nPaidoutVal = ptResp->nPaidOutVal * ICPCHGInfo.Status.nUSF;
			//Cvt16ToBigEndian(&nPaidoutVal);
			ICPCHGHook_SetPaidoutAmount(nPaidoutVal);
			nRespType = ICP_CHG_RESPTYPE_EXP04;
		} else {
			if(ICPAddrCommand == ICP_RW_ACK) {
				nRespType = ICP_RESPTYPE_RW_ACK;
				ICPDrvSetComState(COM_IDLE);
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
	#if _IcpChgLevel_ >= 3
		T_CHGRESP_EXP04 *ptResp;
		uint8_t nPaidoutVal;

		nPaidoutVal = ICPCHGHook_GetPaidOutVal() / ICPCHGInfo.Status.nUSF;
		if(ICPCHGHook_IsPayoutCompleted() && !nPaidoutVal) {
			ICPDrvSendRW(ICP_RW_ACK);
		} else {
			ptResp = (T_CHGRESP_EXP04 *)&ICPInfo.buff[0];
			ptResp->nPaidOutVal = nPaidoutVal;
			ICPInfo.nFrameLen = sizeof(ptResp->nPaidOutVal);
			ICPDrvSendMsg(ICPInfo.nFrameLen);
		}
	#endif
	}
#endif
	return nRespType;
}


//===============================================
//		EXP. SEND DIAG. STATUS Command (0FH 05H)
//===============================================
void ICPProt_CHGExp05(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_EXP05 *ptCmd = (T_CHGCMD_EXP05 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPDIAGSTATUS;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp05Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
	uint8_t i;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_EXP05 *ptResp = (T_CHGRESP_EXP05 *)&ICPInfo.buff[0];

		if(!ICPInfo.fSlaveRW) {
			ICPDrvSendRW(ICP_RW_ACK);

			for (i=0;i<(ICPInfo.iRX-1);i+=2){
			
			switch (ptResp->aStatusCode[i]){
			case 3:
				ICPCHGInfo.chg_exp_state = CHG_OK;
				break;
			case 4:
				ICPCHGInfo.chg_exp_state = CHG_KEY_SHIFTED;
				break;
			case 5:
				switch (ptResp->aStatusCode[i+1]){
				case 0x10:
					ICPCHGInfo.chg_exp_state = CHG_MANUAL_FILL;
					if (nRespType == ICP_CHG_RESPTYPE_TUBESTATUS)nRespType = ICP_CHG_RESPTYPE_EXP05_10_20;
					else nRespType = ICP_CHG_RESPTYPE_EXP05;
					break;
				case 0x20:
					ICPCHGInfo.chg_exp_state = CHG_NEW_INVENTORY;
					if (nRespType == ICP_CHG_RESPTYPE_EXP05)nRespType = ICP_CHG_RESPTYPE_EXP05_10_20;
					else nRespType = ICP_CHG_RESPTYPE_TUBESTATUS;
					break;
				default:
					break;
				}  //switch 1
			default:
				break;
			}	//switch 0
		  }  //for i
		}	//if fslaverw
	}  //if master
#endif
		return nRespType;
} //funct


//===============================================
//		EXP. MAN. FILL REPORT Command (0FH 06H)
//===============================================
void ICPProt_CHGExp06(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_EXP06 *ptCmd = (T_CHGCMD_EXP06 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPMANFILLREP;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp06Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
	uint8_t ix;
	uint8_t nRespLen;
	uint32_t fill_amount = 0;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_EXP06 *ptResp = (T_CHGRESP_EXP06 *)&ICPInfo.buff[0];

		if(!ICPInfo.fSlaveRW) {
			ICPDrvSendRW(ICP_RW_ACK);
			nRespLen = ICPInfo.iRX - 1;
			for (ix=0;ix<nRespLen;ix++){
				AuditFillManual[ix] = ptResp->aCoinsManFilled[ix];
				if (ptResp->aCoinsManFilled[ix]){
					fill_amount += ((ICPCHGInfo.Status.aCoinTypeVal[ix] * ICPCHGInfo.Status.nUSF) * AuditFillManual[ix]);  
					}
				}
			for (ix=nRespLen;ix<ICP_MAX_COINTYPES;ix++)AuditFillManual[ix] = 0;   // NRI manda solo 15 bytes invece che 16 come definito nel protocollo

			CoinFillManualAuditStart(fill_amount);
			nRespType = ICP_CHG_RESPTYPE_EXP06;
			}
		}
#endif
		return nRespType;
}


//===============================================
//		EXP. MAN. PAYOUT REP. Command (0FH 07H)
//===============================================
void ICPProt_CHGExp07(void) {
	
#if (_IcpMaster_ == true)
	T_CHGCMD_EXP07 *ptCmd = (T_CHGCMD_EXP07 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPMANPAYOUTREP;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExp07Resp(void) {
	
	uint8_t nRespType = ICP_RESPTYPE_NONE;
	uint8_t ix;
	uint8_t nRespLen;
	uint32_t payd_amount = 0;
	extern uint8_t AuditPaydManual[];

#if (_IcpMaster_ == true)
	if (icpMasterMode) {
		T_CHGRESP_EXP07 *ptResp = (T_CHGRESP_EXP07 *)&ICPInfo.buff[0];
		if (!ICPInfo.fSlaveRW) {
			ICPDrvSendRW(ICP_RW_ACK);
			nRespLen = ICPInfo.iRX - 1;
			for (ix=0;ix<nRespLen;ix++) {
				AuditPaydManual[ix] = ptResp->aCoinsManPayout[ix];
				if (ptResp->aCoinsManPayout[ix]) {
					payd_amount += ((ICPCHGInfo.Status.aCoinTypeVal[ix] * ICPCHGInfo.Status.nUSF) * AuditPaydManual[ix]);  
				}
			}
			for (ix=nRespLen; ix<ICP_MAX_COINTYPES; ix++) AuditPaydManual[ix] = 0;   			// NRI manda solo 15 bytes invece che 16 come definito nel protocollo
		    CoinPayoutExpAuditStart(payd_amount, (CMDrvPAYPOS) Pay_Manual);
			nRespType = ICP_CHG_RESPTYPE_EXP07;
		}
	}
#endif	

	return nRespType;
}


//===============================================
//		EXP. DIAG. Command (0FH FFH)
//===============================================
void ICPProt_CHGExpFF(void) {
#if (_IcpMaster_ == true)
	T_CHGCMD_EXPFF *ptCmd = (T_CHGCMD_EXPFF *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar;
	uint8_t nFrameLen;
	uint8_t **pt, *ptData, nDataLen, i;

	ptCmd->nCmd = icpSlvAddr + ICP_CHG_CMD_EXP;
	ptCmd->nSubCmd = ICP_CHG_CMD_EXPDIAG;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	ptCmdPar = NULL;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_CHG_DIAG) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	if(ptCmdPar) {
		nDataLen = *((uint8_t *)ptCmdPar);
		if(nDataLen > (ICP_MAX_FRAMELEN - 2))
			nDataLen = ICP_MAX_FRAMELEN - 2;
		pt = (uint8_t **)(ptCmdPar + sizeof(uint8_t));
		ptData = (uint8_t *)(*pt);
		for(i = 0; i < nDataLen; i++) {
			ptCmd->aDiagData[i] = ptData[i];
		}
		nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd) + nDataLen;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_CHGExpFFResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_CHGRESP_EXPFF *ptResp = (T_CHGRESP_EXPFF *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPDrvSendRW(ICP_RW_ACK);
			ICPCHGHook_SetDiagData(ptResp->aDiagData, (uint8_t)(ICPInfo.iRX - 1));
			nRespType = ICP_CHG_RESPTYPE_EXPFF;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	{
#if kModifMDB_RestomatExpFFCHG
	uint16_t *pMsk;
	ICPDrvSendRW(ICP_RW_ACK);
	pMsk = (uint16_t*)&ICPInfo.buff[3];
	Cvt16ToBigEndian(pMsk)
	ICPCHGHook_SetCoinDstCashBox(*pMsk);
#if kModifMDB_RestomatExpFFCHGDebug
mskCashBoxGet = *pMsk;
#endif
#endif	//kModifMDB_RestomatExpFFCHG
	}
#endif

	return nRespType;
}



#if _IcpChgFTL_ > 0

void ICPProt_CHGExpFA(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CHGExpFAResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	//...
#endif
	return nRespType;
}

void ICPProt_CHGExpFB(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CHGExpFBResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	//...
#endif
	return nRespType;
}

void ICPProt_CHGExpFC(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CHGExpFCResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	//...
#endif
	return nRespType;
}

void ICPProt_CHGExpFD(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CHGExpFDResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	//...
#endif
	return nRespType;
}

void ICPProt_CHGExpFE(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_CHGExpFEResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_CHANGER)
	//...
#endif
	return nRespType;
}
#endif	// _IcpChgFTL_ > 0




//-------------------------------------------------
// Routines to give CHG info
//-------------------------------------------------
#if (_IcpMaster_ == true)
bool ICPProt_CHGGetPayout(void) {
	return (ICPProt_GetLevel() >= 3) ? true : false;
}

bool ICPProt_CHGGetExtDiag(void) {
	return (ICPProt_GetLevel() >= 3) ? true : false;
//	return false;
}

bool ICPProt_CHGGetManualFill(void) {
	return (ICPProt_GetLevel() >= 3) ? true : false;
//	return false;
}

bool ICPProt_CHGGetFTL(void) {
	return _IcpChgFTL_;
}

bool ICPProt_CHGGetReady(void){
	return sCMDICPCash.chgReady;
}

bool ICPProt_CHGGet_exp_state(void){
	return ICPCHGInfo.chg_exp_state;
}

void ICPProt_CHGRes_exp_state(void){
	ICPCHGInfo.chg_exp_state = CHG_NONE;
}

uint8_t ICPProt_CHGGet_USF(void){
	return ICPCHGInfo.Status.nUSF ;
}


uint16_t ICPProt_CHGGetCoinValue(uint8_t ix){
	return ICPCHGInfo.Status.aCoinTypeVal[ix];
}

uint16_t GetnCoinTypeInTubes(void){
	return ICPCHGInfo.Status.nCoinTypeInTubes;
}

uint16_t GetnCoinTypeEnabled(void) {
	return ICPCHGInfo.CoinTypeState.nCoinEnable;
}

uint16_t GetCoinsInTube(uint8_t ic){
	return ICPCHGInfo.TubeStatus.aCoinsInTube[ic];
}

uint16_t GetCoinsTubeFull(void){
	return ICPCHGInfo.TubeStatus.nCoinTypeTubeFull;
}

void SetCHGInfoExChange(void){
	Set_fExactChange;
	ICPCHGInfo.nInfoMask |= ICP_CHGINFO_EXCH;
}

void ClrCHGInfoExChange(void){
	Clear_fExactChange;
	ICPCHGInfo.nInfoMask &= ~ICP_CHGINFO_EXCH;
}

void UpdateCHGInfoKeyState(uint8_t KeyState){
	if(KeyState)
	ICPCHGInfo.nInfoMask |= ICP_CHGINFO_KEY_STATE;
	else
	ICPCHGInfo.nInfoMask &= ~ICP_CHGINFO_KEY_STATE;
}

void Set_Chg_Id_Data(uint8_t ix, uint8_t l, uint8_t *point){
	uint8_t i;
	
	for (i=0;i<l;i++){
		ICPCHGInfo.ChgInfo.aManufactCode[i+ix] = *point;   //info dati Coin Changer
		point++;
	}
}

//MR19 Aggiunta Giu 2019
void Set_Chg_SwRev_Data(uint16_t Chng_Rev){

  ICPCHGInfo.ChgInfo.nSwVersion = Chng_Rev;					//Coin Changer Sw Rervision
}
#endif


#if (_IcpSlaveCode_ & ICP_CHANGER)
void ICPProt_CHGClearPollData(void) {
	ICPCHGPollInfo.nData = 0;
	ICPCHGPollInfo.nWrData = 0;
	ICPCHGPollInfo.nRdData = 0;
}

void ICPProt_CHGResetPollData(void) {
	ICPProt_CHGClearPollData();
	ICPProt_CHGSetStatus(ICP_CHG_JUSTRESET);
}

uint8_t ICPProt_CHGGetPollData(uint8_t *ptData, bool fAllData) {
	uint8_t nDataLen = 0;
	uint8_t nData = ICPCHGPollInfo.nData;
	uint8_t nRdData = ICPCHGPollInfo.nRdData;
	uint8_t *ptDest = ptData;
	uint8_t *ptSrc;

	if(ptData && nData) {
		ptSrc = &ICPCHGPollInfo.aPollData[0];
		if(!fAllData) nData = 2;
		ICPCHGPollInfo.nData -= nData;
		do {
			*ptDest++ = ptSrc[nRdData];
			if(ptSrc[nRdData] & 0x80) {
				*ptDest++ = ptSrc[nRdData+1];
			} else {
				if(ptSrc[nRdData] & 0x40) {
					*ptDest++ = ptSrc[nRdData+1];
				}
			}
			nRdData = (nRdData + 2) & 0x0F;
			nData -= 2;
		} while(nData);
		ICPCHGPollInfo.nRdData = nRdData;
		nDataLen = ptDest - ptData;
	}
	return nDataLen;
}


void ICPProt_CHGSetStatus(ET_CHGPOLLSTATUS nStatusType) {

	if(ICPCHGPollInfo.nData < sizeof(ICPCHGPollInfo.aPollData)) {

		switch(nStatusType) {
			case ICP_CHG_ESCROWREQ:
			case ICP_CHG_PAYOUTBUSY:
			case ICP_CHG_NOCREDIT:
			case ICP_CHG_DEFECTTUBESENSOR:
			case ICP_CHG_DOUBLEARRIVAL:
			case ICP_CHG_ACCEPTORUNPLUGGED:
			case ICP_CHG_TUBEJAM:
			case ICP_CHG_ROMCHKERROR:
			case ICP_CHG_COINROUTINGERROR:
			case ICP_CHG_BUSY:
			case ICP_CHG_JUSTRESET:
			case ICP_CHG_COINJAM:
				break;
			default:
				// invalid status type!
				return;
		}
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData] = nStatusType;
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData + 1] = 0;
		ICPCHGPollInfo.nWrData = (ICPCHGPollInfo.nWrData + 2) & 0x0F;
		ICPCHGPollInfo.nData += 2;
	}
}

void ICPProt_CHGSetSlug(uint8_t nSlugs) {
	if(ICPCHGPollInfo.nData < sizeof(ICPCHGPollInfo.aPollData)) {
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData] = (nSlugs & 0x1F) | 0x20;
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData + 1] = 0;
		ICPCHGPollInfo.nWrData = (ICPCHGPollInfo.nWrData + 2) & 0x0F;
		ICPCHGPollInfo.nData += 2;
	}
}

void ICPProt_CHGSetCoinsDeposited(uint8_t nCoinType, ET_CHGPOLLCOINDEST DestType) {
	uint8_t nCoinsDeposited = 0x40;
	uint8_t nDestType;

	if(ICPCHGPollInfo.nData < sizeof(ICPCHGPollInfo.aPollData)) {
		nDestType = DestType << 4;
		nCoinsDeposited |= nDestType;
		nCoinType &= 0x0F;
		nCoinsDeposited |= nCoinType;
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData] = nCoinsDeposited;
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData + 1] = ICPCHGHook_GetCoinsInTube(nCoinType);
		ICPCHGPollInfo.nWrData = (ICPCHGPollInfo.nWrData + 2) & 0x0F;
		ICPCHGPollInfo.nData += 2;
	}
}

void ICPProt_CHGSetCoinsDispMan(uint8_t nCoinType, uint8_t nCoins) {
	uint8_t nCoinsDispMan = 0x80;

	if(ICPCHGPollInfo.nData < sizeof(ICPCHGPollInfo.aPollData)) {
		nCoins = (nCoins & 0x07) << 4;
		nCoinsDispMan |= nCoins;
		nCoinType &= 0x0F;
		nCoinsDispMan |= nCoinType;
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData] = nCoinsDispMan;
		ICPCHGPollInfo.aPollData[ICPCHGPollInfo.nWrData + 1] = ICPCHGHook_GetCoinsInTube(nCoinType);
		ICPCHGPollInfo.nWrData = (ICPCHGPollInfo.nWrData + 2) & 0x0F;
		ICPCHGPollInfo.nData += 2;
	}
}

void ICPProt_CHGSetCoinTypeState(void) {
	uint8_t nCoinType;
	uint16_t nMask;

	for(nCoinType = 0; nCoinType < ICP_MAX_COINTYPES; nCoinType++) {
		nMask = 1 << nCoinType;
		if(ICPCHGInfo.CoinTypeState.nCoinEnable & nMask) {
			ICPCHGHook_SetCoinEnable(nCoinType, true);
		} else ICPCHGHook_SetCoinEnable(nCoinType, false);
		if(ICPCHGInfo.CoinTypeState.nManDispenseEnable & nMask) {
			ICPCHGHook_SetManDispenseEnable(nCoinType, true);
		} else ICPCHGHook_SetManDispenseEnable(nCoinType, false);
	}
}

bool ICPProt_CHGIsPayoutEnable(void) {
#if kICPPatchRPSolutions_NoRxPayoutOpt
	return (_IcpChgLevel_ >= 3) ? true : false;
#endif
	return (ICPCHGInfo.nFOptions & ICP_CHGOPT3_PAYOUT) ? true : false;
}

bool ICPProt_CHGIsExtDiagEnable(void) {
	return (ICPCHGInfo.nFOptions & ICP_CHGOPT3_EXTDIAG) ? true : false;
}

bool ICPProt_CHGIsManFillEnable(void) {
	return (ICPCHGInfo.nFOptions & ICP_CHGOPT3_MANFILL) ? true : false;
}
#endif

#endif //(_IcpSlave_ & ICP_CHANGER)
