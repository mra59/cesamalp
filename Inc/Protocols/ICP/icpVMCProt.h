
#ifndef __icpVMCProt__
#define __icpVMCProt__

#include "icpProt.h"
#include "icpVMC.h"


#pragma pack(1)


// Master commands
typedef enum  {
	ICP_VMCCMD_NONE = 0,
	// Changer
	ICP_VMCCMD_CHG_RESET,					// 0x01
	ICP_VMCCMD_CHG_TUBESTATUS,
	ICP_VMCCMD_CHG_ENABLE,
	ICP_VMCCMD_CHG_ENABLECOININTUBE,
	ICP_VMCCMD_CHG_INHIBIT,
	ICP_VMCCMD_CHG_DISPENSE,
	ICP_VMCCMD_CHG_PAYOUT2,
	ICP_VMCCMD_CHG_PAYOUT3,
	ICP_VMCCMD_CHG_DIAG,					// 0x09
//	ICP_VMCCMD_CHG_FILL,
	// Bill Validator
	ICP_VMCCMD_BILL_RESET,					// 0x0a
	ICP_VMCCMD_BILL_SECURITY,
	ICP_VMCCMD_BILL_ENABLE,
	ICP_VMCCMD_BILL_INHIBIT,
	ICP_VMCCMD_BILL_ESCROW,
	ICP_VMCCMD_BILL_STACKER,
	ICP_VMCCMD_BILL_DIAG,					// 0x10
	// CPC
	ICP_VMCCMD_CPC_RESET,					// 0x11
	ICP_VMCCMD_CPC_DISABLE,
	ICP_VMCCMD_CPC_ENABLE,
	ICP_VMCCMD_CPC_CANCEL,
	ICP_VMCCMD_CPC_VENDREQ,					// 0x15
	ICP_VMCCMD_CPC_NEGVENDREQ,
	ICP_VMCCMD_CPC_VENDSUCC,
	ICP_VMCCMD_CPC_VENDFAIL,
	ICP_VMCCMD_CPC_SESSCOMP,
	ICP_VMCCMD_CPC_VENDCANCEL,
	ICP_VMCCMD_CPC_CASHSALE,
	ICP_VMCCMD_CPC_REVALUELIMIT,
	ICP_VMCCMD_CPC_REVALUEREQ,
	ICP_VMCCMD_CPC_DATETIME,
	ICP_VMCCMD_CPC_DIAG,					// 0x1f
	// USD
	ICP_VMCCMD_USD_RESET,					// 0x20
	ICP_VMCCMD_USD_DISABLE,
	ICP_VMCCMD_USD_ENABLE,
	ICP_VMCCMD_USD_VENDREQ,
	ICP_VMCCMD_USD_VENDAPPROVED,
	ICP_VMCCMD_USD_VENDDENIED,				// 0x25
	ICP_VMCCMD_USD_VENDFAIL,				// 0x26
	ICP_VMCCMD_USD_VENDSUCC,				// 0x27
	ICP_VMCCMD_USD_VENDSEL,					// 0x28
	ICP_VMCCMD_USD_VENDHOMESEL,				// 0x29
	ICP_VMCCMD_USD_VENDSELSTATUS,			// 0x2a
	ICP_VMCCMD_USD_FUNDSCREDIT,				// 0x2b
	ICP_VMCCMD_USD_FUNDSSELPRICE,			// 0x2c
	ICP_VMCCMD_USD_DIAG						// 0x2d
} ET_VMCCMDS;

typedef enum  {
	ICP_VMCCMDSTATE_JUSTRESET = 0,
	ICP_VMCCMDSTATE_REJECTED = 1,
	ICP_VMCCMDSTATE_INQUEUE = 2,
	ICP_VMCCMDSTATE_RUNNING = 3,
	// Changer
	ICP_VMCCMDSTATE_CHG_INITDONE,
	ICP_VMCCMDSTATE_CHG_COINTYPEDONE,
	ICP_VMCCMDSTATE_CHG_PAYOUT2DONE,
	ICP_VMCCMDSTATE_CHG_PAYOUT3DONE,
	ICP_VMCCMDSTATE_CHG_DIAGDONE,
//	ICP_VMCCMDSTATE_CHG_FILLDONE,
	// Bill Validator
	ICP_VMCCMDSTATE_BILL_INITDONE,
	ICP_VMCCMDSTATE_BILL_BILLTYPEDONE,
	ICP_VMCCMDSTATE_BILL_ESCROWDONE,
	ICP_VMCCMDSTATE_BILL_DIAGDONE,
	// CPC
	ICP_VMCCMDSTATE_CPC_INITDONE,
	ICP_VMCCMDSTATE_CPC_DISABLED,
	ICP_VMCCMDSTATE_CPC_ENABLED,
	ICP_VMCCMDSTATE_CPC_ENDSESSION,				// 0x10
	ICP_VMCCMDSTATE_CPC_CANCELLED,
	ICP_VMCCMDSTATE_CPC_CASHSALEDONE,
	ICP_VMCCMDSTATE_CPC_VENDDENY,
	ICP_VMCCMDSTATE_CPC_VENDAPP,
	ICP_VMCCMDSTATE_CPC_VENDSUCC,
	ICP_VMCCMDSTATE_CPC_VENDFAIL,
	ICP_VMCCMDSTATE_CPC_REVALUELIMIT,
	ICP_VMCCMDSTATE_CPC_REVALUEDENY,
	ICP_VMCCMDSTATE_CPC_REVALUEAPP,
	ICP_VMCCMDSTATE_CPC_DIAGDONE,				// 0x1a
	// USD
	ICP_VMCCMDSTATE_USD_INITDONE,				// 0x1b
	ICP_VMCCMDSTATE_USD_DISABLED,
	ICP_VMCCMDSTATE_USD_ENABLED,
	ICP_VMCCMDSTATE_USD_VENDAPPROVED,
	ICP_VMCCMDSTATE_USD_VENDDENIED,
	ICP_VMCCMDSTATE_USD_VENDSEL,				// 0x20
	ICP_VMCCMDSTATE_USD_VENDHOMESEL,
	ICP_VMCCMDSTATE_USD_VENDSELSTATUS,
	ICP_VMCCMDSTATE_USD_VENDSUCC,
	ICP_VMCCMDSTATE_USD_VENDFAIL,
	ICP_VMCCMDSTATE_USD_FUNDSCREDIT,			// 0x25
	ICP_VMCCMDSTATE_USD_FUNDSSELPRICE,
	ICP_VMCCMDSTATE_USD_DIAGDONE				// 0x27
} ET_VMCCMDSTATES;



//-------------------------------------------------
// V M C  Info
//-------------------------------------------------

typedef struct {
	uint8_t nFLevel;
	uint16_t nUSF;
	uint8_t nDPP;
	uint8_t nMaxTmApproveDeny;
} T_VMC_SETUP;

typedef struct {
	uint8_t nFLevel;
	uint8_t nDispCol;
	uint8_t nDispRow;
	uint8_t nDispType;
} T_VMC_SETUP00;

typedef struct {
	CPCCreditType_L1 nMaxPrice;
	CPCCreditType_L1 nMinPrice;
} T_VMC_SETUP01_L1;

typedef struct {
	CPCCreditType_L3 nMaxPrice;
	CPCCreditType_L3 nMinPrice;
	uint16_t nCurrencyCode;
} T_VMC_SETUP01_L3;

typedef struct {
	CPCCreditType_L1 nPrice;
	uint16_t nSelNumber;
} T_VMC_VEND00_L1;

typedef struct {
	CPCCreditType_L3 nPrice;
	uint16_t nSelNumber;
	uint16_t nCurrencyCode;
} T_VMC_VEND00_L3;

typedef struct {
	CPCCreditType_L1 nRevalue;
} T_VMC_REVAL00_L1;

typedef struct {
	CPCCreditType_L3 nRevalue;
	uint16_t nCurrencyCode;
} T_VMC_REVAL00_L3;

typedef struct {
	uint8_t aManufCode[3];
	uint8_t aSerialNumber[12];
	uint8_t aModelNumber[12];
	uint16_t nSwVersion;
} T_VMC_EXP00_L1;

typedef struct {
	uint8_t aManufCode[3];
	uint8_t aSerialNumber[12];
	uint8_t aModelNumber[12];
	uint16_t nSwVersion;
	uint32_t nFOptions;
} T_VMC_EXP00_L3;

typedef struct {
	uint8_t nYears;
	uint8_t nMonths;
	uint8_t nDays;
	uint8_t nHours;
	uint8_t nMinutes;
	uint8_t nSeconds;
	uint8_t nDayOfWeek;
	uint8_t nWeekNumber;
	uint8_t nSummertime;
	uint8_t nHoliday;
} T_VMC_EXP03;



ET_VMCCMDS ICPProt_VMCGetCmd(uint8_t nSlaveType);
uint8_t *ICPProt_VMCGetPar(uint8_t nSlaveType);

bool ICPProt_VMCIsCmdBufferBusy(uint8_t nSlaveType);
void ICPProt_VMCCmdRejected(uint8_t nSlaveType);
void ICPProt_VMCCmdDone(uint8_t nSlaveType, uint8_t nCmdResult);

ET_VMCCMDS ICPProt_VMCCheckExactChange(void);
ET_VMCCMDS ICPProt_VMCCheckTubesFull(void);
ET_VMCCMDS ICPProt_VMCCheckStackerFull(void);
ET_VMCCMDS ICPProt_VMCCheckEscrowReturn(void);
bool ICPProt_VMCDispense(void);
bool ICPProt_VMCTxDispense(void);
void ICPProt_VMCEndDispense(void);


void ICPProt_VMCGetSetupInfo(T_VMC_SETUP00 *ptInfo);
void ICPProt_VMCUsdGetSetupInfo(T_VMC_SETUP *ptInfo);



//@
/*
void ICPVMCHook_GetManufactureCode(uint8_t *ptData, uint8_t nDataLen);
void ICPVMCHook_GetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPVMCHook_GetModelNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPVMCHook_GetSwVersion(uint16_t *ptSwVersion);
void ICPVMCHook_GetDate(uint8_t *ptYears, uint8_t *ptMonths, uint8_t *ptDays);
void ICPVMCHook_GetTime(uint8_t *ptHours, uint8_t *ptMinutes, uint8_t *ptSeconds);
void ICPVMCHook_GetDateInfo(uint8_t *ptDayOfWeek, uint8_t *ptWeekNumber, uint8_t *ptSummertime, uint8_t *ptHoliday);
uint16_t ICPVMCHook_GetCoinEnableMask(void);
uint16_t ICPVMCHook_GetBillEnableMask(void);
bool ICPVMCHook_GetMultiVend(void);
bool ICPVMCHook_GetSlaveUSD(void);
CreditCpcValue ICPVMCHook_GetMaxPrice(void);
CreditCpcValue ICPVMCHook_GetMinPrice(void);

void ICPVMCHook_DisplayMsg(uint8_t *ptMsg, uint8_t nMsgLen, uint16_t nMsecDisplayTime);

//void ICPProt_VMCGetExpID(T_VMC_EXP00_L3 *ptInfo);
*/
//@

#endif
