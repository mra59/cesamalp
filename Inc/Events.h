/****************************************************************************************
File:
    Events.h

Description:
    Include file con definizioni e strutture per il file Events.

History:
    Date       Aut  Note
    Apr 2019	MR   

 *****************************************************************************************/

#ifndef __Events_H
#define __Events_H

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *uart);
void IRQ_Tick_Timer(void);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
#if IRDA_PRESENTE
	void HAL_IRDA_RxCpltCallback(IRDA_HandleTypeDef *hirda1);
#endif


/*
void PIT0_OnCounterRestart(LDD_TUserData *UserDataPtr);
void TWRSER3_OnBlockReceived(LDD_TUserData *UserDataPtr);
void TWRSER3_OnBlockSent(LDD_TUserData *UserDataPtr);
void TWRSER3_OnTxComplete(LDD_TUserData *UserDataPtr);
void TWRSER4_OnBlockReceived(LDD_TUserData *UserDataPtr);
void TWRSER4_OnBlockSent(LDD_TUserData *UserDataPtr);
void TWRSER4_OnError(LDD_TUserData *UserDataPtr);
void TWRSER4_OnTxComplete(LDD_TUserData *UserDataPtr);
void TWRSER1_OnBlockReceived(LDD_TUserData *UserDataPtr);
void TWRSER1_OnError(LDD_TUserData *UserDataPtr);
void TWRSER1_OnTxComplete(LDD_TUserData *UserDataPtr);
void TWRSER4_OnBreak(LDD_TUserData *UserDataPtr);
void TWRSER1_OnBlockSent(LDD_TUserData *UserDataPtr);
void DDCMP_SCI_OnBlockReceived(LDD_TUserData *UserDataPtr);
void DDCMP_SCI_OnBlockSent(LDD_TUserData *UserDataPtr);
void DDCMP_SCI_OnError(LDD_TUserData *UserDataPtr);
void DDCMP_SCI_OnTxComplete(LDD_TUserData *UserDataPtr);
void Cpu_OnNMIINT0(void);
void AtoD1_OnMeasurementComplete(LDD_TUserData *UserDataPtr);
void Cpu_OnNMIINT(void);
void SCI_EXECUTIVE_OnBlockReceived(LDD_TUserData *UserDataPtr);
void SCI_EXECUTIVE_OnBlockSent(LDD_TUserData *UserDataPtr);
void SCI_EXECUTIVE_OnError(LDD_TUserData *UserDataPtr);
void SCI_EXECUTIVE_OnTxComplete(LDD_TUserData *UserDataPtr);
void SCI_EXECUTIVE_OnBreak(LDD_TUserData *UserDataPtr);
void PortC_OnPortEvent(LDD_TUserData *UserDataPtr);
void SPI0_OnBlockSent(LDD_TUserData *UserDataPtr);
void SPI0_OnBlockReceived(LDD_TUserData *UserDataPtr);
void GP_I2C_OnMasterBlockSent(LDD_TUserData *UserDataPtr);
void GP_I2C_OnMasterBlockReceived(LDD_TUserData *UserDataPtr);
void SPI1_OnBlockSent(LDD_TUserData *UserDataPtr);
void SPI1_OnBlockReceived(LDD_TUserData *UserDataPtr);
void SPI1_OnError(LDD_TUserData *UserDataPtr);
void TWRSER3_OnError(LDD_TUserData *UserDataPtr);
void GP_I2C_OnError(LDD_TUserData *UserDataPtr);
*/

#endif /* __Events_H*/

