/**
  ******************************************************************************
  * @file    USB_Host/MSC_Standalone/Src/explorer.c 
  * @author  MCD Application Team
  * @brief   Explore the USB flash disk content
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license SLA0044,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        http://www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "explorer.h"

extern USBH_HandleTypeDef hUsbHostFS;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/  
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Displays disk content.
  * @param  path: Pointer to root path
  * @param  recu_level: Disk content level 
  * @retval Operation result
  */
uint8_t day_of_month,month;
uint16_t year;
uint8_t seconds, minutes, hours;

static bool FAT_dt_convert(WORD fat_date, WORD fat_time)
{
  /*
  Converts a FAT date and time into a Python datetime object.
  FAT date time is mainly used in DOS/Windows file formats and FAT.
  The FAT date and time is a 32-bit value containing two 16-bit values:
    * The date (lower 16-bit).
      * bits 0 - 4:  day of month, where 1 represents the first day
      * bits 5 - 8:  month of year, where 1 represent January
      * bits 9 - 15: year since 1980
    * The time of day (upper 16-bit).
      * bits 0 - 4: seconds (in 2 second intervals)
      * bits 5 - 10: minutes
      * bits 11 - 15: hours
  */
  
  day_of_month = (fat_date & 0x1f);
  month = ((fat_date >> 5) & 0x0f);
  year = ((fat_date >> 9) & 0x7f) +1980;

  if ((day_of_month > 30) || (month > 11))
    return false;

  seconds = (fat_time & 0x1f) * 2;
  minutes = (fat_time >> 5) & 0x3f;
  hours = (fat_time >> 11) & 0x1f;

  if ((hours > 23) || (minutes > 59) || (seconds > 59))
    return false;

  return true;
  
}




FRESULT Explore_Disk(char *path, uint8_t recu_level)
{
  FRESULT res = FR_OK;
  FILINFO fno;
  DIR dir;
  char *fn;

  res = f_opendir(&dir, path);
  if(res == FR_OK) 
  {
    while(USBH_MSC_IsReady(&hUsbHostFS))
    {
      res = f_readdir(&dir, &fno);
      if(res != FR_OK || fno.fname[0] == 0) 
      {
        break;
      }
      if(fno.fname[0] == '.')
      {
        continue;
      }
      
 
      fn = fno.fname;
      FAT_dt_convert(fno.fdate, fno.ftime);
      //strcpy(tmp, fn); 
      
      if((fno.fattrib & AM_DIR) == AM_DIR)
      {
        //strcat(tmp, "\n"); 
        Explore_Disk(fn, 2);
      }
      
      if(((fno.fattrib & AM_DIR) == AM_DIR)&&(recu_level == 2))
      {
        Explore_Disk(fn, 2);
      }
    }
    f_closedir(&dir);
  }
  return res;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
