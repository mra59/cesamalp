/****************************************************************************************
File:
    icpChg.h

Description:
    Include file protocollo MDB per bill changer

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __icpCHG__
#define __icpCHG__

#include "Icp.h"


// ExpFF per Vmc Restomat
#ifndef kModifMDB_RestomatExpFFCHG
#define kModifMDB_RestomatExpFFCHG	false
#endif



/*--------------------------------------------------------------------------------------*\
Types and constants
\*--------------------------------------------------------------------------------------*/

#define ICP_MAX_COINTYPES	16

typedef uint8_t ICPCoinType;

// Feature options
typedef enum  {
	ICP_CHGOPT_NONE 	= 0x00,
	ICP_CHGOPT3_PAYOUT 	= 0x01,
	ICP_CHGOPT3_EXTDIAG = 0x02,
	ICP_CHGOPT3_MANFILL = 0x04,
	ICP_CHGOPT3_FTL 	= 0x08
} ET_CHGFOPTIONS;

// Status events
typedef enum {
	ICP_CHG_ESCROWREQ = 1,
	ICP_CHG_PAYOUTBUSY,
	ICP_CHG_NOCREDIT,
	ICP_CHG_DEFECTTUBESENSOR,
	ICP_CHG_DOUBLEARRIVAL,
	ICP_CHG_ACCEPTORUNPLUGGED,
	ICP_CHG_TUBEJAM,
	ICP_CHG_ROMCHKERROR,
	ICP_CHG_COINROUTINGERROR,
	ICP_CHG_BUSY,
	ICP_CHG_JUSTRESET,
	ICP_CHG_COINJAM
} ET_CHGPOLLSTATUS;

// Coin destinations
typedef enum {
	ICP_CHG_CASHBOX = 0,
	ICP_CHG_TUBES,
	ICP_CHG_NOTUSED,
	ICP_CHG_REJECT
} ET_CHGPOLLCOINDEST;

//Expansion status
typedef enum {
	CHG_NONE = 0,
	CHG_OK,
	CHG_KEY_SHIFTED,
	CHG_MANUAL_FILL,
	CHG_NEW_INVENTORY
} ET_CHGEXP05;



/*--------------------------------------------------------------------------------------*\
Interface routines
\*--------------------------------------------------------------------------------------*/

void ICPProt_CHGSetStatus(ET_CHGPOLLSTATUS nStatusType);
	// Chiamate per indicare degli eventi di stato per ICP.
	// NB: gli eventi ICP_CHG_PAYOUTBUSY e ICP_CHG_BUSY sono generati automaticamente
	// da ICP e quindi non vanno utilizzati in questa chiamata

void ICPProt_CHGSetSlug(uint8_t nSlugs);
	// Chiamata per indicare a ICP che sono stati rilevati uno o pi� slugs

void ICPProt_CHGSetCoinsDeposited(uint8_t nCoinType, ET_CHGPOLLCOINDEST nDestType);
	// Chiamata per indicare che una moneta del tipo specificato � stata introdotta
	// nDestType specifica la destinazione della moneta.

void ICPProt_CHGSetCoinsDispMan(uint8_t nCoinType, uint8_t nCoins);
	// Chiamata per indicare che una moneta del tipo specificato � stata restituita manualmente

//void ICPProt_CHGSetCoinPaidOut(uint8_t nCoinType);
	// Chiamata durante l'operazione di pay-out per indicare che una moneta
	// del tipo specificato � stata restituita

bool ICPCHGIsInitCompleted(bool *initInProgess);
	// Chiamata per verificare se l'inizializzazione � stata completeta
	
uint16_t ICPProt_CHGGetCoinValue(uint8_t ix);
uint16_t 	 GetCoinsInTube(uint8_t ic);
uint8_t 	 ICPProt_CHGGet_USF(void);

void CHGInit(uint8_t nSlaveType);
void CHGSlaveResp(void);
void CHGInactive(void);
void CHGDisabled(void);
void CHGEnabled(void);
void CHGPayout(void);
void ICPCHGHook_GetSetupInfo(void);
void ICPProt_CHGReset(void);
uint8_t ICPProt_CHGResetResp(void);
void ICPProt_CHGStatus(void);
uint8_t ICPProt_CHGStatusResp(void);
void ICPProt_CHGTubeStatus(void);
uint8_t ICPProt_CHGTubeStatusResp(void);
void ICPProt_CHGPoll(void);
uint8_t ICPProt_CHGPollResp(void);
void ICPProt_CHGCoinType(void);
uint8_t ICPProt_CHGCoinTypeResp(void);
void ICPProt_CHGDispense(void);
uint8_t ICPProt_CHGDispenseResp(void);
void ICPProt_CHGExp00(void);
uint8_t ICPProt_CHGExp00Resp(void);
void ICPProt_CHGExp01(void);
uint8_t ICPProt_CHGExp01Resp(void);
void ICPProt_CHGExp02(void);
uint8_t ICPProt_CHGExp02Resp(void);
void ICPProt_CHGExp03(void);
uint8_t ICPProt_CHGExp03Resp(void);
void ICPProt_CHGExp04(void);
void ICPProt_CHGExp05(void);
void ICPProt_CHGExp06(void);
void ICPProt_CHGExp07(void);
uint8_t ICPProt_CHGExp07Resp(void);
void ICPProt_CHGExpFF(void);
uint8_t ICPProt_CHGExpFFResp(void);
uint8_t ICPProt_CHGExpFAResp(void);
void ICPProt_CHGExpFB(void);
uint8_t ICPProt_CHGExpFBResp(void);
void ICPProt_CHGExpFC(void);
uint8_t ICPProt_CHGExpFCResp(void);
void ICPProt_CHGExpFD(void);
uint8_t ICPProt_CHGExpFDResp(void);
void ICPProt_CHGExpFE(void);
uint8_t ICPProt_CHGExpFEResp(void);
bool ICPProt_CHGGetPayout(void);
bool ICPProt_CHGGetExtDiag(void);
bool ICPProt_CHGGetManualFill(void);
bool ICPProt_CHGGetFTL(void);
bool ICPProt_CHGGetReady(void);
bool ICPProt_CHGGet_exp_state(void);
void ICPProt_CHGRes_exp_state(void);
uint16_t GetnCoinTypeInTubes(void);
uint16_t GetnCoinTypeEnabled(void);
uint16_t GetCoinsTubeFull(void);
void SetCHGInfoExChange(void);
void ClrCHGInfoExChange(void);
void UpdateCHGInfoKeyState(uint8_t KeyState);
void Set_Chg_Id_Data(uint8_t ix, uint8_t l, uint8_t *point);
void ICPProt_CHGClearPollData(void);
void ICPProt_CHGResetPollData(void);
uint8_t ICPProt_CHGGetPollData(uint8_t *ptData, bool fAllData);
void ICPProt_CHGSetCoinTypeState(void);
bool ICPProt_CHGIsPayoutEnable(void);
bool ICPProt_CHGIsExtDiagEnable(void);
bool ICPProt_CHGIsManFillEnable(void);
void Set_Chg_SwRev_Data(uint16_t Chng_Rev);



/*
//MR19 Esiste solo se gettoniera slave
//void ICPCHGHook_Init(void);
	// Chiamata quando viene ricevuto un reset da master.
	// Disabilita accettazione di tutte le monete, abilita l'espulsione manuale di tutte
	// le monete (Ref1p5.7), e disabilita tutte le funzionalit� opzionali (Ref1p5.9)

//MR19 Esiste solo se gettoniera slave
//void ICPCHGHook_PayoutVal(CreditCoinValue nPayoutVal);
	// Richiesta inizio operazione di pay out. Specifica l'ammontare del resto
	// da restituire

//MR19 Esiste solo se gettoniera slave
//void ICPCHGHook_DispenseCoins(uint8_t nCoinType, uint8_t nCoins);
	// Richiede la restituzione di nCoins monete di tipo nCoinType
	// NB: se due coin-type hanno lo stesso valore, vanno restituite prima
	// le monete con coin-type pi� alto (Ref1p5.7)


//MR19 Non esiste in questo file
//bool ICPCHGHook_IsAvailable(void);
	// Return true if Changer peripheral is used

//MR19 Non esiste in questo file
//bool ICPCHGHook_IsInitCompleted(void);
	// Ritorna true alla fine della fase di inizializzazione

//MR19 Non esiste in questo file
//bool ICPCHGHook_IsPayoutBusy(void);
	// Verifica se operazione di payout in corso

//MR19 Non esiste in questo file
//bool ICPCHGHook_IsAcceptorUnplugged(void);
	// Verifica se selettore scollegato

//MR19 Non esiste in questo file
//bool ICPCHGHook_IsCoinTypeInTube(uint8_t nCoinType);
	// Ritorna true se il tipo moneta specificato va in un tubo

//MR19 Non esiste in questo file
//bool ICPCHGHook_IsCoinTypeTubeFull(uint8_t nCoinType);
	// Ritorna true se il tubo contenente il tipo moneta specificato � pieno

//MR19 Non esiste in questo file
//bool ICPCHGHook_IsPayoutCompleted(void);
	// Ritorna true se l'esecuzione dell'ultima richiesta di PayoutVal() � terminata

//MR19 Non esiste in questo file
//uint8_t ICPCHGHook_GetCoinsInTube(uint8_t nCoinType);
	// Ritorna il numero di monete presenti nel tubo associato al tipo moneta specificato

//MR19 Non esiste in questo file
//CreditCoinValue ICPCHGHook_GetCoinTypeVal(uint8_t nCoinType);
	// Ritorna il valore del tipo moneta specificato. 
	// Ritorna 0 se il tipo moneta non � utilizzato

//MR19 Non esiste in questo file
//uint8_t ICPCHGHook_GetCoinsPaidOut(uint8_t nCoinType);
	// Ritorna quante monete del tipo moneta specificato sono state restituite
	// nell'operazione di pay-out

//MR19 Non esiste in questo file
//CreditCoinValue ICPCHGHook_GetPaidOutVal(void);
	// Ritorna resto dato durante l'operazione di pay-out dalla
	// precedente chiamata a questa routine.

//MR19 Non esiste in questo file
//uint8_t ICPCHGHook_GetScalingFactor(void);
	// Ritorna il fattore di scala da utilizzare per la rappresentazione del credito

//MR19 Non esiste in questo file
//uint8_t ICPCHGHook_GetDecimalPoint(void);
	// Ritorna il punto decimale da usare per la rappresentazione del credito

//MR19 Non esiste in questo file
//uint8_t ICPCHGHook_GetFeatureLevel(void);
	// Return ICP feature level. Programmabile o fisso??

//MR19 Non esiste in questo file
//uint16_t ICPCHGHook_GetCountryCode(void);
	// Return country code

//MR19 Non esiste in questo file
//ET_CHGFOPTIONS ICPCHGHook_GetFeatures(void);
	// Ritorna le funzionalit� supportate dal rendiresto

//MR19 Non esiste in questo file
//void ICPCHGHook_SetCoinEnable(uint8_t nCoinType, bool fEnable);
	// Abilita/disabilita l'accettazione del tipo moneta specificato

//MR19 Non esiste in questo file
//void ICPCHGHook_SetManDispenseEnable(uint8_t nCoinType, bool fEnable);
	// Abilita/disabilita l'espulsione manuale del tipo moneta specificato


// ExpFF per Vmc Restomat
//void ICPCHGHook_SetCoinDstCashBox(uint16_t dstCashBox);
	// "dstCashBox" indica quali coin-type destinati nei tubi devono essere rediretti in cassa
	// bit a 1 --> manda moneta in cassa, bit a 0 --> destinazione di programmazione originale
*/



#endif
