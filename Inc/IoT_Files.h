/****************************************************************************************
File:
    IoT_Files.h

Description:
    Include file con definizioni e strutture per la comunicazione con Gateway ST.

History:
    Date       Aut  Note
    Dic 2018	MR   

 *****************************************************************************************/

#ifndef __IoT_Files_H
#define __IoT_Files_H


/*--------------------------------------------------------------------------------------*\
Private Declarations 
\*--------------------------------------------------------------------------------------*/

#define	TRANSAC_ID_LEN					36					// Massimo numero di chr che costituiscono il TransactionIDValue
	
//#define TX_AWS_DATA_BUFFER_SIZE		(1024)					// 1K bytes
#define TX_AWS_DATA_BUFFER_SIZE			512
#define	AUDIT_LIST						1

#define	TSTAMP_OFFSET					946684800			//this is the Unix timestamp offset for 00:00:00 01-01-2000
#define	STATUS_ID_LEN					128

/*--------------------------------------------------------------------------------------*\
Functions Prototypes 
\*--------------------------------------------------------------------------------------*/

void  			Response_AWS_OK_Rejected(uint8_t AnswerType, uint8_t CMD_Code);
void  			Prepare_MachineStatus_Buffer(void);
void  			Prepare_AuditBuffer(void);
void 			get_time_stamp(uint32_t* ts, uint16_t* ms);
void 			get_time_stamp_string(char* tstring);
bool 			Export_Audit_to_AWS(void);
bool 			Export_Config_to_AWS(void);
void  			Prepare_Data_Buffer(char *DataBuffer, uint16_t Size, uint8_t BlockNum, uint8_t Command);
void  			RemoteErogazAnswer(uint8_t ErrorCode);

/*--------------------------------------------------------------------------------------*\
Public Declarations 
\*--------------------------------------------------------------------------------------*/

extern	char 		TransactionIDValue[TRANSAC_ID_LEN];
extern	char 		CopyOfTransID[TRANSAC_ID_LEN];
extern	uint8_t		GTW_command;
extern	uint16_t 	ts_milliseconds;



#endif		// __IoT_Files_H


