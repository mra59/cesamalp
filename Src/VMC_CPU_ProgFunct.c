/****************************************************************************************
File:
    VMC_CPU_ProgFunct.c

Description:
    File con funzioni di elaborazioni dati di programmazione VMC tramite tastiera
	e display LCD.
    

History:
    Date       Aut  Note
    Set 2016	MR   

*****************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>


#include "ORION.H"
#include "VMC_CPU_Prog.h"
#include "IICEEP.h"
#include "VMC_CPU_HW.h"
#include "VMC_CPU_LCD.h"
#include "VMC_CPU_ProgFunct.h"
#include "I2C_EEPROM.h"
#include "Icp.h"
#include "General_utility.h"
#include "Funzioni.h"
#include "VMC_CPU_Tasti.h"
#include "OrionTimers.h"
#include "RTC_utility.h"
#include "VMC_CPU_Motori.h"

#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void 	ReadMessage(char *pnt, uint8_t lang, uint8_t mess, uint8_t msgLen);
extern	void	LeggiOrologio(void);
extern	const char sLocalizationDefault[];

extern	char	LCD_DataBuff[LCD_DBUF_SIZE];
//extern	char	LegalTime(uint32_t iYear, uint32_t iMonth, uint32_t iDay, uint32_t iHour, uint32_t iDayW);




/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/
tProgBool		sBool;								// Struttura per elaborare un boolean
tProgHex8		sHex8;								// Struttura per elaborare un Hex a 8 bit (0-256)
tProgHex16		sHex16;								// Struttura per elaborare un Hex a 16 bit (0-65535)
tProgHex32		sHex32;								// Struttura per elaborare un Hex a 32 bit (0-4.294.967.296)
tProgBCD		sBCD;								// Struttura per elaborare un BCD (0-99)

static	uint8_t	oldtasto;
		bool	LastInputData;


/*------------------------------------------------------------------------------------------*\
 Method: Prog_Opzioni_Selezioni
 	 Imposta l'opzione passata come parametro della selezione desiderata.
	
   IN:  - numero selezione
  OUT:  - true se premuto tasto '-' per terminare la programmazione di questa selezione
\*------------------------------------------------------------------------------------------*/
bool  Prog_Opzioni_Selezioni(uint8_t numeroselez, uint8_t DisplayMessageNum, uint8_t OptionBit)
{
	sBool.Msg 		= DisplayMessageNum;
	sBool.Row		= Row_4;
	sBool.MaskData	= OptionBit;
	sBool.AddrData 	= EEPConfVMCAddr(EE_SelOptA[numeroselez]);
	Elab_Boolean(&sBool);
	if (Tasto == Tasto_Meno) return true;																		// Terminare questa programmazione 
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_Numero_ASCII
 	 Imposta una cifra espressa come chr ASCII.
 	 La visualizzazione avviene richiamando il msg passato come parametro sulla riga 4 del
 	 display LCD.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
bool  Prog_Numero_ASCII(uint8_t msgnum, uint16_t EEaddress, uint16_t ParLen) {

#define			RigaParam		Row_4
	bool		endElab = false;
	uint8_t		offsetRow;
	uint16_t	j;
	char		localBuff[24];
	char		*StrPnt;
	
	ReadEEPI2C(EEaddress, (uint8_t *)(&localBuff[0]), ParLen);																	// Leggo Parametro ASCII in RAM
	StrPnt = &localBuff[0];
	do {
		VisMessage(msgnum, RigaParam, 0, 0, false);																			// Visualizza messaggio sulle riga richiesta senza cancellare le altre
		offsetRow = RicercaPosizione(RigaParam, kLocLCDCifraIntera);
		strncpy(&LCD_DataBuff[offsetRow], &localBuff[0], ParLen);
		ZeroToSpaces(&LCD_DataBuff[offsetRow], ParLen);																		// Tolgo gli zeri non significativi dal buffer display
		Refresh_LCD();																									// Visualizza valore

		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {	Warning[Pe186]: pointless comparison of unsigned integer with zero
		if ((Tasto <= Tasto_Nove)) {
			for (j=1; j<ParLen; j++) {																						// Shift left stringa ASCII
				*(StrPnt+(j-1)) = *(StrPnt+j);
			}
			*(StrPnt+(ParLen-1)) = (Tasto+'0');																				// Tasto nella stringa as LSB
			WriteEEPI2C(EEaddress, (uint8_t *)(&localBuff[0]), ParLen);														// Nuovo valore in EE
		} else {
			endElab = true;																									// Richiesta uscita funzione
		}
	} while (endElab == false);
	if (Tasto == Tasto_Meno) return true;																					// Terminare questa programmazione
	return false;
}	
	
/*------------------------------------------------------------------------------------------*\
 Method: Prog_Fascia
 	 Imposta le 4 fasce orarie del giorno selezionato.
 	 La riga 1 visualizza il menu.
 	 La riga 2 visualizza il tipo di dato:  HH:MM inizio, HH:MM fine
 	 La riga 3 visualizza i valori di data e ora
 	 La riga 4 visualizza una freccia che indica qual'e' il dato in programmazione.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
bool Prog_Fasce(uint8_t TipoFascia, uint8_t weekday, uint8_t numfascia) {
	
	uint8_t		j, offsetRow, arrowPnt, paramVal, OraON, OraOFF, MinON, MinOFF, MaxData, MinData;
	uint16_t	AddrParam, ParamON, ParamOFF;
	char		buffer[12];

#define	arrowPnt_Ore_Start	2
#define	arrowPnt_Min_Start	5
#define	arrowPnt_Ore_Stop	14
#define	arrowPnt_Min_Stop	17
	
	arrowPnt = arrowPnt_Ore_Start;
	//j = InizioFasce + ((TipoFascia * FasciaSettimana_Len) + ((DataOra.DayOfWeek-1) * (sizeof(SizeEE_FA_Array) * 4) + ((numfascia-1) * 2)));					// Pointer alla fascia del giorno da elaborare
	j = InizioFasce + ((TipoFascia * FasciaSettimana_Len) + ((weekday-1) * (sizeof(SizeEE_FA_Array) * 4) + ((numfascia-1) * 2)));					// Pointer alla fascia del giorno da elaborare
	AddrParam = EEFasceOrarieAddr(EE_ParamFasc[j]);

	do {
		ReadEEPI2C(AddrParam, (uint8_t *)(&ParamON), sizeof(ParamON));
		HexWordToDec(ParamON);
		OraON = DecHex(DecH);
		MinON = DecHex(DecL);
		ReadEEPI2C(AddrParam+2, (uint8_t *)(&ParamOFF), sizeof(ParamOFF));
		HexWordToDec(ParamOFF);
		OraOFF = DecHex(DecH);
		MinOFF = DecHex(DecL);
		
#if (CESAM == true)
		VisMessage(0, Row_1, mFasciaNum, Row_2, true);											// Nessuna messaggio su riga 1, numero fascia su riga 2
#else		
		VisMessage(mDayOfTheWeek, Row_1, mFasciaNum, Row_2, true);								// Messaggio Giorno della sett. su riga 1, numero fascia su riga 2
		sprintf(buffer,"%u",weekday);
		offsetRow = Riga_1_In_Buffer + (LCD_NUMCOL-1);											// Pointer all'ultima colonna della riga 1
		LCD_DataBuff[offsetRow] = buffer[0];
		Refresh_LCD();
#endif		
		sprintf(buffer,"%u",numfascia);
		offsetRow = Riga_2_In_Buffer + (LCD_NUMCOL-1);											// Pointer all'ultima colonna della riga 2
		LCD_DataBuff[offsetRow] = buffer[0];
		Refresh_LCD();
		sprintf((char*)&TempBuff," %02d %02d   -   %02d %02d  ", OraON, MinON, OraOFF, MinOFF);
		Msg_InAnyRow((char*)&TempBuff, Row_3, false);
		LCD_DataBuff[Riga_4_In_Buffer+arrowPnt] = ASCII_Arrow;									// Visualizzo freccia su riga 4 che punta alla colonna in modifica
		Refresh_LCD();
		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {		Warning[Pe186]: pointless comparison of unsigned integer with zero
		if (Tasto <= Tasto_Nove)
		{
			MinData = 0;
			switch(arrowPnt) {
			
				case arrowPnt_Ore_Start:
					MaxData = 23;
					paramVal = OraON;
					break;

				case arrowPnt_Ore_Stop:
					MaxData = 23;
					paramVal = OraOFF;
					break;
			
				case arrowPnt_Min_Start:
					MaxData = 59;
					paramVal = MinON;
					break;

				case arrowPnt_Min_Stop:
					MaxData = 59;
					paramVal = MinOFF;
					break;
			}
			paramVal = Elab_uint8(paramVal, Tasto, MaxData);									// Nuovo valore
			if (paramVal > MaxData) {
				paramVal = Tasto;
				if (paramVal < MinData) {
					paramVal = MinData;
				}
			}
			switch (arrowPnt) {
				case arrowPnt_Ore_Start:	OraON	= paramVal; break;
				case arrowPnt_Ore_Stop:		OraOFF	= paramVal; break;
				case arrowPnt_Min_Start:	MinON	= paramVal; break;
				case arrowPnt_Min_Stop:		MinOFF	= paramVal; break;
			}
			if ((arrowPnt == arrowPnt_Ore_Start) || (arrowPnt == arrowPnt_Min_Start)) {			// Sto impostando Ore:Min ON
				ParamON = ((OraON *100) + MinON);
				WriteEEPI2C(AddrParam, (uint8_t *)(&ParamON), sizeof(uint16_t));
			} else {																			// Sto impostando Ore:Min OFF
				ParamOFF = ((OraOFF *100) + MinOFF);
				WriteEEPI2C(AddrParam+2, (uint8_t *)(&ParamOFF), sizeof(uint16_t));
			}
		} else if (Tasto == Tasto_Piu) {														// Passare al parametro successivo
			switch (arrowPnt) {
				case arrowPnt_Ore_Start:	arrowPnt = arrowPnt_Min_Start; break;
				case arrowPnt_Min_Start:	arrowPnt = arrowPnt_Ore_Stop; break;
				case arrowPnt_Ore_Stop:		arrowPnt = arrowPnt_Min_Stop; break;
				case arrowPnt_Min_Stop:		arrowPnt = arrowPnt_Ore_Start; break;
				default:					arrowPnt = arrowPnt_Ore_Start; break;
			}
		}
		if (Tasto == Tasto_Meno) return true;													// Terminare questa programmazione 
	} while (true);
}


/*------------------------------------------------------------------------------------------*\
 Method: Prog_Orologio
 	 Imposta data e ora nell'orologio del uP.
 	 La riga 1 visualizza il menu.
 	 La riga 2 visualizza il tipo di dato: D M Y W  H:M
 	 La riga 3 visualizza i valori di data e ora
 	 La riga 4 visualizza una freccia che indica qual'e' il dato in programmazione.
	
   IN:  - 
  OUT:  - true se premuto tasto '-' per terminare la programmazione
\*------------------------------------------------------------------------------------------*/
bool  Prog_Orologio(void)
{
	char		L_flag;
	uint8_t		arrowPnt, paramVal, MinData, MaxData;
	
#define	arrowPnt_Giorno		2
#define	arrowPnt_Mese		5
#define	arrowPnt_Anno		8
#define	arrowPnt_weekday	10
#define	arrowPnt_Ore		14
#define	arrowPnt_Min		17
	
	arrowPnt = arrowPnt_Giorno;
	do {
		LeggiOrologio();																		// Leggo Orologio
		VisMessage(mDataOra, Row_1, mClockTipoDato, Row_2, true);								// Messaggio Data/Ora su riga 1 e Clock Tipo Dati su riga 2
		if (DataOra.Year < MILLENNIO_2000) DataOra.Year = MILLENNIO_2000;						// Anno non corretto: visualizzo 0
		sprintf((char*)&TempBuff," %2d %2d %2d %1d  %02d %02d  ", DataOra.Day, DataOra.Month, (DataOra.Year-MILLENNIO_2000), DataOra.DayOfWeek, DataOra.Hour, DataOra.Minute);
		Msg_InAnyRow((char*)&TempBuff, Row_3, false);
		
		LCD_DataBuff[Riga_4_In_Buffer+arrowPnt] = ASCII_Arrow;									// Visualizzo freccia su riga 4 che punta alla colonna in modifica
		Refresh_LCD();
		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {		Warning[Pe186]: pointless comparison of unsigned integer with zero
		if (Tasto <= Tasto_Nove)
		{
			switch(arrowPnt)
			{
				case arrowPnt_Giorno:
					MinData = 1;
					MaxData = 31;
					paramVal = DataOra.Day;
					break;
				
				case arrowPnt_Mese:
					MinData = 1;
					MaxData = 12;
					paramVal = DataOra.Month;
					break;
			
				case arrowPnt_Anno:
					MinData = 1;
					MaxData = 99;
					if (DataOra.Year < MILLENNIO_2000)											// Anno non corretto: parto da 0
					{
						paramVal = 0;
					}
					else
					{
						paramVal = (DataOra.Year-MILLENNIO_2000);
					}
					break;
			
				case arrowPnt_weekday:
					MinData = 1;
					MaxData = 7;
					paramVal = DataOra.DayOfWeek;
					break;
			
				case arrowPnt_Ore:
					MinData = 0;
					MaxData = 23;
					paramVal = DataOra.Hour;
					break;
			
				case arrowPnt_Min:
					MinData = 0;
					MaxData = 59;
					paramVal = DataOra.Minute;
					break;
			}
			paramVal = Elab_uint8(paramVal, Tasto, MaxData);									// Nuovo valore in paramVal
			if (paramVal > MaxData) {
				paramVal = Tasto;
				if (paramVal < MinData) {
					paramVal = MinData;
				}
			}
			switch (arrowPnt) {
				case arrowPnt_Giorno:	DataOra.Day 		= paramVal; break;
				case arrowPnt_Mese:		DataOra.Month 		= paramVal; break;
				case arrowPnt_Anno:		DataOra.Year 		= (paramVal+MILLENNIO_2000); break;
				case arrowPnt_weekday:	DataOra.DayOfWeek 	= paramVal; break;
				case arrowPnt_Ore:		DataOra.Hour 		= paramVal; break;
				case arrowPnt_Min:		DataOra.Minute 		= paramVal; break;
			}
			DataOra.Second = 0;
			RTC_SetTimeAndDate(&DataOra);														// Scrivo nuovo valore parametro in EE
			//----------------------------------------------------------------------------------------------
			//-- Set flag Legale/Solare in accordo alla data che sto impostando: non si --
			//-- imposta una data di ora Legale se sono in ora Solare e viceversa       --
			L_flag = LegalTime(DataOra.Year, DataOra.Month, DataOra.Day, DataOra.Hour, DataOra.DayOfWeek);
			if (L_flag == ASCII_L)
			{
				//-- Impostata una data in ora Legale --
				if (RTC_Read_BKP() != BKP_LEGALE)
				{
					RTC_Write_BKP(BKP_LEGALE);
				}
			}
			else
			{
				//-- Impostata una data in ora Solare --
				if (RTC_Read_BKP() != BKP_SOLARE)
				{
					RTC_Write_BKP(BKP_SOLARE);
				}
			}
			//----------------------------------------------------------------------------------------------
		}
		else 
		{
			if (Tasto == Tasto_Piu) 															// Passare al parametro successivo
			{
				switch (arrowPnt) {
				  case arrowPnt_Giorno:		arrowPnt = arrowPnt_Mese; break;
				  case arrowPnt_Mese:		arrowPnt = arrowPnt_Anno; break;
				  case arrowPnt_Anno:		arrowPnt = arrowPnt_weekday; break;
				  case arrowPnt_weekday:	arrowPnt = arrowPnt_Ore; break;
				  case arrowPnt_Ore:		arrowPnt = arrowPnt_Min; break;
				  case arrowPnt_Min:		arrowPnt = arrowPnt_Giorno; break;
				  default:					arrowPnt = arrowPnt_Giorno; break;
				}
			}
		}
		if (Tasto == Tasto_Meno) return true;													// Terminare questa programmazione 
	} while (true);
}

/*------------------------------------------------------------------------------------------*\
 Method: Visualizza_Valore
 	 Visualizza il dato passato nel parametro sulla riga desiderata.
 	 Il dato puo' andare dal byte all'uint32_t, valuta o solo dato numerico.
 	 E' allineato a destra nella riga del display LCD quindi non deve essere utilizzato un
 	 messaggio con le barre '|' perche' non sarebbero cancellate quelle non sovrascritte.
	
   IN:  - riga dove visualizzare il dato, dato, valore del dato, size del dato, flag valuta
  OUT:  - true se premuto tasto '-' per terminare la programmazione di questa selezione
\*------------------------------------------------------------------------------------------*/
bool  Visualizza_Valore(uint8_t rownum, uint32_t Valore, uint8_t SizeDato, bool valuta) {
	
	uint8_t		Pointpos, i, destpnt, numchar;
	char		buffer[12];
	
	switch (rownum) {
		case 1:
			destpnt = Riga_1_In_Buffer;
			break;
		case 2:
			destpnt = Riga_2_In_Buffer;
			break;
		case 3:
			destpnt = Riga_3_In_Buffer;
			break;
		case 4:
		default:
			destpnt = Riga_4_In_Buffer;
			break;
	}
	destpnt += (LCD_NUMCOL-1);																	// Pointer all'ultima colonna della riga (Byte LSB del valore in entrata)
	
	if ((valuta == false) || (DecimalPointPosition == 0)) {
		Pointpos = 0;																			// Non e' una valuta o non c'e' punto decimale
	} else {
		Pointpos = destpnt - (uint8_t)DecimalPointPosition;
	}
	sprintf(buffer,"%u",Valore);																// Trasformo valore in chr ASCII in buffer temporaneo
	numchar = (strlen(&buffer[0]));
	for (i=(numchar-1); i !=0xff; i--) {														// Init "i = numchar-1" as pointer al LSB del valore in entrata
		if (destpnt == Pointpos) {																// Posizione del punto decimale
			LCD_DataBuff[destpnt--] = '.';
		}
		LCD_DataBuff[destpnt--] = buffer[i];
	}
	if ((Pointpos != 0) && (destpnt >= Pointpos) && (Valore > 0)) {								// Il punto decimale non e' ancora stato stampato: aggiungo zeri fino all'unita'
		do {
			if (destpnt == Pointpos) {															// Posizione del punto decimale
				LCD_DataBuff[destpnt--] = '.';
			}
			LCD_DataBuff[destpnt--] = ASCII_ZERO;
		} while (destpnt >= Pointpos);
	}
	Refresh_LCD();
	AttesaTasto(false);
	if (Tasto == Tasto_Meno) return true;														// Terminare il menu
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_Generic_Boolean
 	 Imposta opzione Y/N nel parametro desiderato.
 	 Se mask = 0 si imposta mask = 1.
	
   IN:  - messaggio da visualizzare e address EE opzione da programmare
  OUT:  - true se premuto tasto '-' per terminare la programmazione di questa selezione
\*------------------------------------------------------------------------------------------*/
bool  Prog_Generic_Boolean(uint8_t msgnum, uint16_t EEaddress, uint8_t mask) {
	
	sBool.Msg 		= msgnum;
	sBool.Row		= Row_4;
	sBool.MaskData	= mask;
	if (sBool.MaskData == 0) sBool.MaskData = 1;
	sBool.AddrData 	= EEaddress;
	Elab_Boolean(&sBool);
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione 
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_Generic_BCD
 	 Imposta valore da 00 a 99 nel parametro desiderato.
	
   IN:  - numero messaggio, address EE del parametro, valori minimo e massimo.
  OUT:  - true se premuto tasto '-' per terminare la programmazione di questa selezione
\*------------------------------------------------------------------------------------------*/
bool  Prog_Generic_BCD(uint8_t msgnum, uint16_t EEaddress, uint8_t min, uint8_t MAX, bool UnaCifra)
{
	sBCD.Msg 		= msgnum;
	sBCD.Row		= Row_4;
	sBCD.MinData	= min;
	sBCD.MaxData	= MAX;
	sBCD.AddrData 	= EEaddress;
	Elab_BCD(&sBCD, UnaCifra);
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_Generic_uint8
 	 Imposta valore a 8 bit nel parametro desiderato.
	
   IN:  - numero messaggio, address EE del parametro, valori minimo e massimo.
  OUT:  - true se premuto tasto '-' per terminare la programmazione di questa selezione
\*------------------------------------------------------------------------------------------*/
bool  Prog_Generic_uint8(uint8_t msgnum, uint16_t EEaddress, uint8_t min, uint8_t MAX) {
	
	sHex8.Msg 		= msgnum;
	sHex8.Row		= Row_4;
	sHex8.MinData	= min;
	sHex8.MaxData	= MAX;
	sHex8.AddrData 	= EEaddress;
	Elab_Hex8(&sHex8);
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_Generic_uint16
 	 Imposta valore a 16 bit nel parametro desiderato, visualizzato sulla Riga 4.
	
   IN:  - numero messaggio, address EE del parametro, valori minimo e massimo.
  OUT:  - true se premuto tasto '-' per terminare la programmazione 
\*------------------------------------------------------------------------------------------*/
bool  Prog_Generic_uint16(uint8_t msgnum, uint16_t EEaddress, uint16_t min, uint16_t MAX, bool valuta) {
	
	sHex16.Msg 		= msgnum;
	sHex16.Row		= Row_4;
	sHex16.MinData	= min;
	sHex16.MaxData	= MAX;
	sHex16.AddrData = EEaddress;
	Elab_Hex16(&sHex16, valuta);
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_Generic_uint32
 	 Imposta valore a 32 bit nel parametro desiderato.
	
   IN:  - numero messaggio, address EE del parametro, valori minimo e massimo.
  OUT:  - true se premuto tasto '-' per terminare la programmazione
\*------------------------------------------------------------------------------------------*/
bool  Prog_Generic_uint32(uint8_t msgnum, uint16_t EEaddress, uint32_t min, uint32_t MAX, bool valuta) {
	
	sHex32.Msg 		= msgnum;
	sHex32.Row		= Row_4;
	sHex32.MinData	= min;
	sHex32.MaxData	= MAX;
	sHex32.AddrData = EEaddress;
	Elab_Hex32(&sHex32, valuta);
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_PrezzoSelez
 	 Imposta il Prezzo della selezione desiderata.
	
   IN:  - numero selezione
  OUT:  - true se premuto tasto '-' per terminare la programmazione
\*------------------------------------------------------------------------------------------*/
bool  Prog_PrezzoSelez(uint8_t numeroselez) {
	
	sHex16.Msg 		= mSel_Prezzo;
	sHex16.Row		= Row_4;
	sHex16.MinData	= MinPrezzo;
	sHex16.MaxData	= MaxPrezzo;
	sHex16.AddrData = IICEEPConfigAddr(EEPrezzoPR[numeroselez]);
	if (sHex16.AddrData > EE_Prezzo_Hlimit) {
		Tasto = Tasto_Meno; 																	// L'indice ha sbordato l'array, termino questa programmazione
		return true;
	}
	if (Elab_Hex16(&sHex16, true) == true) {													// Valore modificato, aggiorno Audit
		SetDatePrice();
	}
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_ScontoSelez
 	 Imposta lo Sconto della selezione desiderata.
	
   IN:  - numero selezione
  OUT:  - true se premuto tasto '-' per terminare la programmazione
\*------------------------------------------------------------------------------------------*/
bool  Prog_ScontoSelez(uint8_t numeroselez) {
	
	sHex16.Msg 		= mSel_Sconto;
	sHex16.Row		= Row_4;
	sHex16.MinData	= MinSconto;
	sHex16.MaxData	= MaxSconto;
	sHex16.AddrData = IICEEPConfigAddr(EEPrezzoPA[numeroselez]);
	if (sHex16.AddrData > EE_Sconto_Hlimit) {
		Tasto = Tasto_Meno; 																	// L'indice ha sbordato l'array, termino questa programmazione
		return true;
	}
	if (Elab_Hex16(&sHex16, true) == true) {													// Valore modificato, aggiorno Audit
		SetDatePrice();
	}
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: AttesaPassword
 	 Imposta una password per accedere alla programmazione.
 	 Si esce dalla funzione impostando una delle 3 password oppure da reset ripremendo il
 	 tasto PROG. 
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
uint16_t  AttesaPassword(uint8_t msg1, uint8_t rowmsg1, uint8_t msg2, uint8_t rowmsg2, uint8_t numcifre) {
	
	uint8_t		j, offsetRow, rigaParam, NumCifra = 0;
	uint8_t		CifreInserite[6] = {0,0,0,0,0,0};
	uint16_t	PSW_Digitata;
	
	if (rowmsg2 != 0) rigaParam = rowmsg2;														// Riga che contiene i chr di ricerca
	do {
		VisMessage(msg1, rowmsg1, msg2, rowmsg2, true);
		if (NumCifra > 0) {																		// Sono state gia' digitate cifre, le visualizzo al posto degli '*'
			offsetRow = RicercaPosizione(rigaParam, kLocCifraInputPsw);
			if (offsetRow != 0xff) {
				for (j=0; j < NumCifra; j++) {
					LCD_DataBuff[(offsetRow)] = (CifreInserite[j]);
					offsetRow++;
				}
				Refresh_LCD();
			}
		}
		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {		Warning[Pe186]: pointless comparison of unsigned integer with zero
		if (Tasto <= Tasto_Nove)
		{
			if (NumCifra < numcifre) {															// Dopo l'ultima cifra non prendo altri numeri
				CifreInserite[NumCifra++] = (Tasto | '0');
			}
		}
		if ((Tasto == Tasto_PrevMenu) || (Tasto == Tasto_NextMenu)) {
			PSW_Digitata = atouint32(&CifreInserite[0], numcifre);
			return (uint16_t)PSW_Digitata;
		}
	} while (true);
}

/*------------------------------------------------------------------------------------------*\
 Method: ProgNewPSW
 	 Modifica PSW inserita.
	
   IN:  - livello di accesso
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
bool  ProgNewPSW(uint8_t AccLev)
{
	sHex16.Msg 		= mNewPassword;
	sHex16.Row		= Row_4;
	sHex16.MinData	= 0;
	sHex16.MaxData	= 9999;
	if (AccLev == 1) sHex16.AddrData = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_1]);
	if (AccLev == 2) sHex16.AddrData = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_2]);
	if (AccLev == 3) sHex16.AddrData = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_3]);
	Elab_Hex16(&sHex16, false);
	if (Tasto == Tasto_Meno) return true;														// Terminare questa programmazione
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: AttesaTasto
 	 Attende pressione tasto tastiera.
 	 Controlla anche il tasto "PROG" per resettare se premuto.
	 Se abilitato controlla il timer per uscire anche senza alcun tasto premuto.
	
   IN:  - 
  OUT:  - esce se tasto premuto dopo aver rilasciato il precedente oppure reset da WDT
\*------------------------------------------------------------------------------------------*/
void AttesaTasto(bool checkTout)
{
	while (true) {
		WDT_Clear(gVars.devWDT);
		if (TastoProgrammazione == 0)
		{																						// Tasto PROG premuto
			if (gVMCState.TastoProgElaborato == false)
			{
				Restart_Da_Prog();																// Attesa WDT da tasto PROG premuto per uscire dalla programmazione
			}
		}
		else
		{
			gVMCState.TastoProgElaborato = false;												// Tasto PROG rilasciato
		}
		if (fTastoPmt == true)
		{
			ResetToutProgram();																	// Ricarico Timer uscita automatica programmazione senza piu' attivita' sulla tastiera
			if (fTastoElab == false)															// Tasto da elaborare
			{
				if (fTastoPmt == true)															// Ricontrollo flag perche' puo' essere intervenuto l'IRQ che ha resettato fTastoElab
				{																				// per tasto rilasciato e attiverebbe ancora il buzzer, dando un doppio beep
					fTastoElab = true;
#if (DEB_NO_BUZZ == 0)
					Buz_start(Milli50, HZ_2000, 0);												// Segnalo tasto premuto
#endif
					if (Tasto != oldtasto)
					{
						oldtasto = Tasto;
						break;																	// Esco con tasto premuto in Tasto
					}
				}
			}
		} else {
			oldtasto = 0xff;
		}
		if (checkTout == true) {																// Controllo timer: se scaduto esco anche senza alcun tasto premuto
			if (TmrTimeout(gVMCState.ToutFase)) {
				Tasto = NoTastoPremuto;
				break;
			}
		}
#if OutProg		
		if (IsToutProgramElapsed() == true) {
			Restart_Da_Prog();																	// Attesa WDT da timeout per uscire dalla programmazione
		}
#endif		
	}
}	
	
/*------------------------------------------------------------------------------------------*\
 Method: VisMessage
	Legge dalla memoria (Flash o EEprom) il messaggio e lo visualizza o sulla riga
	desiderata.
	Possono essere visualizzati al massimo 2 messaggi su due righe.
	Opzionalmente il buffer display e' preventivamente riempito con spazi.
	
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void VisMessage(uint8_t NumMess_1, uint8_t NumRigaMsg_1, uint8_t NumMess_2, uint8_t NumRigaMsg_2, bool FillSpace_All_LCD) {

	uint8_t	i;
	uint8_t	*pnt;
	
	if (NumMess_1 > 0) {
		if (NumMess_1 == SpacesInBuffer) {														// Richiesta di riempire di spazi la riga
			pnt = &TempBuff;
			for (i=0; i<MessLength; i++) {
				*pnt++ = ASCII_SPACE;
			}
		} else {
			ReadMessage((char*)&TempBuff, Lingua, NumMess_1, MessLength);						// Messaggio da EE in TempBuff
		}
		Msg_InAnyRow((char*)&TempBuff, NumRigaMsg_1, FillSpace_All_LCD);
		FillSpace_All_LCD = false;																// Clear buffer gia' eseguito con la prima riga
	}
	if (NumMess_2 > 0) {
		if (NumMess_2 == SpacesInBuffer) {														// Richiesta di riempire di spazi la riga
			pnt = &TempBuff;
			for (i=0; i<MessLength; i++) {
				*pnt++ = ASCII_SPACE;
			}
		} else {
			ReadMessage((char*)&TempBuff, Lingua, NumMess_2, MessLength);						// Messaggio da EE in TempBuff
		}
		Msg_InAnyRow((char*)&TempBuff, NumRigaMsg_2, FillSpace_All_LCD);
	}
	LCD_Disp_Clear();
	Refresh_LCD();
}

/*------------------------------------------------------------------------------------------*\
 Method: VisualNum_1cifra
	Visualizza un numero di 1 cifra alla prima occorrenza del chr '|' del buffer display 
	LCD nella riga desiderata.
	Il parametro deve essere un byte Hex limitato al valore massimo di 9.
	
   IN:  - Numero in Hex e numero riga dove cercare il chr di riferimento
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void VisualNum_1cifra(uint8_t Numero, uint8_t NumRiga)
{
	uint8_t	offsetRow;
	
	offsetRow = RicercaPosizione(NumRiga, kLocLCDCifraIntera);
	if (offsetRow != 0xff)
	{
		Visual_BCD_1_Cifra(&LCD_DataBuff[0], Numero, offsetRow);
	}
	Refresh_LCD();
}

/*------------------------------------------------------------------------------------------*\
 Method: VisualNum_2cifre
	Visualizza un numero di 2 cifre alla prima occorrenza del chr '|' del buffer display 
	LCD nella riga desiderata.
	Il parametro deve essere un byte Hex limitato al valore massimo di 0x63 (99d).
	
   IN:  - Numero in Hex e numero riga dove cercare il chr di riferimento
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void VisualNum_2cifre(uint8_t Numero, uint8_t NumRiga) {
	
	uint8_t	offsetRow;
	
	offsetRow = RicercaPosizione(NumRiga, kLocLCDCifraIntera);
	if (offsetRow != 0xff) {
		Visual_BCD_2_Cifre(&LCD_DataBuff[0], Numero, offsetRow);
	}
	Refresh_LCD();
}

/*------------------------------------------------------------------------------------------*\
 Method: RicercaPosizione
 	 Ricerca la posizione di un carattere in una delle righe del buffer display LCD.
	
   IN:  - buffer address, chr da ricercare
  OUT:  - offset del chr nella riga del buffer - 0xff se non trovato chr di riferimento
\*------------------------------------------------------------------------------------------*/
uint8_t  RicercaPosizione(uint8_t NumRiga, LocalAttrId chrRef) {

	char	*charPnt;
	uint8_t	offsetRow;
	
	switch (NumRiga) {
		case 1:
			offsetRow = Riga_1_In_Buffer;
			break;
		case 2:
			offsetRow = Riga_2_In_Buffer;
			break;
		case 3:
			offsetRow = Riga_3_In_Buffer;
			break;
		default:
		case 4:
			offsetRow = Riga_4_In_Buffer;
			break;
	}
	charPnt = strchr(&LCD_DataBuff[offsetRow], (int)GetRefChar(chrRef));
	if (charPnt == 0) return 0xff;
	offsetRow = (charPnt - &LCD_DataBuff[0]);
	return offsetRow;
}

/*--------------------------------------------------------------------------------------*\
Method:	GetRefChar
	Ritorna il chr ASCII da un array puntato dal parametro di chiamata.
	Il chr e' usato nella ricerca della sua posizione nel buffer del display LCD per
	essere sotituito dal parametro in programmazione.
	  
   IN:  - id del chr da ricercare
  OUT:  - chr ASCII del carattere ricercato o ' ' se non trovato.
\*--------------------------------------------------------------------------------------*/
static char GetRefChar(LocalAttrId attr) {

	return attr>=kLocAttr_Count? ' ': sLocalizationDefault[attr];
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_BCD
 	 Funzione per visualizzare e modificare un numero BCD da 0 a 99.
 	 Il parametro e' sempre memorizzato in EEprom.
 	 Con tasti + e - si esce dalla funzione. 
 	 Solo prima di uscire si controllano i limiti e, se ecceduti, si visualizza il valore di
 	 default impostato e si attende un'ulteriore pressione dei tasti + e -.
	
   IN:  - struttura sBCD
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  Elab_BCD(tProgBCD *sBCDpnt, bool UnaCifra)
{
	uint8_t  offsetRow;
	bool	endElab = false;

	do {
		VisMessage(sBCDpnt->Msg, sBCDpnt->Row, 0, 0, false);														// Visualizza messaggio sulle riga richiesta senza cancellare le altre
		ReadEEPI2C(sBCDpnt->AddrData, (uint8_t *)(&sBCDpnt->paramVal), sizeof(sBCDpnt->paramVal));					// Leggo attuale valore parametro dalla EE
		offsetRow = RicercaPosizione(sBCDpnt->Row, kLocLCDCifraIntera);
		if (UnaCifra == true)
		{
			Visual_BCD_1_Cifra(&LCD_DataBuff[0], sBCDpnt->paramVal, offsetRow);										// Visualizza valore a una cifra
		}
		else
		{
			Visual_BCD_2_Cifre(&LCD_DataBuff[0], sBCDpnt->paramVal, offsetRow);										// Visualizza valore su 2 cifre
		}
		Refresh_LCD();																								// Visualizza valore
		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {			Warning[Pe186]: pointless comparison of unsigned integer with zero
		if (Tasto <= Tasto_Nove)
		{
			if (UnaCifra == true)
			{
				sBCDpnt->paramVal = Tasto;																			// Nuovo valore
				if (sBCDpnt->paramVal < sBCDpnt->MinData)
				{
					sBCDpnt->paramVal = sBCDpnt->MinData;
				} 
				else if (sBCDpnt->paramVal > sBCDpnt->MaxData)
				{
					sBCDpnt->paramVal = sBCDpnt->MaxData;
				}
			}
			else
			{
				sBCDpnt->paramVal = Elab_uint8(sBCDpnt->paramVal, Tasto, 99U);										// Nuovo valore
			}
			WriteEEPI2C(sBCDpnt->AddrData, (uint8_t *)(&sBCDpnt->paramVal), sizeof(sBCDpnt->paramVal));				// Scrivo nuovo valore parametro in EE
		} 
		else
		{
			if ((Tasto == Tasto_Piu) || (Tasto == Tasto_Meno))
			{
				endElab = true;
				if (sBCDpnt->paramVal < sBCDpnt->MinData)
				{
					endElab = false;
					sBCDpnt->paramVal = sBCDpnt->MinData;
				} 
				else if (sBCDpnt->paramVal > sBCDpnt->MaxData)
				{
					endElab = false;
					sBCDpnt->paramVal = sBCDpnt->MaxData;
				}
				if (endElab == false)																				// Limiti ecceduti, visualizzare valore default senza uscire
				{
					WriteEEPI2C(sBCDpnt->AddrData, (uint8_t *)(&sBCDpnt->paramVal), sizeof(sBCDpnt->paramVal));		// Scrivo nuovo valore parametro in EE
				}
			}
		}
	} while (endElab == false);
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_Hex8
 	 Funzione per visualizzare e modificare un byte; il parametro e' sempre salvato in EE.
 	 Il valore e' visualizzato dove c'e' il chr di riferimento '|'.
 	 Con tasti + e - si esce dalla funzione.
 	 Solo prima di uscire si controllano i limiti e, se ecceduti, si visualizza il valore di
 	 default impostato e si attende un'ulteriore pressione dei tasti + e -.
 	 Se l'indirizzo del parametro fa riferimento alla RAM, non viene memorizzato in EE.
	
   IN:  - pointer a struttura sHex8
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void  Elab_Hex8(tProgHex8 *sHex8pnt) {
	
	uint8_t  offsetRow;
	bool	endElab = false;

	do {
		VisMessage(sHex8pnt->Msg, sHex8pnt->Row, 0, 0, false);													// Visualizza messaggio sulle riga richiesta senza cancellare le altre
		ReadEEPI2C(sHex8pnt->AddrData, (uint8_t *)(&sHex8pnt->paramVal), sizeof(sHex8pnt->paramVal));			// Leggo attuale valore parametro dalla EE
		offsetRow = RicercaPosizione(sHex8pnt->Row, kLocLCDCifraIntera);
		Hex8InMessage(&LCD_DataBuff[0], sHex8pnt->paramVal, offsetRow, false, 0);								// Visualizza dato fino a 3 cifre
		Refresh_LCD();																							// Visualizza valore
		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {		Warning[Pe186]: pointless comparison of unsigned integer with zero
		if (Tasto <= Tasto_Nove)
		{
			sHex8pnt->paramVal = Elab_uint8(sHex8pnt->paramVal, Tasto, 0xff);									// Nuovo valore
			WriteEEPI2C(sHex8pnt->AddrData, (uint8_t *)(&sHex8pnt->paramVal), sizeof(sHex8pnt->paramVal));		// Scrivo nuovo valore parametro in EE
		}
		else
		{
		 	if ((Tasto == Tasto_Piu) || (Tasto == Tasto_Meno))
			{
				endElab = true;																						// Richiesta uscita funzione
				if (sHex8pnt->paramVal < sHex8pnt->MinData)
				{
				 	endElab = false;
					sHex8pnt->paramVal = sHex8pnt->MinData;
				} 
				else 
				{
					if (sHex8pnt->paramVal > sHex8pnt->MaxData)
					{
						endElab = false;
						sHex8pnt->paramVal = sHex8pnt->MaxData;
					}
				}
				if (endElab == false)																				// Limiti ecceduti, visualizzare valore default senza uscire
				{
					WriteEEPI2C(sHex8pnt->AddrData, (uint8_t *)(&sHex8pnt->paramVal), sizeof(sHex8pnt->paramVal));	// Scrivo nuovo valore parametro in EE
				}
			}
		}
	} while (endElab == false);
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_Hex16
 	 Funzione per visualizzare e modificare una word; il parametro e' sempre salvato in EE.
 	 Il valore e' visualizzato dove c'e' il chr di riferimento '|'.
 	 Con tasti + e - si esce dalla funzione.
 	 Solo prima di uscire si controllano i limiti e, se ecceduti, si visualizza il valore di
 	 default impostato e si attende un'ulteriore pressione dei tasti + e -.
	
   IN:  - pointer a struttura sHex16
  OUT:  - true se almeno una modifica e' stata effettuata.
\*------------------------------------------------------------------------------------------*/
static bool  Elab_Hex16(tProgHex16 *sHex16local, bool valuta) {
	
	bool	ValModified = false;
	uint8_t	offsetRow;
	bool	endElab = false;

	do {
		VisMessage(sHex16local->Msg, sHex16local->Row, 0, 0, false);														// Visualizza messaggio sulle riga richiesta senza cancellare le altre
		ReadEEPI2C(sHex16local->AddrData, (uint8_t *)(&sHex16local->paramVal), sizeof(sHex16local->paramVal));				// Leggo attuale valore parametro dalla EE
		offsetRow = RicercaPosizione(sHex16local->Row, kLocLCDCifraIntera);
		if (valuta == true) {
			ValutaInMessage(&LCD_DataBuff[0], sHex16local->paramVal, offsetRow);											// Visualizza dato con decimal point
		} else {
			Hex16InMessage(&LCD_DataBuff[0], sHex16local->paramVal, offsetRow);												// Visualizza dato fino a 5 cifre
		}
		Refresh_LCD();																										// Visualizza valore
		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {			Warning[Pe186]: pointless comparison of unsigned integer with zero
		if (Tasto <= Tasto_Nove)
		{
			ValModified = true;
			sHex16local->paramVal = Elab_uint16(sHex16local->paramVal, Tasto);												// Nuovo valore
			WriteEEPI2C(sHex16local->AddrData, (uint8_t *)(&sHex16local->paramVal), sizeof(sHex16local->paramVal));			// Scrivo nuovo valore parametro in EE
		} else if ((Tasto == Tasto_Piu) || (Tasto == Tasto_Meno)) {
			endElab = true;																									// Richiesta uscita funzione
			if (sHex16local->paramVal < sHex16local->MinData) {
				endElab = false;
				sHex16local->paramVal = sHex16local->MinData;
			} else if (sHex16local->paramVal > sHex16local->MaxData) {
				endElab = false;
				sHex16local->paramVal = sHex16local->MaxData;
			}
			if (endElab == false) {																							// Limiti ecceduti, visualizzare valore default senza uscire
				WriteEEPI2C(sHex16local->AddrData, (uint8_t *)(&sHex16local->paramVal), sizeof(sHex16local->paramVal));		// Scrivo nuovo valore parametro in EE
			}
		}
	} while (endElab == false);
	return ValModified;
}

/*----------------------------------------------------------------------------------------------*\
 Method: Elab_Hex32
 	 Funzione per visualizzare e modificare una long word; il parametro e' sempre salvato in EE.
 	 Il valore e' visualizzato a partire dal primo chr di riferimento '|' da sinistra a destra.
 	 Con tasti + e - si esce dalla funzione.
 	 Solo prima di uscire si controllano i limiti e, se ecceduti, si visualizza il valore di
 	 default impostato e si attende un'ulteriore pressione dei tasti + e -.
	
   IN:  - pointer a struttura sHex32
  OUT:  - true se almeno una modifica e' stata effettuata.
\*----------------------------------------------------------------------------------------------*/
static bool  Elab_Hex32(tProgHex32 *sHex32local, bool valuta) {
	
#define	Data32Len		7																									// Lunghezza buffer dati a 32 bit (uno viene aggiunto dalla VisValore_32bit)

	bool	ValModified = false;
	uint8_t	offsetRow;
	bool	endElab = false;

	do {
		VisMessage(sHex32local->Msg, sHex32local->Row, 0, 0, false);														// Visualizza messaggio sulle riga richiesta senza cancellare le altre
		ReadEEPI2C(sHex32local->AddrData, (uint8_t *)(&sHex32local->paramVal), sizeof(sHex32local->paramVal));				// Leggo attuale valore parametro dalla EE
		offsetRow = RicercaPosizione(sHex32local->Row, kLocLCDCifraIntera);
		if (valuta == true) {
			ValutaInMessage(&LCD_DataBuff[0], sHex32local->paramVal, offsetRow);											// Visualizza dato con decimal point
		} else {
			Hex32InMessage(&LCD_DataBuff[0], sHex32local->paramVal, offsetRow, 8);											// Visualizza dato fino a 8 cifre
			//VisValore_32bit(&LCD_DataBuff[offsetRow], sHex32local->paramVal, valuta, Data32Len);
		}
		Refresh_LCD();																									// Visualizza valore
		AttesaTasto(false);
		//MR19 if ((Tasto >= Tasto_Zero) && (Tasto <= Tasto_Nove)) {			Warning[Pe186]: pointless comparison of unsigned integer with zero
		if (Tasto <= Tasto_Nove)
		{
			ValModified = true;
			sHex32local->paramVal = Elab_uint32_wLimit(sHex32local->paramVal, Tasto, sHex32local->MaxData);					// Nuovo valore
			WriteEEPI2C(sHex32local->AddrData, (uint8_t *)(&sHex32local->paramVal), sizeof(sHex32local->paramVal));			// Scrivo nuovo valore parametro in EE
		} else if ((Tasto == Tasto_Piu) || (Tasto == Tasto_Meno)) {
			endElab = true;																									// Richiesta uscita funzione
			if (sHex32local->paramVal < sHex32local->MinData) {
				endElab = false;
				sHex32local->paramVal = sHex32local->MinData;
			} else if (sHex32local->paramVal > sHex32local->MaxData) {
				endElab = false;
				sHex32local->paramVal = sHex32local->MaxData;
			}
			if (endElab == false) {																							// Limiti ecceduti, visualizzare valore default senza uscire
				WriteEEPI2C(sHex32local->AddrData, (uint8_t *)(&sHex32local->paramVal), sizeof(sHex32local->paramVal));		// Scrivo nuovo valore parametro in EE
			}
		}
	} while (endElab == false);
	return ValModified;
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_Boolean
 	 Funzione per visualizzare e modificare un parametro Y/N tramite tasti 0 e 1.
 	 Il parametro e' sempre memorizzato in EEprom.
 	 Con tasti + e - si esce dalla funzione. 
	
   IN:  - struttura sBool
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void  Elab_Boolean(tProgBool *sBoolean) {
	
	uint8_t  offsetRow;
	bool	endElab = false;

	do {
		VisMessage(sBoolean->Msg, sBoolean->Row, 0, 0, false);									// Visualizza messaggio sulle riga richiesta senza cancellare le altre
		ReadEEPI2C(sBoolean->AddrData, (uint8_t *)(&sBoolean->paramVal), 1U);					// Leggo attuale valore parametro dalla EE
		LastInputData = sBoolean->paramVal;														// Salvo parametro in LastInputData per elaborazione al di fuori di questa funzione 
		offsetRow = RicercaPosizione(sBoolean->Row, kLocLCDCifraIntera);
		if (offsetRow != 0xff) {
			if ((sBoolean->paramVal & sBoolean->MaskData) == 0) {
				LCD_DataBuff[offsetRow] = 'N';
				LastInputData = false;															// Salvo parametro in LastInputData per elaborazione al di fuori di questa funzione 
			} else {
				LCD_DataBuff[offsetRow] = 'Y';
				LastInputData = true;															// Salvo parametro in LastInputData per elaborazione al di fuori di questa funzione 
			}
			Refresh_LCD();
		}
		AttesaTasto(false);
		if ((Tasto == Tasto_Zero) || (Tasto == Tasto_Uno)) {
			sBoolean->paramVal &= ~(sBoolean->MaskData);
			if (Tasto == Tasto_Uno) sBoolean->paramVal |= sBoolean->MaskData;
			WriteEEPI2C(sBoolean->AddrData, (uint8_t *)(&sBoolean->paramVal), 1U);				// Scrivo nuovo valore parametro in EE
		} else if ((Tasto == Tasto_Piu) || (Tasto == Tasto_Meno)) {
			endElab = true;
		}
	} while (endElab == false);
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_uint8
 	 Chiamata con tasto numerico per determinare un parametro uint8_t (0-255).
 	 Esce con nuovo valore numerico.
	
   IN:  - Valore attuale, tasto numerico premuto
  OUT:  - Valore aggiornato
\*------------------------------------------------------------------------------------------*/
uint8_t Elab_uint8(uint8_t EntryVal, uint8_t TastoVal, uint8_t maxval) {

	uint32_t	ActualVal, temp;
	
	ActualVal = EntryVal*10;
	if ((ActualVal+TastoVal) > maxval) {
		temp = ActualVal/100;
		temp = temp * 10;
		ActualVal = (EntryVal - temp);
		if (ActualVal < 10) ActualVal *= 10;
	}
	ActualVal += TastoVal;
	return	(uint8_t)ActualVal;
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_uint16_wLimit
 	 Chiamata con tasto numerico per determinare un parametro uint16_t.
 	 Esce con nuovo valore numerico.
	
   IN:  - Valore attuale, tasto numerico premuto e limite massimo
  OUT:  - Valore aggiornato
\*------------------------------------------------------------------------------------------*/
uint16_t Elab_uint16_wLimit(uint16_t EntryVal, uint8_t TastoVal, uint16_t MaxVal) {

//MR19 volatile uint32_t	ActualVal, temp;		Warning ????  Warning[Pa082]: undefined behavior: the order of volatile accesses is undefined in this statement C:\RMFATech\Baltom-EV\Src\VMC_CPU_ProgFunct.c 984 
	uint32_t	ActualVal, temp;
	
	ActualVal = EntryVal*10;
	if ((ActualVal + (uint32_t)TastoVal) > (uint32_t)MaxVal) {
		ActualVal = 0;
		ActualVal += TastoVal;
		return	(uint16_t)ActualVal;
	}
	temp = ActualVal/100000;
	temp = temp * 100000;
	ActualVal -= temp;
	ActualVal += TastoVal;
	return	(uint16_t)ActualVal;
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_uint16
 	 Chiamata con tasto numerico per determinare un parametro uint16_t (0-65535).
 	 Esce con nuovo valore numerico.
	
   IN:  - Valore attuale, tasto numerico premuto
  OUT:  - Valore aggiornato
\*------------------------------------------------------------------------------------------*/
uint16_t Elab_uint16(uint16_t EntryVal, uint8_t TastoVal) {

	uint32_t	ActualVal, temp;
	
	ActualVal = EntryVal*10;
	if ((ActualVal+TastoVal) > 0xffff) {
		temp = ActualVal/10000;
		temp = temp * 1000;
		ActualVal = (EntryVal - temp) * 10;
	}
	ActualVal += TastoVal;
	return	(uint16_t)ActualVal;
}

/*------------------------------------------------------------------------------------------*\
 Method: Elab_uint32_wLimit
 	 Chiamata con tasto numerico per determinare un parametro uint32_t.
 	 Esce con nuovo valore numerico.
	
   IN:  - Valore attuale, tasto numerico premuto e limite massimo
  OUT:  - Valore aggiornato
\*------------------------------------------------------------------------------------------*/
uint32_t Elab_uint32_wLimit(uint32_t EntryVal, uint8_t TastoVal, uint32_t MaxVal) {

//MR19 volatile uint32_t	ActualVal, temp;		Warning ????  Warning[Pa082]: undefined behavior: the order of volatile accesses is undefined in this statement C:\RMFATech\Baltom-EV\Src\VMC_CPU_ProgFunct.c 984 
	uint32_t	ActualVal, temp;
	
	ActualVal = EntryVal*10;
	temp = ActualVal/100000000;
	temp = temp * 100000000;
	ActualVal -= temp;
	if ((ActualVal + (uint32_t)TastoVal) > MaxVal) {
		ActualVal = 0;
	}
	ActualVal += TastoVal;
	return ActualVal;
}

/*------------------------------------------------------------------------------------------*\
 Method: TestPaymentSettings
 	 Controlla le opzioni dei sistemi di pagamento per verificarne la congruita'.
 	 Definisce la priorita' nel caso siano impostati piu' sistemi di pagamento in contrasto
 	 tra di loro.
 	 La priorita' e': 1) Executive SLV, 2) Mifare+selettore 3) Gratuito 4) MDB.
 	 Modifica la EE della opzione Mifare per evitare che qui sia impostata in un modo e 
 	 nella programmazione si veda un valore diverso, poiche' la programmazione utilizza 
 	 direttamente la EE e non la variabile in RAM.
	
   IN: - 
  OUT: - 
\*------------------------------------------------------------------------------------------*/
void  TestPaymentSettings(void)
{
	uint8_t		ParamVal = 0;

	if ((gVMC_ConfVars.ExecutiveSLV | gVMC_ConfVars.PresMifare | gVMC_ConfVars.Gratuito | gVMC_ConfVars.ProtMDB) == false)

#if CESAM
	{	// --- Nessuna impostazione: set Mifare + Selettore ---
		gVMC_ConfVars.PresMifare = true;
		gVMC_ConfVars.ExecutiveSLV 	= false;
		gVMC_ConfVars.ExecMSTSLV  	= false;
		gVMC_ConfVars.Gratuito 		= false;
		gVMC_ConfVars.CPC_MDB = false;
		gVMC_ConfVars.ProtMDBSLV = false;
	}
#else	
	{	// --- Nessuna impostazione: set Exec Slave Std ---
		gVMC_ConfVars.ExecutiveSLV 	= true;
		gOrionConfVars.PriceHolding = false;
		gVMC_ConfVars.ExecMSTSLV  	= false;
	}
#endif
	
	else if (gVMC_ConfVars.ExecutiveSLV == true)
	{
		// ---- Executive  Slave -----
		gVMC_ConfVars.Gratuito 		= false;
		gVMC_ConfVars.PresMifare 	= false;
		gVMC_ConfVars.ProtMDB 		= false;
		gVMC_ConfVars.ProtMDBSLV 	= false;
		WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]), (uint8_t *)(&ParamVal), 1U);	// Clear Lettore Mifare collegato (evita Antenna Mifare Fail)
	}
	else if (gVMC_ConfVars.PresMifare == true)
	{
		// ---- Mifare +  Selettore -----
		gVMC_ConfVars.ExecutiveSLV 	= false;
		gVMC_ConfVars.ExecMSTSLV  	= false;
		gVMC_ConfVars.Gratuito 		= false;
		if (gVMC_ConfVars.ProtMDB == true)
		{																						// MDB con Mifare Locale: disabilito CPC esterno
			gVMC_ConfVars.CPC_MDB = false;
			gVMC_ConfVars.ProtMDBSLV = false;													// Con Mifare Locale non puo' essere USD Slave
		}
	}
	else if (gVMC_ConfVars.ProtMDB == true)
	{
		// ---- M D B      -----
		gVMC_ConfVars.ExecutiveSLV 	= false;
		gVMC_ConfVars.ExecMSTSLV  	= false;
		gVMC_ConfVars.Gratuito 		= false;
		if (gOrionConfVars.RestoSubito)
		{																						// Resto Subito = Y: set Multivend cosi' non attivo Payout al termine
			MultiVend = true;																	// delle selezioni
		}
		if (gVMC_ConfVars.ProtMDBSLV)
		{																						// USD Slave: rispondo come USD1
			gVMC_ConfVars.USD_Addr = ICP_USD1_ADDR;
			gVMC_ConfVars.PresMifare = false;													// In USD Slave non ho sistemi di pagamento
		}
		if (gVMC_ConfVars.PresMifare == false)
		{																						// MDB senza Mifare Locale: abilito CPC esterno
			gVMC_ConfVars.CPC_MDB = true;
		}
	}
	
	gOrionConfVars.NoTimeCash = true;															// Non azzerare mai il cash residuo, e' azzerato solo all'accensione
	if (MaxRecharge == 0) MaxRecharge = 10000;													// Ricarica Mifare
	if (MaxCash == 0) MaxCash = 500;															// Max Cash
	gOrionConfVars.CardLevelsEnabled 	= false;
	gOrionConfVars.EnDisLocationCode 	= false;												// Disabilito uso del Codice Locazione
	MaxCredGetCard 						= 0;													// Test Credito massimo per accettare carta disattivato
	gOrionConfVars.ListaPrezzo 			= true;													// Le Liste sono prezzi
	//MR20 gOrionConfVars.AuditExt 			= false;			// Riattivo per Baltom
	//MR20 gOrionConfVars.TestAudExtFull 		= false;		// Riattivo per Baltom

	gOrionFreeCrVars.FreeCr1 = 0;
	gOrionFreeCrVars.FreeCr2 = 0;
	gOrionFreeCrVars.FreeCr3 = 0;
	gOrionFreeCrVars.FreeCr4 = 0;
	gOrionFreeCrVars.FreeCr5 = 0;
	gOrionFreeCrVars.FreeCrMode = 0;
	FreeCredAllaSelez = 0;
	gOrionFreeCrVars.FreeCrPeriod		= no_period;											// Freecredit disattivato
	gOrionFreeVendVars.FreeVend1 = 0;
	gOrionFreeVendVars.FreeVend2 = 0;
	gOrionFreeVendVars.FreeVend3 = 0;
	gOrionFreeVendVars.FreeVend4 = 0;
	gOrionFreeVendVars.FreeVendMode = 0;
	FreeVendAllaSelez = 0;
	gOrionFreeVendVars.FreeVendPeriod	= no_period;											// FreeVend disattivate

	Locaz1 = 0;
	Locaz2 = 0;
	Locaz3 = 0;
	Locaz4 = 0;
	CoinEnMask = 0xffff;																		// Non essendo programmabili da chiave USB e da remoto predispongo tutte le monete
	BillEnMask = 0xffff;																		// e banconote abilitate: saranno quindi le periferiche MDB a determinare quali sono abilitate e quali inibite
	gOrionConfVars.NewUserCode = false;															// No Nuovo Codice Utente su carte vergini
	gOrionConfVars.EquazExCh = false;															// Ex-Ch set da qualsiasi tubo vuoto
	gOrionConfVars.CambiaMonete = false;
	gOrionConfVars.InhRechargeCard = false;
	MaxCredGetCard = 0;																			// 0 = carte sempre accettate, indipendentemente dal credito
	gOrionConfVars.ValidatorPolarity = false;													// Sempre attivo basso
	Lingua = 0;
	gOrionConfVars.AuditExt = false;
	gOrionConfVars.TestAudExtFull = false;
	gVMC_ConfVars.FunzDeconto = false;

}

/*------------------------------------------------------------------------------------------*\
 Method: VisValore_32bit
	Prepara un buffer ASCII con il valore fino a 32 bit passato come parametro.
	Se e' una valuta determina la posizione del DPP.
	Converte in ASCII il dato in entrata e lo posiziona in un buffer di lunghezza fissa di 
	11 chr partendo dal LSB. Verifica il DPP per posizionarlo dinamicamente.
	Se il dato in entrata e' zero e DPP = 0, restituisce 0.
	Se il dato in entrata e' 1 centesimo e DPP = 2, restituisce 0.01

   IN:  - 
  OUT:  - numero di chr del buffer, cioe' DataLen+1
\*------------------------------------------------------------------------------------------*/
uint8_t VisValore_32bit(char *DestBuff, uint32_t Valore, bool valuta, uint8_t DataLen)
{
	//uint8_t	buffpos, decimi, centes;
	uint8_t	bufflen;																			// Len buffer di conversione ASCII (es 10 = 11 bytes, da 0 a 10 compreso)
	uint8_t	Pointpos, i, destpnt, numchar;
	char	buffer[16];

	bufflen = DataLen;
	destpnt = bufflen;																			// Pointer all'ultima colonna della riga (Byte LSB del valore in entrata)

	if ((valuta == FALSE) || (DecimalPointPosition == 0)) {
		Pointpos = 0;																			// Non c'e' punto decimale
	} else {
		Pointpos = destpnt - (uint8_t)DecimalPointPosition;
	}
	sprintf(buffer,"%u",Valore);																// Trasformo valore in chr ASCII in buffer temporaneo
	numchar = (strlen(&buffer[0]));
	for (i=(numchar-1); i != 0xff; i--) {														// Init "i = numchar-1" as pointer al LSB del valore in entrata
		if ((Pointpos > 0)&& (destpnt == Pointpos)) {											// Posizione del punto decimale
			DestBuff[destpnt--] = '.';
		}
		DestBuff[destpnt] = buffer[i];
		if (destpnt > 0) destpnt--;
	}
	if ((Pointpos != 0) && (destpnt >= Pointpos) && (Valore > 0)) {								// Il punto decimale non e' ancora stato stampato: aggiungo zeri fino all'unita'
		do {
			if (destpnt == Pointpos) {															// Posizione del punto decimale
				DestBuff[destpnt--] = '.';
			}
			DestBuff[destpnt--] = ASCII_ZERO;
		} while (destpnt >= Pointpos);
	}
	return (bufflen+1);
}


/*------------------------------------------------------------------------------------------*\
 Method: SetDefaultFabbrica
	
   IN: - 
  OUT: - 
\*------------------------------------------------------------------------------------------*/
void  SetDefaultFabbrica(void)
{
	uint16_t	ValMonete[6];
	
	gVMC_ConfVars.PresMifare 	= true;
	gVMC_ConfVars.ExecutiveSLV 	= false;
	gVMC_ConfVars.Gratuito 		= false;
	gVMC_ConfVars.ProtMDB 		= false;
	gVMC_ConfVars.ProtMDBSLV 	= false;
	gVMC_ConfVars.ExecMSTSLV  	= false;
	
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[OptExecMstSlav]), 	(uint8_t *)(&gVMC_ConfVars.ExecMSTSLV),	1U);					// Executive Master/SLAVE
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]), 	(uint8_t *)(&gVMC_ConfVars.PresMifare),	1U);					// Presenza Lettore Mifare
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]), (uint8_t *)(&gVMC_ConfVars.Gratuito),	1U);					// Macchina in gratuito
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MDBPaymSyst]), 		(uint8_t *)(&gVMC_ConfVars.ProtMDB),	1U);					// MDB
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MDB_SLV]), 			(uint8_t *)(&gVMC_ConfVars.ProtMDBSLV),	1U);					// MDB SLAVE
	
	sSelParam_A[0].OutNum_1 = OUT1;
	sSelParam_A[0].OutNum_2 = 0;
	sSelParam_A[1].OutNum_1 = OUT2;
	sSelParam_A[1].OutNum_2 = 0;
	sSelParam_A[2].OutNum_1 = OUT3;
	sSelParam_A[2].OutNum_2 = 0;
	sSelParam_A[3].OutNum_1 = OUT4;
	sSelParam_A[3].OutNum_2 = 0;
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz1_Out1_Num]),	(uint8_t *)(&sSelParam_A[0].OutNum_1), 1U);						// I Uscita associata all'erogazione
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz1_Out2_Num]),	(uint8_t *)(&sSelParam_A[0].OutNum_2), 1U);						// II Uscita associata all'erogazione
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz2_Out1_Num]),	(uint8_t *)(&sSelParam_A[1].OutNum_1), 1U);						// I Uscita associata all'erogazione
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz2_Out2_Num]),	(uint8_t *)(&sSelParam_A[1].OutNum_2), 1U);						// II Uscita associata all'erogazione
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz3_Out1_Num]),	(uint8_t *)(&sSelParam_A[2].OutNum_1), 1U);						// I Uscita associata all'erogazione
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz3_Out2_Num]),	(uint8_t *)(&sSelParam_A[2].OutNum_2), 1U);						// II Uscita associata all'erogazione
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz4_Out1_Num]),	(uint8_t *)(&sSelParam_A[3].OutNum_1), 1U);						// I Uscita associata all'erogazione
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz4_Out2_Num]),	(uint8_t *)(&sSelParam_A[3].OutNum_2), 1U);						// II Uscita associata all'erogazione

	sSelParam_A[0].FlussNum = FLUSS1;
	sSelParam_A[1].FlussNum = FLUSS1;
	sSelParam_A[2].FlussNum = FLUSS1;
	sSelParam_A[3].FlussNum = FLUSS1;
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz1_FlussimNum]), (uint8_t *)(&sSelParam_A[0].FlussNum), 1U);
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz2_FlussimNum]), (uint8_t *)(&sSelParam_A[1].FlussNum), 1U);
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz3_FlussimNum]), (uint8_t *)(&sSelParam_A[2].FlussNum), 1U);
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz4_FlussimNum]), (uint8_t *)(&sSelParam_A[3].FlussNum), 1U);

	sSelParam_A[0].FlowCounterNominale = 100;
	sSelParam_A[1].FlowCounterNominale = 100;
    sSelParam_A[2].FlowCounterNominale = 100;
	sSelParam_A[3].FlowCounterNominale = 100;
	WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_1]), (uint8_t *)(&sSelParam_A[0].FlowCounterNominale), 2U);
	WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_2]), (uint8_t *)(&sSelParam_A[1].FlowCounterNominale), 2U);
	WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_3]), (uint8_t *)(&sSelParam_A[2].FlowCounterNominale), 2U);
	WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_4]), (uint8_t *)(&sSelParam_A[3].FlowCounterNominale), 2U);
	
	DecimalPointPosition = 2;
	WriteEEPI2C(IICEEPConfigAddr(EEDPP), &DecimalPointPosition, sizeof(DecimalPointPosition));
	
	UnitScalingFactor = 1;
	WriteEEPI2C(IICEEPConfigAddr(EEUSF), &UnitScalingFactor, sizeof(UnitScalingFactor));
	
	MaxRecharge = 2500;																								// MAXCRED carta
	MaxCash		= 1000;																								// MAXCRED cash
	WriteEEPI2C(IICEEPConfigAddr(EECreditoMaxCarta), (uint8_t *)(&MaxRecharge), sizeof(MaxRecharge));
	WriteEEPI2C(IICEEPConfigAddr(EECreditoMaxCash), (uint8_t *)(&MaxCash), sizeof(MaxCash));
	
	CurrencyCode = 1978;																							// Currency Code (Hex) (per l'Euro 0x1978 --> 0x7BA in CurrencyHex)
	WriteEEPI2C(IICEEPConfigAddr(EEDescrValuta), (uint8_t *)(&CurrencyCode), sizeof(CurrencyCode));

	ValMonete[0] = 5;
	ValMonete[1] = 10;
	ValMonete[2] = 20;
	ValMonete[3] = 50;
	ValMonete[4] = 100;
	ValMonete[5] = 200;
	WriteEEPI2C(IICEEPConfigAddr(EEMonete[0]), (byte *)(&ValMonete[0]), (6*2));

	sprintf(&MachineModelNumber[0], " A L P  SrL         ");														// Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20)
	WriteEEPI2C(IICEEPConfigAddr(EEModelloMacchina), (uint8_t *)(&MachineModelNumber), Len_ID102);

	TabellaSelezPrezzo[0] = 1;
	TabellaSelezPrezzo[1] = 2;
	TabellaSelezPrezzo[2] = 3;
	TabellaSelezPrezzo[3] = 4;
	WriteEEPI2C(EEPConfVMCAddr(EE_SelezPrezzo), (uint8_t *)(&TabellaSelezPrezzo[0]), MAXNUMSEL);
	
#if DEBUG_DEFAULT	
	gVMC_ConfVars.ExecutiveSLV 	= false;
	gVMC_ConfVars.Gratuito 		= true;
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[ExecSlave]), (uint8_t *)(&gVMC_ConfVars.ExecutiveSLV), 1U);
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]), (uint8_t *)(&gVMC_ConfVars.Gratuito), 1U);			// Macchina in gratuito
	
	MRPPrices.Prices99.MRPPricesList99[0] = 5;
	MRPPrices.Prices99.MRPPricesList99[1] = 20;
	MRPPrices.Prices99.MRPPricesList99[2] = 30;
	MRPPrices.Prices99.MRPPricesList99[3] = 40;
	WriteEEPI2C(IICEEPConfigAddr(EEPrezzoPR), (uint8_t *)(&MRPPrices.Prices99.MRPPricesList99[0]), (4*2));
	
	CodiceMacchina = 445566;
	WriteEEPI2C(IICEEPConfigAddr(EECodiceMacchina), (uint8_t *)(&CodiceMacchina), sizeof(CodiceMacchina));
	
	Pin1 = 11111;
	Pin2 = 22222;
	Gestore = 11111;
	WriteEEPI2C(IICEEPConfigAddr(EEPin1), (uint8_t *)(&Pin1), sizeof(Pin1));
	WriteEEPI2C(IICEEPConfigAddr(EEPin2), (uint8_t *)(&Pin2), sizeof(Pin2));
	WriteEEPI2C(IICEEPConfigAddr(EEGestore), (uint8_t *)(&Gestore), sizeof(Gestore));
	
	sSelParam_A[0].FlussNum = FLUSS1;
	sSelParam_A[1].FlussNum = FLUSS1;
	sSelParam_A[2].FlussNum = FLUSS1;
	sSelParam_A[3].FlussNum = FLUSS1;
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz1_FlussimNum]), (uint8_t *)(&sSelParam_A[0].FlussNum), 1U);
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz2_FlussimNum]), (uint8_t *)(&sSelParam_A[1].FlussNum), 1U);
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz3_FlussimNum]), (uint8_t *)(&sSelParam_A[2].FlussNum), 1U);
	WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[Erogaz4_FlussimNum]), (uint8_t *)(&sSelParam_A[3].FlussNum), 1U);
	
#endif
}				
				

