/* ***************************************************************************************
File:
    AuditStrutture.c

Description:
    Elenco strutture che compongono l'Audit della Serie PK3
    

History:
    Date       Aut  Note
    Set 2012 	MR   

 *****************************************************************************************/

#include <stdio.h>        
#include <string.h>
#include <stdlib.h>


#include "ORION.H"
#include "AuditDataDescr.h"
#include "AuditStrutture.h"
#include "Audit.h"
#include "IICEEP.h"
#include "icpBILL.h"
#include "icpCHGProt.h"
#include "icpBILLProt.h"
#include "icpCPCProt.h"
#include "Funzioni.h"


extern CreditCoinValue	BVHBillValuesList[];
extern CreditCoinValue	CoinValuesList[];
extern CreditCoinValue	BVHBillValuesList[];

#if  TestAudStruct
const void*	StructAddress;
#endif


#define kAuditId1_1String  "*" kProductAuditIdString 					// ID101: "ALP"
#define kAuditDA102_String "*" kProductAuditIdString "*"				// DA102: "ALP"
#define kAuditRevision "*6*0" 

uint8_t SicurRBTKeyA[] 	= {0x13,0xA6,0x5E,0xC4,0x29,0x82};			// KeyA per leggere il settore Sicurezza


// ************  ELENCO   EVENTI   EA1 - EA2  ******************
//                                        12345678901234567890 ***** AN  1 - 20 chr  ******
	const char mSelezSenzaPrezzo[]	=	{"EA2*ED_01 VndPrcNotFound"};
	const char mPayoutInterrotto[]	=	{"EA2*ED_02 PayoutInterrup"};
	const char mAccreditoDubbio[]	=	{"EA2*ED_03 Uncertain Chrg"};
	const char mCaricamBlocchi[]	=	{"EA2*ED_04 Blocks Charged"};
	const char mResNoSyncMDB[]		=	{"EA2*ED_07 MDB-SyncLost  "};
	const char mResChiaveUSB[]		=	{"EA2*ED_08 USBKeyInserted"};
	const char mCaricamBlkOverpay[]	=	{"EA2*ED_09 BlkChrgdOverp."};
	const char mCarteInizializzate[]=	{"EA2*ED_10 Cards Initial."};

#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
const char AUDIT_EA1[]  = {"EA1*"};

const char DESCR_EA1[]    = {"OCF?OCF?EA_03?EA_04?ED_05?ED_06?ED_07?\0"};
const char DESCR_EA106[]  = {"PW_OFF?PW_ON?ExCh_IN?ExCh_OUT?WDRes?ClrBkupEE?SwRevUpdate?\0"};

// "OCF_OFF": Power Down
// "OCF_ON ": Power Up
// "EA_03  ": Exact-change IN
// "EA_04  ": Exact-Change OUT
// "ED_05  ": Reset da Watch-Dog
// "ED_06  ": Clear BackupRAM
// "ED_07  ": Sw Rev Update

#endif

//--------------------------------------------------------------

const char AUDIT_ID1[]		= {"ID1"};
const char AUDIT_ID1_1[] 	= { kAuditId1_1String };
const char AUDIT_ID1_2[] 	= {"*Rev. "};
const char AUDIT_ID1_7[] 	= { kAuditRevision };

const char AUDIT_ID1_1Manif[] = {"*ALP"};						// Sigla del costruttore che precede il serial number
	
#if CodMacch_10Cifre
	const char AUDIT_ID1_5[] 	= {"*0*"};
#else
	const char AUDIT_ID1_5[] 	= {"*0"};
#endif

	
uint16_t	Local_BillInfo_SwVersion;
uint16_t	Local_CHGInfo_SwVersion;
uint16_t	Local_CPCInfo_SwVersion;


const char AUDIT_DA1[]	= {"DA1"};
const char AUDIT_CA1[]	= {"CA1"};
const char AUDIT_BA1[]	= {"BA1"};
const char chr_STAR[]	= {"*"};
const char Two_STAR[]	= {"**"};
const char Three_STAR[]	= {"***"};
const char AUDIT_DA102[]= { kAuditDA102_String };				// DA102: "ALP"

//const char AUDIT_MA5_0[] = {"MA5*0*AUDIT FILE REVISION "};

const char AUDIT_ID4[]   = {"ID4"};
const char AUDIT_ID4_1[] = {"*0"};

const char AUDIT_EA3[]   = {"EA3"};
const char AUDIT_EA3_1[] = {"*"};

const char AUDIT_EA4[]   = {"EA4"};
const char AUDIT_EA5[]   = {"EA5"};
const char AUDIT_EA7[]   = {"EA7"};
const char AUDIT_PA1[]   = {"PA1"};
const char AUDIT_PA2a[]  = {"PA2"};
const char AUDIT_PA2_2[] = {"*0"};

const char AUDIT_PA2[]   = {"PA2*0*0"};
const char AUDIT_LA1_0[] = {"LA1*0"};
const char AUDIT_LA1_1[] = {"LA1*1"};
const char AUDIT_LA1_2[] = {"LA1*2"};
const char AUDIT_LA1_3[] = {"LA1*3"};
const char AUDIT_LA1_4[] = {"LA1*4"};

const char AUDIT_LA1_8[] = {"LA1*8"};
const char AUDIT_LA1_9[] = {"LA1*9"};

const char AUDIT_CA2[]   = {"CA2"};
const char AUDIT_CA3[]   = {"CA3"};
const char AUDIT_CA7[]   = {"CA7"};
const char AUDIT_CA8[]   = {"CA8"};
const char AUDIT_CA9[]   = {"CA9"};
const char AUDIT_CA14[]  = {"CA14"};
const char AUDIT_CA15[]  = {"CA15"};
const char AUDIT_DA2[]   = {"DA2"};
const char AUDIT_DA3[]   = {"DA3"};
const char AUDIT_DA4[]   = {"DA4"};
const char AUDIT_DA5[]   = {"DA5"};
const char AUDIT_DA6[]   = {"DA6"};
const char AUDIT_DA9[]   = {"DA9"};
const char AUDIT_DA99[]  = {"DA99"};
const char AUDIT_CA11[]  = {"CA11"};
const char AUDIT_CA4[]   = {"CA4"};
const char AUDIT_CA10[]  = {"CA10"};

#if kApplTokenAudit
const char AUDIT_TA2[]   = {"TA2*0*0*0*0"};
const char AUDIT_TA5[]   = {"TA5"};
#endif

const char AUDIT_VA1[]   = {"VA1"};
const char AUDIT_VA2[]   = {"VA2"};
const char AUDIT_VA3[]   = {"VA3"};

const char AUDIT_DXS[]   = {"DXS*ALP0000000*VA*V0/6*1"};

const char AUDIT_DXE[]   = {"DXE*1*1"};
const char AUDIT_CRLF[]  = {"\r\n"};
const char AUDIT_JSON_CRLF[]  = {"\\r\\n"};


/************************************************************************************/
/*  ATTENZIONE !!!!!!!!!!!!!!!                                                      */
/*  NEL CASO IN CUI VENGANO CAMBIATE LE LUNGHEZZE DELLE STRINGHE SOTTO RIPORTATE,   */
/*  RICORDARSI DI AGGIORNARE LE DEFINIZIONI IN "AuditStrutture.h"                   */ 
/************************************************************************************/

const tAString      tDXS_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DXS, sizeof(AUDIT_DXS)-1 };
const tAEndString   tDXS_01     = { AUDIT_END_STRING, END_STRING, 0, 0 };

const tAString      tDXE_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DXE, sizeof(AUDIT_DXE)-1 };
const tAEndString   tDXE_01     = { AUDIT_END_STRING, END_STRING, 0, 0 };


// ========================================================================
// ========   ID  1                     ===================================
// ========================================================================
//	"ID1*"
const tAString      tID1_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_ID1, sizeof(AUDIT_ID1)-1 };					// "ID1"
//	ID101:	"MHD" + SerialNumber 
const tAString      tID1_01_a	= { AUDIT_STRING, CONSTANT_STRING, AUDIT_ID1_1Manif, sizeof(AUDIT_ID1_1Manif)-1 };		// "ECO" 
const tADataLong    tID1_01_b   = { AUDIT_DATA, 0x40, &SerialNumber, DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+10 };			// Serial Number
//	ID102:	Machine Model
const tAString      tID1_01_c   = { AUDIT_STRING, CONSTANT_STRING, chr_STAR, sizeof(chr_STAR)-1 };
const tAString      tID1_02     = { AUDIT_STRING, CONSTANT_STRING, MachineModelNumber, 20}; 
//	ID103:	kProductSWVersionStr
const tAString      tID1_03_a    = { AUDIT_STRING, CONSTANT_STRING, chr_STAR, sizeof(chr_STAR)-1 };
const tAString      tID1_03_b    = { AUDIT_STRING, CONSTANT_STRING, gSWRevCode, 4 };
//	ID104:	Codice Locazione
const tADataInt    	tID1_04     = { AUDIT_DATA, 0, &Locazione, DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+5 };
//	ID105:	 "*0" User Defined Field
const tAString      tID1_05     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_ID1_5, sizeof(AUDIT_ID1_5)-1 };

//	ID106:	 Asset Number (Codice Macchina)
#if CodMacch_10Cifre
	const tAString      tID1_06     = { AUDIT_STRING, CONSTANT_STRING, ID106_AssetNumberCodeMacchina, EE_CodeMacchinaLen };
#else
	const tADataLong    tID1_06     = { AUDIT_DATA, 0, &CodiceMacchina, DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
#endif
	
//	ID107:	 DTS Level
const tAString      tID1_07     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_ID1_7, sizeof(AUDIT_ID1_7)-1 };
//	ID108:	 DTS Revision
const tAEndString   tID1_08     = { AUDIT_END_STRING, END_STRING, 0, 0 };


// ========================================================================
// ========   IDENTIFICATORE  DEL   PRODOTTO  BA1-CA1-DA1  ================
// ========================================================================
#if (_IcpMaster_ == true)
const tATestLA1     tBA100_0   = { AUDIT_EXEC_FUNCTION, 0, kTestBillReader, 9, 0 };										// Salta 9 strutture se non c'e' Bill Reader MDB collegato
const tAString      tBA100     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_BA1, sizeof(AUDIT_BA1)-1 };						//"BA1"
const tAString      tBA100_a   = { AUDIT_STRING, CONSTANT_STRING, &chr_STAR, 1 };							
const tAString   	tBA101     = { AUDIT_STRING, CONSTANT_STRING, &ICPBILLInfo.BillInfo.aManufactCode, sizeof(ICPBILLInfo.BillInfo.aManufactCode) };
const tAString   	tBA101_a   = { AUDIT_STRING, CONSTANT_STRING, &ICPBILLInfo.BillInfo.aSerialNumber, sizeof(ICPBILLInfo.BillInfo.aSerialNumber) };
const tAString      tBA101_b   = { AUDIT_STRING, CONSTANT_STRING, chr_STAR, 1 };
const tAString      tBA102     = { AUDIT_STRING, CONSTANT_STRING, &ICPBILLInfo.BillInfo.aModelNumber, sizeof(ICPBILLInfo.BillInfo.aModelNumber) };	
const tAString      tBA103     = { AUDIT_DATA, 0, &Local_BillInfo_SwVersion, DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tBA104     = { AUDIT_END_STRING, END_STRING, 0, 0 };

const tATestLA1     tCA100_0   = { AUDIT_EXEC_FUNCTION, 0, kTestCA1, 9, 0 };										// Salta 9 strutture se ICPCHGInfo vuota
const tAString      tCA100     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA1, sizeof(AUDIT_CA1)-1 };										//"CA1"
const tAString      tCA100_a   = { AUDIT_STRING, CONSTANT_STRING, &chr_STAR, 1 };							
const tAString   	tCA101     = { AUDIT_STRING, CONSTANT_STRING, &ICPCHGInfo.ChgInfo.aManufactCode, sizeof(ICPCHGInfo.ChgInfo.aManufactCode) };
const tAString   	tCA101_a   = { AUDIT_STRING, CONSTANT_STRING, &ICPCHGInfo.ChgInfo.aSerialNumber, sizeof(ICPCHGInfo.ChgInfo.aSerialNumber) };
const tAString      tCA101_b   = { AUDIT_STRING, CONSTANT_STRING, chr_STAR, 1 };
const tAString      tCA102     = { AUDIT_STRING, CONSTANT_STRING, &ICPCHGInfo.ChgInfo.aModelNumber, sizeof(ICPCHGInfo.ChgInfo.aModelNumber) };	
const tAString      tCA103     = { AUDIT_DATA, 0, &Local_CHGInfo_SwVersion, DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tCA104     = { AUDIT_END_STRING, END_STRING, 0, 0 };


const tATestLA1     tDA100_0   = { AUDIT_EXEC_FUNCTION, 0, kTestDA1, 9, 0 };										// Salta 9 strutture se ICPCPCInfo vuota
const tAString      tDA100     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA1, sizeof(AUDIT_DA1)-1 };										//"DA1"
const tAString      tDA100_a   = { AUDIT_STRING, CONSTANT_STRING, &chr_STAR, 1 };							
const tAString   	tDA101     = { AUDIT_STRING, CONSTANT_STRING, &ICPCPCInfo.CPCInfo.aManufactCode, sizeof(ICPCPCInfo.CPCInfo.aManufactCode) };
const tAString   	tDA101_a   = { AUDIT_STRING, CONSTANT_STRING, &ICPCPCInfo.CPCInfo.aSerialNumber, sizeof(ICPCPCInfo.CPCInfo.aSerialNumber) };
const tAString      tDA101_b   = { AUDIT_STRING, CONSTANT_STRING, chr_STAR, 1 };
const tAString      tDA102     = { AUDIT_STRING, CONSTANT_STRING, &ICPCPCInfo.CPCInfo.aModelNumber, sizeof(ICPCPCInfo.CPCInfo.aModelNumber) };	
const tAString      tDA103     = { AUDIT_DATA, 0, &Local_CPCInfo_SwVersion, DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tDA104     = { AUDIT_END_STRING, END_STRING, 0, 0 };

#endif

// =============================================================================================================================================================


//*********  DPP e Currency Description   *******************
const tAString      tID4_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_ID4, sizeof(AUDIT_ID4)-1 };
const tADataByte    tID4_01     = { AUDIT_DATA, 0, &DecimalPointPosition, DATA_NO_ZERI+DATA_NO_INDEX+DATA_BYTE+1 };
const tAString      tID4_02     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_ID4_1, sizeof(AUDIT_ID4_1)-1 };
const tADataInt     tID4_03     = { AUDIT_DATA, 0, &CurrencyCode, DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tID4_04     = { AUDIT_END_STRING, END_STRING, 0, 0 };


// ========================================================================
// ========   ID   C A S H              ===================================
// ========================================================================
//*********  CA2  Cash Vends              *******************
const tAString      tCA2_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA2, sizeof(AUDIT_CA2)-1 };
const tADataByte    tCA2_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCashVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tCA2_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCashNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tCA2_03     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCashVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tCA2_04     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCashNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tAEndString   tCA2_05     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  CA3  Cash Incassato          *******************
const tAString      tCA3_01     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA3, sizeof(AUDIT_CA3)-1 };
const tADataByte    tCA3_02     = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(LRCashBox), DATA_NO_INDEX+DATA_LONG };			// |
const tADataByte    tCA3_02b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(LRCashTub), DATA_NO_INDEX+DATA_LONG };				// |
const tADataByte    tCA3_03     = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(LRBankInp), DATA_NO_INDEX+DATA_LONG };				// |--> CA301	Value of ALL Cash In 	LR
const tADataByte    tCA3_03b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(CashFill),  DATA_NO_INDEX+DATA_LONG };				// |
const tADataByte    tCA3_04     = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };												// |
const tADataByte    tCA3_05     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCashBox), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };	//---->	CA302	Value of Cash to CashBox LR
const tADataByte    tCA3_06     = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(LRCashTub), DATA_NO_INDEX+DATA_LONG };			// |
const tADataByte    tCA3_06b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(CashFill),  DATA_NO_INDEX+DATA_LONG };				// |--> CA303	Value of Cash to Tubes   LR
const tADataByte    tCA3_06c    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };												// |

#if Debug_AudBill
const tADataByte    tCA3_07     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRBankInp), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };	//----> CA304	Value of Bills In 		LR
#else
const tAString      tCA3_07   = { AUDIT_STRING, CONSTANT_STRING, &chr_STAR, 1 };							
#endif

const tADataByte    tCA3_08     = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(INCashBox), DATA_NO_INDEX+DATA_LONG };			// |
const tADataByte    tCA3_08b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INCashTub), DATA_NO_INDEX+DATA_LONG };				// |
const tADataByte    tCA3_09     = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INBankInp), DATA_NO_INDEX+DATA_LONG };				// |-->	CA305	Value of ALL Cash In 	 IN
const tADataByte    tCA3_09b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INCashFill), DATA_NO_INDEX+DATA_LONG };			// |
const tADataByte    tCA3_10     = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };												// |
const tADataByte    tCA3_11     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCashBox), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };	//---->	CA306	Value of Cash to CashBox IN
const tADataByte    tCA3_12     = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(INCashTub), DATA_NO_INDEX+DATA_LONG };			// |
const tADataByte    tCA3_12b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INCashFill),  DATA_NO_INDEX+DATA_LONG };			// |--> CA307	Value of Cash to Tubes   IN
const tADataByte    tCA3_12c    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };												// |

#if Debug_AudBill
const tADataByte    tCA3_13     = { AUDIT_DATA, 0, IICEEPAuditOffset(INBankInp), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };	//---->	CA308	Value of Bills In 		IN
#else
const tAString      tCA3_13   = { AUDIT_STRING, CONSTANT_STRING, &chr_STAR, 1 };							
#endif

// EVA-DTS 6.1.1 Appendix A - Page 6: CA304 e CA308 devono essere riportati SOLO come CA309 e CA310 rispettivamente
const tADataByte    CA309       = { AUDIT_DATA, 0, IICEEPAuditOffset(LRBankInp), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };	//----> CA309	Value of Bills In 		LR
const tADataByte    CA310       = { AUDIT_DATA, 0, IICEEPAuditOffset(INBankInp), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };	//---->	CA310	Value of Bills In 		IN
const tAEndString   tCA3_14     = { AUDIT_END_STRING, END_STRING, 0, 0 };
// *********  CA4   Value of Cash Dispensed  **********
#if (_IcpMaster_ == true)
const tAString      tCA4_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA4, sizeof(AUDIT_CA4)-1 };
const tADataByte    tCA4_01     = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(ManDispen), DATA_NO_INDEX+DATA_LONG };
const tADataByte    tCA4_01a    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(Dispensed), DATA_NO_INDEX+DATA_LONG };
const tADataByte    tCA4_01b    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };
const tADataByte    tCA4_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(ManDispen), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tCA4_03     = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(INManDispen), DATA_NO_INDEX+DATA_LONG };
const tADataByte    tCA4_03a    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INDispensed), DATA_NO_INDEX+DATA_LONG };
const tADataByte    tCA4_03b    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };
const tADataByte    tCA4_04     = { AUDIT_DATA, 0, IICEEPAuditOffset(INManDispen), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tCA4_05     = { AUDIT_END_STRING, END_STRING, 0, 0 };
#endif

//*********  CA7  Cash  Discount               *******************
const tAString      tCA7_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA7, sizeof(AUDIT_CA7)-1 };
const tADataByte    tCA7_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCashDis), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tCA7_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCashDis), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tCA7_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };


//*********  CA8  OVERPAY   Cash               *******************
const tAString      tCA8_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA8, sizeof(AUDIT_CA8)-1 };
const tADataByte    tCA8_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(LROverpay), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tCA8_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INOverpay), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tCA8_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  CA9  Cash Vends in Exact-Change    *******************
const tAString      tCA9_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA9, sizeof(AUDIT_CA9)-1 };
const tADataByte    tCA9_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRVndExc), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tCA9_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INVndExc), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tCA9_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };

#if (_IcpMaster_ == true)
//*********  CA10  Monete  Inserite Manualmente    *******************
const tAString      tCA10_00    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA10, sizeof(AUDIT_CA10)-1 };
const tADataByte    tCA10_01    = { AUDIT_DATA, 0, IICEEPAuditOffset(CashFill), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tCA10_02    = { AUDIT_DATA, 0, IICEEPAuditOffset(INCashFill), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tCA10_03    = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  CA11  Monete  Incassate    *******************
const tABeginLoopIx tCA11_00    = { AUDIT_BEGIN_LOOP_INDEX, 0, 0, &num_icp_coin, 1 };
const tAString      tCA11_01    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA11, sizeof(AUDIT_CA11)-1 };
const tADataInt     tCA11_02    = { AUDIT_DATA, 0, &CoinValuesList[0], DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tCA11_03    = { AUDIT_DATA, 0, IICEEPAuditOffset(numCoin), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tCA11_04    = { AUDIT_DATA, 0, IICEEPAuditOffset(CashCoin), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tCA11_05    = { AUDIT_DATA, 0, IICEEPAuditOffset(TubCoin), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tAEndString   tCA11_06    = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tCA11_07    = { AUDIT_END_LOOP, END_STRING, &tCA11_01, 0 };

//*********  CA14  Banconote  Incassate    *******************
const tATestLA1     tCA14_00    = { AUDIT_EXEC_FUNCTION, 0, kTestBillReader, 7, 0 };								// Salta 7 strutture se non c'e' Bill Reader MDB collegato
const tABeginLoopIx tCA14_01    = { AUDIT_BEGIN_LOOP_INDEX, 0, 0, &num_icp_bill, 1 };
const tAString      tCA14_02    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA14, sizeof(AUDIT_CA14)-1 };
const tADataInt     tCA14_03    = { AUDIT_DATA, 0, &BVHBillValuesList[0], DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tCA14_04    = { AUDIT_DATA, 0, IICEEPAuditOffset(numBank), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tAEndString   tCA14_05    = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tCA14_06    = { AUDIT_END_LOOP, END_STRING, &tCA14_02, 0 };

//*********  CA15  Valore delle monete nei Tubi *******************
const tATestLA1     tCA15_00    = { AUDIT_EXEC_FUNCTION, 0, kTubesCoinValGet, 4, 0 };								// Salta 4 strutture se non c'e' Changer MDB collegato
const tAString      tCA15_01    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA15, sizeof(AUDIT_CA15)-1 };
const tADataByte    tCA15_02    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };
const tAEndString   tCA15_03    = { AUDIT_END_STRING, END_STRING, 0, 0 };
#endif

#if  (_IcpMaster_ == false)
//*********  CA11  Monete  Incassate    *******************
const tABeginLoopIx tCA11_00    = { AUDIT_BEGIN_LOOP_INDEX, 0, 0, &num_icp_coin, 1 };
const tAString      tCA11_01    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_CA11, sizeof(AUDIT_CA11)-1 };
const tADataInt     tCA11_02    = { AUDIT_DATA, 0, &CoinValuesList[0], DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tCA11_03    = { AUDIT_DATA, 0, IICEEPAuditOffset(numCoin), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tCA11_04    = { AUDIT_DATA, 0, IICEEPAuditOffset(CashCoin), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tCA11_05    = { AUDIT_DATA, 0, IICEEPAuditOffset(TubCoin), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tAEndString   tCA11_06    = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tCA11_07    = { AUDIT_END_LOOP, END_STRING, &tCA11_01, 0 };
#endif

// ========================================================================
// ========   ID   C A R T E            ===================================
// ========================================================================
//*********  DA2  Vendite con Carta     *******************
const tAString      tDA2_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA2, sizeof(AUDIT_DA2)-1 };
const tADataByte    tDA2_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCardVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA2_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCardNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tDA2_03     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCardVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA2_04     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCardNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tAEndString   tDA2_06     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  DA3  Addebitato alle Carte     *******************
const tAString      tDA3_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA3, sizeof(AUDIT_DA3)-1 };
const tADataByte    tDA3_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCardDeb), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA3_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCardDeb), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA3_03     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRManCard), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tDA3_06     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  DA4  Accreditato alle Carte     *******************
const tAString      tDA4_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA4, sizeof(AUDIT_DA4)-1 };
const tADataByte    tDA4_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCardCre), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA4_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCardCre), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA4_03     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCreFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tDA4_04     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  DA5  Sconti alle Carte     *******************
const tAString      tDA5_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA5, sizeof(AUDIT_DA5)-1 };
const tADataByte    tDA5_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCardDis), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA5_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(NumCardDis), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tDA5_03     = { AUDIT_DATA, 0, IICEEPAuditOffset(INCardDis), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tDA5_04     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  DA6  FreeCredit Accreditato alle Carte     *******************
const tAString      tDA6_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA6, sizeof(AUDIT_DA6)-1 };
const tADataByte    tDA6_01	    = { AUDIT_DATA, 0, IICEEPAuditOffset(INCreFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA6_02 	= { AUDIT_DATA, 0, IICEEPAuditOffset(LRCreFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tDA6_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  DA9  OVERPAY   Cashless            *******************
const tAString      tDA9		= { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA9, sizeof(AUDIT_DA9)-1 };
const tADataByte    tDA901	    = { AUDIT_DATA, 0, IICEEPAuditOffset(LROverpayCard), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA902		= { AUDIT_DATA, 0, IICEEPAuditOffset(INOverpayCard), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tDA9_End	= { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  DA99  FreeCredit Addebitato alle Carte     *******************
const tAString      tDA99_00    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA99, sizeof(AUDIT_DA99)-1 };
const tADataByte    tDA99_01	= { AUDIT_DATA, 0, IICEEPAuditOffset(INDebFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA99_02 	= { AUDIT_DATA, 0, IICEEPAuditOffset(LRDebFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tDA99_03    = { AUDIT_END_STRING, END_STRING, 0, 0 };


#if Debug_DA1000														// Per debug con Audit Control
//*********  DA100  FreeCredit Accreditato e Addebitato alle Carte     *******************
const byte AUDIT_DA10[]  = {"DA10"};
const tAString      tDA10_00    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_DA10, sizeof(AUDIT_DA10)-1 };
const tADataByte    tDA10_01    = { AUDIT_DATA, 0, IICEEPAuditOffset(INDebFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA10_02    = { AUDIT_DATA, 0, IICEEPAuditOffset(LRDebFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA10_03    = { AUDIT_DATA, 0, IICEEPAuditOffset(INCreFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tDA10_04    = { AUDIT_DATA, 0, IICEEPAuditOffset(LRCreFRCR), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tDA10_05    = { AUDIT_END_STRING, END_STRING, 0, 0 };
#endif


// ========================================================================
// ========   ID     E V E N T I        ===================================
// ========================================================================



// ========================================================================
// ========   P R E Z Z I  PA1-PA2  E   L I S T E   P R E Z Z I   LA1  ====
// ========================================================================
const tABeginLoop   tPA1_00     = { AUDIT_BEGIN_LOOP, 0, 0, MRPPricesListLenght99-1, 1 };
const tATestData   	tPA1_00a    = { AUDIT_TEST_PRICE, 0, 0, 1 };														// Se Price non valido salta a tPA1_05
const tAString      tPA1_01     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_PA1, sizeof(AUDIT_PA1)-1 };
const tALoopIndex   tPA1_02     = { AUDIT_LOOP_INDEX, 0, 1, DATA_SI_ZERI+DATA_NO_INDEX+2, 0 };
const tADataInt     tPA1_03     = { AUDIT_DATA, 0, &MRPPrices.Prices99.MRPPricesList99[0], DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tAEndString   tPA1_04     = { AUDIT_END_STRING, 0, 0, 0 };
#if kApplVendCountersSupport
const tAString      tPA2_01a    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_PA2a, sizeof(AUDIT_PA2a)-1 };
const tADataByte    tPA2_02a    = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(INCashNumSel), DATA_SI_INDEX+DATA_LONG };
const tADataByte    tPA2_03a    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INCardNumSel), DATA_SI_INDEX+DATA_LONG };
const tADataByte    tPA2_04a    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tAString      tPA2_05a    = { AUDIT_STRING, CONSTANT_STRING, AUDIT_PA2_2, sizeof(AUDIT_PA2_2)-1 };
#else
const tAString      tPA2_01     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_PA2, sizeof(AUDIT_PA2)-1 };
#endif
const tADataByte    tPA2_02     = { AUDIT_COPY_DATA, 0, IICEEPAuditOffset(NumSelFullPrice), DATA_SI_INDEX+DATA_INT };
const tADataByte    tPA2_03     = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(NumSelDiscount1), DATA_SI_INDEX+DATA_INT };
#if !kAuditLA1OldModeSupport
const tADataByte    tPA2_03b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(NumSelDiscount2), DATA_SI_INDEX+DATA_INT };
const tADataByte    tPA2_03c    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(NumSelDiscount3), DATA_SI_INDEX+DATA_INT };
const tADataByte    tPA2_03d    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(NumSelDiscount4), DATA_SI_INDEX+DATA_INT };
#endif
const tADataByte    tPA2_04     = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tADataByte    tPA2_05     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRSelVnd), DATA_NO_ZERI+DATA_SI_INDEX+DATA_LONG+8 };
const tAEndString   tPA2_06     = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tPA1_05     = { AUDIT_END_LOOP, END_STRING, &tPA1_00a, 15 };										// Numero di strutture da saltare alla fine del loop


//*********  LISTA  PREZZI 0   LA1 - 0     *******************
const tATestLA1     tLA1_0_0    = { AUDIT_EXEC_FUNCTION, 0, kTestLA1, 9, kLA1_0 };										// Salta 9 strutture se array "NumSelFullPrice" vuoto
const tABeginLoop   tLA1_0_00   = { AUDIT_BEGIN_LOOP, 0, 0, MRPPricesListLenght99-1, 1 };
const tATestData   	tLA1_0_00a  = { AUDIT_TEST_PRICE, 0, 0, 2 };														// Se Price non valido salta a tLA1_0_06
const tAString      tLA1_0_01   = { AUDIT_STRING, CONSTANT_STRING, AUDIT_LA1_0, sizeof(AUDIT_LA1_0)-1 };
const tALoopIndex   tLA1_0_02   = { AUDIT_LOOP_INDEX, 0, 1, DATA_SI_ZERI+DATA_NO_INDEX+2, 0 };
const tADataInt     tLA1_0_03   = { AUDIT_DATA, 0, &MRPPrices.Prices99.MRPPricesList99[0], DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tADataByte    tLA1_0_04   = { AUDIT_DATA, 0, IICEEPAuditOffset(NumSelFullPrice), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
#if kApplVendCountersSupport
const tADataByte    tLA1_0_04a  = { AUDIT_DATA, 0, IICEEPAuditOffset(INCashNumSel), DATA_NO_ZERI+DATA_SI_INDEX+DATA_LONG+6 };
#endif
const tAEndString   tLA1_0_05   = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tLA1_0_06   = { AUDIT_END_LOOP, END_STRING, &tLA1_0_00a, 7 };										// Numero di strutture da saltare alla fine del loop


//*********  LISTA  PREZZI 1   LA1 - 1     *******************
const tATestLA1     tLA1_1_0    = { AUDIT_EXEC_FUNCTION, 0, kTestLA1, 11, kLA1_1 };										// Salta 11 strutture se array "NumSelDiscount1" vuoto
const tABeginLoop   tLA1_1_00   = { AUDIT_BEGIN_LOOP, 0, 0, MRPPricesListLenght99-1, 1 };
const tATestData   	tLA1_1_00a  = { AUDIT_TEST_PRICE, 0, 0, 3 };														// Se Price non valido salta a tLA1_1_08
const tAString      tLA1_1_01   = { AUDIT_STRING, CONSTANT_STRING, AUDIT_LA1_1, sizeof(AUDIT_LA1_1)-1 };
const tALoopIndex   tLA1_1_02   = { AUDIT_LOOP_INDEX, 0, 1, DATA_SI_ZERI+DATA_NO_INDEX+2, 0 };
const tADataInt     tLA1_1_03   = { AUDIT_COPY_DATA, 0, &MRPPrices.Prices99.MRPPricesList99[0], DATA_SI_INDEX+DATA_INT };
const tADataInt     tLA1_1_04   = { AUDIT_PRICEDISC_DATA, 0, (uint*)IICEEPConfigAddr(EEPrezzoPA), DATA_SI_INDEX+DATA_INT };
const tADataByte    tLA1_1_05   = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tADataByte    tLA1_1_06   = { AUDIT_DATA, 0, IICEEPAuditOffset(NumSelDiscount1), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
#if kApplVendCountersSupport
const tADataByte    tLA1_1_06a  = { AUDIT_DATA, 0, IICEEPAuditOffset(INCardNumSel), DATA_NO_ZERI+DATA_SI_INDEX+DATA_LONG+6 };
#endif
const tAEndString   tLA1_1_07   = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tLA1_1_08   = { AUDIT_END_LOOP, END_STRING, &tLA1_1_00a, 9 };										// Numero di strutture da saltare alla fine del loop



//*********  LISTA  PREZZI 2   LA1 - 2     *******************
const tATestLA1     tLA1_2_0    = { AUDIT_EXEC_FUNCTION, 0, kTestLA1, 11, kLA1_2 };										// Salta 11 strutture se array "NumSelDiscount2" vuoto
const tABeginLoop   tLA1_2_00   = { AUDIT_BEGIN_LOOP, 0, 0, MRPPricesListLenght99-1, 1 };
const tATestData   	tLA1_2_00a  = { AUDIT_TEST_PRICE, 0, 0, 4 };														// Se Price non valido salta a tLA1_2_08
const tAString      tLA1_2_01   = { AUDIT_STRING, CONSTANT_STRING, AUDIT_LA1_2, sizeof(AUDIT_LA1_2)-1 };
const tALoopIndex   tLA1_2_02   = { AUDIT_LOOP_INDEX, 0, 1, DATA_SI_ZERI+DATA_NO_INDEX+2, 0 };
const tADataInt     tLA1_2_03   = { AUDIT_COPY_DATA, 0, &MRPPrices.Prices99.MRPPricesList99[0], DATA_SI_INDEX+DATA_INT };
const tADataInt     tLA1_2_04   = { AUDIT_PRICEDISC_DATA, 0, (uint*)IICEEPConfigAddr(EEPrezzoPB), DATA_SI_INDEX+DATA_INT };
const tADataByte    tLA1_2_05   = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tADataByte    tLA1_2_06   = { AUDIT_DATA, 0, IICEEPAuditOffset(NumSelDiscount2), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tAEndString   tLA1_2_07   = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tLA1_2_08   = { AUDIT_END_LOOP, END_STRING, &tLA1_2_00a, 9 };										// Numero di strutture da saltare alla fine del loop

//*********  LISTA  PREZZI 3   LA1 - 3     *******************
const tATestLA1     tLA1_3_0    = { AUDIT_EXEC_FUNCTION, 0, kTestLA1, 11, kLA1_3 };										// Salta 11 strutture se array "NumSelDiscount3" vuoto
const tABeginLoop   tLA1_3_00   = { AUDIT_BEGIN_LOOP, 0, 0, MRPPricesListLenght99-1, 1 };
const tATestData   	tLA1_3_00a  = { AUDIT_TEST_PRICE, 0, 0, 5 };														// Se Price non valido salta a tLA1_3_08
const tAString      tLA1_3_01   = { AUDIT_STRING, CONSTANT_STRING, AUDIT_LA1_3, sizeof(AUDIT_LA1_3)-1 };
const tALoopIndex   tLA1_3_02   = { AUDIT_LOOP_INDEX, 0, 1, DATA_SI_ZERI+DATA_NO_INDEX+2, 0 };
const tADataInt     tLA1_3_03   = { AUDIT_COPY_DATA, 0, &MRPPrices.Prices99.MRPPricesList99[0], DATA_SI_INDEX+DATA_INT };
const tADataInt     tLA1_3_04   = { AUDIT_PRICEDISC_DATA, 0, (uint*)IICEEPConfigAddr(EEPrezzoPC), DATA_SI_INDEX+DATA_INT };
const tADataByte    tLA1_3_05   = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tADataByte    tLA1_3_06   = { AUDIT_DATA, 0, IICEEPAuditOffset(NumSelDiscount3), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tAEndString   tLA1_3_07   = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tLA1_3_08   = { AUDIT_END_LOOP, END_STRING, &tLA1_3_00a, 9 };										// Numero di strutture da saltare alla fine del loop

//*********  LISTA  PREZZI 4   LA1 - 4     *******************
const tATestLA1     tLA1_4_0    = { AUDIT_EXEC_FUNCTION, 0, kTestLA1, 11, kLA1_4 };										// Salta 11 strutture se array "NumSelDiscount4" vuoto
const tABeginLoop   tLA1_4_00   = { AUDIT_BEGIN_LOOP, 0, 0, MRPPricesListLenght99-1, 1 };
const tATestData   	tLA1_4_00a  = { AUDIT_TEST_PRICE, 0, 0, 6 };														// Se Price non valido salta a tLA1_4_08
const tAString      tLA1_4_01   = { AUDIT_STRING, CONSTANT_STRING, AUDIT_LA1_4, sizeof(AUDIT_LA1_4)-1 };
const tALoopIndex   tLA1_4_02   = { AUDIT_LOOP_INDEX, 0, 1, DATA_SI_ZERI+DATA_NO_INDEX+2, 0 };
const tADataInt     tLA1_4_03   = { AUDIT_COPY_DATA, 0, &MRPPrices.Prices99.MRPPricesList99[0], DATA_SI_INDEX+DATA_INT };
const tADataInt     tLA1_4_04   = { AUDIT_PRICEDISC_DATA, 0, (uint*)IICEEPConfigAddr(EEPrezzoPD), DATA_SI_INDEX+DATA_INT };
const tADataByte    tLA1_4_05   = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tADataByte    tLA1_4_06   = { AUDIT_DATA, 0, IICEEPAuditOffset(NumSelDiscount4), DATA_NO_ZERI+DATA_SI_INDEX+DATA_INT+6 };
const tAEndString   tLA1_4_07   = { AUDIT_END_STRING, 0, 0, 0 };
const tAEndLoop     tLA1_4_08   = { AUDIT_END_LOOP, END_STRING, &tLA1_4_00a, 9 };										// Numero di strutture da saltare alla fine del loop


// ========================================================================
// ========    T O T A L E    V E N D S       =============================
// ========================================================================
const tAString      tVA1_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_VA1, sizeof(AUDIT_VA1)-1 };
const tADataByte    tVA1_01a    = { AUDIT_COPY_DATA,0, IICEEPAuditOffset(INCashVnd), DATA_NO_INDEX+DATA_LONG }; //  CA201
const tADataByte    tVA1_01b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INCardVnd), DATA_NO_INDEX+DATA_LONG }; // +DA201
#if kAuditLA1OldModeSupport
const tADataByte    tVA1_01c    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INVndExc),  DATA_NO_INDEX+DATA_LONG }; // +CA902
#endif
const tADataByte    tVA1_01d    = { AUDIT_SUB_DATA, 0, IICEEPAuditOffset(INCashDis), DATA_NO_INDEX+DATA_LONG }; // -CA702
const tADataByte    tVA1_01e    = { AUDIT_SUB_DATA, 0, IICEEPAuditOffset(INCardDis), DATA_NO_INDEX+DATA_LONG }; // -DA503
const tADataByte    tVA1_01f    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };
const tADataByte    tVA1_02a    = { AUDIT_COPY_DATA,0, IICEEPAuditOffset(INCashNum), DATA_NO_INDEX+DATA_INT };  //  CA202
const tADataByte    tVA1_02b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(INCardNum), DATA_NO_INDEX+DATA_INT };  // +DA202
const tADataByte    tVA1_02c    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tADataByte    tVA1_03a    = { AUDIT_COPY_DATA,0, IICEEPAuditOffset(LRCashVnd), DATA_NO_INDEX+DATA_LONG }; //  CA203
const tADataByte    tVA1_03b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(LRCardVnd), DATA_NO_INDEX+DATA_LONG }; // +DA203
#if kAuditLA1OldModeSupport
const tADataByte    tVA1_03c    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(LRVndExc), DATA_NO_INDEX+DATA_LONG };  // +CA901
#endif
const tADataByte    tVA1_03d    = { AUDIT_SUB_DATA, 0, IICEEPAuditOffset(LRCashDis), DATA_NO_INDEX+DATA_LONG }; // -CA701
const tADataByte    tVA1_03e    = { AUDIT_SUB_DATA, 0, IICEEPAuditOffset(LRCardDis), DATA_NO_INDEX+DATA_LONG }; // -DA501
const tADataByte    tVA1_03f    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+8 };
const tADataByte    tVA1_04a    = { AUDIT_COPY_DATA,0, IICEEPAuditOffset(LRCashNum), DATA_NO_INDEX+DATA_INT };  //  CA204
const tADataByte    tVA1_04b    = { AUDIT_SUM_DATA, 0, IICEEPAuditOffset(LRCardNum), DATA_NO_INDEX+DATA_INT };  // +DA203
const tADataByte    tVA1_04c    = { AUDIT_RESULT_DATA, 0, 0, DATA_NO_ZERI+6 };
const tAEndString   tVA1_end    = { AUDIT_END_STRING, END_STRING, 0, 0 };

// ************  VA2   Test  Vends  **************
const tAString      tVA2_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_VA2, sizeof(AUDIT_VA2)-1 };
const tADataByte    tVA2_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(INTestVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tVA2_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INTestNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tVA2_03     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRTestVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tVA2_04     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRTestNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tVA2_05     = { AUDIT_END_STRING, END_STRING, 0, 0 };

const tAString      tVA3_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_VA3, sizeof(AUDIT_VA3)-1 };
const tADataByte    tVA3_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(INFreeVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tVA3_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INFreeNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tVA3_03     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRFreeVnd), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tVA3_04     = { AUDIT_DATA, 0, IICEEPAuditOffset(LRFreeNum), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tVA3_05     = { AUDIT_END_STRING, END_STRING, 0, 0 };


// ========================================================================
// ========   T O K E N           =========================================
// ========================================================================
#if (_IcpMaster_ == true)
  #if kApplTokenAudit
const tAString      tTA2_00   = { AUDIT_STRING, CONSTANT_STRING, AUDIT_TA2, sizeof(AUDIT_TA2)-1 };
const tADataByte    tTA2_01   = { AUDIT_DATA, 0, IICEEPAuditOffset(INTokenVend), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tTA2_02   = { AUDIT_DATA, 0, IICEEPAuditOffset(INNumSelecToken), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tADataByte    tTA2_03   = { AUDIT_DATA, 0, IICEEPAuditOffset(LRTokenVend), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tTA2_04   = { AUDIT_DATA, 0, IICEEPAuditOffset(LRNumSelecToken), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tTA2_05     = { AUDIT_END_STRING, END_STRING, 0, 0 };

const tAString      tTA5_00   = { AUDIT_STRING, CONSTANT_STRING, AUDIT_TA5, sizeof(AUDIT_TA5)-1 };
const tADataByte    tTA5_01   = { AUDIT_DATA, 0, IICEEPAuditOffset(LRTokenOverPay), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataByte    tTA5_02   = { AUDIT_DATA, 0, IICEEPAuditOffset(INTokenOverPay), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tTA5_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };
  #endif
#endif


// ========================================================================
// ========    E V E N T I                    =============================
// ========================================================================

#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
const tABeginLoop   tEA1_00     = { AUDIT_BEGIN_LOOP, 0, 0, EVENT_NUMBER, 1 };
const tATestData    tEA1_01     = { AUDIT_TEST_EVENT, 0, 0, 1 };
const tAString      tEA1_02     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_EA1, sizeof(AUDIT_EA1)-1};									// "EA1*"
const tAMessIndex   tEA101      = { AUDIT_MESS_INDEX, MESSAGE_STRING, (byte *)DESCR_EA1, sizeof(DESCR_EA1)-1 };						// EA101 Event Description
const tADataByte    tEA102      = { AUDIT_DATA, 0, IICEEPAuditOffset(DateEventList), DATA_SI_ZERI+DATA_SI_INDEX+DATA_LONG+6 };		// EA102 Event Date
const tADataByte    tEA103      = { AUDIT_DATA, 0, IICEEPAuditOffset(TimeEventList), DATA_SI_ZERI+DATA_SI_INDEX+DATA_INT+4 };		// EA103 Event Time
const tAString      tEA104_5   	= { AUDIT_STRING, CONSTANT_STRING, Three_STAR, sizeof(Three_STAR)-1 };								// EA104+EA105 Event Duration in minutes and msec
const tAMessIndex   tEA106      = { AUDIT_MESS_INDEX, MESSAGE_STRING, (byte *)DESCR_EA106, sizeof(DESCR_EA106)-1 };					// EA106 User Define Data: additional information
const tAEndString   tEA1_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };
const tAEndLoop     tEA1_04     = { AUDIT_END_LOOP, END_STRING, &tEA1_01, 9 };
#endif

//*********  Date/Time Prelievo Attuale e precedente   *******************
const tAString      tEA3_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_EA3, sizeof(AUDIT_EA3)-1 };
const tADataByte    tEA3_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(NumReads), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tADataLong    tEA3_02     = { AUDIT_DATA, 0, &SaveData, DATA_SI_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataInt     tEA3_03     = { AUDIT_DATA, 0, &SaveTime, DATA_SI_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAString      tEA3_04     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_EA3_1, sizeof(AUDIT_EA3_1)-1 };
const tADataByte    tEA3_05     = { AUDIT_DATA, 0, IICEEPAuditOffset(DateLast), DATA_SI_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEA3_06     = { AUDIT_DATA, 0, IICEEPAuditOffset(TimeLast), DATA_SI_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tEA3_07     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  Date/Time Inizializzazione Memoria   *******************
const tAString      tEA4_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_EA4, sizeof(AUDIT_EA4)-1 };
const tADataByte    tEA4_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(DateInit), DATA_SI_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEA4_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(TimeInit), DATA_SI_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tEA4_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  Date/Time Modifica Prezzi  *******************
const tAString      tEA5_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_EA5, sizeof(AUDIT_EA5)-1 };
const tADataByte    tEA5_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(DatePrice), DATA_SI_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEA5_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(TimePrice), DATA_SI_ZERI+DATA_NO_INDEX+DATA_INT+4 };
const tAEndString   tEA5_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  Numero di OFF-ON            *******************
const tAString      tEA7_00     = { AUDIT_STRING, CONSTANT_STRING, AUDIT_EA7, sizeof(AUDIT_EA7)-1 };
const tADataByte    tEA7_01     = { AUDIT_DATA, 0, IICEEPAuditOffset(LROutage), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEA7_02     = { AUDIT_DATA, 0, IICEEPAuditOffset(INOutage), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tAEndString   tEA7_03     = { AUDIT_END_STRING, END_STRING, 0, 0 };


//*********  "EA2*ED_01" "Prezzo Non in Lista"      *******************
const tAString     	tEVENT_1_00 = { AUDIT_STRING, CONSTANT_STRING, mSelezSenzaPrezzo, sizeof(mSelezSenzaPrezzo)-1 };
const tADataByte    tEVENT_1_01 = { AUDIT_DATA, 0, IICEEPAuditOffset(LREvent1), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tEVENT_1_02 = { AUDIT_DATA, 0, IICEEPAuditOffset(INEvent1), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEVENT_1_03 = { AUDIT_DATA, 0, IICEEPAuditOffset(LREv1Val), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tEVENT_1_04 = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  "EA2*ED_03" "Accredito dubbio"         *******************
const tAString		tEVENT_3_00 = { AUDIT_STRING, CONSTANT_STRING, mAccreditoDubbio, sizeof(mAccreditoDubbio)-1 };
const tADataByte    tEVENT_3_01 = { AUDIT_DATA, 0, IICEEPAuditOffset(LREvent3), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tEVENT_3_02 = { AUDIT_DATA, 0, IICEEPAuditOffset(INEvent3), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEVENT_3_03 = { AUDIT_DATA, 0, IICEEPAuditOffset(LREv3Val), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tEVENT_3_04 = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  "EA2*ED_04" "Caricamento a Blocchi"        *******************
const tAString      tEVENT_8_00 = { AUDIT_STRING, CONSTANT_STRING, mCaricamBlocchi, sizeof(mCaricamBlocchi)-1  };
const tADataByte    tEVENT_8_01 = { AUDIT_DATA, 0, IICEEPAuditOffset(LREvent8), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tEVENT_8_02 = { AUDIT_DATA, 0, IICEEPAuditOffset(INEvent8), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEVENT_8_03 = { AUDIT_DATA, 0, IICEEPAuditOffset(LREv8Val), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tEVENT_8_05 = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  "EA2*ED_07" "WD Reset da No Sync MDB"        *******************
const tAString      tEK_07_Event_00 = { AUDIT_STRING, CONSTANT_STRING, mResNoSyncMDB, sizeof(mResNoSyncMDB)-1  };
const tADataByte    tEK_07_Event_01 = { AUDIT_DATA, 0, IICEEPAuditOffset(LR_EK_07_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tEK_07_Event_02 = { AUDIT_DATA, 0, IICEEPAuditOffset(IN_EK_07_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tAEndString   tEK_07_Event_03 = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  "EA2*ED_08" "WD Reset da Chiave USB"        *******************
const tAString      tEK_08_Event_00 = { AUDIT_STRING, CONSTANT_STRING, mResChiaveUSB, sizeof(mResChiaveUSB)-1  };
const tADataByte    tEK_08_Event_01 = { AUDIT_DATA, 0, IICEEPAuditOffset(LR_EK_08_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tEK_08_Event_02 = { AUDIT_DATA, 0, IICEEPAuditOffset(IN_EK_08_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tAEndString   tEK_08_Event_03 = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  "EA2*ED_09" "Overpay Caricamento a Blocchi"        *******************
const tAString      tEK09_EA201   = { AUDIT_STRING, CONSTANT_STRING, mCaricamBlkOverpay, sizeof(mCaricamBlkOverpay)-1  };
const tAString      tEK09_EA202_4 = { AUDIT_STRING, CONSTANT_STRING, Two_STAR, sizeof(Two_STAR)-1 };
const tADataByte    tEK09_EA205   = { AUDIT_DATA, 0, IICEEPAuditOffset(LREv8Ovp), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tEK09_End     = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  "EA2*ED_10" "Carte Inizializzate"        *******************
const tAString      tEK10_EA201 = { AUDIT_STRING, CONSTANT_STRING, mCarteInizializzate, sizeof(mCarteInizializzate)-1  };
const tADataByte    tEK10_EA202 = { AUDIT_DATA, 0, IICEEPAuditOffset(LR_EK_10_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tEK10_EA203 = { AUDIT_DATA, 0, IICEEPAuditOffset(IN_EK_10_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tAEndString   tEK10_End   = { AUDIT_END_STRING, END_STRING, 0, 0 };

//*********  "EA2*ED_02" "Payout Interrotto"        *******************
const tAString     	tEA_01_Event_00 = { AUDIT_STRING, CONSTANT_STRING, mPayoutInterrotto, sizeof(mPayoutInterrotto)-1 };
const tADataByte    tEA_01_Event_01 = { AUDIT_DATA, 0, IICEEPAuditOffset(LR_EA_01_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_INT+6 };
const tADataByte    tEA_01_Event_02 = { AUDIT_DATA, 0, IICEEPAuditOffset(IN_EA_01_Event), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+6 };
const tADataByte    tEA_01_Event_03 = { AUDIT_DATA, 0, IICEEPAuditOffset(LR_EA_01_Val), DATA_NO_ZERI+DATA_NO_INDEX+DATA_LONG+8 };
const tAEndString   tEA_01_Event_04 = { AUDIT_END_STRING, END_STRING, 0, 0 };




// =========================================================================================
// ========   Struttura ID Audit per CPU  in  MDB  Master              =====================
// =========================================================================================

const tAuditTable   tAuditTable_MDB_Master[]  = {
											 &tDXS_00,		
											 &tDXS_01,

											 &tID1_00,
											 &tID1_01_a,
											 &tID1_01_b,
											 &tID1_01_c,
											 &tID1_02,
											 &tID1_03_a,
											 &tID1_03_b,
											 &tID1_04,
											 &tID1_05,
											 &tID1_06,
											 &tID1_07,
											 &tID1_08,
											 
											 &tBA100_0,
											 &tBA100,
											 &tBA100_a,
											 &tBA101,
											 &tBA101_a, 
											 &tBA101_b, 
											 &tBA102,
											 &tBA103,
											 &tBA104,
											 
											 &tCA100_0,
											 &tCA100,
											 &tCA100_a,
											 &tCA101,
											 &tCA101_a, 
											 &tCA101_b, 
											 &tCA102,
											 &tCA103,
											 &tCA104,

											 &tDA100_0,
											 &tDA100,
											 &tDA100_a,
											 &tDA101,
											 &tDA101_a,
											 &tDA101_b,
											 &tDA102,
											 &tDA103,
											 &tDA104,
											 
#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
											 &tEA1_00,
											 &tEA1_01,					// Il numero di strutture da qui alla fine del loop deve essere 
											 &tEA1_02,					// riportato nell'ultimo byte della struttura "tAEndLoop".
											 &tEA101,					// In questo caso, da tEA1_01 a tEA1_04 compresi, abbiamo 9 strutture:
											 &tEA102,					// riporteremo il numero 9 nella struttura indirizzata dall'ultimo elemento (tEA1_04)
											 &tEA103,
											 &tEA104_5,
											 &tEA106,
											 &tEA1_03,
											 &tEA1_04,
#endif

											 &tEA3_00,
											 &tEA3_01,
											 &tEA3_02,
											 &tEA3_03,
											 &tEA3_04,
											 &tEA3_05,
											 &tEA3_06,
											 &tEA3_07,
											 
											 &tEA4_00,
											 &tEA4_01,
											 &tEA4_02,
											 &tEA4_03,
											 &tEA5_00,
											 &tEA5_01,
											 &tEA5_02,
											 &tEA5_03,
											 &tEA7_00,
											 &tEA7_01,
											 &tEA7_02,
											 &tEA7_03,

											 &tID4_00,
											 &tID4_01,
											 //&tID4_02,
											 &tID4_03,
											 &tID4_04,

											 &tCA2_00,
											 &tCA2_01,
											 &tCA2_02,
											 &tCA2_03,
											 &tCA2_04,
											 &tCA2_05,

											 &tCA3_01,
											 &tCA3_02,
											 &tCA3_02b,
											 &tCA3_03,
											 &tCA3_03b,
											 &tCA3_04,
											 &tCA3_05,
#if !kAuditLA1OldModeSupport
											 &tCA3_06,
											 &tCA3_06b,
											 &tCA3_06c,
#else
											 &tCA3_06,
#endif
											 &tCA3_07,
											 &tCA3_08,
											 &tCA3_08b,
											 &tCA3_09,
											 &tCA3_09b,
											 &tCA3_10,
											 &tCA3_11,
#if !kAuditLA1OldModeSupport
											 &tCA3_12,
											 &tCA3_12b,
											 &tCA3_12c,
#else
											 &tCA3_12,
#endif
											 &tCA3_13,
											 &CA309,
											 &CA310,
											 &tCA3_14,	

											 &tCA4_00,
											 &tCA4_01,
											 &tCA4_01a,
											 &tCA4_01b,
											 &tCA4_02,
											 &tCA4_03,
											 &tCA4_03a,
											 &tCA4_03b,
											 &tCA4_04,
											 &tCA4_05,

											 &tCA7_00,							// Cash discount ?
											 &tCA7_01,
											 &tCA7_02,
											 &tCA7_03,

											 &tCA8_00,
											 &tCA8_01,
											 &tCA8_02,
											 &tCA8_03,
											 
											 &tCA9_00,
											 &tCA9_01,
											 &tCA9_02,
											 &tCA9_03,

											 &tCA10_00,							// Monete inserite manualmente
											 &tCA10_01,
											 &tCA10_02,
											 &tCA10_03,

											 &tCA11_00,
											 &tCA11_01,
											 &tCA11_02,
											 &tCA11_03,
											 &tCA11_04,
											 &tCA11_05,
											 &tCA11_06,
											 &tCA11_07,

											 &tCA14_00,							// Banconote incassate
											 &tCA14_01,
											 &tCA14_02,
											 &tCA14_03,
											 &tCA14_04,
											 &tCA14_05,
											 &tCA14_06,
											 
											 &tCA15_00,							// Valore delle monete nei tubi
											 &tCA15_01,
											 &tCA15_02,
											 &tCA15_03,
											 
											 &tPA1_00,
											 &tPA1_00a,
											 &tPA1_01,
											 &tPA1_02,
											 &tPA1_03,
											 &tPA1_04,
#if kApplVendCountersSupport
											 &tPA2_01a,
#else
											 &tPA2_01,
#endif											 
											 &tPA2_02,
											 &tPA2_03,
											 &tPA2_03b,
											 &tPA2_03c,
											 &tPA2_03d,
											 &tPA2_04,
											 &tPA2_05,
											 &tPA2_06,
											 &tPA1_05,							// Riportare 15 strutture (da tPA1_00a a qui) in "tPA1_05"

/*     09.17  	Tolte anche le Liste LA0 e LA1 perche' se il CPC MDB attribuisce sconti alla selezione non posso avere 2000 liste con i valori
 * 				che le selezioni possono assumere
//											 &tLA1_0_0,
											 &tLA1_0_00,
                                             &tLA1_0_00a,
                                             &tLA1_0_01,
                                             &tLA1_0_02,
                                             &tLA1_0_03,
                                             &tLA1_0_04,
                                             &tLA1_0_05,
                                             &tLA1_0_06,						// Riportare 7 strutture (da tLA1_0_00a a qui) in "tLA1_0_06"

//                                             &tLA1_1_0,
                                             &tLA1_1_00,
                                             &tLA1_1_00a,
                                             &tLA1_1_01,
                                             &tLA1_1_02,
                                             &tLA1_1_03,
                                             &tLA1_1_04,
                                             &tLA1_1_05,
                                             &tLA1_1_06,
                                             &tLA1_1_07,
                                             &tLA1_1_08,						// Riportare 9 strutture (da tLA1_1_00a a qui) in "tLA1_1_08"
 */ 											 


/*										 
											 &tLA1_2_0,
											 &tLA1_2_00,
											 &tLA1_2_00a,
                                             &tLA1_2_01,
                                             &tLA1_2_02,
                                             &tLA1_2_03,
                                             &tLA1_2_04,
                                             &tLA1_2_05,
                                             &tLA1_2_06,
                                             &tLA1_2_07,
                                             &tLA1_2_08,						// Riportare 9 strutture (da tLA1_2_00a a qui) in "tLA1_2_08"

											 &tLA1_3_0,
											 &tLA1_3_00,
											 &tLA1_3_00a,
                                             &tLA1_3_01,
                                             &tLA1_3_02,
                                             &tLA1_3_03,
                                             &tLA1_3_04,
                                             &tLA1_3_05,
                                             &tLA1_3_06,
                                             &tLA1_3_07,
                                             &tLA1_3_08,						// Riportare 9 strutture (da tLA1_3_00a a qui) in "tLA1_3_08"

											 &tLA1_4_0,
											 &tLA1_4_00,
											 &tLA1_4_00a,
                                             &tLA1_4_01,
                                             &tLA1_4_02,
                                             &tLA1_4_03,
                                             &tLA1_4_04,
                                             &tLA1_4_05,
                                             &tLA1_4_06,
                                             &tLA1_4_07,
                                             &tLA1_4_08,						// Riportare 9 strutture (da tLA1_4_00a a qui) in "tLA1_4_08"
*/
                                             &tDA2_00,
											 &tDA2_01,
											 &tDA2_02,
											 &tDA2_03,
											 &tDA2_04,
											 &tDA2_06,
											 &tDA3_00,
											 &tDA3_01,
											 &tDA3_02,
											 &tDA3_03,
											 &tDA3_06,
											 &tDA4_00,
											 &tDA4_01,
											 &tDA4_02,
											 &tDA4_03,
											 &tDA4_04,
											 &tDA5_00,
											 &tDA5_01,
											 &tDA5_02,
											 &tDA5_03,
											 &tDA5_04,
/*
											 &tDA6_00,					// Free Credit Accreditato
											 &tDA6_01,
											 &tDA6_02,
											 &tDA6_03,
*/
											 &tDA9,
											 &tDA901,
											 &tDA902,
											 &tDA9_End,
/*
											 &tDA99_00,					// Free Credit Addebitato
											 &tDA99_01,
											 &tDA99_02,
											 &tDA99_03,
*/											 
#if Debug_DA1000														// Per debug con Audit Control 
											 &tDA10_00,
											 &tDA10_01,
											 &tDA10_02,
											 &tDA10_03,
											 &tDA10_04,
											 &tDA10_05,
#endif

											 &tVA1_00,
											 &tVA1_01a,
											 &tVA1_01b,
											 &tVA1_01d,
											 &tVA1_01e,
											 &tVA1_01f,
											 &tVA1_02a,
											 &tVA1_02b,
											 &tVA1_02c,
											 &tVA1_03a,
											 &tVA1_03b,
											 &tVA1_03d,
											 &tVA1_03e,
											 &tVA1_03f,
											 &tVA1_04a,
											 &tVA1_04b,
											 &tVA1_04c,
											 &tVA1_end,

											 &tVA2_00,						// Test Vends
											 &tVA2_01,
											 &tVA2_02,
											 &tVA2_03,
											 &tVA2_04,
											 &tVA2_05,
											 &tVA3_00,
											 &tVA3_01,
											 &tVA3_02,
											 &tVA3_03,
											 &tVA3_04,
											 &tVA3_05,

											 nil
};

// =========================================================================================
// ========   Struttura ID Audit per CPU  con Mifare e Selettore       =====================
// =========================================================================================

const tAuditTable   tAuditTable_MIfare_Selettore[]  = {
											 &tDXS_00,		
											 &tDXS_01,

											 &tID1_00,
											 &tID1_01_a,
											 &tID1_01_b,
											 &tID1_01_c,
											 &tID1_02,
											 &tID1_03_a,
											 &tID1_03_b,
											 &tID1_04,
											 &tID1_05,
											 &tID1_06,
											 &tID1_07,
											 &tID1_08,

#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
											 &tEA1_00,
											 &tEA1_01,					// Il numero di strutture da qui alla fine del loop deve essere 
											 &tEA1_02,					// riportato nell'ultimo byte della struttura "tAEndLoop".
											 &tEA101,					// In questo caso, da tEA1_01 a tEA1_04 compresi, abbiamo 9 strutture:
											 &tEA102,					// riporteremo il numero 9 nella struttura indirizzata dall'ultimo elemento (tEA1_04)
											 &tEA103,
											 &tEA104_5,
											 &tEA106,
											 &tEA1_03,
											 &tEA1_04,
#endif

											 &tEA3_00,
											 &tEA3_01,
											 &tEA3_02,
											 &tEA3_03,
											 &tEA3_04,
											 &tEA3_05,
											 &tEA3_06,
											 &tEA3_07,
											 
											 &tEA4_00,
											 &tEA4_01,
											 &tEA4_02,
											 &tEA4_03,
											 &tEA5_00,
											 &tEA5_01,
											 &tEA5_02,
											 &tEA5_03,
											 &tEA7_00,
											 &tEA7_01,
											 &tEA7_02,
											 &tEA7_03,

											 &tID4_00,
											 &tID4_01,
											 //&tID4_02,
											 &tID4_03,
											 &tID4_04,

											 &tCA2_00,
											 &tCA2_01,
											 &tCA2_02,
											 &tCA2_03,
											 &tCA2_04,
											 &tCA2_05,

											 &tCA3_01,
											 &tCA3_02,
											 &tCA3_02b,
											 &tCA3_03,
											 &tCA3_03b,
											 &tCA3_04,
											 &tCA3_05,
#if !kAuditLA1OldModeSupport
											 &tCA3_06,
											 &tCA3_06b,
											 &tCA3_06c,
#else
											 &tCA3_06,
#endif
											 &tCA3_07,
											 &tCA3_08,
											 &tCA3_08b,
											 &tCA3_09,
											 &tCA3_09b,
											 &tCA3_10,
											 &tCA3_11,
#if !kAuditLA1OldModeSupport
											 &tCA3_12,
											 &tCA3_12b,
											 &tCA3_12c,
#else
											 &tCA3_12,
#endif
											 &tCA3_13,
											 &CA309,
											 &CA310,
											 &tCA3_14,	

											 &tCA8_00,
											 &tCA8_01,
											 &tCA8_02,
											 &tCA8_03,
											 
											 &tCA9_00,
											 &tCA9_01,
											 &tCA9_02,
											 &tCA9_03,

											 &tCA11_00,
											 &tCA11_01,
											 &tCA11_02,
											 &tCA11_03,
											 &tCA11_04,
											 &tCA11_05,
											 &tCA11_06,
											 &tCA11_07,

											 &tPA1_00,
											 &tPA1_00a,
											 &tPA1_01,
											 &tPA1_02,
											 &tPA1_03,
											 &tPA1_04,
#if kApplVendCountersSupport
											 &tPA2_01a,
#else
											 &tPA2_01,
#endif											 
											 &tPA2_02,
											 &tPA2_03,
											 &tPA2_03b,
											 &tPA2_03c,
											 &tPA2_03d,
											 &tPA2_04,
											 &tPA2_05,
											 &tPA2_06,
											 &tPA1_05,							// Riportare 15 strutture (da tPA1_00a a qui) in "tPA1_05"
											 
//											 &tLA1_0_0,
											 &tLA1_0_00,
                                             &tLA1_0_00a,
                                             &tLA1_0_01,
                                             &tLA1_0_02,
                                             &tLA1_0_03,
                                             &tLA1_0_04,
                                             &tLA1_0_05,
                                             &tLA1_0_06,						// Riportare 7 strutture (da tLA1_0_00a a qui) in "tLA1_0_06"

//                                             &tLA1_1_0,
                                             &tLA1_1_00,
                                             &tLA1_1_00a,
                                             &tLA1_1_01,
                                             &tLA1_1_02,
                                             &tLA1_1_03,
                                             &tLA1_1_04,
                                             &tLA1_1_05,
                                             &tLA1_1_06,
                                             &tLA1_1_07,
                                             &tLA1_1_08,						// Riportare 9 strutture (da tLA1_1_00a a qui) in "tLA1_1_08"
/*										 
											 &tLA1_2_0,
											 &tLA1_2_00,
											 &tLA1_2_00a,
                                             &tLA1_2_01,
                                             &tLA1_2_02,
                                             &tLA1_2_03,
                                             &tLA1_2_04,
                                             &tLA1_2_05,
                                             &tLA1_2_06,
                                             &tLA1_2_07,
                                             &tLA1_2_08,						// Riportare 9 strutture (da tLA1_2_00a a qui) in "tLA1_2_08"

											 &tLA1_3_0,
											 &tLA1_3_00,
											 &tLA1_3_00a,
                                             &tLA1_3_01,
                                             &tLA1_3_02,
                                             &tLA1_3_03,
                                             &tLA1_3_04,
                                             &tLA1_3_05,
                                             &tLA1_3_06,
                                             &tLA1_3_07,
                                             &tLA1_3_08,						// Riportare 9 strutture (da tLA1_3_00a a qui) in "tLA1_3_08"

											 &tLA1_4_0,
											 &tLA1_4_00,
											 &tLA1_4_00a,
                                             &tLA1_4_01,
                                             &tLA1_4_02,
                                             &tLA1_4_03,
                                             &tLA1_4_04,
                                             &tLA1_4_05,
                                             &tLA1_4_06,
                                             &tLA1_4_07,
                                             &tLA1_4_08,						// Riportare 9 strutture (da tLA1_4_00a a qui) in "tLA1_4_08"
*/
                                             &tDA2_00,
											 &tDA2_01,
											 &tDA2_02,
											 &tDA2_03,
											 &tDA2_04,
											 &tDA2_06,
											 &tDA3_00,
											 &tDA3_01,
											 &tDA3_02,
											 &tDA3_03,
											 &tDA3_06,
											 &tDA4_00,
											 &tDA4_01,
											 &tDA4_02,
											 &tDA4_03,
											 &tDA4_04,
											 &tDA5_00,
											 &tDA5_01,
											 &tDA5_02,
											 &tDA5_03,
											 &tDA5_04,
/*
											 &tDA6_00,					// Free Credit Accreditato
											 &tDA6_01,
											 &tDA6_02,
											 &tDA6_03,
*/
											 &tDA9,
											 &tDA901,
											 &tDA902,
											 &tDA9_End,
/*
											 &tDA99_00,
											 &tDA99_01,
											 &tDA99_02,
											 &tDA99_03,
*/											 
#if Debug_DA1000														// Per debug con Audit Control 
											 &tDA10_00,
											 &tDA10_01,
											 &tDA10_02,
											 &tDA10_03,
											 &tDA10_04,
											 &tDA10_05,
#endif

											 &tVA1_00,
											 &tVA1_01a,
											 &tVA1_01b,
											 &tVA1_01d,
											 &tVA1_01e,
											 &tVA1_01f,
											 &tVA1_02a,
											 &tVA1_02b,
											 &tVA1_02c,
											 &tVA1_03a,
											 &tVA1_03b,
											 &tVA1_03d,
											 &tVA1_03e,
											 &tVA1_03f,
											 &tVA1_04a,
											 &tVA1_04b,
											 &tVA1_04c,
											 &tVA1_end,

											 &tVA2_00,						// Test Vends
											 &tVA2_01,
											 &tVA2_02,
											 &tVA2_03,
											 &tVA2_04,
											 &tVA2_05,
											 &tVA3_00,
											 &tVA3_01,
											 &tVA3_02,
											 &tVA3_03,
											 &tVA3_04,
											 &tVA3_05,

											 nil
};


// =========================================================================================
// ========   Struttura ID Audit per CPU  come Slave Executive         =====================
// =========================================================================================

const tAuditTable   tAuditTable_SlaveExec[]  = {
											 &tDXS_00,		
											 &tDXS_01,

											 &tID1_00,
											 &tID1_01_a,
											 &tID1_01_b,
											 &tID1_01_c,
											 &tID1_02,
											 &tID1_03_a,
											 &tID1_03_b,
											 &tID1_04,
											 &tID1_05,
											 &tID1_06,
											 &tID1_07,
											 &tID1_08,

#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
											 &tEA1_00,
											 &tEA1_01,					// Il numero di strutture da qui alla fine del loop deve essere 
											 &tEA1_02,					// riportato nell'ultimo byte della struttura "tAEndLoop".
											 &tEA101,					// In questo caso, da tEA1_01 a tEA1_04 compresi, abbiamo 9 strutture:
											 &tEA102,					// riporteremo il numero 9 nella struttura indirizzata dall'ultimo elemento (tEA1_04)
											 &tEA103,
											 &tEA104_5,
											 &tEA106,
											 &tEA1_03,
											 &tEA1_04,
#endif

											 &tEA3_00,
											 &tEA3_01,
											 &tEA3_02,
											 &tEA3_03,
											 &tEA3_04,
											 &tEA3_05,
											 &tEA3_06,
											 &tEA3_07,
											 
											 &tEA4_00,
											 &tEA4_01,
											 &tEA4_02,
											 &tEA4_03,
											 &tEA5_00,
											 &tEA5_01,
											 &tEA5_02,
											 &tEA5_03,
											 &tEA7_00,
											 &tEA7_01,
											 &tEA7_02,
											 &tEA7_03,

											 &tID4_00,
											 &tID4_01,
											 //&tID4_02,
											 &tID4_03,
											 &tID4_04,

											 &tPA1_00,
											 &tPA1_00a,
											 &tPA1_01,
											 &tPA1_02,
											 &tPA1_03,
											 &tPA1_04,
#if kApplVendCountersSupport
											 &tPA2_01a,
#else
											 &tPA2_01,
#endif											 
											 &tPA2_02,
											 &tPA2_03,
											 &tPA2_03b,
											 &tPA2_03c,
											 &tPA2_03d,
											 &tPA2_04,
											 &tPA2_05,
											 &tPA2_06,
											 &tPA1_05,							// Riportare 15 strutture (da tPA1_00a a qui) in "tPA1_05"
											 
											 &tVA1_00,
											 &tVA1_01a,
											 &tVA1_01b,
											 &tVA1_01d,
											 &tVA1_01e,
											 &tVA1_01f,
											 &tVA1_02a,
											 &tVA1_02b,
											 &tVA1_02c,
											 &tVA1_03a,
											 &tVA1_03b,
											 &tVA1_03d,
											 &tVA1_03e,
											 &tVA1_03f,
											 &tVA1_04a,
											 &tVA1_04b,
											 &tVA1_04c,
											 &tVA1_end,

											 &tVA2_00,
											 &tVA2_01,
											 &tVA2_02,
											 &tVA2_03,
											 &tVA2_04,
											 &tVA2_05,
											 &tVA3_00,
											 &tVA3_01,
											 &tVA3_02,
											 &tVA3_03,
											 &tVA3_04,
											 &tVA3_05,
/*
											 &tEK_08_Event_00,
											 &tEK_08_Event_01,
											 &tEK_08_Event_02,
											 &tEK_08_Event_03,
*/
											 
											 nil
};


// =========================================================================================
// ========   Struttura ID Audit per CPU  in modalita'  Gratuito       =====================
// =========================================================================================

const tAuditTable   tAuditTable_Gratuito[]  = {
											 &tDXS_00,		
											 &tDXS_01,

											 &tID1_00,
											 &tID1_01_a,
											 &tID1_01_b,
											 &tID1_01_c,
											 &tID1_02,
											 &tID1_03_a,
											 &tID1_03_b,
											 &tID1_04,
											 &tID1_05,
											 &tID1_06,
											 &tID1_07,
											 &tID1_08,

#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
											 &tEA1_00,
											 &tEA1_01,					// Il numero di strutture da qui alla fine del loop deve essere 
											 &tEA1_02,					// riportato nell'ultimo byte della struttura "tAEndLoop".
											 &tEA101,					// In questo caso, da tEA1_01 a tEA1_04 compresi, abbiamo 9 strutture:
											 &tEA102,					// riporteremo il numero 9 nella struttura indirizzata dall'ultimo elemento (tEA1_04)
											 &tEA103,
											 &tEA104_5,
											 &tEA106,
											 &tEA1_03,
											 &tEA1_04,
#endif

											 &tEA3_00,
											 &tEA3_01,
											 &tEA3_02,
											 &tEA3_03,
											 &tEA3_04,
											 &tEA3_05,
											 &tEA3_06,
											 &tEA3_07,
											 
											 &tEA4_00,
											 &tEA4_01,
											 &tEA4_02,
											 &tEA4_03,
											 &tEA5_00,
											 &tEA5_01,
											 &tEA5_02,
											 &tEA5_03,
											 &tEA7_00,
											 &tEA7_01,
											 &tEA7_02,
											 &tEA7_03,

											 &tID4_00,
											 &tID4_01,
											 //&tID4_02,
											 &tID4_03,
											 &tID4_04,

											 
											 &tVA1_00,
											 &tVA1_01a,
											 &tVA1_01b,
											 &tVA1_01d,
											 &tVA1_01e,
											 &tVA1_01f,
											 &tVA1_02a,
											 &tVA1_02b,
											 &tVA1_02c,
											 &tVA1_03a,
											 &tVA1_03b,
											 &tVA1_03d,
											 &tVA1_03e,
											 &tVA1_03f,
											 &tVA1_04a,
											 &tVA1_04b,
											 &tVA1_04c,
											 &tVA1_end,

											 &tVA2_00,
											 &tVA2_01,
											 &tVA2_02,
											 &tVA2_03,
											 &tVA2_04,
											 &tVA2_05,
											 &tVA3_00,
											 &tVA3_01,
											 &tVA3_02,
											 &tVA3_03,
											 &tVA3_04,
											 &tVA3_05,
/*
											 &tEK_08_Event_00,
											 &tEK_08_Event_01,
											 &tEK_08_Event_02,
											 &tEK_08_Event_03,
*/
											 nil
};


//********************************************************************************************************

/*--------------------------------------------------------------------------------------*\
Method: AuditInitProfiler

	Initialize programming module

Parameters:
\*--------------------------------------------------------------------------------------*/
void AuditInitProfiler(AuditDataHandlerFn auditFunction, AuditEndTestFn auditTest, char* workArea)
{

#if (_IcpMaster_ == true)	
	Local_CPCInfo_SwVersion	  = ICPCPCInfo.CPCInfo.nSwVersion;
	Local_BillInfo_SwVersion  = ICPBILLInfo.BillInfo.nSwVersion;
	Local_CHGInfo_SwVersion	  = ICPCHGInfo.ChgInfo.nSwVersion;
#endif


	if (gVMC_ConfVars.ExecutiveSLV == true) {
		// --- Slave Executive ----
		AuditInit((tAuditTable*)&tAuditTable_SlaveExec[0], auditFunction, auditTest, workArea, kNoTestEndString);
	} else if (gVMC_ConfVars.ProtMDB) {
		// --- MDB  Master    ----
		AuditInit((tAuditTable*)&tAuditTable_MDB_Master[0], auditFunction, auditTest, workArea, kNoTestEndString);
	} else if (gVMC_ConfVars.PresMifare) {
		// --- Mifare + Selettore ----
		AuditInit((tAuditTable*)&tAuditTable_MIfare_Selettore[0], auditFunction, auditTest, workArea, kNoTestEndString);
	} else if (gVMC_ConfVars.Gratuito) {
		// --- Gratuito ----
		AuditInit((tAuditTable*)&tAuditTable_Gratuito[0], auditFunction, auditTest, workArea, kNoTestEndString);
	}
}

/*--------------------------------------------------------------------------------------*\
Method: TestAudit

	Controllo strutture da elaborare

Parameters:
\*--------------------------------------------------------------------------------------*/
bool  TestAudit(const void** pnt) {

	const void* vpnt = *pnt;
	uint8_t a;
  
  	//MR19 if (f99Selections && vpnt == &tLA1_2_00) *pnt = &tCA2_00;
  	if (vpnt == &tLA1_2_00) *pnt = &tCA2_00;
  	vpnt = *pnt;
  
  	// Controllo se ci sono altre strutture da elaborare
  	a = (*((uint8_t*)vpnt + 4));
  	if (a == NULL) {
  		return false;       
  	} else {
  		return true;
  	}
}


/*---------------------------------------------------------------------------------------------*\
Method: IsStruct_ID106
	
	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
bool IsStruct_ID106(const void* pnt) {

	return (pnt == &tID1_06 ? true : false);
}




#if  TestAudStruct
/*---------------------------------------------------------------------------------------------*\
Method: StructToBreak
	Routine di Debug per fermare l'esecuzione prima di elaborare la struttura passata come
	parametro in "sAuditCommVars.pntAudit".
	L'indirizzo della struttura da eseguire e' comparato con l'indirizzo inserito manualmente
	nella variabile "StructAddress".
	
	IN:	  - address struttura da eseguire e indirizzo tabella di confronto in "StructAddress"
	OUT:  - true or false
\*----------------------------------------------------------------------------------------------*/

bool StructToBreak(const void* pnt) {

	if( pnt == StructAddress) {
		return true;
	} else {
		return false;
	}
}
#endif


#if kApplConfigTxSupport

extern void ConfTxDataHandle(char *pnt, uint size);

/*--------------------------------------------------------------------------------------*\
Method: ConfTxStringRG01
\*--------------------------------------------------------------------------------------*/
void ConfTxStringRG01(void) {
//	static const char sRG01[]= "RG01*" kProductAuditIdString "*" kProductSWVersionStr "\r\n";
	static char sRG01[]= "RG01*" kProductAuditIdString "*" kProductSWVersionStr "\r\n";
	
	ConfTxDataHandle(sRG01, sizeof(sRG01)-1);
}

/*--------------------------------------------------------------------------------------*\
Method: ConfTxStringID1
\*--------------------------------------------------------------------------------------*/
void ConfTxStringID1(void) {
	char buffer[20];
}

/*--------------------------------------------------------------------------------------*\
Method: ConfEmptyFileSet
\*--------------------------------------------------------------------------------------*/
uint16 ConfEmptyFileSet(char *dest) {
	char buffer[20];
	return 0x0;					// TODO Per togliere errori di compilazione
}
#endif  //kApplConfigTxSupport
