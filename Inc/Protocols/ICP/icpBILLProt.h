#ifndef __icpBILLProt__
#define __icpBILLProt__

#include "icpBILL.h"
#include "ICPGlobal.h"
#include "ICPProt.h"


#pragma pack(1)



#define ICP_BILL_CMD_RESET 0
#define ICP_BILL_CMD_STATUS 1
#define ICP_BILL_CMD_SECURITY 2
#define ICP_BILL_CMD_POLL 3
#define ICP_BILL_CMD_BILLTYPE 4
#define ICP_BILL_CMD_ESCROW 5
#define ICP_BILL_CMD_STACKER 6
#define ICP_BILL_CMD_EXP 7

#define ICP_BILL_CMD_EXPREQUESTID1 0
#define ICP_BILL_CMD_EXPENABLEOPT 1
#define ICP_BILL_CMD_EXPREQUESTID2 2
#define ICP_BILL_CMD_EXPFTLREQTORCV 0xFA
#define ICP_BILL_CMD_EXPFTLRETRYDENY 0xFB
#define ICP_BILL_CMD_EXPFTLSENDBLOCK 0xFC
#define ICP_BILL_CMD_EXPFTLOKTOSEND 0xFD
#define ICP_BILL_CMD_EXPFTLREQTOSEND 0xFE
#define ICP_BILL_CMD_EXPDIAG 0xFF



#define ICP_BILL_INACTIVE 0
#define ICP_BILL_DISABLED 1
#define ICP_BILL_ENABLED 2

#define ICP_BILL_INACTIVE_TXRESET 0
#define ICP_BILL_INACTIVE_WAITINIT 1
#define ICP_BILL_INACTIVE_TXPOLL_1 2
#define ICP_BILL_INACTIVE_TXPOLL_2 3

#define ICP_BILL_DISABLED_TXSTATUS 0
#define ICP_BILL_DISABLED_TXEXP00 1
#define ICP_BILL_DISABLED_TXEXP01 2
#define ICP_BILL_DISABLED_TXEXP02 3
#define ICP_BILL_DISABLED_TXSTACKER 4
#define ICP_BILL_DISABLED_TXBILLTYPE 5
#define ICP_BILL_DISABLED_TXSECURITY 6
#define ICP_BILL_DISABLED_TXPOLL 7
#define ICP_BILL_DISABLED_TXESCROW 8
#define ICP_BILL_DISABLED_TXBILLTYPE1 9
#define ICP_BILL_DISABLED_TXEXPDIAG 10

#define ICP_BILL_ENABLED_TXPOLL 0
#define ICP_BILL_ENABLED_TXSTACKER 1
#define ICP_BILL_ENABLED_TXBILLTYPE 2
#define ICP_BILL_ENABLED_TXSECURITY 3
#define ICP_BILL_ENABLED_TXESCROW 4
#define ICP_BILL_ENABLED_WAITENDESCROW 5
#define ICP_BILL_ENABLED_TXBILLTYPE1 6
#define ICP_BILL_ENABLED_TXEXPDIAG 7

typedef enum  {
	ICP_BILLINFO_NONE = 0,
	ICP_BILLINFO_STACKERFULL = 0x01,
	ICP_BILLINFO_STACKERFULLINH = 0x02
} ET_BILLINFO;

/*#if (_IcpMaster_ == true)
#else
	#define ICP_BILL_ENABLED 0
#endif*/



#define ICP_BILL_RESPTYPE_STATUS (ICP_BILL_RESPTYPE + 1)
#define ICP_BILL_RESPTYPE_STACKER (ICP_BILL_RESPTYPE + 2)
#define ICP_BILL_RESPTYPE_DISABLED (ICP_BILL_RESPTYPE + 3)
#define ICP_BILL_RESPTYPE_INVALIDESCROW (ICP_BILL_RESPTYPE + 4)
#define ICP_BILL_RESPTYPE_BUSY (ICP_BILL_RESPTYPE + 5)
#define ICP_BILL_RESPTYPE_INHIBIT (ICP_BILL_RESPTYPE + 6)
#define ICP_BILL_RESPTYPE_EXP00 (ICP_BILL_RESPTYPE + 7)
#define ICP_BILL_RESPTYPE_EXP02 (ICP_BILL_RESPTYPE + 8)
#define ICP_BILL_RESPTYPE_EXPFA (ICP_BILL_RESPTYPE + 9)
#define ICP_BILL_RESPTYPE_EXPFD (ICP_BILL_RESPTYPE + 10)
#define ICP_BILL_RESPTYPE_EXPFE (ICP_BILL_RESPTYPE + 11)
#define ICP_BILL_RESPTYPE_EXPFF (ICP_BILL_RESPTYPE + 12)
#define ICP_BILL_RESPTYPE_BILLSTACKED (ICP_BILL_RESPTYPE + 13)
#define ICP_BILL_RESPTYPE_BILLRETURNED (ICP_BILL_RESPTYPE + 14)



//-------------------------------------------------
// B I L L  Info
//-------------------------------------------------

typedef struct {
	uint16_t nSecurityLevel;
} T_BILL_SECURITY;

typedef struct {
	uint16_t nBillsInStacker;
} T_BILL_STACKER;

typedef struct {
	uint8_t nFLevel;
	uint16_t nCCode;
	uint16_t nUSF;
	uint8_t nDPP;
	T_BILL_STACKER StackerState;
	T_BILL_SECURITY SecurityLevel;
	uint8_t fEscrow;
	uint8_t aBillTypeVal[ICP_MAX_BILLTYPES];
} T_BILL_STATUS;

typedef struct {
	uint16_t nBillEnable;
	uint16_t nEscrowEnable;
} T_BILL_BILLTYPE;

typedef struct {
	uint8_t aManufactCode[3];
	uint8_t aSerialNumber[12];
	uint8_t aModelNumber[12];
	uint16_t nSwVersion;
} T_BILL_PERIPHID1;

typedef struct {
	T_BILL_PERIPHID1 PeriphID1;
	uint32_t nFOptions;
} T_BILL_PERIPHID2;

typedef struct {
	uint32_t nFOptions;
	T_BILL_STATUS Status;
	T_BILL_STACKER StackerState;
	T_BILL_BILLTYPE BillTypeState;
	T_BILL_PERIPHID1 BillInfo;
#if (_IcpMaster_ == true)
	uint8_t nWaitInitCnt;
	uint8_t nCmdState;
	uint8_t nCmdType;
	uint8_t aCmdParameters[6];
	uint8_t nInfoMask;
	uint8_t bill_state;
#endif
} T_ICP_BILLINFO;



//-------------------------------------------------
// B I L L  Command/Response Structures
//-------------------------------------------------

typedef struct {
	uint8_t nCmd;
} T_BILLCMD_RESET;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_RESET;

typedef struct {
	uint8_t nCmd;
} T_BILLCMD_STATUS;

typedef union {
	uint8_t nodata;
	T_BILL_STATUS Status;
} T_BILLRESP_STATUS;

typedef struct {
	uint8_t nCmd;
	T_BILL_SECURITY SecurityLevel;
} T_BILLCMD_SECURITY;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_SECURITY;

typedef struct {
	uint8_t nCmd;
} T_BILLCMD_POLL;

typedef union {
	uint8_t nodata;
	uint8_t aPollData[ICP_MAX_FRAMELEN];
} T_BILLRESP_POLL;

typedef struct {
	uint8_t nCmd;
	T_BILL_BILLTYPE BillTypeState;
} T_BILLCMD_BILLTYPE;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_BILLTYPE;

typedef struct {
	uint8_t nCmd;
	uint8_t fStackBill;
} T_BILLCMD_ESCROW;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_ESCROW;

typedef struct {
	uint8_t nCmd;
} T_BILLCMD_STACKER;

typedef union {
	uint8_t nodata;
	T_BILL_STACKER StackerState;
} T_BILLRESP_STACKER;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_BILLCMD_EXP00;

typedef union {
	uint8_t nodata;
	T_BILL_PERIPHID1 PeriphID1;
} T_BILLRESP_EXP00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint32_t nFOptions;
} T_BILLCMD_EXP01;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_EXP01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_BILLCMD_EXP02;

typedef union {
	uint8_t nodata;
	T_BILL_PERIPHID2 PeriphID2;
} T_BILLRESP_EXP02;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t aDiagData[ICP_MAX_FRAMELEN - 2];
} T_BILLCMD_EXPFF;

typedef union {
	uint8_t nodata;
	uint8_t aDiagData[ICP_MAX_FRAMELEN];
} T_BILLRESP_EXPFF;


/*da implementare*/
typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_BILLCMD_EXPFA;
typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_EXPFA;
typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_BILLCMD_EXPFB;
typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_EXPFB;
typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_BILLCMD_EXPFC;
typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_EXPFC;
typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_BILLCMD_EXPFD;
typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_EXPFD;
typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_BILLCMD_EXPFE;
typedef union {
	uint8_t nodata;
	uint8_t data;
} T_BILLRESP_EXPFE;
/*da implementare*/


void ICPProt_BILLReset(void);
void ICPProt_BILLStatus(void);
void ICPProt_BILLSecurity(void);
void ICPProt_BILLPoll(void);
void ICPProt_BILLBillType(void);
void ICPProt_BILLEscrow(void);
void ICPProt_BILLStacker(void);
void ICPProt_BILLExp00(void);
void ICPProt_BILLExp01(void);
void ICPProt_BILLExp02(void);
void ICPProt_BILLExpFF(void);
#if _IcpBillFTL_ > 0
void ICPProt_BILLExpFA(void);
void ICPProt_BILLExpFB(void);
void ICPProt_BILLExpFC(void);
void ICPProt_BILLExpFD(void);
void ICPProt_BILLExpFE(void);
#endif

uint8_t ICPProt_BILLResetResp(void);
uint8_t ICPProt_BILLStatusResp(void);
uint8_t ICPProt_BILLSecurityResp(void);
uint8_t ICPProt_BILLPollResp(void);
uint8_t ICPProt_BILLBillTypeResp(void);
uint8_t ICPProt_BILLEscrowResp(void);
uint8_t ICPProt_BILLStackerResp(void);
uint8_t ICPProt_BILLExp00Resp(void);
uint8_t ICPProt_BILLExp01Resp(void);
uint8_t ICPProt_BILLExp02Resp(void);
uint8_t ICPProt_BILLExpFFResp(void);
#if _IcpBillFTL_ > 0
uint8_t ICPProt_BILLExpFAResp(void);
uint8_t ICPProt_BILLExpFBResp(void);
uint8_t ICPProt_BILLExpFCResp(void);
uint8_t ICPProt_BILLExpFDResp(void);
uint8_t ICPProt_BILLExpFEResp(void);
#endif

void BILLInit(uint8_t nSlaveType);
void BILLInactive(void);
void BILLDisabled(void);
void BILLEnabled(void);
//void BILLFtl(void);


#if (_IcpMaster_ == true)
bool ICPProt_BILLGetFTL(void);
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
typedef struct {
	uint8_t aPollData[16];
	uint8_t nRdData;
	uint8_t nWrData;
	uint8_t nData;
} T_ICP_BILLPOLLDATA;

void ICPProt_BILLClearPollData(void);
void ICPProt_BILLResetPollData(void);
uint8_t ICPProt_BILLGetPollData(uint8_t *ptData, bool fAllData);
void ICPProt_BILLSetBillTypeState(void);

bool ICPProt_BILLIsFTLEnable(void);
#endif




extern T_ICP_BILLINFO ICPBILLInfo;

#endif
