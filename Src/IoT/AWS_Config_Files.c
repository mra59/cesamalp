/****************************************************************************************
File:
    AWS_Config_Files.c

Description:
    File con funzioni per la lettura dei files di configurazione account AWS da
	USB Memory Stick.

History:
    Date       Aut  Note
    Ott 2021	MR   

*****************************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include <string.h>
#include "main.h"
#include "ff.h"
#include "ORION.H"
#include "AWS_Config_Files.h"
#include "IICEEP.h"
#include "Monitor.h"

#if AWS_CONFIG


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	bool  WriteEEPI2C(uint16_t AddrEEP, uint8_t *AddrSource, uint16_t size);
extern	bool  ReadEEPI2C(uint16_t AddrEEP, uint8_t *AddrDest, uint16_t size);
extern	void  RedLedCPU(uint8_t On_Off);
extern	void  GreenLedCPU(uint8_t On_Off);


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static uint16_t rx_file_baseadd;
static uint32_t add_eeprom;

/* Private constant ---------------------------------------------------------*/

const char AWS_Device_FileName[] 	= {"AWS_arn_DeviceName"};

const char AWS_Endpoint_FileName[] 	= {"AWS_AccountServerAddress"};
//const char AWS_Endpoint_Content[] 	= {"iot.eu-west-1.amazonaws.com"};
const char AmazonSite[] 			= {"amazonaws.com"};

const char CertificateStartString[] = {"-----BEGIN CERTIFICATE-----"};
const char CertificateEndString[] 	= {"-----END CERTIFICATE-----"};

const char PrivateKeyStartString[] 	= {"-----BEGIN RSA PRIVATE KEY-----"};
const char PrivateKeyEndString[] 	= {"-----END RSA PRIVATE KEY-----"};

//const char PartialAWSServerNameString[] = {".iot.eu-west-1.amazonaws.com"};


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/*---------------------------------------------------------------------------------------------*\
Method: Check_New_AWS_Config
	Controlla se ci sono nuovi parametri di configurazione AWS da inviare alla scheda 
	gateway.

	IN:	  - 
	OUT:  - un bit set per ogni configurazione da inviare
\*----------------------------------------------------------------------------------------------*/
void  Check_New_AWS_Config(uint8_t* resp, uint8_t* Quantity)
{
	uint16_t	EEAddr;
	char*		bufPnt;

	*resp = 0;
	*Quantity = 0;
	
	//-- Check presenza new AWS Certificate.pem --	
	Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_SECURE_CERTIF);									// In EEAddr start address RxAWSSecureCertifBuff e banco
	EEAddr = 0;				
	EEAddr += MemoryBytes[1];
	EEAddr += (MemoryBytes[0]<<8);
	memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));									// Azzero buffer ddcmp
	ReadEEPI2C(EEAddr, &gVars.ddcmpbuff[0], sizeof(CertificateStartString));
	if (gVars.ddcmpbuff[0] != NULL)
	{
		bufPnt = strstr((char const*)&gVars.ddcmpbuff[0], CertificateStartString);
		if (bufPnt != NULL)
		{
			*resp |= NEW_AWS_SECURE_CERTIF_BIT;
			(*Quantity)++;
		}
	}

	//-- Check presenza new AWS Certificate private.pem  --	
	Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_PRIVATE_KEY);										// In EEAddr start address RxAWSPrivateKeyfBuff e banco
	EEAddr = 0;				
	EEAddr += MemoryBytes[1];
	EEAddr += (MemoryBytes[0]<<8);
	memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));									// Azzero buffer ddcmp
	ReadEEPI2C(EEAddr, &gVars.ddcmpbuff[0], sizeof(PrivateKeyStartString));
	if (gVars.ddcmpbuff[0] != NULL)
	{
		bufPnt = strstr((char const*)&gVars.ddcmpbuff[0], PrivateKeyStartString);
		if (bufPnt != NULL)
		{
			*resp |= NEW_AWS_PRIVATE_KEY_BIT;
			(*Quantity)++;
		}
	}

	//-- Check presenza new AWS Endpoint --	
	Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_ACCOUNT_SERVER_ADDRESS);							// In EEAddr start address RxAWS_AccountServerAddr e banco (es "agg23zwk76whx-ats.iot.eu-west-1.amazonaws.com" per account MHD)
	EEAddr = 0;				
	EEAddr += MemoryBytes[1];
	EEAddr += (MemoryBytes[0]<<8);
	memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));									// Azzero buffer ddcmp
	ReadEEPI2C(EEAddr, &gVars.ddcmpbuff[0], RxAWS_AccountServerAddr_Len);
	if (gVars.ddcmpbuff[0] != NULL)
	{
		/*
		Dalla Rev. 1.40 non testo piu' l'Endpoint perche' e' capitato che un cliente abbia creato l'account in una regione
		diversa da Irlanda (eu-west1) e questo file non fosse trasferito alla Gateway
		
		bufPnt = strstr((char const*)&gVars.ddcmpbuff[0], PartialAWSServerNameString);
		if (bufPnt != NULL)
		{
			*resp |= NEW_AWS_ACCOUNT_SERVER_ADDR_BIT;
			(*Quantity)++;
		}
		*/

		*resp |= NEW_AWS_ACCOUNT_SERVER_ADDR_BIT;
		(*Quantity)++;
	}

	//-- Check presenza new AWS Device name --	
	Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_DEVICE);											// In EEAddr start address RxAWS_arnDeviceObject e banco
	EEAddr = 0;				
	EEAddr += MemoryBytes[1];
	EEAddr += (MemoryBytes[0]<<8);
	memset(&gVars.ddcmpbuff[0], 0, sizeof(gVars.ddcmpbuff));									// Azzero buffer ddcmp
	ReadEEPI2C(EEAddr, &gVars.ddcmpbuff[0], RxAWS_arnDeviceName_Len);
	if (gVars.ddcmpbuff[0] != NULL)
	{
		*resp |= NEW_AWS_DEVICE_BIT;
		(*Quantity)++;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Import_AWS_Conf
	Legge i files di configurazione oggetto AWS dalla chiave USB e la memorizza nella EEProm
	per essere inviati alla scheda IoT

	IN:	 - 
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
USB_Key_Err Import_AWS_Conf(void)
{
    USB_Key_Err		resp;
  	DIR 			dir;
    FILINFO 		fno;
    char 			conf_path[] = "";
	char*			bufPnt;

	uint8_t 		MAXCP = 255;				//default security --> max files checked into root, to find default "OrionPK3_FW"
	static uint8_t 	i, FilesCnt;
	bool 			result;
	FRESULT 		returnCode;                 // Result code
	FIL     		fp;                         // File pointer
	uint32_t  		size, len;
	uint8_t  		buffer[BUFFER_LENGTH];      // Buffer for parse 2K
	TipoFile		FileType;
	
	FilesCnt = 0;																				// Numero di files rilevati sulla chiave
	//-- Fino a 3 tentativi di verificare se la chiave USB ha il file system --
	for (i=0; i < 3; i++)
	{
    	returnCode = f_opendir(&dir, conf_path);
		if (returnCode != FR_NO_FILESYSTEM) break;
    }
	
	/*
	//-- Predispongo nomefile .cfw nella stringa cfrx_namefile --
	memset(&cfrx_namefile, 0, sizeof(cfrx_namefile));
	len_cfrx_namefile = strlen(FileNameTxConf_String);
	memcpy(&cfrx_namefile[0], FileNameTxConf_String, len_cfrx_namefile);						// Risultato: "nomecliente_"
	
	// -- Merge della password di sistema letta dalla EE nel nomefile dopo il chr "_" --
	// -- Se e' diversa da zero accetto solo filenames con password identica, alrimenti--
	// -- accetto qualsiasi file di configurazione: serve ad inizializzare --
	// -- automaticamente la password al primo uso del prodotto --
	valid_pw = false;																			//  Accetto file config con qualsiasi password
	if (AccPsw)
	{
		valid_pw = true;																		// Accetto solo file config con password esatta
	}
	sprintf(&cfrx_namefile[len_cfrx_namefile], "%05d.cfw", AccPsw);								// Aggiungo psw e creo la stringa "nomecliente_xxxxx.cfw\0"
	len_cfrx_namefile = strlen(cfrx_namefile);
	*/
	

	//-- Lettura nomefiles presenti su chiave USB e ricerca estensioni ".key" e ".crt" --
	//-- La comparazione e' case-sensitive --
	for (i=0; i<MAXCP; i++)
	{
		FileType = NO_FILE_TYPE;
		returnCode = f_readdir(&dir, &fno);
        if (returnCode != FR_OK || fno.fname[0] == 0)
		{
			//f_close(&fplog);
			if (FilesCnt == 0)
			{
				resp = ERR_NO_CONFIG_FILE;														// Non ci sono files di configurazione sulla chiave
			}
        	goto EndImportConf;
		}
		{
			if (strstr(fno.fname, ".crt"))														// check if filename terminates with ".crt"
			{
				Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_SECURE_CERTIF);						// In MemoryBytes[] indirizzo di inizio del Buffer RxAWSSecureCertifBuff
				rx_file_baseadd = 0;				
				rx_file_baseadd += MemoryBytes[1];												// LSB
				rx_file_baseadd += (MemoryBytes[0]<<8);											// MSB
				add_eeprom = rx_file_baseadd;
				FileType = FILE_TYPE_CERTIFICATE;
				FilesCnt++;
			}
        	if (strstr(fno.fname, ".key"))														// check if filename terminates with ".key"
			{
				Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_PRIVATE_KEY);							// In MemoryBytes[] indirizzo di inizio del Buffer RxAWSPrivateKeyfBuff
				rx_file_baseadd = 0;				
				rx_file_baseadd += MemoryBytes[1];												// LSB
				rx_file_baseadd += (MemoryBytes[0]<<8);											// MSB
				add_eeprom = rx_file_baseadd;
				FileType = FILE_TYPE_PRIVATE_KEY;
				FilesCnt++;
			}
        	if (strstr(fno.fname, ".txt"))														// check if filename terminates with ".txt"
			{
				if (strstr(fno.fname, AWS_Device_FileName))
				{
					Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_DEVICE);							// In MemoryBytes[] indirizzo di inizio del Buffer RxAWS_arnDeviceObject
					rx_file_baseadd = 0;				
					rx_file_baseadd += MemoryBytes[1];											// LSB
					rx_file_baseadd += (MemoryBytes[0]<<8);										// MSB
					add_eeprom = rx_file_baseadd;
					FileType = FILE_TYPE_DEVICE;
					FilesCnt++;
				}
				else if (strstr(fno.fname, AWS_Endpoint_FileName))
				{
					Rd_EEBuffStartAddr_And_BankSelect(BUF_AWS_ACCOUNT_SERVER_ADDRESS);			// In MemoryBytes[] indirizzo di inizio del Buffer RxAWS_AccountServerAddr
					rx_file_baseadd = 0;				
					rx_file_baseadd += MemoryBytes[1];											// LSB
					rx_file_baseadd += (MemoryBytes[0]<<8);										// MSB
					add_eeprom = rx_file_baseadd;
					FileType = FILE_TYPE_ACCOUNTSERVADDR;
					FilesCnt++;
				}
			}
			if (FileType != NO_FILE_TYPE)
			{
				//-- Read file content into EEprom buffer --
				resp = USB_AWS_ERR_OK;
				returnCode = f_open(&fp, fno.fname, FA_READ);
				if(returnCode == FR_NO_FILESYSTEM)
				{
					resp = ERR_RW_FILE;
					goto EndImportConf;
				}
				if (returnCode != FR_OK)
				{
					resp = ERR_RW_FILE;
					goto EndImportConf;
				}
				
				// --  Loop trasferimento files di configurazione in EEprom ---
				do
				{
					//-- Get BUFFER_LENGTH bytes and parse for programming --
					memset(&buffer, 0, sizeof(buffer));
				  	returnCode = f_read(&fp, buffer, BUFFER_LENGTH, &size);								// La f_read rende il buffer e il suo size
					if (returnCode)
					{
						buffer[0]= NULL;
						WriteEEPI2C(rx_file_baseadd, &buffer[0], 1U);									// Azzero il primo byte del buffer EE per disattivarne l'uso
						resp = ERR_RW_FILE;
						break;
					}
					if(size == 0)
					{
						//-- Trasferimento completato, aggiungo chr di chiusura buffer EE --
						buffer[0]= NULL;
						WriteEEPI2C(add_eeprom, &buffer[0], 1U);
						break;
					}
					else
					{	
						//-- Elimino eventuali chr LF o CR alla fine del buffer letto da file --
						switch (FileType)
						{
							case FILE_TYPE_DEVICE:
								len = strlen((char*)&buffer);											// Elimino eventuali CR o LF sostituendoli con fine buffer
								for (i=0; i < len; i++)
								{
									if ((buffer[i] == ASCII_CR) || (buffer[i] == ASCII_LF))
									{
										buffer[i] = 0;
										size = ++i;
										break;
									}
								}
								break;
							
							case FILE_TYPE_ACCOUNTSERVADDR:
								/*
								Dalla Rev. 1.40 non testo piu' l'Endpoint perche' e' capitato che un cliente abbia creato l'account in una regione
								diversa da Irlanda (eu-west1) e questo file non fosse letto e trasferito alla Gateway

								if(strstr((char const*)&buffer, AWS_Endpoint_Content) == false)			// Contenuto del file "AWS_AccountServerAddress_xxxx.txt" non corretto
								{
									FileType = NO_FILE_TYPE;
								}
								else
								{
									bufPnt = strstr((char const*)&buffer, AmazonSite);
									bufPnt += (sizeof(AmazonSite) - 1);									// Pointer al primo byte dopo "amazonaws.com" per inserire fine buffer ed eliminare eventuali LF
									*bufPnt = 0;
								}
								*/
							
								bufPnt = strstr((char const*)&buffer, AmazonSite);
								bufPnt += (sizeof(AmazonSite) - 1);									// Pointer al primo byte dopo "amazonaws.com" per inserire fine buffer ed eliminare eventuali LF
								*bufPnt = 0;
								break;

							case FILE_TYPE_CERTIFICATE:
								bufPnt = strstr((char const*)&buffer, CertificateEndString);
								if (bufPnt == NULL)
								{
									FileType = NO_FILE_TYPE;
								}
								else
								{
									bufPnt = strstr((char const*)&buffer, CertificateEndString);
									bufPnt += sizeof(CertificateEndString);								// Pointer al primo byte dopo "-----END CERTIFICATE-----" per inserire fine buffer ed eliminare eventuali LF o CR
									*bufPnt = 0;
								}
								break;
						
							case FILE_TYPE_PRIVATE_KEY:
								bufPnt = strstr((char const*)&buffer, PrivateKeyEndString);
								if (bufPnt == NULL)
								{
									FileType = NO_FILE_TYPE;
								}
								else
								{
									bufPnt = strstr((char const*)&buffer, PrivateKeyEndString);
									bufPnt += sizeof(PrivateKeyEndString);								// Pointer al primo byte dopo "-----END RSA PRIVATE KEY-----" per inserire fine buffer ed eliminare eventuali LF o CR
									*bufPnt = 0;
								}
								break;
						}

						if (FileType != NO_FILE_TYPE)
						{
							//-- Store buffer from file to EE --
							SetLed(LED_VERDE, false);
							SetLed(LED_ROSSO, true);
							result = EE_BlockMngr(buffer, size);										// parse and flash an array to EEprom memory 
							if(result == false) 
							{
								buffer[0]= NULL;
								WriteEEPI2C(rx_file_baseadd, &buffer[0], 1U);							// Write FAIL, azzero il primo byte del buffer EE per disattivarne l'uso
								resp = ERR_EE_WRITE;
								break;
							}
						}
					}
					HAL_Delay(Milli500);
					SetLed(LED_ROSSO, false);
					SetLed(LED_VERDE, true);
					HAL_Delay(Milli500);
				} while(true);
				
				if (resp != USB_AWS_ERR_OK)
				{
					break;
				}
				returnCode = f_close(&fp);  
			}
        }
    }
	returnCode = f_close(&fp);  

EndImportConf:	
	SetLed(LED_VERDE, true);
	SetLed(LED_ROSSO, false);
	return resp;
}


/*------------------------------------------------------------------------------------------*\
 Method: EE_Block
	Scrittura e rilettura di verifica di un blocco dati nella EEprom.
	Se la rilettura conferma la correttezza dei dati memorizzati, si incrementa l'address
	EEprom.

   IN:  - eeprom bank, source buffer pointer, buffer size
  OUT:  - address eeprom incrementato del block size se scrittura OK
\*------------------------------------------------------------------------------------------*/
static bool EE_BlockMngr(uint8_t *arr, uint32_t size)
{	
#define	BLOCKSIZE	0x1000;
	
	bool		result;
	uint16_t 	EEAddr, i, offs, BytesToRead, EntrySize, BuffReadOffs;
	
	EntrySize = size;
	EEAddr = (uint16_t)add_eeprom;																// Param predisposto prima della chiamata funzione
	result = false;
	
	if (WriteEEPI2C(EEAddr, arr, (uint16_t)size) == true)										// write data from file buffer to EEprom
	
	//-- Readback and Compare ---
	{
		result = true;
		offs = -1;
		BuffReadOffs = 0;
		do
		{
			if (size > sizeof(gVars.ddcmpbuff))
			{
				BytesToRead = (uint16_t)(sizeof(gVars.ddcmpbuff) - 16);
			}
			else
			{
				BytesToRead = (uint16_t)size;
			}
			ReadEEPI2C((EEAddr + BuffReadOffs), (uint8_t*)&gVars.ddcmpbuff, BytesToRead);		// data verify --> read and compare
			BuffReadOffs += BytesToRead;
			for (i=0; i < BytesToRead; i++)
			{
				offs++;
				if (*(arr + offs) != gVars.ddcmpbuff[i])
				{
					result = false;
					break;
				}
			}
			if (result == false) break;
			size -= BytesToRead;
		} while(size > 0);
	}
	if (result == true)
	{
		add_eeprom += EntrySize;																// Write OK, aggiorno address EEprom
	}
	return result;
}


/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : SetLed
* Returned Value : None
* Comments       : Set LED state true = led on; false = led off
*     
*END*--------------------------------------------------------------------*/
static void SetLed(uint32_t output, bool state)
{
	if (output == LED_ROSSO) RedLedCPU(state);
	if (output == LED_VERDE) GreenLedCPU(state);
}

/*---------------------------------------------------------------------------------------------*\
Method: GetMessageFromEE
	Trasferisce dalla EEPROM, a partire dall'indirizzo in EEAddr, tanti caratteri 
	fino a crlf (compresi) o a NULL.
	
	IN:	  - EEprom adress start e pointer al buffer di destinazione
	OUT:  - numero di caratteri trasferiti
\*----------------------------------------------------------------------------------------------*/
uint16_t  GetMessageFromEE(uint16_t * Addr_EEP, uint8_t *DestBuf)
{
	uint16_t	i, TempEE_Addr;
	uint8_t*	DestPnt;
	
	DestPnt = DestBuf;
	TempEE_Addr = *Addr_EEP;
	for (i=0; i < BUFFER_LENGTH; i++)
	{
		ReadEEPI2C((uint)(TempEE_Addr), (DestPnt + i), 0x01);										// Leggo un byte dalla EEPROM
		TempEE_Addr ++;
		if (*(DestPnt + i) == NULL)
		{
			//Found_Zero = true;
			//return i;
			break;
		}
		if ((*(DestPnt + i) == ASCII_n) && (*(DestPnt + (i-1)) == ASCII_BS)) break;					// Nei JSON file ricevuti da AWS il NL lo rappresento con \n
		if ((*(DestPnt + i) == ASCII_CR) && (*(DestPnt + (i-1)) == ASCII_LF)) break;
		if ((*(DestPnt + i) == ASCII_LF) && (*(DestPnt + (i-1)) == ASCII_CR)) break;
	}
	return (i >= BUFFER_LENGTH)? NULL: i;															// Test anche per superamento di MAX_LEN_STRING senza trovare crlf
}


#endif










