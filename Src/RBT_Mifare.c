/****************************************************************************************
File:
    RBT_Mifare.c

Description:
    File con funzioni di uso generale per la gestione delle carte/chiavi Mifare
    

History:
    Date       Aut  Note
    Set 2012	MR   

*****************************************************************************************/


/* Includes ------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>


#include "ORION.H" 
#include "RBT_Mifare.h"
#include "OrionCredit.h"
#include "AuditStrutture.h"
#include "Funzioni.h"
#include "Monitor.h"
#include "IICEEP.h"
#include "AuditStrutture.h"
#include "I2C_EEPROM.h"
#include "phcsBflStatus.h"
#include "VMC_CPU_HW.h"
#include "Power.h"

#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	DATEREFUNDS		DataRefundValues, DataRefundFC, DataRefundFV, DataRefundCashCB;
extern	uint8_t			FaseWrite;
extern	void 			Overpay(uint8_t);
extern	uint16_t 		TotaleCreditoAttuale(void);
extern	uint16_t		MifareReaderRead(uint8_t *rdbuff, uint8_t SectorToRead, uint8_t *BlockToRead);
extern	uint16_t 		MifareWriteAndCompare(uint8_t *wrbuff, uint8_t SectorToWrite, uint8_t *BlockToWrite, bool ReadAfterWrite);
extern	void			RevalueKR(uint8_t, uint32_t, uint8_t);
extern	bool  			MifareReaderRequest(void);
extern	bool 			MifareReaderAnticoll(uint8_t *ptSnum);
extern	bool 			CompareBuffer (uint8_t *ptBuf1, uint8_t *ptBuf2, uint8_t bufLen);
extern	void 			PresetAudFreeCredit(void);
extern	void 			PresetAudFreeVend(void);
extern	bool  			IsPeriodElapsed(DATEFREE *DataCarta);

extern	phcsBfl_Status_t 	PN512_set_led (uint8_t pin, uint8_t mask);
extern	void				Accendi_LedGiallo(void);


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/
// Array indice dei settori che contengono applicazioni RBT
// ---------|----------|
// |SectNum | TipoAppl |
// ---------|----------|


uint32_t	Secure0, Secure1, Secure2, Secure3;
uint32_t	cryptuid;
uint8_t  	RBT_IndexSect[30];				// Array indici Appl RBT

uint8_t		KeyB[6];						// Array con la Key calcolata con l'apposito algoritmo

uint8_t  	MAD_SectInfo;					// Settore che contiene l'InfoSector delle Mifare con MAD
uint8_t  	NoMAD_SectInfo;					// Settore che contiene l'InfoSector delle Mifare senza MAD
uint16_t	SecurCRC;

#define  	RBT_SectInfoLen sizeof(RBT_IndexSect)
//#define		uint32_t		LDD_TDeviceData

/*========================================================================================*/
// Dichiarazione delle strutture

DATAMIFARE 		MF_DataExch;				// Struttura per MIFARE
INFOCODE 		RBT_CodiciCarta;			// Struttura settore Info, Blocco Codici Carta
CREDITSECTOR 	RBTCredSect;				// Struttura settore Credito
GESTSEL 		CardGestSelVars;

static uint8_t		GPB, HEXH, HEXL, CRC8, BackupFreeVend;
static uint16_t		MADresp, Seed, CRC16, CRC16Inp, Dummy, BackupFreeCredit;
static uint32_t		CRC32Inp, Temp_FC;


// Usata per Debug
uint8_t 	DebugFormatCard;

const uint32_t k[]				= {0x4C2F59AD, 0x8E4D3FFA, 0xC27A5208, 0x2B6756CE};				// Chiave di calcolo cifratura
static uint8_t VergineRBTKeyB[] = {0xC7,0x14,0xD5,0x0A,0xE3,0x06};								// KeyB per accedere a carte vergini RBT

#if SetVergineRBT
	static uint8_t 	CardHolderKeyB[] = {0x6D,0x69,0x63,0x72,0x6F,0x68};							// KeyB per accedere al settore Card Holder (Solitamente il settore 1)
	//const uint32_t  F[] = {0x4C2F59AD, 0x8E4D3FFA, 0xC27A5965, 0x2B6756CE};						// Chiave di calcolo cifratura
	uint8_t			FormatType;

extern	void HexByteToAsciiInt(uint8_t, uint16_t*, uint8_t);
#endif
	

/*========================================================================================*/

uint16_t  ReadMifare(void) {
	
#if (SetVergineRBT == 1)
	//uint8_t	s, i;

	// FormatType == 1 --> DaUtenteACostruttore
	// FormatType == 2 --> Da_Costruttore_A_Vergine_MHD
	// FormatType == 3 --> Trasmette al PC KeyB, UID e codice di sicurezza di una carta Utente
	// FormatType == 4 --> Da_Costruttore_A_Vergine_Prepagata_MHD_Con_Taglio_10_Euro

	if 	(FormatType) {
	  response = Resp_OK;
	  if (!gOrionKeyVars.RFFieldFree) {

		// *********  Formatta una carta da vergine costruttore in vergine Prepagata MHD  ********
		if (FormatType == 4) {
			ReadEEPI2C(IICEEPConfigAddr(EEUtente), (uint8_t *)(&Utente), sizeof(Utente));
			GreenLedCPU(OFF);													// Led Verde CPU OFF
			RedLedCPU(ON);														// Led Rosso CPU ON
			response = Da_Costruttore_A_Vergine_Prepagata_MHD_Con_Taglio_10_Euro();
			gOrionKeyVars.RFFieldFree = true;									// Fine Format: attendere rimozione
			if (response == Resp_OK) {
				RedLedCPU(OFF);													// Led Rosso CPU OFF
				GreenLedCPU(ON);												// Formattazione OK: Led Verde CPU ON
				Utente++;
				WriteEEPI2C(IICEEPConfigAddr(EEUtente), (uint8_t *)(&Utente), sizeof(Utente));
			}			
		}

	/*
		// *******  Trasmette sulla SCI KeyB, UID e codice di sicurezza di una carta Utente ******
		if (FormatType == 3)
		{																					// Calcolo KeyB, codici sicurezza e li trasmesso sulla linea SCI al PC
			response = CalcolaDatiCarta();
			if (response == Resp_OK) {
				CalcKeyB(Pin1, Pin2);														// Calcola la chiave KeyB
				SetPC_Comm(Baud115200);
				i = 0;
				for (s=0; s<6; s++) {														// KeyB in formato ASCII in gVars.ddcmpbuff
					HexByteToAsciiInt(KeyB[s],&Dummy, NULL);
					gVars.ddcmpbuff[i] = Dummy>>8;
					gVars.ddcmpbuff[i+1] = Dummy;
					i +=2;
				}
				gVars.ddcmpbuff[i] = ' ';													// Space
				i++;
				for (s=0; s<4; s++) {														// KeyB in formato ASCII in gVars.ddcmpbuff
					HexByteToAsciiInt(Snum_media_in[s],&Dummy, NULL);
					gVars.ddcmpbuff[i] = Dummy>>8;
					gVars.ddcmpbuff[i+1] = Dummy;
					i +=2;
				}
				gVars.ddcmpbuff[i] = ' ';													// Space
				i++;
				for (s=0; s<16; s++) {														// Codice di sicurezza in formato ASCII in gVars.ddcmpbuff
					HexByteToAsciiInt(MF_DataExch.WrBuff[s],&Dummy, NULL);
					gVars.ddcmpbuff[i] = Dummy>>8;
					gVars.ddcmpbuff[i+1] = Dummy;
					i +=2;
				}
				gVars.ddcmpbuff[i] = ' ';													// Space
				i++;
				HexByteToAsciiInt(MF_DataExch.WrBuff[16],&Dummy, NULL);						// CRCL
				gVars.ddcmpbuff[i] = Dummy>>8;
				gVars.ddcmpbuff[i+1] = Dummy;
				i +=2;
				HexByteToAsciiInt(MF_DataExch.WrBuff[17],&Dummy, NULL);						// CRCH
				gVars.ddcmpbuff[i] = Dummy>>8;
				gVars.ddcmpbuff[i+1] = Dummy;
				i +=2;
				gVars.ddcmpbuff[i] = 0x0a;
				gVars.ddcmpbuff[i+1] = 0x0d;
				i +=2;
				gVars.devErr = TWRSER1_SendBlock(gVars.devCOM1, &gVars.ddcmpbuff[0], i);	// Trasmetto tutto il buffer al PC
				//SysMonitor.TxCardErrMsg = 1;
			}
			return response;
		}
	*/
		// *********  Formatta una carta da vergine costruttore in vergine MicroHard ********
		if (FormatType == 2) {
			ReadEEPI2C(IICEEPConfigAddr(EEUtente), (uint8_t *)(&Utente), sizeof(Utente));
			GreenLedCPU(OFF);													// Led Verde CPU OFF
			RedLedCPU(ON);														// Led Rosso CPU ON
			response = Da_Costruttore_A_Vergine_MHD();
			gOrionKeyVars.RFFieldFree = true;									// Fine Format: attendere rimozione
			if (response == Resp_OK) {
				RedLedCPU(OFF);													// Led Rosso CPU OFF
				GreenLedCPU(ON);												// Formattazione OK: Led Verde CPU ON
				Utente++;
				WriteEEPI2C(IICEEPConfigAddr(EEUtente), (uint8_t *)(&Utente), sizeof(Utente));
			}			
		}
		// ******** Ritrasforma una carta Utente in una carta vergine da costruttore *******
		if (FormatType == 1) {
			GreenLedCPU(OFF);													// Led Verde CPU OFF
			RedLedCPU(ON);														// Led Rosso CPU ON
			response = DaUtenteACostruttore(); 
			gOrionKeyVars.RFFieldFree = true;									// Fine Format: attendere rimozione
			if (response == Resp_OK) {
				// Formattazione OK: accendo il led verde
				RedLedCPU(OFF);													// Led Rosso CPU OFF
				GreenLedCPU(ON);												// Led Verde CPU ON
			}
		}
	  }
	  return response;
  
	}  // Fine SetVergineRBT
#endif
	

	LeggiDatiDaMifare();
	if (response != Resp_OK) return response;
	if ( (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_CBLK) || gOrionKeyVars.Prepag) return response;
	if (RefundValues.RefundCnt != 0 || RefundFreeCredit.RefundCnt != 0 || RefundFreeVend.RefundCnt != 0) {
		if (!GiveRefund()) return (response = ErrWriteRefund);
	}
	return response;
}

/*---------------------------------------------------------------------------------------------*\
Method: LeggiDatiDaMifare
	La LeggiDatiDaMifare legge i contenuti della carta/chiave quali il Credito e tutti i parametri opzionali.
	La LeggiDatiDaMifare impiega circa 280 msec a leggere il credito quando tutte le letture sono corrette.
	******************   Al 19 Set 2013 i msec di lettura carta sono circa 356 *****************
	IN:		- 
	OUT:	Return Reponse != Resp_OK se lettura fallita, altrimenti il Credito e altri valori.
\*----------------------------------------------------------------------------------------------*/
uint16_t  LeggiDatiDaMifare(void) {

	uint8_t i, Lev;

	memset(&sDataCarta, 0, sizeof(sDataCarta));											// Azzero strutture che potrebbero non essere utilizzate
	memset(&RBTCredSect, 0, sizeof(RBTCredSect));
	memset(&RBT_CodiciCarta, 0, sizeof(RBT_CodiciCarta));
	
	response = Resp_OK;																	// Per debug reset della variabile di uscita
	response = CheckMAD();

	/* ============================================================================================== */ 
	//  Errore Riattivazione Mifare dopo fallimento lettura Trailer Sect 0 --> REJECT
	if (response != Resp_OK && MADresp == FirstMAD_Read) return (response = NoValidCard);
	/* ============================================================================================== */ 
	//  MAD tipo 2  --> REJECT
	if (response == Resp_OK && MADresp == MAD2) return (response = NoValidFormat);
	/* ============================================================================================== */ 
	//  MAD1 presente ma errore lettura Blocchi 1 e 2 --> REJECT
	if (response != Resp_OK && MADresp == MADReadErr) return (response = Err_Read);
	/* ============================================================================================== */ 
	//  MAD1 presente ma senza applicazioni RBT --> REJECT
	if (response == Resp_OK && MADresp == MAD1_NoSectRBT) return (response = NoRBTCard);
	/* ============================================================================================== */ 

	SetKeyA(&KeyAReadMHD[0]);																	// Uso la Read KEYA per leggere tutte le carte
	if (response == Resp_OK && MADresp == MAD1_OK) {
		/* *************************************************************************************************
		  *********************************       MIFARE  CON  MAD     *************************************
		  **************************************************************************************************
		  La CheckMAD ha messo nell'array RBT_IndexSect il numero dei settori che contengono un'applicazione
		  RBT: cerco in questi settori l'InfoSector.   */
		
		MF_DataExch.BlockNum[0] = Trailer;														// Leggo il solo trailer di ogni Settore
		MF_DataExch.BlockNum[1] = NoBlock;
		MF_DataExch.BlockNum[2] = NoBlock;
		MF_DataExch.BlockNum[3] = NoBlock;
		MAD_SectInfo = 0;
		MF_DataExch.SectorType = 0;
		
		// Leggo tutti i settori con RBT application fino a trovare l'InfoSector o un ID che 
		// identifichi carte speciali MHD (AudCLR, MemCLR, MasterCard..etc)
		gOrionKeyVars.Prepag = false;
		for (i=0; i<RBT_SectInfoLen; i=i+2)  {
			MF_DataExch.SectNum = RBT_IndexSect[i];												// Settore da leggere
			if (MF_DataExch.SectNum != 0x00) {
				response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum);
				if (response == Resp_OK) {		
					GPB = MF_DataExch.RdBuff[GPB_POSITION];										// Il GPB e' il Byte 10 del Trailer di ogni settore.
					if (GPB == RBT_GenInfo || GPB == PrepagataInfoSct) {
						MAD_SectInfo = MF_DataExch.SectNum;										// Trovato InfoSect Card Utente (std o Prepagata)
						if (GPB == PrepagataInfoSct) {
							gOrionKeyVars.Prepag = true;
						}
						break;																	// Termino ricerca
					}
					// Controllo se nella carta c'e' un InfoSector relativo ad altri 
					// tipi di carte diversa dalla Utente
					if (GPB == RBT_Vergine || GPB == RBT_MasterCard || GPB == RBT_MHD_AudClrCard
							|| GPB == RBT_MHD_EEClrCard || GPB == Verg_Prep_InfoSct
							|| GPB == RBT_Gest_EEClrCard) {
						MF_DataExch.SectorType = GPB;											// Carta speciale MHD
						break;																	// Termino ricerca infosector
					}
				} else {
					response = Retry_Card_Activation();
					if (response != Resp_OK) return (response = NoValidCard);					// Errore di accesso o carta rimossa --> REJECT
				}
			} 
		}
	}  // FINE MIFARE CON MAD
	else  {

	/* =============================================================================================================================== */ 
		/* ***************************************************************************************************
		  *********************************       MIFARE  SENZA  MAD     *************************************
		  ****************************************************************************************************
		Cerco sequenzialmente il settore SectInfo che contiene l'indirizzo degli altri settori presenti sulla Mifare con applicazione RBT.
		Se ho gia' introdotto una Mifare ho gia' trovato il setttore SectInfo: verifico se anche su questa Mifare il settore e' lo stesso.
		Con questa tecnica, dopo la prima Mifare introdotta, l'accesso alle successive diventa molto rapido. */

		CalcKeyB(Pin1, Pin2);																	// Calcola la chiave KeyB di accesso alla Mifare
		SetKeyB(&KeyB[0]);																		// Predispongo l'uso della KeyB
		if ((response != Resp_OK && MADresp == SecondMAD_Read) || (response == Resp_OK && MADresp == NoMAD)) {

			// Riattivo Mifare se arrivo da un errore di accesso
			if (response != Resp_OK) {
				response = Retry_Card_Activation();
				if (response != Resp_OK) return (response = NoValidCard);						// Errore di accesso o carta rimossa --> REJECT
			}
			MF_DataExch.BlockNum[0] = Trailer;													// Leggo il solo trailer
			MF_DataExch.BlockNum[1] = NoBlock;
			MF_DataExch.BlockNum[2] = NoBlock;
			MF_DataExch.BlockNum[3] = NoBlock;
	
			if (NoMAD_SectInfo != 0) {
				MF_DataExch.SectNum = NoMAD_SectInfo;											// Accedo allo stesso Sect gia' utilizzato da altre Mifare precedenti
				response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum);
				if(response == Resp_OK) {
					GPB = MF_DataExch.RdBuff[GPB_POSITION];
					if (GPB != RBT_GenInfo) NoMAD_SectInfo = 0;									// Accesso Ok ma NON e' il settore con l'InfoSect 
				} else {
					NoMAD_SectInfo = 0;															// Errore di lettura: NON e' il settore con l'InfoSect
				}																				// Proseguo alla ricerca del settore con l'InfoSect
			}
			if (NoMAD_SectInfo == 0) {															// Non ho trovato il settore con l'InfoSect quindi lo ricerco
				for (i=1; i<MIFStdNumSect; i++)  {												// sequenzialmente partendo dal Settore N.1 
					if (response != Resp_OK) {													// Riattivo Mifare se arrivo da un errore di accesso
						response = Retry_Card_Activation();
						if (response != Resp_OK) return (response = NoValidCard);				// Errore di accesso o carta rimossa --> REJECT
					}
					MF_DataExch.SectNum = i;													// Settore da leggere
					response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum);
					if(response == Resp_OK) {
						GPB = MF_DataExch.RdBuff[GPB_POSITION];
						if (GPB == RBT_GenInfo) {
							NoMAD_SectInfo = MF_DataExch.SectNum;								// Trovato InfoSect: interrompo la ricerca
							break;
						}
					}
				}
			}
		}
	}

	/* ============================================================================================== */ 
	//  Leggo il Settore InfoSect che contiene l'indice dei settori con applicazione RBT

	// Ricerco qual'e' il settore che contiene il SectorInfo e lo metto in "MF_DataExch.SectNum" 
	if (MF_DataExch.MAD) {
		if (MAD_SectInfo != 0) {
			MF_DataExch.SectNum = MAD_SectInfo;													// Settore InfoSect di una Card Utente 
		} else {

			// **********   Carta Vergine MHD Utente o  Prepagata  ************ 
			if (MF_DataExch.SectorType == RBT_Vergine || MF_DataExch.SectorType == Verg_Prep_InfoSct) {
				if (Pin1 == 0 && Pin2 == 0) return (response == PinZero);						// Se i Pin sono zero non inizializzo la carta
				response = InizializzaCartaUtente();											// Inizializzo carta Vergine MHD
				if (response == Resp_OK) {
					MF_DataExch.SectNum = MAD_SectInfo;											// Settore con InfoSect
				} else {
					return response;															// Inizializzazione fallita
				}
			}

			// **********   Master  Card ************ 
			if (MF_DataExch.SectorType == RBT_MasterCard) {
				MasterCard();																	// Se Master Card valida aggiorno codici e attendo WDT reset
				Set_AttesaEstrazione();
				return response;																// Altrimenti esco con errore carta
			}
			
			// **********   Carta Azzeramento Memoria EEPROM ************ 
			if (MF_DataExch.SectorType == RBT_MHD_EEClrCard
					|| MF_DataExch.SectorType == RBT_Gest_EEClrCard ) {							// Se Card Azzeramento memoria valida cancello tutta la memoria e mi fermo
				Card_ClrMemory(MF_DataExch.SectorType);											// con led rosso CPU lampeggiante
				return response;																// Altrimenti esco con errore carta
			}
			
			// **********   Carta  Azzeramento Audit ************ 
			if (MF_DataExch.SectorType == RBT_MHD_AudClrCard) {
				ClrAuditCard();																	// Se Card Azzeramento Audit valida cancello tutta l'Audit e mi fermo 
				return response;																// con led rosso CPU lampeggiante
			}
			if (MAD_SectInfo == 0) return (response = NoInfoSect);
		}
	
	} else  {

		// Carte senza il MAD
		if (NoMAD_SectInfo != 0) {
			MF_DataExch.SectNum = NoMAD_SectInfo;								// Settore con InfoSect
		} else {
			return (response = NoInfoSect);										// Non c'e' Infosect su questa Mifare
		}
	}
// -----------------------------------------------------------------------------------------------
// *****  Lettura InfoSector  *******

	// Verifico se e' la carta RBT 
	CheckCartaRBT();
	
	if (gOrionKeyVars.RTB_Card == 6916) {
		CalcKeyB(25965, 5961);																// Calcola la chiave KeyB di accesso alla CartaRBT
	} else {
		CalcKeyB(Pin1, Pin2);																// Calcola la chiave KeyB di accesso alla Mifare
	}
	SetKeyB(&KeyB[0]);																		// Predispongo l'uso della KeyB
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	if (!LeggiSettore()) return response;													// Errore di accesso o carta rimossa --> REJECT

	// Trasferisco il Blocco 0 di InfoSect dalla Mifare alla struttura RBT_CodiciCarta se il CRC e' corretto 
	if (!MIFBlock_To_CodiciCarta(&MF_DataExch.RdBuff[0])) return (response = Fail_CRC);

	gOrionKeyVars.KR_TipoChiave = RBT_CodiciCarta.TipoCarta;
	gOrionKeyVars.CodiceUtente = RBT_CodiciCarta.Utente;
	gOrionKeyVars.LivelliChiave = RBT_CodiciCarta.Livelli;
	if (gOrionKeyVars.RTB_Card != 6916) {														// Non CartaRBT: controllo codici
		if (RBT_CodiciCarta.Gestore != Gestore) return (response = Fail_Gestore);
		if (gOrionConfVars.EnDisLocationCode) {
			if (RBT_CodiciCarta.Locazione != Locazione) return (response = Fail_Locaz);			// Se Codice Locazione non corretto rifiuto la carta
		} else {
			if (Locaz1 || Locaz2 || Locaz3 || Locaz4) {											// Se Locaz1, Locaz2, Locaz3 e Locaz4 = 0 esco senza controllare Codice Locazione 
				if (RBT_CodiciCarta.Locazione != Locaz1 && RBT_CodiciCarta.Locazione != Locaz2 
					&& RBT_CodiciCarta.Locazione != Locaz3 && RBT_CodiciCarta.Locazione != Locaz4
					&& RBT_CodiciCarta.Locazione != Locazione) {
					return (response = Fail_Locaz);												// Rifiuto la carta se il suo Cod Locazione diverso da tutti e 5 i Cod Locazione
				}
			}
		}
	}

	// Trasferisco il Blocco 1 di InfoSect dalla Mifare nell'array RBT_IndexSect se il CRC e' corretto 
	if (!MIFBlock_To_SectorInfo(&MF_DataExch.RdBuff[1*MIF_BLOCKLEN])) return (response = Fail_CRC);
	
	
/* ============================================================================================== */ 
//  Cerco il Settore Sicurezza (per ora solo nelle carte con MAD)
	
// ********** ATTENZIONE: LE CARTE SENZA MAD SONO RIFIUTATE PER MANCANZA SETTORE SICUREZZA *******  (12.09.2013)

#if	!SaltaSicurezza
	
  if (MF_DataExch.MAD) {
	response = NoSecureSect;																	// Predispongo errore mancanza settore Sicurezza
	for (i=1; i<(MIFStdNumSect-2); i=i+2) {
		if (RBT_IndexSect[i] == RBT_Secure) {
			MF_DataExch.SectNum = RBT_IndexSect[i-1];											// Settore Sicurezza
			SetKeyA(&SicurRBTKeyA[0]);															// Set KEYA Sicur per Autenticare il Settore Sicurezza
			MF_DataExch.BlockNum[0] = Block0;
			MF_DataExch.BlockNum[1] = Block1;
			MF_DataExch.BlockNum[2] = NoBlock;
			MF_DataExch.BlockNum[3] = NoBlock;
			if (!LeggiSettore()) return response;												// Errore di accesso o carta rimossa --> REJECT

			Secure0 = (MF_DataExch.RdBuff[0] | (MF_DataExch.RdBuff[1]<<8) | (MF_DataExch.RdBuff[2]<<16) | (MF_DataExch.RdBuff[3]<<24) );
			Secure1 = (MF_DataExch.RdBuff[4] | (MF_DataExch.RdBuff[5]<<8) | (MF_DataExch.RdBuff[6]<<16) | (MF_DataExch.RdBuff[7]<<24) );
			Secure2 = (MF_DataExch.RdBuff[8] | (MF_DataExch.RdBuff[9]<<8) | (MF_DataExch.RdBuff[10]<<16) | (MF_DataExch.RdBuff[11]<<24) );
			Secure3 = (MF_DataExch.RdBuff[12] | (MF_DataExch.RdBuff[13]<<8) | (MF_DataExch.RdBuff[14]<<16) | (MF_DataExch.RdBuff[15]<<24) );
			SecurCRC = (MF_DataExch.RdBuff[16] | (MF_DataExch.RdBuff[17]<<8) );

			// Calcolo checksum dell'intero Blocco 0 criptato
			SetCRC16Buff(&MF_DataExch.RdBuff[0], MIF_BLOCKLEN, SemeCrypto);
			if (!gVars.CRC16 == SecurCRC) return (response = SecureCRCErr);						// Errore CRC Sicurezza
			if (!CheckSicurezza()) return (response = SecureErr);								// Errore settore Sicurezza
			response = Resp_OK;
			break;
		}
	}
	if (response != Resp_OK)  return (response = NoSecureSect);									// Non trovato alcun Settore Sicurezza
  } else {
		return (response = NoSecureSect);														// Non trovato alcun Settore Sicurezza
  }
	
#endif
	
// =================================================================
// *****************************************************************
// ******    TEST  CARTA  CARICAMENTO A BLOCCHI        *************
// *****************************************************************
//  Cerco, nella struttura degli indici, il Settore che contiene i dati
//	di Caricamento a Blocchi (la struttura del Settore e' come quella
//	del Credito).

	if (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_CBLK) {
		response = GestioneStatoEnabled();														// Controllo se accettare la carta
		if (response != Resp_OK) return response;												// Carta rifiutata

	  	response = NoSectCaricBlk;																// Predispongo errore mancanza settore dati Caricamento a Blocchi
		for (i=1; i<(MIFStdNumSect-2); i=i+2) {
			if (RBT_IndexSect[i] == RBT_CaricamBlk) {
				MF_DataExch.SectNum = RBT_IndexSect[i-1];										// Settore con i dati di Caricamento a Blocchi
				if (!ReadCreditSector()) return response;
				
				DofWk_CaricamBlk  	= (uint8_t)DataOra.DayOfWeek;									// Predispongo data odierna  
				Day_CaricamBlk 		= (uint8_t)DataOra.Day;
				Mese_CaricamBlk		= (uint8_t)DataOra.Month;
				Anno_CaricamBlk		= (uint8_t) (DataOra.Year - MILLENNIO_2000);
				if (CashCaricamentoBlocchi > 0) {
					// **** Carta reintrodotta: riaccredito blocco scalato  *****
					if (OpMode == MDB) {
						if (gOrionKeyVars.State != kOrionCPCStates_Idle) {						// Reintrodotta carta CB ma Orion non ancora in IDLE quindi esco
							return (response = WaitElab);										// senza elaborare la carta
						}
					}
					PN512_set_led ((led_verde+led_rosso), (!led_verde|led_rosso));					// Led verde OFF - Led Rosso ON
					PSHProcessingData();														// Azzero dati di caricamento prima di scrivere la carta
					CashCaricamentoBlocchi = 0;
					ValoreBloccoCaricamento = 0;
					PSHCheckPowerDown();
					NumeroBlocchi ++;
					if (WrCredit()) {															// Scrittura dati caricamento a blocchi ok
						response = Resp_OK;
					} else {
						response = OK_WaitEstraz;												// Scrittura fallita: attendo estrazione carta
					}
				} else {
					// ****  Introduzione carta: scalare un blocco  ******
					if (OpMode == MDB) {
						if (gOrionKeyVars.State != kOrionCPCStates_Enabled) {					// Introdotta carta CB ma Orion non ancora in ENABLE quindi esco
							return (response = WaitElab);										// senza elaborare la carta
						}
					}
					if (RefundCashCB.RefundCnt > 0) {											// Se ci sono dati nella struttura Refund controllo se appartengono
						GiveRefund();															// alla carta in entrata
					}
					if (NumeroBlocchi == 0 || ValoreBlocco == 0) {
						RifiutaCarta();															// Accende led giallo ad indicazione ad indicazione
						return (response = Err_CAR_7);
					}
					PN512_set_led (led_verde+led_rosso, !led_verde|led_rosso);					// Led verde OFF - Led Rosso ON
					NumeroBlocchi --;
					if (WrCredit()) {															// Scrittura dati caricamento a blocchi ok
						PSHProcessingData();													// Scrittura OK: utilizzo i dati di caricamento
						CashCaricamentoBlocchi = ValoreBlocco;
						ValoreBloccoCaricamento = ValoreBlocco;
						UID_CaricamBlock = Snum_media_in[0]<<24 | Snum_media_in[1]<<16 | Snum_media_in[2]<<8 | Snum_media_in[3];
						PSHCheckPowerDown();
						CaricaToutCash();														// Carico Timeout Cash
#if  DebugCaricBlk
						Tmr32Start(gOrionCashVars.creditTmr, THIRTYSEC);						// Per debug carico 30 sec
#endif						
						gOrionKeyVars.CaricamBlocchi = true;
						response = Resp_OK;
					} else {
						response = OK_WaitEstraz;												// Scrittura fallita: attendo estrazione carta
					}
				}
				PN512_set_led (led_verde+led_rosso, !led_verde|!led_rosso);						// Led verde OFF - Led Rosso OFF
				gOrionKeyVars.Credit = ValoreBlocco;											// Visualizzo il valore del blocco
				break;
			}
		}
		return response;
	}

	// =================================================================
	// *****************************************************************
	// ***********    LETTURA   CREDITO  CARTA           ***************
	// *****************************************************************
	//  Cerco, nella struttura degli indici, il Settore che contiene il 
	//	Credito.

		response = NoCreditSect;																	// Predispongo errore mancanza settore Credito
		for (i=1; i<(MIFStdNumSect-2); i=i+2) {
			if (RBT_IndexSect[i] == RBT_Credito || RBT_IndexSect[i] == PrepagataCreditSct) {		// Settore Credito
				MF_DataExch.SectNum = RBT_IndexSect[i-1];
				if (!ReadCreditSector()) return response;
				gOrionKeyVars.Credit 	 = RBTCredSect.Cred;
				gOrionKeyVars.FreeCredit = 0;
				gOrionKeyVars.FreeVend	 = 0;
				if (gOrionFreeCrVars.FreeCrPeriod == 0) {											// FreeCredit NON abilitato: salvo il freecredit della carta per non
					BackupFreeCredit = RBTCredSect.FreeCr;											// cancellarglielo
				}
				if (gOrionFreeVendVars.FreeVendPeriod == 0) {										// FreeVend NON abilitate: salvo le freevend della carta per non
					BackupFreeVend = RBTCredSect.FreeVend;											// cancellargliele
				}
				response = Resp_OK;
				break;
			}
		}
		if (response != Resp_OK) {
			return (response = NoCreditSect);														// Da qui non dovrebbe mai uscire: non trovato alcun Settore Credito
		}
	

// ==============================================================
// **********   TEST INIBIZIONE ACCETTAZIONE CARTE   ************ 

	response = GestioneStatoEnabled();															// Controllo se accettare la carta
	if (response != Resp_OK) {
		gOrionKeyVars.Credit = 0;														// 12.11.14: senza il clr, timeout di eventuale cash, il PK3-EXE aggiorna il display VMC
		return response;																// con la somma di tutti i crediti e visualizza il credito dell'ultima carta letta.
	}																					// Il credito non e' ovviamente utilizzabile ma non e' corretto vedere un credito strano

	if (gOrionKeyVars.Prepag 																	// Carta prepagata o carta Vendite di Test: non ci sono altri dati da leggere,
		|| gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_VENDITE_PROVA) return response;				// pertanto esco

// =================================================================
// *****************************************************************
// ******    TEST  CASH DA CARICAMENTO A BLOCCHI        ************
// *****************************************************************
//  Controllo se c'e' credito da Caricamento a Blocchi da aggiungere
//	al credito carta. Lo aggiungo solo se la somma dei crediti non
//	supera MAX_SYSTEM_CREDIT.
//	Come per il cash azzero prima si scrivere la carta.

	if (CashCaricamentoBlocchi > NULL_VALUE) {														// C'era una carta di Caricamento a Blocchi
		//if (!ICPCPCTest_IdleState()) return (response = WaitElab);									// In attesa Begin Session precedente carta CB: attendo
		if ((TotCreditCard() + CashCaricamentoBlocchi) < MAX_SYSTEM_CREDIT) {						// La somma dei crediti carta NON supera il massimo ammissibile
			gOrionKeyVars.Credit += CashCaricamentoBlocchi;
			RevalueKR(REVALUE_KR_CARICAMENTO_BLOCCHI, CashCaricamentoBlocchi, CaricamentoDubbio);	// Predispongo Audit caricamento a blocchi con caricamento dubbio
			PSHProcessingData();																	// Azzero cash caricamento a blocchi
			CashCaricamentoBlocchi = NULL_VALUE;
			ValoreBloccoCaricamento = NULL_VALUE;
			PSHCheckPowerDown();
			gOrionKeyVars.FreeCredit = RBTCredSect.FreeCr;											// Ripristino FreeCredit altrimeni sarebbe stato scritto a zero 
			gOrionKeyVars.FreeVend = RBTCredSect.FreeVend;											// Ripristino FreeVend altrimeni sarebbero state scritte a zero 
			PN512_set_led (led_verde+led_rosso, !led_verde|led_rosso);								// Led verde OFF - Led Rosso ON
			if (WrCredit()) {																		// Scrittura nuovo credito
				response = Resp_OK;
				RechargeFlags &= ~(CaricamentoDubbio);												// Non aggiorno counter "EA2*EK_03 accredito dubbio"
			} else {
				response = OK_WaitEstraz;															// Scrittura fallita: attendo estrazione carta
			}
		}
		PN512_set_led (led_verde+led_rosso, !led_verde|!led_rosso);									// Led verde OFF - Led Rosso OFF
		return response;
	}

// =============================================================
// ******************************************************
// ***********    TEST  FREE-CREDIT       ***************
// ******************************************************
// Il FreeCredit in aggiunta e il credito normale non devono superare
// il MAX_SYSTEM_CREDIT: in tal caso limito il FreeCredit aggiunto.
// Anche in sostituzione la somma dei crediti non deve superare il
// MAX_SYSTEM_CREDIT.
	
	Lev = gOrionKeyVars.LivelliChiave>>8;														// Il livello e' il MSB
	if (gOrionFreeCrVars.FreeCrPeriod) {
		if (gOrionConfVars.CardLevelsEnabled && (Lev == 0 || Lev > 4)) {						// La carta non appartiene ad alcun livello
			gOrionKeyVars.RestoreFreeCred = true;												// Non utilizzare il FreeCredit
			BackupFreeCredit = RBTCredSect.FreeCr;												// Salvo FreeCredit per non cancellarglielo
			goto End_TestFreeCredit;
		}
		gOrionKeyVars.FreeCredit = RBTCredSect.FreeCr;											// Utilizzo FreeCredit letto dalla carta
		sDataCarta.DofWk 	= RBTCredSect.DofWk_FreeCr;											// Preparo dati carta nella struttura per il test del periodo 
		sDataCarta.Giorno 	= RBTCredSect.Giorno_FreeCr;
		sDataCarta.Mese 	= RBTCredSect.Mese_FreeCr;
		sDataCarta.Anno 	= RBTCredSect.Anno_FreeCr;
		sDataCarta.Periodo 	= gOrionFreeCrVars.FreeCrPeriod;									// Periodo FreeCredit come da configurazione
		
		if (IsPeriodElapsed(&sDataCarta)) {														// Periodo trascorso: attribuire nuovo Free Credit
			Temp_FC = gOrionFreeCrVars.FreeCr1;													// Free Credit 1 as default
			if (gOrionConfVars.CardLevelsEnabled) {												// Uso Livelli carte
				switch (Lev) {
					case 1:
						Temp_FC = gOrionFreeCrVars.FreeCr1;
						break;
					case 2:
						Temp_FC = gOrionFreeCrVars.FreeCr2;
						break;
					case 3:
						Temp_FC = gOrionFreeCrVars.FreeCr3;
						break;
					case 4:
						Temp_FC = gOrionFreeCrVars.FreeCr4;
						break;
				}
			}
			PSHProcessingData();
			if (gOrionFreeCrVars.FreeCrMode) {													// FreeCredit Aggiunto
				if ((TotCreditCard() + Temp_FC) > MAX_SYSTEM_CREDIT) {							// La somma dei crediti carta supera il massimo ammissibile
					Temp_FC = MAX_SYSTEM_CREDIT - (TotCreditCard());							// Il FreeCredit e' la rimanenza per arrivare a MAX_SYSTEM_CREDIT
				}
				FreeCreditAddebitato = 0;
			} else {																			// FreeCredit Sostituito
				if ((gOrionKeyVars.Credit + Temp_FC) > MAX_SYSTEM_CREDIT) {						// La somma dei crediti carta supera il massimo ammissibile
					Temp_FC = MAX_SYSTEM_CREDIT - gOrionKeyVars.Credit;							// Il FreeCredit e' la rimanenza per arrivare a MAX_SYSTEM_CREDIT
				}
				FreeCreditAddebitato = gOrionKeyVars.FreeCredit;								// Serve solo per l'Audit
				gOrionKeyVars.FreeCredit = 0;
			}
			gOrionKeyVars.FreeCredit += Temp_FC;												// Accredito Free-credit perche' sia conteggiato nel credito totale verso il VMC...
			FreeCreditAccreditato = Temp_FC;													// ..anche se non e' ancora scritto sulla carta
			PSHCheckPowerDown();
			NewFC_Attribuito = gOrionKeyVars.FreeCredit;										// Salvo valore FC per usarlo quando FC alla selezione e preceduta da revalue
			
			PresetAudFreeCredit();																// Predispongo dati per Audit Free-Credit
			
			if (FreeCredAllaSelez == false) {													// Free Credit subito
				SetDataOdiernaFreeCredit();														// Predispone la data nei bytes RBTCredSect
				PN512_set_led (led_verde+led_rosso, !led_verde|led_rosso);						// Led Antenna verde OFF - Led Rosso ON
				WrCredit();																		// Scrittura Nuovo Credito Carta
				PN512_set_led (led_verde+led_rosso, !led_verde|!led_rosso);						// Led Antenna verde OFF - Led Rosso OFF
				if (FaseWrite >= FaseToutWRFlag) {												// Avevo gia' cominciato a scrivere la flag del credito
					PSHProcessingData();														// quindi aggiorno subito Audit FreeCredit
					StatusFreeCredit = 1;														// Trigger Audit Free Credit
					PSHCheckPowerDown();
				} else {																		// Scrittura fallita
					gOrionKeyVars.Credit 	 = NULL;
					gOrionKeyVars.FreeCredit = NULL;
					return (response = OK_WaitEstraz);
				}
			} else {																			// Free Credit alla selezione
				gOrionKeyVars.WriteNewFreeCred = true;											// Aggiorna al momento della selezione
			}
		}
	}

End_TestFreeCredit:

// =======================================================
// ******************************************************
// ***********    TEST  FREE-VEND         ***************
// ******************************************************
		
	if (gOrionFreeVendVars.FreeVendPeriod) {
		if (gOrionConfVars.CardLevelsEnabled && (Lev == 0 || Lev > 4)) {						// La carta non appartiene ad alcun livello
			gOrionKeyVars.RestoreFreeVend = true;												// Non utilizzare le FreeVend
			BackupFreeVend = RBTCredSect.FreeVend;												// Salvo le FreeVend per non cancellargliele
			goto End_TestFreeVend;
		}
#if DefPARALLELO	
		if (OpMode_PAR == ModeSingleVendAutom) {
			if (gOrionKeyVars.Credit < (uint32_t)(FVCardMinCred())) {
				gOrionKeyVars.RestoreFreeVend = true;											// Non utilizzare le FreeVend
				BackupFreeVend = RBTCredSect.FreeVend;											// Salvo le FreeVend per non cancellargliele
				goto End_TestFreeVend;
			}
		}
#endif		
		if (gOrionKeyVars.RTB_Card == 6916) {													// Solo per carta RBT in PASVENS a Scadenza simulo data Free-vend uguale al giorno
			sDataCarta.Giorno 	= (uint8_t)DataOra.Day;											// odierno ma del prossimo anno. La struttura sDataCarta sara' elaborata in Pasvens.c
			sDataCarta.Mese 	= (uint8_t)DataOra.Month;
			sDataCarta.Anno 	= (uint8_t) (DataOra.Year - MILLENNIO_2000);
			sDataCarta.Anno ++;
		} else {
			sDataCarta.DofWk 	= RBTCredSect.DofWk_FreeVend;									// Preparo dati carta nella struttura per il test del periodo 
			sDataCarta.Giorno 	= RBTCredSect.Giorno_FreeVend;
			sDataCarta.Mese 	= RBTCredSect.Mese_FreeVend;
			sDataCarta.Anno 	= RBTCredSect.Anno_FreeVend;
			sDataCarta.Periodo 	= gOrionFreeVendVars.FreeVendPeriod;							// Periodo FreeVend come da configurazione
			if (gOrionFreeVendVars.UseOnly == 0) {
				gOrionKeyVars.FreeVend = RBTCredSect.FreeVend;									// Utilizzo FreeVend lette dalla carta
				if (IsPeriodElapsed(&sDataCarta)) {												// Periodo trascorso: attribuire nuove FreeVend
					Dummy = gOrionFreeVendVars.FreeVend1;										// FreeVend 1 as default
					if (gOrionConfVars.CardLevelsEnabled) {										// Uso Livelli carte
						switch (Lev) {
							case 1:
								Dummy = gOrionFreeVendVars.FreeVend1;
								break;
							case 2:
								Dummy = gOrionFreeVendVars.FreeVend2;
								break;
							case 3:
								Dummy = gOrionFreeVendVars.FreeVend3;
								break;
							case 4:
								Dummy = gOrionFreeVendVars.FreeVend4;
								break;
						}
					}
					if (gOrionFreeVendVars.FreeVendMode) {										// FreeVend Aggiunte
						if ((Dummy + gOrionKeyVars.FreeVend) > MaxNumFreeVend) {				// La somma delle FreeVend supera il massimo ammissibile
							Dummy = (MaxNumFreeVend - gOrionKeyVars.FreeVend);					// Le FreeVend sono la rimanenza per arrivare a MaxNumFreeVend
						}
					} else {																	// FreeVend Sostituite
						gOrionKeyVars.FreeVend = 0;
					}
					gOrionKeyVars.FreeVend += Dummy;
					NewFV_Attribuite = gOrionKeyVars.FreeVend;									// Salvo valore FV per usarlo quando FV alla selezione e preceduta da revalue

					PresetAudFreeVend();														// Predispongo dati per Audit Free-Vend
					
					if (FreeVendAllaSelez == false) {											// Free Vend subito
						SetDataOdiernaFreeVend();												// Predispone la data nei bytes RBTCredSect
						PN512_set_led (led_verde+led_rosso, !led_verde|led_rosso);				// Led verde OFF - Led Rosso ON
						WrCredit();																// Scrittura Nuove FreeVend
						PN512_set_led (led_verde+led_rosso, !led_verde|!led_rosso);				// Led verde OFF - Led Rosso OFF
						if (FaseWrite < FaseToutWRFlag) {										// NON avevo ancora cominciato a scrivere la flag del credito 
							return (response = OK_WaitEstraz);									// quindi scrittura fallita: attendo estrazione
						}
					} else {																	// Free Vend alla selezione
						gOrionKeyVars.WriteNewFreeVend = true;									// Aggiorna al momento della selezione
					}

				}
			}
		}
	}
End_TestFreeVend:
	
	return response;
}		// Fine LeggiDatiDaMifare

/* ****************************************************************************************************************************************************************************
*******************************************************************************************************************************************************************************
********************************************************************************************************************************************************************************/

#if DefPARALLELO
/*---------------------------------------------------------------------------------------------*\
Method: ReadSel_Gest_Sector
	La ReadSel_Gest_Sector trasferisce il Settore Tipo Sel_Gest nella struttura "CardGestSelVars".
	
	IN:		- ID del Settore Sel_Gest (RBT_SelGest1 o RBT_SelGest2)
	OUT:	- struttura "CardGestSelVars" con i dati dalla carta
\*----------------------------------------------------------------------------------------------*/
static bool  ReadSel_Gest_Sector(uint8_t ID_GestSect) {

	uint8_t	n, BytePnt, DestPnt, fBlock;
	
	SetKeyB(&KeyB[0]);																		// Predispongo l'uso della KeyB
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	if (!LeggiSettore()) return false;														// Errore di accesso o carta rimossa --> REJECT
	fBlock = MF_DataExch.RdBuff[(Block2 * MIF_BLOCKLEN)];									// Flag blocco attivo in fBlock
	if ((fBlock & FlagCredMask) == 0x00) {
		BytePnt = 0*MIF_BLOCKLEN;															// Pointer al blocco 1 del Settore Sel_Gest
	} else {
		BytePnt = 1*MIF_BLOCKLEN;															// Pointer al blocco 2 del Settore Sel_Gest
	}
	if (ID_GestSect == RBT_SelGest1) {
		// Dati dal Settore Sel_Gest1
		CardGestSelVars.SelGestFlag[0] = fBlock;
		DestPnt = 0;
	} else {
		// Dati dal Settore Sel_Gest2
		CardGestSelVars.SelGestFlag[1] = fBlock;
		DestPnt = 4;
	}
	for (n=0; n<4; n++) {
		CardGestSelVars.Selez[DestPnt+n] = MF_DataExch.RdBuff[BytePnt++];
		CardGestSelVars.Selez[DestPnt+n] |= (MF_DataExch.RdBuff[BytePnt++] << 8);
		CardGestSelVars.Selez[DestPnt+n] |= (MF_DataExch.RdBuff[BytePnt++] << 16);
		CardGestSelVars.Selez[DestPnt+n] |= (MF_DataExch.RdBuff[BytePnt++] << 24);
	}
	return true;
}	

/*---------------------------------------------------------------------------------------------*\
Method: Prepare_Gest_Sector
	La Prepare_Gest_Sector trasferisce la struttura "CardGestSelVars" nel Settore Sel_Gest.
	
	IN:		- ID del Settore Sel_Gest (RBT_SelGest1 o RBT_SelGest2)
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
static void  Prepare_Gest_Sector(uint8_t *wrbuffPnt, uint8_t selgestgroup) {

	uint8_t	n, BytePnt, SourcePnt, fBlock;
	
	BytePnt = 0;																			// Pointer al buffer MF_DataExch.WrBuff
	if (selgestgroup) {
		SourcePnt = 4;
	} else {
		SourcePnt = 0;
	}
	for (n=0; n<4; n++) {
		wrbuffPnt[BytePnt++] = (uint8_t)CardGestSelVars.Selez[SourcePnt+n];
		wrbuffPnt[BytePnt++] = (uint8_t)(CardGestSelVars.Selez[SourcePnt+n] >> 8);
		wrbuffPnt[BytePnt++] = (uint8_t)(CardGestSelVars.Selez[SourcePnt+n] >> 16);
		wrbuffPnt[BytePnt++] = (uint8_t)(CardGestSelVars.Selez[SourcePnt+n] >> 24);
	}
}	

/*---------------------------------------------------------------------------------------------*\
Method: WrSect_SelGest
	La WrSect_SelGest aggiorna sulla carta il Settore Sel_Gest relativo alla selezione richiesta. 
	La WrSect_SelGest impiega circa xxx msec per scrivere e rileggere i due blocchi.
	
	IN:		- gOrionKeyVars.Credit, gOrionKeyVars.CreditFlag
			- Snum_media_in[] che contiene l'UID
			- la struct MF_DataExch con KeyA/KeyB per l'autenticazione e il valore della Key
	OUT:	Response=Resp_OK se la scrittura del credito e' andata a buon fine.
\*----------------------------------------------------------------------------------------------*/
static uint16_t WrSect_SelGest(void) {

	uint8_t	i;
	bool 	RdAfterWr, WrSectSelez1;
	
	WrSectSelez1 = false;																		// Flag "scrivi sempre settore con selez 1" = false
	gOrionKeyVars.UpdateGestSel = false;														// Clr flag aggiorna settori Sel_Gest
	MF_DataExch.BlockNum[2] = Block2;															// Blocco con la flag blocco attivo
	MF_DataExch.BlockNum[3] = NoBlock;															// Blocco da NON scrivere del settore SectNum
	for (i=0; i<MIF_BLOCKLEN; i++) {															// 0xff in tutto il blocco 2
		MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)+i] = 0xFF;
	}
	RdAfterWr = true;
	if (gOrionKeyVars.RTB_Card == 6916) {
		CalcKeyB(25965, 5961);																	// Calcola la chiave KeyB di accesso alla CartaRBT
	} else {
		CalcKeyB(Pin1, Pin2);																	// Calcola la chiave di accesso al Settore
	}
	if ((gOrionKeyVars.VendSel) <= 4) {
		// Selezione compresa tra 1 e 4: scrivo il solo settore Sel_Gest1
		response = UpDate_SelGest1(RdAfterWr);													// Scrivo Sel_Gest 1 con read-after-write perche' e' l'unico settore da aggiornare
	} else {
		// Selezione compresa tra 5 e 8, scrivo il settore Sel_Gest2.
		// Se si utilizzano i contatori globali battute, prima si scrive il Sel_Gest 1 che
		// contiene il contatore delle selezioni che la carta puo' avere, poi il Sel_Gest 2.
		if (gPostMixVars.GlobalSelez) {
			response = UpDate_SelGest1(false);													// Scrivo Sel_Gest 1 senza read-after-write perche' poi devo scrivere il Sel_Gest 2
			if (response != Resp_OK) return response;											// Scrittura fallita: esco
		}
		MF_DataExch.SectNum = CardGestSelVars.SelGestSectors[1];								// Sel_Gest 2
		if ((CardGestSelVars.SelGestFlag[1] & FlagCredMask) == 0x00) {
			CardGestSelVars.SelGestFlag[1] = SetFlagCred;
			MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)] = CardGestSelVars.SelGestFlag[1];		// Flag blocco attivo in WrBuff
			Prepare_Gest_Sector(&MF_DataExch.WrBuff[1*MIF_BLOCKLEN], 1U);						// Dati nel Blocco 1
			MF_DataExch.BlockNum[0] = NoBlock;
			MF_DataExch.BlockNum[1] = Block1;
		} else {
			CardGestSelVars.SelGestFlag[1] = ClrFlagCred;
			MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)] = CardGestSelVars.SelGestFlag[1];		// Flag blocco attivo in WrBuff
			Prepare_Gest_Sector(&MF_DataExch.WrBuff[0*MIF_BLOCKLEN], 1U);						// Dati nel Blocco 0
			MF_DataExch.BlockNum[0] = Block0;
			MF_DataExch.BlockNum[1] = NoBlock;
		}
		response = MifareWriteAndCompare((uint8_t*)&MF_DataExch.WrBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum, RdAfterWr);
	}
	return response;
}

/*---------------------------------------------------------------------------------------------*\
Method: UpDate_SelGest1
	La UpDate_SelGest1 aggiorna sulla carta il Settore Sel_Gest1. 
	
	IN:	 - 
	OUT: -
\*----------------------------------------------------------------------------------------------*/
static uint16_t UpDate_SelGest1(bool CheckScrittura) {

	uint16_t	esitoWrite;
	
	MF_DataExch.SectNum = CardGestSelVars.SelGestSectors[0];								// Sel_Gest 1
	if ((CardGestSelVars.SelGestFlag[0] & FlagCredMask) == 0x00) {
		CardGestSelVars.SelGestFlag[0] = SetFlagCred;
		MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)] = CardGestSelVars.SelGestFlag[0];		// Flag blocco attivo in WrBuff
		Prepare_Gest_Sector(&MF_DataExch.WrBuff[1*MIF_BLOCKLEN], 0);						// Dati nel Blocco 1
		MF_DataExch.BlockNum[0] = NoBlock;
		MF_DataExch.BlockNum[1] = Block1;
	} else {
		CardGestSelVars.SelGestFlag[0] = ClrFlagCred;
		MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)] = CardGestSelVars.SelGestFlag[0];		// Flag blocco attivo in WrBuff
		Prepare_Gest_Sector(&MF_DataExch.WrBuff[0*MIF_BLOCKLEN], 0);						// Dati nel Blocco 0
		MF_DataExch.BlockNum[0] = Block0;
		MF_DataExch.BlockNum[1] = NoBlock;
	}
	esitoWrite = MifareWriteAndCompare((uint8_t*)&MF_DataExch.WrBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum, CheckScrittura);
	return esitoWrite;
}
#endif


/*---------------------------------------------------------------------------------------------*\
Method: ReadCreditSector
	La ReadCreditSector trasferisce il Settore Tipo Credito nella struttura "RBTCredSect" se
	il CRC dei blocchi letti e' corretto.
	Viene salvato il Settore del Credito carta in 
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
bool  ReadCreditSector(void) {

	SetKeyB(&KeyB[0]);																			// Predispongo l'uso della KeyB
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	if (!LeggiSettore()) return false;															// Errore di accesso o carta rimossa --> REJECT
	gOrionKeyVars.CreditSect = MF_DataExch.SectNum;												// Salvo settore credito di questa carta
	gOrionKeyVars.CreditFlag = MF_DataExch.RdBuff[(Block2 * MIF_BLOCKLEN)];
	if ((gOrionKeyVars.CreditFlag & FlagCredMask) == 0x00) {									// Valido il Credito del blocco 1
		if (!MIFBlock_To_CreditoCarta(&MF_DataExch.RdBuff[0*MIF_BLOCKLEN])) {
			response = Fail_CRC;
			return false;
		}
	} else {																					// Valido il Credito del blocco 2
		if (!MIFBlock_To_CreditoCarta(&MF_DataExch.RdBuff[1*MIF_BLOCKLEN])) {
			response = Fail_CRC;
			return false;
		}
	}
	return true;
}	
	
/*---------------------------------------------------------------------------------------------*\
Method: WrCredit
	La WrCredit trasferisce il Credito contenuto in gOrionKeyVars.Credit sulla Mifare, 
	controllando se deve essere scritto come Credito1 o Credito2.
	La WrCredit impiega circa 125 msec per scrivere e rileggere i due blocchi.
	
	IN:		- gOrionKeyVars.Credit, gOrionKeyVars.CreditFlag
			- Snum_media_in[] che contiene l'UID
			- la struct MF_DataExch con KeyA/KeyB per l'autenticazione e il valore della Key
	OUT:	Status = True se la scrittura del credito e' andata a buon fine, altrimenti False.
\*----------------------------------------------------------------------------------------------*/
bool WrCredit(void) {

	uint8_t	i;
	bool 	RdAfterWr;
	uint32_t	TestSecure;
	
#if	!SaltaSicurezza
	TestSecure = Manifacturer[8];																// Controllo Sicurezza carta
	TestSecure |= Manifacturer[9]<<8;
	TestSecure |= Manifacturer[10]<<16;
	TestSecure |= Manifacturer[11]<<24;
	if ( cryptmsg[0] != TestSecure ) return false;
#endif
	
	gOrionKeyVars.WR_ErrCode = Resp_OK;															// Clear Errore di scrittura 
	if (gOrionKeyVars.RTB_Card == 6916) {
		CalcKeyB(25965, 5961);																	// Calcola la chiave KeyB di accesso alla CartaRBT
	} else {
		CalcKeyB(Pin1, Pin2);																	// Calcola la chiave di accesso al Settore
	}
	RdAfterWr = true;
	

	
#if DefPARALLELO	
	if (gOrionConfVars.GestSel == 0) {
#endif
		if (gOrionKeyVars.KR_TipoChiave != KR_CHIAVE_CBLK) {
			RBTCredSect.Cred		= gOrionKeyVars.Credit;											// Copio il credito nella struttura RBTCredSect
			RBTCredSect.FreeCr		= gOrionKeyVars.FreeCredit;										// Copio il FreeCredit nella struttura RBTCredSect
			RBTCredSect.FreeVend	= gOrionKeyVars.FreeVend;										// Copio le FreeVend nella struttura RBTCredSect
			if (gOrionFreeCrVars.FreeCrPeriod == 0 || gOrionKeyVars.RestoreFreeCred) {				// FreeCredit NON abilitato o non utilizzabile: riscrivo il FreeCredit che
				RBTCredSect.FreeCr = BackupFreeCredit;												// la carta aveva all'entrata
			}
			if (gOrionFreeVendVars.FreeVendPeriod == 0 || gOrionKeyVars.RestoreFreeVend) {			// FreeVend NON abilitate o non utilizzabili: riscrivo le FreeVend che
				RBTCredSect.FreeVend = BackupFreeVend;												// la carta aveva all'entrata
			}
		}
		
		MF_DataExch.BlockNum[2] = Block2;															// Blocco con la flag Credit
		MF_DataExch.BlockNum[3] = NoBlock;															// Blocco da NON scrivere del settore SectNum
		MF_DataExch.SectNum = gOrionKeyVars.CreditSect;												// Set numero Settore da scrivere
		/* Predispongo "Erased" il blocco 2 che contiene la flag credito */
		for (i=0; i<MIF_BLOCKLEN; i++) {
			MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)+i] = 0xFF;
		}
		// Metto nell'array WrBuff due blocchi, quello del Credito e quello della CreditFlag:
		if ((gOrionKeyVars.CreditFlag & FlagCredMask) == 0x00) {
			gOrionKeyVars.CreditFlag = SetFlagCred;
			MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)] = gOrionKeyVars.CreditFlag;					// Flag Credito in WrBuff
			CreditoCarta_to_MIFBlock(&MF_DataExch.WrBuff[1*MIF_BLOCKLEN]);							// Credito in --> Credito 2
			MF_DataExch.BlockNum[0] = NoBlock;
			MF_DataExch.BlockNum[1] = Block1;														// Blocco contenente il Credito 2

		} else {
			gOrionKeyVars.CreditFlag = ClrFlagCred;
			MF_DataExch.WrBuff[(Block2 * MIF_BLOCKLEN)] = gOrionKeyVars.CreditFlag;					// Flag Credito in WrBuff
			CreditoCarta_to_MIFBlock(&MF_DataExch.WrBuff[0*MIF_BLOCKLEN]);							// Credito in --> Credito 1
			MF_DataExch.BlockNum[0] = Block0;														// Blocco contenente il Credito 1
			MF_DataExch.BlockNum[1] = NoBlock;														// Blocco da NON scrivere del settore SectNum
		}

		response = MifareWriteAndCompare((uint8_t*)&MF_DataExch.WrBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum, RdAfterWr);
		// if (response != Resp_OK) return false;														// Scrittura fallita 19.09.15 Non serve, c'e' gia' il return dopo
#if DefPARALLELO	
	} else {
		// *******  Gestione Selezioni   *******
		if (!gOrionKeyVars.UpdateGestSel) return true;												// Non ci sono altri settori da scrivere: esco con scrittura ok
		response = WrSect_SelGest();
	}
#endif	
	gOrionKeyVars.WR_ErrCode = response;															// Copio response in WR_ErrCode
	return (response == Resp_OK) ? true : false;
}

/*---------------------------------------------------------------------------------------------*\
Method: SetDataOdiernaFreeCredit
	Copia la data attuale nei bytes RBTCredSect che saranno scritti sulla carta insieme al 
	FreeCredit.

	IN:	  -
	OUT:  -	
\*----------------------------------------------------------------------------------------------*/
void  SetDataOdiernaFreeCredit(void) {
	
	RBTCredSect.DofWk_FreeCr  = (uint8_t)DataOra.DayOfWeek;  
	RBTCredSect.Giorno_FreeCr = (uint8_t)DataOra.Day;
	RBTCredSect.Mese_FreeCr	  = (uint8_t)DataOra.Month;
	RBTCredSect.Anno_FreeCr	  = (uint8_t) (DataOra.Year - MILLENNIO_2000);
}

/*---------------------------------------------------------------------------------------------*\
Method: SetDataOdiernaFreeVend
	Copia la data attuale nei bytes RBTCredSect che saranno scritti sulla carta insieme alle 
	FreeVend.

	IN:	  -
	OUT:  -	
\*----------------------------------------------------------------------------------------------*/
void  SetDataOdiernaFreeVend(void) {
	
	RBTCredSect.DofWk_FreeVend  = (uint8_t)DataOra.DayOfWeek;  
	RBTCredSect.Giorno_FreeVend = (uint8_t)DataOra.Day;
	RBTCredSect.Mese_FreeVend	= (uint8_t)DataOra.Month;
	RBTCredSect.Anno_FreeVend	= (uint8_t) (DataOra.Year - MILLENNIO_2000);
}

/*---------------------------------------------------------------------------------------------*\
Method: SuspendWriteNew_FC_FV
	Ripristina FreeCredit e/o FreeVend di entrata della carta poiche' devono essere accreditati
	alla selezione: se si lasciasse il valore invariato, essendo gia' stato aggiornato all'ingresso
	della carta, sarebbero accreditati anche senza effettuare la selezione, ad esempio con una
	ricarica CASH come in questo caso.
	Questa funzione e' chiamata da un Revalue.

	IN:	  -
	OUT:  -	true se ripristinato FreeCredit, altrimenti false (serve a rivisualizzare il credito
			totale aggiornato)
\*----------------------------------------------------------------------------------------------*/
bool  SuspendWriteNew_FC_FV(void) {
	
	if (FreeVendAllaSelez && gOrionKeyVars.WriteNewFreeVend) {									// FreeVend al momento della selezione ma questo e' un Revalue
		gOrionKeyVars.FreeVend = RBTCredSect.FreeVend;											// Ripristino FreeVend di entrata
	}
	if (FreeCredAllaSelez && gOrionKeyVars.WriteNewFreeCred) {									// FreeCredit al momento della selezione ma questo e' un Revalue
		gOrionKeyVars.FreeCredit = RBTCredSect.FreeCr;											// Ripristino FreeCredit di entrata
		return true;
	} else {
		return false;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: Test_FC_FV_AllaSelezione
	Predispone la scrittura del FreeCredit/FreeVend sulla carta e aggiorna la relativa Audit:
	questa funzione e' chiamata da una Vend Request. 

	IN:	  -
	OUT:  -	
\*----------------------------------------------------------------------------------------------*/
void  Test_FC_FV_AllaSelezione(void) {
	
	if (FreeCredAllaSelez && gOrionKeyVars.WriteNewFreeCred) {									// FreeCredit al momento della selezione
		gOrionKeyVars.FreeCredit = NewFC_Attribuito;											// Memorizzo nuovo FC
		SetDataOdiernaFreeCredit();																// Predispone la data nei bytes RBTCredSect
		PSHProcessingData();
		StatusFreeCredit = 1;																	// Trigger Audit Free Credit
		PSHCheckPowerDown();
	}
	if (FreeVendAllaSelez && gOrionKeyVars.WriteNewFreeVend) {									// FreeVend al momento della selezione
		gOrionKeyVars.FreeVend = NewFV_Attribuite;												// Memorizzo nuovo FV
		SetDataOdiernaFreeVend();																// Predispone la data nei bytes RBTCredSect
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: CheckMAD
	La CheckMAD verifica la presenza del MAD sulla Mifare: se lo trova recupera i settori che 
	contengono l'applicazione.
	Al MAD si accede leggendo il Settore Zero con KeyA = A0A1A2A3A4A5.

	IN:	  -
	OUT:  -	response != Resp_OK: errore accesso Mifare (Sect0 senza MAD oppure autenticazione fallita)
		  -	MADresp = MAD1_OK: array RBT_IndexSect[] che contiene i settori con le applicazione RBTech.
		  -	MADresp = NoMAD: Settore 0 letto regolarmente ma Mifare senza MAD.
		  -	MADresp = Err_Read: per errore lettura.
		  -	MADresp = MAD2:  Mifare con MAD tipo 2
		  -	MADresp = MAD_NoSectRBT: MAD1 ma senza applicazione RBT
\*----------------------------------------------------------------------------------------------*/
uint16_t	CheckMAD(void) {

	// Declaration of used parameters and structures //
	uint8_t i, n;
	
	MADresp = FirstMAD_Read;
	MF_DataExch.MAD = false;
	SetKeyA(&StdKeyA[0]);																		// Set KEYA standard per Autenticare il Settore 0 che contiene il MAD
	MF_DataExch.SectNum = 0;																	// Settore 0 che dovrebbe contenere il MAD
	MF_DataExch.BlockNum[0] = Trailer;															// Blocco da leggere del settore SectNum
	MF_DataExch.BlockNum[1] = NoBlock;															// Blocco da NON leggere del settore SectNum
	MF_DataExch.BlockNum[2] = NoBlock;															// Blocco da NON leggere del settore SectNum
	MF_DataExch.BlockNum[3] = NoBlock;															// Blocco da NON leggere del settore SectNum
	
	/* ==============================================================================================
		**********************   Lettura  Trailer  Settore 0  (MAD)   *******************************/

	response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
	if (response != Resp_OK) {
		// Read Fail. Cause: 1) KeyA errata 2) carta estratta 3) errore di lettura --> Ritento attivazione
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);			// Riattivazione fallita: REJECT CARD

		MADresp = SecondMAD_Read;
		response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
		if (response != Resp_OK) return (response);			// Lettura fallita: Settore 0 non accessibile
	}
	/* ============================================================================================== */ 
		// La presenza del MAD e' indicata dal bit7 del GPB (Byte N. 10 del Trailer del settore 0)

		GPB = MF_DataExch.RdBuff[GPB_POSITION];
		MF_DataExch.MAD = (GPB & MAD_Bit) ? true : false;

		if (MF_DataExch.MAD) {
			//*********   Mifare con MAD   *************
			MF_DataExch.MAD_V1 = (((GPB & Tipo_MAD_Mask) == Tipo_MAD1) ? true : false);		//Determino versione MAD
			if (MF_DataExch.MAD_V1) {

				//*******************   MAD1  Settori 1-0x0F ****************************
				// I 32 bytes del MAD1 sono in "MF_DataExch.RdBuff": leggo tutte le coppie 
				// di byte, scartando la prima che contiene il CRC8 e l'InfoByte, per 
				// cercare gli AID RBTech (Cluster 0x69 e ApplicationCode 0x38).
				MF_DataExch.BlockNum[0] = Block1;
				MF_DataExch.BlockNum[1] = Block2;
				MF_DataExch.BlockNum[2] = Block0;
				MF_DataExch.BlockNum[3] = NoBlock;
				response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
				if (response == Resp_OK) {
					// Azzero array settori RBTech
					for (i=0; i<RBT_SectInfoLen; i++)  {
						RBT_IndexSect[i] = 0x0;
					}
					for (i=0; i<MIF_BLOCKLEN; i++)  {
						Manifacturer[i] = MF_DataExch.RdBuff[32+i];				// Leggo Manifacturer Code
					}

					// ****  CRC8 dei blocchi MAD  ***********
					n = Calc_CRC8_MAD(&MF_DataExch.RdBuff[1], 31);				// Calcola CRC8 dei blocchi 1 e 2 del settore 0
					if (n != MF_DataExch.RdBuff[0]) {
						MADresp = NoMAD;										// Set come NoMAD
						MF_DataExch.MAD = false;
						return response;										// Esco e trattero' la carta come senza MAD
					}

					// Tralascio i primi 2 bytes (CRC8 e InfoByte)
					n = 0;
					for (i=2; i<MIF_BLOCKLEN*2; i=i+2)  {
						if ((MF_DataExch.RdBuff[i] == RBT_ApplicationCode) && (MF_DataExch.RdBuff[i+1] == RBT_ClusterCode)) {
							RBT_IndexSect[n] = i/2;								// Settore con Applicaz. RBT nel byte pari dell'array
							n = n + 2;
						}
					}
					if (n == 0) {
							MADresp = MAD1_NoSectRBT;							// MAD1 senza alcun AID RBTech
					} else {
							MADresp = MAD1_OK;									// MAD1 con Applicazione RBTech
					}
				} else {
						MADresp = MADReadErr;									// Errore di lettura del MAD
				}
			}  else	 {
					MADresp = MAD2;												// MAD2: Settori 0x01-0x27
			}
		} else  {
				MADresp = NoMAD;												// Letto GPB del settore 0 ma non ha il bit del MAD
		}

	return response;
		
}

/*--------------------------------------------------------------------------------------*\
Method: InizializzaCartaUtente
  La carta ha il MAD ma non c'e' il SectorInfo: leggo il Sector 2 con apposita KeyB e
  verifico se il GPB contiene 0x21 per le carte standard o 0x41 per le prepagate.
  Se e' una carta vergine RBT la inizializzo con Gestore, Locazione e KeyB della carta
  in tutti i settori utilizzati: aggiorno con le KeyB FreeSector le KeyB dei settori liberi.
  Per migliorare l'inizializzazione delle carte si tiene conto del fatto, nel limite del
  possibile, che una carta potrebbe essere allontanata prima del completamento di tutte
  le scritture necessarie.
  
Parameters:
	IN	-
	OUT: - response e MAD_SectInfo
\*--------------------------------------------------------------------------------------*/
uint16_t	InizializzaCartaUtente(void) {

	uint8_t	i;

	if (gOrionConfVars.NewUserCode) {
		ReadEEPI2C(IICEEPConfigAddr(EEUtente), (uint8_t *)(&Utente), sizeof(Utente));
	}
	CalcKeyB(Pin1, Pin2);																		// Calcola la chiave KeyB per eventualmente utilizzarla
	SetKeyB(&VergineRBTKeyB[0]);																// Set KEYB con il valore delle carte vergini RBT
	MF_DataExch.SectNum = InfoSectorNum;														// Settore che contiene l'InfoSector nelle Vergini RBT
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	
	// ******  Lettura Trailer Sector 2 ********
	if (!LeggiSettore()) return response;														// Errore di accesso o carta rimossa --> REJECT
	GPB = MF_DataExch.RdBuff[GPB_POSITION + (2*MIF_BLOCKLEN)];
	if (GPB != RBT_Vergine && GPB != Verg_Prep_InfoSct) return (response = No_Vergine);

	// ******  Aggiornamento Blocco 0 Sector 2 ********
	GreenLedCPU(OFF);																			// Led Verde CPU OFF
	RedLedCPU(ON);																				// Led Rosso CPU ON
	PN512_set_led (led_verde+led_rosso, !led_verde|led_rosso);									// Led verde OFF - Led Rosso ON
	MF_DataExch.RdBuff[0] = Gestore>>8;															// Copio Gestore e Locazione nel Blocco 1 dell'InfoSector
	MF_DataExch.RdBuff[1] = Gestore;
	MF_DataExch.RdBuff[2] = Locazione>>8;
	MF_DataExch.RdBuff[3] = Locazione;
	if (gOrionConfVars.NewUserCode) {
		MF_DataExch.RdBuff[4] = Utente>>24;														// Copio Codice Utente nel Blocco 1 dell'InfoSector
		MF_DataExch.RdBuff[5] = Utente>>16;
		MF_DataExch.RdBuff[6] = Utente>>8;
		MF_DataExch.RdBuff[7] = Utente;
	}
	SetCRC16Buff(&MF_DataExch.RdBuff[0], MIF_BLOCKLEN-2, 0);
	MF_DataExch.RdBuff[14] = gVars.CRC16;														// CRC16 LSB
	MF_DataExch.RdBuff[15] = (gVars.CRC16)>>8;													// CRC16 MSB
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	response = UpDateCard(false);																// Aggiorno Block 0
	if (response != Resp_OK) goto FailInitCard;													// Settore non accessibile con KeyB delle carte vergini

	// ******  Scrittura KeyB Settori Utilizzati ******
	MIFBlock_To_SectorInfo(&MF_DataExch.RdBuff[1*MIF_BLOCKLEN]);								// In RBT_IndexSect l'elenco dei settori utilizzati
	for (i=0; i<(2*MIF_SectUsed); i+=2) {														// Scrivo la KeyB nei settori utilizzati
		if (RBT_IndexSect[i+1] != 0 && RBT_IndexSect[i+1] != RBT_Secure) {
			if (!WriteKeyB_UsedSector(RBT_IndexSect[i])) {										// Scrivo KeyB nel Settore
				goto FailInitCard;																// Scrittura fallita: esco
			}
		} 
	}

	// ******  Scrittura KeyB Settori Free **********
	MF_DataExch.BlockNum[0] = Trailer;
	MF_DataExch.BlockNum[1] = NoBlock;															// Devo leggere il solo Trailer, gli altri blocchi non interessano
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	for (i=(MHDCreditSector+1); i<(MIFStdNumSect-1); i++) {										// Leggo in loop il trailer di tutti i settori
		SetKeyA(&KeyAReadMHD[0]);																// Uso la Read KEYA per leggere tutte le carte
		MF_DataExch.SectNum = i;																// Settore da leggere 
		if (!LeggiSettore()) {																	// Errore di lettura
			response = Retry_Card_Activation();
			if (!LeggiSettore()) goto FailInitCard;												// Carta Estratta o non piu' accessibile
			SetKeyB(&MF_DataExch.RdBuff[10]);													// Tento con KeyB FreeSector perche' il settore potrebbe essere gia' aggiornato
			if (!LeggiSettore()) goto FailInitCard;												// Carta Estratta o settore non piu' accessibile
		}
		GPB = MF_DataExch.RdBuff[GPB_POSITION];
		if (GPB == 0)  {
			SetKeyB(&VergineRBTKeyB[0]);														// Set KEYB con il valore delle carte vergini RBT
			MF_DataExch.RdBuff[0] 	= 0x2B;														// Ripristino KeyA di lettura (2B 67 56 CE 59 A6)
			MF_DataExch.RdBuff[1] 	= 0x67;
			MF_DataExch.RdBuff[2] 	= 0x56;
			MF_DataExch.RdBuff[3] 	= 0xCE;
			MF_DataExch.RdBuff[4] 	= 0x59;
			MF_DataExch.RdBuff[5] 	= 0xA6;
			HEXL = MF_DataExch.SectNum ^ 0xFF;													// Calcolo KeyB del settore libero
			MF_DataExch.RdBuff[10] 	= Snum_media_in[0] ^ HEXL;
			MF_DataExch.RdBuff[11] 	= Snum_media_in[1] ^ HEXL;
			MF_DataExch.RdBuff[12] 	= Snum_media_in[2] ^ HEXL;
			MF_DataExch.RdBuff[13] 	= Snum_media_in[3] ^ HEXL;
			MF_DataExch.RdBuff[14] 	= HEXL;
			MF_DataExch.RdBuff[15] 	= MF_DataExch.SectNum;
			HEXL = MF_DataExch.SectNum;															// Stato formattazione: riporta il settore sul quale si opera
			response = UpDateCard(false);														// Update Trailer
			if (response != Resp_OK) {
				response = Retry_Card_Activation();
				if (response != Resp_OK) goto FailInitCard;										// Carta Estratta o non piu' accessibile
				SetKeyB(&MF_DataExch.RdBuff[10]);												// Tento scrittura con KeyB FreeSector perche' potrebbe essere stata gia' aggiornata
				response = UpDateCard(false);													// ma senza aver terminato tutta l'inizializzazione
				if (response != Resp_OK) goto FailInitCard;										// Carta Estratta o settore non piu' accessibile
			}
		}
		
	}
	// ******  Scrittura KeyB Sector Info e relativo GPB  **********
	SetKeyB(&VergineRBTKeyB[0]);																// Set KEYB con il valore delle carte vergini RBT
	MF_DataExch.SectNum = InfoSectorNum;														// Sector Info
	if (MF_DataExch.SectorType == RBT_Vergine) {
		MF_DataExch.RdBuff[GPB_POSITION] = RBT_GenInfo;											// GPB = Sector Info per carta Utente std
		gOrionKeyVars.Prepag = false;
	} else {
		MF_DataExch.RdBuff[GPB_POSITION] = PrepagataInfoSct;									// GPB = Sector Info per carta Utente prepagata
		gOrionKeyVars.Prepag = true;
	}
	for (i=0; i<6; i++) {																		// KeyB del Gestore
		MF_DataExch.RdBuff[i+KeyB_POSITION] = KeyB[i];
	}
	response = UpDateCard(false);																// Update Trailer SectorInfo
	if (response != Resp_OK) goto FailInitCard;													// Settore non accessibile con KeyB delle carte vergini
	MAD_SectInfo = 2;
	RedLedCPU(OFF);																				// Led Rosso CPU OFF
	PN512_set_led (led_verde+led_rosso, !led_verde|!led_rosso);									// Led verde e Led Rosso Antenna OFF

	// Aggiorno Audit carte inizializzate e, se attivo, nuovo Codice Utente
	PSHProcessingData();
	if (gOrionConfVars.NewUserCode) {															// Se ho utilizzato il codice utente per questa carta lo incremento per la prossima
		Utente++;
		WriteEEPI2C(IICEEPConfigAddr(EEUtente), (uint8_t *)(&Utente), sizeof(Utente));
	}
   	AuditUlongInc(IICEEPAuditAddr(LR_EK_10_Event));    											// "EA2*EK_10" "Card Init"
   	AuditUlongInc(IICEEPAuditAddr(IN_EK_10_Event));
    PSHCheckPowerDown();
	return response;

FailInitCard:
	RedLedCPU(OFF);																				// Led Rosso CPU OFF
	GreenLedCPU(ON);																			// Led Verde CPU ON
	PN512_set_led (led_verde+led_rosso, !led_verde|!led_rosso);									// Led verde e Led Rosso Antenna OFF
	return (response = Format_Fail);
	
}

/*--------------------------------------------------------------------------------------*\
Method: WriteKeyB_UsedSector
	Scrivo la chiave KeyB di sistema nel trailer del Settore usato per l'applicazione.

Parameters:
	IN	- 
	OUT - false se scrittura fallita
\*--------------------------------------------------------------------------------------*/
static bool WriteKeyB_UsedSector(uint8_t NumSect) {
	
	uint8_t i;

	// ***  Inizio leggendo il Trailer  **** 
	SetKeyB(&VergineRBTKeyB[0]);																					// Set KEYB con il valore delle carte vergini RBT
	MF_DataExch.SectNum = NumSect; 
	MF_DataExch.BlockNum[0] = Trailer;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum);
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return false;																					// Carta Estratta o non piu' accessibile
			SetKeyB(&KeyB[0]);																									// Tento scrittura con KeyB carta perche' potrebbe essere stata gia' aggiornata
			response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum);	// ma senza aver terminato tutta l'inizializzazione
			if (response != Resp_OK) { 
				return false;																						// Carta Estratta o non piu' accessibile
			} else {
				return true;																						// KeyB gia' aggiornata
			}
	}
	MF_DataExch.RdBuff[0] 	= 0x2B;																					// Ripristino KeyA (2B 67 56 CE 59 A6)
	MF_DataExch.RdBuff[1] 	= 0x67;
	MF_DataExch.RdBuff[2] 	= 0x56;
	MF_DataExch.RdBuff[3] 	= 0xCE;
	MF_DataExch.RdBuff[4] 	= 0x59;
	MF_DataExch.RdBuff[5] 	= 0xA6;
	if (MF_DataExch.RdBuff[GPB_POSITION] != 0) {															// KeyB del Gestore solo se e' un Settore utilizzato
		for (i=0; i<6; i++) {
			MF_DataExch.RdBuff[i+KeyB_POSITION] = KeyB[i];
		}
		response = UpDateCard(false);																		// Update Trailer
		if (response != Resp_OK) return false;																// Settore non accessibile con KeyB delle carte vergini
	} else {
		return false;
	}
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method: UpDateCard
  Trasferisco RdBuff in WrBuff e lo scrivo sulla carta.

Parameters:
	IN	- RdBuff, MF_DataExch.SectNum e MF_DataExch.BlockNum gia' predisposti
	OUT - in response risultato dell'operazione
\*--------------------------------------------------------------------------------------*/
uint16_t	UpDateCard(bool ReadAfterWrite){
	uint8_t	i;
	
	for (i=0; i<MIF_BLOCKLEN; i++) {						// Copio RdBuff in WrBuff
		MF_DataExch.WrBuff[i] = MF_DataExch.RdBuff[i];
	}
	response = MifareWriteAndCompare((uint8_t*)&MF_DataExch.WrBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum, ReadAfterWrite);
	return response;
}

/*--------------------------------------------------------------------------------------*\
Method: SetKeyA
  Inserisco in "MF_DataExch.KeyVal" la KeyA per accedere alla Mifare 
Parameters:
	IN	- pointer a StdKeyA
\*--------------------------------------------------------------------------------------*/
void SetKeyA(uint8_t *ValoreKey_A) {

	uint8_t	i;
	
	for (i=0; i<6; i++) {
		MF_DataExch.KeyVal[i] = *(ValoreKey_A + i);
	}
	MF_DataExch.KeyType = false;  			//KeyA
}

/*--------------------------------------------------------------------------------------*\
Method: SetKeyB
  Inserisco in "MF_DataExch.KeyVal" la KeyB per accedere alla Mifare
Parameters:
	IN	- 
\*--------------------------------------------------------------------------------------*/
void SetKeyB(uint8_t *ValKey_B) {

	uint8_t	i;
	for (i=0; i<6; i++) {
		MF_DataExch.KeyVal[i] = ValKey_B[i];
	}
	MF_DataExch.KeyType = true; 			// Set KeyB
}

/*--------------------------------------------------------------------------------------*\
Method: CalcKey
  Calcolo la KeyB per l'accesso ai settori RBT
Parameters:
	IN	- Snum_media_in[4] (s/n della carta)
	OUT	- keyB[6]
\*--------------------------------------------------------------------------------------*/
void CalcKeyB(uint16_t val1, uint16_t val2) {

	// Determino prima coppia di bytes di KeyB
	Init_CRC(SemeKeyB_Part1);
	CRC32Inp = ((val1)<<16 | (Snum_media_in[0]<<8) | (Snum_media_in[1]));
	CRC16 = Calc_CRC_GetCRC32(CRC32Inp);
	KeyB[0] = (CRC16)>>8;
	KeyB[1] = (uint8_t) (CRC16);
	
	// Determino seconda coppia di bytes di KeyB
	Init_CRC(SemeKeyB_Part2);
	CRC32Inp = ((val2)<<16 | (Snum_media_in[2]<<8) | (Snum_media_in[3]));
	CRC16 = Calc_CRC_GetCRC32(CRC32Inp);
	KeyB[2] = (CRC16)>>8;
	KeyB[3] = (uint8_t) (CRC16);

	// Determino terza coppia di bytes di KeyB
	Init_CRC(SemeKeyB_Part3);
	CRC32Inp = ((val1)<<16 | (val2));
	CRC16 = Calc_CRC_GetCRC32(CRC32Inp);
	CRC32Inp = ( (Snum_media_in[0]<<24) | (Snum_media_in[1]<<16) | (Snum_media_in[2]<<8) | (Snum_media_in[3]) );
	CRC16 = Calc_CRC_GetCRC32(CRC32Inp);
	KeyB[4] = (CRC16)>>8;
	KeyB[5] = (uint8_t) (CRC16);
	
}

/*	**************************************************************************************
	Trasferisce 16 bytes puntati da rdbuffPnt nella struct "RBT_CodiciCarta" solo se il
	CRC16 e' corretto
	************************************************************************************* */

bool  MIFBlock_To_CodiciCarta(uint8_t *rdbuffPnt) {

	uint8_t	i;
	
	Seed = SemeCRC_Std;
	if (!Check_CRC_MifareBlock(rdbuffPnt, Seed)) return false;
	RBT_CodiciCarta.Gestore 	= ((*(rdbuffPnt+0) << 8) + *(rdbuffPnt+1));
	RBT_CodiciCarta.Locazione 	= ((*(rdbuffPnt+2) << 8) + *(rdbuffPnt+3));
	RBT_CodiciCarta.Utente 		= ((*(rdbuffPnt+4) << 24) + ((*(rdbuffPnt+5)) << 16) + ((*(rdbuffPnt+6)) << 8) + (*(rdbuffPnt+7)));
	RBT_CodiciCarta.TipoCarta	= *(rdbuffPnt+8);
	RBT_CodiciCarta.Livelli 	= ((*(rdbuffPnt+9) << 8) + *(rdbuffPnt+10));
	for (i=0; i<3;i++) {
		RBT_CodiciCarta.CodCartaBytesVuoti[i] = (uint8_t) *(rdbuffPnt+(9+i));
	}
	RBT_CodiciCarta.Chks 		= ((*(rdbuffPnt+14) << 8) + *(rdbuffPnt+15));
	return true;
}		

/*	**************************************************************************************
	Trasferisce 16 bytes puntati da rdbuffPnt nell'array "RBT_IndexSect" solo se il
	CRC16 e' corretto
	************************************************************************************* */

bool  MIFBlock_To_SectorInfo(uint8_t *rdbuffPnt) {

	uint8_t	i;
	
	Seed = SemeCRC_Std;
	if (!Check_CRC_MifareBlock(rdbuffPnt, Seed)) return false;
	for (i=0; i<(MIF_BLOCKLEN-2);i++) {
		RBT_IndexSect[i] = *(rdbuffPnt+i);							// Trasferisco Indici nell'array RBT_IndexSect
	}
	RBT_CodiciCarta.Chks 		= ((*(rdbuffPnt+14) << 8) + *(rdbuffPnt+15));
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method: MIFBlock_To_CreditoCarta
	Trasferisce 16 bytes puntati da rdbuffPnt nella struct "RBTCredSect" solo se il
	CRC16 e' corretto.
	Questa struttura e' utilizzata anche dalle carte di Caricamento a Blocchi.

	IN	- MF_DataExch.RdBuff
	OUT	- true se CRC ok altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  MIFBlock_To_CreditoCarta(uint8_t *rdbuffPnt) {

	Seed = SemeCRC_Std;
	if (!Check_CRC_MifareBlock(rdbuffPnt, Seed)) return false;
	RBTCredSect.Cred 			= (*(rdbuffPnt) + (*(rdbuffPnt+1) << 8));
	RBTCredSect.BLK 			= *(rdbuffPnt+2);
	RBTCredSect.DofWk_FreeCr	= *(rdbuffPnt+3);
	RBTCredSect.Giorno_FreeCr	= *(rdbuffPnt+4);
	RBTCredSect.Mese_FreeCr		= *(rdbuffPnt+5);
	RBTCredSect.Anno_FreeCr		= *(rdbuffPnt+6);
	RBTCredSect.FreeCr 			= (*(rdbuffPnt+7) + (*(rdbuffPnt+8) << 8));
	RBTCredSect.DofWk_FreeVend	= *(rdbuffPnt+9);
	RBTCredSect.Giorno_FreeVend	= *(rdbuffPnt+10);
	RBTCredSect.Mese_FreeVend	= *(rdbuffPnt+11);
	RBTCredSect.Anno_FreeVend	= *(rdbuffPnt+12);
	RBTCredSect.FreeVend		= *(rdbuffPnt+13);
	RBTCredSect.Chks 			= ((*(rdbuffPnt+14) << 8) + *(rdbuffPnt+15));
	return true;
}		

/*--------------------------------------------------------------------------------------*\
Method: CreditoCarta_to_MIFBlock
	Trasferisce la struct "RBTCredSect" nei 16 bytes puntati da wrbuffPnt e calcola
	il CRC16.

	IN	- MF_DataExch.WrBuff
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  CreditoCarta_to_MIFBlock(uint8_t *wrbuffPnt) {

	uint8_t	i;
	
	for (i=0; i<MIF_BLOCKLEN; i++) {				// Azzero Write Buffer
		*(wrbuffPnt+i) = 0x00;
	}
	*(wrbuffPnt+0) = RBTCredSect.Cred;				// Credit MSB
	*(wrbuffPnt+1) = (RBTCredSect.Cred)>>8;			// Credit LSB
	*(wrbuffPnt+2) = RBTCredSect.BLK;
	*(wrbuffPnt+3) = RBTCredSect.DofWk_FreeCr;
	*(wrbuffPnt+4) = RBTCredSect.Giorno_FreeCr;
	*(wrbuffPnt+5) = RBTCredSect.Mese_FreeCr;
	*(wrbuffPnt+6) = RBTCredSect.Anno_FreeCr;
	*(wrbuffPnt+7) = RBTCredSect.FreeCr;			// FreeCred MSB
	*(wrbuffPnt+8) = (RBTCredSect.FreeCr)>>8;		// FreeCred LSB
	*(wrbuffPnt+9) = RBTCredSect.DofWk_FreeVend;
	*(wrbuffPnt+10) = RBTCredSect.Giorno_FreeVend;
	*(wrbuffPnt+11) = RBTCredSect.Mese_FreeVend;
	*(wrbuffPnt+12) = RBTCredSect.Anno_FreeVend;
	*(wrbuffPnt+13) = RBTCredSect.FreeVend;			// FreeVend
	Seed = SemeCRC_Std;
	SetCRC16Buff(wrbuffPnt, MIF_BLOCKLEN-2, Seed);
	*(wrbuffPnt+14) = gVars.CRC16;					// CRC16 LSB
	*(wrbuffPnt+15) = (gVars.CRC16)>>8;				// CRC16 MSB
}		

/*--------------------------------------------------------------------------------------*\
Method: SetCRC16Buff
  Calcola CRC16 di un buffer 
Parameters:
	IN	- buffer, lunghezza del buffer e seme del CRC16
	OUT	- (uint16_t)gVars.CRC16
\*--------------------------------------------------------------------------------------*/
void SetCRC16Buff(uint8_t *destbuff, uint8_t destbufflen, uint16_t Seme) {

	uint8_t	i;
	// Inizializzo CRC con Seed ricevuto
	Init_CRC(Seme);

	// Calcolo CRC16 del buffer puntato da destbuff
	for (i=0; i<destbufflen; i++) {
		HEXH = *(destbuff+i);
		CRC16 = Calc_CRC_GetCRC8(HEXH);
	}
	gVars.CRC16 = CRC16;
}


/*	**************************************************************************************
	*************  Verifica CRC16  Blocco ricevuto dalla Mifare    ***********************
	************************************************************************************** */

bool  Check_CRC_MifareBlock(uint8_t *rdbuffPnt, uint16_t Seme) {

	uint8_t	i;
	// Inizializzo CRC con Seed ricevuto
	Init_CRC(Seme);

/*
	// Controllo CRC16 del buffer ricevuto
	//MIFbuff = &MF_DataExch.RdBuff[0];							// Pointer buffer dati ricevuto dalla MifareRead
	for (i=0;i<(MIF_BLOCKLEN);i++) {
		HEXH = *(rdbuffPnt+i);
		gVars.CRC16 = Calc_CRC_GetCRC8(HEXH);
	}
	if (gVars.CRC16!=0) return false;
*/

	/* ============================================================================================== */ 
	/*  Utilizzo la variabile (uint16_t) CRC16 per conoscere il calcolo del 
		CRC sui 14 bytes del buffer: mi serve per programmare manualmente le carte.
		Il CRC calcolato deve essere scritto invertito sulla carta.
		Verifico correttezza CRC16 del buffer ricevuto   */
		
	for (i=0;i<(MIF_BLOCKLEN-2);i++) {
		HEXH = *(rdbuffPnt+i);
		gVars.CRC16 = Calc_CRC_GetCRC8(HEXH);
	}
	// Mettere un break qui per leggere il CRC16 dei 14 bytes dati
	CRC16 = gVars.CRC16;
	gVars.CRC16 = Calc_CRC_GetCRC8(*(rdbuffPnt+i));
	gVars.CRC16 = Calc_CRC_GetCRC8(*(rdbuffPnt+i+1));
	if (gVars.CRC16 == 0) {
		return true;
	} else {
		return false;
	}
}

/*	**************************************************************************************
//	****************    RIATTIVAZIONE MIFARE         *************************************
//	************************************************************************************** */

uint16_t	Retry_Card_Activation(void) {		

	if (!MifareReaderRequest()) 											return (response = Err_Read);
	if (!MifareReaderAnticoll(Snum_media_chk)) 								return (response = Err_Read);
	if (!CompareBuffer(Snum_media_chk, Snum_media_in, UID_MEDIA_BUF_LEN))	return (response = NoSameCard);
	return (response = Resp_OK);
}

/*	**************************************************************************************
//	****************    GESTIONE   REFUND            *************************************
//	************************************************************************************** */

/*---------------------------------------------------------------------------------------------*\
Method: SaveRefund
	Cerco un spazio libero nella struttura "Refund" in RAM per aggiungere i nuovi dati, poi
	copio tutta la struttura in EEPROM.
	Esce controllando il PowerDown solo se richiesto.
	Se la struttura di destinazione e' piena si cancella il dato piu' vecchio: se si tratta
	di credito carta lo si memorizza in Overpay Card (xx) oltre agli ID addebitato alle carte.
	*******  02.07.15  ANCORA  DA FARE  L'AZZERAMENTO DEL DATO PIU' VECCHIO  ********
 
	IN:		- Address struttura sorgente, tipo di refund e valore di refund da memorizzare
	OUT:	- True se registrati dati in EE, false se non c'e' piu' spazio per registrarli.
\*----------------------------------------------------------------------------------------------*/
bool  SaveRefund(REFUNDS* DestRefund, DATEREFUNDS* DestDateRefund, TipiRefund TipoRefund, CreditCpcValue RefundValue, bool TestPWD) {

	bool			FoundSpace;
	uint8_t			i, TabIndex;
	uint16_t			EEP_Address;
	uint32_t			TimeStamp;
	REFUNDS* 		ptRefunds;
	DATEREFUNDS* 	ptDataRefunds;
	
	ptRefunds = DestRefund;
	ptDataRefunds = DestDateRefund;
	FoundSpace = false;
	
	for (i=0; i<MaxRefund; i++) {																// Cerco uno spazio libero da crediti salvati
		if (ptRefunds->RefundVal[i] == 0) {
			FoundSpace = true;																	// Trovato uno spazio libero e [TabIndex] ne e' l'indice
			TabIndex = i;
			break;
		}
	}
	if (FoundSpace == false) {																	// Non ci sono piu' spazi liberi: cerco la data con valore piu' basso
		TabIndex = 0;																			// e utilizzo il suo indice per memorizzare i nuovi dati
		TimeStamp = ptDataRefunds->DateRefund[TabIndex];										// Leggo la prima data in TimeStamp
		for (i=1; i<MaxRefund; i++) {
			if (ptDataRefunds->DateRefund[i] < TimeStamp) {										// Data piu' vecchia di quella in TimeStamp
				TimeStamp = ptDataRefunds->DateRefund[i];										// Nuova data in TimeStamp
				TabIndex = i;																	// Indice alla data piu' vecchia
			}
		}
		if (TipoRefund == TipoRefundCredito) {
			PSHProcessingData();
			ValoreReinstate = ptRefunds->RefundVal[TabIndex];									// In ValoreReinstate il credito da memorizzare in Overpay Cashless DA9
			Overpay(OVERPAY_KR);
		}
	}
	// Memo in RAM il nuovo Refund con il suo UID e CRC
	ptRefunds->UID[TabIndex]		= Snum_media_in[0]<<24 | Snum_media_in[1]<<16 | Snum_media_in[2]<<8 | Snum_media_in[3];
	ptRefunds->RefundVal[TabIndex]	= RefundValue;

	Init_CRC(SeedZero);									
	CRC32Inp = Snum_media_in[0]<<24 | Snum_media_in[1]<<16 | Snum_media_in[2]<<8 | Snum_media_in[3];
	CRC16 = Calc_CRC_GetCRC32(CRC32Inp);
	CRC16Inp = RefundValue<<16 | RefundValue;
	CRC16 = Calc_CRC_GetCRC16(CRC16Inp);
	ptRefunds->CRC16Refund[TabIndex]= CRC16;
	if (ptRefunds->RefundCnt < MaxRefund) ptRefunds->RefundCnt ++;								// Se non e' gia' al massimo, incremento contatore crediti refund
	ptDataRefunds->DateRefund[TabIndex] = RTC_Counter;											// Memorizzo TimeStamp attuale
	EEP_Address = BeginRefundData + (TipoRefund * RefundSpace);									// Address struttura Refund da scrivere
	PSHProcessingData();
	WriteEEPI2C(EEP_Address, (uint8_t*)&DestRefund, RefundSpace);											// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
	EEP_Address = BeginRefundDatetime + (TipoRefund * DateRefundSpace);							// Address struttura DataRefund da scrivere
	WriteEEPI2C(EEP_Address, (uint8_t*)&DestDateRefund, DateRefundSpace);									// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
	if (TestPWD) {
		PSHCheckPowerDown();
	}
	return false;																				// Memoria piena: non ci stanno altri refunds
}
	
/*---------------------------------------------------------------------------------------------*\
Method: ReadRefundValues
	Leggo dalla EEPROM i dati di refund richiesti e, se il CRC e' corretto, li salvo nella 
	struttura di destinazione (Credit o FreeCredit o FreeVend).
	Nella struttura DATEREFUNDS salvo la data del salvataggio del Refund per poter cancellare
	la registrazione piu' vecchia e liberare spazio per nuovi refund.
 
	IN:		- Address struttura di destinazione e tipo di refund
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
bool ReadRefundValues(REFUNDS* DestRefund, DATEREFUNDS* DestDateRefund, TipiRefund TipoRefund) {

	uint8_t	i;
	uint16_t	EEP_Address;
	REFUNDS* 	 ptRefunds;
	DATEREFUNDS* ptDataRefunds;
	
	ptRefunds = DestRefund;
	ptDataRefunds = DestDateRefund;
	EEP_Address = BeginRefundData + (TipoRefund * RefundSpace);									// Address struttura Refund da leggere
	if (!ReadEEPI2C(EEP_Address, (uint8_t*)&DestRefund, RefundSpace)) return false;
	EEP_Address = BeginRefundDatetime + (TipoRefund * DateRefundSpace);							// Address struttura DataRefund da leggere
	if (!ReadEEPI2C(EEP_Address, (uint8_t*)&DestDateRefund, DateRefundSpace)) return false;
	ptRefunds->RefundCnt = 0;																	// Azzero il contatore dei refund presenti: lo aggiornero' nel loop di controllo

	// Controllo CRC16 dei dati di refund letti ed eventuale setting della flag presenza Refund
	for (i=0; i<MaxRefund; i++) {
		Init_CRC(SeedZero);
		CRC16 = Calc_CRC_GetCRC32(ptRefunds->UID[i]);
		CRC16 = Calc_CRC_GetCRC16(ptRefunds->RefundVal[i]);
		if (CRC16 != ptRefunds->CRC16Refund[i] || (ptRefunds->RefundVal[i] > 0 && ptRefunds->UID[i] == 0)) {
			ptRefunds->UID[i]			 = 0;													// Leggo zero se CRC non corretto oppure c'e' un valore di credito senza UID carta
			ptRefunds->RefundVal[i]		 = 0;
			ptRefunds->CRC16Refund[i]	 = 0;
			ptDataRefunds->DateRefund[i] = 0;
		}
		if (ptRefunds->RefundVal[i] != 0) {
			ptRefunds->RefundCnt ++;
		}
	}
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: GiveRefund
	Se c'e' un valore da refund per la Mifare in entrata, la aggiorno azzerando poi i valori
	in RAM ed EEPROM.
 
	IN:		- 
	OUT:	- true se refund effettuato o non c'erano refund, false se scrittura refund fallita
\*----------------------------------------------------------------------------------------------*/
bool GiveRefund(void)
{
	uint8_t	CredPnt, FCPnt, FVPnt, CBPnt;									// **** Loop pointers lasciati alla posizione nella quale c'e' il valore da rendere ******
	uint8_t	CntRef;
	uint16_t	EEP_Address;
	uint32_t	TempSnum;
	
	TempSnum = Snum_media_in[0]<<24 | Snum_media_in[1]<<16 | Snum_media_in[2]<<8 | Snum_media_in[3];
	CntRef = 0;																					// Flag refund trovati per la carta in entrata

	if (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_CBLK)
	{
		// Verifico se c'e' un Cash Caricamento a Blocchi Refund per la CB in entrata:
		// se c'e' e valore blocco uguale al valore memorizzato, incremento il contatore
		// di blocchi ed esco. Se il valore del blocco e' diverso lo azzero in Overpay
		// aggiornando anche l'evento "EK_04 Blocks Charged"
		for (CBPnt=0; CBPnt<MaxRefund; CBPnt++) {
			if (RefundCashCB.UID[CBPnt] == TempSnum) {											// E' rientrata la stessa Mifare dalla quale ho tolto un blocco salvato in EEPROM
				if (RefundCashCB.RefundVal[CBPnt] == ValoreBlocco) {							// Se la chiave ha il blocco con lo stesso valore di quando e' stato salvato in refund lo
					NumeroBlocchi ++;															// riaccredito, altrimenti Overpay ("EA2*EK_04")
				} else  {
					Overpay(OVERPAY_CARICAMENTO_BLOCCHI);										// Cash Caricamento a Blocchi-->Overpay ("EA2*EK_04 Blocks Charged" ed "EA2*EK_09 BlkChrgdOverp.")
				}
				RefundCashCB.UID[CBPnt] = 0;													// Azzero i dati della tabella in EEPROM
				RefundCashCB.RefundVal[CBPnt] = 0;
				RefundCashCB.CRC16Refund[CBPnt] = 0;
				DataRefundCashCB.DateRefund[CBPnt] = 0;											// Azzero data della memorizzazione che sto annullando
				if (RefundCashCB.RefundCnt) RefundCashCB.RefundCnt --;							// Decremento counter elementi della tabella RefundCashCB
				PSHProcessingData();
				EEP_Address = BeginRefundData + (TipoRefundCaricamBlk * RefundSpace);			// Address EEPROM struttura RefundCashCB da scrivere
				WriteEEPI2C(EEP_Address, (uint8_t*)&RefundCashCB, RefundSpace);					// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
				EEP_Address = BeginRefundDatetime + (TipoRefundCaricamBlk * DateRefundSpace);	// Address EEPROM struttura DataRefundCashCB da scrivere
				WriteEEPI2C(EEP_Address, (uint8_t*)&DataRefundCashCB, DateRefundSpace);			// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
				PSHCheckPowerDown();
				break;
			}	
		}
	} 
	else
	{
		// Verifico se c'e' un Credito Refund
		if (RefundValues.RefundCnt != 0) {
			for (CredPnt=0; CredPnt<MaxRefund; CredPnt++) {
				if (RefundValues.UID[CredPnt] == TempSnum) {									// E' per la stessa Mifare in entrata
					gOrionKeyVars.Credit += RefundValues.RefundVal[CredPnt];					// Aggiungo il credito Refund al credito attuale
					CntRef |= CreditReso;														// Set flag Credito reso
					break;
				}
			}
		}

		// Verifico se c'e' un FreeCredit Refund
		if (RefundFreeCredit.RefundCnt != 0) {
			for (FCPnt=0; FCPnt<MaxRefund; FCPnt++) {
				if (RefundFreeCredit.UID[FCPnt] == TempSnum) {									// E' per la stessa Mifare in entrata
					gOrionKeyVars.FreeCredit += RefundFreeCredit.RefundVal[FCPnt];				// Aggiungo il FreeCredit Refund al FreeCredit attuale
					CntRef |= FreeCreditReso;													// Set flag FreeCredit reso
					break;
				}	
			}
		}
		
		// Verifico se c'e' un FreeVend Refund
		if (RefundFreeVend.RefundCnt != 0) {
			for (FVPnt=0; FVPnt<MaxRefund; FVPnt++) {
				if (RefundFreeVend.UID[FVPnt] == TempSnum) {									// E' per la stessa Mifare in entrata
					gOrionKeyVars.FreeVend += RefundFreeVend.RefundVal[FVPnt];					// Aggiungo le FreeVend Refund alle FreeVend attuali
					CntRef |= FreeVendReso;														// Set flag FreeVend reso
					break;
				}	
			}
		}
	}
	if (CntRef == 0) return true;																// Non ci sono Refund per la carta in entrata
	
	// Aggiorno settore del credito con tutti i valori aggiornati
	PN512_set_led (led_verde+led_rosso, !led_verde|led_rosso);									// Led verde OFF - Led Rosso ON
	WrCredit();
	PN512_set_led (led_verde+led_rosso, !led_verde|!led_rosso);									// Led verde OFF - Led Rosso OFF
	if (FaseWrite >= FaseToutWRFlag)															// Azzero Refunds perche' avevo gia' cominciato a scrivere la flag del credito
	{
	  RevalueKR(REFUND_DELAYED_KR, 0x00, true);													// Ext Audit per Card Refund Delayed
		if (CntRef & CreditReso) {
			RefundValues.UID[CredPnt] = 0;
			RefundValues.RefundVal[CredPnt] = 0;
			RefundValues.CRC16Refund[CredPnt] = 0;
			DataRefundValues.DateRefund[CredPnt] = 0;											// Azzero data della memorizzazione che sto annullando
			if (RefundValues.RefundCnt) RefundValues.RefundCnt --;								// Decremento counter elementi della tabella RefundValues
			PSHProcessingData();
			EEP_Address = BeginRefundData + (TipoRefundCredito * RefundSpace);					// Address EEPROM struttura Refund da scrivere
			WriteEEPI2C(EEP_Address, (uint8_t*)&RefundValues, RefundSpace);						// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
			EEP_Address = BeginRefundDatetime + (TipoRefundCredito * DateRefundSpace);			// Address EEPROM struttura DataRefundValues da scrivere
			WriteEEPI2C(EEP_Address, (uint8_t*)&DataRefundValues, DateRefundSpace);				// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
			PSHCheckPowerDown();
		}
		if (CntRef & FreeCreditReso) {
			RefundFreeCredit.UID[FCPnt] = 0;
			RefundFreeCredit.RefundVal[FCPnt] = 0;
			RefundFreeCredit.CRC16Refund[FCPnt] = 0;
			DataRefundFC.DateRefund[FCPnt] = 0;													// Azzero data della memorizzazione che sto annullando
			if (RefundFreeCredit.RefundCnt) RefundFreeCredit.RefundCnt --;						// Decremento counter elementi della tabella RefundFreeCredit
			PSHProcessingData();
			EEP_Address = BeginRefundData + (TipoRefundFreeCred * RefundSpace);					// Address EEPROM struttura Refund da scrivere
			WriteEEPI2C(EEP_Address, (uint8_t*)&RefundFreeCredit, RefundSpace);					// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
			EEP_Address = BeginRefundDatetime + (TipoRefundFreeCred * DateRefundSpace);			// Address EEPROM struttura DataRefundFC da scrivere
			WriteEEPI2C(EEP_Address, (uint8_t*)&DataRefundFC, DateRefundSpace);					// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
			PSHCheckPowerDown();
		}
		if (CntRef & FreeVendReso) {
			RefundFreeVend.UID[FVPnt] = 0;
			RefundFreeVend.RefundVal[FVPnt] = 0;
			RefundFreeVend.CRC16Refund[FVPnt] = 0;
			DataRefundFV.DateRefund[FVPnt] = 0;													// Azzero data della memorizzazione che sto annullando
			if (RefundFreeVend.RefundCnt) RefundFreeVend.RefundCnt --;							// Decremento counter elementi della tabella RefundFreeVend
			PSHProcessingData();
			EEP_Address = BeginRefundData + (TipoRefundFreeVend * RefundSpace);					// Address EEPROM struttura Refund da scrivere
			WriteEEPI2C(EEP_Address, (uint8_t*)&RefundFreeVend, RefundSpace);					// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
			EEP_Address = BeginRefundDatetime + (TipoRefundFreeCred * DateRefundSpace);			// Address EEPROM struttura DataRefundFV da scrivere
			WriteEEPI2C(EEP_Address, (uint8_t*)&DataRefundFV, DateRefundSpace);					// Circa 1.3 msec per Tx EE_Buff e circa 4 msec write: Totale circa 6 msec.
			PSHCheckPowerDown();
		}
		return true;
	} 
	else
	{
		return false;
	}
	//return true;																				// Nessun credito Refund per questa carta
}

/*------------------------------------------------------------------------------------------*\
Method: Calc_CRC8_MAD
  Calcola il CRC8 del MAD di una MIFARE.
  IN:  - pointer al buffer MF_DataExch.RdBuff del quale calcola il CRC8 partendo dal
		 secondo byte (il primo e' il CRC8 della Mifare).
		 Il polinomio e' x8 + x4 + x3 + x2 + 1 (0x1d) e il seme e' 0xE3 (ruotato e' 0xC7).
\*------------------------------------------------------------------------------------------*/
uint8_t  Calc_CRC8_MAD(uint8_t *start, uint8_t length) {
	uint8_t car, k;

	CRC8=0xC7;
	while( length-- ) {
		car = *start++;
		CRC8 = CRC8 ^ car;
		for(k=0; k<8; k++) {
			if( CRC8 & 0x80 ) {
				CRC8 = (CRC8 << 1) ^ 0x1d;
			} else {
				CRC8 = CRC8 << 1;
			}
		}
  }
	return CRC8;
} 

/*------------------------------------------------------------------------------------------*\
Method: decipher
  Decifra un messaggio di 64 bit (8 Bytes) con la chiave k di 128 bits (16 Bytes).
  La funzione dura circa 22,5 uSec @ 50MHz.
  IN:  - pointer al messaggio da decifrare e pointer alla chiave di cifratura (32 bits)
 OUT:  - messaggio in chiaro
\*------------------------------------------------------------------------------------------*/
void decipher(uint32_t* cryptmsg, const uint32_t* k)
{
    uint8_t num_rounds = 32;
	uint32_t v0=cryptmsg[0], v1=cryptmsg[1], i;
    uint32_t delta=0x9E3779B9, sum=delta*num_rounds;

    for(i=0; i<num_rounds; i++) {
        v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + k[(sum>>11) & 3]);
        sum -= delta;
        v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + k[sum & 3]);
    }
    cryptmsg[0]=v0; cryptmsg[1]=v1;
}

/*---------------------------------------------------------------------------------------------*\
Method: CheckSicurezza
	Controlla che la carta sia emessa da noi.
 
	IN:		- 
	OUT:	- Response = Resp_OK se la scrittura di tutti i settori e' OK
\*----------------------------------------------------------------------------------------------*/
#if	!SaltaSicurezza

bool  CheckSicurezza(void)
{
	uint8_t	r;

	// Decrypto i primi 8 bytes del Blocco 0 sel settore sicurezza
	cryptmsg[0] = Secure0;
	cryptmsg[1] = Secure1;
	decipher(cryptmsg, k);
	cryptuid = cryptmsg[0];
	MF_DataExch.WrBuff[0] = cryptmsg[0];
	MF_DataExch.WrBuff[1] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[2] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[3] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[4] = cryptmsg[1];
	MF_DataExch.WrBuff[5] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[6] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[7] = cryptmsg[1]>>24;
	for (r=0; r<8; r++) {
		if (MF_DataExch.WrBuff[r] != Manifacturer[r]) return false;
	}
	// Decrypto i secondi 8 bytes del Blocco 0 sel settore sicurezza
	cryptmsg[0] = Secure2;
	cryptmsg[1] = Secure3;
	decipher(cryptmsg, k);
	
	// XOR con i primi 8 bytes criptati
	cryptmsg[0] ^= Secure0;
	cryptmsg[1] ^= Secure1;

	MF_DataExch.WrBuff[0] = cryptmsg[0];
	MF_DataExch.WrBuff[1] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[2] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[3] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[4] = cryptmsg[1];
	MF_DataExch.WrBuff[5] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[6] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[7] = cryptmsg[1]>>24;
	for (r=0; r<8; r++) {
		if (MF_DataExch.WrBuff[r] != Manifacturer[8+r]) return false;
	}
	return true;
}
#endif

/*---------------------------------------------------------------------------------------------*\
Method: ClrCardFlags
	Resetta le varie flags utilizzate nella gestione delle carte.
	***** NON azzerare la struttura con la data delle FreeVend  *****
	***** perche' utilizzata dal PK3-PAR anche senza la carta   *****
	***** nel lettore (Pasvens)                                 *****
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  ClrCardFlags(void) {
	uint8_t  r;
	
	gOrionKeyVars.KR_TipoChiave 	= KR_CHIAVE_ASSENTE;
	gOrionKeyVars.CodiceUtente 		= 0;
	gOrionKeyVars.LivelliChiave 	= 0;
	gOrionKeyVars.RTB_Card			= 0;
	gOrionKeyVars.CreditSect		= 0;
	gOrionKeyVars.RFFieldFree 		= false;
	gOrionKeyVars.Inserted			= false;
	gOrionKeyVars.CardReadErr 		= false;					// MR Aggiunto il 05.02.2014
	gOrionKeyVars.FirstAccess 		= false;					// MR Aggiunto il 05.02.2014
	gOrionKeyVars.CreditWrite 		= false;					// MR Aggiunto il 05.02.2014
	gOrionKeyVars.CreditWriteEnd	= false;					// MR Aggiunto il 05.02.2014
	gOrionKeyVars.CheckSecure1 		= false;
	gOrionKeyVars.CheckSecure2		= false;
	gOrionKeyVars.Prepag			= false;
	gOrionKeyVars.WriteNewFreeCred	= false;
	gOrionKeyVars.RestoreFreeCred	= false;
	gOrionKeyVars.RestoreFreeVend	= false;
	BackupFreeCredit 				= 0;
	BackupFreeVend					= 0;
	cryptmsg[0] = NULL;
	cryptmsg[1] = NULL;
	for (r=0; r<MIF_BLOCKLEN; r++) {
		Manifacturer[r] = NULL;
	}
#if DefPARALLELO
	if (gOrionConfVars.GestSel) {
		memset(&CardGestSelVars, 0, sizeof(CardGestSelVars));
	}
#endif	
}

/*---------------------------------------------------------------------------------------------*\
Method: MasterCard
	Controlla se ci sono le condizioni per accettare la Master Card ed eventualmente
	riprogramma i nuovi codici.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
uint16_t  MasterCard(void)
{
	uint8_t	r;

	response = NoNewCodesSect;													// Predispongo errore mancanza settore NuoviCodici
	if (Gestore != 0 || Pin1 != 0 || Pin2 != 0) return (response);				// Master Card accettata solo se Pin1, Pin2 e GEST = 0						
	
	MF_DataExch.SectNum = InfoSectMasterCard;									// Settore NuoviCodici
	CalcKeyB(0x0, 0x0);															// Calcola la chiave KeyB di accesso alla MasterCard
	SetKeyB(&KeyB[0]);
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	if (!LeggiSettore()) return response;										// Errore di accesso o carta rimossa --> REJECT

	// Sfrutto la "MIFBlock_To_CodiciCarta" per il controllo del CRC
	if (!MIFBlock_To_CodiciCarta(&MF_DataExch.RdBuff[0])) return (response = Fail_CRC);

	// Scrivo nel buffer "RxConfigBuff" come se fosse una nuova configurazione
	// in modo che al Reset avvengano tutte le operazioni previste per una nuova
	// configurazione.
	r = 0;
	Gestore = MF_DataExch.RdBuff[0]<<8 | MF_DataExch.RdBuff[1];
	sprintf((char*)&gVars.ddcmpbuff[r],"GEST*%1u\r\n",Gestore);									// Memorizza una stringa ASCII con formato "GEST*" + Dato + crlf + NULL
	r = strlen((char*)&gVars.ddcmpbuff);

	Pin1 = MF_DataExch.RdBuff[2]<<8 | MF_DataExch.RdBuff[3];
	sprintf((char*)&gVars.ddcmpbuff[r],"FLAG*%1u\r\n",Pin1);									// Memorizza una stringa ASCII con formato "FLAG*" + Dato + crlf + NULL
	r = strlen((char*)&gVars.ddcmpbuff);
			
	Pin2 = MF_DataExch.RdBuff[4]<<8 | MF_DataExch.RdBuff[5];
	sprintf((char*)&gVars.ddcmpbuff[r],"CODE*%1u\r\n",Pin2);									// Memorizza una stringa ASCII con formato "CODE*" + Dato + crlf + NULL
	r = strlen((char*)&gVars.ddcmpbuff);
	
	UnitScalingFactor = MF_DataExch.RdBuff[6];
	sprintf((char*)&gVars.ddcmpbuff[r],"UNSF*%1u\r\n",UnitScalingFactor);						// Memorizza una stringa ASCII con formato "UNSF*" + Dato + crlf + NULL
	r = strlen((char*)&gVars.ddcmpbuff);

	DecimalPointPosition = MF_DataExch.RdBuff[7];
	sprintf((char*)&gVars.ddcmpbuff[r],"DECP*%1u\r\n",DecimalPointPosition);					// Memorizza una stringa ASCII con formato "DECP*" + Dato + crlf + NULL
	r = strlen((char*)&gVars.ddcmpbuff);
	
	MaxRecharge = MF_DataExch.RdBuff[8]<<8 | MF_DataExch.RdBuff[9];
	sprintf((char*)&gVars.ddcmpbuff[r],"CRMK*%1u\r\n",MaxRecharge);								// Memorizza una stringa ASCII con formato "CRMK*" + Dato + crlf + NULL
	r = strlen((char*)&gVars.ddcmpbuff);
	
	WriteEEPI2C(RxConfigBuff, (uint8_t *)(&gVars.ddcmpbuff), (uint8_t) (r+1));					// Scrittura buffer in EEProm compreso "0x00" di chiusura
	while(1) {};																				// ATTENDO RESET WD
}

/*---------------------------------------------------------------------------------------------*\
Method: Card_ClrMemory
	Controlla se e' una reale Carta Azzeramento Memoria con KeyB corretta.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
uint16_t  Card_ClrMemory(uint8_t SectorType) {
	
	uint8_t 	*Blk0_pnt;
	uint8_t		Tipocarta;
	uint32_t	TestVal;
	
	MF_DataExch.SectNum = InfoSectMasterCard;									// Settore NuoviCodici
	if (SectorType == RBT_MHD_EEClrCard) {
		CalcKeyB(0x6A95, 0xDF4C);												// Carta MHD di azzeramento: utilizza la chiave KeyB di accesso alla Mifare
	} else {
		CalcKeyB(Pin1, Pin2);													// Carta gestore: calcola la chiave KeyB di accesso alla Mifare
	}
	SetKeyB(&KeyB[0]);
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	if (!LeggiSettore()) return response;
	
	// Controllo valori del Blocco 0
	Seed = SemeCRC_Std;
	Blk0_pnt = &MF_DataExch.RdBuff[0];											// Normalmente si scrive cosi' ma si puo' usare anche il nome
	//Blk0_pnt = MF_DataExch.RdBuff;											// dell'array come indirizzo del primo elemento
	
	if (!Check_CRC_MifareBlock(Blk0_pnt, Seed)) return (response = NoValidFormat);
	TestVal = ( *(Blk0_pnt+3) + (*(Blk0_pnt+2) << 8) + (*(Blk0_pnt+1) << 16) + (*(Blk0_pnt) << 24) );
	Tipocarta = (*(Blk0_pnt+8));
	// Controllo che la carta non sia in Black List
	//RBT_CodiciCarta.Utente = ((*(Blk0_pnt+4) << 24) + ((*(Blk0_pnt+5)) << 16) + ((*(Blk0_pnt+6)) << 8) + (*(Blk0_pnt+7)));

	if (SectorType == RBT_MHD_EEClrCard) {
		if (TestVal != 0x59655961 || Tipocarta != 0x08)  return (response = NoValidFormat);
	} else {
		if (TestVal != 0xA50F3EC7  || Tipocarta != 0x09)  return (response = NoValidFormat);
	}
	MIFARE_OFF();																// Disattivo alimentazione PN512
	GreenLedCPU(OFF);															// Led Verde CPU OFF
	RedLedCPU(ON);																// Led Rosso CPU ON
	AzzeraTuttaEEPROM();
	BlinkErrRedLed_HALT(OneSec, End_Card_ClrMEMORY);							// Fermo il programma lampeggiando il led rosso CPU ogni secondo
	return response;
}

/*---------------------------------------------------------------------------------------------*\
Method: ClrAuditCard
	Controlla se e' una reale Carta Azzeramento Audit con KeyB corretta.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
uint16_t  ClrAuditCard(void) {
	
	uint8_t *  Blk0_pnt;
	uint32_t	TestVal;

	MF_DataExch.SectNum = InfoSectMasterCard;									// Settore NuoviCodici
	CalcKeyB(0x3FCD, 0xA9B8);													// Calcola la chiave KeyB di accesso alla Mifare
	SetKeyB(&KeyB[0]);
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	if (!LeggiSettore()) return response;

	// Controllo valori del Blocco 0
	Seed = SemeCRC_Std;
	Blk0_pnt = &MF_DataExch.RdBuff[0];											// Normalmente si scrive cosi' ma si puo' usare anche il nome
	//Blk0_pnt = MF_DataExch.RdBuff;											// dell'array come indirizzo del primo elemento
	
	if (!Check_CRC_MifareBlock(Blk0_pnt, Seed)) return (response = NoValidFormat);
	TestVal = ( *(Blk0_pnt+3) + (*(Blk0_pnt+2) << 8) + (*(Blk0_pnt+1) << 16) + (*(Blk0_pnt) << 24) );
	if (TestVal != 0x03120724)  return (response = NoValidFormat);
	if (*(Blk0_pnt+8) != 0x07)  return (response = NoValidFormat);
	
	GreenLedCPU(OFF);															// Led Verde CPU OFF
	RedLedCPU(ON);																// Led Rosso CPU ON
	AuditINClear();
	AuditLRClear();																// Se abilitata Audit Estesa azzera anche i relativi contatori blocchi e prelievi
	if (gOrionConfVars.AuditExt) {
		memset(&MemoryBytes[0], 0, 4U);											// Azzero anche counter prelievo con chiave USB in EE Area interscambio bootloader
		WriteEEPI2C(LogFileContentLen, (uint8_t *)(&MemoryBytes[0]), 4U);
	}
	BlinkErrRedLed_HALT(OneSec, End_Card_ClrAUDIT);								// Fermo il programma lampeggiando il led rosso CPU ogni secondo
	return response;
}


/*---------------------------------------------------------------------------------------------*\
Method: LeggiSettore
	Legge in MF_DataExch.RdBuff il/i blocchi del settore MF_DataExch.SectNum.
	Effettua una retry se prima lettura fallita: al secondo fallimento esce con false
 
	IN:		- 
	OUT:	- False se lettura fallita
\*----------------------------------------------------------------------------------------------*/
bool  LeggiSettore(void) {

	response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum);
	if (response == Resp_OK) return true;
	response = Retry_Card_Activation();															// Tento riattivazione carta
	if (response != Resp_OK) goto Card_Err;
	response = MifareReaderRead((uint8_t*)&MF_DataExch.RdBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum);
	if (response == Resp_OK)  return true;

Card_Err:
	response = Err_Read;																		// Errore di accesso o carta rimossa --> REJECT (0x0300)
	return false;
}
	
/*---------------------------------------------------------------------------------------------*\
Method: GestioneStatoEnabled
	Controllo tutte le condizioni per accettare/rifiutare la carta in entrata.

	IN:		- 
	OUT:	- status = Resp_OK oppure status = errore
\*----------------------------------------------------------------------------------------------*/
static uint16_t GestioneStatoEnabled(void) {

	uint16_t	totaleCash, status;

#if  DefPARALLELO				
	if (OpMode_PAR == PASVENS_AuditEMP500) {											// Questo modo di lavoro non utilizza le carte
		RifiutaCarta();
		return (status = Err_CAR_6);
	}
#endif			
	// totaleCash = TotaleCash();														// Sostituito il 25.02.15 da gOrionCashVars.creditCash per vedere se segnalazioni Baltom su
	totaleCash = gOrionCashVars.creditCash;												// errori di cash e overpay sono causati dall'uso della RAM backup invece che la .creditCash
																						// 26.02.15: gli errori erano causati dalla lettura del selettore non corretta, sistemata in "Sel24.c"
	MaxRevalue = MaxRecharge;
	status = Resp_OK;																			// Predispongo risultato OK
	
	if (gOrionConfVars.AuditExt && gOrionConfVars.TestAudExtFull && gVars.AudExtFull) {
		return (status = ExtAudFull);															// Con Audit Estesa Full non accetto alcun tipo di carta utente
	}
	
    switch (gOrionKeyVars.KR_TipoChiave) {

		case KR_CHIAVE_UTENTE :
#if  DefPARALLELO				
			if (OpMode_PAR == ModeSelettore && CashDaSelettore > 0) {							// Rifiuto la carta perche' il cash potrebbe gia' essere stato trasmesso alla
				RifiutaCarta();																	// macchina e in piu' verrebbe ricaricato sulla carta 
				return (status = Err_CAR_6);
			}
			if (OpMode_PAR == PASVENS_AuditEMP500) {
				RifiutaCarta();																	// Questo modo di lavoro non utilizza il credito 
				return (status = Err_CAR_6);
			}
#endif			
			if (gOrionKeyVars.Prepag) {
				// ==========      P R E P A G A T A          =============
				//	Accetto la carta Utente Prepagata solo se reader Enable e non c'e' cash
				MaxRevalue = 0;																	// La Carta prepagata NON e' ricaricabile
				if (!IsReaderStateEnabled() || (ArePinZero())) {
					return (status = ReaderInh);												// Rifiuto le carte prepagate se Reader non e' Enable o se i Pin sono a zero
				}
				if (fVMC_Inibito || fVMC_NoLink || TotaleCreditoAttuale() > 0) {
					RifiutaCarta();																// Accende led giallo e set flag estrazione
					status = Err_CAR_6;
				}
#if  DefPARALLELO				
				if (OpMode_PAR >= PASVENS_Lava && OpMode_PAR <= PASVENS_CartaScadenza) {		// Con PASVENS no carte prepagate perche' non si possono scrivere le free-vend
					RifiutaCarta();																// Accende led giallo e set flag estrazione
					status = Err_CAR_6;
				}
#endif			
				return status;

			} else {
				// =================   CARTA   UTENTE    STANDARD     =====================
				// Accetto la carta Utente standard se reader Enable o Idle con caricamento
				// a blocchi e i Pin non sono a zero
				if ((!ArePinZero()) && (IsReaderStateEnabled() || IsReaderIdleCB())) {
					if (gOrionConfVars.InhRechargeCard && totaleCash > NULL_VALUE) {			// Cash presente e Carta non ricaricabile
						if (gOrionConfVars.OverpayCashNoRecharge) {
							Overpay(OVERPAY_CASH);												// Cash-->Overpay e accetto Carta
							gOrionCashVars.creditCash = 0;
							CheckCashType();
						} else {           
							RifiutaCarta();														// Accende led giallo e set flag estrazione
							return (status = Err_CAR_6);
						}           
					}
					if (fVMC_Inibito || fVMC_NoLink) {
						RifiutaCarta();															// Accende led giallo e set flag estrazione
						status = Err_CAR_6;
						return status;
					}
					if ((MAX_SYSTEM_CREDIT - gOrionKeyVars.Credit) <= TotaleCreditoAttuale()) {
						RifiutaCarta();															// Riufiuto carta se il suo credito sommato al cash presente > 0xFFFF
						status = Err_CAR_1;														// Accende led giallo e set flag estrazione
						return status;
					}
					if ((MaxCredGetCard > 0) && (gOrionKeyVars.Credit > MaxCredGetCard)) {
						RifiutaCarta();															// Se MaxAcceptanceCredit e' impostato e il card credit lo supera, rifiuto la carta
						status = Err_CAR_1;														// Accende led giallo e set flag estrazione
						return status;
					}
					SetMaxRevalue();															// Determino credito caricabile sulla Carta
					if (totaleCash > MaxRevalue) {												// Cash Presente > MaxRevalue: verifico opzione "cash in overpay" o "reject card"
						if (gOrionConfVars.OverpayCashNoRecharge) {				
							Overpay(OVERPAY_CASH);												// Cash-->Overpay e accetto Carta
							gOrionCashVars.creditCash = 0;
							CheckCashType();
						} else {           
							RifiutaCarta();														// Accende led giallo e set flag estrazione
							status = Err_CAR_6;													// CAR_6 anche per rifiuto Carta che non puo' essere ricaricata
						}           
					}
	    		} else {
	    			status = ReaderInh;
	    		}
				return status;
			}

		case KR_CHIAVE_CBLK :
			if (fVMC_Inibito || fVMC_NoLink || (Pin1 == 0 && Pin2 == 0)) {						// Se VMC NoLink o Inhibit o in Pin sono a zero rifiuto la carta
				RifiutaCarta();																	// Accende led giallo e set flag estrazione
				status = Err_CAR_6;
			} else {
				if (gOrionConfVars.CardBLOCK_Reject) {
					RifiutaCarta();																// Accende led giallo e set flag estrazione
					status = Err_CAR_8;															// Carta non ammessa
				} else {
					MaxRevalue = NULL_VALUE;													// Inibisco ricarica chiave
					//MR19 StatusCaricamentoBlocchi = 1;			Non e' usata da nessuna parte
					if (totaleCash > 0) {
						Overpay(OVERPAY_CASH);
						gOrionCashVars.creditCash = 0;
						CheckCashType();
					}
				}
			}
			return status;
    }
	
    // Per tutti i tipi di carta rimanenti, da qui in avanti controllo lo stato 
    // di Enable e i Pin che non siano entrambi a zero
    if (!IsReaderStateEnabled() || ArePinZero()) {
		return (status = ReaderInh);															// Rifiuto le carte se Reader non e' nello stato di Enable
	}
    switch (gOrionKeyVars.KR_TipoChiave) {
			
		case KR_CHIAVE_VENDITE_PROVA :
			if (CashCaricamentoBlocchi > 0) {													// Cash da caricamento a blocchi: rifiuto carta vendite di test 
				RifiutaCarta();																	// Accende led giallo e set flag estrazione
				status = Err_CAR_6;
			} else {
				if (gOrionConfVars.RechTestVendCard == false) {									// Carta di test NON ricaricabile
					if (totaleCash > 0) { 														// Cash in Overpay
						Overpay(OVERPAY_CASH);
						gOrionCashVars.creditCash = 0;
						CheckCashType();
					}
					MaxRevalue = NULL_VALUE;													// Inhibit accettazione cash
				} else {
					SetMaxRevalue();															// Carta vendite di test ricaricabile: determino credito caricabile sulla Carta
				}
			}
			break;
		
		default :
			status = Fail_TipoCarta;
			break;
    }
    return status;
}   

/*---------------------------------------------------------------------------------------------*\
Method: AzzeraCreditCard
	Azzera le variabili che contengono i crediti carta.

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  AzzeraCreditCard(void) {

	gOrionKeyVars.Credit = 0;
	gOrionKeyVars.FreeCredit = 0;
}

/*---------------------------------------------------------------------------------------------*\
Method: ArePinZero
	Controlla se Pin1 e Pin2 sono uguali a zero.

	IN:		- 
	OUT:	- true se Pin1 e Pin2 = 0, altrimenti false
\*----------------------------------------------------------------------------------------------*/
bool  ArePinZero(void) {

	return ((Pin1 == 0 && Pin2 == 0) ? true : false);
}

/*---------------------------------------------------------------------------------------------*\
Method: CheckCartaRBT
	Carta universale RBT.

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  CheckCartaRBT(void) {
	
	uint32_t	Word1, Word2, Word3, Word4;
	uint8_t		BackupSectNum;

	Word1 = 0;
	Word2 = 0;
	Word3 = 0;
	Word4 = 0;
	
	gOrionKeyVars.RTB_Card = 0;																// NO Carta RBT
	if (MF_DataExch.MAD) {
		BackupSectNum = MF_DataExch.SectNum;												// Salvo SectorNum di entrata
		MF_DataExch.SectNum = 0x0f;															// Settore Sicurezza
		SetKeyA(&SicurRBTKeyA[0]);															// Set KEYA Sicur per Autenticare il Settore Sicurezza
		MF_DataExch.BlockNum[0] = Block2;
		MF_DataExch.BlockNum[1] = NoBlock;
		MF_DataExch.BlockNum[2] = NoBlock;
		MF_DataExch.BlockNum[3] = NoBlock;
		LeggiSettore();
		MF_DataExch.SectNum = BackupSectNum;												// Restore SectorNum di entrata
				
		Word1 = (MF_DataExch.RdBuff[0] | (MF_DataExch.RdBuff[1]<<8) | (MF_DataExch.RdBuff[2]<<16) | (MF_DataExch.RdBuff[3]<<24) );
		Word2 = (MF_DataExch.RdBuff[4] | (MF_DataExch.RdBuff[5]<<8) | (MF_DataExch.RdBuff[6]<<16) | (MF_DataExch.RdBuff[7]<<24) );
		Word3 = (MF_DataExch.RdBuff[8] | (MF_DataExch.RdBuff[9]<<8) | (MF_DataExch.RdBuff[10]<<16) | (MF_DataExch.RdBuff[11]<<24) );
		Word4 = (MF_DataExch.RdBuff[12] | (MF_DataExch.RdBuff[13]<<8) | (MF_DataExch.RdBuff[14]<<16) | (MF_DataExch.RdBuff[15]<<24) );

		if ((Word1 == 596594961) && (Word2 == 27352931) && (Word3 == 59619396) && (Word4 == 11090711))
		{
			gOrionKeyVars.RTB_Card = 6916;													// Carta RBT
		}
	}
}	

/*---------------------------------------------------------------------------------------------*\
Method: RifiutaCarta
	Accende il led Giallo sulla bocchetta e set flag estrazione carta.

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  RifiutaCarta(void) {
	
	Accendi_LedGiallo();
	Set_AttesaEstrazione();
}

#if DefPKCOIN
/*---------------------------------------------------------------------------------------------*\
Method: PresenzaGettone
	Chiamata continuamente dal Main, restituisce lo stato della linea "RxComGett" che indica
	la presenza o meno di un gettone nel selettore.
	Se rilevato un gettone carico un timeout entro il quale le funzioni Mifare devono averlo
	letto: in caso contrario significa che il gettone non era Mifare e deve essere espulso.
	 
	IN:		- 
	OUT:	- gOrionKeyVars.MIF_Token true se gettone presente, altrimenti false
\*----------------------------------------------------------------------------------------------*/
void  PresenzaGettone(void) {

	if ((Read_PortE() & RxComGett) == 0) {
		if (gOrionKeyVars.MIF_Token == false) {
			gOrionKeyVars.MIF_Token = true;
			ToutToken = gVars.gKernelFreeCounter + OneSec;										// Timeout rilevamento gettone
		}
	} else {
		gOrionKeyVars.MIF_Token = false;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: CheckTokenMIF
	Dopo la lettura del gettone Mifare ne controlla l'esito per sapere se il sistema intende 
	tenerlo in uso, renderlo o incassarlo (RFFieldFree=true).
	Uso il timer "Touts.tmRelay" perche' libero dal PK3-PAR.
	 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  CheckTokenMIF(void) {
	
	if (gOrionKeyVars.MIF_Token) {
		if (gOrionKeyVars.RFFieldFree) {														// L'esito della ReadMifare e' una richiesta di reject
			OutSlave2(ON);																		// Attivo uscita TxComGett per richiedere il reject alla gettoniera
			Touts.tmRelay = Milli30;
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: TokenMIF_Timeout
	Quando la gettoniera segnala la presenza di un gettone, il sistema deve leggere una Mifare
	entro il timeout e azzerare il counter "ToutToken", altrimenti significa che il gettone
	non e' Mifare e si deve segnalare alla gettoniera di renderlo.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  TokenMIF_Timeout(void) {

	if (gOrionKeyVars.MIF_Token) {
		if (ToutToken == 0) return;																// Gettone letto
		if (gVars.gKernelFreeCounter >= ToutToken) {											// Timeout rilevamento gettone scaduto
			ToutToken = 0;
			OutSlave2(ON);																		// Attivo uscita TxComGett per segnalare reject alla gettoniera
			Touts.tmRelay = Milli30;
		}
	}
}

#endif


#if SetVergineRBT
/* ********************************************************************************************************************************* */
/* ********************************************************************************************************************************* */
/* ********************************************************************************************************************************* */
/*---------------------------------------------------------------------------------------------*\
Method: Da_Costruttore_A_Vergine_MHD
	Trasforma una carta Vergine da Costruttore in Vergine MicroHard.
	Le carte devono avere i trailer 'FFFFFFFFFFFFFF078069FFFFFFFFFFFF' che verra' letto
	'000000000000FF078069FFFFFFFFFFFF' essendo la KeyA mai leggibile.
	Si accede in scrittura con KeyA = FFFFFFFFFFFF.
	 
	IN:		- 
	OUT:	- Response = Resp_OK se la scrittura di tutti i settori e' OK
\*----------------------------------------------------------------------------------------------*/

uint16_t  Da_Costruttore_A_Vergine_MHD(void) {
	uint8_t	r;
	uint32_t	EorCalc[2];
	

	MF_DataExch.KeyType = false;  														// Set KeyA per accesso in scrittura ai settori
	for (r=0; r<6; r++) {
		MF_DataExch.KeyVal[r] = 0xFF;													// KeyA = 0xFFFFFFFFFFFF
	}
	
/* **************  Settore  0  -  MAD     *******************************/
	
	ClrWrBuff();																		// Azzero i 48 bytes di WrBuff
	MF_DataExch.WrBuff[1] =	0x01;
	MF_DataExch.WrBuff[2] =	0x04;
	for (r=4; r<16; r++) {
		MF_DataExch.WrBuff[r] 	=	0x38;
		MF_DataExch.WrBuff[r+1] =	0x69;
		r++;
	}
	MF_DataExch.WrBuff[30] 	=	0x38;													// Settore Sicurezza
	MF_DataExch.WrBuff[31] 	=	0x69;
	
	MF_DataExch.WrBuff[0]	= Calc_CRC8_MAD(&MF_DataExch.WrBuff[1], 31);				// Calcola CRC8 dei blocchi 1 e 2 del settore 0

	// ***  Trailer  Settore 0  *****
	MF_DataExch.WrBuff[32] 	= 0xA0;								// KeyA standard A0, A1, A2, A3, A4, A5
	MF_DataExch.WrBuff[33] 	= 0xA1;
	MF_DataExch.WrBuff[34] 	= 0xA2;
	MF_DataExch.WrBuff[35] 	= 0xA3;
	MF_DataExch.WrBuff[36] 	= 0xA4;
	MF_DataExch.WrBuff[37] 	= 0xA5;
	MF_DataExch.WrBuff[38] 	= 0x78;								// Access bits
	MF_DataExch.WrBuff[39] 	= 0x77;
	MF_DataExch.WrBuff[40] 	= 0x88;
	MF_DataExch.WrBuff[41]	= 0xC1;								// GPB MAD
	MF_DataExch.WrBuff[42] 	= 0xB2;								// KeyB MAD
	MF_DataExch.WrBuff[43] 	= 0x76;
	MF_DataExch.WrBuff[44] 	= 0x65;
	MF_DataExch.WrBuff[45] 	= 0xEC;
	MF_DataExch.WrBuff[46] 	= 0x95;
	MF_DataExch.WrBuff[47] 	= 0x6A;

	MF_DataExch.SectNum = 0;									// Settore 0 (MAD)
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block1;
	MF_DataExch.BlockNum[1] = Block2;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	WriteSector();
	if (response != Resp_OK) return response;
	
/* **************  Settore  1  -  CARD HOLDER   *************************/

	MF_DataExch.WrBuff[0] 	= 0xED;								// 'MICROHARD_SRL_47042_CESENATICO_TEL_054775450
	MF_DataExch.WrBuff[1] 	= 'M';
	MF_DataExch.WrBuff[2] 	= 'I';
	MF_DataExch.WrBuff[3] 	= 'C';
	MF_DataExch.WrBuff[4] 	= 'R';
	MF_DataExch.WrBuff[5] 	= 'O';
	MF_DataExch.WrBuff[6] 	= 'H';
	MF_DataExch.WrBuff[7] 	= 'A';
	MF_DataExch.WrBuff[8] 	= 'R';
	MF_DataExch.WrBuff[9] 	= 'D';
	MF_DataExch.WrBuff[10] 	= '_';
	MF_DataExch.WrBuff[11] 	= 'S';
	MF_DataExch.WrBuff[12] 	= 'R';
	MF_DataExch.WrBuff[13] 	= 'L';
	MF_DataExch.WrBuff[14] 	= '_';
	MF_DataExch.WrBuff[15] 	= '4';

	MF_DataExch.WrBuff[16] 	= '7';
	MF_DataExch.WrBuff[17] 	= '0';
	MF_DataExch.WrBuff[18] 	= '4';
	MF_DataExch.WrBuff[19] 	= '2';
	MF_DataExch.WrBuff[20] 	= '_';
	MF_DataExch.WrBuff[21] 	= 'C';
	MF_DataExch.WrBuff[22] 	= 'E';
	MF_DataExch.WrBuff[23] 	= 'S';
	MF_DataExch.WrBuff[24] 	= 'E';
	MF_DataExch.WrBuff[25] 	= 'N';
	MF_DataExch.WrBuff[26] 	= 'A';
	MF_DataExch.WrBuff[27] 	= 'T';
	MF_DataExch.WrBuff[28] 	= 'I';
	MF_DataExch.WrBuff[29] 	= 'C';
	MF_DataExch.WrBuff[30] 	= 'O';
	MF_DataExch.WrBuff[31] 	= '_';

	MF_DataExch.WrBuff[32] 	= 'T';
	MF_DataExch.WrBuff[33] 	= 'E';
	MF_DataExch.WrBuff[34] 	= 'L';
	MF_DataExch.WrBuff[35] 	= '_';
	MF_DataExch.WrBuff[36] 	= '0';
	MF_DataExch.WrBuff[37] 	= '5';
	MF_DataExch.WrBuff[38] 	= '4';
	MF_DataExch.WrBuff[39] 	= '7';
	MF_DataExch.WrBuff[40] 	= '7';
	MF_DataExch.WrBuff[41] 	= '5';
	MF_DataExch.WrBuff[42] 	= '4';
	MF_DataExch.WrBuff[43] 	= '5';
	MF_DataExch.WrBuff[44] 	= '0';
	MF_DataExch.WrBuff[45] 	= 0x00;
	MF_DataExch.WrBuff[46] 	= 0x00;
	MF_DataExch.WrBuff[47] 	= 0x00;

	MF_DataExch.SectNum = 1;									// Settore 1 CARD HOLDER
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	WriteSector();												// A volte programmo anche carte che non hanno accesso al Settore 1
	if (response == Resp_OK) {									// quindi se errore riattivo la carta e procedo col Settore 2

		// ***  Trailer  Settore 1 Card Holder   *****
		MF_DataExch.WrBuff[0] 	= 0xA0;								// KeyA standard A0, A1, A2, A3, A4, A5
		MF_DataExch.WrBuff[1] 	= 0xA1;
		MF_DataExch.WrBuff[2] 	= 0xA2;
		MF_DataExch.WrBuff[3] 	= 0xA3;
		MF_DataExch.WrBuff[4] 	= 0xA4;
		MF_DataExch.WrBuff[5] 	= 0xA5;
		MF_DataExch.WrBuff[6] 	= 0x78;								// Access bits
		MF_DataExch.WrBuff[7] 	= 0x77;
		MF_DataExch.WrBuff[8] 	= 0x88;
		MF_DataExch.WrBuff[9] 	= 0x00;								// GPB = 0
		MF_DataExch.WrBuff[10] 	= 0x6D;								// KeyB Settore Card_Holder (6D 69 63 72 6F 68)
		MF_DataExch.WrBuff[11] 	= 0x69;
		MF_DataExch.WrBuff[12] 	= 0x63;
		MF_DataExch.WrBuff[13] 	= 0x72;
		MF_DataExch.WrBuff[14] 	= 0x6F;
		MF_DataExch.WrBuff[15] 	= 0x68;

		MF_DataExch.BlockNum[0] = Trailer;
		MF_DataExch.BlockNum[1] = NoBlock;
		MF_DataExch.BlockNum[2] = NoBlock;
		MF_DataExch.BlockNum[3] = NoBlock;
		WriteSector();
	}
	else {
		Retry_Card_Activation();								
	}
		
		
/* ***************  Settore  2  -  INFOSECTOR   *************************/
	
	ClrWrBuff();																	// Azzero i 48 bytes di WrBuff

	MF_DataExch.SectNum = InfoSectorNum;											// Settore 2 InfoSector
	HEXL = MF_DataExch.SectNum;														// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block2;												// Azzera il contenuto del blocco 3 del settore perche' non e' utilizzato
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();														// Scrittura settore
	if (response != Resp_OK) return response;
	
	MF_DataExch.WrBuff[4] 	= Utente>>24;											// Codice Utente (MSB)
	MF_DataExch.WrBuff[5] 	= Utente>>16;											// Codice Utente
	MF_DataExch.WrBuff[6] 	= Utente>>8;											// Codice Utente
	MF_DataExch.WrBuff[7] 	= Utente;												// Codice Utente (LSB)
	MF_DataExch.WrBuff[8] 	= KR_CHIAVE_UTENTE;										// Tipo Carta
	SetCRC16Buff(&MF_DataExch.WrBuff[0], MIF_BLOCKLEN-2, 0);
	MF_DataExch.WrBuff[14] = gVars.CRC16;											// CRC16 LSB
	MF_DataExch.WrBuff[15] = (gVars.CRC16)>>8;										// CRC16 MSB

	MF_DataExch.WrBuff[16] 	= Secure_Sector;
	MF_DataExch.WrBuff[17] 	= RBT_Secure;
	MF_DataExch.WrBuff[18] 	= MHDCreditSector;
	MF_DataExch.WrBuff[19] 	= RBT_Credito;
	MF_DataExch.WrBuff[20] 	= SelGest1_Sector;
	MF_DataExch.WrBuff[21] 	= RBT_SelGest1;
	MF_DataExch.WrBuff[22] 	= SelGest2_Sector;
	MF_DataExch.WrBuff[23] 	= RBT_SelGest2;
	MF_DataExch.WrBuff[30] 	= 0x6A;
	MF_DataExch.WrBuff[31] 	= 0x41;

	// ***  Trailer  Settore 2  *****
	MF_DataExch.WrBuff[32] 	= 0x2B;								// KeyA lettura settori (2B 67 56 CE 59 A6)
	MF_DataExch.WrBuff[33] 	= 0x67;
	MF_DataExch.WrBuff[34] 	= 0x56;
	MF_DataExch.WrBuff[35] 	= 0xCE;
	MF_DataExch.WrBuff[36] 	= 0x59;
	MF_DataExch.WrBuff[37] 	= 0xA6;
	MF_DataExch.WrBuff[38] 	= 0x78;								// Access bits
	MF_DataExch.WrBuff[39] 	= 0x77;
	MF_DataExch.WrBuff[40] 	= 0x88;
	MF_DataExch.WrBuff[41] 	= RBT_Vergine;						// GPB carta vergine
	MF_DataExch.WrBuff[42] 	= 0xC7;								// KeyB per tutti i settori carte vergini RBT (C7 14 D5 0A E3 06)
	MF_DataExch.WrBuff[43] 	= 0x14;
	MF_DataExch.WrBuff[44] 	= 0xD5;
	MF_DataExch.WrBuff[45] 	= 0x0A;
	MF_DataExch.WrBuff[46] 	= 0xE3;
	MF_DataExch.WrBuff[47] 	= 0x06;

	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	WriteSector();												// Scrittura settore
	if (response != Resp_OK) return response;
	
/* ***************  Settore  3  -  CREDITO   ****************************/
	
	// ***  Trailer  Settore 3  *****
	ClrWrBuff();																	// Azzero i 48 bytes di WrBuff
	MF_DataExch.SectNum = MHDCreditSector;											// Settore 3 CreditSector
	HEXL = MF_DataExch.SectNum;														// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;												// Azzera il contenuto dei 3 blocchi del settore
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();														// Scrittura settore
	if (response != Resp_OK) return response;
	
	MF_DataExch.WrBuff[0] 	= 0x2B;													// KeyA lettura settori (2B 67 56 CE 59 A6)
	MF_DataExch.WrBuff[1] 	= 0x67;
	MF_DataExch.WrBuff[2] 	= 0x56;
	MF_DataExch.WrBuff[3] 	= 0xCE;
	MF_DataExch.WrBuff[4] 	= 0x59;
	MF_DataExch.WrBuff[5] 	= 0xA6;
	MF_DataExch.WrBuff[6] 	= 0x78;													// Access bits
	MF_DataExch.WrBuff[7] 	= 0x77;
	MF_DataExch.WrBuff[8] 	= 0x88;
	MF_DataExch.WrBuff[9] 	= RBT_Credito;											// GPB Settore Credito
	MF_DataExch.WrBuff[10] 	= 0xC7;													// KeyB per tutti i settori carte vergini RBT (C7 14 D5 0A E3 06)
	MF_DataExch.WrBuff[11] 	= 0x14;
	MF_DataExch.WrBuff[12] 	= 0xD5;
	MF_DataExch.WrBuff[13] 	= 0x0A;
	MF_DataExch.WrBuff[14] 	= 0xE3;
	MF_DataExch.WrBuff[15] 	= 0x06;
	
	MF_DataExch.BlockNum[0] = Trailer;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();														// Scrittura settore
	if (response != Resp_OK) return response;
	
	
// *******  Settore  4  -  Gestione Selezioni  N. 1 ************
		
	// ***  Trailer  Settore 4  *****
	ClrWrBuff();																	// Azzero i 48 bytes di WrBuff
	MF_DataExch.SectNum = SelGest1_Sector;											// Settore 4 Gestione Selezioni N. 1
	HEXL = MF_DataExch.SectNum;														// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;												// Azzera il contenuto dei 3 blocchi del settore
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();														// Scrittura settore
	if (response != Resp_OK) return response;

	MF_DataExch.WrBuff[0] 	= 0x2B;												// KeyA lettura settori (2B 67 56 CE 59 A6)
	MF_DataExch.WrBuff[1] 	= 0x67;
	MF_DataExch.WrBuff[2] 	= 0x56;
	MF_DataExch.WrBuff[3] 	= 0xCE;
	MF_DataExch.WrBuff[4] 	= 0x59;
	MF_DataExch.WrBuff[5] 	= 0xA6;
	MF_DataExch.WrBuff[6] 	= 0x78;												// Access bits
	MF_DataExch.WrBuff[7] 	= 0x77;
	MF_DataExch.WrBuff[8] 	= 0x88;
	MF_DataExch.WrBuff[9] 	= RBT_SelGest1;										// GPB Settore Gestione Selezioni N. 1
	MF_DataExch.WrBuff[10] 	= 0xC7;												// KeyB per tutti i settori carte vergini RBT (C7 14 D5 0A E3 06)
	MF_DataExch.WrBuff[11] 	= 0x14;
	MF_DataExch.WrBuff[12] 	= 0xD5;
	MF_DataExch.WrBuff[13] 	= 0x0A;
	MF_DataExch.WrBuff[14] 	= 0xE3;
	MF_DataExch.WrBuff[15] 	= 0x06;
	
	MF_DataExch.BlockNum[0] = Trailer;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();													// Scrittura settore
	if (response != Resp_OK) return response;
		

// *******  Settore  5  -  Gestione Selezioni  N. 2 ************
				
	// ***  Trailer  Settore 5  *****
	ClrWrBuff();																	// Azzero i 48 bytes di WrBuff
	MF_DataExch.SectNum = SelGest2_Sector;											// Settore 5 Gestione Selezioni N. 2
	HEXL = MF_DataExch.SectNum;														// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;												// Azzera il contenuto dei 3 blocchi del settore
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();														// Scrittura settore
	if (response != Resp_OK) return response;

	MF_DataExch.WrBuff[0] 	= 0x2B;												// KeyA lettura settori (2B 67 56 CE 59 A6)
	MF_DataExch.WrBuff[1] 	= 0x67;
	MF_DataExch.WrBuff[2] 	= 0x56;
	MF_DataExch.WrBuff[3] 	= 0xCE;
	MF_DataExch.WrBuff[4] 	= 0x59;
	MF_DataExch.WrBuff[5] 	= 0xA6;
	MF_DataExch.WrBuff[6] 	= 0x78;												// Access bits
	MF_DataExch.WrBuff[7] 	= 0x77;
	MF_DataExch.WrBuff[8] 	= 0x88;
	MF_DataExch.WrBuff[9] 	= RBT_SelGest2;										// GPB Settore Gestione Selezioni N. 2
	MF_DataExch.WrBuff[10] 	= 0xC7;												// KeyB per tutti i settori carte vergini RBT (C7 14 D5 0A E3 06)
	MF_DataExch.WrBuff[11] 	= 0x14;
	MF_DataExch.WrBuff[12] 	= 0xD5;
	MF_DataExch.WrBuff[13] 	= 0x0A;
	MF_DataExch.WrBuff[14] 	= 0xE3;
	MF_DataExch.WrBuff[15] 	= 0x06;
	
	MF_DataExch.SectNum = SelGest2_Sector;										// Settore 5 Gestione Selezioni N. 2
	HEXL = MF_DataExch.SectNum;													// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Trailer;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();													// Scrittura settore
	if (response != Resp_OK) return response;
				
	

// ***************  Settori  LIBERI  DAL 6 AL 14  ***********************
//	Dei settori liberi cambio solo il Trailer con la KeyB da "FFFFFFFFFFFF"
//	a FreeSector. Se la KeyB non e' tutti "FF" tento con la KeyB per ogni
//	settore perche' potrebbe essere una carta gia' utilizzata per lo sviluppo.
	
	MF_DataExch.WrBuff[9] 	= 0;													// GPB = 0
	for (r=6; r<=14; r++) {
		SetKeyA(&KeyA_VergCostr[0]);												// Set KeyA = 0xFFFFFFFFFFFF per accesso in scrittura ai settori
		MF_DataExch.SectNum = r;													// Settore in aggiornamento
		HEXL = r;																	// Stato formattazione: riporta il settore sul quale si opera
		response = WriteSector();													// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response = FailWriteFreeSect);			// Carta Estratta o non piu' accessibile
			CalcKeyB_FreeSectors(MF_DataExch.SectNum);								// Tento scrittura con KeyB FreeSector
			SetKeyB(&KeyB[0]);
			response = WriteSector();												// Scrittura settore
			if (response != Resp_OK) {
				response = Retry_Card_Activation();
				if (response != Resp_OK) return (response = FailWriteFreeSect);		// Carta Estratta o non piu' accessibile
				SetKeyB(&VergineRBTKeyB[0]);										// Tento scrittura con vecchia KeyB fissa per i FreeSector
				response = WriteSector();											// Scrittura settore
				if (response != Resp_OK) return (response = FailWriteFreeSect);		// Carta Estratta o non piu' accessibile
			}
		}
	}

/* ***************  Settore 15  -  SICUREZZA ****************************/
	// Se DebugFormatCard=1 sono in debug e non ho modificato settori
	//  precedenti, per cui lascio invariata KeyA = tutti FF
	if (!DebugFormatCard) {
		SetKeyA(&StdKeyA[0]);														// Set KEYA standard per Autenticare il Settore 0
	}

	MF_DataExch.SectNum = 0;														// Settore 0
	MF_DataExch.BlockNum[0] = Block0;												// Blocco da leggere del settore SectNum
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;

	response = MifareReaderRead(&MF_DataExch.RdBuff[0], MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
	if (response != Resp_OK) {
		// Read Fail: Ritento attivazione
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);									// Riattivazione fallita: REJECT CARD
		response = MifareReaderRead(&MF_DataExch.RdBuff[0], MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
		if (response != Resp_OK) return (response);									// Lettura fallita: Settore 0 non accessibile
	}
	ClrWrBuff();																	// Azzero i 48 bytes di WrBuff
	// Crypto i primi 8 bytes del Blocco 0 in cryptmsg
	cryptmsg[0] = ( MF_DataExch.RdBuff[0] | (MF_DataExch.RdBuff[1]<<8) | (MF_DataExch.RdBuff[2]<<16) | (MF_DataExch.RdBuff[3]<<24) );
	cryptmsg[1] = ( MF_DataExch.RdBuff[4] | (MF_DataExch.RdBuff[5]<<8) | (MF_DataExch.RdBuff[6]<<16) | (MF_DataExch.RdBuff[7]<<24) );
	encipher(cryptmsg, k);
	MF_DataExch.WrBuff[0] = cryptmsg[0];
	MF_DataExch.WrBuff[1] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[2] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[3] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[4] = cryptmsg[1];
	MF_DataExch.WrBuff[5] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[6] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[7] = cryptmsg[1]>>24;

	// Crypto i secondi 8 bytes del Blocco 0 dopo aver eseguito l'Xor con i primi 8 bytes criptati
	EorCalc[0] = ( MF_DataExch.RdBuff[8] | (MF_DataExch.RdBuff[9]<<8) | (MF_DataExch.RdBuff[10]<<16) | (MF_DataExch.RdBuff[11]<<24) );
	EorCalc[1] = ( MF_DataExch.RdBuff[12] | (MF_DataExch.RdBuff[13]<<8) | (MF_DataExch.RdBuff[14]<<16) | (MF_DataExch.RdBuff[15]<<24) );
	cryptmsg[0] ^= EorCalc[0];
	cryptmsg[1] ^= EorCalc[1];
	encipher(cryptmsg, k);
	MF_DataExch.WrBuff[8] = cryptmsg[0];
	MF_DataExch.WrBuff[9] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[10] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[11] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[12] = cryptmsg[1];
	MF_DataExch.WrBuff[13] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[14] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[15] = cryptmsg[1]>>24;

	// Calcolo checksum dell'intero Blocco 0 criptato
	SetCRC16Buff(&MF_DataExch.WrBuff[0], MIF_BLOCKLEN, SemeCrypto);
	MF_DataExch.WrBuff[16] = gVars.CRC16;											// CRC16 LSB
	MF_DataExch.WrBuff[17] = (gVars.CRC16)>>8;										// CRC16 MSB

	// Preparo Trailer Settore Sicurezza
	MF_DataExch.WrBuff[32] 	= 0x13;													// KeyA Settore Sicurezza (13 A6 5E C4 29 82)
	MF_DataExch.WrBuff[33] 	= 0xA6;
	MF_DataExch.WrBuff[34] 	= 0x5E;
	MF_DataExch.WrBuff[35] 	= 0xC4;
	MF_DataExch.WrBuff[36] 	= 0x29;
	MF_DataExch.WrBuff[37] 	= 0x82;
	MF_DataExch.WrBuff[38] 	= 0x78;													// Access bits (78 7A F8)
	MF_DataExch.WrBuff[39] 	= 0x77;
	MF_DataExch.WrBuff[40] 	= 0x88;
	MF_DataExch.WrBuff[41] 	= 0x33;													// GPB Settore Sicurezza
	MF_DataExch.WrBuff[42] 	= 0x3A;													// KeyB Settore Sicurezza (3A F7 51 8C 40 DB)
	MF_DataExch.WrBuff[43] 	= 0xF7;
	MF_DataExch.WrBuff[44] 	= 0x51;
	MF_DataExch.WrBuff[45] 	= 0x8C;
	MF_DataExch.WrBuff[46] 	= 0x40;
	MF_DataExch.WrBuff[47] 	= 0xDB;

	// Preparo KeyA di scrittura Settore Sicurezza
	MF_DataExch.KeyType = false;  													// Set KeyA = 0xFFFFFFFFFFFF per scrivere il Settore sicurezza
	for (r=0; r<6; r++) {
		MF_DataExch.KeyVal[r] = 0xFF;
	}
	MF_DataExch.SectNum = 15;														// Settore 15: Settore Sicurezza
	HEXL = MF_DataExch.SectNum;														// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();														// Scrittura settore
	return response;
}

/*---------------------------------------------------------------------------------------------*\
Method: Da_Costruttore_A_Vergine_Prepagata_MHD_Con_Taglio_10_Euro
	Trasforma una carta Vergine da Costruttore in Vergine Prepagata.
	Le carte devono avere i trailer 'FFFFFFFFFFFFFF778069FFFFFFFFFFFF'.
	Si accede in scrittura con KeyA = FFFFFFFFFFFF.
	 
	IN:		- 
	OUT:	- Response = Resp_OK se la scrittura di tutti i settori e' OK
\*----------------------------------------------------------------------------------------------*/

uint16_t  Da_Costruttore_A_Vergine_Prepagata_MHD_Con_Taglio_10_Euro(void) {

uint8_t	r;
uint32_t	EorCalc[2];


	MF_DataExch.KeyType = false;  			// Set KeyA per accesso in scrittura ai settori
	for (r=0; r<6; r++) {
		MF_DataExch.KeyVal[r] = 0xFF;		// KeyA = 0xFFFFFFFFFFFF
	}

/* **************  Settore  0  -  MAD     *******************************/

	ClrWrBuff();							// Azzero i 48 bytes di WrBuff
	MF_DataExch.WrBuff[1] =	0x01;
	MF_DataExch.WrBuff[2] =	0x04;
	for (r=4; r<16; r++) {
		MF_DataExch.WrBuff[r] 	=	0x38;
		MF_DataExch.WrBuff[r+1] =	0x69;
		r++;
	}
	MF_DataExch.WrBuff[30] 	=	0x38;		// Settore Sicurezza
	MF_DataExch.WrBuff[31] 	=	0x69;

	MF_DataExch.WrBuff[0]	= Calc_CRC8_MAD(&MF_DataExch.WrBuff[1], 31);				// Calcola CRC8 dei blocchi 1 e 2 del settore 0

	// ***  Trailer  Settore 0  *****
	MF_DataExch.WrBuff[32] 	= 0xA0;								// KeyA standard A0, A1, A2, A3, A4, A5
	MF_DataExch.WrBuff[33] 	= 0xA1;
	MF_DataExch.WrBuff[34] 	= 0xA2;
	MF_DataExch.WrBuff[35] 	= 0xA3;
	MF_DataExch.WrBuff[36] 	= 0xA4;
	MF_DataExch.WrBuff[37] 	= 0xA5;
	MF_DataExch.WrBuff[38] 	= 0x78;								// Access bits
	MF_DataExch.WrBuff[39] 	= 0x77;
	MF_DataExch.WrBuff[40] 	= 0x88;
	MF_DataExch.WrBuff[41]	= 0xC1;								// GPB MAD
	MF_DataExch.WrBuff[42] 	= 0xB2;								// KeyB MAD
	MF_DataExch.WrBuff[43] 	= 0x76;
	MF_DataExch.WrBuff[44] 	= 0x65;
	MF_DataExch.WrBuff[45] 	= 0xEC;
	MF_DataExch.WrBuff[46] 	= 0x95;
	MF_DataExch.WrBuff[47] 	= 0x6A;

	MF_DataExch.SectNum = 0;									// Settore 0 (MAD)
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block1;
	MF_DataExch.BlockNum[1] = Block2;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	WriteSector();
	if (response != Resp_OK) return response;

	/* **************  Settore  1  -  CARD HOLDER   *************************/

	MF_DataExch.WrBuff[0] 	= 0xED;								// 'MICROHARD_SRL_47042_CESENATICO_TEL_054775450
	MF_DataExch.WrBuff[1] 	= 'M';
	MF_DataExch.WrBuff[2] 	= 'I';
	MF_DataExch.WrBuff[3] 	= 'C';
	MF_DataExch.WrBuff[4] 	= 'R';
	MF_DataExch.WrBuff[5] 	= 'O';
	MF_DataExch.WrBuff[6] 	= 'H';
	MF_DataExch.WrBuff[7] 	= 'A';
	MF_DataExch.WrBuff[8] 	= 'R';
	MF_DataExch.WrBuff[9] 	= 'D';
	MF_DataExch.WrBuff[10] 	= '_';
	MF_DataExch.WrBuff[11] 	= 'S';
	MF_DataExch.WrBuff[12] 	= 'R';
	MF_DataExch.WrBuff[13] 	= 'L';
	MF_DataExch.WrBuff[14] 	= '_';
	MF_DataExch.WrBuff[15] 	= '4';

	MF_DataExch.WrBuff[16] 	= '7';
	MF_DataExch.WrBuff[17] 	= '0';
	MF_DataExch.WrBuff[18] 	= '4';
	MF_DataExch.WrBuff[19] 	= '2';
	MF_DataExch.WrBuff[20] 	= '_';
	MF_DataExch.WrBuff[21] 	= 'C';
	MF_DataExch.WrBuff[22] 	= 'E';
	MF_DataExch.WrBuff[23] 	= 'S';
	MF_DataExch.WrBuff[24] 	= 'E';
	MF_DataExch.WrBuff[25] 	= 'N';
	MF_DataExch.WrBuff[26] 	= 'A';
	MF_DataExch.WrBuff[27] 	= 'T';
	MF_DataExch.WrBuff[28] 	= 'I';
	MF_DataExch.WrBuff[29] 	= 'C';
	MF_DataExch.WrBuff[30] 	= 'O';
	MF_DataExch.WrBuff[31] 	= '_';

	MF_DataExch.WrBuff[32] 	= 'T';
	MF_DataExch.WrBuff[33] 	= 'E';
	MF_DataExch.WrBuff[34] 	= 'L';
	MF_DataExch.WrBuff[35] 	= '_';
	MF_DataExch.WrBuff[36] 	= '0';
	MF_DataExch.WrBuff[37] 	= '5';
	MF_DataExch.WrBuff[38] 	= '4';
	MF_DataExch.WrBuff[39] 	= '7';
	MF_DataExch.WrBuff[40] 	= '7';
	MF_DataExch.WrBuff[41] 	= '5';
	MF_DataExch.WrBuff[42] 	= '4';
	MF_DataExch.WrBuff[43] 	= '5';
	MF_DataExch.WrBuff[44] 	= '0';
	MF_DataExch.WrBuff[45] 	= 0x00;
	MF_DataExch.WrBuff[46] 	= 0x00;
	MF_DataExch.WrBuff[47] 	= 0x00;

	MF_DataExch.SectNum = 1;									// Settore 1 CARD HOLDER
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	WriteSector();												// A volte programmo anche carte che non hanno accesso al Settore 1
	if (response == Resp_OK) {									// quindi se errore riattivo la carta e procedo col Settore 2

	// ***  Trailer  Settore 1 Card Holder   *****
	MF_DataExch.WrBuff[0] 	= 0xA0;								// KeyA standard A0, A1, A2, A3, A4, A5
	MF_DataExch.WrBuff[1] 	= 0xA1;
	MF_DataExch.WrBuff[2] 	= 0xA2;
	MF_DataExch.WrBuff[3] 	= 0xA3;
	MF_DataExch.WrBuff[4] 	= 0xA4;
	MF_DataExch.WrBuff[5] 	= 0xA5;
	MF_DataExch.WrBuff[6] 	= 0x78;								// Access bits
	MF_DataExch.WrBuff[7] 	= 0x77;
	MF_DataExch.WrBuff[8] 	= 0x88;
	MF_DataExch.WrBuff[9] 	= 0x00;								// GPB = 0
	MF_DataExch.WrBuff[10] 	= 0x6D;								// KeyB Settore Card_Holder (6D 69 63 72 6F 68)
	MF_DataExch.WrBuff[11] 	= 0x69;
	MF_DataExch.WrBuff[12] 	= 0x63;
	MF_DataExch.WrBuff[13] 	= 0x72;
	MF_DataExch.WrBuff[14] 	= 0x6F;
	MF_DataExch.WrBuff[15] 	= 0x68;

	MF_DataExch.BlockNum[0] = Trailer;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	WriteSector();
	}
	else {
		Retry_Card_Activation();								
	}
	
	
	/* ***************  Settore  2  -  INFOSECTOR   *************************/

	ClrWrBuff();												// Azzero i 48 bytes di WrBuff
	MF_DataExch.WrBuff[4] 	= Utente>>24;						// Codice Utente (MSB)
	MF_DataExch.WrBuff[5] 	= Utente>>16;						// Codice Utente
	MF_DataExch.WrBuff[6] 	= Utente>>8;						// Codice Utente
	MF_DataExch.WrBuff[7] 	= Utente;							// Codice Utente (LSB)
	MF_DataExch.WrBuff[8] 	= KR_CHIAVE_UTENTE;					// Tipo Carta
	SetCRC16Buff(&MF_DataExch.WrBuff[0], MIF_BLOCKLEN-2, 0);
	MF_DataExch.WrBuff[14] = gVars.CRC16;						// CRC16 LSB
	MF_DataExch.WrBuff[15] = (gVars.CRC16)>>8;					// CRC16 MSB

	MF_DataExch.WrBuff[16] 	= 0x0F;
	MF_DataExch.WrBuff[17] 	= RBT_Secure;
	MF_DataExch.WrBuff[18] 	= 0x03;
	MF_DataExch.WrBuff[19] 	= PrepagataCreditSct;				// Credit Sector con Taglio
	MF_DataExch.WrBuff[30] 	= 0x30;
	MF_DataExch.WrBuff[31] 	= 0x99;

	// ***  Trailer  Settore 2  *****
	MF_DataExch.WrBuff[32] 	= 0x2B;								// KeyA lettura settori (2B 67 56 CE 59 A6)
	MF_DataExch.WrBuff[33] 	= 0x67;
	MF_DataExch.WrBuff[34] 	= 0x56;
	MF_DataExch.WrBuff[35] 	= 0xCE;
	MF_DataExch.WrBuff[36] 	= 0x59;
	MF_DataExch.WrBuff[37] 	= 0xA6;
	MF_DataExch.WrBuff[38] 	= 0x78;								// Access bits
	MF_DataExch.WrBuff[39] 	= 0x77;
	MF_DataExch.WrBuff[40] 	= 0x88;
	MF_DataExch.WrBuff[41] 	= Verg_Prep_InfoSct;				// GPB carta vergine prepagata
	MF_DataExch.WrBuff[42] 	= 0xC7;								// KeyB per tutti i settori carte vergini RBT (C7 14 D5 0A E3 06)
	MF_DataExch.WrBuff[43] 	= 0x14;
	MF_DataExch.WrBuff[44] 	= 0xD5;
	MF_DataExch.WrBuff[45] 	= 0x0A;
	MF_DataExch.WrBuff[46] 	= 0xE3;
	MF_DataExch.WrBuff[47] 	= 0x06;

	MF_DataExch.SectNum = 2;									// Settore 2 InfoSector
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	WriteSector();												// Scrittura settore
	if (response != Resp_OK) return response;

	/* ***************  Settore  3  -  CREDITO   ****************************/
	// ***  Blocco 0 Settore 3 : scrivo 10,00 Euro *****
	ClrWrBuff();												// Azzero i 48 bytes di WrBuff anche se ne uso 16
	MF_DataExch.WrBuff[0] 	= 0xE8;								// KeyA lettura settori (2B 67 56 CE 59 A6)
	MF_DataExch.WrBuff[1] 	= 0x03;
	MF_DataExch.WrBuff[14] 	= 0xE3;
	MF_DataExch.WrBuff[15] 	= 0x06;

	MF_DataExch.SectNum = 3;									// Settore 3 CreditSector
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();									// Scrittura settore
	if (response != Resp_OK) return response;

	// ***  Trailer  Settore 3  *****
	ClrWrBuff();												// Azzero i 48 bytes di WrBuff anche se ne uso 16
	MF_DataExch.WrBuff[0] 	= 0x2B;								// KeyA lettura settori (2B 67 56 CE 59 A6)
	MF_DataExch.WrBuff[1] 	= 0x67;
	MF_DataExch.WrBuff[2] 	= 0x56;
	MF_DataExch.WrBuff[3] 	= 0xCE;
	MF_DataExch.WrBuff[4] 	= 0x59;
	MF_DataExch.WrBuff[5] 	= 0xA6;
	MF_DataExch.WrBuff[6] 	= 0x78;								// Access bits
	MF_DataExch.WrBuff[7] 	= 0x77;
	MF_DataExch.WrBuff[8] 	= 0x88;
	MF_DataExch.WrBuff[9] 	= PrepagataCreditSct;				// GPB Settore Credito Attivo
	MF_DataExch.WrBuff[10] 	= 0xC7;								// KeyB per tutti i settori carte vergini RBT (C7 14 D5 0A E3 06)
	MF_DataExch.WrBuff[11] 	= 0x14;
	MF_DataExch.WrBuff[12] 	= 0xD5;
	MF_DataExch.WrBuff[13] 	= 0x0A;
	MF_DataExch.WrBuff[14] 	= 0xE3;
	MF_DataExch.WrBuff[15] 	= 0x06;
	MF_DataExch.SectNum = 3;									// Settore 3 CreditSector
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Trailer;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();									// Scrittura settore
	if (response != Resp_OK) return response;

	/* ***************  Settori  LIBERI  DAL  4 AL 14  ***********************/
	//	Dei settori liberi cambio solo il Trailer con la KeyB da "FFFFFFFFFFFF"
	//	a FreeSector. Se la KeyB non e' tutti "FF" tento con la KeyB per ogni
	//	settore perche' potrebbe essere una carta gia' utilizzata per lo sviluppo.

	MF_DataExch.WrBuff[9] 	= 0;													// GPB = 0
	for (r=4; r<=14; r++) {
		SetKeyA(&KeyA_VergCostr[0]);												// Set KeyA = 0xFFFFFFFFFFFF per accesso in scrittura ai settori
		MF_DataExch.SectNum = r;													// Settore in aggiornamento
		HEXL = r;																	// Stato formattazione: riporta il settore sul quale si opera
		response = WriteSector();													// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response = FailWriteFreeSect);			// Carta Estratta o non piu' accessibile
			CalcKeyB_FreeSectors(MF_DataExch.SectNum);							// Tento scrittura con KeyB FreeSector
			SetKeyB(&KeyB[0]);
			response = WriteSector();											// Scrittura settore
			if (response != Resp_OK) {
				response = Retry_Card_Activation();
				if (response != Resp_OK) return (response = FailWriteFreeSect);	// Carta Estratta o non piu' accessibile
				SetKeyB(&VergineRBTKeyB[0]);									// Tento scrittura con vecchia KeyB fissa per i FreeSector
				response = WriteSector();										// Scrittura settore
				if (response != Resp_OK) return (response = FailWriteFreeSect);	// Carta Estratta o non piu' accessibile
			}
		}
	}

	/* ***************  Settore 15  -  SICUREZZA ****************************/
	// Se DebugFormatCard=1 sono in debug e non ho modificato settori
	//  precedenti, per cui lascio invariata KeyA = tutti FF
	if (!DebugFormatCard) {
		SetKeyA(&StdKeyA[0]);					// Set KEYA standard per Autenticare il Settore 0
	}

	MF_DataExch.SectNum = 0;					// Settore 0
	MF_DataExch.BlockNum[0] = Block0;			// Blocco da leggere del settore SectNum
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;

	response = MifareReaderRead(&MF_DataExch.RdBuff[0], MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
	if (response != Resp_OK) {
		// Read Fail: Ritento attivazione
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);			// Riattivazione fallita: REJECT CARD
		response = MifareReaderRead(&MF_DataExch.RdBuff[0], MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
		if (response != Resp_OK) return (response);			// Lettura fallita: Settore 0 non accessibile
	}
	ClrWrBuff();											// Azzero i 48 bytes di WrBuff
	// Crypto i primi 8 bytes del Blocco 0 in cryptmsg
	cryptmsg[0] = ( MF_DataExch.RdBuff[0] | (MF_DataExch.RdBuff[1]<<8) | (MF_DataExch.RdBuff[2]<<16) | (MF_DataExch.RdBuff[3]<<24) );
	cryptmsg[1] = ( MF_DataExch.RdBuff[4] | (MF_DataExch.RdBuff[5]<<8) | (MF_DataExch.RdBuff[6]<<16) | (MF_DataExch.RdBuff[7]<<24) );
	encipher(cryptmsg, k);
	MF_DataExch.WrBuff[0] = cryptmsg[0];
	MF_DataExch.WrBuff[1] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[2] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[3] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[4] = cryptmsg[1];
	MF_DataExch.WrBuff[5] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[6] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[7] = cryptmsg[1]>>24;

	// Crypto i secondi 8 bytes del Blocco 0 dopo aver eseguito l'Xor con i primi 8 bytes criptati
	EorCalc[0] = ( MF_DataExch.RdBuff[8] | (MF_DataExch.RdBuff[9]<<8) | (MF_DataExch.RdBuff[10]<<16) | (MF_DataExch.RdBuff[11]<<24) );
	EorCalc[1] = ( MF_DataExch.RdBuff[12] | (MF_DataExch.RdBuff[13]<<8) | (MF_DataExch.RdBuff[14]<<16) | (MF_DataExch.RdBuff[15]<<24) );
	cryptmsg[0] ^= EorCalc[0];
	cryptmsg[1] ^= EorCalc[1];
	encipher(cryptmsg, k);
	MF_DataExch.WrBuff[8] = cryptmsg[0];
	MF_DataExch.WrBuff[9] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[10] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[11] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[12] = cryptmsg[1];
	MF_DataExch.WrBuff[13] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[14] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[15] = cryptmsg[1]>>24;
	
	// Calcolo checksum dell'intero Blocco 0 criptato
	SetCRC16Buff(&MF_DataExch.WrBuff[0], MIF_BLOCKLEN, SemeCrypto);
	MF_DataExch.WrBuff[16] = gVars.CRC16;						// CRC16 LSB
	MF_DataExch.WrBuff[17] = (gVars.CRC16)>>8;					// CRC16 MSB

	// Preparo Trailer Settore Sicurezza
	MF_DataExch.WrBuff[32] 	= 0x13;								// KeyA Settore Sicurezza (13 A6 5E C4 29 82)
	MF_DataExch.WrBuff[33] 	= 0xA6;
	MF_DataExch.WrBuff[34] 	= 0x5E;
	MF_DataExch.WrBuff[35] 	= 0xC4;
	MF_DataExch.WrBuff[36] 	= 0x29;
	MF_DataExch.WrBuff[37] 	= 0x82;
	MF_DataExch.WrBuff[38] 	= 0x78;								// Access bits (78 7A F8)
	MF_DataExch.WrBuff[39] 	= 0x77;
	MF_DataExch.WrBuff[40] 	= 0x88;
	MF_DataExch.WrBuff[41] 	= 0x33;								// GPB Settore Sicurezza
	MF_DataExch.WrBuff[42] 	= 0x3A;								// KeyB Settore Sicurezza (3A F7 51 8C 40 DB)
	MF_DataExch.WrBuff[43] 	= 0xF7;
	MF_DataExch.WrBuff[44] 	= 0x51;
	MF_DataExch.WrBuff[45] 	= 0x8C;
	MF_DataExch.WrBuff[46] 	= 0x40;
	MF_DataExch.WrBuff[47] 	= 0xDB;
	
	// Preparo KeyA di scrittura Settore Sicurezza
	MF_DataExch.KeyType = false;  								// Set KeyA = 0xFFFFFFFFFFFF per scrivere il Settore sicurezza
	for (r=0; r<6; r++) {
		MF_DataExch.KeyVal[r] = 0xFF;
	}
	MF_DataExch.SectNum = 15;									// Settore 15: Settore Sicurezza
	HEXL = MF_DataExch.SectNum;									// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();									// Scrittura settore
	return response;
}

/* ********************************************************************************************************************************* */
/* ********************************************************************************************************************************* */
/* ********************************************************************************************************************************* */
/*---------------------------------------------------------------------------------------------*\
Method: DaUtenteACostruttore
	Trasforma una carta Utente in Vergine da Costruttore.
	Si scrivono i settori con la KeyB propria della carta mettendo a zero tutti i bytes.
	Le carte escono con trailer 'FFFFFFFFFFFFFF778069FFFFFFFFFFFF'.
	In alcune carte possono essere stati utilizzati anche i settori 4 e 5 per la gestione delle
	selezioni: tento la scrittura con la KeyB, se non funziona considero i settori come liberi. 
	Funzione di servizio per ripristinare le carte durante lo sviluppo.
	 
	IN:		- carta utente MicroHard
	OUT:	- Response = Resp_OK se la scrittura di tutti i settori e' OK
\*----------------------------------------------------------------------------------------------*/

uint16_t	DaUtenteACostruttore(void) {
	uint8_t	i, Sect4, Sect5;

	ClrWrBuff();														// Azzera 48 bytes di WrBuff
	
	// **********   Azzera Dati Settore  1  *****************
	SetKeyB(&CardHolderKeyB[0]);										// KeyB per Card Holder
	MF_DataExch.SectNum = 1;											// Settore 1
	HEXL = MF_DataExch.SectNum;											// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura blocchi 0, 1 e 2 del settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
	}
	
	CalcKeyB(Pin1, Pin2);												// Set KeyB della carta
	
	// **********   Azzera Dati Settore  2  *****************
	SetKeyB(&KeyB[0]);													// Predispongo l'uso della KeyB
	MF_DataExch.SectNum = 2;											// Settore 2
	HEXL = MF_DataExch.SectNum;											// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura blocchi 0, 1 e 2 del settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&VergineRBTKeyB[0]);									// Tento scrittura con vecchia KeyB fissa
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) return (response);						// KeyB settore 2 NON trovata
	}
	
	// **********   Azzera Dati  Settore  3  *****************
	SetKeyB(&KeyB[0]);													// Predispongo l'uso della KeyB
	MF_DataExch.SectNum = 3;											// Settore 3
	HEXL = MF_DataExch.SectNum;											// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura blocchi 0, 1 e 2 del settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&VergineRBTKeyB[0]);									// Tento scrittura con vecchia KeyB fissa
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) return (response);						// KeyB settore 3 NON trovata
	}
	
	// **********   Azzera Dati  Settore  4  *****************
	Sect4 = false;														// Flag Settore 4
	SetKeyB(&KeyB[0]);													// Predispongo l'uso della KeyB
	MF_DataExch.SectNum = 4;											// Settore 4
	HEXL = MF_DataExch.SectNum;											// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura blocchi 0, 1 e 2 del settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&VergineRBTKeyB[0]);									// Tento scrittura con vecchia KeyB fissa
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response);					// Carta Estratta o non piu' accessibile
			Sect4 = true;												// KeyB settore 4 NON trovata: probabile settore FREE su questa carta
		}
	}
	
	// **********   Azzera Dati  Settore  5  *****************
	Sect5 = false;														// Flag Settore 5
	SetKeyB(&KeyB[0]);													// Predispongo l'uso della KeyB
	MF_DataExch.SectNum = 5;											// Settore 5
	HEXL = MF_DataExch.SectNum;											// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = Block2;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura blocchi 0, 1 e 2 del settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&VergineRBTKeyB[0]);									// Tento scrittura con vecchia KeyB fissa
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response);					// Carta Estratta o non piu' accessibile
			Sect5 = true;												// KeyB settore 5 NON trovata: probabile settore FREE su questa carta
		}
	}
	
	// **********   Azzera Dati Settore  0  ****************
	SetKeyB(&KeyBWriteMAD[0]);											// Set KEYB per modificare il Settore 0 (MAD)
	MF_DataExch.SectNum = 0;											// Settore 0
	HEXL = MF_DataExch.SectNum;											// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block1;
	MF_DataExch.BlockNum[1] = Block2;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura blocchi 1 e 2 del settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&KeyA_VergCostr[0]);									// Set KEYB con il valore delle carte vergini da costruttore (0xFFFFFFFFFFFF)
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response);					// Carta Estratta o non piu' accessibile
		}
	}
	
	// **********   Azzera Dati Settore  Sicurezza  ****************
	SetKeyB(&KeyBWriteSicur[0]);										// Set KEYB per modificare il Settore 15 (Sicurezza)
	MF_DataExch.SectNum = 15;											// Settore 15
	HEXL = MF_DataExch.SectNum;											// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = Block0;
	MF_DataExch.BlockNum[1] = Block1;
	MF_DataExch.BlockNum[2] = NoBlock;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura blocchi 1 e 2 del settore
	if (response != Resp_OK) {											// Se fail ma la riattivazione funziona potrebbe essere una carta
		response = Retry_Card_Activation();								// che non aveva la sicurezza: controllo se la KeyB e' gia' stata aggiornata
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&KeyA_VergCostr[0]);									// Set KEYB con il valore delle carte vergini da costruttore (0xFFFFFFFFFFFF)
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response);					// Carta Estratta o non piu' accessibile
		}
	}
	
	
	// **********   Set Trailer dei Settori  ****************

	MF_DataExch.WrBuff[32] 	= 0xFF;										// KeyA carte vergini
	MF_DataExch.WrBuff[33] 	= 0xFF;
	MF_DataExch.WrBuff[34] 	= 0xFF;
	MF_DataExch.WrBuff[35] 	= 0xFF;
	MF_DataExch.WrBuff[36] 	= 0xFF;
	MF_DataExch.WrBuff[37] 	= 0xFF;
	MF_DataExch.WrBuff[38] 	= 0xFF;										// Access bits
	MF_DataExch.WrBuff[39] 	= 0x07;
	MF_DataExch.WrBuff[40] 	= 0x80;
	MF_DataExch.WrBuff[41] 	= 0x69;										// GPB carta vergine
	MF_DataExch.WrBuff[42] 	= 0xFF;										// KeyB carte vergini
	MF_DataExch.WrBuff[43] 	= 0xFF;
	MF_DataExch.WrBuff[44] 	= 0xFF;
	MF_DataExch.WrBuff[45] 	= 0xFF;
	MF_DataExch.WrBuff[46] 	= 0xFF;
	MF_DataExch.WrBuff[47] 	= 0xFF;

	// **********   Format Trailer Settore  Sicurezza 15  ****************
	
	MF_DataExch.SectNum = Secure_Sector;
	HEXL = (MF_DataExch.SectNum + 0x10);								// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = NoBlock;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura settore
	if (response != Resp_OK) {											// Se fail ma la riattivazione funziona potrebbe essere una carta
		response = Retry_Card_Activation();								// che non aveva la sicurezza
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&KeyA_VergCostr[0]);									// Set KEYB con il valore delle carte vergini da costruttore (0xFFFFFFFFFFFF)
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
	}

	// **********   Format Trailer Settore  0  ****************
	
	SetKeyB(&KeyBWriteMAD[0]);											// Ripristino KEYB per modificare il Settore 0 (MAD)
	MF_DataExch.SectNum = 0;											// Settore 0
	HEXL = (MF_DataExch.SectNum + 0x10);								// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = NoBlock;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura settore
	if (response != Resp_OK) {											// Se fail ma la riattivazione funziona la KeyB potrebbe essere 
		response = Retry_Card_Activation();								// gia' stata programmata: tento con KeyB carte vergini
		SetKeyB(&KeyA_VergCostr[0]);									// Set KEYB con il valore delle carte vergini da costruttore (0xFFFFFFFFFFFF)
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
	}

	// **********   Format Trailer Settore  1  ****************
	
	SetKeyB(&KeyBWriteCardHolder[0]);									// Ripristino KEYB per modificare il Settore 1 (Card Holder)
	MF_DataExch.SectNum = 1;											// Settore 1
	HEXL = (MF_DataExch.SectNum + 0x10);								// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = NoBlock;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura settore
	if (response != Resp_OK) {											// Se fail ma la riattivazione funziona la KeyB potrebbe essere 
		response = Retry_Card_Activation();								// gia' stata programmata: tento con KeyB carte vergini
		SetKeyB(&KeyA_VergCostr[0]);									// Set KEYB con il valore delle carte vergini da costruttore (0xFFFFFFFFFFFF)
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
	}

	// **********   Format Trailer Settore  2  ****************
	
	SetKeyB(&KeyB[0]);													// Predispongo l'uso della KeyB
	MF_DataExch.SectNum = InfoSectorNum;
	HEXL = (MF_DataExch.SectNum + 0x10);								// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = NoBlock;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&VergineRBTKeyB[0]);									// Tento scrittura con vecchia KeyB fissa
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) return (response);						// KeyB settore 2 NON trovata
	}

	// **********   Format Trailer Settore  3  ****************
	
	SetKeyB(&KeyB[0]);													// Predispongo l'uso della KeyB
	MF_DataExch.SectNum = MHDCreditSector;
	HEXL = (MF_DataExch.SectNum + 0x10);								// Stato formattazione: riporta il settore sul quale si opera
	MF_DataExch.BlockNum[0] = NoBlock;
	MF_DataExch.BlockNum[1] = NoBlock;
	MF_DataExch.BlockNum[2] = Trailer;
	MF_DataExch.BlockNum[3] = NoBlock;
	response = WriteSector();											// Scrittura settore
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Carta Estratta o non piu' accessibile
		SetKeyB(&VergineRBTKeyB[0]);									// Tento scrittura con vecchia KeyB fissa
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) return (response);						// KeyB settore 3 NON trovata
	}

	// **********   Format Trailer Settore  4  ****************
	
	if (Sect4 == false) {
		SetKeyB(&KeyB[0]);												// Predispongo l'uso della KeyB
		MF_DataExch.SectNum = SelGest1_Sector;
		HEXL = (MF_DataExch.SectNum + 0x10);							// Stato formattazione: riporta il settore sul quale si opera
		MF_DataExch.BlockNum[0] = NoBlock;
		MF_DataExch.BlockNum[1] = NoBlock;
		MF_DataExch.BlockNum[2] = Trailer;
		MF_DataExch.BlockNum[3] = NoBlock;
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response);					// Carta Estratta o non piu' accessibile
			SetKeyB(&VergineRBTKeyB[0]);								// Tento scrittura con vecchia KeyB fissa
			response = WriteSector();									// Scrittura settore
			if (response != Resp_OK) return (response);					// KeyB settore 4 NON trovata
		}
	}

	// **********   Format Trailer Settore  5  ****************
	
	if (Sect5 == false) {
		SetKeyB(&KeyB[0]);												// Predispongo l'uso della KeyB
		MF_DataExch.SectNum = SelGest2_Sector;
		HEXL = (MF_DataExch.SectNum + 0x10);							// Stato formattazione: riporta il settore sul quale si opera
		MF_DataExch.BlockNum[0] = NoBlock;
		MF_DataExch.BlockNum[1] = NoBlock;
		MF_DataExch.BlockNum[2] = Trailer;
		MF_DataExch.BlockNum[3] = NoBlock;
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response);					// Carta Estratta o non piu' accessibile
			SetKeyB(&VergineRBTKeyB[0]);								// Tento scrittura con vecchia KeyB fissa
			response = WriteSector();									// Scrittura settore
			if (response != Resp_OK) return (response);					// KeyB settore 5 NON trovata
		}
	}

	// **********   Format Trailers Settori  4-6 - 14  ****************
	// Ogni settore ha la propria KeyB pero' potrebbero esserci carte
	// con KeyB fissa perche' formattate con revisioni vecchie.

	i = 6;
	if (Sect4 && Sect5) {												// Se Sect4 e Sect5 sono true significa che il 4 e il 5 erano settori free
		i = 4;
	}
	for (; i <= 14; i++) {
		MF_DataExch.SectNum = i;										// Settore in aggiornamento
		HEXL = (MF_DataExch.SectNum + 0x20);							// Stato formattazione: riporta il settore sul quale si opera
		CalcKeyB_FreeSectors(MF_DataExch.SectNum);
		SetKeyB(&KeyB[0]);
		response = WriteSector();										// Scrittura settore
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return (response);					// Carta Estratta o non piu' accessibile
			SetKeyB(&VergineRBTKeyB[0]);								// Tento scrittura con vecchia KeyB fissa per i FreeSector
			response = WriteSector();									// Scrittura settore
			if (response != Resp_OK) return (response);					// KeyB settore Free NON trovata
		}
	}
	return response;
}

/*---------------------------------------------------------------------------------------------*\
Method: CalcolaDatiCarta
	Legge il s/n e calcola il Codice Sicurezza 
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
uint16_t  CalcolaDatiCarta(void) {

	uint32_t	EorCalc[2];

	SetKeyA(&StdKeyA[0]);												// Set KEYA standard per Autenticare il Settore 0 che contiene il MAD
	MF_DataExch.SectNum = 0;											// Settore 0 che dovrebbe contenere il MAD
	MF_DataExch.BlockNum[0] = Block0;									// Blocco da leggere del settore SectNum
	MF_DataExch.BlockNum[1] = NoBlock;			
	MF_DataExch.BlockNum[2] = NoBlock;			
	MF_DataExch.BlockNum[3] = NoBlock;			
	response = MifareReaderRead(&MF_DataExch.RdBuff[0], MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
	if (response != Resp_OK) {
		response = Retry_Card_Activation();
		if (response != Resp_OK) return (response);						// Riattivazione fallita: REJECT CARD
		response = MifareReaderRead(&MF_DataExch.RdBuff[0], MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum); 
		if (response != Resp_OK) return (response);					// Letture fallite: REJECT CARD
		
	}
	ClrWrBuff();														// Azzero i 48 bytes di WrBuff
	// Crypto i primi 8 bytes del Blocco 0 in cryptmsg
	cryptmsg[0] = ( MF_DataExch.RdBuff[0] | (MF_DataExch.RdBuff[1]<<8) | (MF_DataExch.RdBuff[2]<<16) | (MF_DataExch.RdBuff[3]<<24) );
	cryptmsg[1] = ( MF_DataExch.RdBuff[4] | (MF_DataExch.RdBuff[5]<<8) | (MF_DataExch.RdBuff[6]<<16) | (MF_DataExch.RdBuff[7]<<24) );
	encipher(cryptmsg, k);
	MF_DataExch.WrBuff[0] = cryptmsg[0];
	MF_DataExch.WrBuff[1] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[2] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[3] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[4] = cryptmsg[1];
	MF_DataExch.WrBuff[5] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[6] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[7] = cryptmsg[1]>>24;

	// Crypto i secondi 8 bytes del Blocco 0 dopo aver eseguito l'Xor con i primi 8 bytes criptati
	EorCalc[0] = ( MF_DataExch.RdBuff[8] | (MF_DataExch.RdBuff[9]<<8) | (MF_DataExch.RdBuff[10]<<16) | (MF_DataExch.RdBuff[11]<<24) );
	EorCalc[1] = ( MF_DataExch.RdBuff[12] | (MF_DataExch.RdBuff[13]<<8) | (MF_DataExch.RdBuff[14]<<16) | (MF_DataExch.RdBuff[15]<<24) );
	cryptmsg[0] ^= EorCalc[0];
	cryptmsg[1] ^= EorCalc[1];
	encipher(cryptmsg, k);
	MF_DataExch.WrBuff[8] = cryptmsg[0];
	MF_DataExch.WrBuff[9] = cryptmsg[0]>>8;
	MF_DataExch.WrBuff[10] = cryptmsg[0]>>16;
	MF_DataExch.WrBuff[11] = cryptmsg[0]>>24;
	MF_DataExch.WrBuff[12] = cryptmsg[1];
	MF_DataExch.WrBuff[13] = cryptmsg[1]>>8;
	MF_DataExch.WrBuff[14] = cryptmsg[1]>>16;
	MF_DataExch.WrBuff[15] = cryptmsg[1]>>24;

	// Calcolo checksum dell'intero Blocco 0 criptato
	SetCRC16Buff(&MF_DataExch.WrBuff[0], MIF_BLOCKLEN, SemeCrypto);
	MF_DataExch.WrBuff[16] = gVars.CRC16;								// CRC16 LSB
	MF_DataExch.WrBuff[17] = (gVars.CRC16)>>8;							// CRC16 MSB
	return (response);
	
}	

/*---------------------------------------------------------------------------------------------*\
Method: CalcKeyB_FreeSectors
	Calcola la KeyB del settore libero.
 
	IN:		- numero del settore del quale calcolare la KeyB e UID della carta in "Snum_media_in"
	OUT:	- KeyB[]
\*----------------------------------------------------------------------------------------------*/
void	CalcKeyB_FreeSectors(uint8_t numSector) {
	uint8_t	temp;
	
	temp = numSector ^ 0xFF;
	KeyB[0] = Snum_media_in[0] ^ temp;
	KeyB[1] = Snum_media_in[1] ^ temp;
	KeyB[2] = Snum_media_in[2] ^ temp;
	KeyB[3] = Snum_media_in[3] ^ temp;
	KeyB[4] = temp;
	KeyB[5] = numSector;
}

/* ======================================================*/
/* Scrive i blocchi indicati in MF_DataExch.BlockNum.
   Tenta fino a due volte la scrittura.
   Esce con response=Resp_OK oppure con codice di errore */

uint16_t	WriteSector(void) {

	if (DebugFormatCard) {
		return (response = Resp_OK);
	} else {
		response = MifareWriteAndCompare((uint8_t*)&MF_DataExch.WrBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum, false);
		// Ritento scrittura se fallita
		if (response != Resp_OK) {
			response = Retry_Card_Activation();
			if (response != Resp_OK) return response;
			response = MifareWriteAndCompare((uint8_t*)&MF_DataExch.WrBuff, MF_DataExch.SectNum, (uint8_t*)&MF_DataExch.BlockNum, false);
			if (response != Resp_OK) return response;
		}
		return response;
	}
}

/* ======================================================*/
void SetFormatType(void) {
	if ((FormatType & 0xF0) == 0) {
		FormatType++;
		FormatType = FormatType | 0xF0;
	}
}

void CheckIfNMIPending(void) {
	FormatType = FormatType & 0x0F;
}

void ClrWrBuff(void) {
	uint8_t s;
	for (s=0; s<48; s++) {					
		MF_DataExch.WrBuff[s] =	0x00;
	}
}
#endif		// Fine SetVergineRBT

#if (SetVergineRBT == true)
/*------------------------------------------------------------------------------------------*\
Method: encipher
  Cifra un messaggio di 64 bit (8 Bytes) con la chiave k di 128 bits (16 Bytes).
  La funzione dura circa 22,5 uSec @ 50MHz.
  IN:  - pointer al messaggio da cifrare e pointer alla chiave di cifratura.
 OUT:  - messaggio cifrato
\*------------------------------------------------------------------------------------------*/
void encipher(uint32_t* cryptmsg, const uint32_t* k) {
	
    uint8_t		num_rounds = 32;
    uint32_t	v0=cryptmsg[0], v1=cryptmsg[1], i;
    uint32_t	sum=0, delta=0x9E3779B9;

    for(i=0; i<num_rounds; i++) {
       v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + k[sum & 3]);
        sum += delta;
        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + k[(sum>>11) & 3]);
    }
    cryptmsg[0]=v0; cryptmsg[1]=v1;
}


#endif	// End (PCLink == TRUE)

