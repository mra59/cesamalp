/****************************************************************************************
File:
    icpUSD.h

Description:
    Include file protocollo MDB per Universal Satellite Device

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __icpUSD__
#define __icpUSD__

#include "Icp.h"


/*--------------------------------------------------------------------------------------*\
Types and constants
\*--------------------------------------------------------------------------------------*/

// Feature options
typedef enum  {
	ICP_USDOPT_NONE = 0x00,
	ICP_USDOPT_USD1 = 0x01,
	ICP_USDOPT_USD2 = 0x02,
	ICP_USDOPT_FTL = 0x04
} ET_USDFOPTIONS;

// Status events
typedef enum {
	ICP_USD_JUSTRESET = 0,
	ICP_USD_VENDREQ = 1,
	ICP_USD_VENDSUCC = 2,
	ICP_USD_VENDFAILED = 3,
	ICP_USD_SETUP = 4,
	ICP_USD_PRICEREQ = 5,
	ICP_USD_ERRCODE = 6,
	ICP_USD_PERIPHID = 7,
	ICP_USD_SELSTATUS = 8,
	ICP_USD_BLOCKN = 9,
	ICP_USD_BLOCKDATA = 10,
	ICP_USD_FTLREQTORCV = 0x1B,
	ICP_USD_FTLRETRYDENY= 0x1C,
	ICP_USD_FTLSENDBLOCK = 0x1D,
	ICP_USD_FTLOKTOSEND = 0x1E,
	ICP_USD_FTLREQTOSEND = 0x1F,
	ICP_USD_DIAG = 0xFF
} ET_USDPOLLSTATUS;

// Status events
typedef enum {
	ICP_USD_HEALTHSAFETYVIOLATION = 0x0001,
	ICP_USD_CHUTESENSORFAILURE = 0x0002,
	ICP_USD_KEYPADSWITCHFAILURE = 0x0004
} ET_USDSTATUS;

typedef enum  {
	ICP_USDINFO_NONE = 0,
	ICP_USDINFO_JUSTRESET = 0x0001,
	ICP_USDINFO_VENDREQ = 0x0002,
	ICP_USDINFO_VENDSUCC = 0x0004,
	ICP_USDINFO_VENDFAILED = 0x0008,
	ICP_USDINFO_PRICEREQ = 0x0010,
	ICP_USDINFO_ERRCODE = 0x0020,
	ICP_USDINFO_SELSTATUS = 0x0040
} ET_USDINFO;


/*--------------------------------------------------------------------------------------*\
Interface routines
\*--------------------------------------------------------------------------------------*/

void ICPProt_USDSetEvtMask(uint8_t currUSD, uint8_t nEvtMask);
	// Called to signal reader events to ICP protocol, see ET_USDINFO



//void ICPUSDHook_VendApproved(uint8_t usdType);
	// Response from VMC: approved the previous Vend Request

//void ICPUSDHook_VendDenied(uint8_t usdType);
	// Response from VMC: disapproved the previous Vend Request




bool ICPUSDHook_IsInitCompleted(void);
	// Return true when initialisation is completed

bool IsUSD1Connected(void);
	// Called to know if USD connected

uint32_t Get_USD1_CreditoAttuale(void);
	// Called to know USD credit display
		
void Set_USD1_NewCredit(uint32_t NuovoCredito);
	// Called to set new USD credit display

#endif
