/**
  ******************************************************************************
  * @file    file_operations.h
  * @author  Abe - (C) COPYRIGHT AbTech september 2019
  * @brief   Baltom USB file functions headers 
  ******************************************************************************
 */

#ifndef __FILE_OPERATION_H__
#define __FILE_OPERATION_H__


// =================================================================================================

#define _INIT_                  0
#define RTC_INIT                0
#define FIXED_NAME              0
#define	NO_CLEAR_LOG			0

#define	NEW_AWS_SECURE_CERTIF_BIT		0x01
#define	NEW_AWS_PRIVATE_KEY_BIT			0x02
#define	NEW_AWS_DEVICE_BIT				0x04
#define	NEW_AWS_ACCOUNT_SERVER_ADDR_BIT	0x08

#define BUFFER_LENGTH		(EE_RxAWSConfigBuff_Len / 2 )		// 2K bytes
#define TX_AWS_BUFFER_SIZE	(1024)								// 1K bytes

#define LED_ROSSO			0
#define LED_VERDE			1

#define	AUDIT_LIST			1

typedef enum {
	USB_ERR_OK        	= 0,	// 0
	ERR_NO_CONFIG_FILE,			// 1
	ERR_RW_FILE,				// 2
	ERR_NO_DIR,					// 3
	ERR_PSW,					// 4
	ERR_FULL_DIR,				// 5
	ERR_AUDIT_DENIED,			// 6
	ERR_NO_DATA_LOG,			// 7
	ERR_EE_WRITE,				// 8
	USB_AWS_ERR_OK        		// 9
}USB_Key_Err;


typedef enum{
	FILENAME_COMPARE_OK = 0,
	FILENAME_FAIL = -1
}CompFileName;

typedef enum {
	NO_FILE_TYPE       			 = 0,
	FILE_TYPE_CERTIFICATE,		// 1
	FILE_TYPE_PRIVATE_KEY,		// 2
	FILE_TYPE_DEVICE,			// 3
	FILE_TYPE_ACCOUNTSERVADDR	// 4
}TipoFile;


/* Private function prototypes -----------------------------------------------*/

static bool		EE_BlockMngr(uint8_t *arr, uint32_t size);
static void 	SetLed(uint32_t output, bool state);
USB_Key_Err 	Import_AWS_Conf(void);
uint16_t  		GetMessageFromEE(uint16_t * Addr_EEP, uint8_t *DestBuf);
void  			Check_New_AWS_Config(uint8_t* resp, uint8_t* Quantity);

#endif  //__FILE_OPERATION_H__