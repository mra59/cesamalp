/****************************************************************************************
File:
    VMC_Testing.c

Description:
    Funzioni di collaudo CPU PK4.
    

History:
    Date       Aut  Note
    Mar 2022 	MR   

*****************************************************************************************/

#include <string.h>
#include <stdio.h>

#include "main.h"
#include "ORION.H"
#include "IICEEP.h"
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"
#include "VMC_Testing.h"
#include "Funzioni.h"
#include "VMC_CPU_HW.h"
#include "OrionTimers.h"
#include "VMC_Main.h"	
#include "phcsBflStatus.h"
#include "VMC_CPU_LCD.h"




/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void  Testing(void);
extern	void  CheckModePasvens(void);
extern	void  cifra(void);
extern	void  ICPHookCommOpen(void);
extern	void  Start_IrDA(void);
extern	void  Init_DDCMP_Protocol(void);
extern	void  Sel24_init(void);
extern  bool  IsModePasvens(void);
extern	bool  ConnessioneDDCMP_IrDA(void);
extern	void  DDCMPTask(void);
extern	void  Check_Clear_Audit_LR(void);
extern	bool  SetInvioReportCollaudo(void);
extern	bool  IsReportSent(void);
extern	uint8_t		CheckNewConfig(void);
extern TIM_HandleTypeDef 	htim1;
extern	phcsBfl_Status_t 	PN512_mif_init(uint32_t speed, uint8_t mask);
extern	void  VisMessage(uint8_t NumMess_1, uint8_t NumRigaMsg_1, uint8_t NumMess_2, uint8_t NumRigaMsg_2, bool FillSpace_All_LCD);
extern	void  VisualNum_2cifre(uint8_t Numero, uint8_t NumRiga);


extern	MAINTimes 	MainTouts;
extern	uint8_t		PAR_Out;

/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

bool	NewCPU, NewConfigFromKeyUSB;
	
/*---------------------------------------------------------------------*\
Private constants and types definition	
\*---------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------*\
Method:	Testing
	Controlla se CPU vergine: in caso negativo procede al suo collaudo e crea i codici
	di sicurezza al termine della funzione "Collaudo_Part_1".
	Se non sono presenti la scheda I/O e la Telemetria si ferma con errore.
	
Parameters:
	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
void Testing(void)
{

	bool		TestCodes;
	uint16_t	addrbuff;

	NewCPU = false;

	addrbuff = AlTur(Data[0]);
	memset(&gVars.ddcmpbuff[0], 0, 32u);
  	ReadEEPI2C(addrbuff, (uint8_t *)(&gVars.ddcmpbuff[0]), sizeof(AlTur));
  	TestCodes = Test_uP(&gVars.ddcmpbuff[0]);
	if (TestCodes == true) return;																// Collaudo non necessario perche' CPU non nuova e codici gia' presenti

	NewCPU = true;
	
	//-- Azzeramento  Memorie  EEProm  --
	LCD_Disp_AzzeramMemoria();
	BankSelect(0);																				// Select Bank0 eeprom 1
	GreenLedCPU(OFF);																			// Led Verde CPU OFF
	RedLedCPU(ON);																				// Led Rosso CPU ON
	AzzeraTuttaEEPROM();
	FillEEPI2C((uint16_t)BEGIN_DATA_CODES, 0, DATA_CODES_LEN);									// Azzero area codici 0x2A00 0x2B00
	AccPsw = 35279;																				// Password per file di configurazione chiave USB "Alp_35279.cfw"
	WriteEEPI2C(IICEEPConfigAddr(EEAccessPsw), (uint8_t *)(&AccPsw), sizeof(AccPsw));			// Password di Accesso
	RedLedCPU(OFF);																				// Led Rosso CPU OFF
	
	//-- Inizio Collaudo --
	LCD_Disp_CollaudoInCorso();
	HAL_Delay(OneSec);
}

/*--------------------------------------------------------------------------------------*\
Method:	Collaudo_Part_1
	Esegue il collaudo di CPU con I/O Pasvens.
	Se tutto OK invia il SN ad AWS e inserisce i codici di sicurezza.
	
Parameters:
	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
void Collaudo_Part_1(void)
{
	
	uint32_t	j;
	
		//--  Controllo  Tensione MIFARE --
		if (Check_MIF_Voltage() == false)
		{
			MIFARE_OFF();																		// Spengo PN512
			VisErrCollaudo(7u);																	// Fermo il collaudo con ERRORE 7
		}
		
		// ====  Legge SSID e PSW dalla chiave USB per attivare MH620 ===
		//-- Si alternano al lampeggio led verde e rosso  --
		
		LCD_Disp_ReadConfDaChiaveUSB();
		HAL_Delay(OneSec);
		
		MIFARE_OFF();																			// Spengo PN512
		NewConfigFromKeyUSB = false;
		TmrStart(MainTouts.MainTime, FREQ_1_LED);
		RedLedCPU(OFF);																			// Per alternarli
		GreenLedCPU(ON);
		while(CheckNewConfig() != UpdateCfg_OK)
		{
			do
			{
				GestioneUSB_Host_Device();
				if (TmrTimeout(MainTouts.MainTime))
				{
					TmrStart(MainTouts.MainTime, FREQ_1_LED);
					GreenLedCPU_Toggle();
					RedLedCPU_Toggle();
				}
			}while(NewConfigFromKeyUSB == false);
		}
		HAL_Delay(OneSec);
		
		// ========    Test IrDA locale       =========================
		//-- Lampeggiano verde e rosso fino al termine --
ReadIrDA_Audit:

		LCD_Disp_PrelievoIrdaLocale();
		MIFARE_OFF();																			// Tolgo +5V Mifare per usare l'IrDA locale
		MIFARE_COMM_DeInit();
		gVMC_ConfVars.PresMifare = false;
		RedLedCPU(ON);																			// Per parallelare i due led
		GreenLedCPU(ON);
		TmrStart(MainTouts.MainTime, FREQ_1_LED);
		Start_IrDA();
		Init_DDCMP_Protocol();
		j = 5;
		Buz_start(Milli50, HZ_2500, 0);															// Impulso di buzzer per segnalare che siamo in attesa del rilevamento Audit
		do
		{
			DDCMPTask();
			if (TmrTimeout(MainTouts.MainTime))
			{
				TmrStart(MainTouts.MainTime, FREQ_1_LED);
				GreenLedCPU_Toggle();
				RedLedCPU_Toggle();
				if (j-- == 0)
				{
					j = 5;
					Buz_start(Milli50, HZ_2500, 0);												// Impulso di buzzer per segnalare che siamo in attesa del rilevamento Audit
				}
			}
		}while(ConnessioneDDCMP_IrDA() == false);
		
		VisMessage(mMenuAudit, 1, 0, 0, true);
		GreenLedCPU(OFF);
		RedLedCPU(OFF);
		do
		{
			DDCMPTask();
		}while(ConnessioneDDCMP_IrDA() == true);
		if (EnableAuditClr == DDCMP_ENA_AUDIT_CLEAR)
		{
			Check_Clear_Audit_LR();																// Azzero tutta la memoria Audit
			AuditINClear();
			gOrionConfVars.AuditExt = true;														// Azzero anche contatori Audit Estesa
			AuditLRClear();
			gOrionConfVars.AuditExt = false;
		}
		else
		{
			goto ReadIrDA_Audit;
		}
		RedLedCPU(OFF);
		GreenLedCPU(OFF);
		HAL_Delay(OneSec);																		// Per dare tempo al display LED di ritornare al msg di stby
		
		//--   Test Mifare  --
		gVMC_ConfVars.PresMifare = true;
		MIFARE_COMM_Init();
		response = PN512_mif_init(MIFARE_BAUD_FAST, DDRPN);
		if (response != Resp_OK)
		{
			VisErrCollaudo(8u);																	// Fermo il collaudo con ERRORE 8
		}
		
		//-- Calcola Codici di sicurezza --
		BankSelect(0);																			// Select Bank0 eeprom 1
		FillEEPI2C((uint16_t)GP_CODES, 0, (EE_FREE1 - GP_CODES));								// Azzera Area Codici prima di inserire il codice di sicurezza
		cifra();
		AccPsw = 0;																				// Rimette a zero la Password USB
		WriteEEPI2C(IICEEPConfigAddr(EEAccessPsw), (uint8_t *)(&AccPsw), sizeof(AccPsw));		// Password di Accesso
		LCD_Disp_EndColl_OK();
		Buz_start(TwoSec, HZ_2000, HZ_1000);													// Collaudo OK - Buzzer bitonale
		MIFARE_OFF();
		MIFARE_COMM_DeInit();																	// Tolgo l'ulteriore tensione proveniente dai pin UART 

		TmrStart(MainTouts.MainTime, FREQ_1_LED);
		do
		{
			if (TmrTimeout(MainTouts.MainTime))
			{
				TmrStart(MainTouts.MainTime, FREQ_1_LED);
				GreenLedCPU_Toggle();
				RedLedCPU_Toggle();
				BlueLedCPU_Toggle();
				YellowLedCPU_Toggle();
			}
		}while(true);
}

/*--------------------------------------------------------------------------------------*\
Method:	VisErrCollaudo

	ERRORE  7: Controllo  Tensione MIFARE
	ERRORE  8: Test Mifare

	
Parameters:
	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
static void VisErrCollaudo(uint8_t ErrorNum)
{
	VisMessage(mErrCollaudo, 1, 0, 0, true);
	VisualNum_2cifre(ErrorNum, 1);
	MIFARE_OFF();
	do {
		RedLedCPU_Toggle();
		GreenLedCPU_Toggle();
		BlueLedCPU_Toggle();
		YellowLedCPU_Toggle();
		Touts.ToutVend = OneSec;
		Buz_start(Milli500, HZ_2000, 0);
		while(Touts.ToutVend);
	} while (true);
}
