/****************************************************************************************
File:
    DDCMPProtocol.h

Description:
    Include file con definizioni e costanti per il protocollo DDCMP

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __DDCMPProtocol__
#define __DDCMPProtocol__


// Message class
typedef enum {
	kDDCMPMsgClassControl=	0x05,					// Control message
	kDDCMPMsgClassData=		0x81					// Data message (cmd, cmd response, data block)
} DDCMPMsgClass;

// Control messages
typedef enum {
	kDDCMPCtrlStart= 		0x06,
	kDDCMPCtrlStack= 		0x07,
	kDDCMPCtrlAck= 			0x01,
	kDDCMPCtrlNack= 		0x02
} DDCMPCtrl;

// Timings & retries

#ifndef kDDCMPStartMaxCount
	#define	kDDCMPStartMaxCount	10					// Max start attempts [Ref1p3.9]
	#define	kDDCMPStartTO		300					// Time in ms between Start messages [Ref1p3.37]
#endif

enum {
	kDDCMPRetryMaxCount=	5,						// Max data transfer retries [Ref1p3.9]
	kDDCMPDataMsgRxTO=		500,					// ms [Ref1p3.9]
//	kDDCMPDataMsgRWPRxTO=	15000,					// ms [Ref1p3.9]
	kDDCMPStackCTO=			60 + 200,				// characters [Ref1p3.9] (extended by kDDCMPLineDelay)
	kDDCMPAckCTO=			40 + 200,				// characters [Ref1p3.9] (extended by kDDCMPLineDelay)
	kDDCMPInterByteCTO=		15 + 10					// characters [Ref1p3.9] (NOT extended)
	// I timeout sono stati aumentati per evitare problemi con i sistemi lenti
};

enum {
	kDDCMPDataLenMax=		16365
};

// Frame Header 
enum {
	kDDCMPFrameHdrClass,					// 0
	kDDCMPFrameHdrType_Len,					// 1
	kDDCMPFrameHdrFlags,					// 2
	kDDCMPFrameHdrSBD_RR,					// 3
	kDDCMPFrameHdrMBD_XX,					// 4
	kDDCMPFrameHdrSAdd,						// 5
	kDDCMPFrameHdrCRC16,					// 6
	kDDCMPFrameHdrCRC16H,					// 7

	kDDCMPFrameHdrLen						// 8
};

enum {
	// Used only for FAGE proprietary extaned-address mode
	kDDCMPFrameHdrExtAddr= kDDCMPFrameHdrCRC16
};


// NACK error codes [Ref1p3.20]
typedef enum {
	kDDCMPNackErrNone=		0,
	kDDCMPNackErrHdrCRC=	1,
	kDDCMPNackErrDataCRC=	2,
	kDDCMPNackErrREP=		3,
	kDDCMPNackErrNoBuff=	8,
	kDDCMPNackErrRxOvrn=	9,
	kDDCMPNackErrMsgTooLong=16,
	kDDCMPNackErrHdrFmt=	17,

	kDDCMPNackErrMask=		0x3f
} DDCMPNackErr;

// Baud rates: la mumerazione e' quella riportata
// nel doc "Data Transfer Standard" EVA
typedef enum {
	kDDCMPbrUnchanged,			// 0
	kDDCMPbr1200,				// 1
	kDDCMPbr2400,				// 2
	kDDCMPbr4800,				// 3
	kDDCMPbr9600,				// 4
	kDDCMPbr19200,				// 5
	kDDCMPbr38400,				// 6
	kDDCMPbr57600,				// 7
	kDDCMPbr115200,				// 8
	kDDCMPbr_Count
} DDCMPBaudRate;


// Bits and masks
#define	kDDCMPSelectFlagMask	0x80
#define	kDDCMPSyncFlagMask		0x40
#define	kDDCMPDataLenMask		0x3f


#endif


