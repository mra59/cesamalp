/****************************************************************************************
File:
    VMC_CPU_Prog.c

Description:
    File con funzioni di programmazione del VMC tramite tastiera e display LCD.
    

History:
    Date       Aut  Note
    Set 2016	MR   

*****************************************************************************************/

/*
 * 10.07.2017 - Nota sulla gestione dei codici EVA-DTS: 
 * ID102: Machine Model Number (AN 01-20): programmabile SOLO tramite USB con 'MODM' e visualizzato su LCD e Touch all'accensione (da inserire nel DDCMP)
 * ID104: Machine Location     (AN 01-30): programmabile come numerico max 65535 chr (LCD in MenuOpzioniVarie e visualizzato nel menu Audit)
 * ID105: User Defined Field   (AN 01-12): Serial Number Produttore, SOLO tramite USB con 'SNXX', visualizzato su LCD e Touch all'accensione (da inserire nel DDCMP)
 * ID106: Machine Asset Number (AN 01-20): Codice Macchina programmabile come numerico max 8 chr (LCD in MenuOpzioniVarie e visualizzato nel menu Audit)
 */


/* Includes ------------------------------------------------------------------*/
#include <string.h>

#include "ORION.H"
#include "VMC_CPU_Prog.h"
#include "VMC_CPU_ProgFunct.h"
#include "AuditDataDescr.h"
#include "IICEEP.h"
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"
#include "VMC_CPU_LCD.h"
#include "OrionTimers.h"
#include "VMC_CPU_Tasti.h"

#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	tProgHex16	sHex16;											// Struttura per elaborare un Hex a 16 bit (0-65535)
extern	tProgBCD	sBCD;											// Struttura per elaborare un BCD (0-99)
extern	void 		ZeroToSpaces(char *Buff, uint16_t BuffLen);
extern	void  		ClearAudit(void);
extern	void  		ClrDeconto(void);
extern	uint8_t  	CHGHook_GetNumCoinInTube(uint8_t CoinType);
extern	void   		AzzeraTuttaEEPROM(void);
extern	void  		ResetGuasti(void);
extern	bool  		Ciclo_Selezione(uint8_t NumeroSelez);
extern	void 		ResetToutProgram(void);
extern	void  		Check_Timer_Vari(void);
extern	void 		Motori_OFF(void);
extern	void  		AttesaStopMotore(uint32_t TimeAttesa);

extern	char	LCD_DataBuff[LCD_DBUF_SIZE];


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/
	
const uint8_t SelezAvailable[] = {
	Selez_01, Selez_02, Selez_03, Selez_04,
	/*
	Selez_05, Selez_06,
	Selez_07, Selez_08, Selez_09, Selez_10,
	Selez_11, Selez_12, Selez_13, Selez_14, Selez_15, Selez_16,
	Selez_21, Selez_22, Selez_23, Selez_24, Selez_25, Selez_26,
	Selez_31, Selez_32, Selez_33, Selez_34, Selez_35, Selez_36,
	Selez_41, Selez_42, Selez_43, Selez_44, Selez_45, Selez_46,
	Selez_51, Selez_52, Selez_53, Selez_54, Selez_55, Selez_56,
	Selez_61, Selez_62, Selez_63, Selez_64, Selez_65, Selez_66,
	*/
	nil
};

const char sLocalizationDefault[]= "|||~-*YN.,YN";	// Default localization settings

ProgramVars		sProgVars;							// Struttura main principale

//*********************************************************************
// ********   PARAMETRI   SELEZIONI                            ********
//*********************************************************************

/*------------------------------------------------------------------------------------------*\
 Method: TD_SubMenu_SceltaSelez
	Scelta della selezione da programmare.
	Se macchina senza pulsanti si forza alla sola selezione 1.

	IN:  - 
    OUT: - true se scelta una selezione da programmare in sProgVars.NumSel, false se si 
    	   vuole tornare al menu principale
\*------------------------------------------------------------------------------------------*/
static bool TD_SubMenu_SceltaSelez(void)
{
	uint16_t	AddrParam;
	
	// -- - Visualizzo sulla riga 2 per quale Menu devo scorrere le singole selezioni -----
	if (sProgVars.MainStatus == Pr_Menu_TempiDosi)
	{
		VisMessage(mTempiDosi, Row_2, 0, 0, true);												// mTempiDosi
	} 

#if (CESAM == false)
	else if (sProgVars.MainStatus == Pr_Menu_PrezziSel)
	{
		VisMessage(mPrezzoSelez, Row_2, 0, 0, true);											// mPrezzoSelez
	}
	else if (sProgVars.MainStatus == Pr_Menu_ScontiSel)
	{
		VisMessage(mScontoSelez, Row_2, 0, 0, true);											// mScontoSelez
	}
#endif
	
	else if (sProgVars.MainStatus == Pr_Menu_ImpostazProduttore)
	{
		VisMessage(mOpzNumRotaz, Row_3, 0, 0, true);											// mOpzNumRotaz
	} 
	AddrParam = RFUAddr(EEDummyByte);
	if (gOrionConfVars.NO_Pulsanti == true)
	{
		Prog_Generic_BCD(mSelezioneUnaCifra, AddrParam, 1, 1, true);							// Chiama la Prog_Generic_BCD per visualizzare la selezione 1 ma non accetta altre selez da impostare
	}
	else
	{
		if (LASTSELEZ < 0x09)
		{
			Prog_Generic_BCD(mSelezioneUnaCifra, AddrParam, FIRSTSELEZ, LASTSELEZ, true);
		}
		else
		{
			Prog_Generic_BCD(mSelezioneNumero, AddrParam, FIRSTSELEZ, LASTSELEZ, false);
		}
	}
	ReadEEPI2C(AddrParam, (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));				// Selezione che si vuole impostare 
	if (Tasto == Tasto_PrevMenu) {
		return false;																			// Tasto indietro: esco
	}
	sProgVars.NumSel--;
	return true;
}

/*------------------------------------------------------------------------------------------*\
 Method: TD_SubMenu_ProgSelez
	Programmazione della selezione.
	
	IN:  -  sProgVars.NumSel as pointer alla EE, quindi gia' decrementato di 1 rispetto alla
			visualizzazione
    OUT: - 
\*------------------------------------------------------------------------------------------*/
static void TD_SubMenu_ProgSelez(void) 
{
	uint16_t	AddrParam;
	
	if (LASTSELEZ < 0x09)
	{
		VisMessage(mSelezioneUnaCifra, Row_2, 0, 0, false);
		VisualNum_1cifra(sProgVars.NumSel+1, Row_2);												// Visualizza richiesta numero selezione da impostare
	}
	else
	{
		VisMessage(mSelezioneNumero, Row_2, 0, 0, false);
		VisualNum_2cifre(sProgVars.NumSel+1, Row_2);												// Visualizza richiesta numero selezione da impostare
	}
	if (Prog_Opzioni_Selezioni(sProgVars.NumSel, mSel_Disable, BIT_INH_SEL) == true) return;		// Programma opzione Selez abilitata 
	if (Prog_Opzioni_Selezioni(sProgVars.NumSel, mSel_Impulsi, BIT_IMPULSI) == true) return;		// Programma opzione Impulsi
	if (LastInputData == true)
	{
		//-- Selezione a Impulsi ---
		AddrParam = EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_1 + sProgVars.NumSel]);
		if (Prog_Generic_uint16((mVal_CounterSel), AddrParam, MIN_FLOW_SEL, MAX_FLOW_SEL, NoValuta) == true) return;
	}
	else
	{
		//-- Selezione a Tempo ---
		AddrParam = EEPVMCParam16Addr(EE_ParamMisc[TIMESelez_1 + sProgVars.NumSel]);
		if (Prog_Generic_uint16((mVal_TimeSel), AddrParam, MIN_TIME_SEL, MAX_TIME_SEL, NoValuta) == true) return;
	}
	if (Prog_PrezzoSelez((sProgVars.NumSel)) == true) return;										// Programma il Prezzo della Selezione
	if (Prog_ScontoSelez((sProgVars.NumSel)) == true) return;										// Programma il prezzo scontato della Selezione
}

/*------------------------------------------------------------------------------------------*\
 Method: MenuTempiDosi
 	 Funzioni di programmazione param selezioni, si esce con un reset da tasto PROG o si
 	 ritorna al Main Menu.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void MenuTempiDosi(void)
{
	ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[SenzaPulsanti]), (uint8_t *)(&gOrionConfVars.NO_Pulsanti), 1U);			// Opzione Macchina senza pulsanti
	if (gOrionConfVars.NO_Pulsanti > 0) gOrionConfVars.NO_Pulsanti = true;
	if (gOrionConfVars.NO_Pulsanti == true)
	{
			sProgVars.NumSel = FIRSTSELEZ;																// Macchina senza pulsanti: programmo solo la Selezione 1
			WriteEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));	// Il numero selez che verra' usato e' in EEDummyByte
	}
	else
	{
		ReadEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
		if (sProgVars.NumSel > 1)
		{
			sProgVars.NumSel = FIRSTSELEZ;																// Parto visualizzando la prima Selezione predefinita
			WriteEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
		}
	}
	do {
		if (TD_SubMenu_SceltaSelez() == false) return;												// Richiesta ritorno menu principale
		TD_SubMenu_ProgSelez();																		// Accesso alle selezioni da programmare
	} while (true);
}

/*------------------------------------------------------------------------------------------*\
 Method: MenuPrezzi

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
#if (CESAM == false)

static void MenuPrezzi(void) {
	
	ReadEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
	if (sProgVars.NumSel > 1) {
		sProgVars.NumSel = FIRSTSELEZ;																// Parto visualizzando la prima Selezione predefinita
		WriteEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
	}
	do {
		if (TD_SubMenu_SceltaSelez() == false) return;												// Richiesta ritorno menu principale
		if (Prog_PrezzoSelez((sProgVars.NumSel)) == true) break;									// Programma il Prezzo della Selezione
	} while (true);
}
#endif

/*------------------------------------------------------------------------------------------*\
 Method: MenuSconti

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
#if (CESAM == false)

static void MenuSconti(void) {

	//MR19 uint16_t	AddrParam;
	
	//MR19 AddrParam = EEPVMCParam16Addr(EE_ParamMisc[ValScontoBicchiere]);
	//MR19 if (Prog_Generic_uint16(mSel_ScontoBicch, AddrParam, MinSconto, MaxSconto, true) == true) return;
	
	ReadEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
	if (sProgVars.NumSel > 1) {
		sProgVars.NumSel = FIRSTSELEZ;																	// Parto visualizzando la prima Selezione predefinita
		WriteEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
	}
	do {
		if (TD_SubMenu_SceltaSelez() == false) return;													// Richiesta ritorno menu principale
		if (Prog_ScontoSelez((sProgVars.NumSel)) == true) break;										// Programma il prezzo scontato della Selezione
	} while (true);
}
#endif

/*------------------------------------------------------------------------------------------*\
 Method: MenuTabSelezPrezzo

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
#if (CESAM == false)

static void MenuTabSelezPrezzo(void) {

	uint16_t	AddrParam;
	
	ReadEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
	if (sProgVars.NumSel > 1) {
		sProgVars.NumSel = FIRSTSELEZ;																	// Parto visualizzando la prima Selezione predefinita
		WriteEEPI2C(RFUAddr(EEDummyByte), (byte *)(&sProgVars.NumSel), sizeof(sProgVars.NumSel));
	}
	do {
		if (TD_SubMenu_SceltaSelez() == false) return;													// Richiesta ritorno menu principale
		AddrParam = EEPConfVMCAddr(EE_SelezPrezzo[sProgVars.NumSel]);
		if (Prog_Generic_uint8(mTab_SelPrez, AddrParam, MIN_NUMPRICE, MAX_NUMPRICE) == true) return;	// Programma il Numero del prezzo della Selezione
	} while (true);
}
#endif

/*------------------------------------------------------------------------------------------*\
 Method: TD_SubMenu_CoinVal
	Programmazione valore monete.
	
	IN:  - 
    OUT: - 
\*------------------------------------------------------------------------------------------*/
static bool TD_SubMenu_CoinVal(void) {
	
	uint8_t		NumCoin = 0;
	uint16_t	AddrParam;

	for (NumCoin=0; NumCoin < NumeroMonete; NumCoin++) {
		VisMessage(mPay_CoinNum, Row_3, 0, 0, true);
		VisualNum_2cifre((NumCoin+1), Row_3);																// Visualizza numero moneta in programmazione
		AddrParam = IICEEPConfigAddr(EEMonete[NumCoin]);													// Address EE moneta in programmazione
		if (Prog_Generic_uint16(mPay_CoinVal, AddrParam, MinCoinVal, MaxCoinVal, SiValuta) == true)
		{
		 	return true;
		}
	}
	return false;
}	

/*------------------------------------------------------------------------------------------*\
 Method: MenuAudit
 	Visualizza i dati Audit disponibili in base alle opzioni di funzionamento del DA.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void MenuAudit(void) {
	
#define	Data32Len		7																							// Lunghezza buffer dati a 32 bit

		uint8_t		Param, offsetRow;
		uint16_t	AddrParam, OffsAddr;
static	uint32_t	DatoAudit, Temp1, Temp2, Temp3, Temp4, Temp5;
const 	uint8_t		*pntSelNum;																						// Pointer alla tabella con i numeri delle selezioni 

#if (CodMacch_10Cifre == true)			
	char		localBuff[24];
#endif
	
		
	// -- Visualizzo Codice Macchina (ID106) sulla prima riga --

#if (CodMacch_10Cifre == true)			
	// --- Codice Macchina 10 cifre nel buffer LCD ---- 
	ReadEEPI2C(IICEEPConfigAddr(EE_CodeMacchina[0]), (byte *)(&localBuff[0]), EE_CodeMacchinaLen);					// Leggo Codice MAcchina ASCII in RAM
	ZeroToSpaces(&localBuff[0], EE_CodeMacchinaLen);																// Tolgo gli zeri non significativi
	VisMessage(mPay_CodiceMacchina10Cifre, Row_1, 0, Row_2, true);
	offsetRow = RicercaPosizione(Row_1, kLocLCDCifraIntera);
	strncpy(&LCD_DataBuff[offsetRow], &localBuff[0], EE_CodeMacchinaLen);
	Refresh_LCD();																									// Visualizza valore
#else
	VisMessage(mPay_CodiceMacchina, Row_1, 0, Row_2, true);
	ReadEEPI2C(IICEEPConfigAddr(EECodiceMacchina), (byte *)(&CodiceMacchina), sizeof(CodiceMacchina));				// Leggo Codice Macchina
	if (CodiceMacchina > Max_CodiceMacchina) CodiceMacchina = Max_CodiceMacchina;
	offsetRow = RicercaPosizione(Row_1, kLocLCDCifraIntera);
	Hex32InMessage(&LCD_DataBuff[0], CodiceMacchina, offsetRow, NUMCIFRE_COD_MACCH);								// Visualizza dato fino a 8 cifre
	Refresh_LCD();																									// Visualizza valore
#endif
	
	// -----  Totale  Battute  LR  -----
	AddrParam = IICEEPAuditAddr(LRCashNum);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp1), sizeof(uint16_t));
	AddrParam = IICEEPAuditAddr(LRCardNum);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp2), sizeof(uint16_t));
	DatoAudit = Temp1 + Temp2;
	VisMessage(mAud_TotBattuteLR, Row_3, SpacesInBuffer, Row_4, false);
	if ((Visualizza_Valore(Row_4, DatoAudit, sizeof(uint32_t), false)) == true) return;			// Richiesta ritorno menu principale

	// -----  Totale  Battute  IN  -----
	AddrParam = IICEEPAuditAddr(INCashNum);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp1), sizeof(uint16_t));
	AddrParam = IICEEPAuditAddr(INCardNum);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp2), sizeof(uint16_t));
	DatoAudit = Temp1 + Temp2;
	VisMessage(mAud_TotBattuteIN, Row_3, SpacesInBuffer, Row_4, false);
	if ((Visualizza_Valore(Row_4, DatoAudit, sizeof(uint32_t), false)) == true) return;			// Richiesta ritorno menu principale
	
	// -----  Totale  Battute in Gratuito  LR  -----
	AddrParam = IICEEPAuditAddr(LRTestNum);
	ReadEEPI2C(AddrParam, (uint8_t *)(&DatoAudit), sizeof(uint16_t));
	VisMessage(mAud_TotBattuteTest, Row_3, SpacesInBuffer, Row_4, false);
	if ((Visualizza_Valore(Row_4, DatoAudit, sizeof(uint32_t), false)) == true) return;			// Richiesta ritorno menu principale

	// -----  Totale  Venduto  LR  -----
	AddrParam = IICEEPAuditAddr(LRCashVnd);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp1), sizeof(uint32_t));
	AddrParam = IICEEPAuditAddr(LRCardVnd);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp2), sizeof(uint32_t));
	DatoAudit = Temp1 + Temp2;
	VisMessage(mAud_TotVendutoLR, Row_3, SpacesInBuffer, Row_4, false);
	if ((Visualizza_Valore(Row_4, DatoAudit, sizeof(uint32_t), true)) == true) return;			// Richiesta ritorno menu principale
	
	// -----  Totale  Venduto  IN  -----
	AddrParam = IICEEPAuditAddr(INCashVnd);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp1), sizeof(uint32_t));
	AddrParam = IICEEPAuditAddr(INCardVnd);
	ReadEEPI2C(AddrParam, (uint8_t *)(&Temp2), sizeof(uint32_t));
	DatoAudit = Temp1 + Temp2;
	VisMessage(mAud_TotVendutoIN, Row_3, SpacesInBuffer, Row_4, false);
	if ((Visualizza_Valore(Row_4, DatoAudit, sizeof(uint32_t), true)) == true) return;			// Richiesta ritorno menu principale

	// -----  Numero  Battute  per Selezione LR  -----
	pntSelNum = &SelezAvailable[0];																// Prima selezione
	sProgVars.pntselnum = &SelezAvailable[0];
	sProgVars.NumSel = *pntSelNum;

	do {
		OffsAddr = (sProgVars.NumSel - 1) * sizeof(uint16_t);
		AddrParam = IICEEPAuditAddr(NumSelFullPrice) + OffsAddr;
		ReadEEPI2C(AddrParam, (uint8_t *)(&Temp1), sizeof(uint16_t));
		
		AddrParam = IICEEPAuditAddr(NumSelDiscount1) + OffsAddr;
		ReadEEPI2C(AddrParam, (uint8_t *)(&Temp2), sizeof(uint16_t));

		AddrParam = IICEEPAuditAddr(NumSelDiscount2) + OffsAddr;
		ReadEEPI2C(AddrParam, (uint8_t *)(&Temp3), sizeof(uint16_t));

		AddrParam = IICEEPAuditAddr(NumSelDiscount3) + OffsAddr;
		ReadEEPI2C(AddrParam, (uint8_t *)(&Temp4), sizeof(uint16_t));

		AddrParam = IICEEPAuditAddr(NumSelDiscount4) + OffsAddr;
		ReadEEPI2C(AddrParam, (uint8_t *)(&Temp5), sizeof(uint16_t));

		DatoAudit = Temp1 + Temp2 + Temp3 + Temp4 + Temp5;
		VisMessage(mSelezioneNumero, Row_3, SpacesInBuffer, Row_4, false);
		VisualNum_2cifre(sProgVars.NumSel, Row_3);												// Visualizza numero selezione
		if ((Visualizza_Valore(Row_4, DatoAudit, sizeof(uint32_t), false)) == true) break;		// Richiesta ritorno menu principale
		if (*(++sProgVars.pntselnum) != nil) {
			sProgVars.NumSel = *sProgVars.pntselnum;
		} else {
			break;
		}
	} while (true);
	
	//  ---- Azzeramento Audit ------
	VisMessage(SpacesInBuffer, Row_3, 0, 0, false);												// Clr Riga 3 che contiene il numero di selezione
	AddrParam = RFUAddr(TestByte);
	if (Prog_Generic_Boolean(mOpt_ClearAuditEE, AddrParam, 0x01) == true) return;
	if (LastInputData == true) {
		Param = 0;
		WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);										// Memo zero per sicurezza nel byte di test
		if (Prog_Generic_Boolean(mOpt_Sicuro, AddrParam, 0x01) == true) return;
		if (LastInputData == true) {
			RedLedCPU(ON);																		// Led Rosso CPU ON
			Param = 0;
			WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);									// Memo zero per sicurezza nel byte di test
			ClearAudit();
			RedLedCPU(OFF);																		// Led Rosso CPU OFF
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: MenuPayments
	Imposta il sistema di pagamento desiderato.
	La funzione disabilita automaticamente i restanti sistemi di pagamento.
	La funzione richiede le eventuali ulteriori opzioni del sistema di pagamento impostato.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void MenuPayments(void) {
	
	uint16_t 	AddrParam, ValParam, Pin_Digitato;
	uint8_t		ParamZero = 0;
	//uint8_t		ParamBool, NumCoin, EnaDisMsk, offsetRow;

	sProgVars.PaymentStatus = Paym_Mifare;																// Prima voce del menu payment
	VisMessage(mSistemiPagamento, Row_3, 0, 0, true);													// msg menu sistemi di pagamento su riga 3

	do {
		switch (sProgVars.PaymentStatus)
		{
		/*
			case Paym_Executive:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[ExecSlave]);
				if (Prog_Generic_Boolean(mPay_Executive, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_Mifare;
				if (LastInputData != 0) {
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]), 	(uint8_t *)(&ParamZero), 1U);
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]), (uint8_t *)(&ParamZero), 1U);
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MDB_SLV]), 			(uint8_t *)(&ParamZero), 1U);
					sProgVars.PaymentStatus = Paym_ExecPriceHolding;									// Cambio voce successiva in conseguenza della programmazione Exec Y
				}
				break;
				
			case Paym_ExecPriceHolding:
				AddrParam = IICEEPConfigAddr(EEPriceHolding);
				if (Prog_Generic_Boolean(mPay_ExecPrHold, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_ExecPriceDisplay;
				if (LastInputData == 0) {
					WriteEEPI2C(IICEEPConfigAddr(EEPriceDisplay), (uint8_t *)(&ParamZero), 1U);			// Clr opzione pricedisplay
					sProgVars.PaymentStatus = Paym_ExecRestoSubito;										// Salto voce successiva in conseguenza di Price Holding N
				}
				break;
				
			case Paym_ExecPriceDisplay:
				AddrParam = IICEEPConfigAddr(EEPriceDisplay);
				if (Prog_Generic_Boolean(mPay_ExecPrDisplay, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_ExecRestoSubito;
				break;
				
			case Paym_ExecRestoSubito:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptExecRestoSubito]);
				if (Prog_Generic_Boolean(mPay_ExecMDBRestoSubito, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_ExecMasterSlave;
				break;
				
			case Paym_ExecMasterSlave:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptExecMstSlav]);
				if (Prog_Generic_Boolean(mPay_ExecMstSlv, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_Mifare;
				break;
			*/
			
			case Paym_Mifare:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]);
				if (Prog_Generic_Boolean(mPay_Mifare, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_CoinVal;
				if (LastInputData != 0) {
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[ExecSlave]), 		(uint8_t *)(&ParamZero), 1U);
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]), (uint8_t *)(&ParamZero), 1U);
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MDB_SLV]), 			(uint8_t *)(&ParamZero), 1U);
					sProgVars.PaymentStatus = Paym_Mif_Pin1;											// Cambio voce successiva in conseguenza della programmazione Mifare Y
				}
				break;

			case Paym_Mif_Pin1:
				Pin_Digitato = AttesaPassword(mSistemiPagamento, Row_3, mPay_MifarePin1, Row_4, 5U);
				if (Tasto == Tasto_Meno) return;
				AddrParam = IICEEPConfigAddr(EEPin1);
				ReadEEPI2C(AddrParam, (uint8_t *)(&ValParam), 2U);										// Leggo attuale Pin dalla EE
				if (Pin_Digitato == ValParam) {
					if (Prog_Generic_uint16(mPay_NewMifarePin, AddrParam, Min_uint16, Max_uint16, NoValuta) == true) return;
				}
				sProgVars.PaymentStatus = Paym_Mif_Pin2;
				break;

			case Paym_Mif_Pin2:
				Pin_Digitato = AttesaPassword(mSistemiPagamento, Row_3, mPay_MifarePin2, Row_4, 5U);
				if (Tasto == Tasto_Meno) return;
				AddrParam = IICEEPConfigAddr(EEPin2);
				ReadEEPI2C(AddrParam, (uint8_t *)(&ValParam), 2U);										// Leggo attuale Pin dalla EE
				if (Pin_Digitato == ValParam) {
					if (Prog_Generic_uint16(mPay_NewMifarePin, AddrParam, Min_uint16, Max_uint16, NoValuta) == true) return;
				}
				sProgVars.PaymentStatus = Paym_Mif_Gest;
				break;

			case Paym_Mif_Gest:
				AddrParam = IICEEPConfigAddr(EEGestore);
				if (Prog_Generic_uint16(mPay_MifareGest, AddrParam, Min_uint16, Max_uint16, NoValuta) == true) return;
				sProgVars.PaymentStatus = Paym_MaxCard;
				break;
				
			case Paym_MaxCard:
				AddrParam = IICEEPConfigAddr(EECreditoMaxCarta);
				if (Prog_Generic_uint16(mPay_MaxCard, AddrParam, Min_uint16, Max_uint16, SiValuta) == true) return;
				sProgVars.PaymentStatus = Paym_EnableDiscount;
				break;

			case Paym_EnableDiscount:
				AddrParam = IICEEPConfigAddr(EEDiscountEna);
				if (Prog_Generic_Boolean(mPay_EnaDiscount, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_CoinVal;
				break;
				
			// --- Programmazione valore monete -----
			case Paym_CoinVal:
				if (TD_SubMenu_CoinVal() == true) return;
				VisMessage(mSistemiPagamento, Row_3, 0, 0, true);										// Rivisualizzo msg menu sistemi di pagamento su riga 3
				sProgVars.PaymentStatus = Paym_MaxCash;													// Prossima voce nel menu
				break;
			
			case Paym_MaxCash:
				AddrParam = IICEEPConfigAddr(EECreditoMaxCash);
				if (Prog_Generic_uint16(mPay_MaxCash, AddrParam, Min_uint16, Max_uint16, SiValuta) == true) return;
				sProgVars.PaymentStatus = Paym_Gratuito;												// Prossima voce nel menu
				break;

			case Paym_Gratuito:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]);
				if (Prog_Generic_Boolean(mPay_Gratis, AddrParam, 0x01) == true) return;
				if (LastInputData != 0) {
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[ExecSlave]), (uint8_t *)(&ParamZero), 1U);
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]), (uint8_t *)(&ParamZero), 1U);
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MDBPaymSyst]), (uint8_t *)(&ParamZero), 1U);
				}
				sProgVars.PaymentStatus = Paym_MDB;
				break;

			case Paym_MDB:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[MDBPaymSyst]);
				if (Prog_Generic_Boolean(mPay_MDB, AddrParam, 0x01) == true) return;
				if (LastInputData != 0) {
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[ExecSlave]), (uint8_t *)(&ParamZero), 1U);
					WriteEEPI2C(EEPVMCParamAddr(EE_ParamOpt[GratuitoPaymSyst]), (uint8_t *)(&ParamZero), 1U);
					sProgVars.PaymentStatus = Paym_MDBRestoSubito;
					gVMC_ConfVars.ProtMDBSLV = false;													// MDB MASTER
				} else {
					sProgVars.PaymentStatus = Paym_Mifare;
				}
				break;
			
			/*
			case Paym_MDB_SLV:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[MDB_SLV]);
				if (Prog_Generic_Boolean(mPay_MDBSLV, AddrParam, 0x01) == true) return;
				if (LastInputData != 0) {
					gVMC_ConfVars.ProtMDBSLV = true;													// Impostato MDB/SLAVE
				} else {
					gVMC_ConfVars.ProtMDBSLV = false;													// MDB MASTER
				}
				sProgVars.PaymentStatus = Paym_MDBRestoSubito;
				break;
			*/
				
			case Paym_MDBRestoSubito:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptExecRestoSubito]);
				if (Prog_Generic_Boolean(mPay_ExecMDBRestoSubito, AddrParam, 0x01) == true) return;
				/*
				if (gVMC_ConfVars.ProtMDBSLV == true) {
					sProgVars.PaymentStatus = Paym_Executive;											// MDB/SLAVE: termino impostazioni MDB col solo Resto Subito
					break;
				}
				*/
				sProgVars.PaymentStatus = Paym_MDB_MultiVend;
				if (LastInputData != 0) {																// Resto Subito, salto richiesta Multi/Single Vend: impostero' MultiVend
					sProgVars.PaymentStatus = Paym_MDB_MaxCash;											// per non attivare il Payout dopo la selezione
				}
				break;
				
			case Paym_MDB_MultiVend:
				AddrParam = IICEEPConfigAddr(EEMultiVendCard);
				if (Prog_Generic_Boolean(mPay_MDBMultiVend, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_MDB_MaxCash;
				break;

			case Paym_MDB_MaxCash:
				AddrParam = IICEEPConfigAddr(EECreditoMaxCash);
				if (Prog_Generic_uint16(mPay_MaxCash, AddrParam, Min_uint16, Max_uint16, SiValuta) == true) return;
				sProgVars.PaymentStatus = Paym_MDB_MaxChange;
				break;

			case Paym_MDB_MaxChange:
				AddrParam = IICEEPConfigAddr(EEMaxChange);
				if (Prog_Generic_uint16(mPay_MDBMaxChange, AddrParam, Min_uint16, Max_uint16, SiValuta) == true) return;
				sProgVars.PaymentStatus = Paym_MDB_CambiaMonete;
				break;
				
			case Paym_MDB_CambiaMonete:
		/*	
				AddrParam = IICEEPConfigAddr(EECambiaMonete);
				if (Prog_Generic_Boolean(mPay_MDBCambiaMonete, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_MDB_BillEnaWithCard;
				break;
		*/
			case Paym_MDB_BillEnaWithCard:
				AddrParam = IICEEPConfigAddr(EEBillValidWCard);
				if (Prog_Generic_Boolean(mPay_MDBBillEnaWithCard, AddrParam, 0x01) == true) return;

				sProgVars.PaymentStatus = Paym_Mifare;
				
		/*				
				// -- Determino next menu in base alla presenza Mifare: non ha relazione col menu BillEnaWCard --
				ReadEEPI2C(EEPVMCParamAddr(EE_ParamOpt[MifarePaymSyst]), (byte *)(&ParamBool), 1U);	// Leggo impostazione Presenza Lettore Mifare
				if (ParamBool) {																		// Mifare Locale presente, salto all'abilitazione Coins/Bills
					sProgVars.PaymentStatus = Paym_MDBCoinEna;											// perche' EnableDiscount gia' programmato in Mifare
				} else {
					sProgVars.PaymentStatus = Paym_MDB_EnableDiscount;
				}
				break;
		*/
				
		/*	MR21 Tolte dalla programmazione abilitaz coin e bill in MDB perche' avrei dovuto renderle impostabili anche da remoto e da chiave USB
			       Inoltre abilitare la gettoniera parallela insieme all'MDB diventava problematico, quindi coin e bill saranno abilitate dalle periferiche RR e Bill Validator
				
			case Paym_MDB_EnableDiscount:
				AddrParam = IICEEPConfigAddr(EEDiscountEna);
				if (Prog_Generic_Boolean(mPay_EnaDiscount, AddrParam, 0x01) == true) return;
				sProgVars.PaymentStatus = Paym_MDBCoinEna;
				break;
				
			case Paym_MDBCoinEna:
				//AddrParam = IICEEPConfigAddr(EEfCoinBillsEna[0]);										// Address EE flags Ena/dis Coins 1-8
				AddrParam = IICEEPConfigAddr(EEfCoinEnabled[0]);										// Address EE flags Ena/dis Coins 1-8
				EnaDisMsk = 1;
				for (NumCoin=0; NumCoin < MAX_NUM_COINTYPES; NumCoin++) {
					VisMessage(mPay_CoinNum, Row_2, 0, 0, true);										// Numero moneta in programmazione su riga 2
					VisualNum_2cifre((NumCoin+1), Row_2);
					VisMessage(mPay_MDBCoinsInTube, Row_3, 0, 0, false);								// "Coins in Tube    |||" su riga 2
					offsetRow = RicercaPosizione(Row_3, kLocLCDCifraIntera);
					Hex8InMessage(&LCD_DataBuff[0], CHGHook_GetNumCoinInTube(NumCoin), offsetRow, false, 0);			// Visualizza dato fino a 3 cifre
					if (Prog_Generic_Boolean(mPay_MDBCoinBillEna, AddrParam, EnaDisMsk) == true) break;
					if (EnaDisMsk == 0x80) {
						EnaDisMsk = 1;
						AddrParam++;																	// Address EE flags Ena/dis Coins 9-16
					} else {
						EnaDisMsk = EnaDisMsk << 1;
					}
				}
				sProgVars.PaymentStatus = Paym_MDBBillEna;
				break;

			case Paym_MDBBillEna:
				//AddrParam = IICEEPConfigAddr(EEfCoinBillsEna[2]);										// Address EE flags Ena/dis Bills 1-8
				AddrParam = IICEEPConfigAddr(EEfBillEnabled[0]);										// Address EE flags Ena/dis Bills 1-8
				EnaDisMsk = 1;
				for (NumCoin=0; NumCoin < MAX_NUM_COINTYPES; NumCoin++) {
					VisMessage(mPay_MDBBillNum, Row_3, 0, 0, true);										// Numero banconota in programmazione su riga 3
					VisualNum_2cifre((NumCoin+1), Row_3);
					if (Prog_Generic_Boolean(mPay_MDBCoinBillEna, AddrParam, EnaDisMsk) == true) break;
					if (EnaDisMsk == 0x80) {
						EnaDisMsk = 1;
						AddrParam++;																	// Address EE flags Ena/dis Bills 9-16
					} else {
						EnaDisMsk = EnaDisMsk << 1;
					}
				}
				VisMessage(mSistemiPagamento, Row_3, 0, 0, true);										// msg menu sistemi di pagamento su riga 3
				sProgVars.PaymentStatus = Paym_Mifare;
				break;
		*/
				

		}
	} while (true);
}

/*------------------------------------------------------------------------------------------*\
 Method: MenuOpzioniVarie

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void MenuOpzioniVarie(void)
{
	uint8_t 	Param;
	uint16_t 	AddrParam;
	
	sProgVars.OptionsStatus = Opt_SetOrologio;													// Prima voce del menu Options
	VisMessage(mOpzioniVarie, Row_3, 0, 0, true);												// msg menu "Opzioni Varie" su riga 3

	do {
		switch (sProgVars.OptionsStatus)
		{
			//  ---- Comando Impostazione Orologio  ------
			case Opt_SetOrologio:
				AddrParam = RFUAddr(TestByte);
				if (Prog_Generic_Boolean(mOrologio, AddrParam, 0x01) == true) return;
				if (LastInputData == true) {
					Param = 0;
					WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);										// Memo zero per sicurezza nel byte di test
					Prog_Orologio();
					VisMessage(mOpzioniVarie, Row_3, 0, 0, true);											// msg menu "Opzioni Varie" su riga 3
				}
				sProgVars.OptionsStatus = Opt_START_AUTOMATICO;
				break;
			
			//  ---- Start Erogazione Senza Premere Pulsante ------
			case Opt_START_AUTOMATICO: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[StartAutomatico]);
				if (Prog_Generic_Boolean(mOpt_AutomaticStart, AddrParam, 1U) == true) return;				// Opzione "Start automatica selezione 1 all'inserimento del credito anche con macchine con pulsanti"
				gOrionConfVars.AutomaticStart = LastInputData;
				sProgVars.OptionsStatus = Opt_MULTI_VEND_AUTOMATICO;
				break;

			//  ---- Multi Vendita  Automatica (scala automaticamente il credito) ------
			case Opt_MULTI_VEND_AUTOMATICO: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[MultiVendAutomatica]);
				if (Prog_Generic_Boolean(mOpt_AutomaticMultiVend, AddrParam, 1U) == true) return;			// Opzione "Vendita automatica" per allungare automaticamente la selezione
				gOrionConfVars.AutomaticMultiVend = LastInputData;
				sProgVars.OptionsStatus = Opt_CambioLingua;
				break;

			//  ---- Cambio Lingua Messaggi  ------
			case Opt_CambioLingua:
		/*
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptLinguaMessaggi]);
				if (Prog_Generic_Boolean(mLanguageNum, AddrParam, 0x01) == true) return;
				if (LastInputData == false) {
					Lingua = 0;
				} else {
					Lingua = 1;
				}
				sProgVars.OptionsStatus = Opt_DPP;
				break;
		*/
				
			//  ------       DPP        ------
			case Opt_DPP:
				AddrParam = IICEEPConfigAddr(EEDPP);
				if (Prog_Generic_uint8(mOpt_DPP, AddrParam, Min_DPP, Max_DPP) == true) return;
				ReadEEPI2C(AddrParam, (uint8_t *)(&DecimalPointPosition), 1U);								// Leggo DPP impostato per visualizzarlo nelle valute da impostare
				sProgVars.OptionsStatus = Opt_CodiceMacchina;
				break;

			// -- Imposta Codice Macchina ---
			case Opt_CodiceMacchina:
#if (CodMacch_10Cifre == true)			
				AddrParam = IICEEPConfigAddr(EE_CodeMacchina);
				if (Prog_Numero_ASCII(mPay_CodiceMacchina10Cifre, AddrParam, EE_CodeMacchinaLen) == true) return;
				sProgVars.OptionsStatus = TipoValuta;
#else				
				AddrParam = IICEEPConfigAddr(EECodiceMacchina);
				if (Prog_Generic_uint32(mPay_CodiceMacchina, AddrParam, Min_CodiceMacchina, Max_CodiceMacchina, NoValuta) == true) return;
				sProgVars.OptionsStatus = TipoValuta;
#endif
				break;
					
			//  ------   Tipo Valuta        ------
			case TipoValuta:
				AddrParam = IICEEPConfigAddr(EEDescrValuta);
				if (Prog_Generic_uint16(mTipoValuta, AddrParam, MIN_VALUTA, MAX_VALUTA, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_MULTICICLO;
				break;

			//  ------   Opzione  Multiciclo     ------
			case Opt_MULTICICLO:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[MultiCiclo]);
				if (Prog_Generic_Boolean(mMultiCiclo, AddrParam, 1U) == true) return;
				sProgVars.OptionsStatus = Opt_SetOrologio;
				break;

		/*			
			// -- Imposta Codice Locazione ---
			case Opt_CodiceLocazione: 
				AddrParam = IICEEPConfigAddr(EELocazione);
				if (Prog_Generic_uint16(mPay_CodiceLocazione, AddrParam, Min_uint16, Max_uint16, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_SetOrologio;
				break;
				
			//  ---- Opzione Deconto ------
			case Opt_EnaDeconto:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptDeconto]);
				if (Prog_Generic_Boolean(mOpt_EnaDeconto, AddrParam, 0x01) == true) return;
				sProgVars.OptionsStatus = Val_DecontoAttuale;
				if (LastInputData == false) sProgVars.OptionsStatus = Opt_SetOrologio;									// Salta al prossimo parametro
				break;

			//  ---- Valore Attuale Deconto   ------
			case Val_DecontoAttuale: 
				VisMessage(msg_DecontoAttuale, Row_4, 0, 0, false);
				if ((Visualizza_Valore(Row_4, DecontoVal, sizeof(DecontoVal), false)) == true) return;					// Richiesta ritorno menu principale
				sProgVars.OptionsStatus = Val_InizialeDeconto;
				break;
					
			//  ---- Valore iniziale Deconto   ------
			case Val_InizialeDeconto: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[ValMaxDeconto]);
				if (Prog_Generic_uint16(mOpt_DecontoMassimo, AddrParam, Min_Deconto, Max_Deconto, NoValuta) == true) return;
				sProgVars.OptionsStatus = Val_RiservaDeconto;
				break;
				
			//  ---- Valore Riserva Deconto   ------
			case Val_RiservaDeconto: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[ValRiservaDeconto]);
				if (Prog_Generic_uint16(mOpt_DecontoRiserva, AddrParam, Min_RiservaDeconto, Max_RiservaDeconto, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_ResetDeconto;
				break;
				
			//  ---- Reset Deconto ------
			case Opt_ResetDeconto:
				AddrParam = RFUAddr(TestByte);
				if (Prog_Generic_Boolean(mCMD_ResetDeconto, AddrParam, 0x01) == true) return;
				if (LastInputData == true)
				{
					Param = 0;
					WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);													// Memo zero per sicurezza nel byte di test
					ClrDeconto();																						// Clr Stato Deconto e Azzera Riserva
				}
				sProgVars.OptionsStatus = Opt_SetOrologio;
				break;
		*/

		}
	} while (true);
}

/*------------------------------------------------------------------------------------------*\
 Method: MenuImpostazioniProduttore

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void MenuImpostazioniProduttore(void) {

	uint8_t 	Param, offsetRow;
	uint16_t 	AddrParam;
	uint32_t	DatoAudit;
	
	sProgVars.OptionsStatus = Opt_SN_Produttore;												// Prima voce del menu Impostazioni Produttore da eseguire
	VisMessage(mImpostazioniProduttore, Row_3, 0, 0, true);										// msg menu "Impostazioni Produttore" su riga 3

	do {
		switch (sProgVars.OptionsStatus) {
			
			//  ---- Serial Number Produttore (read only)  ------
			case Opt_SN_Produttore:
/*
			  memset(&LCD_DataBuff[Riga_4_In_Buffer], ASCII_SPACE, LCD_NumCol);
				ReadEEPI2C(IICEEPSNProduttAddr(EE_SN_Produttore), (byte *)(&LCD_DataBuff[Riga_4_In_Buffer]), Len_ID105);		// Serial Number Produttore ID105 EVA-DTS (Alfanumerico 1-12)
				Refresh_LCD();
				AttesaTasto(false);
				if (Tasto == Tasto_Meno) return;																	// Terminare il menu
				sProgVars.OptionsStatus = Audit_Battute_Costruttore;
				break;
*/		
			// -----  Serial Number CPU   -----
			case CPU_SERIAL_NUMBER: 
				VisMessage(mPay_CPU_SerialNumber, Row_4, 0, 0, true);
				offsetRow = RicercaPosizione(Row_1, kLocLCDCifraIntera);
				Hex32InMessage(&LCD_DataBuff[0], SerialNumber, offsetRow, NUMCIFRE_SERIALNUMB);						// Visualizza dato fino a 10 cifre
				Refresh_LCD();
				AttesaTasto(false);
				if (Tasto == Tasto_Meno) return;																	// Terminare il menu
				sProgVars.OptionsStatus = Audit_Battute_Costruttore;
				break;
			
			// -----  Totale  Battute  Costruttore  -----
			case Audit_Battute_Costruttore: 
				AddrParam = BattuteCostrutt(VMCTotBattute);
				ReadEEPI2C(AddrParam, (uint8_t *)(&DatoAudit), sizeof(uint32_t));
				VisMessage(mAud_TotBattuteIN, Row_3, SpacesInBuffer, Row_4, true);
				if ((Visualizza_Valore(Row_4, DatoAudit, sizeof(uint32_t), false)) == true) return;					// Richiesta ritorno menu principale
				AddrParam = RFUAddr(TestByte);																		// Richiesta azzeramento counter batture costruttore
				if (Prog_Generic_Boolean(mOpt_ClearAuditEE, AddrParam, 0x01) == true) return;
				if (LastInputData == true) {
					Param = 0;
					WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);												// Memo zero per sicurezza nel byte di test
					if (Prog_Generic_Boolean(mOpt_Sicuro, AddrParam, 0x01) == true) return;
					if (LastInputData == true) {
						RedLedCPU(ON);																				// Led Rosso CPU ON
						Param = 0;
						WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);											// Memo zero per sicurezza nel byte di test
						DatoAudit = 0;
						AddrParam = BattuteCostrutt(VMCTotBattute);
						WriteEEPI2C(AddrParam, (uint8_t *)(&DatoAudit), sizeof(uint32_t));
						RedLedCPU(OFF);																				// Led Rosso CPU OFF
					}
				}
				VisMessage(mImpostazioniProduttore, Row_3, 0, 0, true);												// msg menu "Impostazioni Produttore" su riga 3
				sProgVars.OptionsStatus = Opt_NoPushbuttons;
				break;

		/*
#if (CESAM == false)				
			//  ---- Counter Contatore Volumetrico Selez 1 ------
			case Val_Volumetrico1: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_1]);
				if (Prog_Generic_uint16(mVal_Counter_H2O_1, AddrParam, MIN_FLOW_CNT, MAX_FLOW_CNT, NoValuta) == true) return;
				sProgVars.OptionsStatus = Val_Volumetrico2;
				break;

			//  ---- Counter Contatore Volumetrico Selez 2 ------
			case Val_Volumetrico2: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_2]);
				if (Prog_Generic_uint16(mVal_Counter_H2O_2, AddrParam, MIN_FLOW_CNT, MAX_FLOW_CNT, NoValuta) == true) return;
				sProgVars.OptionsStatus = Val_Volumetrico3;
				break;
			
			//  ---- Counter Contatore Volumetrico Selez 3 ------
			case Val_Volumetrico3: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_3]);
				if (Prog_Generic_uint16(mVal_Counter_H2O_3, AddrParam, MIN_FLOW_CNT, MAX_FLOW_CNT, NoValuta) == true) return;
				sProgVars.OptionsStatus = Val_Volumetrico4;
				break;
			
			//  ---- Counter Contatore Volumetrico Selez 4 ------
			case Val_Volumetrico4: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[H2OFlowCounter_4]);
				if (Prog_Generic_uint16(mVal_Counter_H2O_4, AddrParam, MIN_FLOW_CNT, MAX_FLOW_CNT, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_NoPushbuttons;
				break;
#endif
		*/

			//  ---- Erogazione Senza Pulsanti ------
			case Opt_NoPushbuttons: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[SenzaPulsanti]);
				if (Prog_Generic_Boolean(mOpt_SenzaPulsanti, AddrParam, 1U) == true) return;							// Opzione "Senza Pulsanti ?"
				gOrionConfVars.NO_Pulsanti = LastInputData;
				sProgVars.OptionsStatus = Opt_SWITCH_PRODOTTO_1;
				break;
				
			//  ---- MicroSwitch Fine Prodotto N. 1: NO o NC ------
			case Opt_SWITCH_PRODOTTO_1: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[PolaritaSwitchProdotto_1]);
				if (Prog_Generic_Boolean(mOpt_SwitchFineProdotto_N_1, AddrParam, 1U) == true) return;					// Opzione "Switch Fine Prodotto N.1 attivo aperto?"
				gOrionConfVars.SwitchFineProdotto_1_Open = LastInputData;
				sProgVars.OptionsStatus = Opt_SEL_INH_FINE_PROD_1;
				break;
				
			//  ---- Selezione Disabilitata da Fine Prodotto N. 1  ------
			case Opt_SEL_INH_FINE_PROD_1: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[NumSelInhDaFineProd_1]);
				if (Prog_Generic_BCD(mOpt_InhSelAfterEndProd_N_1, AddrParam, MIN_NUMERO_SEL, MAX_NUMERO_SEL, MAX_NUMERO_SEL) == true) return;
				gOrionConfVars.SelInhEndProd_1 = LastInputData;
				sProgVars.OptionsStatus = Opt_SWITCH_PRODOTTO_2;
				break;
				
			//  ---- MicroSwitch Fine Prodotto N. 2: NO o NC ------
			case Opt_SWITCH_PRODOTTO_2: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[PolaritaSwitchProdotto_2]);
				if (Prog_Generic_Boolean(mOpt_SwitchFineProdotto_N_2, AddrParam, 1U) == true) return;					// Opzione "Switch Fine Prodotto N.2 attivo aperto?"
				gOrionConfVars.SwitchFineProdotto_2_Open = LastInputData;
				sProgVars.OptionsStatus = Opt_SEL_INH_FINE_PROD_2;
				break;
				
			//  ---- Selezione Disabilitata da Fine Prodotto N. 2  ------
			case Opt_SEL_INH_FINE_PROD_2: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[NumSelInhDaFineProd_2]);
				if (Prog_Generic_BCD(mOpt_InhSelAfterEndProd_N_2, AddrParam, MIN_NUMERO_SEL, MAX_NUMERO_SEL, MAX_NUMERO_SEL) == true) return;
				gOrionConfVars.SelInhEndProd_2 = LastInputData;
				sProgVars.OptionsStatus = OUTNum_Erogaz_1;
				break;

			//  ---- Set Uscite per Erogazione 1 ------
			case OUTNum_Erogaz_1:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz1_Out1_Num]);
				if (Prog_Generic_uint8(mVal_Sel_1_Out1, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = II_OUTNum_Erogaz_1;
				break;

			case II_OUTNum_Erogaz_1:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz1_Out2_Num]);
				if (Prog_Generic_uint8(mVal_Sel_1_Out2, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = OUTNum_Erogaz_2;
				break;
				
			//  ---- Set Uscite per Erogazione 2 ------
			case OUTNum_Erogaz_2:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz2_Out1_Num]);
				if (Prog_Generic_uint8(mVal_Sel_2_Out1, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = II_OUTNum_Erogaz_2;
				break;
				
			case II_OUTNum_Erogaz_2:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz2_Out2_Num]);
				if (Prog_Generic_uint8(mVal_Sel_2_Out2, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = OUTNum_Erogaz_3;
				break;
				
				//  ---- Set Uscite per Erogazione 3 ------
			case OUTNum_Erogaz_3:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz3_Out1_Num]);
				if (Prog_Generic_uint8(mVal_Sel_3_Out1, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = II_OUTNum_Erogaz_3;
				break;
				
			case II_OUTNum_Erogaz_3:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz3_Out2_Num]);
				if (Prog_Generic_uint8(mVal_Sel_3_Out2, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = OUTNum_Erogaz_4;
				break;
				
			//  ---- Set Uscite per Erogazione 4 ------
			case OUTNum_Erogaz_4:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz4_Out1_Num]);
				if (Prog_Generic_uint8(mVal_Sel_4_Out1, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = II_OUTNum_Erogaz_4;
				break;
				
			case II_OUTNum_Erogaz_4:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz4_Out2_Num]);
				if (Prog_Generic_uint8(mVal_Sel_4_Out2, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = OUT_PuliziaFiltro;
				break;

			//  ---- Set Uscita pulizia filtro  ------
			case OUT_PuliziaFiltro:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[CleanFilter_OutNum]);
				if (Prog_Generic_uint8(mVal_CleanFilterOutNum, AddrParam, MIN_OUTPUT, MAX_OUTPUT) == true) return;
				sProgVars.OptionsStatus = RemoteInh;
				break;

	/*
#if (CESAM == false)					
			//  ---- Set Numero Flussimetro per Erogazione 1 ------
			case FlussNum_Erogaz_1:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz1_FlussimNum]);
				if (Prog_Generic_uint8(mVal_FlussimErogaz_1, AddrParam, MIN_FLUSSNUM, MAX_FLUSSNUM) == true) return;
				sProgVars.OptionsStatus = FlussNum_Erogaz_2;
				break;
				
			//  ---- Set Numero Flussimetro per Erogazione 2 ------
			case FlussNum_Erogaz_2:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz2_FlussimNum]);
				if (Prog_Generic_uint8(mVal_FlussimErogaz_2, AddrParam, MIN_FLUSSNUM, MAX_FLUSSNUM) == true) return;
				sProgVars.OptionsStatus = FlussNum_Erogaz_3;
				break;
				
			//  ---- Set Numero Flussimetro per Erogazione 3 ------
			case FlussNum_Erogaz_3:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz3_FlussimNum]);
				if (Prog_Generic_uint8(mVal_FlussimErogaz_3, AddrParam, MIN_FLUSSNUM, MAX_FLUSSNUM) == true) return;
				sProgVars.OptionsStatus = FlussNum_Erogaz_4;
				break;
				
			//  ---- Set Numero Flussimetro per Erogazione 4 ------
			case FlussNum_Erogaz_4:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Erogaz4_FlussimNum]);
				if (Prog_Generic_uint8(mVal_FlussimErogaz_4, AddrParam, MIN_FLUSSNUM, MAX_FLUSSNUM) == true) return;
				sProgVars.OptionsStatus = TemperaturaVentola;
				break;
				
			//  ---- Set Temperatura Attivazione Ventola --------
			case TemperaturaVentola:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[ValTemperVentolaON]);
				if (Prog_Generic_BCD(mVal_TemperVentolaON, AddrParam, MIN_TEMPERVENTOLA, MAX_TEMPERVENTOLA, false) == true) return;
				sProgVars.OptionsStatus = RemoteInh;
				break;
#endif
		*/
				
			//  ---- Set Remote Inhibit  --------
			case RemoteInh:
				AddrParam = IICEEPConfigAddr(EERemoteINH);
				if (Prog_Generic_Boolean(mLCD_RemoteInhibit, AddrParam, 0x01) == true) return;
				sProgVars.OptionsStatus = Opt_Tastiera;
				break;


		/*
#if (CESAM == false)
			//  ---- Set Presenza Ozono  --------
			case Opt_Ozono:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptPresOzono]);
				if (Prog_Generic_Boolean(mOpz_Ozono, AddrParam, 0x01) == true) return;
				sProgVars.OptionsStatus = Opt_TimeWaitOzono;
				if (LastInputData == false) sProgVars.OptionsStatus = Opt_Sirena;												// Salta al prossimo parametro
				break;
				
			//  ---- Tempo Attesa Attivazione Ozono ------
			case Opt_TimeWaitOzono: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[OZONO_Timer_WAIT]);
				if (Prog_Generic_uint16(mVal_AttesaOzono, AddrParam, MIN_OZONO_DELAY, MAX_OZONO_DELAY, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_TimeActiveOzono;
				break;
				
			//  ---- Tempo Attivazione Uscita Ozono ------
			case Opt_TimeActiveOzono: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[OZONO_Timer_ON]);
				if (Prog_Generic_uint16(mVal_ErogazOzono, AddrParam, MIN_OZONO_ON, MAX_OZONO_ON, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_Sirena;
				break;

			//  ---- Set Presenza Sirena Allarme Porta Aperta  --------
			case Opt_Sirena:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptSirena]);
				if (Prog_Generic_Boolean(mOpz_Sirena, AddrParam, 0x01) == true) return;
				sProgVars.OptionsStatus = Opt_TimeDelaySirena;
				if (LastInputData == true) sProgVars.OptionsStatus = Opt_Tastiera;												// Salta al prossimo parametro
				break;
				
			//  ---- Delay attivazione Sirena Allarme Porta Aperta ------
			case Opt_TimeDelaySirena: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[SIRENA_Timer_Delay]);
				if (Prog_Generic_uint16(mVal_RitardoSirena, AddrParam, MIN_SIRENA_DELAY, MAX_SIRENA_DELAY, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_TimeSirenaON;
				break;
				
			//  ---- Durata Suono Sirena Allarme Porta Aperta ------
			case Opt_TimeSirenaON: 
				AddrParam = EEPVMCParam16Addr(EE_ParamMisc[SIRENA_Timer_ON]);
				if (Prog_Generic_uint16(mVal_AttivazSirena, AddrParam, MIN_SIRENA_ON, MAX_SIRENA_ON, NoValuta) == true) return;
				sProgVars.OptionsStatus = Opt_Tastiera;
				break;
#endif
		*/		
				
			//  ---- Tipo Tastiera ------
			case Opt_Tastiera: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[EE_Keyb_Type]);
				if (Prog_Generic_uint8(mOpt_KeyBoardType, AddrParam, MIN_TASTIERA, MAX_TASTIERA) == true) return;
				sProgVars.OptionsStatus = Opt_LCD_16x2;
				break;

			//  ---- LCD 16x2 ------
			case Opt_LCD_16x2:
/*			
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptLCD_Type]);
				if (Prog_Generic_Boolean(mLCD_16x2, AddrParam, 1U) == true) return;
				sProgVars.OptionsStatus = Opt_ClearEE;
				break;
*/
			
/*
			//  ---- LCD su Scheda Remota ------
			case Opt_LCD_Remoto: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[Remote_LCD]);
				if (Prog_Generic_Boolean(mLCD_Remoto, AddrParam, 1U) == true) return;
				sProgVars.OptionsStatus = Opt_Spirali;
				break;

			//  ---- Tipo distributore ------
			case Opt_Spirali: 
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[EE_DA_Type]);
				if (Prog_Generic_uint8(mOpt_DaType, AddrParam, MIN_DATYPE, MAX_DATYPE) == true) return;
				sProgVars.OptionsStatus = Opt_Tastiera;
				break;
			
	*/
			//  ---- Azzeramento Memoria EE  ------
			case Opt_ClearEE: 
				AddrParam = RFUAddr(TestByte);
				if (Prog_Generic_Boolean(mOpt_ClearEE, AddrParam, 0x01) == true) return;
			  	ReadEEPI2C(AddrParam, (byte *)(&Param), 1U);
				if (Param != false) {
					Param = 0;
					WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);												// Memo zero per sicurezza nel byte di test
					if (Prog_Generic_Boolean(mOpt_Sicuro, AddrParam, 0x01) == true) return;
				  	ReadEEPI2C(AddrParam, (byte *)(&Param), 1U);
					if (Param != false)
					{
						LCD_Disp_ConfigUpdate();																	// Visualizzo messaggio di attesa
						GreenLedCPU(OFF);																			// Led Verde CPU OFF
						RedLedCPU(ON);																				// Led Rosso CPU ON
						Param = 0;
						WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);											// Memo zero per sicurezza nel byte di test
						AzzeraTuttaEEPROM();
						RedLedCPU(OFF);																				// Led Rosso CPU OFF
						GreenLedCPU(ON);																			// Led Verde CPU ON
						LCD_ForzaNormalVisual();
						VisMessage(mImpostazioniProduttore, Row_3, 0, 0, true);										// msg menu "Impostazioni Produttore" su riga 3
					}
				}
				sProgVars.OptionsStatus = Audit_Battute_Costruttore;												// Riparto dalla prima voce del menu Avanzate
				break;
		}
	} while (true);
}

/*------------------------------------------------------------------------------------------*\
 Method: MenuCollaudo

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
#if (CESAM == false)
static void MenuCollaudo(void)
{

}
#endif

/*------------------------------------------------------------------------------------------*\
 Method: MenuRisparmio
	In Ecoline ci sono solo le 2 fasce orarie Luci, valide per tutti i giorni della settimana.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void MenuRisparmio(void)
{
	//uint8_t 	Param
	uint8_t		Giorno, numerofascia, j;
	uint16_t 	AddrParam, ParamON_1, ParamOFF_1, ParamON_2, ParamOFF_2;
	
	sProgVars.OptionsStatus = Opt_Ena_Luci_ON;											// Prima voce del menu Risparmio
	VisMessage(mRisparmio, Row_3, 0, 0, true);													// msg menu "Fasce Orarie" su riga 3

	do {
		switch (sProgVars.OptionsStatus)
		{
			
		/*	
			//  ---- Abilitazione Risparmio Energetico  ------
			// -- Msg: "Risparmio Energ.?  |"
			case Opt_EnaRisparmioEnerg:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptEnaRisparmioEnerg]);
				if (Prog_Generic_Boolean(mOpt_EnaRisparmioEnergetico, AddrParam, 0x01) == true) return;
				sProgVars.OptionsStatus = Opt_Ena_Luci_ON;
				if (LastInputData == false) sProgVars.OptionsStatus = Opt_EnaRisparmioEnerg;					// Salta al prossimo parametro
				break;
		*/
			
			//  ---- Abilitazione ON Luci ------
			// -- Msg: "Fasce orarie Luci? |"
			case Opt_Ena_Luci_ON:
				AddrParam = EEPVMCParamAddr(EE_ParamOpt[OptEnaONLuci]);
				if (Prog_Generic_Boolean(mOpt_EnaONLuci, AddrParam, 0x01) == true) return;
				sProgVars.OptionsStatus = Opt_ImpostaFasceLuciRequest;
				if (LastInputData == false) sProgVars.OptionsStatus = Opt_Ena_Luci_ON;							// Salta prossimo parametro
				break;
				
			//  ---- Richiesta se impostare Fasce Orarie Luci ------
			case Opt_ImpostaFasceLuciRequest: 
				
			/*
				AddrParam = RFUAddr(TestByte);
				Prog_Generic_Boolean(mImpostaFasce, AddrParam, 0x01);
				sProgVars.OptionsStatus = Opt_Ena_Luci_ON;
				if (LastInputData == true) {
					Param = 0;
					WriteEEPI2C(AddrParam, (uint8_t *)(&Param), 1U);
					sProgVars.OptionsStatus = Opt_ImpostaFasceONLuci;
				}
				break;
			*/
			
			
			//  ---- Impostazione fasce ON-OFF Luci ------
			case Opt_ImpostaFasceONLuci:
				Giorno = 1;
				// In questo loop chiedo il giorno della settimana
				//do
				//{
				//	VisMessage(mRisparmio, Row_3, mDayOfTheWeek, Row_4, true);
				//	if ((Visualizza_Valore(Row_4, Giorno, sizeof(Giorno), false)) == true) break;
				//	if (Tasto == Tasto_NextMenu)
				//	{
						if ((Giorno > 0) && (Giorno < 8))
						{
							numerofascia = 1;
							// In questo loop imposto le fasce del giorno della settimana scelto sopra
							do
							{
								VisMessage(mRisparmio, Row_3, mFasciaNum, Row_4, true);
								if ((Visualizza_Valore(Row_4, numerofascia, sizeof(numerofascia), false)) == true) break;
								if (Tasto == Tasto_NextMenu)
								{
									if ((numerofascia > 0) && (numerofascia <= MAX_NUM_FASCE))
									{
										do
										{
											Prog_Fasce(Fasce_Luci, Giorno, numerofascia);
										} while (Tasto != Tasto_PrevMenu);
										Tasto = 1;																// Per non uscire dal loop numero fascia
									}
								}
								else
								{
									numerofascia = Tasto;
									if ((numerofascia == 0) || (numerofascia > MAX_NUM_FASCE)) numerofascia = 1;
								}
							} while (Tasto != Tasto_PrevMenu);
							//Tasto = 0;
						}
					//}
					//else
					//{
					//	Giorno = Tasto;
					//}
				//} while (Tasto != Tasto_PrevMenu);
				sProgVars.OptionsStatus = Opt_Last;																	// Esco
				break;
		}
	} while (sProgVars.OptionsStatus != Opt_Last);
	
	//-- Copio le 2 fasce orarie programmate per il Luned� in tutti i giorni della settimana --
	numerofascia = 1;
	Giorno = 1;
	j = InizioFasce + ((Fasce_Luci * FasciaSettimana_Len) + ((Giorno-1) * (sizeof(SizeEE_FA_Array) * 4) + ((numerofascia-1) * 2)));					// Pointer alla fascia del giorno da elaborare
	AddrParam = EEFasceOrarieAddr(EE_ParamFasc[j]);
	ReadEEPI2C(AddrParam, (uint8_t *)(&ParamON_1), sizeof(ParamON_1));
	ReadEEPI2C(AddrParam+2, (uint8_t *)(&ParamOFF_1), sizeof(ParamOFF_1));
	ReadEEPI2C(AddrParam+4, (uint8_t *)(&ParamON_2), sizeof(ParamON_2));
	ReadEEPI2C(AddrParam+6, (uint8_t *)(&ParamOFF_2), sizeof(ParamOFF_2));
	
	for (Giorno = 2; Giorno < 7; Giorno++)
	{
		j = InizioFasce + ((Fasce_Luci * FasciaSettimana_Len) + ((Giorno-1) * (sizeof(SizeEE_FA_Array) * 4) + ((numerofascia-1) * 2)));
		AddrParam = EEFasceOrarieAddr(EE_ParamFasc[j]);
		WriteEEPI2C(AddrParam, (uint8_t *)(&ParamON_1), sizeof(ParamON_1));
		WriteEEPI2C(AddrParam+2, (uint8_t *)(&ParamOFF_1), sizeof(ParamOFF_1));
		WriteEEPI2C(AddrParam+4, (uint8_t *)(&ParamON_2), sizeof(ParamON_2));
		WriteEEPI2C(AddrParam+6, (uint8_t *)(&ParamOFF_2), sizeof(ParamOFF_2));
	}
}


/*------------------------------------------------------------------------------------------*\
 Method: MenuDefaultFabbrica

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
#if (CESAM == false)
static void MenuDefaultFabbrica(void) {
	
	uint8_t 	Param8bit;
	uint16_t 	AddrParam;

	VisMessage(menuParamFabbrica, Row_3, 0, 0, true);											// msg menu "Parametri  Fabbrica" su riga 3

	//  ---- Set Parametri di Fabbrica  ------
		AddrParam = RFUAddr(TestByte);
		if (Prog_Generic_Boolean(mSetDefaultFabbrica, AddrParam, 0x01) == true) return;
		if (LastInputData == true) {
			Param8bit = 0;
			WriteEEPI2C(AddrParam, (uint8_t *)(&Param8bit), 1U);
			if (Prog_Generic_Boolean(mOpt_Sicuro, AddrParam, 0x01) == true) return;
			if (LastInputData == true) {
				Param8bit = 0;
				WriteEEPI2C(AddrParam, (uint8_t *)(&Param8bit), 1U);
				SetDefaultFabbrica();
			}
		}
	return;
}
#endif

/*------------------------------------------------------------------------------------------*\
 Method: MenuPSW
	Attende inserimento di una password valida.
	La password introdotta determina il livello di accesso alla programmazione.
	Solo per il livello zero e' prevista la possibilita' di entrare senza digitare password
	a condizione che la psw del livello zero sia zero.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void MenuPSW(void) {
	
	bool 		endPSWInput;
	uint8_t		j;
	uint16_t	PSW_Digitata, addrPSW_EE, PSW_EE;
	
	do {
		endPSWInput = false;
		PSW_Digitata = AttesaPassword(mInserire, Row_3, mPassword, Row_4, 4U);
		if (PSW_Digitata == 0) {																// Se non digito password e la psw di Livello 0 e' zero, entro
			addrPSW_EE = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_1]);								// nella programmazione con Livello 0
			ReadEEPI2C(addrPSW_EE, (uint8_t *)(&PSW_EE), sizeof(PSW_EE));						// Leggo PSW Liv 0 dalla EE
			if (PSW_EE == 0) {
				sProgVars.AccessLev = AccLevel_0;
				endPSWInput = true;
			}
		} else {
			for (j=0; j < NumeroDiPSW; j++) {													// Comparazione psw inserita con quelle del VMC
				if (j == 0) addrPSW_EE = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_1]);
				if (j == 1) addrPSW_EE = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_2]);
				if (j == 2) addrPSW_EE = EEPVMCParam16Addr(EE_ParamMisc[VMCPsw_3]);
				ReadEEPI2C(addrPSW_EE, (uint8_t *)(&PSW_EE), sizeof(PSW_EE));					// Leggo PSW dalla EE
				if (PSW_Digitata == PSW_EE) {
					sProgVars.AccessLev = j+1;
					endPSWInput = true;
					break;
				}
			}
		}
	} while (endPSWInput == false);
	
	//---  Richiesta Modifica PSW -------
	VisMessage(mReq_DaProgrammare, Row_3, 0, 0, false);											// Visualizzo richiesta di modifica attuale password
	AttesaTasto(false);
	if (Tasto == Tasto_Enter) {
		VisMessage(mInserire, Row_3, 0, 0, false);												// Visualizzo richiesta di modifica attuale password
		ProgNewPSW(sProgVars.AccessLev);														// Modifica psw
	}
	if (sProgVars.AccessLev == AccLevel_0)
	{
		sProgVars.MainStatus = FirstMenu_Liv_0;
	}
	else if (sProgVars.AccessLev == AccLevel_1)
	{
		sProgVars.MainStatus = FirstMenu_Liv_1;
	}
	else
	{
		sProgVars.MainStatus = FirstMenu_Liv_2;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: Prog_Main
 	 Funzioni di programmazione, si esce solo con un reset da tasto PROG.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Prog_Main(void) {

	memset (&sProgVars, 0x0, sizeof(sProgVars));
	sProgVars.MainStatus = Pr_AskPSWV;
	
	do {
		switch (sProgVars.MainStatus) {
		  
			case Pr_AskPSWV:
			default:
				MenuPSW();
				break;

			case Pr_Menu_TempiDosi:
				VisMessage(mMenu, Row_3, mTempiDosi, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuTempiDosi();
				}
		  		break;

#if (CESAM == false)
			case Pr_Menu_PrezziSel:
				VisMessage(mMenu, Row_3, mPrezzoSelez, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuPrezzi();
				}
		  		break;
		  		
			case Pr_Menu_ScontiSel:
				VisMessage(mMenu, Row_3, mScontoSelez, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuSconti();
				}
		  		break;
#endif
				
			case Pr_Menu_Audit:
				VisMessage(mMenu, Row_3, mMenuAudit, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuAudit();
				}
		  		break;
		  		
			case Pr_Menu_SystPagam:
				VisMessage(mMenu, Row_3, mSistemiPagamento, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuPayments();
				}
		  		break;

#if (CESAM == false)
			case Pr_Menu_AssociazSelPrez:
				VisMessage(mMenu, Row_3, mSelezPrezzo, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuTabSelezPrezzo();
				}
		  		break;
#endif
				
			case Pr_Menu_OpzVarie:
				VisMessage(mMenu, Row_3, mOpzioniVarie, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuOpzioniVarie();
				}
		  		break;
		
			case Pr_Menu_ImpostazProduttore:
				VisMessage(mMenu, Row_3, mImpostazioniProduttore, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuImpostazioniProduttore();
				}
		  		break;
		
#if (CESAM == false)
			case Pr_Menu_Collaudo:
				VisMessage(mMenu, Row_3, mCollaudo, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuCollaudo();
				}
		  		break;
#endif
				
			case Pr_Menu_FasceOrarie:
				VisMessage(mMenu, Row_3, mRisparmio, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuRisparmio();
				}
		  		break;

#if (CESAM == false)
			case Pr_Menu_SettaggiFabbrica:
				VisMessage(mMenu, Row_3, menuParamFabbrica, Row_4, true);
				if (GestTastiMainMenu() == true) {												// Tasto Enter
					MenuDefaultFabbrica();
				}
		  		break;
#endif
				
		}
	} while (true);
}

/*------------------------------------------------------------------------------------------*\
 Method: GestTastiMainMenu
	Elabora il tasto premuto e determina il successivo menu in base al livello della
 	password impostata.
	Se il menu e' inserito nella lista esclusioni, passo al menu successivo.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static bool GestTastiMainMenu(void)
{
	//static MainMenuSTAT LIV_1_EXCL_LIST[] = {Pr_Menu_FasceOrarie};
	//static MainMenuSTAT LIV_2_EXCL_LIST[] = {Pr_Menu_FasceOrarie};
	static MainMenuSTAT LIV_1_EXCL_LIST[] = {NoMoreMenu};
	static MainMenuSTAT LIV_2_EXCL_LIST[] = {NoMoreMenu};
	
	bool		endGestTasti = false;
	bool		Forward;
	uint32_t	j;
	
	do {
		AttesaTasto(false);
		if (Tasto == Tasto_Enter) return true;																// Richiesta esecuzione menu
		if (Tasto == Tasto_PrevMenu || Tasto == Tasto_NextMenu) {
			if (Tasto == Tasto_NextMenu) 
			{
				sProgVars.MainStatus++;																		// Menu successivo
				Forward = true;
			}
			else
			{
				sProgVars.MainStatus--;																		// Menu precedente
				Forward = false;
			}
			switch (sProgVars.AccessLev)
			{
				// --- Livello  0  ---
				case AccLevel_0:
				default:
					if (sProgVars.MainStatus > LastMenu_Liv_0) 	sProgVars.MainStatus = FirstMenu_Liv_0;			// Riparto dal primo menu per il Livello 0
					if (sProgVars.MainStatus < FirstMenu_Liv_0) sProgVars.MainStatus = LastMenu_Liv_0;			// Torno al menu precedente
					
					break;
			
				// --- Livello  1  ---
				case AccLevel_1:
					//if (sProgVars.MainStatus > LastMenu_Liv_1)  sProgVars.MainStatus = FirstMenu_Liv_1;			// Riparto dal primo menu per il Livello 1
					//if (sProgVars.MainStatus < FirstMenu_Liv_1) sProgVars.MainStatus = LastMenu_Liv_1;			// Torno al menu precedente
					for (j=0; j < (sizeof(LIV_1_EXCL_LIST)); j++)
					{
						if (sProgVars.MainStatus == LIV_1_EXCL_LIST[j])
						{
							if (Forward == true)
							{
								sProgVars.MainStatus++;
							}
							else
							{
								sProgVars.MainStatus--;
							}
							break;
						}
					}
					if (sProgVars.MainStatus > LastMenu_Liv_1)  sProgVars.MainStatus = FirstMenu_Liv_1;			// Riparto dal primo menu per il Livello 1
					if (sProgVars.MainStatus < FirstMenu_Liv_1) sProgVars.MainStatus = LastMenu_Liv_1;			// Torno al menu precedente
					break;
			
				// --- Livello  2  ---
				case AccLevel_2:
					for (j=0; j < (sizeof(LIV_2_EXCL_LIST)); j++)
					{
						if (sProgVars.MainStatus == LIV_2_EXCL_LIST[j])
						{
							if (Forward == true)
							{
								sProgVars.MainStatus++;
							}
							else
							{
								sProgVars.MainStatus--;
							}
							break;
						}
					}
					if (sProgVars.MainStatus > LastMenu_Liv_2)  sProgVars.MainStatus = FirstMenu_Liv_2;			// Riparto dal primo menu per il Livello 2
					if (sProgVars.MainStatus < FirstMenu_Liv_2) sProgVars.MainStatus = LastMenu_Liv_2;			// Torno al menu precedente
					break;
			}
		}
		endGestTasti = true;
	} while (endGestTasti == false);

	return false;
}






