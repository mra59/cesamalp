/****************************************************************************************
File:
    RTC_utility.h

Description:
    File con funzioni dedicate alla gestione del RTC integrato nella MPU ST

History:
    Date       Aut  Note
    Gen 2013	abe   

 *****************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <time.h>
#include <stdio.h>

#include "ORION.H"
#include "RTC_utility.h"
#include "rtc.h"
#include "VMC_CPU_HW.h"

#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	RTC_TimeTypeDef 	sTime;
extern	RTC_DateTypeDef 	sDate;
extern	void  				RTC_GetTimeAndDate(LDD_RTC_TTime *DateTime);

/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*-------------------------------------------------------------------------------------*/

LDD_RTC_TTime 	DataOra;
DATEFREE 		sDataCarta;					// Struttura con la data del FreeCredit o delle FreeVend
DATEFREE 		DataScadenzaPasvens;		// Struttura con la data di scadenza Pasvens

// Stringhe di valori prelevata dal file "RTC1.c"
static const uint8_t ULY[] = {0U,31U,28U,31U,30U,31U,30U,31U,31U,30U,31U,30U,31U};	 	// Un-leap-year
static const uint8_t  LY[] = {0U,31U,29U,31U,30U,31U,30U,31U,31U,30U,31U,30U,31U}; 		// Leap-year


/* ----------------------------------------------------------------------------
   -- ReadTimeAlarmReg
   ---------------------------------------------------------------------------- */

/**
 * Returns the content of the Time alarm register.
 * @param peripheralBase Peripheral base address.
 */
//#define RTC_PDD_ReadTimeAlarmReg() ( \
//    RTC_TAR_REG(RTC_BASE_PTR) \
//  )

/* ----------------------------------------------------------------------------
   -- WriteTimeAlarmReg
   ---------------------------------------------------------------------------- */

/**
 * Writes value to the Time alarm register.
 * @param peripheralBase Peripheral base address.
 * @param Value Value written to the Time alarm register.
 */
//#define RTC_PDD_WriteTimeAlarmReg(Value) ( \
//    RTC_TAR_REG(RTC_BASE_PTR) = \
//     (uint32_t)(Value) \
//  )


/*--------------------------------------------------------------------------------------*\
Method: LeggiOrologio
	Legge l'orologio interno alla MPU e memorizza i valori nella struttura DataOra.

Parameters:
	OUT: - Data e ora nella struttura DataOra
	
\*--------------------------------------------------------------------------------------*/
void  LeggiOrologio(void)
{
  	RTC_GetTimeAndDate(&DataOra);
}

/*--------------------------------------------------------------------------------------*\
Method: Ora_LS_align
	Determina se l'ora e il giorno sono quelli del cambio ora solare/legale ed aggiorna
	l'ora di conseguenza.
	Deve essere chiamata periodicamente.

Parameters:
	OUT: - Ora eventualmente aggiornata
	
\*--------------------------------------------------------------------------------------*/
void Ora_LS_align(LDD_RTC_TTime *DataOra)
{	
  	char 	L_flag;
	bool	fBKP;

	RTC_GetTimeAndDate(DataOra);
	L_flag = LegalTime((uint32_t)DataOra->Year, (uint32_t)DataOra->Month, (uint32_t)DataOra->Day, (uint32_t)DataOra->Hour, (uint32_t)DataOra->DayOfWeek);
	if (L_flag != NULL)
	{
		fBKP = RTC_Read_BKP();
		if (fBKP == BKP_LEGALE)																	// Gia' impostata ora legale
		{
			if (L_flag == ASCII_S)																// Impostare ora solare (SUB1H)
			{
				if (DataOra->Hour > 0)
				{
					DataOra->Hour--;
					RTC_SetDayLightTime(DataOra, BKP_SOLARE);
					DataOra->DayLightSaving = BKP_SOLARE;
				}
			}
		}
		else
		{																						// Gia' impostata ora solare
			if (L_flag == ASCII_L)																// Impostare ora legale (ADD1H)
			{
				if (DataOra->Hour < 23) 
				{
					DataOra->Hour++;
					RTC_SetDayLightTime(DataOra, BKP_LEGALE);
					DataOra->DayLightSaving = BKP_LEGALE;
				}
			}
		}
	}
}

/*-----------------------------------------------------------------------------------------*\
Method: LegalTime
	Determina se la data in parametri ricade nell'ora solare [S] o legale [L].
	Deve essere chiamata ogni qualvolta si aggiorna il RTC

Parameters:
	IN:	 - Anno, Mese, giorno, ora, giorno della settimana estratti dalla struttura "DataOra"
	OUT: - ASCII_S = SOLARE, altrimenti ASCII_L = LEGALE
	
\*-----------------------------------------------------------------------------------------*/
char LegalTime(uint32_t iYear, uint32_t iMonth, uint32_t iDay, uint32_t iHour, uint32_t iDayW)
{
	// La flag del cambio ora
 	char cFlag = NULL;

 	// Se sono ad ottobre
 	if(iMonth == STM32_RTC_OCTOBER)
 	{
    	// Se � domenica
		if(iDayW == STM32_RTC_SUNDAY)
     	{
         	// Se � l'ultima domenica
         	if((iDay + 7) > 31)
         	{
             	// Se l'ora � >= delle 3.00
             	if(iHour >= 3)cFlag = ASCII_S;
         	}
     	}
    	else
     	{
         	// Se non � domenica, ma l'ultima � gi� passata
		  	if((iDay + (7 - iDayW))> 31)
			{
				cFlag = ASCII_S;
			}
			else
			{
			  	cFlag = ASCII_L;
		
			}
		}
 	}
	// Se sono a marzo
 	else if(iMonth == STM32_RTC_MARCH)
 	{
     	// Se � domenica
     	if(iDayW == STM32_RTC_SUNDAY)
     	{
         	// Se � l'ultima domenica
         	if((iDay + 7) > 31)
         	{
             	// Se l'ora � >= delle 2.00
             	if(iHour >= 2) cFlag = ASCII_L;
         	}
     	}
     	else
     	{
         	// Se non � domenica, ma l'ultima � gi� passata
         	if((iDay + (7 - iDayW))> 31)
         	{
             	cFlag = ASCII_L;
        	 }
         	else
         	{
             	cFlag = ASCII_S;
                 
         	}
     	}
 	}
 	// Se sono nel periodo dell'ora legale
 	else if((iMonth >= STM32_RTC_APRIL) && (iMonth <= STM32_RTC_SEPTEMBER))
	{
    	cFlag = ASCII_L;
 	}
 	// Se sono nel periodo dell'ora solare
 	else if(((iMonth >= STM32_RTC_JANUARY) && (iMonth <= STM32_RTC_FEBRUARY)) ||
			((iMonth >= STM32_RTC_NOVEMBER) && (iMonth <= STM32_RTC_DECEMBER)))
 	{
     	cFlag = ASCII_S;
 	}
 return cFlag;
 }

/*-----------------------------------------------------------------------------------------*\
Method: FromSecondsToDateTime
	Converte "Seconds", che rappresenta i secondi trascorsi dal 1/1/2000, nel formato
	della struttura "LDD_RTC_TTime".
	Routine esportata dal file "RTC1.c"

Parameters:
	IN:	 - Seconds
	OUT: - Anno, Mese, giorno, ora, giorno della settimana, minuti e secondi nella struttura
		   passata come parametro
\*-----------------------------------------------------------------------------------------*/
void  FromSecondsToDateTime(uint32_t Seconds, uint32_t *data, uint *time) {	

	//uint32_t 		x;
	//uint32_t 		Days;
	LDD_RTC_TTime	LocalDataOra;
  	
	time_t		rawtime = (time_t)Seconds;
	struct tm	*NewTime;
	NewTime = localtime(&rawtime);
	NewTime->tm_year += 1900;
	NewTime->tm_mon++;
	
	LocalDataOra.Second = NewTime->tm_sec;
	LocalDataOra.Minute = NewTime->tm_min;
	LocalDataOra.Hour = NewTime->tm_hour;
	LocalDataOra.Day = NewTime->tm_mday;
	LocalDataOra.Month = NewTime->tm_mon;
	LocalDataOra.Year = NewTime->tm_year;
	
/*	
	Days = Seconds / 86400U;             								// Days
	Seconds = Seconds % 86400U;          								// Seconds left
	LocalDataOra.Hour = Seconds / 3600U;								// Hours 
	Seconds = Seconds % 3600u;           								// Seconds left 
	LocalDataOra.Minute = Seconds / 60U;    							// Minutes 
	LocalDataOra.Second = Seconds % 60U;     							// Seconds
	LocalDataOra.DayOfWeek = (Days + 6U) % 7U; 							// Day of week
	LocalDataOra.Year = (4U * (Days / ((4U * 365U) + 1U))) + 1970U; 	// Year 
	Days = Days % ((4U * 365U) + 1U);
	if (Days == ((0U * 365U) + 59U))
	{ 																	// 59
		LocalDataOra.Day = 29U;
		LocalDataOra.Month = 2U;
		return;
	}
	 else if (Days > ((0U * 365U) + 59U))
	{
		Days--;
	}
	else
	{
	}
	x =  Days / 365U;
	LocalDataOra.Year += x;
	Days -= x * 365U;
	for (x=1U; x <= 12U; x++) {
		if (Days < ULY[x]) {
			LocalDataOra.Month = x;
			break;
		} else {
			Days -= ULY[x];
		}
	}
	LocalDataOra.Day = Days + 1U;
	*/
	
	// Format della Data/Ora nei bytes di destinazione
	*data = LocalDataOra.Year%100;
	*data *=  10000;
	*data += ((LocalDataOra.Month * 100)  + LocalDataOra.Day);
	*time = ((LocalDataOra.Hour * 100) + LocalDataOra.Minute);

}

/*-----------------------------------------------------------------------------------------*\
Method: IsPeriodElapsed
	Determina se e' trascorso il periodo passato come parametro.
	Si controlla anche che l'orologio interno non riporti una data inferiore a quella sulla
	carta.
 
	IN:	 - pointer alla struttura DATEFREE della Carta che contiene anche il periodo 
	OUT: - true se period elapsed, altrimenti false
\*-----------------------------------------------------------------------------------------*/
bool  IsPeriodElapsed(DATEFREE *DataCarta) {
	
	uint8_t	TimeDiff, GiorniMeseCarta;
	uint8_t	Clk_Day, Clk_Month, Clk_Year;
	
	if (DataCarta->DofWk > 6 || DataCarta->Giorno > 31 || DataCarta->Mese > 12 || DataCarta->Anno > 99) return false;
//	if (DataCarta->DofWk > 6 || DataCarta->Giorno == 0 || DataCarta->Giorno > 31 || 
//			DataCarta->Mese == 0 || DataCarta->Mese > 12 || DataCarta->Anno > 99) return false;			// Data non corretta

	Clk_Day   = (uint8_t)DataOra.Day;
	Clk_Month = (uint8_t)DataOra.Month;
	Clk_Year  = (uint8_t)(DataOra.Year - MILLENNIO_2000);
	
	if (Clk_Year < DataCarta->Anno) return false;									// Clock indietro rispetto alla data sulla carta
	TimeDiff = (Clk_Year - DataCarta->Anno);										// Differenza tra gli anni delle due date
	if (TimeDiff > 1) return true;													// Trascorso PIU' di un anno tra le due date quindi sicuramente periodo trascorso	
	if (TimeDiff == 0) {
			
		// ******************************************************
		// ********* Stesso Anno della carta  *******************
		// ******************************************************
			
		if (Clk_Month < DataCarta->Mese) return false;								// Clock indietro rispetto alla data sulla carta
		if (Clk_Month == DataCarta->Mese) {
			
			// -------  Stesso Anno Stesso Mese   --------------
			if (Clk_Day < DataCarta->Giorno) return false;							// Clock indietro rispetto alla data sulla carta
			TimeDiff = (Clk_Day - DataCarta->Giorno);								// Giorni di differenza nello stesso mese
		} else {
			
			// ------  Stesso Anno Mese Diverso  ---------------
			TimeDiff = (Clk_Month - DataCarta->Mese);
			if (TimeDiff > 1) return true;											// 2 mesi di differenza quindi sicuramente periodo trascorso
			
			// ------- Mesi Adiacenti: calcolo giorni di distanza -------
			if (DataCarta->Periodo == mensile) return true;							// Nuovo mese quindi periodo trascorso
			if ( (Clk_Year%4) == 0) {
				GiorniMeseCarta = LY[DataCarta->Mese];								// Anno bisestile
			} else {
				GiorniMeseCarta = ULY[DataCarta->Mese];								// Anno NON bisestile
			}
			TimeDiff = (GiorniMeseCarta - DataCarta->Giorno) + Clk_Day;				// Giorni residui del mese precedente e giorni passati del nuovo mese
		}
	} else {
			
		// ******************************************************
		// **** Anno successivo a quello della carta  ***********
		// ******************************************************
		
		if (Clk_Month >= DataCarta->Mese) return true;								// Trascorsi 12 mesi o piu' quindi sicuramente periodo trascorso
		TimeDiff = ((Clk_Month + 12) - DataCarta->Mese);							// Mesi di differenza
		if (TimeDiff > 1) return true;												// Trascorso piu' di un mese tra le due date quindi sicuramente periodo trascorso
		
		// ------- Mesi Adiacenti a cavallo di un Anno  ---------
		if (DataCarta->Periodo == mensile) return true;								// Nuovo mese quindi periodo trascorso
		TimeDiff = (31 - DataCarta->Giorno) + Clk_Day;								// Giorni residui del mese precedente (Dic) + giorni trascorsi del nuovo mese (Gen)
	}
	switch (DataCarta->Periodo) {

		case giornaliero:
			return (TimeDiff > 0 ? true : false);
		
		case settimanale:
			if (DataCarta->DofWk == 0 && TimeDiff > 0) return true;					// Passato sicuramente almeno un Lunedi' quindi nuova settimana
			if ((DataCarta->DofWk + TimeDiff) > 6 ) {
				return true;														// Passato un Lunedi' quindi nuova settimana
			} else {
				return false;														// Non ancora trascorsa la settimana
			}

		case quindicinale:
			if (DataCarta->DofWk == 0 && TimeDiff > 7) return true;					// Passati due Lunedi' quindi periodo trascorso
			if ((DataCarta->DofWk + TimeDiff) > 14 ) {
				return true;														// Trascorse le due settimane
			} else {
				return false;														// Non ancora trascorse le due settimane
			}

		default:
			return false;															// Periodo non ancora trascorso
	}
}

/*-----------------------------------------------------------------------------------------*\
Method: Ckeck_SystemClock
	Controlla se i valori nell'orologio del sistema sono leciti.
 
	IN:	 -  
	OUT: - true se clock FAIL, altrimenti false
\*-----------------------------------------------------------------------------------------*/
bool  Ckeck_SystemClock() {

	if ((uint8_t)DataOra.Day == 0 || (uint8_t)DataOra.Day > 31 ||
		(uint8_t)DataOra.Month == 0 || (uint8_t)DataOra.Month > 12 ||
		DataOra.Year < 2015) {
		return true;
	} else {
		return false;
	}
	
}
