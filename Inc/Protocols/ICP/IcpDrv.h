/****************************************************************************************
File:
    ICPDrv.h

Description:
    Include file con definizioni e strutture per i driver ICP

History:
    Date       Aut  Note
    Set 2017 	MR

*****************************************************************************************/

#ifndef __ICPUSDHook_h
#define __ICPUSDHook_h

#ifndef kUSDExpFFDataSize
#define kUSDExpFFDataSize (ICP_MAX_BUFFSIZE-2-1)
#endif

#ifndef _IcpSetModeBit_
#define _IcpSetModeBit_			true
#endif
#ifndef _IcpChkModeBit_
#define _IcpChkModeBit_			true
#endif
#ifndef _IcpSlaveInterByteTm_
#define _IcpSlaveInterByteTm_	false
#endif


#ifndef _IcpTestRetry_
#define _IcpTestRetry_			false
#endif

#if _IcpTestRetry_
bool fTxRetry = false;
#endif


#if kIcpRs232Support
#undef _IcpSetModeBit_
#undef _IcpChkModeBit_
#undef _IcpSlaveInterByteTm_
#define _IcpSlaveInterByteTm_	false
#define _IcpSetModeBit_	false
#define _IcpChkModeBit_	false
#endif


void ICPTxEmptyInd(void);
void ICPTxComplete(void);
void ICPDataInd(uint8_t nData);
void ICPErrInd(uint8_t nData, uint8_t rxErr);
void ICPDrvSendRW(uint8_t nReservedWord);
void ICPDrvSendMsg(uint8_t nFrameLen);
void ICPDrvHandleComState(void);
uint8_t ICPDrvGetComState(void);
void ICPDrvSetComState(uint8_t nComState);
void ICPDrvStartRX(void);
bool ICPDrvTestChecksum(void);
void ICPDrvTestEndFrame(bool icpMasterMode);
void ICPTimerStart(uint16_t nTimerID);
bool ICPTimerTimeout(uint16_t nTimerID);
void  TestTimeoutNoLink(void);


#endif	// End __ICPUSDHook_h