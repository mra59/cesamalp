/* ***************************************************************************************
File:
    OrionCredit.c

Description:
    Gestione credito carta e cash
    

History:
    Date       Aut  Note
    Set 2012 	MR   

 *****************************************************************************************/

#include "ORION.H" 
#include "OrionCredit.h"
#include "VMC_GestSelez.h"
#include "OrionTimers.h"
#include "VMC_CPU_LCD.h"
#include "Power.h"
#include "Totalizzazioni.h"
#include "IcpDrv.h"
#include "Monitor.h"
#include "IICEEP.h"
#include "icpCPCProt.h"

#include "Dummy.h"



// ===  dichiarazione variabili e strutture   ===

uint8_t		Manifacturer[16];
uint8_t		VMC_Response, VMC_NumeroSelezione;

uint16_t	response, VMC_CreditoDaVisualizzare;
uint16_t	VMC_ValoreSelezione, CreditoMancante, CostoRealeSelezione;

uint32_t	cryptmsg[2];
uint32_t	MaxRevalue;

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	LDD_RTC_TTime 	DataOra;
extern	DATEREFUNDS		DataRefundValues, DataRefundFC, DataRefundFV, DataRefundCashCB;
extern	bool  			SuspendWriteNew_FC_FV(void);
extern	void  			AzzeraCreditCard(void);
extern	void  			Init_CRC(uint16_t Seme);
extern	uint16_t  		Calc_CRC_GetCRC16(uint16_t Dato);
extern	void 			CheckChgCashType(void);
extern	void 			CheckBillCashType(void);
extern	bool ICPProt_CHGGetReady(void);
extern	bool ICPCHGHook_Change(void);
extern	bool ICPCPCVendSuccess(uint16_t nSelNumber);
extern	void ICPCPCHookSetMaxRevalue(CreditCpcValue CostoSelezione);
extern	bool ICPCPCRevalueRequest(CreditCpcValue nRevalueAmount, uint16_t nCurrencyCode);
extern	bool ICPCPCVendFailure(void);
extern	bool ICPCPCVendReq(CreditCpcValue nSelPrice, uint16_t nSelNumber, uint16_t nCurrencyCode);
extern	void			RevalueKR(uint8_t, uint32_t, uint8_t);
extern	void			Accendi_LedGiallo(void);
extern	void  			Test_FC_FV_AllaSelezione(void);
extern	void 			Overpay(uint8_t);
extern	bool  			SaveRefund(REFUNDS* DestRefund, DATEREFUNDS* DestDateRefund, TipiRefund TipoRefund, CreditCpcValue RefundValue, bool TestPWD);
extern	bool  			ReadEEPI2C(uint16_t AddrEEP, uint8_t *AddrDest, uint16_t size);


#if (CESAM == false)
	extern	void  			SetDoorOpenDelay(void);
#endif

/*---------------------------------------------------------------------------------------------*\
Method: CashlessTask
	Gestione stato cashless interno

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void CashlessTask(void) {
	
	uint32_t	TestSecure;
	//MR19 uint16_t	TempCash;
	
	
	if (gVMC_ConfVars.ProtMDBSLV) return;													// DA as USD Slave: non c'e' nulla da fare qui
	TestTimeoutNoLink();
	switch (gOrionKeyVars.State) {

// ========================================================================
// ========    ENABLED   CASE           ===================================
// ========================================================================
	
	case kOrionCPCStates_Enabled:
		gOrionKeyVars.Idle_To_Enable = false;												// Sono in ENABLE quindi posso azzerare la flag "from IDLE to Enable"
		if (gOrionKeyVars.Inserted)
		{
#if	!SaltaSicurezza
			if (!gOrionKeyVars.CheckSecure1)
			{
				gOrionKeyVars.CheckSecure1 = true;											// Controllo Sicurezza carta
				TestSecure = Manifacturer[12];
				TestSecure |= Manifacturer[13]<<8;
				TestSecure |= Manifacturer[14]<<16;
				TestSecure |= Manifacturer[15]<<24;
				if (cryptmsg[1] != TestSecure)
				{
					gOrionKeyVars.RFFieldFree = true;
					gOrionKeyVars.Inserted = false;
					return;
				}
			}
#endif			
			CreditUpdate(); //ABELE																// Aggiorna credito sul VMC executive quando entra una carta
			CheckCashType();																	// predispongo per controllare abilitazione banconote/monete
			SetVisualAll();																		// Sul display locale visualizza tutte le cifre: serve a far vedere all'utente che la carta e' stata accettata ma e' senza credito.

#if (CESAM == false)
			if (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_VENDITE_PROVA)
			{
				SetDoorOpenDelay();
			}
#endif
			
			gOrionKeyVars.State = kOrionCPCStates_Idle;
			return;
		}
		else
		{
			// Controllo timeout eventuale credito refund card su display
			if (gOrionKeyVars.failvendcardcredit && TmrTimeout(gOrionCashVars.creditTmr))
			{
				gOrionKeyVars.failvendcardcredit = 0;
				CreditUpdate(); //ABELE															// Aggiorna credito sul VMC executive quando entra una carta
			}
		}
		break;

// ========================================================================
// ========    IDLE      CASE           ===================================
// ========================================================================
		
	case kOrionCPCStates_Idle:
		
		if (gVMC_ConfVars.CPC_MDB == true)
		{																						// CPC MDB
			if (gOrionKeyVars.Inserted == false)
			{
				AzzeraCreditCard();
				CreditUpdate();																	// Azzera credito sul VMC executive quando esce la carta
				ClrVisualAll();																	// Sul display locale visualizza le sole cifre significative
				CheckCashType();
				gOrionKeyVars.State = kOrionCPCStates_Enabled;
				break;
			}
			else
			{
				if (Is_CPC_Idle_State() == false)												// Con carta presente anche il CPC deve essere in IDLE
				{
					ICPProt_Init();																// Reset protocollo MDB e periferiche
				}
			}
		}

		if (gOrionKeyVars.Idle_To_Enable) {
			// ==============================================================
			// ********  Attesa conclusione Session Cancel Request **********
			break;																				// Attendo conclusione della Session Cancel Request e ritorno in ENABLE
		} else {
			if (gOrionKeyVars.CaricamBlocchi) {
				// ==============================================================
				// **************  Idle Caricamento a blocchi *******************
				if (CashCaricamentoBlocchi == 0) {												// Cash CB = zero: e' stato riaccreditato sulla carta CB o su una utente
					if (gOrionKeyVars.Inserted) {												// Carta inserita: se CB esco in attesa di estrazione, se utente termino IDLE_CB												
						if (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_CBLK) {
							break;																// Reinserita Carta CB e riaccreditato blocco: attendo estrazione
						} else {
							goto Termina_IDLE_CB;												// Inserita carta utente e accreditato blocco: termino IDLE_CB per visualizzare
						}																		// credito carta utente oppure zero se scrittura fallita
					} else {																	// Credito CB zero e carta estratta: termino IDLE_CB
						AzzeraCreditCard();
						goto Termina_IDLE_CB;
					}
				} else {
					// Cash caricamento presente: controllo timeout se non c'e' carta
					if (gOrionKeyVars.Inserted) {
						break;
					} else {
						if (TmrTimeout(gOrionCashVars.creditTmr)) {							// Timeout Cash CB scaduto: salvo in EEPROM il CB e termino IDLE_CB
							//MR19 TempCash = CashCaricamentoBlocchi;
							PSHProcessingData();												// Azzero dati di caricamento
							CashCaricamentoBlocchi = 0;
							ValoreBloccoCaricamento = 0;
							//MR19 SaveRefund(&RefundCashCB, &DataRefundCashCB, TipoRefundCaricamBlk, TempCash, true);		// Cash CB in Refund EEPROM
							AzzeraCreditCard();
							goto Termina_IDLE_CB;
						} else {
							break;																// Timeout NON scaduto: esco
						}
					}
				}
				
			} else {
				// ==============================================================
				// ********** Idle normale: controllo estrazione carta **********
				if (!gOrionKeyVars.Inserted) {
					AzzeraCreditCard();
					CreditUpdate(); //ABELE														// Azzera credito sul VMC executive quando esce la carta
					ClrVisualAll();																// Sul display locale visualizza le sole cifre significative
					CheckCashType();
					gOrionKeyVars.State = kOrionCPCStates_Enabled;
					break;
				} else {
					SelezPrenotata();
				}
				break;
			}
		}

		// ==============================================================
		// **************      Termino IDLE        **********************
Termina_IDLE_CB:																				// Timeout cash CB scaduto o credito restituito alla CB o caricato su utente

		gOrionKeyVars.CaricamBlocchi = false;
		if (gOrionKeyVars.RFFieldFree) {														// RFFieldFree=1 quindi c'e' stato un errore nella scrittura carta
			AzzeraCreditCard();																	// Azzero qui il credito alrimenti non si sarebbe piu' cancellato
			CreditUpdate();																		// Aggiorna credito sul VMC Executive
			CheckCashType();
			gOrionKeyVars.State = kOrionCPCStates_Enabled;										// Torno immediatamente a ENABLE
		}
		break;

// ========================================================================
// ========    VEND  REQUEST   CASE     ===================================
// ========================================================================
	case kOrionCPCStates_Vend:
		
		if (gOrionKeyVars.Inserted)
		{																						// Credito da Carta, Mifare Locale o CPC
			response = GestioneStatoIdle();														// Determina costo finale selezione
			if (response == Resp_OK)
			{
				//  *******************************************************
				//  **********  CARD  STANDARD   VEND  ********************
				//  *******************************************************
				response = TestCondizioniPerVendita(gOrionKeyVars.VendAppAmount);
				if (response == Resp_OK)
				{
					VendAmountKR = gOrionKeyVars.VendAppAmount;									// Uso VendAmountKR per non cambiare il file Vendita.c
					if (gVMC_ConfVars.PresMifare == true)
					{
						//-- Mifare Locale ---
						if (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_VENDITE_PROVA)
						{																		// Set Audit per Test Vend 
							SetDatiAuditVendita(VEND_CON_KR_PROVA);
						}
						else
						{
							SetDatiAuditVendita(VEND_CON_KR);									// Set Audit per vendita con carta utente 
						}
						CreditWriteStart(enCreditWriteVend);									// Inizio scrittura carta Mifare Locale
						TmrStart(gVMCState.ToutFase, VEND_TOUT_MIFARE_LOCALE);					// Tout attesa fine scrittura carta Mifare
						gOrionKeyVars.State = kOrionCPCStates_VendWr;							// main state = kOrionCPCStates_VendWr
					}
					else
					{
						//-- Cashless  MDB  ----	
						if (ExtCPC_CreditWriteStart(gOrionKeyVars.VendAppAmount, gOrionKeyVars.VendSel) == false)
						{																		// Inviato Vend Request al CPC MDB 
							SetDatiAuditVendita(VEND_CON_KR);									// Set Audit per vendita con carta utente
							TmrStart(gVMCState.ToutFase, VEND_TOUT_PERIPH_MDB);					// Tout attesa fine scrittura CPC MDB
							gOrionKeyVars.State = kOrionCPCStates_VendWr;						// main state = kOrionCPCStates_VendWr
							return;
						}
						else
						{
							Monitor_DeniedReason(FailToSendMDBVendReq);							// Se Monitor attivo visualizzo motivo del denied
							VendAuthorResult(false);											// Vend Denied alla Gestione Selezione                                               	
							gOrionKeyVars.State = kOrionCPCStates_Idle;
						}
					}
					return;
				}
				else
				{
					CreditoMancante = gOrionKeyVars.VendAppAmount;
				}
			}
			else
			{
				// ****  Vend Denied  *******
			}
		}
		else
		{
			//  *******************************************************
			//  ******  CASH   VEND  PARALLELO   o   MDB    ***********
			//  *******************************************************
			response = Resp_OK;																	// Per evitare continue trasmissioni ad AWS quando premuto un pulsante senza credito
			if (gVMC_ConfVars.ProtMDB == false)
			{
				if (fVMCFreeVendRequired)
				{
					SetDatiAuditVendita(VEND_SALE_FREE);										// Free-Vend request
					VendAuthorResult(true);														// Vend Approved alla Gestione Selezione 
					gOrionKeyVars.State = kOrionCPCStates_VendEndWait;
					return;
				}
			}
			if (!CheckVendInh())
			{																					// Sistema NON inibito alla vendita
				SetCostoSelezioneConCash();														// Ridetermina costo selezione
				if (gOrionCashVars.creditCash >= gOrionKeyVars.VendAppAmount)
				{
					gOrionCashVars.creditCash -= gOrionKeyVars.VendAppAmount;					// Scalo valore selezione dal totale cash
					VendAmountSEL = 0;
					if (CashDaBankReader >= gOrionKeyVars.VendAppAmount)
					{
						VendAmountBR = gOrionKeyVars.VendAppAmount;								// Cash Bill sufficiente
					}
					else
					{
						VendAmountBR = CashDaBankReader;										// Cash Bill insufficiente: scalo cash bill
						VendAmountSEL = gOrionKeyVars.VendAppAmount - CashDaBankReader;			// e rimanente da cash selettore
					}
					PSHProcessingData();														// Scalo il valore selezione dal reale credito cash
					CashDaSelettore -= VendAmountSEL;
					CashDaBankReader -= VendAmountBR;
					MemoCashAdd(0, kCreditCashTypeCoin, NocheckPWD);							// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
					MemoCashAdd(0, kCreditCashTypeBill, NocheckPWD);							// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
					SetDatiAuditVendita(VEND_CON_CASH);
					VendAuthorResult(true);														// Vend Approved alla Gestione Selezione                                               	
					CreditUpdate();																// Aggiorna credito da visualizzare
					gOrionKeyVars.State = kOrionCPCStates_VendEndWait;
					if (gVMC_ConfVars.ProtMDB == true)
					{
						gVars.EnablePayout = true;												// Effettuata una vendita, payout abilitato (per Multi-vend)
					}					
					return;
				}
				else
				{																				// Credito insufficiente
					CreditoMancante = gOrionKeyVars.VendAppAmount;
				}
			}
		}
		//  **********  Vend Denied  ********************
		Monitor_DeniedReason(response);															// Se Monitor attivo visualizzo motivo del denied
		VendAuthorResult(false);																// Vend Denied alla Gestione Selezione                                               	
		gOrionKeyVars.State = kOrionCPCStates_Idle;
		break;
	

// ********************************************************************************
// **************        Attendo    fine     scrittura    carta    ****************
// ********************************************************************************
	case kOrionCPCStates_VendWr:

		if (gVMC_ConfVars.PresMifare == true)
		{
			// --------------  Attendo fine scrittura carta   MIFARE   LOCALE  ----------------
			if(CreditWriteEnd() || TmrTimeout(gVMCState.ToutFase))
			{
				if((CreditWriteOk() && TmrTimeout(gVMCState.ToutFase) == false))
				{
					VendAuthorResult(true);															// Vend Approved alla Gestione Selezione
					CreditUpdate();																	// Aggiorna credito da visualizzare
					VMC_Response = VMC_RESP_NO_RESPONSE;
					gOrionKeyVars.State = kOrionCPCStates_VendEndWait;
				}
				else
				{
					// --  Scrittura   FALLITA  --
					StatusVendita = VEND_CardWriteErr;												// Registra Audit Estesa per errore scrittura carta al Vend Request.
					VendAuthorResult(false);														// Vend Denied alla Gestione Selezione
					gOrionKeyVars.State = kOrionCPCStates_Idle;
				}
				gOrionKeyVars.WriteNewFreeCred = false;												// Clr flag ricarica FreeCredit
				gOrionKeyVars.WriteNewFreeVend = false;												// Clr flag ricarica FreeVend
			}
		}
		else
		{
			// --------------  Attendo fine aggiornamento CPC  MDB   ----------------
			if (gOrionKeyVars.CreditWriteEnd || TmrTimeout(gVMCState.ToutFase))
			{
				if ((gOrionKeyVars.ExtKeyVendApp) && (TmrTimeout(gVMCState.ToutFase) == false))
				{
					// --  Aggiornamento   OK   CPC  MDB  --
					VendAuthorResult(true);															// Vend Approved alla Gestione Selezione
					CreditUpdate();																	// Aggiorna credito da visualizzare
					VMC_Response = VMC_RESP_NO_RESPONSE;
					gOrionKeyVars.State = kOrionCPCStates_VendEndWait;
				}
				else
				{
					// --  Aggiornamento FAIL  CPC  MDB  --
					StatusVendita = VEND_CardWriteErr;												// Registra Audit Estesa per errore scrittura carta al Vend Request e azzera flags e stati dalla "GestioneAuditVendita"
					if (TipoVendita == NO_VEND_IN_CORSO)
					{
						TipoVendita = VEND_CON_KR;													// Set Tipovendita per registrare ExtAudit
					}
					VendAuthorResult(false);														// Vend Denied alla Gestione Selezione
					gOrionKeyVars.State = kOrionCPCStates_Idle;
					if (TmrTimeout(gVMCState.ToutFase))												// Se la causa e' stato il timeout attesa risposta dal CPC, lo resetto
					{
						CPCInit(ICP_CPC_ADDR);														// RESET Cashless MDB

					}
				}
			}
		}
	break;
		
// ********************************************************************************
// **************  Attendo Fine Vend: puo' essere OK o FAIL     *******************
// ********************************************************************************
	case kOrionCPCStates_VendEndWait:
		
		if (VMC_Response == VMC_RESP_NO_RESPONSE) break;										// Attendo fine selezione
		
		if (VMC_Response == VMC_RESP_VEND_OK) {
			// ----------  VEND    SUCCESS    ----------------------
			gOrionKeyVars.State = kOrionCPCStates_Idle;
			SetMaxRevalue();																	// Determino credito caricabile sulla Carta
			StatusVendita = VEND_SUCCESS;														// MR Audit
			CheckCashType();   																	// Predispongo per controllare abilitazione banconote/monete
			if (TipoVendita == VEND_CON_CASH) {													// CASH VEND
				// ------ CASH  VEND  ---------
				CaricaToutCash();																// Ricarico Timeout Cash
				if (gVMC_ConfVars.ProtMDB == true) {											// MDB: potrebbe esserci la RR
		  			if (ICPProt_CHGGetReady()) {												// RR presente
		  				if (!MultiVend) ICPCHGHook_Change();									// CASH VEND MDB: in Single Vend do il resto
		  			}
				}
			} else {
				// ------ CARD  VEND  ---------
				if (gVMC_ConfVars.PresMifare == false) {
					ICPCPCVendSuccess(gOrionKeyVars.VendSel);									// Vend Success al CPC MDB
					ICPCPCHookSetMaxRevalue(gOrionKeyVars.VendAppAmount);						// Determino credito caricabile sulla Carta
				}
			}
		} else {
			// ----------  VEND    FAILURE    -----------------------
			// -- Gestisco FAIL VEND in kOrionCPCStates_Refund sia per 
			// Mifare Locale, che CPC MDB o Cash da selettore/CHG-BILL
			gOrionKeyVars.State = kOrionCPCStates_Refund;									
		}
		VMC_Response = VMC_RESP_NO_RESPONSE;
		break;
		
// ========================================================================
// ========    REVALUE   CASE           ===================================
// ========================================================================
	case kOrionCPCStates_Revalue:
		
		response = GestioneStatoIdle();
		if (gVMC_ConfVars.PresMifare == true)
		{
			// ---    Mifare Locale   ----
			if ((gOrionKeyVars.Inserted) && (response == Resp_OK))
			{
				gOrionKeyVars.State = kOrionCPCStates_RevalueWr;
				CreditWriteStart(enCreditWriteRevalue);
			}
			else
			{
				// TODO: se la carta e' ancora presente ma non e' stato possibile aggiungere il credito cash, questo dovrebbe essere messo in overpay
				// oppure verificare cosa succede lasciandolo in uso insieme alla carta
				gOrionKeyVars.State = kOrionCPCStates_Idle;
			}
		}
		else
		{
			// ---    CPC   MDB   ------
			if (response == Resp_OK)
			{
				if (ICPCPCRevalueRequest(gOrionKeyVars.RevalueAmount, UnitScalingFactor) == false)
				{																				// MDB Revalue Request predisposto
					gOrionKeyVars.CreditWriteEnd 	= false;
					gOrionKeyVars.ExtKeyRevalueApp 	= false;
					TmrStart(gVMCState.ToutFase, REVALUE_TOUT_PERIPH_MDB);						// Tout attesa fine scrittura CPC MDB
					gOrionKeyVars.State = kOrionCPCStates_RevalueWr;
				}
				else
				{
					gOrionKeyVars.State = kOrionCPCStates_Idle;									// Errore invio comando Revalue, torno a Idle per riprovarci
				}
			}
			else
			{
				gOrionKeyVars.State = kOrionCPCStates_Idle;										// Carta non ricaricabile, torno a Idle 
			}
		}
		break;
		
	// =======================================================================
	// -----  Fase 2 Revalue: Attesa fine scrittura cash su carta  ----------- 
	// =======================================================================
	
	case kOrionCPCStates_RevalueWr:
		
		if (gVMC_ConfVars.PresMifare == true)
		{
			// ---    Mifare Locale   ----
			if(CreditWriteEnd())
			{
				gOrionKeyVars.State = kOrionCPCStates_Idle;
				if(CreditWriteOk())
				{																				// Scrittura corretta
					RechargeFlags &= ~(CaricamentoDubbio);										// Non aggiorno counter "EA2*EK_03 accredito dubbio"
				}
				RechargeFlags &= ~(WaitEndCashWrite);											// Esegui audit caricamento cash
			}
		}
		else
		{
			// --------------  Attendo fine aggiornamento CPC  MDB   ----------------
			if (gOrionKeyVars.CreditWriteEnd || TmrTimeout(gVMCState.ToutFase))
			{
				if (TmrTimeout(gVMCState.ToutFase))
				{
					CPCInit(ICP_CPC_ADDR);														// RESET Cashless MDB
					gOrionKeyVars.State = kOrionCPCStates_Idle;
				}
				else
				{
					gOrionKeyVars.State = kOrionCPCStates_Idle;
					if(gOrionKeyVars.ExtKeyRevalueApp == true)
					{																			// Revalue Approved
						RechargeFlags &= ~(CaricamentoDubbio);									// Non aggiorno counter "EA2*EK_03 accredito dubbio"
					}
				}
				RechargeFlags &= ~(WaitEndCashWrite);											// Esegui audit caricamento cash
				gOrionKeyVars.CreditWriteEnd 	= false;
				gOrionKeyVars.ExtKeyRevalueApp 	= false;
			}
		}
		break;

		
// ========================================================================
// ========    REFUND DA FAIL VEND  CASE   ================================
// ========================================================================
//  Se selezione effettuata con cash, set StatusVendita ed esco: altrimenti
//	tento di riaccreditare sulla carta quanto scalato per la selezione.
//	Se la carta non e' piu' nel lettore memorizzo il suo credito refund in EEPROM.
//	In MDB si risponde Vend Fail.
//	Il set di StatusVendita e' stato spostato all'interno della Elabora_Refund()
//	perche' puo' essere chiamata in modo forzoso dal Temporarystate senza
//	passare da questo case.
	
	case kOrionCPCStates_Refund:
	
		if (TipoVendita == VEND_CON_CASH) {
			PSHProcessingData();
			StatusVendita = VEND_FAILURE;														// Set StatusVendita per non lasciare stati pendenti
			CashDaSelettore += VendAmountSEL;													// Riaccredito cash
			CashDaBankReader += VendAmountBR;
			MemoCashAdd(0, kCreditCashTypeCoin, NocheckPWD);									// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
			MemoCashAdd(0, kCreditCashTypeBill, CheckPWD);										// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
			CaricaToutCash();																	// Ricarico Timeout Cash
			CMCreditAdd(&gOrionCashVars.creditCash, (VendAmountSEL + VendAmountBR));
			CreditUpdate();																		// Update visual credit (06.09.17 aggiunto perche' l'USD non riceve il credito aggiornato)
			gOrionKeyVars.State = kOrionCPCStates_Idle;
		} else {
			if (gVMC_ConfVars.PresMifare == true) {												// Mifare Locale
				Elabora_Refund();
			} else {																			// CPC MDB
			  	if (ICPCPCVendFailure() == false) {												// Predisposto invio Vend Fail al CPC senza errori
					StatusVendita = VEND_FAILURE;												// Per eseguire la ResetDatiVendita in "vendita.c"
					//MR19 Credo sia un errore, CreditUsedKR e' 0 con CPC MDB: uso VendAmountKR
					//MR19 gOrionKeyVars.Credit += CreditUsedKR;										// Per la visualizzazione riaccredito Valore del Credito usato per la vendita
					gOrionKeyVars.Credit += VendAmountKR;										// Per la visualizzazione riaccredito Valore del Credito usato per la vendita
					
					SetMaxRevalue();															// Determino credito caricabile sulla Carta
					CreditUpdate();																// Aggiorna credito da visualizzare
					gOrionKeyVars.State = kOrionCPCStates_Idle;
				}
			}
		}
		break;

/* ---------------------------------------------------------------------------------------------------------------
	Arrivo qui solo se alla ricezione di "FAIL VEND" la carta era ancora inserita: se fallisce il refund 
	significa che la carta e' stata estratta mentre si stava scrivendo il nuovo credito, pertanto non memorizzo
	nulla perche' si rischia di riaccreditare piu' volte il valore della selezione fallita.
	In MDB si risponde ACK: non e' piu' prevista la risposta "REFUND ERR" perche' non gestita dai DA. */
	
	case kOrionCPCStates_RefundWr:

		Elabora_FineWriteRefund();
		break;

	}	// FINE Switch (gOrionKeyVars.State)

/*====================================================================================================*/

	// ***************  KEY  INS   STATE   ************************
	if (gOrionKeyVars.State == kOrionCPCStates_KeyIns) {
/*	Inserendo e togliendo velocemente la carta succede che lo stato dell'Orion non e' piu' sincrono
	con lo stato dell'MDB poiche' l'Orion cambia stato immediatamente quando sente la carta estratta
	mentre l'MDB cambia gli stati in funzione delle chiamate del master.
	Togliendo immediatamente la carta l'Orion torna in Enable e cancella le flasg  ICP_CPCINFO_BEGINSESSION
	e ICP_CPCINFO_SESSCANCREQ ma, se l'MDB ha gia' iniziato ad inviare Begin Session, lo stato MDB rimane
	in IDLE e non e' piu' possibile inviare il Session Cancel Request.
	Anche il passaggio da IDLE a ENABLE e' comandato dall'MDB e non piu' direttamente qui nell'OrionCredit
	nel case IDLE.
	Tolgo queste istruzioni dalla Rev. 1.33 dell'MDB (27.05.2014) 
		
		if (!gOrionKeyVars.CaricamBlocchi) {
			if (!gOrionKeyVars.Inserted) {												// Carte estratte subito dopo l'inserimento: torno in Enable
				gOrionKeyVars.State = kOrionCPCStates_Enabled;
			}
			if(OpMode == MDB) {
				if (!gOrionKeyVars.Inserted) {											// Carte estratte subito dopo l'inserimento: cancello richiesta Begin Session
					ICPProt_CPCResetEvtMask(ICP_CPCINFO_BEGINSESSION | ICP_CPCINFO_SESSCANCREQ);
				} else {
					ICPProt_CPCResetEvtMask(ICP_CPCINFO_SESSCANCREQ);					// Carta presente: cancello richiesta Session Cancel Request
				}			
			}
		}
	}
	if (OpMode == MDB) {
		if (ICPProt_CPCIsSetEvtMask(ICP_CPCINFO_BEGINSESSION && !gOrionKeyVars.CaricamBlocchi)) {
			if(!gOrionKeyVars.Inserted) {												// Carte estratte subito dopo l'inserimento: cancello richiesta Begin Session
				ICPProt_CPCResetEvtMask(ICP_CPCINFO_BEGINSESSION);
			}
		}
*/	
	}

}
	
/******************************************************************************************************/
/*******************          F I N E    C A S H L E S S     T A S K     ******************************/
/******************************************************************************************************/

/*---------------------------------------------------------------------------------------------*\
Method: CreditWriteStart
	Predispone la scrittura sulla carta interna che verra' fatta partire nel MAIN, oppure
	invia il comando appropriato al cashless MDB esterno.
	Dalla funzione si potrebbe uscire senza aver dato il via alla scrittura se mancasse il link
	col VMC.

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
static bool CreditWriteStart(uint8_t wrtype) 
{
	if (!fVMC_NoLink) {									// ABELE se VMC in nolink non parte la scrittura
		gOrionKeyVars.CreditWrite = true;				// Nel Main parte la chiamata alla Write
		gOrionKeyVars.CreditWriteEnd = false;
		gOrionKeyVars.CreditWriteCnt = 0;
	}
	return gOrionKeyVars.CreditWrite;
}

/*---------------------------------------------------------------------------------------------*\
Method: CreditWriteEnd
	Funzione chiamata per sapere se e' terminata una scrittura della carta interna.
	Se e' terminata, sia che lo sia effettivamente o che sia fallita, azzera la flag
	"gOrionKeyVars.CreditWrite" altrimenti nel main rimarrebbe continuamente nel loop di 
	scrittura.

	IN:		- 
	OUT:	- True = terminata; False = in corso
\*----------------------------------------------------------------------------------------------*/
bool CreditWriteEnd(void) {
	
	if (gOrionKeyVars.CreditWrite && (!gOrionKeyVars.CreditWriteEnd && (gOrionKeyVars.CreditWriteCnt < kMaxCreditWriteCnt))) {
		return false;
	}
	gOrionKeyVars.CreditWrite = false;
	return true;
}

/*---------------------------------------------------------------------------------------------*\
Method: CreditWriteOk
	Ritorna lo stato di scrittura sia della carta interna che di quella esterna MDB.
	La flag "gOrionKeyVars.CreditWriteEnd", che e' anche la flag di scrittura OK, e' azzerata
	per evitare che questa funzione restituisca OK anche quando e' chiamata senza che la 
	CreditWriteStart sia stata fatta partire: capita se la CreditWriteStart e' chiamata mentre
	la flag "fNoLink" e' stata settata da timeout di protocollo. 

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
static bool CreditWriteOk(void) {
	
	bool	TestOk;

	TestOk = gOrionKeyVars.CreditWriteEnd;
	gOrionKeyVars.CreditWriteEnd = false;
	return (TestOk);
}


/*---------------------------------------------------------------------------------------------*\
Method: ExtCPC_CreditWriteStart
	Invia il comando Vend Request al cashless MDB esterno.

	IN:	 - 
	OUT: - false se Vend Request inviato al Cashless MDB, altrimenti true
\*----------------------------------------------------------------------------------------------*/
static bool ExtCPC_CreditWriteStart(CreditCpcValue VendAmount, uint16_t NumSel)
{
	gOrionKeyVars.ExtKeyVendApp  = false;														// Predispongo Vend Denied
	if (ICPCPCVendReq(VendAmount, NumSel, (uint16_t)UnitScalingFactor) == false)
	{																							// Invio comando Vend Request con successo 
		gOrionKeyVars.CreditWriteEnd = false;
		return false;
	}
	return true;
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*MR22  Funzione fino alla Rev. 1.53
static void ExtCPC_CreditWriteStart(CreditCpcValue VendAmount, uint16_t NumSel) {

	if (ICPCPCVendReq(VendAmount, NumSel, (uint16_t)UnitScalingFactor) == false) {				// Invio comando Vend Request con successo 
		gOrionKeyVars.CreditWriteEnd = false;
	} else {
		gOrionKeyVars.CreditWriteEnd = true;													// Invio comando Vend Request Fallito = Vend Denied
		gOrionKeyVars.ExtKeyVendApp  = false;
	}
}
*/

/*---------------------------------------------------------------------------------------------*\
Method: KeyEnabled

	IN:		- enable=true se MDB state >= ENABLED; enable=false se MDB state Inactive o Disabled
	OUT:	- uscita = enable
\*----------------------------------------------------------------------------------------------*/
bool KeyEnabled(bool enable) {
	// Svuotata il 04.07.14 perche' faceva terminare la sessione dopo la vendita con quelle gettoniere
	// doppio protocollo che non comunicano col PK3 durante la vendita.
	// Scattava il timeout attesa comunicazione e questa funzione era chiamata dalla "ICPHook_SetLink"
	// in ICPHook.c con parametro = false, causando il clear della Inserted con conseguente richiesta
	// di terminare la sessione alla fine della vendita.
/*	
	gOrionKeyVars.InitEnd = enable;
	if(!enable) {
		gOrionKeyVars.Inserted = enable;
	}
*/	
	return enable;
}

/*---------------------------------------------------------------------------------------------*\
Method: Set_AttesaEstrazione
	Set flag RFFieldFree per forzare l'estrazione della carta dal campo e FirstAccess per poter
	eventualmente visualizzare su PC l'errore rilevato.

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  Set_AttesaEstrazione(void)
{
	gOrionKeyVars.RFFieldFree = true;
	gOrionKeyVars.FirstAccess = true;
}

/*---------------------------------------------------------------------------------------------*\
Method: SetMaxRevalue
	Determina il credito caricabile sulla Carta solo se Mifare locale.
	Con Cashless MDB l'aggiornamento e' fatto nella ICPCPCHook. 
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  SetMaxRevalue() {

	if (gVMC_ConfVars.PresMifare == true) {
		if ((gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_VENDITE_PROVA) && !gOrionConfVars.RechTestVendCard) {			// Carta Test-Vend non ricaricabile 
			MaxRevalue = NULL_VALUE;																				// Inhibit accettazione cash
		} else {
			if (gOrionKeyVars.Credit > MaxRecharge || gOrionConfVars.InhRechargeCard) {
				MaxRevalue = NULL_VALUE;																			// Credito carta superiore al CredMax o ricarica inibita
			} else {
				MaxRevalue = MaxRecharge - gOrionKeyVars.Credit;
			}
		}
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: GestioneStatoIdle
	Funzione chiamata da VEND REQUEST e da REVALUE REQUEST
	Gestione Idle State della carta interna: controlla se rifiutare la vendita e determina 
	il costo finale della selezione, oppure verifica se possibile revalue.
	
	2022.07: con cashless MDB si testa solo VendPending

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
uint16_t GestioneStatoIdle(void)
{
	uint16_t	status;
	uint32_t	TestSecure;
	
    // VEND DENIED se vera anche solo una delle condizioni sotto riportate
	if (gOrionKeyVars.State == kOrionCPCStates_Vend)
	{
		if (gVMC_ConfVars.CPC_MDB == true)
		{
	
			if (TipoVendita != NO_VEND_IN_CORSO) return (status = VendPending);
		
		}
		else
		{
			if (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_CBLK)
			{
				return (status = VendDenPerTipoKey);
			}
			else
			{
				if (TipoVendita != NO_VEND_IN_CORSO)
				{
					return (status = VendPending);
				}
				else
				{
					if (CashCaricamentoBlocchi > NULL_VALUE)
					{
						return (status = VendDenPerCash);
					}
					else
					{
						if (CheckVendInh())
						{
							return (status = VendDenOFFDDCMPExtFull);
						}
					}
				}
			}
		}
	}
    status = Resp_OK;
	switch(gOrionKeyVars.KR_TipoChiave) {
	
    case KR_CHIAVE_UTENTE :
#if	!SaltaSicurezza
		if (!gOrionKeyVars.CheckSecure2) {
			gOrionKeyVars.CheckSecure2 = true;																			// Controllo Sicurezza carta
			TestSecure = Manifacturer[12];
			TestSecure |= Manifacturer[13]<<8;
			TestSecure |= Manifacturer[14]<<16;
			TestSecure |= Manifacturer[15]<<24;
			if ( cryptmsg[1] != TestSecure ) {
				gOrionKeyVars.RFFieldFree = true;
				gOrionKeyVars.Inserted = false;
				return (status = GeneralError);
			}
		}
#endif

		if (gOrionKeyVars.Prepag) {
			if (gOrionKeyVars.State == kOrionCPCStates_Vend) {
				SetCostoSelezioneConCarta();																			// Ridetermina costo selezione
			} else {
				return (status = VendDenPerTipoKey);				
			}
		} else {
			if ((gOrionKeyVars.State == kOrionCPCStates_Revalue) && !gOrionConfVars.InhRechargeCard ) {					// REVALUE REQUEST
/*  07.01.14 MR In accordo con Abele decidiamo di accreditare sempre il cash alla carta
				if (gOrionKeyVars.RevalueAmount > MaxRevalue) {
					gOrionKeyVars.RevalueAmount = NULL_VALUE;
					return (status = GeneralError);
				}
*/			
				if ((gOrionKeyVars.Credit + gOrionKeyVars.RevalueAmount) <= MAX_SYSTEM_CREDIT) {
					if (SuspendWriteNew_FC_FV()) {																		// Se ci sono FC/FV alla selezione lo sospende perche' questa e' una revalue
						CreditUpdate();																					// Aggiorna credito da visualizzare
					}
					gOrionKeyVars.Credit += gOrionKeyVars.RevalueAmount;
					if (gOrionCashVars.creditCash > 0) {
						SottraeCash((uint16_t)gOrionKeyVars.RevalueAmount);
						gOrionCashVars.creditCash = TotaleCash();
					}
					RevalueKR(REVALUE_KR_CASH, gOrionKeyVars.RevalueAmount, (CaricamentoDubbio + WaitEndCashWrite));	// Predispongo update Audit caricamento cash con caricamento dubbio
					SetMaxRevalue();																					// Determino credito caricabile sulla Carta
				} else {
					VMC_CreditoDaVisualizzare = gOrionCashVars.creditCash;												// Credito non ricaricabile: visualizzo il solo credito cash..
					Accendi_LedGiallo();																				// ..inserito. Accendo il led giallo e attendo estrazione carta
					Set_AttesaEstrazione();
					return (status = GeneralError);
				}
			} else {
				if (gOrionKeyVars.State == kOrionCPCStates_Vend) {														// VEND REQUEST
					Test_FC_FV_AllaSelezione();																			// Concedo FreeCredit/FreeVend se pendenti
					SetCostoSelezioneConCarta();																		// Ridetermina costo selezione
				}
			}
		}

		break;

    case KR_CHIAVE_VENDITE_PROVA :
		if (gOrionKeyVars.State == kOrionCPCStates_Revalue && gOrionConfVars.InhRechargeCard) status = GeneralError;	// Non dovrebbe mai arrivare a revalue perche' MaxRevalue=NULL
		if (gOrionKeyVars.State == kOrionCPCStates_Revalue && gOrionKeyVars.VendNeg) status = GeneralError;				// Non ammessa ricarica da Negative Vend
		gOrionKeyVars.VendAppAmount = gOrionKeyVars.VendReq;															// Default: prezzo intero
		break;

    case KR_CHIAVE_CBLK :
    	if (gOrionKeyVars.State == kOrionCPCStates_Revalue || gOrionKeyVars.State == kOrionCPCStates_Vend) status = GeneralError;	// Non ammesse Revalue e Vend request
		break;

	default:
		status = GeneralError;

    }   // End Switch (gOrionKeyVars.KR_TipoChiave)

	return status;
	
}

//******************************************************************************************************
//*******************       I  N  I  Z  I  O       C  A  S  H      T  A  S  K       ********************
//******************************************************************************************************
//*************************************************************************
// * ABELE
// * Credit Master procedure for BillValidator and ChangeGiver
// * Credit Master functions to manage Cash Credit with/without Key 
// ************************************************************************

/*------------------------------------------------------------------------------------------*\
 Method: CashTask
	Verifica se attivare il revalue cash sulle carte e se e' scaduto il timeout cash per
	azzerarlo in Overpay.

   IN:  -  
  OUT:  -  
\*------------------------------------------------------------------------------------------*/
void  CashTask(void) {
	
	if (gOrionKeyVars.Inserted && !(fVMC_NoLink) && (gOrionKeyVars.State == kOrionCPCStates_Idle)) {
		if (gOrionCashVars.creditCash > 0) {
			gOrionKeyVars.RevalueAmount = gOrionCashVars.creditCash;							// In "RevalueAmount" il credito cash da aggiungere alla carta
			gOrionKeyVars.State = kOrionCPCStates_Revalue;
			CheckCashType();   																	// predispongo per controllare abilitazione banconote/monete
		}
		return;																					// C'e' una chiave Mifare
	}
	
	// Controlla se c'e' del credito cash che deve andare in overpay
	if (gOrionCashVars.creditCash && IsOrionInStBy() && !gOrionConfVars.NoTimeCash && TmrTimeout(gOrionCashVars.creditTmr)) {
	 	if (!gOrionCashVars.givingChange && !gOrionKeyVars.Inserted) {							// Cash in overpay se payout non in corso e non c'e' carta
			Overpay(OVERPAY_CASH);
			gOrionCashVars.creditCash = 0;
			CheckCashType();
			CMUpdateCI();
	   	}
	}
	if (gOrionCashVars.creditCash == 0 && gVars.EnablePayout) {									// Clr flag Payout abilitato quando il cash si azzera
		gVars.EnablePayout = false;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: CashTaskInit
	All'accensione recupera l'eventuale credito dalla EEprom perche' sia visualizzato e
	utilizzabile

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  CashTaskInit(void) {
	  
	CMCreditAdd(&gOrionCashVars.creditCash, CashDaSelettore);
	CMCreditAdd(&gOrionCashVars.creditCash, CashDaBankReader);
	if (gOrionCashVars.creditCash) {
		CreditUpdate();																			// Aggiorna "VMC_CreditoDaVisualizzare"
		CaricaToutCash();																		// Carico Timeout Cash
	}
}

/*--------------------------------------------------------------------------------------*\
Method: CheckCashType.
	Aggiorna lo stato di Enable/Disable di Bill Reader e Change Giver MDB.

	IN:  - 
   OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  CheckCashType(void)  {

	CheckChgCashType();
	CheckBillCashType();
}
 
/*------------------------------------------------------------------------------------------*\
 Method: CreditUpdate
	Somma tutti i crediti carta e cash per essere visualizzati sul VMC.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
 void CreditUpdate(void) {
	 
	VMC_CreditoDaVisualizzare = (TotCreditCard() + gOrionCashVars.creditCash + gOrionKeyVars.failvendcardcredit);		// Aggiorno credito sul VMC  ABELE
}

/*------------------------------------------------------------------------------------------*\
 Method: DeterminaExactChange
	Verifica la quantita' di monete nei tubi della RR MDB per determinare lo stato di 
	Exact-Change e segnalarlo al VMC.
	Se la RR non e' collegata allora non ci sono monete nei tubi e la flag Ex-Ch e' set.
	Il tubo guasto concorre automaticamente, senza essere testato, a determinare l'Ex-Ch 
	perche' trasmette NumCoin=0, quindi sotto il minimo.
	La flag "gOrionConfVars.EquazExCh" sceglie se lo stato di Exact-Change e' determinato
	da qualsiasi tubo vuoto (flag=0) oppure dal tubo di valore inferiore (flag=1).

   IN:  - numero minimo monete nei tubi
  OUT:  - True se in Exact-Change, altrimenti false
\*------------------------------------------------------------------------------------------*/
bool  DeterminaExactChange(uint8_t MinCoinsNum) {

#if (_IcpMaster_ == true)	 
	uint8_t 	ncoin;
	uint16_t 	FlagCoinInTube, nCoinTypeEnabled, nMask;
	
	FlagCoinInTube = GetnCoinTypeInTubes();														// In FlagCoinInTube i 16 bit di flag con le monete destinate nei tubi
	if (FlagCoinInTube) {																		// Almeno una moneta va in tubo
		nCoinTypeEnabled = GetnCoinTypeEnabled();												// In nCoinTypeEnabled i 16 bit di flag con le monete abilitate
		ClrCHGInfoExChange();																	// Predispongo Ex-Ch clr
		if (gOrionConfVars.EquazExCh == false) {
			// *****  Qualsiasi tubo sotto il min o guasto determina Ex-Ch set  ****
			for (ncoin=0; ncoin < ICP_MAX_COINTYPES; ncoin++) {
				nMask = 1 << ncoin;
				if ((FlagCoinInTube & nMask) && (nCoinTypeEnabled & nMask)) {					// Moneta in tubo e abilitata
					if ((GetCoinsInTube(ncoin) < MinCoinsNum)) {
						SetCHGInfoExChange();													// Tubo sotto il minimo o guasto Ex-Ch set
						break;
					}
				}
			} 
		} else {
			// **** Tubo di valore inferiore sotto il min o guasto determina Ex-Ch set  ****
			for (ncoin=0; ncoin < ICP_MAX_COINTYPES; ncoin++) {
				nMask = 1 << ncoin;
				if ((FlagCoinInTube & nMask) && (nCoinTypeEnabled & nMask)) {					// Prima moneta abilitata che va in tubo: in MDB e' quella di valore inferiore
					if ((GetCoinsInTube(ncoin) < MinCoinsNum)) {
						SetCHGInfoExChange();													// Tubo sotto il minimo o guasto Ex-Ch set
					}
					break;																		// Esco
				}
			} 
		}
	} else {
		SetCHGInfoExChange();																	// Non ci sono monete destinate nei tubi o non c'e' la RR: set Ex-Ch
	}
 	return fExactChange;

#else
 	return false;
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: ExChStatusChanged
	Controlla se lo stato di Exact-Change e' cambiato per aggiornare il display.
	Lo stato di Exact-Change puo' cambiare anche senza incasso o vendita cash: quando,
	ad esempio, la RR MDB comunica tutti i dati all'accensione, la visualizzata al VMC
	e' gia' stata inviata con Ex-Ch set e se i tubi sono pieni si deve inviare di nuovo
	un'altra visualizzata per togliere il messaggio di resto vuoto.

Parameters:
	IN	-
	OUT	- true se lo stato e' cambiato
\*--------------------------------------------------------------------------------------*/
bool ExChStatusChanged(void) {
	
	if (gOrionKeyVars.VariazExCh) {
		if (fExactChange == false) {
			gOrionKeyVars.VariazExCh = false;
			return true;
		}
	} else {
		if (fExactChange) {
			gOrionKeyVars.VariazExCh = true;
			return true;
		}
	}
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: TestExch_InOut
	Controlla se e' cambiato lo stato di Exact change per memorizzare l'evento.
	Lo stato di Ex-Ch e' memorizzato in BackupRAM cosi' da ripartire sempre dallo stato
	corrente ed evitare che ad ogni accensione si registri un evento perche' non si sa se
	partire da clr o set. 

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  TestExch_InOut(void) {

#if kApplExChEvents 

	if (fExactChange) {
		// ********   Exact change   **********
		if (Ex_Ch_Status == false) {															// Sono in Ex-Ch e non lo ero prima
			SetDataTime(&DataExChEvent, &TimeExChEvent);										// Leggo data/ora ad indicazione entrata in Ex-Ch
			PSHProcessingData();
			Ex_Ch_Status = true;																// Set Exact-Change as status attuale
			InsertEvent (kInExactChange, DataExChEvent, TimeExChEvent, true);
		}
	} else {
		// ********  No Exact change **********
		if (Ex_Ch_Status == true) {																// NON sono in Ex-Ch ma prima lo ero
			SetDataTime(&DataExChEvent, &TimeExChEvent);										// Leggo data/ora ad indicazione uscita dall'Ex-Ch
			PSHProcessingData();
			Ex_Ch_Status = false;																// Clr Exact-Change as status attuale
			InsertEvent (kOutExactChange, DataExChEvent, TimeExChEvent, true);
		}
	}
#endif
}

/*------------------------------------------------------------------------------------------*\
 Method: SetAlreadyExChange
	Set flag gia' in Exact change e memorizzo attuale Time.
	Chiamata all'accensione per partire con la stato di Exact change di default con lo
	scopo di conteggiare il tempo nel quale il sistema rimane in Ex-ch.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  SetAlreadyExChange(void) {

#if kApplExChEvents 
	gOrionCashVars.AlreadyExch = true;
	gOrionCashVars.Time_Exch_IN = RTC_Counter;
#endif
}

/*--------------------------------------------------------------------------------------*\
Method: CreditMasterCashAdd
    Aggiunge credito cash al credito cash esistente: chiamata quando si inserisce cash.

   IN:  - cash da aggiungere e tipo di sorgente cash
  OUT:  -  
\*--------------------------------------------------------------------------------------*/
void CreditMasterCashAdd(CreditValue amount, CreditCashTypes cashType) {
	
	gOrionKeyVars.failvendcardcredit = 0;														// Azzero eventuale refund carta su display
	CMCreditAdd(&gOrionCashVars.creditCash, amount);											// Aggiungo il cash alla variabile RAM
	MemoCashAdd(amount, cashType, CheckPWD);													// Memo somma dei cash in BackupRAM e calcolo CRC16
    CMUpdateCI();
    CaricaToutCash();																			// Start Timeout Cash
    CheckCashType();																			// Aggiorno abilitaz/inibiz coin/bill MDB
}

/*--------------------------------------------------------------------------------------*\
Method:  CMCreditAdd
    Somma un credito ad un altro credito, controllando che non si superi il valore
    massimo ammesso per il size delle variabili credito.

   IN:  - pointer al credito da aumentare e valore da aggiungere
  OUT:  - zero se credito sommato nei limiti, altrimenti il credito che non e' stato 
  	  	  possibile aggiungere
\*--------------------------------------------------------------------------------------*/
static CreditValue CMCreditAdd(CreditValue* credit, CreditValue amount) {

	CreditValue maxCash;
	uint32_t		newCash;

	newCash = *credit + amount;
    maxCash = MAX_SYSTEM_CREDIT;
    if (newCash > maxCash || newCash < *credit) {												// Se newcash < della somma dei crediti appena eseguita significa che c'e' overflow
    	newCash = amount - (MAX_SYSTEM_CREDIT - *credit);										// In newCash quanto credito NON e' stato possibile sommare
    	*credit = MAX_SYSTEM_CREDIT;															// Limita il valore al massimo rappresentabile
        return (CreditValue) newCash;
    }
    *credit = (CreditValue)newCash;
    return 0;
}

/*--------------------------------------------------------------------------------------*\
Method: CMUpdateCI
    Update credit info
\*--------------------------------------------------------------------------------------*/
static void CMUpdateCI(void) {
	
    CreditUpdate(); //ABELE																		// Aggiorna credito da visualizzare
}

/*--------------------------------------------------------------------------------------*\
Method: CreditMasterEscrowInd
    Funzione chiamata alla pressione della leva di escrow per chiedere il resto.
    La richiesta e' ignorata se e' in corso una vendita o si sta gia' dando il resto
    o c'e' una carta inserita.

   IN:  - gOrionKeyVars.State e gOrionKeyVars.Inserted
  OUT:  - 
\*--------------------------------------------------------------------------------------*/
void CreditMasterEscrowInd(void) {

	if (!gOrionCashVars.givingChange && IsOrionInStBy()) {
		CreditMasterGiveChange();
    }
}

/*------------------------------------------------------------------------------------------*\
 Method: CreditMasterGiveChange
	Inizia la sequenza di payout se c'e' credito cash da rendere, se tale cash e' inferiore 
	al Resto Massimo impostato da configurazione, oppure se il Resto Massimo e' disabilitato
	(MaxChange = 0).

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void CreditMasterGiveChange(void) {

	CreditValue currCredit = gOrionCashVars.creditCash;
	CreditValue maxChange = MaxChange;

    if (gOrionCashVars.creditCash == 0) return;													// Non c'e' credito cash da rendere
    if (currCredit <= maxChange || maxChange == 0) {
		gOrionCashVars.givingChange = true;
		gOrionCashVars.StartChange = true;														// Start payout
		return;
    }
    // No driver has accepted the pay-out request. Consider pay-out phase completed
    CreditMasterChangePaidOutInd(0);
}

/*------------------------------------------------------------------------------------------*\
 Method: CreditMasterChangePaidOutInd
	Funzione chiamata al termine dell'erogazione resto.
	Comparo la somma dei resi parziali (CashResoParziale) con il totale reso (amount); se 
	non coincidono i casi sono 2:
	1) TotaleReso < CashResoParziale: utilizzo CashResoParziale se non supera il credito
	   CashDaRendere, altrimenti considero il credito reso tutto.
	2) TotaleReso > CashResoParziale: utilizzo TotaleReso.

   IN:  - in amount credito totale restituito, CashResoParziale e CashDaRendere
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void CreditMasterChangePaidOutInd(CreditValue amount) {
	
	CreditValue TotaleReso, CreditoNonReso;
	
	if (amount >= CashResoParziale) {															// 12.09.15 Modif. test da == a >=: Baltom in Spagna ha avuto un resto di 80 Euro
		TotaleReso = amount;																	// causato dal TotaleReso non inizializzato perche' amount era > CashResoParziale.
	} else {
		if (amount < CashResoParziale) {														// Utilizzo il valore piu' sfavorevole all'utente perche' potrebbe essere lui
			if (CashResoParziale <= CashDaRendere) {											// la causa di questa differenza (ad es OFF-ON change giver)
				TotaleReso = CashResoParziale;
			} else {
				TotaleReso = CashDaRendere;
			}
		}
	}
	PSHProcessingData();
	CashResoParziale = 0;
	if (TotaleReso == CashDaRendere) {															// Reso tutto: annullo CashDaRendere e CashResoParziale
		CashDaRendere = 0;
		PSHCheckPowerDown();
	} else {																					// Cash reso solo parzialmente: riaccredito il residuo.
		if (TotaleReso > CashDaRendere) {														// 12.09.15 Aggiunto come ulteriore sicurezza per non avere un credito cash
			TotaleReso = CashDaRendere;															// sbagliato esageratamente (oltre i 65500)
		}
		CreditoNonReso = CashDaRendere - TotaleReso;
		CashDaRendere = 0;																		// 09.07.15 Azzerarlo perche' non c'e' stato un PWD e il residuo verra' ora aggiunto
																								// in CashDaSelettore; CashDaRendere e' usato per avere l'overpay cash nel caso di
																								// PWD durante il payout, non quando la RR non puo' dare tutto il resto.
		// Controllo se il CreditoNonReso ci sta tutto in CashDaSelettore,
		// altrimenti lo splitto tra CashDaSelettore e CashDaBankReader.
		if ((CashDaSelettore+CreditoNonReso) >= CashDaSelettore) {								// Ci sta tutto nei 2 uint8_t di CashDaSelettore
			CashDaSelettore += CreditoNonReso;
			CreditoNonReso = 0;
		} else {
			CreditoNonReso -= (MAX_SYSTEM_CREDIT-CashDaSelettore);								// CashDaSelettore fino a MAX_SYSTEM_CREDIT, il residuo in CashDaBankReader
			CashDaSelettore = MAX_SYSTEM_CREDIT;
		}
		MemoCashAdd(0, kCreditCashTypeCoin, NocheckPWD);										// Uso la MemoCashAdd sommando zero per salvare in BackupRAM e calcolare il CRC16
		if (CreditoNonReso > 0) {																// Non e' possibile accreditarlo tutto al CashSelettore
			MemoCashAdd(CreditoNonReso, kCreditCashTypeBill, NocheckPWD);						// Sommo il resto a CashDaBankReader
		}
		PSHCheckPowerDown();
		gOrionCashVars.creditCash = TotaleCash();												// In "gOrionCashVars.creditCash" somma di CashDaSelettore+CashDaBankReader
	    
		if (!MultiVend && gOrionCashVars.creditCash < MinResidue) {								// Credito residuo in overpay se in single vend e inferiore a MinResidue
	        Overpay(OVERPAY_CASH);
	        gOrionCashVars.creditCash = 0;
	        CheckCashType();
	    }
	}
    CreditUpdate();																				// Aggiorna credito da visualizzare
    gOrionCashVars.givingChange = false;														// Payout terminato
}

/*--------------------------------------------------------------------------------------*\
Method: CreditMasterCoinInInd
  A coin has been accepted and deposited

Parameters:
  coinType  ->  coin channel
  return --> value of Coin/USF
\*--------------------------------------------------------------------------------------*/
void CreditMasterCoinInInd(uint8_t coinType) {

}

/*--------------------------------------------------------------------------------------*\
Method: CreditMasterCoinsDispManInd
  Indicate that a coin has been dipensed manually

Parameters:
  coinType    ->  which coin
\*--------------------------------------------------------------------------------------*/
void CreditMasterCoinsDispManVal(uint8_t coinType) {

}

/*------------------------------------------------------------------------------------------*\
 Method: IsReaderStateEnabled
   Controlla se il reader e' nello stato di Enable (usata per verificare se il reader 
   puo' accettare la carta in ingresso)

   IN:  - gOrionKeyVars.State 
  OUT:  - true se reader nello stato di Enable
\*------------------------------------------------------------------------------------------*/
bool  IsReaderStateEnabled(void) {
	return (gOrionKeyVars.State == kOrionCPCStates_Enabled ? true : false);
}

/*------------------------------------------------------------------------------------------*\
 Method: IsReaderIdleCB
   Controlla se il reader e' nello stato di Idle con Caricamento a Blocchi (usata per 
   verificare se il reader puo' accettare una carta Utente in ingresso).

   IN:  - gOrionKeyVars.State 
  OUT:  - true se reader nello stato di Idle con CB
\*------------------------------------------------------------------------------------------*/
bool  IsReaderIdleCB(void) {
	return ( (gOrionKeyVars.State == kOrionCPCStates_Idle && gOrionKeyVars.CaricamBlocchi) ? true : false);
}

/*------------------------------------------------------------------------------------------*\
 Method: IsOrionInStBy
   Controlla se il reader e' in St-By ovvero in Enable o Idle state e senza carta.

   IN:  - gOrionKeyVars.State e gOrionKeyVars.Inserted
  OUT:  - true se reader in Enable o Idle o carta inserita, altrimenti false
\*------------------------------------------------------------------------------------------*/
bool  IsOrionInStBy(void)
{
	return ((gOrionKeyVars.State <= kOrionCPCStates_Idle) || (gOrionKeyVars.State == kOrionCPCStates_Enabled) || gOrionKeyVars.Inserted);
}

/*---------------------------------------------------------------------------------------------*\
 Method: SetPrice_e_Selnum
   Determina prezzo e numero della selezione richiesta in base all'opzione Price Holding, la
   quale vale sia per l'Executive che per l'MDB.
   Questa funzione e' chiamata anche come USD Slave per determinare il costo della selezione.

   IN:  -  valore e battuta
  OUT:  -  true se selezione trovata con "gOrionKeyVars.VendReq" e "gOrionKeyVars.VendSel" set
\*---------------------------------------------------------------------------------------------*/
bool  SetPrice_e_Selnum(uint16_t valore, uint16_t battuta) {

	bool	trovatoprice;
	uint16_t	prezzosel, numerosel;
	
	if (gVMC_ConfVars.Gratuito == true) {														// DA in gratuito
		if (battuta == NULL_VALUE) return false;
		prezzosel = 0;
		trovatoprice = true;
		numerosel = battuta;
	} else {
		if ((gVMC_ConfVars.PresMifare) || (gVMC_ConfVars.CPC_MDB) ||
			(gVMC_ConfVars.ProtMDBSLV) ||
		    ((gVMC_ConfVars.ExecutiveSLV) && (gOrionConfVars.PriceHolding == false)))  {		// DA come credit master Mifare o Executive Slave Std o CPC MDB

			if (battuta > LenListaPrezzi || battuta == NULL_VALUE) return false;
			trovatoprice = false;
			prezzosel = MRPPrices.Prices99.MRPPricesList99[battuta-1];
			if (prezzosel == PrezzoNonDisponibile) return false;								// Prezzo non valido
			trovatoprice = true;
			numerosel = battuta;
		} else {
			return false;
		}
	}
	if (trovatoprice) {
		gOrionKeyVars.VendReq 	= prezzosel;
		gOrionKeyVars.VendSel 	= numerosel;
		gOrionKeyVars.VendNeg 	= false;
		VMC_ValoreSelezione		= prezzosel; 
		VMC_NumeroSelezione 	= numerosel;
		return true;
	} else {
		return false;
	}
}

/*----------------------------------------------------------------------------------------------*\
 Method: SetCostoSelezioneConCash
   Determina il costo ultimo della selezione effettuata con credito cash.

   IN:  -  
  OUT:  -  
\*----------------------------------------------------------------------------------------------*/
void  SetCostoSelezioneConCash(void) {

	gOrionKeyVars.VendAppAmount = gOrionKeyVars.VendReq;							// Default: prezzo intero
}

/*----------------------------------------------------------------------------------------------*\
 Method: SetCostoSelezioneConCarta
   Determina il costo ultimo della selezione effettuata con credito carta.

   IN:  -  valore e numero selezione richiesta in gOrionKeyVars.VendReq e gOrionKeyVars.VendSel.
  OUT:  -  true e gOrionKeyVars.VendAppAmount se deve essere scalata qualche cifra dalla carta.
\*----------------------------------------------------------------------------------------------*/
bool  SetCostoSelezioneConCarta(void) {

	uint8_t			UserGroup;
	SizeEE_Discount	Sconto;

	gOrionKeyVars.VendAppAmount = gOrionKeyVars.VendReq;										// Default: prezzo intero
	if (gVMCState.USD_Ext_SelezReq) return false;												// Selez richiesta da USD Slave esterno: non applico calcoli
	NumLista = 0;
	if (gOrionConfVars.CardLevelsEnabled) {
		// Uso Livelli carte
		if (gOrionKeyVars.VendSel != NULL_VALUE) {
			UserGroup = gOrionKeyVars.LivelliChiave>>8;											// Lo UserGroup e' il MSB
			switch (UserGroup) {
				case (1):
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPA[gOrionKeyVars.VendSel-1]), (uint8_t *)(&gOrionKeyVars.VendAppAmount), sizeof(SizeEEPrezzo));
					NumLista = 1;
					break;
				case (2):
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPB[gOrionKeyVars.VendSel-1]), (uint8_t *)(&gOrionKeyVars.VendAppAmount), sizeof(SizeEEPrezzo));
					NumLista = 2;
					break;
				case (3):
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPC[gOrionKeyVars.VendSel-1]), (uint8_t *)(&gOrionKeyVars.VendAppAmount), sizeof(SizeEEPrezzo));
					NumLista = 3;
					break;
				case (4):
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPD[gOrionKeyVars.VendSel-1]), (uint8_t *)(&gOrionKeyVars.VendAppAmount), sizeof(SizeEEPrezzo));
					NumLista = 4;
					break;
			}
		}
	} else {
		if (gOrionConfVars.DiscountEnabled && gOrionKeyVars.VendSel != NULL_VALUE) {
			if (gOrionConfVars.ListaPrezzo) {																											// Sconti abilitati
				// Le liste sono PREZZI
				if (gOrionKeyVars.InFasciaSconti) {
					// Applico Lista Prezzi N. 2
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPB[gOrionKeyVars.VendSel-1]), (uint8_t *)(&gOrionKeyVars.VendAppAmount), sizeof(SizeEEPrezzo));
					NumLista = 2;
				} else {
					// Applico Lista Prezzi N. 1
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPA[gOrionKeyVars.VendSel-1]), (uint8_t *)(&gOrionKeyVars.VendAppAmount), sizeof(SizeEEPrezzo));
					NumLista = 1;
				}
			} else {
				// Le liste sono SCONTI
				if (gOrionKeyVars.InFasciaSconti) {
					// Applico Lista Sconti N. 2
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPB[gOrionKeyVars.VendSel-1]), (uint8_t *)(&Sconto), sizeof(SizeEEPrezzo));
					NumLista = 2;
				} else {
					// Applico Lista Sconti N. 1
					ReadEEPI2C(IICEEPConfigAddr(EEPrezzoPA[gOrionKeyVars.VendSel-1]), (uint8_t *)(&Sconto), sizeof(SizeEEPrezzo));
					NumLista = 1;
				}
				if (Sconto <= gOrionKeyVars.VendReq) {
					gOrionKeyVars.VendAppAmount = gOrionKeyVars.VendReq - Sconto;																		// Applico Sconto
				}
			}
		}
	}

	return (gOrionKeyVars.VendAppAmount == PrezzoNonDisponibile) ? false : true;
}

/*------------------------------------------------------------------------------------------*\
 Method: SetDatiAuditVendita
   Predispone le variabili utilizzate in "Vendita.c" per generare l'audit vendita.

   IN:  -  
  OUT:  -  
\*------------------------------------------------------------------------------------------*/
void  SetDatiAuditVendita(uint8_t TipoVend) {

	PSHProcessingData();
	CreditoMancante 	= 0;											// Queste istruzioni erano eseguite dalla funzione "TestVendRequest" in "Vendita.c"
	ValoreSelezione 	= gOrionKeyVars.VendReq;						// subito dopo il set della flag "fVMC_VenditaRichiesta": ora questa flag e' stata 
	CostoRealeSelezione = gOrionKeyVars.VendAppAmount;					// eliminata e portate qui le righe di codice della "TestVendRequest"
	NumeroSelezione 	= gOrionKeyVars.VendSel;
	CodiceUtenteKR 		= (uint16_t) gOrionKeyVars.CodiceUtente;			// Codice Utente carta
	Ex_Ch_InCashVend 	= fExactChange;									// Stato di Ex-Ch nella vendita con Cash
    if (gOrionKeyVars.KR_TipoChiave == KR_CHIAVE_ASSENTE) {
    	NewCreditKR 	= 0;											// Non c'e' Credito carta
    	NewFreeCreditKR = 0;											// non c'e' Free Credit
    } else {
    	NewCreditKR 	= (uint16_t) gOrionKeyVars.Credit;				// Credito carta dopo la vendita
    	NewFreeCreditKR = (uint16_t) gOrionKeyVars.FreeCredit;			// Free Credit carta dopo la vendita
    }
	StatusVendita 		= VEND_REQUEST;
    TipoVendita			= TipoVend;
    PSHCheckPowerDown();
}

/*------------------------------------------------------------------------------------------*\
 Method: Update_DatiAuditVendita
   In caso di vendita con CPC esterno il CostoRealeSelezione potrebbe essere stato 
   modificato. 

   IN:  -  
  OUT:  -  
\*------------------------------------------------------------------------------------------*/
void  Update_DatiAuditVendita(CreditCpcValue CostoSelez) {

	PSHProcessingData();
	CostoRealeSelezione = CostoSelez;
    PSHCheckPowerDown();
}	
	
/*------------------------------------------------------------------------------------------*\
 Method: TestCondizioniPerVendita
   Verifico se il credito carta e' sufficiente per autorizzare la vendita richiesta.
   Nel caso sia attiva l'opzione "Gestione Selezioni" si controllano anche altri parametri 
   per autorizzare la vendita.

   IN:  -  Valore selezione richiesta
  OUT:  -  RespOK se credito sufficiente, altrimenti VendDenInsufCred
\*------------------------------------------------------------------------------------------*/
static uint16_t  TestCondizioniPerVendita(CreditCpcValue ValSelez) {
	
	uint16_t	status;
	
	if (gVMC_ConfVars.PresMifare == false) return Resp_OK;										// CPC Esterno: sara' lui a autorizzare/negare selezione
	status = Resp_OK;
	if (gOrionKeyVars.FreeVend && !gOrionFreeVendVars.RechargeOnly) {							// Disponibili FreeVend per la vendita
		gOrionKeyVars.FreeVend --;
		FreeVendLevel = 1;
		NewFreeVendKR = gOrionKeyVars.FreeVend;
		CreditUsedKR = 0;
		FreeCreditUsedKR = 0;
		return status;
	} else {
		FreeVendLevel = 0;
		if (TotCreditCard() >= ValSelez) {
			if (gOrionKeyVars.FreeCredit >= ValSelez) {
				gOrionKeyVars.FreeCredit -= ValSelez;											// Il FreeCredit e' sufficiente per pagare la selezione
				FreeCreditUsedKR = ValSelez;													// Valore di FreeCredit usato per la vendita
				CreditUsedKR = 0;																// Valore del Credito usato per la vendita
				return status;
			} else {
				ValSelez -= gOrionKeyVars.FreeCredit;											// FreeCredit NON sufficiente: lo utilizzo tutto e il resto lo scalo dal Credito
				FreeCreditUsedKR = gOrionKeyVars.FreeCredit;									// Valore di FreeCredit usato per la vendita
				gOrionKeyVars.FreeCredit = 0;
				gOrionKeyVars.Credit -= ValSelez;
				CreditUsedKR = ValSelez;														// Valore del Credito usato per la vendita
				return status;
			}
		} else {
			return (status = VendDenInsufCred);													// Credito insufficiente per pagare la selezione
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: TotCreditCard
   Somma tutti i crediti presenti sulla carta.

   IN:  -  
  OUT:  -  
\*------------------------------------------------------------------------------------------*/
CreditCpcValue  TotCreditCard(void) {
	
	return (gOrionKeyVars.Credit + gOrionKeyVars.FreeCredit);
}

/*------------------------------------------------------------------------------------------*\
 Method: Elabora_Refund
   Refund carta: se non c'e' piu' la carta registra il refund nella EEPROM, altrimenti
   tenta il riaccredito sulla carta.

   IN:  -  
  OUT:  -  
\*------------------------------------------------------------------------------------------*/

void  Elabora_Refund(void) {

	StatusVendita = VEND_FAILURE;																// MR 03.10.13: per eseguire la ResetDatiVendita in "vendita.c" altrimenti 
	if (gOrionKeyVars.Inserted == false) {														// rimangono stati pendenti e non andranno piu' le vendite successive e l'Audit

		// =======  Carta non piu' nel lettore  =================

		if (gOrionKeyVars.Prepag && CreditUsedKR > 0) {											// Alla carta prepagata non si puo' applicare il refund
			PSHProcessingData();																// Il valore della selezione e' stato scalato ma non e' piu' possibile rifonderlo:
			ValoreReinstate = CreditUsedKR;														// lo memorizzo in ValoreReinstate per l'audit Overpay Cashless DA9
			Overpay(OVERPAY_KR);
		} else {
			if (FreeVendLevel) {
				SaveRefund(&RefundFreeVend, &DataRefundFV, TipoRefundFreeVend, 1U, true);		// Refund FreeVend
			} else {
				if (gOrionKeyVars.VendAppAmount != 0) {
					if (CreditUsedKR > 0) {
						SaveRefund(&RefundValues, &DataRefundValues, TipoRefundCredito, CreditUsedKR, false);		// Refund Credito carta
						CreditUsedKR = 0;
					}
					if (FreeCreditUsedKR > 0) {
						SaveRefund(&RefundFreeCredit, &DataRefundFC, TipoRefundFreeCred, FreeCreditUsedKR, false);	// Refund FreeCredit carta
						FreeCreditUsedKR = 0;
					}
				}
				PSHCheckPowerDown();
				gOrionKeyVars.failvendcardcredit = gOrionKeyVars.VendAppAmount;						// Solo per visualizzare il refund
				CaricaToutCash();																	// Uso  Timeout Cash perche' e' una visualizzazione alternativa al cash
			}
		}
		gOrionKeyVars.State = kOrionCPCStates_Idle;
		
	} else {
		// =======  Carta ancora nel lettore: eseguo riaccredito  ============

		if (gOrionKeyVars.Prepag) {																// Alla carta prepagata non si puo' applicare il refund
			PSHProcessingData();																// Il valore della selezione e' stato scalato ma non e' piu' possibile rifonderlo:
			ValoreReinstate = CreditUsedKR;														// lo memorizzo in ValoreReinstate per l'audit Overpay Cashless DA9
			Overpay(OVERPAY_KR);
			gOrionKeyVars.CreditWriteEnd = true;												// Simulo fine scrittura carta per usare gli stessi stati del refund normale
		} else {
			gOrionKeyVars.Credit += CreditUsedKR;												// Riaccredito Valore del Credito usato per la vendita
			gOrionKeyVars.FreeCredit += FreeCreditUsedKR;										// Riaccredito Valore di FreeCredit usato per la vendita
			if (FreeVendLevel) {
				gOrionKeyVars.FreeVend ++;														// Riaccredito FreeVend
				FreeVendLevel = 0;
			}
			RevalueKR(REFUND_IMMEDIATE_KR, gOrionKeyVars.VendAppAmount, true);					// Ext Audit per Card Refund
			CreditWriteStart(enCreditWriteRefund);
		}
		gOrionKeyVars.State = kOrionCPCStates_RefundWr;
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: Elabora_FineWriteRefund
	Attende fine scrittura Refund sulla carta.
	In MDB riporta poi in kOrionCPCStates_Inactive il TemporaryState e in Idle il main state
	se non diversamente modificato dall'MDB.

   IN:  -  
  OUT:  -  
\*------------------------------------------------------------------------------------------*/
void  Elabora_FineWriteRefund(void)  {

	if (CreditWriteEnd()) {
		SetMaxRevalue();																	// Determino credito caricabile sulla Carta
		CreditUpdate(); //ABELE																// Aggiorna credito da visualizzare

#if (_IcpMaster_ == true)
		CheckCashType();   																	// Predispongo per controllare abilitazione banconote/monete
#endif
		gOrionKeyVars.State = kOrionCPCStates_Idle;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: MemoCashAdd
	Memorizza il nuovo valore di Cash e calcola il suo CRC16.
	Se la somma dei valori supera il massimo ammissibile su 2 uint8_t, si memorizza il 
	massimo ammissibile.
  
	IN	- valore cash + tipo cash
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  MemoCashAdd(CreditValue amount, CreditCashTypes cashType, bool PWD_Control) {

	PSHProcessingData();
	if (cashType == kCreditCashTypeCoin) {
		if ((CashDaSelettore + amount) >= CashDaSelettore) {
			CashDaSelettore +=amount;
		} else {
			CashDaSelettore = MAX_SYSTEM_CREDIT;												// CashDaSelettore+amout supera il MAX_SYSTEM_CREDIT
		}
		SetCRC_CashDaSelettore();
	} else {
		if (cashType == kCreditCashTypeBill) {
			if ((CashDaBankReader + amount) >= CashDaBankReader) {
				CashDaBankReader +=amount;
			} else {
				CashDaBankReader = MAX_SYSTEM_CREDIT;											// CashDaBankReader+amout supera il MAX_SYSTEM_CREDIT
			}
			SetCRC_CashDaBankReader();
		}
	}
	if (PWD_Control) PSHCheckPowerDown();
}

/*--------------------------------------------------------------------------------------*\
Method: ValidateCashDaSelettore
	Controllo validita' credito cash da selettore o rendiresto.
	In caso di errore CRC, azzero il cash errato.
	
	IN	- CashDaSelettore, CRC_CashDaSelettore
	OUT	-
\*--------------------------------------------------------------------------------------*/
void  ValidateCashDaSelettore(void) {
	
	uint16_t CRC_Cash;
	
	Init_CRC(SeedZero);																			// Inizializzo CRC	
	CRC_Cash = Calc_CRC_GetCRC16(CashDaSelettore);
	if (CRC_Cash != CRC_CashDaSelettore) {
		PSHProcessingData();
		CashDaSelettore = 0;
		CRC_CashDaSelettore = 0;
		PSHCheckPowerDown();
	}
}

/*--------------------------------------------------------------------------------------*\
Method: SetCRC_CashDaSelettore
	Calcola nuovo CRC_CashDaSelettore.
	
	IN	- CashDaSelettore
	OUT	-
\*--------------------------------------------------------------------------------------*/
void  SetCRC_CashDaSelettore(void) {
	
	Init_CRC(SeedZero);																			// Inizializzo CRC	
	CRC_CashDaSelettore = Calc_CRC_GetCRC16(CashDaSelettore);
}

/*--------------------------------------------------------------------------------------*\
Method: ValidateCashDaBankReader
	Controllo validita' credito cash da bank reader.
	In caso di errore CRC, azzero il cash errato.
	
	IN	- CashDaBankReader, CRC_CashDaBankReader
	OUT	-
\*--------------------------------------------------------------------------------------*/
void  ValidateCashDaBankReader(void) {

  	uint16_t CRC_Cash;
	
	Init_CRC(SeedZero);																			// Inizializzo CRC	
	CRC_Cash = Calc_CRC_GetCRC16(CashDaBankReader);
	if (CRC_Cash != CRC_CashDaBankReader) {
		PSHProcessingData();
		CashDaBankReader = 0;
		CRC_CashDaBankReader = 0;
		PSHCheckPowerDown();
	}
}

/*--------------------------------------------------------------------------------------*\
Method: SetCRC_CashDaBankReader
	Calcola nuovo CRC_CashDaBankReader.
	
	IN	- CashDaBankReader
	OUT	-
\*--------------------------------------------------------------------------------------*/
void  SetCRC_CashDaBankReader(void) {
	
	Init_CRC(SeedZero);																			// Inizializzo CRC	
	CRC_CashDaBankReader = Calc_CRC_GetCRC16(CashDaBankReader);
}

/*--------------------------------------------------------------------------------------*\
Method: GetValidatedCash
	Legge i crediti cash e ne verifica il CRC: in caso di errore CRC, il credito e'
	azzerato. 
	
	IN	- CashDaSelettore, CRC_CashDaSelettore, CashDaBankReader, CRC_CashDaBankReader
	OUT	-
\*--------------------------------------------------------------------------------------*/
CreditCoinValue  GetValidatedCash(void) {
	
	ValidateCashDaSelettore();
	ValidateCashDaBankReader();
	return (CashDaSelettore+CashDaBankReader);
}

/*--------------------------------------------------------------------------------------*\
Method: CaricaToutCash
	Ricarica il timeout cash dopo l'uso.
	
	IN	- OverpayTimeout
	OUT	-
\*--------------------------------------------------------------------------------------*/
void  CaricaToutCash(void) {
	
	TmrStart(gOrionCashVars.creditTmr, OverpayTimeout);											// Ricarico Timeout Cash
}
//#define TmrStart(localTmr, timeout) 	KernelTmr32Start(&(localTmr), timeout)

/*--------------------------------------------------------------------------------------*\
Method: SetAuditVendInGratuito
	Registra le vendita effettuate in gratuito aggiornando gli stessi ID delle vendite
	di test con chiave, quindi metto a zero FreeVendLevel e passo solo il valore della
	selezione eseguita.
	Aggiorna VA201, VA202, VA203 e VA204. 
	
	IN	- Valore selezione
	OUT	-
\*--------------------------------------------------------------------------------------*/
void  SetAuditVendInGratuito(uint32_t SelVal) {

	gOrionKeyVars.VendReq = SelVal;
	FreeVendLevel = 0;																			// Predisposto per compatibilita' registrazione audit vendite di test
	SetDatiAuditVendita(VEND_DA_IN_GRATUITO);
}

/*------------------------------------------------------------------------------------------*\
 Method: OrionCreditStateCheck
	MDB Master: funzione chiamata dalla "ICPCPCHook_CheckOrionState" quando il master invia
	il comando di RESET al CPC.
	Serve a verificare se lo stato interno dell'OrionCredit sia congruo con, ad esempio, lo
	spegnimento del CPC quando una carta era inserita: non deve esserci credito dal cashless,
	flag Inserted set o stato OrionCredit diverso da kOrionCPCStates_Enabled).
	In caso contrario si procede ad un ripristino delle variabili coinvolte.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  OrionCreditStateCheck(void)
{
	if (gVMC_ConfVars.CPC_MDB == true)
	{																							// Impostato MDB CPC esterno 
		if ((gOrionKeyVars.Inserted == true) || (gOrionKeyVars.State > kOrionCPCStates_Enabled))
		{

#if DBG_MDB_COMEST_LOCK
			TraceError(0x0D, 0, 0, 0, 0);
#endif
			AzzeraCreditCard();
			CreditUpdate();																		// Azzera credito sul VMC executive 
			ClrVisualAll();																		// Sul display locale visualizza le sole cifre significative
			CheckCashType();
			gOrionKeyVars.Inserted = false;
			if (gOrionKeyVars.State == kOrionCPCStates_RevalueWr)
			{																					// Ero in attesa di risposta al Revalue
				gOrionKeyVars.CreditWriteEnd 	= true;
				gOrionKeyVars.ExtKeyRevalueApp 	= false;
			}
			else if (gOrionKeyVars.State == kOrionCPCStates_VendWr)
			{																					// Ero in attesa di risposta al Vend Request
				gOrionKeyVars.CreditWriteEnd 	= true;
				gOrionKeyVars.ExtKeyVendApp  	= false;
			}
			else
			{
				gOrionKeyVars.State = kOrionCPCStates_Enabled;
			}
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: AzzeraCashInOverpay
	
	IN	- Valore selezione
	OUT	-
\*--------------------------------------------------------------------------------------*/
void  AzzeraCashInOverpay(void)
{
	Overpay(OVERPAY_CASH);
	gOrionCashVars.creditCash = 0;
	CheckCashType();
	CMUpdateCI();
	gOrionCashVars.creditTmr.tmrEnd = 0;
}