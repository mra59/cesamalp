/****************************************************************************************
File:
    OrionCredit.h

Description:
    Include file con definizioni e strutture per la gestione del credito cash e cashless.

History:
    Date       Aut  Note
    Apr 2016	Abe/MR  

 *****************************************************************************************/

#ifndef __OrionCredit_H_
#define __OrionCredit_H_

#define	GeneralError		0xFF


typedef enum {
	enCreditWriteVend = 0,
	enCreditWriteVendNeg,
	enCreditWriteRefund,
	enCreditWriteRevalue
}sTypeWrite;

uint16_t	GestioneStatoIdle(void);

bool 	CreditWriteStart(uint8_t wrtype);
bool 	CreditWriteOk(void);
static  bool 	ExtCPC_CreditWriteStart(CreditCpcValue VendAmount, uint16_t NumSel);

bool			KeyEnabled(bool enable);
bool			CreditWriteEnd(void);
void			Set_AttesaEstrazione(void);
bool  			IsReaderStateEnabled(void);
bool  			IsReaderIdleCB(void);
bool  			SetPrice_e_Selnum(uint16_t, uint16_t);
bool  			SetCostoSelezioneConCarta(void);
void  			SetCostoSelezioneConCash(void);
void  			SetDatiAuditVendita(uint8_t TipoVend);
void  			Update_DatiAuditVendita(CreditCpcValue CostoSelez);
static uint16_t 	TestCondizioniPerVendita(CreditCpcValue);
void  			SetMaxRevalue();
void 			CheckCashType(void);
void 			CreditMasterChangePaidOutInd(CreditValue);
bool  			IsOrionInStBy(void);
CreditCpcValue  TotCreditCard(void);
void  			Elabora_Refund(void);
void  			Elabora_FineWriteRefund(void);
void  		 	ValidateCashDaSelettore(void);
void  		 	ValidateCashDaBankReader(void);
void  			SetCRC_CashDaSelettore(void);
void  			SetCRC_CashDaBankReader(void);
void 			MemoCashAdd(CreditValue, CreditCashTypes, bool);
CreditCoinValue GetValidatedCash(void);
void  			SetAlreadyExChange(void);
void  			TestExch_InOut(void);
void  			CaricaToutCash(void);
bool  			DeterminaExactChange(uint8_t MinCoinsNum);
void 			CreditMasterGiveChange(void);
void 			CreditMasterEscrowInd(void);
bool 			ExChStatusChanged(void);
void  			SetAuditVendInGratuito(uint32_t SelVal);
void 			CashlessTask(void);
void  			CashTask(void);
void 			CreditMasterCoinInInd(uint8_t coinType);
void  			OrionCreditStateCheck(void);
void  			AzzeraCashInOverpay(void);
	
	
/**********************************************************************************
 * ABELE
 * definizioni per Bill_Task
 */

static void 		CMUpdateCI(void);
void 				CreditMasterCashAdd(CreditValue amount, CreditCashTypes cashType);
static CreditValue 	CMCreditAdd(CreditValue* credit, CreditValue amount);

/*************************************7
 * Executive cash task functions
 */

void CreditUpdate(void);

#endif		// __OrionCredit_H_
