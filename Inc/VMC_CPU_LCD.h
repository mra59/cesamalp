/****************************************************************************************
File:
    VMC_CPU_LCD.h

Description:
    Definizioni e costanti per il Display LCD della CPU VMC.

History:
    Date       Aut  Note
    Mag 2019	MR   

 *****************************************************************************************/

#ifndef __VMC_CPU_LCD_H_
#define __VMC_CPU_LCD_H_

void  LCD_Init(bool);
void  Gestione_LCD(void);
void  LCD_SetNormalVisual(void);
void  LCD_Disp_Clear(void);
void  LCD_Disp_NOSEL(void);
void  LCD_Disp_VendFail(void);
void  LCD_Home(void);
void  LCD_Disp_CredNoSuff(uint8_t SelezNum, CreditCpcValue PrezzoSel);
void  LCD_ClearBuffer(void);
void  LCD_Disp_SelInProgress(uint8_t NumSel, bool SelezFail);
void  LCD_Disp_PrelevareBevanda(void);
void  LCD_Disp_WaitRestart(void);
void  ForceLCD_NewVisual(void);
void  Msg_InAnyRow(char *pnt, uint8_t NumRiga, bool clrbuff);
void  LCD_Disp_TastoDecinaSelez(void);
void  ClrVisualAll(void);
void  SetVisualAll(void);

bool  IsLCD_NoInitMsg(void);
bool  IsLCD_SelNonDisponibileMsg(void);
bool  IsLCD_PrelevaProdottoMsg(void);

void  VisParam(void);
void  LCD_Disp_VMC_Fail(void);
void  VMC_Programmaz_Msg(void);
void  LCD_ClearAllarmMessage(void);
void  MessaggioStBy(void);
void  LCD_Disp_ConfigUpdate(void);
void  LCD_SetCursor(uint8_t position);
void  LCD_ForzaNormalVisual(void);
void  SetVisualAll(void);
void  VMC_USB_KEY_Msg(void);
void VMC_USB_ErrKeyRilev(void);
void  ReadMessagesFromEE(void);
void  ForceLCD_NewMsgEE(void);


void  ValutaInMessage(char* pntDestMsg, uint16_t Valuta, uint8_t ValPosition);
void  Hex8InMessage(char* pntDestMsg, uint8_t Valore, uint8_t NumPosition, bool integer, int8_t Valore_int8);
void  Hex16InMessage(char* pntDestMsg, uint16_t Valore, uint8_t ValPosition);
void  Hex32InMessage(char* pntDestMsg, uint32_t Valore, uint8_t ValPosition, uint8_t NumCifre);
void  Visual_BCD_2_Cifre(char* pntDestMsg, uint8_t Numero, uint8_t NumPosition);
void  Visual_BCD_1_Cifra(char* pntDestMsg, uint8_t Numero, uint8_t NumPosition);
void  Y_N_InMessage(char* pntDestMsg, uint8_t ValoreHex, uint8_t Pos);
void  Message_In_Message(const char* pntMsgIn, uint8_t RowNum);
	

void  LCD_Disp_CollaudoInCorso(void);
void  LCD_Disp_ReadConfDaChiaveUSB(void);
void  LCD_Disp_PrelievoIrdaLocale(void);
void  LCD_Disp_AzzeramMemoria(void);
void  LCD_Disp_EndColl_OK(void);


/*===============================================================
 *                     LCD Display Commands
 * =============================================================*/

#define	LCD_CLEAR_DISPLAY				0x01							// Clear Display: 0x20 to DDRAM e set 0 to DDRAM address - 1.52msec @ 270KHz
#define	LCD_RETURN_HOME					0x02							// ReturnHome: DDRAM invariata                           - 1.52msec @ 270KHz
#define	LCD_KEYBOARD_MATRIX				0x04							// Comando per LCD remoto per indicare l'invio della matrice tastiera
#define	LCD_SetDDRAM_Addr				0x80							// Set DDRAM Address: 1 AC6 AC5 AC4 AC3 AC2 AC1 AC0       - 37 usec


#define	LCD_NUMCOL						20								// 20 colonne per 4 righe	
#define	FOUR_LCD_NUMCOL					4*LCD_NUMCOL
#define	LCD_CHAR_NUM	 				80								// Numero di caratteri del display
#define	LCD_DBUF_SIZE					84
#define	LCD_DBUF_SIZE_FOR_EXP			LCD_DBUF_SIZE-4
#define	LCD_2x16_NUMCOL					16


//*********************************************************
// Valori Hex da inviare come comandi al display LCD  per * 
// posizionare il cursore sulla riga e sul chr desiderato *
//                                                        *                           
//   Riga       Hex          Offset decimale              *
// Riga 1: 0x80 ---  0x93       0  ---  19                *
// Riga 2: 0xC0 ---  0xD3      64  ---  83                *
// Riga 3: 0x94 ---  0xA7      20  ---  39                *
// Riga 4: 0xD4 ---  0xE7      84  ---  103               *
//*********************************************************

/*===============================================================
 *                     Display States 
 * =============================================================*/

typedef enum {
	LCDNormalOper		=	0,
	LCDInH_Msg			=	1,
	NoLink_Msg			=	2,
	Audit_Msg			=	3,
	LCDVendFail_Msg		=	4,
	LCDNoSel_Msg		=	5,
	LCDSelInCorso_Msg	=	6,
	LCD_OutInit			=	7,
	LCD_VMC_FAIL		=	8,
	LCD_VMC_WaitInit	=	9,
	LCD_VMC_WaitTemper	=	10,
	LCD_VMC_Programmaz	=	11,
	LCD_VMC_NewConfig	=	12,
	LCD_VMC_LavagGrup	=	13,
	LCD_VMC_LavagMixer	=	14,
	LCDPrelevare_Msg	=	15,
	LCDWaitCooling_Msg	=	16,
	LCD_USB_KEY_IN		=	17,
	LCD_USB_KEY_IN_ERR	=	18
} LCD_DispMessage;

typedef enum {
	msgNoAll			=	0,
	msgPompaFail		=	1,
	msgTemperFail		=	2,
	msgGruppoFail		=	3,
	msgCaricoFail		=	4,
	msgColonnaFail		=	5,
	msgNoParam			=	6,
	msgNoBicchieri		=	7,
	msgTroppoPieno		=	8,
	msgErrPompaVent		=	9,
	msgErrLowTemp		=	10,
	msgMotCapsFail		=	11,
	msgErrNoCodes		=	12,
	msgErrDeconto		=	13,
	msgErrDosatMais		=	14
} LCD_AllMsg;


/*===============================================================
 * STRUTTURE
 * =============================================================*/

typedef struct {
	uint16_t			CreditoDisplay;
	uint8_t				VisBuff[80];
	uint8_t				DispPnt;							// Pointer al digit da accendere
	LCD_DispMessage		Phase;
	LCD_AllMsg			AllPhase;							// Tipo di messaggio di allarme visualizzato

	bool				VisFW_Rev;							// Prima Inizializzazione: visualizza Revisione Firmware
	bool				ForzaVisCred;						// Set per riprendere la normale visualizzazione del credito
	bool				SendCmd;							// Invia un comando al display
	bool				CmdSent;							// Comando inviato al display
	bool				VisuAll;							// Set per visualizzare tutte le cifre del credito
	bool				AlreadyOnDisp;						// Messaggio gia' visualizzato
	bool				ClrBeforeNewMsg;					// Esegue comando Clear prima del nuovo messaggio
	bool				NewMsgInEE;							// Nuovi messaggi configurati in EE
} LCD_Display;

typedef struct {
	Tmr32Local	ToutInitDisp;
	Tmr32Local	TimeDispMsg;								// Timer per visualizzare un messaggio per un determinato periodo
	Tmr32Local	TimeVisTemper;								// Timer per visualizzare la temperatura in debug
} DISPLCDTimes;

#define	Riga_1_In_Buffer		0							// Inizio della riga 1 nel buffer display
#define	Riga_2_In_Buffer		40							// Inizio della riga 2 nel buffer display
#define	Riga_3_In_Buffer		20							// Inizio della riga 3 nel buffer display
#define	Riga_4_In_Buffer		60							// Inizio della riga 4 nel buffer display
#define	HALF_ROW_POS			LCD_NUMCOL/2				// Inizio meta' della riga display
#define	OFFS_MSG_EROGAZ			12							// Offset posizione messaggi di stato delle singole erogazioni
#define	LEN_MSG_EROGAZ_STATUS	8							// Lunghezza messaggi di stato delle singole erogazioni
#define	POSIT_MSG1_ROW1			6
#define	POSIT_MSG2_ROW1			14
#define	POSIT_MSG1_ROW2			6
#define	POSIT_MSG2_ROW2			14

				
#define	ROW3_NUMSEL				38							// Numero selezione a due cifre sulla Riga 3
#define	LCD_2X16_ROW2_NUMSEL	14							// Numero selezione a due cifre sulla Riga 2
#define	CursPos_Boolean			79							// Posizione chr "Y" / "N" nel buffer display
				
//#define	LCD_POS_FW_REV_4x20		35							// Posizione firmware scheda CPU
#define	LCD_POS_FW_REV_4x20		50							// Uniformato il msg di revisione fw per visualizzarlo correttamente anche quando la EE e' erased
#define	LCD_POS_FW_REV_2x16		50							// e non e' dato saper che tipo di display LCD e' collegato alla CPU: si usano quindi sempre 2x16 chr
	
#define	CredCursorStart			74							// Posizione migliaia visualizzazione credito
#define	DecontoRiservaCurs		58							// Posizione visualizzazione simbolo deconto sotto la riserva
#define	GuastoCursorPos	 		60							// Posizione visualizzazione guasto fuori servizio
#define	FAIL_CODE_POS	 		58							// Posizione visualizzazione 2 chr del codice di guasto

#define	LCD_ROW_1				1
#define	LCD_ROW_2				2
#define	LCD_ROW_3				3
#define	LCD_ROW_4				4

//  Messaggi a 1 chr su 1 riga
#define	LCD_BUF_POS_STATUS_EROGAZ_1		2
#define	LCD_BUF_POS_STATUS_EROGAZ_2		6
#define	LCD_BUF_POS_STATUS_EROGAZ_3		10
#define	LCD_BUF_POS_STATUS_EROGAZ_4		14
#define	LEN_MSG_STATUS_EROGAZIONE		1					// Lunghezza messaggi di stato delle singole erogazioni
#define	LCD_BUF_START_CREDITO			49					// Posizione migliaia visualizzazione credito

// ========================================================================
// ============    Display    Timing                =======================
// ========================================================================

#define	TMSG_PRELEVARE		TwoSec						// Temporizzazione "Prelevare Prodotto"
#define	TMSG_FAILVEND		TwoSec						// Temporizzazione "Vend Fail"
#define	TMSG_NOSEL			OneSec						// Temporizzazione "NO SEL"
#define	TMSG_CREDNOSUFF		FIVESEC						// Temporizzazione "Credito insufficiente"
#define	TASTO_DECINA_SELEZ	FIVESEC						// Temporizzazione tasto decina selezioni
#define	TMSG_ENDLESS		FIVESEC						// Temporizzazione senza fine
				
#define	TMSG_CMD_CLEAR		Milli5						// Temporizzazione comando clear display


#endif		//   __VMC_CPU_LCD_H_
