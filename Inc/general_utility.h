/****************************************************************************************
File:
    General_utility.h

Description:
    Include file con definizioni e strutture per le funzioni general purpose.

History:
    Date       Aut  Note
    Mag 2019	MR   

 *****************************************************************************************/

#ifndef __General_utility__H
#define __General_utility__H


bool 		CompareBuffer (uint8_t *ptBuf1, uint8_t *ptBuf2, uint8_t bufLen);
//void 		BcdTouint16(uint16_t *, char *, uint8_t);
void 		HexByteToAsciiInt(uint8_t, uint16_t*, uint8_t);
uint16_t  	AsciiToHex(char *, uint8_t);
uint32_t	atouint32(uint8_t* strVal, char len);
uint16_t  	CharConversion(char val);
void  		SetAttesa(uint32_t);
bool  		FineAttesa(void);
uint16_t  	Dec16Touint16(uint16_t);
uint8_t   	DecHex(uint8_t);
void  		HexWordToDec(uint16_t);
void  		HexToDec(uint8_t, uint8_t*);
uint16_t 	Check_Hour_Min(uint16_t val);
void 		uint32toa0(char* strVal, uint32_t val, uint8_t nDigits);
	/* Convert an unsigned integer to a string with the specified number of digits, filling with 0s
	If number doesn't fit in the specified number of digits, MOST significat digits are truncated
	*/

void 		uint32toa(char* strVal, uint32_t val, uint8_t nDigits);
	/* Convert an unsigned integer to a string with the specified number of digits, no filling.
	If number doesn't fit in the specified number of digits, MOST significat digits are truncated
	*/
uint8_t 	BinToBcd(uint8_t bin);
uint8_t 	BcdToBin(uint8_t bcd);
void 		ZeroToSpaces(char *Buff, uint16_t BuffLen);

#if (IOT_PRESENCE == true)
	char* 	uint32ToAscii(uint32_t Hex32, char* bufpnt);
#endif


#endif