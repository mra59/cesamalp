/****************************************************************************************
File:
    Vendita.h

Description:
    Include file contenente definizioni per il file Vendita.c

History:
    Date       Aut  Note
    Giu 2019	MR   

 *****************************************************************************************/

#ifndef __Vendita__H
#define __Vendita__H

/*--------------------------------------------------------------------------------------*\
Function Prototypes
\*--------------------------------------------------------------------------------------*/

void  AggiornaNumSel(uint16_t sconto);
void  ResetDatiVendita(void);
void  AuditVendSuccessConKR(uint8_t *status);
void  AuditVendSuccessConCash(uint8_t *status);
void  AggiornamentoDatiVendita(void);
void  GestioneAuditVendita(void);
void  SetDeconto(void);
void  ClrDeconto(void);
void  CheckDeconto(void);
void  UpDateDeconto(uint8_t NumSelez);
static void  AggiornamentoDatiVenditaConKR(void);
static void  AggiornamentoDatiVenditaConCash(void);
static void  AggiornamentoDati_VMCFreeVend(void);
static void  AggiornamentoDati_DA_In_Gratuito(void);
static void  AggiornamentoDati_Solo_Costruttore(void);
static void  AuditSaleFree(uint8_t *status);

#endif	
	
