/****************************************************************************************
File:
    Auditrevalue.h

Description:
    Include file contenente definizioni per il file AuditRevalue.c

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

static void	ResRevalue(void);
static void RevalueKRCash(void);
static void RevalueKRCaricamentoBlocchi(void);
static void RevalueKRReinstate(uint8_t RefundType);
void 		GestioneRevalue(void);
void		RevalueKR(uint8_t, uint32_t, uint8_t);
