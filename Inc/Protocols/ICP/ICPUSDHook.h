/****************************************************************************************
File:
    ICPUSDHook.h

Description:
    Include file con definizioni e strutture per l'USD MDB

History:
    Date       Aut  Note
    Set 2017 	MR

*****************************************************************************************/

#ifndef __ICPUSDHook_h
#define __ICPUSDHook_h

void  USD_Task_MasterMode(void);
void  USD_Task_SlaveMode(void);


void ICPUSDHook_Data(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen);
void ICPUSDHook_DataBlock(uint8_t usdType, uint8_t nBlock, uint8_t *ptData, uint8_t nDataLen);
void ICPUSDHook_DataBlockReq(uint8_t usdType, uint8_t nBlock);
void ICPUSDHook_ErrCode(uint8_t usdType, uint16_t errCode);
bool ICPUSDHook_IsEnabled(uint8_t usdType);
void ICPUSDHook_PriceReq(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn);
void ICPUSDHook_SelStatus(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn, uint16_t nSelStatus);
void ICPUSDHook_SendDataReq(uint8_t usdType, uint8_t nBlock);
void ICPUSDHook_SetMaxPrice(uint8_t usdType, CreditCpcValue maxPrice);
void ICPUSDHook_SetRowColumn(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn);
void ICPUSDHook_VendFailed(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn, uint16_t errCode);
void ICPUSDHook_VendReq(uint8_t usdType, uint8_t nSelRow, uint8_t nSelColumn, CreditCpcValue nSelPrice);
bool ICPUSDHook_VendApproved(uint8_t usdType);
bool ICPUSDHook_VendDenied(uint8_t usdType);
void ICPUSDHook_VendSucc(uint8_t usdType);
void ICPUSDHook_SetManufactCode(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen);
void ICPUSDHook_SetSerialNumber(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen);
void ICPUSDHook_SetModelNumber(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen);
void ICPUSDHook_SetSWVersion(uint8_t usdType, uint16_t nSwVersion);
void ICPVMCHook_ResetUSD(void);
void ICPUSDHook_SetReady(uint8_t usdType, bool fReady);
bool ICPUSDHook_SetDiagData(uint8_t usdType, uint8_t *ptData, uint8_t nDataLen);
uint8_t  ICPVMCHook_GetDecimalPoint(void);
uint16_t ICPVMCHook_GetScalingFactor(void);
void ICPUSDHook_GetManufactCode(uint8_t *ptData, uint8_t dataLen);
void ICPUSDHook_GetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPUSDHook_GetModelNumber(uint8_t *ptData, uint8_t dataLen);
uint16_t ICPUSDHook_GetSWVersion(void);



void ICPUSDHook_Init(uint8_t slaveType);
bool ICPUSDHook_IsAvailable(void);
void ICPUSDHook_ControlEnable(uint8_t usdType, bool fEnable);
	// Cmd from VMC: enable/disable the USD
	// (when the USD is disabled no vend is available and Vend Failed is returned)
void ICPUSDHook_VMCSetupInfo(T_VMC_SETUP *ptInfo);
void ICPUSDHook_FundsCredit(uint8_t usdType, CreditCpcValue creditVal);
	// Cmd from VMC: update the USD credit
void ICPUSDHook_GetVendRequest(uint8_t usdType, uint8_t *selRow, uint8_t *selColumn, CreditCpcValue *selPrice);
	// Called to get Vend Request Info... Price of current "selRow/selColumn" selection
void ICPUSDHook_GetPriceRequest(uint8_t usdType, uint8_t *selRow, uint8_t *selColumn);
	// Called to get the Price of current "selRow/selColumn" selection
uint8_t ICPUSDHook_GetFeatureLevel(uint8_t usdType);
	// Return the feature level of the USD
CreditCpcValue ICPUSDHook_GetMaxPrice(uint8_t usdType);
	// Return the Maximum Price of the USD selections
void ICPUSDHook_GetRowColumn(uint8_t usdType, uint8_t *selRow, uint8_t *selColumn);
	// Called to get maximum Row & Column of the USD selections 
void ICPUSDHook_VendSel(uint8_t usdType, uint8_t selRow, uint8_t selColumn);
	// Cmd from VMC: vend "selRow/selColumn" selection
void ICPUSDHook_VendHomeSel(uint8_t usdType, uint8_t selRow, uint8_t selColumn);
	// Cmd from VMC: vend "home selRow/selColumn" selection
uint16_t ICPUSDHook_VendSelStatus(uint8_t usdType, uint8_t selRow, uint8_t selColumn);
	// Cmd from VMC: return "selRow/selColumn" selection status
void ICPUSDHook_FundsSelPrice(uint8_t usdType, uint8_t selRow, uint8_t selColumn, CreditCpcValue selPrice, uint16_t selID);
	// Response from VMC: return the Selection Price previously requested
bool ICPUSDHook_USDSlaveSelezRequest(uint8_t numtasto);
void ICPUSDHook_SlaveVendApproved(uint8_t usdType);
void ICPUSDHook_SlaveVendDenied(uint8_t usdType);
void ICPUSDHook_SlaveVendFailed(uint16_t Errore);
void ICPUSDHook_SlaveVendSuccess(void);
bool ICPUSDHook_VendRequest(uint8_t numtasto);













#endif // End ICPUSDHook
