/****************************************************************************************
File:
    DDCMP.h

Description:
    Include file con definizioni e strutture per tutto il protocollo DDCMP

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/


#ifndef __DDCMP__
#define __DDCMP__


// --------------------------------------------------------------------------------------

#ifndef kDDCMPExtendedAddrSupport
#define kDDCMPExtendedAddrSupport			false
	// Extended addressing support for multi-drop connections
	// The extended address must be specified using DDCMPExtendedAddrSet()
	// If the address is not specified, DDCMP operates with standard address.
	// In slave mode, DDCMP can accept connections with standard address
	// even if an extended address has been specified. 
#endif

#ifndef kDDCMPStatisticsSupport
#define	kDDCMPStatisticsSupport				false
#endif

#ifndef kDDCMPConnectionTimeOut
//#define kDDCMPConnectionTimeOut			15000
#define kDDCMPConnectionTimeOut				5000					// MR Maggio 2013 ridotto a 5 sec
	// Timeout diverso da quello std - Zero per disabilitare il controllo del timeout
#endif

#ifndef kDDCMPLineDelaySupport
#define kDDCMPLineDelaySupport				false
	// Define this if you are using a line with long delays (i.e. a modem connection)
#endif

#ifndef kDDCMPIrDASupport
#define kDDCMPIrDASupport					true
	// Enable support for IrDA modules where we have auto-echo on all transmitted characters
	// In this case the serial port used MUST support the TxDone indication.
#endif

#ifndef kDDCMPTurnRoundSupport
#define kDDCMPTurnRoundSupport				kDDCMPIrDASupport
	// Abilita il turn-around time (Rx-to-Tx) come da specifica DDCMP
#endif

#ifndef kDDCMPBaudNegotiationSupport
#define kDDCMPBaudNegotiationSupport		true
	// Negoziazione del Baud rate
#endif

#ifndef kDDCMPLineDelay
#define kDDCMPLineDelay						3000
	// Per aggiungere un ulteriore delay (msec) - (Lasciando a zero aspetta uno START per meno di 1 sec) 
#endif

#ifndef kDDCMPInterByteExtraTO
#define kDDCMPInterByteExtraTO				400
#endif

#define	MAX_ERR_CHAR						100

/*--------------------------------------------------------------------------------------*\
DDCMP base types and constants
\*--------------------------------------------------------------------------------------*/

// DDCMP line release
typedef enum {
	kDDCMPRelease,				 
	kDDCMPNotRelease
} DDCMPLineRelease;

// DDCMP mode
typedef enum {
	kDDCMPModeMaster,						// Master mode
	kDDCMPModeSlave,						// Slave mode
	kDDCMPModeRemote		= 	0x10		// Connessione remota (extended timeouts)
} DDCMPMode;

// DDCMP Data message types
typedef enum {
	kDDCMPDataMsgCmd		= 	0x77,		// Request
	kDDCMPDataMsgCmdResp	= 	0x88,		// Response
	kDDCMPDataMsgDataBlock	=	0x99		// Data block
} DDCMPDataMsg;

// Command messages and responses
typedef enum {
	kDDCMPCmdWhoAreYou		= 	0xe0,
	kDDCMPCmdProgramSlave	= 	0xe1,
	kDDCMPCmdReadData		= 	0xe2,
	kDDCMPCmdWriteData		= 	0xe3,
	kDDCMPCmdDeleteData		= 	0xe4,
	kDDCMPCmdProgramModule	=	0xe5,
	kDDCMPCmdReadMemory		= 	0xe6,
	kDDCMPCmdFinish			= 	0xff
} DDCMPCmd;

// Addresses
typedef enum {
	kDDCMPAddrReserved		=	0x00,
	kDDCMPAddrAny			=	0x01,
	kDDCMPAddrCoinMech		=	0x02,
	kDDCMPAddrCashLess		=	0x03,
	kDDCMPAddrBillValidator	=	0x04,
	kDDCMPAddrAuditModule	=	0x05,
	kDDCMPAddrPublic		=	0x06,
	kDDCMPAddrVending		=	0x07,
	kDDCMPAddrVMCFirst		=	0x10,
	kDDCMPAddrVMCLast		=	0x1f,
	kDDCMPAddrFreeFirst		=	0x20,
	kDDCMPAddrFreeLast		=	0xff
} DDCMPAddr;


// Extended Address definitions
enum {
	kDDCMPAddrExtended 			= 	kDDCMPAddrFreeLast,		// Indica extended address
	kDDCMPExtendedAddrLen		=	8						// Len of extended address
};

typedef uint8_t DDCMPExtendedAddr[kDDCMPExtendedAddrLen];

// Connection statistics
typedef struct {
	uint16_t	txPktCnt;			// Total number of packets received
	uint16_t	rxPktCnt;			// Total number of packets transmitted
	uint16_t	txNackCnt;			// Number of nack sent
	uint16_t	rxNackCnt;			// Number of nack received
	uint16_t	rxTimeoutCnt;		// Number of reception timeouts (missing ack)
} DDCMPConnStats;

/*--------------------------------------------------------------------------------------*\
Private constants and types definition  
\*--------------------------------------------------------------------------------------*/

// Driver state
typedef enum {
  kDDCMPStateClosed,			// 0
  kDDCMPStateIdle,				// 1

  kDDCMPStateRxHdr0,			// 2
  kDDCMPStateRxHdrN,			// 3
  kDDCMPStateRxBody0,			// 4
  kDDCMPStateRxBodyN,			// 5
  kDDCMPStateRxBodyCRC16,		// 6
  kDDCMPStateRxBodyCRC16H,		// 7
  kDDCMPStateRxErr,				// 8

  kDDCMPStateTxReady,			// 9
  kDDCMPStateTxHdr0,			// 10	0x0A
  kDDCMPStateTxHdrN,			// 11	0x0B
  #if kDDCMPExtendedAddrSupport
  kDDCMPStateTxHdrExtAddr,
  #endif
  kDDCMPStateTxHdrCRC16,		// 12	0x0C
  kDDCMPStateTxHdrCRC16H,		// 13	0x0D
  kDDCMPStateTxBody0,			// 14	0x0E
  kDDCMPStateTxBodyN,			// 15	0x0F
  kDDCMPStateTxBodyCRC16,		// 16	0x10
  kDDCMPStateTxBodyCRC16H,		// 17	0x11
  kDDCMPStateTxDone,			// 18	0x12
  kDDCMPStateTxCompleted		// 19	0x13
} DDCMPState;

#define DDCMPStateReceiving(state)  ((state)>=kDDCMPStateRxHdrN && (state)<kDDCMPStateTxReady)			// True se "kDDCMPStateRxHdrN =< sDDCMPVars.state =< kDDCMPStateRxErr" 
#define DDCMPStateSending(state)    ((state)>=kDDCMPStateTxReady && (state)<=kDDCMPStateTxCompleted)	// True se "kDDCMPStateTxReady =< sDDCMPVars.state =< kDDCMPStateTxCompleted" 

// Connection state
typedef enum {
  kDDCMPPhaseStartConn,					// 0
  kDDCMPPhaseWaitStart,					// 1

  kDDCMPPhaseSendStart,					// 2
  kDDCMPPhaseWaitStack,					// 3

  kDDCMPPhaseChangeBaudRate,			// 4
  kDDCMPPhaseConnJustEstablished,		// 5
  kDDCMPPhaseConnEstablished,			// 6

  kDDCMPPhaseFinishingReq,				// 7
  kDDCMPPhaseFinishing,					// 8
  kDDCMPPhaseFinishingDone,				// 9
  kDDCMPPhaseFinished,					// 10	0X0A
  kDDCMPPhaseAbort						// 11	0X0B
} DDCMPPhase;

// Transmission state
typedef enum {
  kDDCMPTxPhaseSend,          			// Send frame
  kDDCMPTxPhaseSending,       			// Sending in progress
  kDDCMPTxPhaseWaitAck,       			// Waiting for acknowledge
  kDDCMPTxPhaseAckd,          			// Frame acknowledged
  kDDCMPTxPhaseDone         			// Tx completed
} DDCMPTxPhase;

typedef struct {
  uint16_t		crc;        			// Current CRC value
  uint8_t*    	buff;       			// Buffer
  uint16_t    	buffLen;     			// Buffer len
  uint16_t    	frameLen;     			// Frame data len (rx only)
  uint16_t    	count;        			// Tx/rx count
  uint8_t     	frameNum;     			// Frame number
  bool      	selectFlag;     		// Value of select flag (solo per tx)
} DDCMPBuff;

typedef struct {
  DDCMPMode   	mode;       			// Master/slave mode
  DDCMPState    state;        			// Protocol state
  DDCMPPhase    phase;        			// Protocol phase
  DDCMPTxPhase  txPhase;      			// Transmission phase
  DDCMPBaudRate baudRate;     			// Current baud-rate
  DDCMPBaudRate baudRateMin; 			// baud-rate min
  #if kDDCMPExtendedAddrSupport
  uint8_t     	frameHdr[kDDCMPFrameHdrLen+kDDCMPExtendedAddrLen];
  #else
  uint8_t     	frameHdr[kDDCMPFrameHdrLen];
  #endif
  uint8_t     	privRxBuff[2];    		// Private rx buffer
  bool      	closeConnReq;   		// Close connection request
  uint8_t     	startAttemptsCount; 	// Number of STARTs packets sent
  TaskId      	taskId;       			// Control task
  Tmr32Local    interByteTmr;   		// Rx interbyte timer
  Tmr32Local    pollTmr;      			// Start/poll timer
  Tmr32Local    ackTmr;       			// Frame ack timer
  Tmr32Local    activityTmr;    		// Connection activity timer
  DDCMPBuff   	tx;         			// Tx buffer
  DDCMPBuff   	rx;         			// Rx buffer
  uint8_t*      rxData;       			// Ptr to data part of last frame received
  uint16_t      rxDataLen;      		// Len of data part of last frame received
  uint16_t      rxDataCRC;      		// CRC of data part of last frame received
  bool      	rxSelectFlag;   		// Select flag of the last frame received
  uint16_t      rxReqBuffLen;   		// Next rx request buffer len
  uint8_t*      rxReqBuff;      		// Next rx request buffer
  uint8_t     	txRetryCnt;     		// Transmission retry counter
  bool      	sendDataAck;    		// Send acknowledge for previous data packet received
  bool      	usingPrivRxBuff;  		// Used to detect if we changed buffer while rx in progress
  bool      	InConnessioneDDCMP;  	// Attivata quando ricevo 05 in WaitStart
  uint8_t		ShowAudit;				// Visualizza Audit sul display
  #if kDDCMPTurnRoundSupport
  bool      	waitTurnRound;    		// Turn-round time in progress
  uint8_t     	turnRoundTimeout; 		// Turn-round timeout
  Tmr32Local    turnRoundTmr;  			// Rx-Tx turn round timer
  #endif
  #if kDDCMPLineDelaySupport
  bool      	isRemote;     			// This is a remote connection
  #endif
  #if kDDCMPExtendedAddrSupport
  uint8_t     	addr;       			// Standard address
  const uint8_t*  extAddr;      		// Extended address
  #endif
  #if kDDCMPStatisticsSupport
  DDCMPConnStats  stats;        		// Connection statistics
  #endif
  uint8_t		EndTxAudit;				// Fine Trasmissione Audit, attesa FINISH
} DDCMPVars;



/*--------------------------------------------------------------------------------------*\
DDCMP interface routines
\*--------------------------------------------------------------------------------------*/

void DDCMPTask(void);
void DDCMPRxIndData(uint8_t data);

bool DDCMPClosePossible(void);
  // Test if it is possible to close DDCMP

void DDCMPOpen(DDCMPMode mode/*, uint16_t rxBuffSize*/);
	/* Open and initializes DDCMP protocol.
	When the protocol is opened in master mode it will try periodically
	to establish a connection with a slave device. When the connection is
	finished (succesfully or unsuccesfully) the protocol must be closed
	and reopened in order to re-establish a new connection. 
	In slave mode this is not necessary, because DDCMP is always ready
	to accept a new connection.
	rxBuffSize specifies the max rx buff size that will be used. If 0
	the default buff size of 256 is assumed [Ref1p3.27]
	*/

void DDCMPClose(DDCMPLineRelease release);
	/* Close and terminate DDCMP protocol. 	Do nothing if DDCMP already closed*/

void DDCMPRun(void);
	/* If tasks not used call this periodically to run DDCMP */

void DDCMPConnect(void);
	/* Must be called in master mode to establish a new connection.
	Ignored [in slave mode or] if connection already established
	*/

void DDCMPDisconnect(void);
	/* Must be called [in master mode] to terminate the connection.
	Ignored [in slave mode or] if no connection
	*/

bool DDCMPFrameSend(const void* frameData, uint16_t frameLen, bool selectFlag);
	/* Send a DDCMP frame with the specified content.
	Data message header and CRC are appended automatically.
	DDCMPHookFrameSentInd() is called when the frame has been sent succesfully,
	otherwise DDCMPHookConnFailedInd()
	Return false if tx request rejected (DDCMP already busy)
	*/

bool DDCMPFrameReceive(void* rxBuff, uint16_t rxBuffLen);
	/* Enable reception of a new DDCMP frame
	This method should be called each time the content of a frame
	received with DDCMPHookFrameRcvdInd() has been processed.
	Return false if rx request rejected (i.e. another request already pending)
	If rxBuff is nil, just acknowledge previous data frame received, if not already done
	*/

void	Init_DDCMP_Protocol(void);
bool	ConnessioneDDCMP_IrDA(void);
bool 	IncomeSTART_DDCMP(void);
void 	Set_Phase_kDDCMPPhaseFinished(void);
void  	LogTxData_DDCMP(uint8_t txdata);
//void	LogTxData_devCOM4(uint8_t);
void  	LogRxData(uint8_t, bool);


#if kDDCMPExtendedAddrSupport
void DDCMPExtendedAddrSet(const DDCMPExtendedAddr extendedAddress);
	/* Set extended address. This is used as the remote address for master connections,
	as the local address for slave connections.
	If extendedAddress is nil, use standard addressing
	*/
#endif

#if kDDCMPStatisticsSupport
void DDCMPConnStatisticsGet(DDCMPConnStats* connStats);
	/* Can be called at any time to read connection statistics
	*/
#endif

/*--------------------------------------------------------------------------------------*\
External routines needed
	These routines are product-specific, so must be provided in some other module
	They are the interface between this module and the rest of the system
\*--------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------*\
User hooks
\*--------------------------------------------------------------------------------------*/
void DDCMPHookFrameSentInd(void);
	// Called by DDCMP to indicate that transmission of previous frame has been
	// completed succesfully, and it is ready to accept a new packet to send

void DDCMPHookFrameRcvdInd(void* frameData, uint16_t frameLen, bool selectFlag);
	// Called by DDCMP to indicate that a valid frame has been received

void DDCMPHookConnEstablishedInd(void);
	// Called when DDCMP connection becomes established

void DDCMPHookConnFinishedInd(void);
	// Called when DDCMP connection is completed normally

void DDCMPHookConnFailedInd(void);
	// Called when DDCMP connection is aborted/failed

void DDCMPHookSendStart(void);
	// Called when DDCMP send Start string 

void DDCMPHookWaitStart(void);
	// Called when DDCMP wait Start string 

bool  DDCMPHookLineRequest(void);
  // Return true if DDCMP can use multiplexed line

void	DDCMPHookLineRelease(void);
	// Called when DDCMP release multiplexed line

/*--------------------------------------------------------------------------------------*\
Low level hooks
	Use DDCMPCommPortAssign() macro to easily connect them to a serial port
\*--------------------------------------------------------------------------------------*/
/*MR19
void DDCMPHookBaudRateChange(uint8_t BdRate);
void DDCMPHookCommClose(void);
void DDCMPHookTxBegin(void);
void DDCMPHookTxEnd(void);
void DDCMPHookRxBegin(void);
void DDCMPHookRxEnd(void);
void DDCMPHookCharTx(void);
void DDCMPHookCheckFineTxAudit(void);
void DDCMPHookRxFinish(void);
*/
	// UART handler

//MR19 uint8_t DDCMPHookBaudRateMaxGet(void);
	// Return maximum baud rate supported by the DDCMP communication port
	// See 'BitRate Type' in DevSerial.h

//MR19 bool DDCMPHookIrDAComm(void);
  // Return true if ddcmp via IrDA

/*--------------------------------------------------------------------------------------*\
Routines Prototypes
\*--------------------------------------------------------------------------------------*/
/// static TaskPhase DDCMPTask(TaskPhase fase, TaskArg arg);
static void DDCMPStateInit(void);
static void DDCMPDataAckHandle(void);
static void DDCMPCommOpen(void);
static void DDCMPConnectStart(void);
static void DDCMPSerOptInit(DDCMPBaudRate br);
static void DDCMPHandleFrameTx(void);
static bool DDCMPHandleFrameRx(void);
static void DDCMPRxRequestActivate(void);
static void DDCMPHandleTurnRoundTmr(void);
static void DDCMPBaudRateRequest(DDCMPBaudRate reqBaudRate);
static void DDCMPCRCInit(uint16_t* crc);
static void DDCMPCRCUpdate(uint8_t data, uint16_t* crc);
static uint16_t DDCMPGetTO(uint8_t cto);
//static uint16_t DDCMPGetDelayedTO(uint8_t cto);
static void DDCMPFinishDone(void);
static void DDCMPFinishCheck(void);
static void DDCMPDecodeFrameHeader(void);
static void DDCMPDecodeCtrlHeader(void);
static void DDCMPDecodeDataHeader(void);
static void DDCMPDecodeFrameData(void);

static void DDCMPConfirmTxFrameNum(uint8_t frameNum);

static void DDCMPFrameTxPrapare(void);
static void DDCMPFrameTxStart(void);
static void DDCMPFrameRxStart(void);
static void DDCMPTurnRoundTmrStart(void);

static void DDCMPSendDataFrame(void);
static void DDCMPSendCtrlFrame(DDCMPCtrl ctrlCode, uint8_t b2, uint8_t b3, uint8_t b4);
static void DDCMPSendStart(void);
static void DDCMPSendStack(DDCMPBaudRate masterBaudRate);
static void DDCMPSendAck(void);
static void DDCMPSendNack(DDCMPNackErr errCode);
static bool DDCMPSendFinish(void);
static void RecordDati_DDCMP(void);

// Controlla se e' uno "START"
#define DDCMPIsStartPkt(pkt) (pkt[kDDCMPFrameHdrClass]==kDDCMPMsgClassControl && pkt[kDDCMPFrameHdrType_Len]==kDDCMPCtrlStart && pkt[kDDCMPFrameHdrFlags]==kDDCMPSyncFlagMask && pkt[kDDCMPFrameHdrSBD_RR]==0x00)


#endif



