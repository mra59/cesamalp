/*--------------------------------------------------------------------------------------*\
File:
	ICPHook.h

Description:
    Dichiarazione strutture e routines file ICPHook

History:
	Date	   Aut	Note
    Giu 2019	MR   
\*--------------------------------------------------------------------------------------*/

#ifndef __ICPHook_H_
#define __ICPHook_H_

/*--------------------------------------------------------------------------------------*\
ICP Types & Constants
\*--------------------------------------------------------------------------------------*/

typedef enum {
	ICP_CHG_LINK = 0x01,
	ICP_BILL_LINK = 0x02,
	ICP_CPC_LINK = 0x04,
	ICP_USD1_LINK = 0x08,
	ICP_USD2_LINK = 0x10,
	ICP_USD3_LINK = 0x20
} ET_SLAVELINK;


/*--------------------------------------------------------------------------------------*\
Function prototypes
\*--------------------------------------------------------------------------------------*/
void ICPHook_SetSlaveLink(ET_SLAVELINK slaveLinkMask);
void ICPHookCommOpen(void);

void ICPHookPutChar(uint8_t ch);
void ICPHookModeBitSet(void);
void ICPHookModeBitClear(void);
bool ICPHookModeBitTest(void);
void ICPHook_GetManufactCode(uint8_t *ptData, uint8_t dataLen);
void ICPHook_GetSerialNumber(uint8_t *ptData, uint8_t dataLen);
void ICPHook_GetModelNumber(uint8_t *ptData, uint8_t dataLen);
uint16_t ICPHook_GetSWVersion(void);
void ICPHook_SetLink(bool link);
bool ICPVMCHook_GetSlaveMode(void);
void  Gestione_MDB(void);
void ICPHook_DateTimeSyncSet(void);
bool ICPHook_DateTimeSyncGet(void);
void ICPHook_DateTimeSync(void);
void ICPHook_DateTimeSet(uint8_t *dt);

/*
void ICPHookTxBegin(void);
void ICPHookTxEnd(void);
void ICPHookRxBegin(void);
void ICPHookRxEnd(void);
*/


#endif		// __ICPHook_H_


