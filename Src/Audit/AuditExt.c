/* ***************************************************************************************
File:
    AuditExt.c

Description:
    Funzioni per la creazione e l'esportazione dell'Audit Estesa, sia tramite DDCMP
    che tramite Log File su chiave USB.
    

History:
    Date       Author
    Mar 2015 	MR   

 *****************************************************************************************/

#include <string.h>
#include <stdio.h>
//#include <time.h>


#include "ORION.H" 
#include "IICEEP.h"
#include "AuditExt.h"
#include "Power.h"
#include "Funzioni.h"

#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void  	InitLogBufferPointers(bool MemClr);
extern	void  	FromSecondsToDateTime(uint32_t, uint32_t *data, uint *time);
extern	void	LeggiOrologio(void);
extern const char 	AUDIT_CRLF[];
extern const char 	AUDIT_JSON_CRLF[];



/*--------------------------------------------------------------------------------------*\
Global Defines
\*--------------------------------------------------------------------------------------*/

#define ExtAuditFullLimit  (ExtAuditMaxBlocks-20)

/*========================================================================================*/
// Dichiarazione delle strutture

sExtAuditHdr 		header;
sExtAuditBlock 		ExtAuditBlock;

/*========================================================================================*/


void  WriteAuditExtHeader(sExtAuditHdr);
void  WriteAuditExtBlock(sExtAuditHdr, sExtAuditBlock);
void  SetExtAuditBlock(sExtAuditBlock*, byte);
static void  SetExtAuditDataTime(byte*);


//******************************************************************************************************
//******************************************************************************************************
//*******************       FUNZIONI  DI  GESTIONE   AUDIT   ESTESA                 ********************
//******************************************************************************************************
//******************************************************************************************************

/*---------------------------------------------------------------------------------------------*\
Method: TestAuditExtFullStatus
	Controlla se lo spazio EE di Audit Estesa e' pieno.
	Si controlla anche la flag RollOver perche', dopo un rollover, il numero di blocchi e'
	sicuramente inferiore al limite massimo ammesso e risulterebbe memoria non piena.
 
	IN:		- 
	OUT:	- Set flag Audit Estesa Full se memoria EE piena, altrimenti clr flag 
\*----------------------------------------------------------------------------------------------*/
void TestAuditExtFullStatus(void)
{

	sExtAuditHdr	Local_Hdr;
	
	if (!gOrionConfVars.TestAudExtFull) return;																// Inibizione da Audit estesa full non abilitata
	IICEEPR((byte *)(&Local_Hdr), IICEEPExtAuditHeader(), sizeof(Local_Hdr));								// Leggo Aud Ext Header
	if ((Local_Hdr.Blocks >= ExtAuditFullLimit) || Local_Hdr.RollOver) {									// Raggiunto o superato il limite delle registrazioni
		gVars.AudExtFull = true;
	} else {
		gVars.AudExtFull = false;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtNumBlocks
	Legge numero di blocchi di Audit Estesa.
	Se non ci sono blocchi il contatore esce con 1.
 
	IN:		- 
	OUT:	- Max Numero Blocchi se RollOver=set, altrimenti il numero di blocchi registrati
\*----------------------------------------------------------------------------------------------*/
uint16_t AuditExtNumBlocks(void)
{
	sExtAuditHdr  Local_Hdr;
	
	IICEEPR((byte *)(&Local_Hdr), IICEEPExtAuditHeader(), sizeof(Local_Hdr));
	if (Local_Hdr.RollOver)
	{
		return ExtAuditMaxBlocks;
	}
	else
	{
		return Local_Hdr.Blocks; 
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: ReadExtAudDataBlock
	Legge un blocco dati di Audit Estesa e lo memorizza nel buffer destinazione.
	Inserisce l'ID MA5*Num* come header blocco, dove Num e' il numero del blocco inviato.
	L'ultimo blocco riporta MA5*LAST*.
 
	IN:		- Pointer al blocco da inviare, numero totale dei blocchi e buffer destinazione
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void ReadExtAudDataBlock(uint16_t numBlock, uint16_t totalBlocks, char* buffer)
{
	sExtAuditBlock 		EAB;
	uint32_t			GMA, Secondi;
	uint16_t			HM;
	uint8_t				len;
  
	IICEEPR((byte *)(&EAB), IICEEPExtAuditBlock(numBlock), sizeof(EAB));
	if (numBlock == 0)
	{																							// 21.07.15 Per ora nel Tipo 0 metto solo data/ora attuale e Codice Macchina
		IICEEPR((byte *)(&header), IICEEPExtAuditHeader(), sizeof(header));						// Leggo numero prelievo in header.NumReads
		Secondi = RTC_Counter;
	}
	else
	{
		Secondi = EAB.dateTime[3];
		Secondi |= EAB.dateTime[2]<<8;
		Secondi |= EAB.dateTime[1]<<16;
		Secondi |= EAB.dateTime[0]<<24;
	}
	FromSecondsToDateTime(Secondi, &GMA, &HM);
	
	if (++numBlock == totalBlocks)
	{
		if ((numBlock == 1) && (totalBlocks == 1) ) 
		{																						// Nessun blocco, invio solo Header perche' il cntr prelievo e' comunque incrementato 
			sprintf(buffer,"MA5*LAST*%06u%04u*%u*%04x*%u*%d*%u*%u",GMA,HM,TypeZero,ExtAudRev_DDCMP,0x00,header.NumReads,0x00,CodiceMacchina);
		}
		else
		{
			sprintf(buffer,"MA5*LAST*%06u%04u*%u*%u*%u*%u*%u*%u*%u",GMA,HM,EAB.type,EAB.value,EAB.selNumber,EAB.userCode,EAB.keyValue,EAB.value1,EAB.value2);
		}
	}
	else
	{
		if (numBlock == 1)
		{																	
			// Blocco Tipo 0: intestazione.
			// 21.07.15 Per ora nel Tipo 0 metto solo data/ora attuale e Codice Macchina.
			// 29.10.15 Aggiunto un ID ad indicazione della revisione dell'Audit Estesa per distinguere i contenuti che possono essere modficati nel tempo.
			// 10.12.15 Aggiunto un ID che contiene in contatore del numero del prelievo.
			sprintf(buffer,"MA5*%u*%06u%04u*%u*%04x*%u*%d*%u*%u",numBlock,GMA,HM,TypeZero,ExtAudRev_DDCMP,0x00,header.NumReads,0x00,CodiceMacchina);
		}
		else
		{
			sprintf(buffer,"MA5*%u*%06u%04u*%u*%u*%u*%u*%u*%u*%u",numBlock,GMA,HM,EAB.type,EAB.value,EAB.selNumber,EAB.userCode,EAB.keyValue,EAB.value1,EAB.value2);
		}
	}
	//-- Aggiungo NL alla fine della stringa --
	len = strlen(buffer);
	if (JSON_Format == true)
	{
		memcpy((buffer+len), &AUDIT_JSON_CRLF[0], strlen(&AUDIT_JSON_CRLF[0]));
		len += strlen(&AUDIT_JSON_CRLF[0]);
		*(buffer+len) = 0;																		// End of string \0
	}
	else
	{
		memcpy((buffer+len), &AUDIT_CRLF[0], strlen(&AUDIT_CRLF[0]));
		len += strlen(&AUDIT_CRLF[0]);
		*(buffer+len) = 0;																		// End of string \0
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtClearData
	Azzera Audit Estesa azzerando la struttura in EE che contiene il numero di blocchi.
	Se flag AzzCntPrelievi = true significa che la funzione e' stata chiamata da qualche
	funzione di azzeramento audit: pertanto azzero il contatore prelievi.
	Se la flag = false lo incremento perche' significa che la funzione e' stata chiamata dopo
	un prelievo Audit Estesa. 
 
	IN:	 - flag AzzCntPrelievi per azzerare o incrementare contatore numero prelievi
	OUT: - 
\*----------------------------------------------------------------------------------------------*/
void AuditExtClearData(bool AzzCntPrelievi) {
	
	PSHProcessingData();
	IICEEPR((byte *)(&header), IICEEPExtAuditHeader(), sizeof(header));
	header.Blocks = 1;																			// 21.07.15 Il primo blocco e' riservato a counter prelievi e data
	header.RollOver = 0;
	if (AzzCntPrelievi) {
		header.NumReads = 0;
	} else {
		header.NumReads++;
	}
	IICEEPW(IICEEPExtAuditHeader(), (byte *)(&header), sizeof(header));
	PSHCheckPowerDown();
	gVars.AudExtFull = false;
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtInitData
	Inizializza Audit Estesa azzerando tutto il contenuto della struttura header e del 
	"log file content lenght" nell'area di interfaccia bootloader.
	Inizializza anche tutti i valori in EEProm per poter utilizzare il Log buffer.
	Chiamata dal Reset quando si attiva l'uso dello spazio di eeprom Log per l'Audit Estesa.
	 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void AuditExtInitData(void)
{
	IICEEPR((byte *)(&header), IICEEPExtAuditHeader(), sizeof(header));							// Leggo counter blocchi
	if (header.Blocks >= ExtAuditMaxBlocks || header.Blocks == 0)
	{
		header.Blocks = 1;																		// 21.07.15 Il primo blocco e' riservato a counter prelievi e data
		header.NumReads = 0;
		if (header.Blocks >= ExtAuditMaxBlocks)
		{
			header.RollOver = 1;
		}
		else
		{
			header.RollOver = 0;
		}
		IICEEPW(IICEEPExtAuditHeader(), (byte *)(&header), sizeof(header) );
	}
	InitLogBufferPointers(false);																// Set pointers nell'area interscambio boot ma senza azzerare il buffer Audit
}

/*---------------------------------------------------------------------------------------------*\
Method: SetExtAuditBlock
	Riempie la struttura in RAM tipo "sExtAuditBlock" dei valori relativi all'operazione
	eseguita sulla carta.
 
	IN:		- Address struttura "sExtAuditBlock" e tipo di operazione eseguita
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  SetExtAuditBlock(sExtAuditBlock* Block, byte type) {
  
	LeggiOrologio();																			// Legge orologio per determinare data/ora prelievo attuale
	SetExtAuditDataTime(&Block->dateTime[0]);													// Inserisce data/ora in Block->dateTime
    Block->type 	= type;																		// Tipo operazione eseguita
    Block->selNumber = NumeroSelezione;
    Block->value 	= ValoreSelezione;
    Block->userCode = CodiceUtenteKR;															// Codice utente
    Block->keyValue = NewCreditKR;																// Credito finale carta
    Block->value1 	= NewFreeCreditKR;															// Free Credit finale carta
    Block->value2 	= NewFreeVendKR;															// Free Vend finale carta
}

/*---------------------------------------------------------------------------------------------*\
Method: SetExtAuditDataTime
	Copia nell'elemento "Block.dateTime" i 4 bytes di RTC_Counter.
 
	IN:		- Address elemento ".dateTime" della struttura sExtAuditBlock
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  SetExtAuditDataTime(byte* data) {
	
    *data 	  = ((RTC_Counter & 0xff000000) >> 24);
    *(data+1) = ((RTC_Counter & 0x00ff0000) >> 16);
    *(data+2) = ((RTC_Counter & 0x0000ff00) >> 8);
    *(data+3) =   RTC_Counter & 0x000000ff;
}

/*---------------------------------------------------------------------------------------------*\
Method: WriteAuditExtHeader
	Aggiorna il contatore in EE dei blocchi di Audit Estesa.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  WriteAuditExtHeader(sExtAuditHdr header) {
	
	IICEEPR((byte *)(&header), IICEEPExtAuditHeader(), sizeof(header) );
	header.Blocks++;
	if (gOrionConfVars.TestAudExtFull && (header.Blocks >= ExtAuditFullLimit)) {
		gVars.AudExtFull = true;
	} else {
		gVars.AudExtFull = false;
	}
	if (header.Blocks >= ExtAuditMaxBlocks) {
		header.Blocks = 1;																		// 21.07.15 Il primo blocco e' riservato a counter prelievi e data
		header.RollOver = 1;
	}
	PSHProcessingData();
	IICEEPW(IICEEPExtAuditHeader(), (byte *)(&header), sizeof(header) );
}

/*---------------------------------------------------------------------------------------------*\
Method: WriteAuditExtBlock
	Scrive in EE il nuovo record dati.
	La funzione legge il numero di blocco gia' in memoria per determinare l'indirizzo di EE
	dove andra' memorizzato il nuovo record.
 
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  WriteAuditExtBlock(sExtAuditHdr header, sExtAuditBlock ExtAuditBlock) {
	
  IICEEPR((byte *)(&header), IICEEPExtAuditHeader(), sizeof(header) );
  IICEEPW(IICEEPExtAuditBlock(header.Blocks), (byte *)(&ExtAuditBlock.dateTime[0]), sizeof(ExtAuditBlock) );
}

/*---------------------------------------------------------------------------------------------*\
Method: Check_ExtAuditFull
	Controlla se Audit Estesa Full per inibire tutto il sistema e visualizzare il messaggio
	"AudF" sul display.
 
	IN:		- 
	OUT:	- true se Audit Estesa Full e non c'e' credito 
\*----------------------------------------------------------------------------------------------*/
bool  Check_ExtAuditFull(void) {
	
	if (gOrionConfVars.AuditExt && gOrionConfVars.TestAudExtFull) {
		if (!gVars.AudExtFull) return false;
		if (VMC_CreditoDaVisualizzare == 0 && !gOrionKeyVars.Inserted) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: SetBlockTypeZero
	Predispone il blocco tipo 0 quando si preleva l'Audit Estesa con chiave USB: inserisce
	data/ora attuali e codice macchina.
	29.10.15 Aggiunto un ID ad indicazione della revisione dell'Audit Estesa per distinguere
	i contenuti che possono essere modficati nel tempo.
	10.12.15 Aggiunto un ID ad indicazione del numero del prelievo attuale.
 
	IN:		- 
	OUT:	-  
\*----------------------------------------------------------------------------------------------*/
void  SetBlockTypeZero(void) {
	
	IICEEPR((byte *)(&header), IICEEPExtAuditHeader(), sizeof(header) );								// Leggo Header dalla EE per avere il contatore del prelievo

	ValoreSelezione = ExtAudRev_USB;
	NumeroSelezione = 0;
    CodiceUtenteKR	= header.NumReads;
    NewCreditKR		= 0;
    NewFreeCreditKR = CodiceMacchina;
    NewFreeVendKR 	= CodiceMacchina>>16;
	SetExtAuditBlock(&ExtAuditBlock, 0x00);
	IICEEPW(IICEEPExtAuditBlock(0x00), (byte *)(&ExtAuditBlock.dateTime[0]), sizeof(ExtAuditBlock) );	// Scrivo i dati nel blocco 0 dell'Audit Estesa
	NewFreeCreditKR = 0;																				// Azzero i valori per non ritrovarli nel primo record successivo
    NewFreeVendKR 	= 0;
}


//******************************************************************************************************
//******************************************************************************************************
//*******************       FUNZIONI  DI  REGISTRAZIONE   AUDIT   ESTESA            ********************
//******************************************************************************************************
//******************************************************************************************************

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtVendSuccessConCash
	=============   Tipo  Operazione  N.  1  ====================
	Registra Audit Estesa per le vendite con Cash
 
	IN:	 - 
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtVendSuccessConCash(void) {

	SetExtAuditBlock(&ExtAuditBlock, kVendCash);
	ExtAuditBlock.value = VendAmountSEL + VendAmountBR;
	ExtAuditBlock.keyValue = 0;
	ExtAuditBlock.userCode = 0;
	WriteAuditExtBlock(header, ExtAuditBlock);												// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);															// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtVendConKR
	=============   Tipo  Operazione  N.  2 - 3 - 8 - 10  ====================
	Registra Audit Estesa per le vendite con Carta, sia OK che FAIL Vend.
	- Tipo Operazione N. 2: vendita con card credit cash e/o Free Credit
	- Tipo Operazione N. 3: vendita con Free Vend
	- Tipo Operazione N. 8: Test Vend
	- Tipo Operazione N.10: Fail Vend
 
	IN:	 - Esito selezione: 0=Ok 1=fallita
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtVendConKR(uint8_t esitovend) {

	SetExtAuditBlock(&ExtAuditBlock, kVendCard);											// Tipo Operazione N. 2: registra data/ora, Credito carta, Free credit e Codice Utente
	if (esitovend) {
		// *****  FAIL Vend  ******
		ExtAuditBlock.type = kVendFailCard;													// Tipo Operazione N.10: Fail Vend con carta (solo Numsel, valselez e cod utente)
		ExtAuditBlock.keyValue = 0;
		ExtAuditBlock.value1 = 0;
		ExtAuditBlock.value2 = 0;
	} else {
		// *****  VEND  OK   ******
		if (TipoVendita == VEND_CON_KR_PROVA) {
			ExtAuditBlock.type = kVendTest;													// Tipo Operazione N. 8: Vendita di test (registrata anche se Fail vend)
		} else {
			if (FreeVendLevel > 0) {
				ExtAuditBlock.type = kVendFree;												// Tipo Operazione N. 3: Vendita con Free Vend (registrata anche se Fail vend)
			}
		}
	}
	WriteAuditExtBlock(header, ExtAuditBlock);												// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);															// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtRevalueKRCash
	=============   Tipo  Operazione  N.  4  ====================
	Registra Audit Estesa per Cash caricato sulle carte.
 
	IN:	 - CashRicaricato contiene cash ricaricato sulla carta
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtRevalueKRCash(uint16_t CashRicaricato) {

	SetExtAuditBlock(&ExtAuditBlock, kRevalueCash);											// Tipo Operazione N. 4: registra data/ora, Credito carta, Free credit e Codice Utente
	ExtAuditBlock.selNumber = 0;
	ExtAuditBlock.value 	= CashRicaricato;												// Cash ricaricato
	WriteAuditExtBlock(header, ExtAuditBlock);												// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);															// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtFreeCredit
	=============   Tipo  Operazione  N.  5  ====================
	Registra Audit Estesa per Free Credit Aggiunto e/o Sottratto.
	Si memorizzano free credit aggiunto in "value", free credit finale in "value1", il 
	free credit sottratto in "value2" e il credito carta in "keyValue".
 
	IN:	 - CashRicaricato contiene cash ricaricato sulla carta
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtFreeCredit(void) {
  
	SetExtAuditBlock(&ExtAuditBlock, kFreeCredit);											// Tipo Operazione N. 5: registra data/ora, Credito carta, Free credit e Codice Utente
	ExtAuditBlock.selNumber = 0;
	ExtAuditBlock.value 	= FreeCreditAccreditato;
	ExtAuditBlock.value1 	= NewFC_Attribuito;												// Free Credit sulla carta
	ExtAuditBlock.value2 	= FreeCreditAddebitato;
	WriteAuditExtBlock(header, ExtAuditBlock);												// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);															// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtRefundKR
	=============   Tipo  Operazione  N.  6 - 7  ====================
	Registra Audit Estesa per refund da Fail Vend.
	Le due operazioni sono distinte dal momento nel quale e' effettuato il refund: l'operazione
	N. 6 e' registrata se la carta e' ancora nel lettore al termine della selezione fallita,
	altrimenti si registra l'operazione N. 7.
	Nell'operazione N. 6 il valore della selezione fallita e' contenuto in CashRecharged dato 
	che la funzione che predispone i dati per il reinstate e' la stessa del revalue cash.
	Nella N. 7 non sono registrati i dati della selezione fallita perche' non sono piu' dispo-
	nibili essendo l'operazione di refund possibile anche dopo mesi (si esegue al primo inseri-
	mento successivo della carta nel lettore).
 
	IN:	 - Tipo di Refund: immediato (0x00) o alla successivareintroduzione della carta (0x01)
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtRefundKR(byte TipoRefund) {

	if (TipoRefund == 0) {
		SetExtAuditBlock(&ExtAuditBlock, kRefundImmediate);										// Tipo Operazione N. 6: registra data/ora, Credito carta, Free credit e Codice Utente
	} else {
		SetExtAuditBlock(&ExtAuditBlock, kRefundDelayed);										// Tipo Operazione N. 7: registra data/ora, Credito carta, Free credit e Codice Utente
		ExtAuditBlock.selNumber = 0;
	}
	ExtAuditBlock.value	= CashRecharged;														// Valore della vendita fallita
	WriteAuditExtBlock(header, ExtAuditBlock);													// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);																// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtVendFailConCash
	=============   Tipo  Operazione  N.  9  ====================
	Registra Audit Estesa per Fail Vend con Cash.
 
	IN:	 - 
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtVendFailConCash(void) {

	SetExtAuditBlock(&ExtAuditBlock, kVendFailCash);											// Tipo Operazione N. 9: registra data/ora e valore+numero selezione
	WriteAuditExtBlock(header, ExtAuditBlock);													// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);																// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtCardWriteErrVendReq
	=============   Tipo  Operazione  N.  11  ====================
	Registra Audit Estesa per errore scrittua carta al Vend Request.
	Al posto delle FreeVend registra il codice di errore delle BFL.
 
	IN:	 - 
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtCardWriteErrVendReq(void) {

	SetExtAuditBlock(&ExtAuditBlock, kCardWriteErrVendReq);										// Tipo Operazione N. 11: registra data/ora, Credito carta, Free credit e Codice Utente
	ExtAuditBlock.value2 = gOrionKeyVars.WR_ErrCode;											// Codice BFL errore scrittura carta
	WriteAuditExtBlock(header, ExtAuditBlock);													// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);																// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtRevalueCartaCaricamentoBlocchi
	=============   Tipo  Operazione  N.  12  ====================
	Registra Audit Estesa per Cash caricato con carta caricamento a blocchi.
 
	IN:	 - CashRicaricato contiene cash ricaricato sulla carta
	OUT: - Esce con PSHProcessingData() set
\*----------------------------------------------------------------------------------------------*/
void  AuditExtRevalueCartaCaricamentoBlocchi(uint16_t CashRicaricato) {

	SetExtAuditBlock(&ExtAuditBlock, kRevalueCartaCaricBlocchi);								// Tipo Operazione N. 12: registra data/ora, Credito carta, Free credit e Codice Utente
	ExtAuditBlock.selNumber = 0;
	ExtAuditBlock.value 	= CashRicaricato;													// Cash ricaricato
	WriteAuditExtBlock(header, ExtAuditBlock);													// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);																// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditExtCardWriteFail
	=============   Tipo  Operazione  N.  13  ====================
	Registra Audit Estesa per errore scrittura carta Mifare
	Registra l'errore di scrittura carta da Revalue (Cash e Caric. a Blocchi) che puo'
	essere dovuto a vero e proprio errore di scrittura o estrazione carta prima della
	rilettura di controllo della scrittura stessa.
 
	IN:	 - 
	OUT: - Esce con PSHProcessingData() clr
\*----------------------------------------------------------------------------------------------*/

void  AuditExtCardWriteFail(uint8_t RevalType, uint16_t cashdaricaricare)
{
  
	SetExtAuditBlock(&ExtAuditBlock, kCardWriteErrRevalue);										// Tipo Operazione N. 13: registra data/ora, Credito carta, Free credit e Codice Utente
	ExtAuditBlock.selNumber = RevalType;														// Tipo di revalue
	ExtAuditBlock.value 	= cashdaricaricare;
	ExtAuditBlock.value1 	= 0;
	ExtAuditBlock.value2 = gOrionKeyVars.WR_ErrCode;											// Codice BFL errore scrittura carta
	WriteAuditExtBlock(header, ExtAuditBlock);													// Scrive il blocco dati in EE.... 
	WriteAuditExtHeader(header);																// ..ma solo ora lo rende attivo incrementando il counter dei blocchi scritti in EE 
}







