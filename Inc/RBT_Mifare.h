/****************************************************************************************
File:
    RBT_Mifare.h

Description:
    Include file con definizioni e strutture per l'uso delle carte Mifare

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __RBT_Mifare__H
#define __RBT_Mifare__H



uint16_t		ReadMifare(void);
uint16_t  		LeggiDatiDaMifare(void);
uint16_t		CheckMAD(void);
bool			ReadCreditSector(void);
bool			ReadGestSelSector(uint8_t);
bool			WrCredit(void);
uint16_t		InizializzaCartaUtente(void);
uint16_t		UpDateCard(bool);
void			SetKeyA(uint8_t *ValoreKey_A);
void			SetKeyB(uint8_t *);
void			CalcKeyB(uint16_t, uint16_t);
bool			MIFBlock_To_CodiciCarta(uint8_t *rdbuffPnt);
bool			MIFBlock_To_SectorInfo(uint8_t *rdbuffPnt);
bool			MIFBlock_To_CreditoCarta(uint8_t *rdbuffPnt);
void    		CreditoCarta_to_MIFBlock(uint8_t *wrbuffPnt);
uint16_t		Retry_Card_Activation(void);
bool			Check_CRC_MifareBlock(uint8_t *rdbuffPnt, uint16_t Seed);
void    		SetCRC16Buff(uint8_t *destbuff, uint8_t destbufflen, uint16_t Seme);
bool  			SaveRefund(REFUNDS* DestRefund, DATEREFUNDS* DestDateRefund, TipiRefund TipoRefund, CreditCpcValue RefundValue, bool TestPWD);
bool 			ReadRefundValues(REFUNDS* DestRefund, DATEREFUNDS* DestDateRefund, TipiRefund TipoRefund);
bool 			GiveRefund(void);
uint8_t			Calc_CRC8_MAD(uint8_t *start, uint8_t length);
void 			decipher(uint32_t*, const uint32_t*);
bool			CheckSicurezza(void);
void  			ClrCardFlags(void);
uint16_t  		MasterCard(void);
uint16_t  		Card_ClrMemory(uint8_t);
uint16_t  		ClrAuditCard(void);
bool  			LeggiSettore(void);
void  			AzzeraCreditCard(void);
bool  			ArePinZero(void);
void  			CheckCartaRBT(void);
static	uint16_t	GestioneStatoEnabled(void);
static  bool 	WriteKeyB_UsedSector(uint8_t);
void  			SetDataOdiernaFreeCredit(void);
void			SetDataOdiernaFreeVend(void);
bool  			SuspendWriteNew_FC_FV(void);
void  			Test_FC_FV_AllaSelezione(void);
void  			RifiutaCarta(void);

#if (SetVergineRBT == 1)
	void 	encipher(uint32_t* cryptmsg, const uint32_t* k);
#endif

#if (Compila_Cifra == 1)
	void 		CalcolaCodiciPcLink(uint8_t * CodeDest);
#endif

#if (SetVergineRBT == 1)
	uint16_t  	Da_Costruttore_A_Vergine_MHD(void);
	uint16_t	Da_Costruttore_A_Vergine_Prepagata_MHD_Con_Taglio_10_Euro(void);
	uint16_t	DaUtenteACostruttore(void);
	uint16_t	WriteSector(void);
	void 		CheckIfNMIPending(void);
	void 		ClrWrBuff(void);
	uint16_t 	CalcolaDatiCarta(void);
	void		CalcKeyB_FreeSectors(uint8_t);
	#define	MinFreeSect		5																		// Numero di settori liberi perche' una carta possa lavorare nei sistemi PK3 MHD
#endif

extern	uint32_t	cryptuid;

/*===============================================================
 *               Definizioni   MIFARE    
 * =============================================================*/

#define  NumAIDMax			     5		// Numero di Settori previsti nelle Mifare con applicazione RBT
#define  GPB_POSITION         0x09		// Posizione del GPB nel blocco
#define  KeyB_POSITION        0x0A		// Posizione della KeyB nel blocco
#define  MAD_Bit              0x80		// Bit 7: posizione del bit indicante la presenza del MAD
#define  Tipo_MAD_Mask        0x03		// Bits ADV (bit 0-1: tipo di MAD - 0x01: MAD Version 1 - 0x02: MAD Version 2
#define  Tipo_MAD1            0x01		// Bits ADV (bit 0-1: tipo di MAD - 0x01)

#define  RBT_ClusterCode      0x69		// ID RBT
#define  RBT_ApplicationCode  0x38		// ID Marca Applicazioni RBT
#define  RBT_GenInfo		  0x20		// ID per il Settore InfoSector che contiene gli indici dei settori con Applicazione RBT
#define  RBT_Vergine		  0x21		// ID per il Settore Infosector che indica la carta Vergine RBT
#define  RBT_Credito		  0x24		// ID per il Settore che contiene il Credito 
#define  RBT_MasterCard		  0x25		// ID per il Settore che contiene i nuovi codici PIN e GEST
#define  RBT_SelGest1		  0x26		// ID per il Settore che contiene le informazioni per la gestione delle selezioni da 1 a 4
#define  RBT_SelGest2		  0x28		// ID per il Settore che contiene le informazioni per la gestione delle selezioni da 5 a 8
#define  RBT_CaricamBlk		  0x2A		// ID per il Settore che contiene i valori del Caricamento a Blocchi 
#define  RBT_Secure		  	  0x33		// ID per il Settore Sicurezza
#define  RBT_MHD_AudClrCard	  0xA0		// ID che identifica la Carta Azzeramento Audit (MHD ad uso interno)
#define  RBT_MHD_EEClrCard    0xA1		// ID che identifica la Carta Azzeramento Memoria EEPROM (MHD ad uso interno)
#define  RBT_Gest_EEClrCard   0xA2		// ID che identifica la Carta Azzeramento Memoria EEPROM del gestore

#define  PrepagataInfoSct     0x40		// ID per il Settore InfoSector della carta prepagata
#define  Verg_Prep_InfoSct    0x41		// ID per il Settore Infosector della prepagata vergine
#define  PrepagataCreditSct	  0x48		// ID per il Settore che contiene il Credito attivo di una Prepagata 
#define  Verg_Prep_CreditSct  0x27		// ID per il Settore che contiene il Credito di una Prepagata SENZA TAGLIO 



#define  Block0				  0x00		// Blocco 1 del settore
#define  Block1				  0x01		// Blocco 2 del settore
#define  Block2				  0x02		// Blocco 3 del settore
#define  Trailer			  0x03		// Blocco 4 del settore, ovvero il trailer
#define  NoBlock	          0xFF		// Blocco da non leggere/scrivere

#define  FlagCredMask         0x80		// Maschera bit flag credito
#define  SetFlagCred          0xFF		// Flag credito 2
#define  ClrFlagCred          0x7F		// Flag credito 1

#define  InfoSectorNum		     2		// Settore della Carta che contiene le informazioni
#define  InfoSectMasterCard	     2		// Settore della MasterCard che contiene le informazioni
#define  MHDCreditSector		 3		// Settore delle Carte formattate da MHD che contiene il credito
#define  SelGest1_Sector		 4		// Settore delle Carte formattate da MHD che contiene le informazioni per la gestione delle selezioni da 1 a 4
#define  SelGest2_Sector		 5		// Settore delle Carte formattate da MHD che contiene le informazioni per la gestione delle selezioni da 5 a 8
#define  Secure_Sector		 	15		// Settore delle Carte formattate da MHD che contiene le informazioni per la sicurezza



#define  Secur0_ManReadPos  	32						// Posizione del primo byte del Secure 0 quando leggo il Settore 0 in CheckMAD
#define  Secur1_ManReadPos    Secur0_ManReadPos+4		// Posizione del primo byte del Secure 1 quando leggo il Settore 0 in CheckMAD
#define  Secur2_ManReadPos    Secur1_ManReadPos+4		// Posizione del primo byte del Secure 2 quando leggo il Settore 0 in CheckMAD
#define  Secur3_ManReadPos    Secur2_ManReadPos+4		// Posizione del primo byte del Secure 3 quando leggo il Settore 0 in CheckMAD


/* Le routine CRC effettuato il "transpos" ovvero ruotano i bit nei bytes 
	e i bytes stessi: ad es posto 5903 il seme da inserire, la define deve
	contenere 0xC09A */
#define  SemeCRC_Std          0x0000	// Seme per calcolo CRC standard
#define  SemeKeyB_Part1       0xC09A	// Seme Parte 1 0x5903
#define  SemeKeyB_Part2       0x489A	// Seme Parte 2 0x5912
#define  SemeKeyB_Part3       0x6024	// Seme Parte 3 0x2406
#define  SemeCrypto			  0x59A6

#define  MIF_SectUsed				6	// Numero di Settori usati



/*===============================================================
 *                     Response Error Values
 * =============================================================*/

#define Resp_OK               (0x0000)            
#define MAD1_OK               (0x0100)   
#define NoMAD_SectRBT         (0x0101)            
#define MAD1_NoSectRBT        (0x0102)            
#define MAD2                  (0x0103)   
#define NoMAD                 (0x0104)   
#define NoInfoSect            (0x0105)
#define MADReadErr            (0x0106)
#define NoCreditSect          (0x0107)
#define ErrWriteRefund        (0x0108)
#define MAD_CRC_Err           (0x0109)
#define No_Vergine            (0x010A)
#define Format_Fail           (0x010B)
#define NoSecureSect          (0x010C)
#define SecureCRCErr          (0x010D)
#define SecureErr          	  (0x010E)
#define NoNewCodesSect        (0x010F)
#define FailWriteFreeSect     (0x0110)
#define ReaderInh		  	  (0x0111)
#define OK_WaitEstraz		  (0x0112)
#define NoSectCaricBlk        (0x0113)
#define NoSelGestSect         (0x0114)
#define ExtAudFull		  	  (0x0115)
#define NoEnoughSpace	  	  (0x0116)
#define AlreadyMHD            (0x0117)            
#define NoLevel			  	  (0x0118)				// Non e' programmato alcun livello sulla carta (Gestione Selezioni)

#define Fail_CRC              (0x0120)
#define Fail_Gestore          (0x0121)
#define Fail_Locaz            (0x0122)
#define Fail_TipoCarta        (0x0123)

#define NoSameCard            (0x0201)   
#define NoValidCard           (0x0202)   			// Errore di accesso o carta rimossa
#define NoValidFormat         (0x0203)  
#define NoRBTCard             (0x0204)  
#define PinZero               (0x0205)  			// Pin a zero

#define Err_Read              (0x0300)
#define Err_CAR_1			  (0x0301)
#define Err_CAR_6			  (0x0306)
#define Err_CAR_7			  (0x0307)				// Carta Caricamento a blocchi Vuota
#define Err_CAR_8			  (0x0308)				// Carta Caricamento a blocchi non abilitata all'accettazione

#define VendDenPerCash		   (0x0310)
#define VendDenOFFDDCMPExtFull (0x0311)
#define VendDenPerTipoKey	   (0x0312)
#define VendDenReadError	   (0x0313)				// Lettura fallita
#define VendDenWriteError	   (0x0314)				// Scrittura fallita
#define VendDenNoKey		   (0x0315)				// Chiave non inserita
#define VendDenNoIDLE		   (0x0316)				// Orion Main non in IDLE
#define VendDenNoPrice		   (0x0317)				// Prezzo Selezione non determinato
#define VendPending			   (0x0318)				// Precedente Vendita non conclusa
#define VendDenInsufCred	   (0x0319)				// Credito Insufficiente
#define NumBattuteInsuf	   	   (0x031A)				// Counter Battute Insufficiente
#define IntervalloBattuta  	   (0x031B)				// Non ancora trascorso l'intervallo dalla battuta precedente
#define NumSelezNotValid 	   (0x031C)				// Numero selezione non valido
#define SelNotEnabled	 	   (0x031D)				// Carta non abilitata alla selezione
#define NumTotaleBattuteInsuf  (0x031E)				// Counter Totale Battute Insufficiente
#define VendMinorenne		   (0x031F)				// Selezione non abilitata ai minorenni

#define FirstMAD_Read         (0xFF11)
#define SecondMAD_Read        (0xFF22)
#define NotYetElab            (0xFFFF)
#define WaitElab			  (0x5555)


//  Codice di errore che ricevo dalle BFL
#define PH_ERR_BFL_IO_TIMEOUT		(0x0001)
#define PH_ERR_BFL_CRC_ERROR		(0x0002)
#define PH_ERR_BFL_AUTHENT_ERROR	(0x0019)

#define BFL_ErrorCode				(0x00FF)		// Il codice di errore e' l'LSB: il MSB e' il component identifier

#define Cred_SUCCESS          (0x0000)            
#define Cred_CRC_ERR          (0x0001)
#define Cred_TOO_HIGH         (0x0002)

// -----  Fasi scrittura credito su MIFARE -----
#define	FaseAuth			0x01
#define	FaseToutWRCred		0x02
#define	FaseWRCred			0x03
#define	FaseToutRDCred		0x04
#define	FaseToutWRFlag		0x05
#define	FaseWRFlag			0x06
#define	FaseToutRDFlag		0x07

/*===============================================================*/

static uint8_t StdKeyA[] 			= {0xA0,0xA1,0xA2,0xA3,0xA4,0xA5};			// KeyA standard per leggere il MAD
static uint8_t KeyAReadMHD[] 		= {0x2B,0x67,0x56,0xCE,0x59,0xA6};			// KeyA standard per leggere i settori MHD
static uint8_t KeyA_VergCostr[]	= {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};			// KeyA standard per scrivere le carte vergini costruttore

#if SetVergineRBT
static uint8_t KeyBWriteMAD[] 	= {0xB2,0x76,0x65,0xEC,0x95,0x6A};			// KeyB per scrivere il MAD
static uint8_t KeyBWriteCardHolder[] 	= {0x6D,0x69,0x63,0x72,0x6F,0x68};		// KeyB per scrivere il Settore 1 Card Holder
static uint8_t KeyBWriteSicur[] 	= {0x3A,0xF7,0x51,0x8C,0x40,0xDB};			// KeyB per accedere al settore Sicurezza (usata solo nella "DaUtenteACostruttore")
#endif

#define	CardClearMem_pin1	0x6A95
#define	CardClearMem_pin2	0xDF4C
#define	CardClearAud_pin1	0x3FCD
#define	CardClearAud_pin2	0xA9B8


#define ServCard_AzzMemoria		0
#define ServCard_Audit			1

typedef struct {
	uint16_t	Gestore;
	uint16_t	Locazione;
	uint32_t	Utente;
	uint8_t	TipoCarta;
	uint16_t	Livelli;					// Livelli di sconto o gruppo prezzi
	uint8_t	CodCartaBytesVuoti[3];
	uint16_t	Chks;
} INFOCODE;

extern INFOCODE RBT_CodiciCarta;



typedef struct {
	uint16_t	Cred;
	uint8_t	BLK;						// Blocco carta da Black List
    uint8_t	DofWk_FreeCr;				// Giorno della settimana FreeCred
	uint8_t	Giorno_FreeCr;				// Giorno FreeCred
	uint8_t	Mese_FreeCr;				// Mese FreeCred
	uint8_t	Anno_FreeCr;				// Anno FreeCred
	uint16_t	FreeCr;						// Valore FreeCred
    uint8_t	DofWk_FreeVend;				// Giorno della settimana FreeVend
	uint8_t	Giorno_FreeVend;			// Giorno FreeVend
	uint8_t	Mese_FreeVend;				// Mese FreeVend
	uint8_t	Anno_FreeVend;				// Anno FreeVend
	uint8_t	FreeVend;					// Numero FreeVend
	uint16_t	Chks;
} CREDITSECTOR;

extern CREDITSECTOR RBTCredSect;


//*********************************************************************
//****  Struttura per la gestione dei Settori Sel_Gest           ******
//*********************************************************************

typedef struct {
	uint32_t	Selez[8];									// 32 bit per ogni selezione (vedere doc formattazione carte per dettagli)
	uint8_t	SelGestFlag[2];								// Flag che indica quale blocco e' in uso
	uint8_t	SelGestSectors[2];							// Settori della carta dove risiedono i dati di Sel_Gest
} GESTSEL;

extern GESTSEL CardGestSelVars;

#define		DayRicarica_Mask	0x0000001f
#define		MinSelez_Mask		0x000007e0
#define		HourSelez_Mask		0x0000f800
#define		LevelSel_Mask		0x00070000
#define		CntBattute_Mask		0x00f80000
#define		TotSelezCarta_Mask	0xff000000

#define		MinSelez_Shift		5
#define		HourSelez_Shift		11
#define		LevelSel_Shift		16
#define		CntBattute_Shift	19
#define		TotSelezCarta_Shift	24

#define		MenoUnoCntBattute		0x00080000
#define		MenoUnoBattuteTotali	0x01000000


//*********************************************************************


#define		ValoreBlocco		RBTCredSect.Cred
#define		DofWk_CaricamBlk	RBTCredSect.DofWk_FreeVend
#define		Day_CaricamBlk		RBTCredSect.Giorno_FreeVend
#define		Mese_CaricamBlk		RBTCredSect.Mese_FreeVend
#define		Anno_CaricamBlk		RBTCredSect.Anno_FreeVend
#define		NumeroBlocchi		RBTCredSect.FreeVend



#endif		// End __RBT_Mifare__H