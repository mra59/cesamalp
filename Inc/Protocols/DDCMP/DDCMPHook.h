/****************************************************************************************
File:
    DDCMPHook.h

Description:
    Include file con definizioni e costanti per il file DDCMPHook.c

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __DDCMPHook__H
#define __DDCMPHook__H

void  UpdateTimeDate(uint8_t* ClockDaWAY);
void  DDCMPHookBaudRateChange(uint8_t BdRate);
void  DDCMPHookCommClose(void);

/*MR19 Con STM32 non e' necessario Tx e Rx ON-OFF, anzi se disattivo-riattivo non funziona 
void  DDCMPHookTxBegin(void);
void  DDCMPHookRxBegin(void);
void  DDCMPHookRxEnd(void);
*/

void DDCMPHookTxEnd(void);
bool DDCMPHookIrDAComm(void);
void  DDCMPHookWaitStart(void);
uint8_t DDCMPHookBaudRateMaxGet(void);
void DEXUCSHookCommOpen(void);
void DEXUCSHookCommClose(void);
void DEXUCSHookTxBegin(void);
void DEXUCSHookTxEnd(void);
void DEXUCSHookRxBegin(void);
void DEXUCSHookRxEnd(void);
void DEXUCSHookCharTx(char ch);
bool DDCMPDTSHookSlaveRelease(void);
void  DDCMPHookLineRelease(void);
bool  DDCMPHookLineRequest(void);
void  DDCMPHookSendStart(void);
void  DDCMPHookCheckFineTxAudit(void);
void  DDCMPHookRxFinish(void);
void DDCMPDTSHookConnEstablishedSlaveInd(void);
void DDCMPDTS2HookConnEstablishedSlaveInd(void);
void DDCMPDTSHookConnFinishedInd(void);
void DDCMPDTS2HookConnFinishedInd(void);
void DDCMPDTSHookConnFailedInd(void);
void DDCMPDTS2HookConnFailedInd(void);
void DDCMPDTSHookDataRxInd(uint8_t dataBlockNum, uint8_t* applData, uint16_t applDataLen, bool dataEnd);
void DDCMPDTS2HookDataRxInd(uint8_t dataBlockNum, uint8_t* applData, uint16_t applDataLen, bool dataEnd);
void DDCMPDTSHookDataTxInd(uint8_t dataBlockNum, char* applDataBuff, uint16_t dataBuffSize);
void DDCMPDTS2HookDataTxInd(uint8_t dataBlockNum, uint8_t* applDataBuff, uint16_t dataBuffSize);
bool DDCMPDTSHookDeleteDataReqInd(uint8_t listNumber, uint8_t recordNumber);
bool DDCMPDTS2HookDeleteDataReqInd(uint8_t listNumber, uint8_t recordNumber);
bool DDCMPDTSHookReadDataReqInd (uint8_t listNumber, uint16_t byteOffset, uint16_t length, uint16_t* actualLen);
bool DDCMPDTS2HookReadDataReqInd(uint8_t listNumber, uint16_t byteOffset, uint16_t length, uint16_t* actualLen);
bool DDCMPDTSHookWriteDataReqInd(uint8_t listNumber, uint16_t length);
bool DDCMPDTS2HookWriteDataReqInd(uint8_t listNumber, uint16_t length);
uint16_t 	DDCMPDTSHookPwdGet(void);
uint16_t 	DDCMPDTS2HookPwdGet(void);
bool 		DDCMPMngrHookConnEstablishedInd(void);
void  		DDCMPMngrHookEnd();
void 		Set_Audit_Buf(char *bufpnt, uint16_t size);
uint16_t 	Get_dataBuffTransmitted(void);
uint16_t	Get_dataBuffFree(void);
uint8_t 	Get_localBufferSize(void);
bool 		Extended_Audit_LoopBody(void);


#endif	//  __DDCMPHook__H
