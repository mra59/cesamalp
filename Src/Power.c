/****************************************************************************************
File:
    Power.c

Description:
    Funzioni di Backup e Restore
    

History:
    Date       Aut  Note
    Apr 2019 	MR   

*****************************************************************************************/

#include <stdio.h>
#include <string.h>

#include "ORION.H" 
#include "Power.h"
#include "OrionCredit.h"
#include "Vendita.h"
#include "AuditRevalue.h"
#include "AuditOverpay.h"
#include "Totalizzazioni.h"
#include "Funzioni.h"

#include "IICEEP.h"
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"


#include "Dummy.h"

#if DBG_PWD_TIME		
	#include "main.h"
#endif

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	void	Attesa_WDT_Reset(void);
extern	void    SetCRC16Buff(uint8_t *destbuff, uint8_t destbufflen, uint16_t Seme);
extern	void	LeggiOrologio(void);
extern	void	HAL_Delay(uint32_t Delay);



// ========================================================================
// ============    RAM con backup in IIC EEPROM     =======================
// ========================================================================

uint8_t  	RechargeFlags;					// Flags varie audit ricarica cash
uint16_t  	CashRecharged;					// Cash caricato sulle carte;
uint16_t  	CashDaSelettore;              	// Valore cash da Selettore
uint16_t  	CashDaRendere;             		// Valore cash in restituzione dalla RR

uint8_t  	Ex_Ch_InCashVend;          		// Flag Ex-Ch nella vendita con cash
uint8_t  	Ex_Ch_Status;	      			// Stato attuale di Exact-Change
uint16_t  	CashCaricamentoBlocchi;         // Valore cash da chiave di caricamento a blocchi 
uint16_t  	CashDaBankReader;             	// XX Valore cash da Bank Reader 
uint32_t  	UID_CaricamBlock;               // UID carta Caricamento a Blocchi dalla quale e' stato tolto il blocco

uint16_t  	VendAmountKR;               	// Valore vendita effettuata con Key Reader 
uint16_t  	VendAmountBR;               	// XX Valore vendita effettuata con Cash da Bank Reader 
uint16_t  	VendAmountSEL;                	// Valore vendita effettuata con Cash da Selettore 
uint16_t  	VendAmountRNST;               	// XX Valore vendita effettuata con Reinstate 

uint16_t  	ValoreReinstate;              	// Free
uint16_t  	FreeVendReinstate;              // Free

uint8_t  	NumeroSelezione;              	// Numero Selezione richiesta da VMC 
uint16_t  	ValoreSelezione;              	// Valore selezione richiesta da VMC 
uint8_t  	FreeVendLevel;                  // Livello Free Vend
uint8_t  	NumLista;               		// Numero Lista Sconti applicata

uint8_t  	TipoOverpay;                	// Tipo Overpay in corso 
uint8_t  	StatoOverpay;               	// Stato execuzione Overpay in corso 

uint8_t  	TipoRevalue;                	// Tipo Revalue in corso 
uint8_t  	StatoRevalue;               	// Stato execuzione Revalue in corso 

uint8_t  	TipoVendita;                	// Tipo Vendita in corso 
uint8_t  	StatusVendita;                	// Stato gestione vendita in corso 
uint8_t  	StatusGestioneDatiVendita1;     // Stato gestione Dati Vendita 
uint8_t  	StatusGestioneDatiVendita2;     // Stato gestione Audit Vendita 

uint8_t  	StatusFreeCredit;             	// Stato gestione Free Credit 
uint16_t  	NewFC_Attribuito;           	// Valore FreeCredit sulla chiave al momento dell'attribuzione
uint8_t  	NewFV_Attribuite;           	// Numero FreeVend sulla chiave al momento dell'attribuzione
uint16_t  	FreeCreditAccreditato;          // Free Credit Sommato 
uint16_t  	FreeCreditAddebitato;           // Free Credit Sostituito 

uint16_t  	ValoreBloccoCaricamento;        // Valore unitario chiavi di caricamento 
uint8_t 	CntEventi_WD_Reset;				// Counter per registrare nell'Audit gli eventi di WDReset

uint8_t  	StatusGestioneDatiVendita3;		// Stato gestione Audit Vendita 

uint8_t  	CntEventi_PowerOutage;			// Counter per registrare nell'Audit gli eventi di Power Outage

uint32_t  	gTotCashCounters;               // total cash
uint32_t  	gCashLimitResidue;                  
uint32_t  	gCashLimitResidueNew;                 

uint16_t  	CreditUsedKR;               	// Valore Credit usato per vendita 
uint16_t  	FreeCreditUsedKR;             	// Valore Free Credit usato per vendita 
uint16_t  	NewCreditKR;                	// Valore Credito aggiornato chiave 
uint16_t  	CodiceUtenteKR;               	// Codice Utente 
uint16_t  	NewFreeCreditKR;               	// Valore Free Credit aggiornato chiave 
uint16_t  	NewFreeVendKR;               	// Numero delle Free Vend aggiornato chiave 

uint32_t  	OffSavedData;       			// Saved shutdown data 
uint16_t  	OffSavedTime;       			// Saved shutdown time 

uint8_t	  	Civetta1;		       			// Valore di controllo 1 correttezza dati in BackupRAM 
uint8_t	  	Civetta2;		       			// Valore di controllo 2 correttezza dati in BackupRAM 
uint16_t  	Civetta3;		       			// Valore di controllo 3 correttezza dati in BackupRAM 
uint16_t  	Civetta4;		       			// Valore di controllo 4 correttezza dati in BackupRAM 

uint16_t 	CRC_CashDaSelettore;
uint16_t 	CRC_CashDaBankReader;

uint8_t		BillRouting;					// dati backup per audit cash
uint8_t 	BillIdx;
uint16_t 	BillVal;	
uint8_t		FaseAuditBill;

uint8_t		CoinRouting;
uint8_t 	CoinIdx;
uint16_t 	CoinVal;	
uint8_t		FaseAuditCoin;

uint8_t		ChangeRouting;
uint16_t	ChangeVal;
uint8_t 	FaseAuditChange;
uint8_t		FaseAuditFillManual;
uint32_t	AmountAuditFillCoin;
uint8_t		IndexManFill;
uint8_t		IndexManPayout;
uint8_t		AuditFillManual[16];				// 16 Bytes
uint8_t		AuditPaydManual[16];				// 16 bytes
uint16_t  	CashResoParziale;             	// Somma del valore parziale reso durante il payout

// ***********************************************************************************************************************


/*--------------------------------------------------------------------------------------*\
Method: Restore
	Chiamata all'accesione per ripristinare eventuali transazioni interrotte al PWDown.
	Ripristina il contenuto della BackupRAM dalla EEPROM e controlla se i dati sono
	corretti: in caso negativo li azzera.

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/

void  Restore(void) {
	
	//MR19 uint16_t	TempCash;
	uint32_t	TotalCash;

	ReloadAndCkeck_BackupRAM();													// Ricarico BackupRAM
	ValidateCashDaSelettore();													// Controllo correttezza eventuale credito cash
	ValidateCashDaBankReader();
	
	TotalCash = CashDaSelettore + CashDaBankReader;								// Sommo 2 valori a 16 bit in uno a 32 bit
	if (TotalCash > MAX_SYSTEM_CREDIT) {										// Overflow sulla somma dei crediti cash
		AzzeraCash(true);
	}

	//	Ripresa totalizzazioni audit Overpay
	if (TipoOverpay != NO_OVERPAY) {
		GestioneAuditOverpay();
	}

	//	Ripresa totalizzazioni audit Revalue 
	if (TipoRevalue != NO_REVALUE) {
		RechargeFlags &= ~(WaitEndCashWrite);									// Eseguo comunque aggiornamento audit caricamento cash
		GestioneRevalue();
	}

	//	Ripresa aggiornamento dati vendita 
	if (StatusGestioneDatiVendita1 > 0) {
		AggiornamentoDatiVendita();
	} else {
		ResetDatiVendita();
	}
  
	//	Ripresa Payout
	if (CashDaRendere > 0) {
		Overpay(OVERPAY_CASH_PAYOUT);											// Cash Payout-->Overpay
	}

	//	Cash Caricamento a Blocchi --> Refund EEPROM
	if (CashCaricamentoBlocchi > NULL_VALUE) {
		Snum_media_in[0] = UID_CaricamBlock>>24;
		Snum_media_in[1] = UID_CaricamBlock>>16;
		Snum_media_in[2] = UID_CaricamBlock>>8;
		Snum_media_in[3] = UID_CaricamBlock;
		//MR19 TempCash = CashCaricamentoBlocchi;										// Uso la variabile temporanea per azzerare il cash da caricamento a blocchi prima di salvarlo 
		PSHProcessingData();													// in EEPROM ed evitare che spegnendo la macchina lo registrino piu' volte. 
		CashCaricamentoBlocchi = 0;
		ValoreBloccoCaricamento = 0;
		//MR19 SaveRefund(&RefundCashCB, TipoRefundCaricamBlk, TempCash, true);
	}

}

/****************************************************************************************
 * *************  A T T E N Z I O N E    ************************************************
 * ******* Se viene modificato il tipo di una variabile in backupRAM ********************
 * ******* ricordarsi di modificare la sua memorizzazione e il suo restore **************
 * **************************************************************************************
 * **************************************************************************************
 */

/*--------------------------------------------------------------------------------------*\
Method: SaveBackupData
	Copia in EEPROM la Backup RAM

Parameters:
	IN	- N.2 pagine di Backup RAM
\*--------------------------------------------------------------------------------------*/
void  SaveBackupData (void) {
	
	uint8_t		BackupBuff[BackupDataLenght];
	uint8_t		cnt, TotBytes;
	uint16_t	Seed;
	
	LeggiOrologio();																			// Ipotesi di salvataggio in eeprom di un solo uint32_t con il counter secondi
	SetDataTime(&OffSavedData, &OffSavedTime);													// Leggo data e ora Power OFF e li memorizzo in EEPROM

	// Il primo byte del buffer conterra' il totale dei byte da salvare
	// Uso questa notazione di puntatore per essere simile a quella di 
	// restore e controllare piu' agevolmente eventuali errori nel
	// riportare i nomi delle variabili.
	
	// ********   uint8_t  ******************	                                          ****  EEPROM  ADDRESS  *****
	cnt = 1;															// TotBytes			0x00
	BackupBuff[cnt+0] = Civetta1;										// n. 1				0x01
	BackupBuff[cnt+1] = RechargeFlags;									// n. 2				0x02
	BackupBuff[cnt+2] = NumeroSelezione;								// n. 3				0x03
	BackupBuff[cnt+3] = FreeVendLevel;									// n. 4				0x04
	BackupBuff[cnt+4] = TipoOverpay;									// n. 5				0x05
	BackupBuff[cnt+5] = StatoOverpay;									// n. 6				0x06
	BackupBuff[cnt+6] = TipoRevalue;									// n. 7				0x07
	BackupBuff[cnt+7] = StatoRevalue;									// n. 8				0x08
	BackupBuff[cnt+8] = TipoVendita;									// n. 9				0x09
	BackupBuff[cnt+9] = StatusVendita;									// n. 10			0x0A
	BackupBuff[cnt+10] = StatusGestioneDatiVendita1;					// n. 11			0x0B
	BackupBuff[cnt+11] = StatusGestioneDatiVendita2;					// n. 12			0x0C
	BackupBuff[cnt+12] = StatusFreeCredit;								// n. 13			0x0D
	BackupBuff[cnt+13] = CntEventi_WD_Reset;							// n. 14			0x0E
	BackupBuff[cnt+14] = CRC_CashDaSelettore;							// n. 15			0x0F
	BackupBuff[cnt+15] = CRC_CashDaSelettore>>8;						// n. 16			0x10
	BackupBuff[cnt+16] = StatusGestioneDatiVendita3;					// n. 17			0x11
	BackupBuff[cnt+17] = CntEventi_PowerOutage;							// n. 18			0x12

	BackupBuff[cnt+18] = BillRouting;									// n. 19			0x13
	BackupBuff[cnt+19] = BillIdx;										// n. 20			0x14
	BackupBuff[cnt+20] = FaseAuditBill;									// n. 21			0x15
		
	BackupBuff[cnt+21] = Civetta2;										// n. 22			0x16
	BackupBuff[cnt+22] = CoinRouting;									// n. 23			0x17
	BackupBuff[cnt+23] = CoinIdx;										// n. 24			0x18
	BackupBuff[cnt+24] = FaseAuditCoin;									// n. 25			0x19
	
	BackupBuff[cnt+25] = FaseAuditChange;								// n. 26			0x1A		
	BackupBuff[cnt+26] = ChangeRouting;									// n. 27			0x1B	
	
	BackupBuff[cnt+27] = IndexManFill;									// n. 28			0x1C
	BackupBuff[cnt+28] = IndexManPayout;								// n. 29			0x1D	

	BackupBuff[cnt+29] = FaseAuditFillManual;							// n. 30			0x1E
	
	BackupBuff[cnt+30] = NumLista;										// n. 31			0x1F
	
	for (TotBytes=0;TotBytes<16;TotBytes++){
		BackupBuff[cnt+31+TotBytes] = AuditFillManual[TotBytes];		// 16 bytes tot 47 bytes (16+31)	0x20 - 0x2F
	}
	
	for (TotBytes=0;TotBytes<16;TotBytes++){
		BackupBuff[cnt+47+TotBytes] = AuditPaydManual[TotBytes];		// 16 bytes tot 63 bytes (16+47)	0x30 - 0X3F
	}
	

	TotBytes = 63;				// Numero di bytes uint8_t (+1 che e' il contatore stesso)
	
	// =======================================================
	// ********   uint16_t  *****************/
	
	cnt += TotBytes;
	BackupBuff[cnt+0] = CashRecharged;									// n. 1				0x40
	BackupBuff[cnt+1] = CashRecharged>>8;								// n. 1
	BackupBuff[cnt+2] = CashDaSelettore;								// n. 2				0x42
	BackupBuff[cnt+3] = CashDaSelettore>>8;								// n. 2
	BackupBuff[cnt+4] = Ex_Ch_InCashVend;								// n. 3				0x44	29.06.15: trasformati in 2 bytes singoli
	BackupBuff[cnt+5] = Ex_Ch_Status;									// n. 3
	BackupBuff[cnt+6] = CashCaricamentoBlocchi;							// n. 4				0x46
	BackupBuff[cnt+7] = CashCaricamentoBlocchi>>8;						// n. 4
	BackupBuff[cnt+8] = CashDaBankReader;								// n. 5				0x48
	BackupBuff[cnt+9] = CashDaBankReader>>8;							// n. 5
	BackupBuff[cnt+10] = VendAmountKR;									// n. 6				0x4A
	BackupBuff[cnt+11] = VendAmountKR>>8;								// n. 6
	BackupBuff[cnt+12] = VendAmountBR;									// n. 7				0x4C
	BackupBuff[cnt+13] = VendAmountBR>>8;								// n. 7
	BackupBuff[cnt+14] = VendAmountSEL;									// n. 8				0x4E
	BackupBuff[cnt+15] = VendAmountSEL>>8;								// n. 8
	BackupBuff[cnt+16] = VendAmountRNST;								// n. 9				0x50
	BackupBuff[cnt+17] = VendAmountRNST>>8;								// n. 9
	BackupBuff[cnt+18] = ValoreReinstate;								// n. 10			0x52
	BackupBuff[cnt+19] = ValoreReinstate>>8;							// n. 10
	BackupBuff[cnt+20] = FreeVendReinstate;								// n. 11			0x54
	BackupBuff[cnt+21] = FreeVendReinstate>>8;							// n. 11
	
	BackupBuff[cnt+22] = Civetta3;										// n. 12			0x56
	BackupBuff[cnt+23] = Civetta3>>8;									// n. 12
	
	BackupBuff[cnt+24] = ValoreSelezione;								// n. 13			0x58
	BackupBuff[cnt+25] = ValoreSelezione>>8;							// n. 13
	BackupBuff[cnt+26] = FreeCreditAccreditato;							// n. 14			0x5A
	BackupBuff[cnt+27] = FreeCreditAccreditato>>8;						// n. 14
	BackupBuff[cnt+28] = FreeCreditAddebitato;							// n. 15			0x5C
	BackupBuff[cnt+29] = FreeCreditAddebitato>>8;						// n. 15
	BackupBuff[cnt+30] = ValoreBloccoCaricamento;						// n. 16			0x5E
	BackupBuff[cnt+31] = ValoreBloccoCaricamento>>8;					// n. 16
	BackupBuff[cnt+32] = CreditUsedKR;									// n. 17			0x60
	BackupBuff[cnt+33] = CreditUsedKR>>8;								// n. 17
	BackupBuff[cnt+34] = FreeCreditUsedKR;								// n. 18			0x62
	BackupBuff[cnt+35] = FreeCreditUsedKR>>8;							// n. 18
	BackupBuff[cnt+36] = NewCreditKR;									// n. 19			0x64
	BackupBuff[cnt+37] = NewCreditKR>>8;								// n. 19
	BackupBuff[cnt+38] = CodiceUtenteKR;								// n. 20			0x66
	BackupBuff[cnt+39] = CodiceUtenteKR>>8;								// n. 20
	BackupBuff[cnt+40] = OffSavedTime;									// n. 21			0x68
	BackupBuff[cnt+41] = OffSavedTime>>8;								// n. 21

	BackupBuff[cnt+42] = BillVal;										// n. 22			0x6A
	BackupBuff[cnt+43] = BillVal>>8;									// n. 22
	BackupBuff[cnt+44] = CoinVal;										// n. 23			0x6C
	BackupBuff[cnt+45] = CoinVal>>8;									// n. 23
	BackupBuff[cnt+46] = ChangeVal;										// n. 24			0x6E
	BackupBuff[cnt+47] = ChangeVal>>8;									// n. 24
	BackupBuff[cnt+48] = CashDaRendere;									// n. 25			0x70
	BackupBuff[cnt+49] = CashDaRendere>>8;								// n. 25
	BackupBuff[cnt+50] = CashResoParziale;								// n. 26			0x72
	BackupBuff[cnt+51] = CashResoParziale>>8;							// n. 26
	BackupBuff[cnt+52] = Civetta4;										// n. 27			0x74
	BackupBuff[cnt+53] = Civetta4>>8;									// n. 27
	BackupBuff[cnt+54] = CRC_CashDaBankReader;							// n. 28			0x76
	BackupBuff[cnt+55] = CRC_CashDaBankReader>>8;						// n. 28

	
	TotBytes = 56;				// Numero di bytes uint16_t (num uint16_t*2)

	// =======================================================
	// ********   uint32_t  *****************/

	cnt += TotBytes;
	BackupBuff[cnt+0] = OffSavedData;									// n. 1			0x78
	BackupBuff[cnt+1] = OffSavedData>>8;								// n. 1
	BackupBuff[cnt+2] = OffSavedData>>16;								// n. 1
	BackupBuff[cnt+3] = OffSavedData>>24;								// n. 1
	
	BackupBuff[cnt+4] = gTotCashCounters;								// n. 2			0x7C
	BackupBuff[cnt+5] = gTotCashCounters>>8;							// n. 2
	BackupBuff[cnt+6] = gTotCashCounters>>16;							// n. 2
	BackupBuff[cnt+7] = gTotCashCounters>>24;							// n. 2
	
	BackupBuff[cnt+8] = gCashLimitResidue;								// n. 3			0x80
	BackupBuff[cnt+9] = gCashLimitResidue>>8;							// n. 3
	BackupBuff[cnt+10] = gCashLimitResidue>>16;							// n. 3
	BackupBuff[cnt+11] = gCashLimitResidue>>24;							// n. 3
	
	BackupBuff[cnt+12] = gCashLimitResidueNew;							// n. 4			0x84
	BackupBuff[cnt+13] = gCashLimitResidueNew>>8;						// n. 4
	BackupBuff[cnt+14] = gCashLimitResidueNew>>16;						// n. 4
	BackupBuff[cnt+15] = gCashLimitResidueNew>>24;						// n. 4
	
	BackupBuff[cnt+16] = AmountAuditFillCoin;							// n. 5			0x88
	BackupBuff[cnt+17] = AmountAuditFillCoin>>8;						// n. 5
	BackupBuff[cnt+18] = AmountAuditFillCoin>>16;						// n. 5
	BackupBuff[cnt+19] = AmountAuditFillCoin>>24;						// n. 5

	BackupBuff[cnt+20] = UID_CaricamBlock;								// n. 6			0x8C
	BackupBuff[cnt+21] = UID_CaricamBlock>>8;							// n. 6
	BackupBuff[cnt+22] = UID_CaricamBlock>>16;							// n. 6
	BackupBuff[cnt+23] = UID_CaricamBlock>>24;							// n. 6

	TotBytes = 24;				// Numero di bytes uint32_t (num uint32_t*4)

/* =======================================================*/
//	Nuovi valori aggiunti 
	
	cnt += TotBytes;
	BackupBuff[cnt+0] = NewFreeCreditKR;								// n. 1			0x90
	BackupBuff[cnt+1] = NewFreeCreditKR>>8;								// n. 1
	BackupBuff[cnt+2] = NewFreeVendKR;									// n. 2			0x92
	BackupBuff[cnt+3] = NewFreeVendKR>>8;								// n. 2
	BackupBuff[cnt+4] = NewFC_Attribuito;								// n. 3			0x94
	BackupBuff[cnt+5] = NewFC_Attribuito>>8;							// n. 3
	BackupBuff[cnt+6] = NewFV_Attribuite;								// n. 4			0x96
	TotBytes = 7;
	
/* =======================================================*/
	cnt += TotBytes;
	BackupBuff[cnt+0] = gVmcState_0;									// n. 1			0x97
	BackupBuff[cnt+1] = gVmcState_1;									// n. 2			0x98
	BackupBuff[cnt+2] = gVmcState_2;									// n. 3			0x99
	BackupBuff[cnt+3] = DecontoState;									// n. 4			0x9a
	TotBytes = 4;

/* =======================================================*/
	
	cnt += TotBytes;
	BackupBuff[0] = cnt;													// In buffer numero totale caratteri da memorizzare
	Seed = SemeBackupRAM;
	SetCRC16Buff(&BackupBuff[0], cnt, Seed);								// Gli ultimi 2 bytes dei dati salvati sono il CRC16
	BackupBuff[cnt] = gVars.CRC16;											// CRC16 LSB
	cnt++;
	BackupBuff[cnt] = (gVars.CRC16)>>8;										// CRC16 MSB
	cnt++;
	WriteEEPI2C(BeginBackupData, (uint8_t *)(&BackupBuff), cnt);

#if DBG_PWD_TIME	
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_SET);									// PE13 HIGH
#endif
	HAL_Delay(2000);														// Attendo 2 secondi, poi reset software se l'alimentazione non � mancata completamente
	WatchDogReset();														// Reset uP tramite WatchDog
	while(true){};
}

/*--------------------------------------------------------------------------------------*\
Method: ReloadAndCkeck_BackupRAM
	Rilegge i dati dall'area BackupRAM della EEPROM e controlla il checksum: se fails
	azzera la BackupRAM in EEPROM e la BackupRAM in RAM per non avere dati corrotti in
	RAM. 
	
	**** ATTENZIONE ALL'ORDINE CON IL QUALE SI RECUPERANO I DATI: DEVE ESSERE ****
	**** LO STESSO CON CUI SONO STATI MEMORIZZATI                             ****
	
Parameters:
	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/

void  ReloadAndCkeck_BackupRAM (void) {

	uint8_t  	cnt, TotBytes;
	uint16_t	CRC_Buff;
	uint16_t	Seed;
	
	PowSup.PUP_Restore = true;														// Inizio restore
	// Leggo tutta la EEPROM riservata alla BackupRAM
	ReadEEPI2C(BeginBackupData, (uint8_t *)(&gVars.ddcmpbuff[0]), BackupDataLenght);
	TotBytes = gVars.ddcmpbuff[0];													// totale chr della BAckupRAM salvata
	CRC_Buff = gVars.ddcmpbuff[TotBytes];											// CRC16 LSB
	CRC_Buff |= (gVars.ddcmpbuff[TotBytes+1]) <<8;									// CRC16 MSB
	

	Seed = SemeBackupRAM;
	SetCRC16Buff(&gVars.ddcmpbuff[0], TotBytes, Seed);								// Calcolo CRC16 di tutto il buffer letto
	if ((gVars.CRC16 != CRC_Buff) || IsFailCivetta()){
		// Dati EEPROM corrotti: azzera EEPROM e RAM
		EraseBackupData();															// Azzero BackupRAM in EEPROM
		memset(&gVars.ddcmpbuff[0], 0, BackupDataLenght);							// Azzero buffer dati BackupRAM
		SetCivetta();
		ClearBackupRAM_Events();													// Inserisco Evento Clear BackupRAM
	}
	
	
	/* =======================================================*/
	/* ********   RESTORE  uint8_t  *****************/
	cnt = 1;
	Civetta1 = gVars.ddcmpbuff[cnt+0];										// n. 1
	RechargeFlags = gVars.ddcmpbuff[cnt+1];									// n. 2
	NumeroSelezione = gVars.ddcmpbuff[cnt+2];								// n. 3
	FreeVendLevel = gVars.ddcmpbuff[cnt+3];									// n. 4
	TipoOverpay = gVars.ddcmpbuff[cnt+4];									// n. 5
	StatoOverpay = gVars.ddcmpbuff[cnt+5];									// n. 6
	TipoRevalue = gVars.ddcmpbuff[cnt+6];									// n. 7
	StatoRevalue = gVars.ddcmpbuff[cnt+7];									// n. 8
	TipoVendita = gVars.ddcmpbuff[cnt+8];									// n. 9
	StatusVendita = gVars.ddcmpbuff[cnt+9];									// n. 10
	StatusGestioneDatiVendita1 = gVars.ddcmpbuff[cnt+10];					// n. 11
	StatusGestioneDatiVendita2 = gVars.ddcmpbuff[cnt+11];					// n. 12
	StatusFreeCredit = gVars.ddcmpbuff[cnt+12];								// n. 13
	CntEventi_WD_Reset = gVars.ddcmpbuff[cnt+13];							// n. 14
	CRC_CashDaSelettore = gVars.ddcmpbuff[cnt+14];							// n. 15
	CRC_CashDaSelettore |= gVars.ddcmpbuff[cnt+15]<<8;						// n. 16
	StatusGestioneDatiVendita3 = gVars.ddcmpbuff[cnt+16];					// n. 17
	CntEventi_PowerOutage = gVars.ddcmpbuff[cnt+17];						// n. 18

	BillRouting = gVars.ddcmpbuff[cnt+18];									// n. 19
	BillIdx = gVars.ddcmpbuff[cnt+19];										// n. 20
	FaseAuditBill = gVars.ddcmpbuff[cnt+20];								// n. 21
		
	Civetta2 = gVars.ddcmpbuff[cnt+21];										// n. 22
	CoinRouting = gVars.ddcmpbuff[cnt+22];									// n. 23
	CoinIdx = gVars.ddcmpbuff[cnt+23];										// n. 24
	FaseAuditCoin = gVars.ddcmpbuff[cnt+24];								// n. 25
	
	ChangeRouting = gVars.ddcmpbuff[cnt+25];								// n. 26
	FaseAuditChange = gVars.ddcmpbuff[cnt+26];								// n. 27
	
	IndexManFill = gVars.ddcmpbuff[cnt+27];									// n. 28
	IndexManPayout = gVars.ddcmpbuff[cnt+28];								// n. 29

	FaseAuditFillManual = gVars.ddcmpbuff[cnt+29];							// n. 30

	NumLista = gVars.ddcmpbuff[cnt+30];										// n. 31

	for (TotBytes=0; TotBytes<16; TotBytes++){
		AuditFillManual[TotBytes] = gVars.ddcmpbuff[cnt+31+TotBytes];		// 16 bytes tot 47 bytes (16+31)
	}
	
	for (TotBytes=0; TotBytes<16; TotBytes++){
		AuditPaydManual[TotBytes] = gVars.ddcmpbuff[cnt+47+TotBytes];		// 16 bytes tot 63 bytes (16+47)
	}
	
	TotBytes = 63;

	/* =======================================================*/
	/* ********   RESTORE  uint16_t  *****************/

	cnt += TotBytes;
	CashRecharged = gVars.ddcmpbuff[cnt+0];									// n. 1
	CashRecharged |= gVars.ddcmpbuff[cnt+1]<<8;								// n. 1
	CashDaSelettore = gVars.ddcmpbuff[cnt+2];								// n. 2
	CashDaSelettore |= gVars.ddcmpbuff[cnt+3]<<8;							// n. 2
	Ex_Ch_InCashVend = gVars.ddcmpbuff[cnt+4];								// n. 3		29.06.15: trasformati in 2 bytes singoli
	Ex_Ch_Status = gVars.ddcmpbuff[cnt+5];									// n. 3
	CashCaricamentoBlocchi = gVars.ddcmpbuff[cnt+6];						// n. 4
	CashCaricamentoBlocchi |= gVars.ddcmpbuff[cnt+7]<<8;					// n. 4
	CashDaBankReader = gVars.ddcmpbuff[cnt+8];								// n. 5
	CashDaBankReader |= gVars.ddcmpbuff[cnt+9]<<8;							// n. 5
	VendAmountKR = gVars.ddcmpbuff[cnt+10];									// n. 6
	VendAmountKR |= gVars.ddcmpbuff[cnt+11]<<8;								// n. 6
	VendAmountBR = gVars.ddcmpbuff[cnt+12];									// n. 7
	VendAmountBR |= gVars.ddcmpbuff[cnt+13]<<8;								// n. 7
	VendAmountSEL = gVars.ddcmpbuff[cnt+14];								// n. 8
	VendAmountSEL |= gVars.ddcmpbuff[cnt+15]<<8;							// n. 8
	VendAmountRNST = gVars.ddcmpbuff[cnt+16];								// n. 9
	VendAmountRNST |= gVars.ddcmpbuff[cnt+17]<<8;							// n. 9
	ValoreReinstate = gVars.ddcmpbuff[cnt+18];								// n. 10
	ValoreReinstate |= gVars.ddcmpbuff[cnt+19]<<8;							// n. 10
	FreeVendReinstate = gVars.ddcmpbuff[cnt+20];							// n. 11
	FreeVendReinstate |= gVars.ddcmpbuff[cnt+21]<<8;						// n. 11
	
	Civetta3 = gVars.ddcmpbuff[cnt+22];										// n. 12
	Civetta3 |= gVars.ddcmpbuff[cnt+23]<<8;									// n. 12
	
	ValoreSelezione = gVars.ddcmpbuff[cnt+24];								// n. 13
	ValoreSelezione |= gVars.ddcmpbuff[cnt+25]<<8;							// n. 13
	FreeCreditAccreditato = gVars.ddcmpbuff[cnt+26];						// n. 14
	FreeCreditAccreditato |= gVars.ddcmpbuff[cnt+27]<<8;					// n. 14
	FreeCreditAddebitato = gVars.ddcmpbuff[cnt+28];							// n. 15
	FreeCreditAddebitato |= gVars.ddcmpbuff[cnt+29]<<8;						// n. 15
	ValoreBloccoCaricamento = gVars.ddcmpbuff[cnt+30];						// n. 16
	ValoreBloccoCaricamento |= gVars.ddcmpbuff[cnt+31]<<8;					// n. 16
	CreditUsedKR = gVars.ddcmpbuff[cnt+32];									// n. 17
	CreditUsedKR |= gVars.ddcmpbuff[cnt+33]<<8;								// n. 17
	FreeCreditUsedKR = gVars.ddcmpbuff[cnt+34];								// n. 18
	FreeCreditUsedKR |= gVars.ddcmpbuff[cnt+35]<<8;							// n. 18
	NewCreditKR = gVars.ddcmpbuff[cnt+36];									// n. 19
	NewCreditKR |= gVars.ddcmpbuff[cnt+37]<<8;								// n. 19
	CodiceUtenteKR = gVars.ddcmpbuff[cnt+38];								// n. 20
	CodiceUtenteKR |= gVars.ddcmpbuff[cnt+39]<<8;							// n. 20
	OffSavedTime = gVars.ddcmpbuff[cnt+40];									// n. 21
	OffSavedTime |= gVars.ddcmpbuff[cnt+41]<<8;								// n. 21

	BillVal = gVars.ddcmpbuff[cnt+42];
	BillVal |= gVars.ddcmpbuff[cnt+43]<<8;									// n. 22
	CoinVal = gVars.ddcmpbuff[cnt+44];
	CoinVal |= gVars.ddcmpbuff[cnt+45]<<8;									// n. 23
	ChangeVal = gVars.ddcmpbuff[cnt+46];
	ChangeVal |= gVars.ddcmpbuff[cnt+47]<<8;								// n. 24
	CashDaRendere = gVars.ddcmpbuff[cnt+48];
	CashDaRendere |= gVars.ddcmpbuff[cnt+49]<<8;							// n. 25
	CashResoParziale = gVars.ddcmpbuff[cnt+50];
	CashResoParziale |= gVars.ddcmpbuff[cnt+51]<<8;							// n. 26

	Civetta4 = gVars.ddcmpbuff[cnt+52];										// n. 27
	Civetta4 |= gVars.ddcmpbuff[cnt+53]<<8;									// n. 27
	CRC_CashDaBankReader = gVars.ddcmpbuff[cnt+54];							// n. 28
	CRC_CashDaBankReader |= gVars.ddcmpbuff[cnt+55]<<8;						// n. 28

	TotBytes = 56;

	/* =======================================================*/
	/* ********   RESTORE  uint32_t  *****************/

	cnt += TotBytes;
	OffSavedData = gVars.ddcmpbuff[cnt+0];										// n. 1
	OffSavedData |= gVars.ddcmpbuff[cnt+1]<<8;									// n. 1
	OffSavedData |= gVars.ddcmpbuff[cnt+2]<<16;									// n. 1
	OffSavedData |= gVars.ddcmpbuff[cnt+3]<<24;									// n. 1

	gTotCashCounters = gVars.ddcmpbuff[cnt+4];									// n. 2
	gTotCashCounters |= gVars.ddcmpbuff[cnt+5]<<8;								// n. 2
	gTotCashCounters |= gVars.ddcmpbuff[cnt+6]<<16;								// n. 2
	gTotCashCounters |= gVars.ddcmpbuff[cnt+7]<<24;								// n. 2

	gCashLimitResidue = gVars.ddcmpbuff[cnt+8];									// n. 3
	gCashLimitResidue |= gVars.ddcmpbuff[cnt+9]<<8;								// n. 3
	gCashLimitResidue |= gVars.ddcmpbuff[cnt+10]<<16;							// n. 3
	gCashLimitResidue |= gVars.ddcmpbuff[cnt+11]<<24;							// n. 3

	gCashLimitResidueNew = gVars.ddcmpbuff[cnt+12];								// n. 4
	gCashLimitResidueNew |= gVars.ddcmpbuff[cnt+13]<<8;							// n. 4
	gCashLimitResidueNew |= gVars.ddcmpbuff[cnt+14]<<16;						// n. 4
	gCashLimitResidueNew |= gVars.ddcmpbuff[cnt+15]<<24;						// n. 4

	AmountAuditFillCoin = gVars.ddcmpbuff[cnt+16];								// n. 5
	AmountAuditFillCoin |= gVars.ddcmpbuff[cnt+17]<<8;							// n. 5
	AmountAuditFillCoin |= gVars.ddcmpbuff[cnt+18]<<16;							// n. 5
	AmountAuditFillCoin |= gVars.ddcmpbuff[cnt+19]<<24;							// n. 5
	
	UID_CaricamBlock 	= gVars.ddcmpbuff[cnt+20];								// n. 6
	UID_CaricamBlock 	|= gVars.ddcmpbuff[cnt+21]<<8;							// n. 6
	UID_CaricamBlock 	|= gVars.ddcmpbuff[cnt+22]<<16;							// n. 6
	UID_CaricamBlock 	|= gVars.ddcmpbuff[cnt+23]<<24;							// n. 6
	
	TotBytes = 24;

	/* =======================================================*/
	/* ********   RESTORE  ultimi valori aggiunti  ************/

	cnt += TotBytes;
	NewFreeCreditKR = gVars.ddcmpbuff[cnt+0];									// n. 1
	NewFreeCreditKR |= gVars.ddcmpbuff[cnt+1]<<8;								// n. 1
	NewFreeVendKR = gVars.ddcmpbuff[cnt+2];										// n. 2
	NewFreeVendKR |= gVars.ddcmpbuff[cnt+3]<<8;									// n. 2
	NewFC_Attribuito = gVars.ddcmpbuff[cnt+4];									// n. 3
	NewFC_Attribuito |= gVars.ddcmpbuff[cnt+5]<<8;								// n. 3
	NewFV_Attribuite = gVars.ddcmpbuff[cnt+6];									// n. 4

	TotBytes = 7;

/* =======================================================*/
	cnt += TotBytes;
	gVmcState_0	= gVars.ddcmpbuff[cnt+0];										// n. 1
	gVmcState_1 = gVars.ddcmpbuff[cnt+1];										// n. 2
	gVmcState_2 = gVars.ddcmpbuff[cnt+2];										// n. 3
	DecontoState = gVars.ddcmpbuff[cnt+3];										// n. 4
/* =======================================================*/
	
	PowSup.PUP_Restore = false;													// Restore terminato
}

/*--------------------------------------------------------------------------------------*\
Method: IsFailCivetta
	Ricarico i bytes Civetta dal buffer letto dalla EEPROM e ne controllo la
	correttezza.

Parameters:
	IN	- 
	OUT - True se bytes Civetta errati, false se tutto OK
\*--------------------------------------------------------------------------------------*/
static bool  IsFailCivetta(void)  {
	
	uint8_t	cnt, TotBytes;

	// Recupero i bytes Civetta per verificarne il corretto valore.
	// Utilizzo lo stesso meccanismo del cnt come pointer al ddcmpbuff per non incorrere
	// in errori dopo eventuali modifiche alla lista dei bytes backup
	cnt = 1;
	Civetta1 = gVars.ddcmpbuff[cnt+0];															// n. 1
	Civetta2 = gVars.ddcmpbuff[cnt+21];															// n. 22
	TotBytes = 63;																				// Totale bytes uint8_t
	cnt += TotBytes;
	Civetta3 = gVars.ddcmpbuff[cnt+22];															// n. 12
	Civetta3 |= gVars.ddcmpbuff[cnt+23]<<8;														// n. 12
	Civetta4 = gVars.ddcmpbuff[cnt+52];															// n. 27
	Civetta4 |= gVars.ddcmpbuff[cnt+53]<<8;														// n. 27
	if (Civetta1 != 0x27 || Civetta2 != 0x35 || Civetta3 != 0x5961 || Civetta4 != 0x9496) {
		return true;																			// FAIL
	} else {
		return false;																			// OK
	}
}

/*--------------------------------------------------------------------------------------*\
Method: SetCivetta
	Inizializza i bytes del buffer ddcmp che verranno poi copiati nei bytes Civetta.

Parameters:
	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
static void  SetCivetta(void)  {
	
	uint8_t	cnt, TotBytes;

	cnt = 1;
	gVars.ddcmpbuff[cnt+0] = 0x27;																// n. 1					Civetta1
	gVars.ddcmpbuff[cnt+21] = 0x35	;															// n. 22				Civetta2
	TotBytes = 63;																				// Totale bytes uint8_t
	cnt += TotBytes;
	gVars.ddcmpbuff[cnt+22] = 0x61;																// n. 12				Civetta3 Low
	gVars.ddcmpbuff[cnt+23] = 0x59;																// n. 12				Civetta3 High
	gVars.ddcmpbuff[cnt+52] = 0x96;																// n. 27				Civetta4 Low
	gVars.ddcmpbuff[cnt+53] = 0x94;																// n. 27				Civetta4 High
}	

/*--------------------------------------------------------------------------------------*\
Method: EraseBackupData
	Azzera tutta la zona di BackupRAM in EEPROM.

	Parameters:
	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
void  EraseBackupData(void)  {

	PSHProcessingData();
	FillEEPI2C((uint)BeginBackupData, NULL, BackupDataLenght);
	PSHCheckPowerDown();
}

/*--------------------------------------------------------------------------------------*\
Method: PSHCheckPowerDown
	Controlla se durante la scrittura in EEPROM appena effettuata e' intervenuto un 
	PowerDown e quindi si deve procedere qui con il SaveBackupRAM.
	Con la "PSHProcessingData" controlla abilitazione alla scrittura EEPROM e BackupRAM.
	
Parameters:
	IN	- Struttura PowSup
	OUT -
\*--------------------------------------------------------------------------------------*/
void PSHCheckPowerDown(void) {
	
	PowSup.EEInUse = false;									// Fine scrittura EEPROM o BackupRAM
	if (PowSup.PWD_Flag) {
		SaveBackupData();									// Salvo area RAM in EEPROM e attendo la' Spegnimento o WD Reset 
	}
}

/*--------------------------------------------------------------------------------------*\
Method: PSHProcessingData
	Set flag "PowSup.EEInUse" indicante che si stanno aggiornando dati in EEPROM
	o in BackupRAM.
	
Parameters:
	IN	-
	OUT	- PowSup.EEInUse
\*--------------------------------------------------------------------------------------*/
void PSHProcessingData(void) {
	
	PowSup.EEInUse = true;									// EEPROM in uso
}
