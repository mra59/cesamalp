/****************************************************************************************
File:
    IoT_Files.c

Description:
    File con funzioni di creazione buffer dati per trasmissione ad AWS.
    

History:
    Date       Aut  Note
    Apr 2021	MR   

*****************************************************************************************/

#include <string.h>
#include <stdio.h>

#include "ORION.H" 
#include "Audit.h"
#include "IoT_Comm.h"
#include "IICEEP.h"
#include "stm32l4xx_hal.h"
#include "I2C_EEPROM.h"
#include "VMC_CPU_HW.h"
#include "DDCMPHook.h"
#include "DDCMPDTSHook.h"
#include "Monitor.h"
#include "OrionTimers.h"
#include "AWS_subscribe_publish.h"
#include "AuditExt.h"
#include "IoT_Files.h"


#if (IOT_PRESENCE == true)


/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

char 		TransactionIDValue[TRANSAC_ID_LEN];
char 		CopyOfTransID[TRANSAC_ID_LEN];
uint16_t 	ts_milliseconds = 0;
bool		JSON_Format = false;


/*--------------------------------------------------------------------------------------*\
Private Declarations 
\*--------------------------------------------------------------------------------------*/

char 		TS_string[32];
uint8_t		BlockNumber;

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	LDD_RTC_TTime 		DataOra;
extern	RTxVars 			gCommVars;
extern	IOT_COMM_STATE		DeviceIoT_State;

// --------------------------------------------------------------------------------------------
//***********************************************************************************************
// --  JSON File di incapsulamento per inviare i dati ad AWS ---

//--  generico JSON File per trasmissione dati ---
const char generic_json_frame[] = {
	"{\"pointofsaleid\":\"%d\","
    "\"transactionid\":\"%s\","
    "\"executiondate\":\"%s\","
	"\"blocknum\":\"%s\","
	"\"parameters\":\"%s\","
	"\"code\":\"%u\"}"
	};

const char statusIDs[] = {"EMRG*%d\\r\\nDOOR*%d\\r\\nPROD*%d\\r\\nINH1*%d\\r\\nINH2*%d\\r\\nINH3*%d\\r\\nINH4*%d\\r\\nTOU1*%d\\r\\nTOU2*%d\\r\\nTOU3*%d\\r\\nTOU4*%d\\r\\nEVC1*%d\\r\\nEVC2*%d\\r\\nEVC3*%d\\r\\nEVC4*%d\\r\\nRINH*%d\\r\\nPRD2*%d\\r\\n"};
const char MachineTtransactionID[] = {"CESAM%d"};
const char NumberOfBlock[] = {"%d"};


/*---------------------------------------------------------------------------------------------*\
Method: Response_AWS_OK_Rejected
	Prepara il messaggio di risposta al comando ricevuto da AWS: OK o Rejected.

	IN:	  - 
	OUT:  - messaggio in gCommVars.Gtwy_TxBuff come BUFFER TYPE 2
\*----------------------------------------------------------------------------------------------*/
void  Response_AWS_OK_Rejected(uint8_t AnswerType, uint8_t CMD_Code)
{
	char *buff = &gCommVars.Gtwy_TxBuff[0];
	char  		blockString[5];
	uint32_t 	t_id_high;
	uint16_t 	t_id_low;
		
	get_time_stamp(&t_id_high, &t_id_low);
	get_time_stamp_string(TS_string);
	blockString[0] = 'L';
	blockString[1] = 'A';
	blockString[2] = 'S';
	blockString[3] = 'T';
	blockString[4] = 0;

	*(buff++) = ASCII_STX;
#if (CRYPTO_FILES == true)
	*(buff++) = BUFFER_TYPE_7;
#else
	*(buff++) = BUFFER_TYPE_2;
#endif
	snprintf(buff, sizeof(gCommVars.Gtwy_TxBuff), generic_json_frame,
					CodiceMacchina,												    				//  "\"pointofsaleid\":\"%d\"},"
					&TransactionIDValue[0],															//	"\"idrequest\":{\"type\":\"%s\","
					TS_string,																		//  "\"executiondate\":\"%s\","
					blockString,																	//  "\"blocknum\":\"%s\","
					((AnswerType == AWS_CMD_OK)? "OK":"REJECTED"),									//  "\"parameters\":\"%s\","
					CMD_Code																		//  "\"code\":\"%u\"
				);
}

/*----------------------------------------------------------------------------------------------*\
Method: Prepare_MachineStatus_Buffer
	Prepara il buffer con messaggio di Status.

	IN:	  - 
	OUT:  - messaggio in gCommVars.Gtwy_TxBuff come BUFFER TYPE 2
\*----------------------------------------------------------------------------------------------*/
void  Prepare_MachineStatus_Buffer(void)
{
	char *buff = &gCommVars.Gtwy_TxBuff[0];
	char  		blockString[5];
	char  		localbuff[512];
	char *Pnt = &localbuff[0];
	uint32_t 	t_id_high;
	uint16_t 	t_id_low;
		
	get_time_stamp(&t_id_high, &t_id_low);
	get_time_stamp_string(TS_string);
	
	//-- Creazione JSON file da inviare ad AWS --
	blockString[0] = 'L';
	blockString[1] = 'A';
	blockString[2] = 'S';
	blockString[3] = 'T';
	blockString[4] = 0;
	memset(&localbuff[0], 0, sizeof(localbuff));
	/*
	snprintf(Pnt, sizeof(statusIDs), statusIDs, gVmcStatusIoT.NTC1, gVmcStatusIoT.NTC2, 																				\
												gVmcStatusIoT.DoorStatus, gVmcStatusIoT.AlarmStatus, gVmcStatusIoT.PressCO2_1, gVmcStatusIoT.PressCO2_2,				\
												gVmcStatusIoT.StatusCompressore, gVmcStatusIoT.Erogaz_1_Inh, gVmcStatusIoT.Erogaz_2_Inh, gVmcStatusIoT.Erogaz_3_Inh, 	\
												gVmcStatusIoT.Erogaz_4_Inh, gVmcStatusIoT.Erogaz_1_Tout, gVmcStatusIoT.Erogaz_2_Tout, gVmcStatusIoT.Erogaz_3_Tout,		\
												gVmcStatusIoT.Erogaz_4_Tout, gVmcStatusIoT.EV1_CC_Failure, gVmcStatusIoT.EV2_CC_Failure, gVmcStatusIoT.EV3_CC_Failure,	\
												gVmcStatusIoT.EV4_CC_Failure, gOrionConfVars.Remote_INH);
	*/
	
	snprintf(Pnt, sizeof(statusIDs), statusIDs, gVmcStatusIoT.EmergencyStatus, gVmcStatusIoT.DoorStatus, gVmcStatusIoT.ProductStatus,										\
												gVmcStatusIoT.Erogaz_1_Inh, gVmcStatusIoT.Erogaz_2_Inh, gVmcStatusIoT.Erogaz_3_Inh, 									\
												gVmcStatusIoT.Erogaz_4_Inh, gVmcStatusIoT.Erogaz_1_Tout, gVmcStatusIoT.Erogaz_2_Tout, gVmcStatusIoT.Erogaz_3_Tout,		\
												gVmcStatusIoT.Erogaz_4_Tout, gVmcStatusIoT.EV1_CC_Failure, gVmcStatusIoT.EV2_CC_Failure, gVmcStatusIoT.EV3_CC_Failure,	\
												gVmcStatusIoT.EV4_CC_Failure, gOrionConfVars.Remote_INH, gVmcStatusIoT.ProductStatus2);
	*(buff++) = ASCII_STX;

#if (CRYPTO_FILES == true)
	*(buff++) = BUFFER_TYPE_7;
#else
	*(buff++) = BUFFER_TYPE_2;
#endif
	snprintf(buff, sizeof(gCommVars.Gtwy_TxBuff), generic_json_frame,
					CodiceMacchina,												    				//  "\"pointofsaleid\":\"%d\"},"
					&TransactionIDValue[0],															//	"\"idrequest\":{\"type\":\"%s\","
					TS_string,																		//  "\"executiondate\":\"%s\","
					blockString,																	//  "\"blocknum\":\"%s\","
					localbuff,																		//  "\"parameters\":\"%s\","
					11																				//  "\"code\":\"%u\"
				);
}

/*----------------------------------------------------------------------------------------------*\
Method: Prepare_Data_Buffer
	Prepara il buffer per inviare dati Audit e di Configurazione.
	Il BlockNum e' trasmesso come stringa decimale (es 0xA3 --> 163).

	IN:	  - 
	OUT:  - messaggio in gCommVars.Gtwy_TxBuff come BUFFER TYPE 2
\*----------------------------------------------------------------------------------------------*/
void  Prepare_Data_Buffer(char *DataBuffer, uint16_t Size, uint8_t BlockNum, uint8_t Command)
{
	char *buff = &gCommVars.Gtwy_TxBuff[0];
	char  		blockString[5];
	char *Pnt = DataBuffer;
	uint32_t 	t_id_high;
	uint16_t 	t_id_low;
		
	get_time_stamp(&t_id_high, &t_id_low);
	get_time_stamp_string(TS_string);
	
	//-- Creazione JSON file da inviare ad AWS --
	memset(&blockString[0], 0, sizeof(blockString));
	if (BlockNum == LASTBLOCK)
	{
		blockString[0] = 'L';
		blockString[1] = 'A';
		blockString[2] = 'S';
		blockString[3] = 'T';
	}
	else
	{
		//blockString[0] =  hex_to_ascii(BlockNum >> 4);
		//blockString[1] =  hex_to_ascii(BlockNum);
		snprintf(&blockString[0], 4U, NumberOfBlock, BlockNum);
	}
	buff = &gCommVars.Gtwy_TxBuff[0];
	*(buff++) = ASCII_STX;
#if (CRYPTO_FILES == true)
	*(buff++) = BUFFER_TYPE_7;
#else
	*(buff++) = BUFFER_TYPE_2;
#endif
	snprintf(buff, sizeof(gCommVars.Gtwy_TxBuff), generic_json_frame,
					CodiceMacchina,												    				//  "\"pointofsaleid\":\"%d\"},"
					&TransactionIDValue[0],															//	"\"idrequest\":{\"type\":\"%s\","
					TS_string,																		//  "\"executiondate\":\"%s\","
					blockString,																	//  "\"blocknum\":\"%s\","
					Pnt,																			//  "\"parameters\":\"%s\","
					Command																			//  "\"code\":\"%u\"
				);
}

/*----------------------------------------------------------------------------------------------*\
Method: get_time_stamp
	Ritorna secondi e millisecondi.

	IN:	  - 
	OUT:  - RTC_Counter e ts_milliseconds
\*----------------------------------------------------------------------------------------------*/
void get_time_stamp(uint32_t* ts, uint16_t* ms)
{
	__disable_irq();
	*ts = RTC_Counter;
	*ms = ts_milliseconds;
	__enable_irq();
}

/*----------------------------------------------------------------------------------------------*\
Method: get_time_stamp_string
	Get time and date string from Unix std timestamp to send to backend.
	format %FT%T%:z => 2007-11-19T08:37:48-06:00 Date and time of day for calendar date (extended) ISO8601 format

	IN:	  - 
	OUT:  - messaggio in gCommVars.Gtwy_TxBuff come BUFFER TYPE 3
\*----------------------------------------------------------------------------------------------*/
void get_time_stamp_string(char* tstring)
{
	uint32_t seconds;
	uint32_t milliseconds;
	struct tm	*timeinfo;
	char		msec[6];
	
	__disable_irq();
	seconds = RTC_Counter;
	milliseconds = ts_milliseconds;
	__enable_irq();
	//seconds += TSTAMP_OFFSET;
	timeinfo = localtime(&seconds);

	strftime (tstring, 32, "%FT%T", timeinfo);
	sprintf(msec, ":%03d", milliseconds);
	strcat(tstring, msec);
}

/*------------------------------------------------------------------------------------------*\
 Method: Export_Audit_to_AWS

   IN:  - 
  OUT:  - true se errore, altrimenti false
\*------------------------------------------------------------------------------------------*/
bool Export_Audit_to_AWS(void)
{
    	bool			resp = false;
		uint16_t		datalen;
		char			*bufpnt;
		uint8_t  		buffer[TX_AWS_DATA_BUFFER_SIZE];      									// Buffer for parse 1k
const 	char Closing_Chars[] = {"DXE*1*1\\r\\n"};
	
	//**************************************************************************************
	//******  Creazione Audit EVA-DTS e     ********
	//**************************************************************************************
	
	if (DDCMPDTSHookReadDataReqInd(AUDIT_LIST, 0, 0, 0) == true)
	{																							// Lista Audit set e init di tutte le variabili, orologio compreso
		GreenLedCPU(OFF);
		RedLedCPU(ON);
		BlockNumber = 1;
		
		memset(&buffer, 0, sizeof(buffer));
		bufpnt = (char*)&buffer[0];
		Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));											// Init Buffer destinazione Audit and counters
		// Dal loop puo' uscire in 2 modi:
		// 1) AuditLoopBody() == false perche' ha finito la scansione degli ID: 
		//    trasferire Buffer su chiave USB aggiungendo DXE di chiusura
		// 2) uData.audit.localBufferSize != 0 perche' il buffer e' pieno e l'ultimo dato letto non ci sta:
		//    trasferire su chiave USB il buffer audit e copiare successivamente l'ultimo dato letto nel buffer
		
		do {
			//  ********   Audit   Standard    ************
			RedLedCPU_Toggle();																	// Toggle Led Rosso
			JSON_Format = true;																	// Nel formato JSON memorizza come CRLF la stringa di 4 chr "\r\n" anziche' i due bytes 0x0d-0x0a
			if (AuditLoopBody() == false)														// Audit Standard END
			{
				if (gOrionConfVars.AuditExt == true)											// Trasmettere anche l'Audit Estesa
				{
					//  ********   Audit   Estesa    ************
					do {
							if (Extended_Audit_LoopBody() == false)
							{
								break;															// Audit Estesa END
							}
							if (Get_localBufferSize() != 0)										// Buffer destinazione Audit pieno e l'ultimo ID appena letto non ci sta
							{
								//--  Trasferimento intermedio Buffer Audit  --
								datalen = Get_dataBuffTransmitted();							// Lunghezza del buffer da trasferire su USB

								//-- Trasmissione blocco Audit ad AWS --
								Prepare_Data_Buffer(bufpnt, datalen, BlockNumber, CMD_AUD_REQ_WOUT_RESET);								
								DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
								TmrStart(gCommVars.GtwyTime, TIME_TO_SEND_DATA_BLOCK);			// Attesa per l'invio del blocco Audit alla Gateway
								gCommVars.CMD_State = Cmd_IDLE;
								Gtwy_Poll();
								while(TmrTimeout(gCommVars.GtwyTime) == false){};				// Attendo invio buffer ad AWS
								BlockNumber++;
								//------------------------------------------------				

								memset(&buffer, 0, sizeof(buffer));
								//bufpnt = (char*)&buffer[0];
								Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));					// Init Buffer destinazione Audit and counters
								DDCMPMngrHookCopyLocalBuffer();									// Trasferisco ultimo ID appena letto nel buffer audit
							}
				  	} while(true);
				}
			  
				//***********************************************				  
			  	//--  Fine File Audit, aggiungo DXE e termino --
				//***********************************************				  
				
				datalen = Get_dataBuffTransmitted();											// Lunghezza del buffer da trasferire su USB
				if (Get_dataBuffFree() < sizeof(Closing_Chars))
				{
					//-- Trasmissione blocco Audit ad AWS --
					Prepare_Data_Buffer(bufpnt, datalen, BlockNumber, CMD_AUD_REQ_WOUT_RESET);	// L'ID chiusura non ci sta nel buffer: copio il buffer in EEPROM e poi aggiungo DXE
					DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
					TmrStart(gCommVars.GtwyTime, TIME_TO_SEND_DATA_BLOCK);						// Attesa per l'invio del blocco Audit alla Gateway
					gCommVars.CMD_State = Cmd_IDLE;
					Gtwy_Poll();
					while(TmrTimeout(gCommVars.GtwyTime) == false){};							// Attendo invio buffer ad AWS
					BlockNumber++;
					memset(&buffer, 0, sizeof(buffer));
					//------------------------------------------------				
					bufpnt = (char*)&buffer[0];
					datalen = 0;
					Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));								// Init Buffer destinazione Audit and counters
				}
				snprintf((char *)&buffer[datalen], sizeof(Closing_Chars), Closing_Chars);		// "DXE*1*1\r\n\0" a chiusura del file Audit
				datalen += (sizeof(Closing_Chars) -1); 
			  	
				//-- Trasmissione blocco Audit ad AWS --
				BlockNumber = LASTBLOCK;
				Prepare_Data_Buffer(bufpnt, datalen, BlockNumber, CMD_AUD_REQ_WOUT_RESET);
				DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
				TmrStart(gCommVars.GtwyTime, TIME_TO_SEND_DATA_BLOCK);							// Attesa per l'invio del blocco Audit alla Gateway
				gCommVars.CMD_State = Cmd_IDLE;
				Gtwy_Poll();
				while(TmrTimeout(gCommVars.GtwyTime) == false){};								// Attendo invio buffer ad AWS
				//------------------------------------------------				
				break;
			}
			if (Get_localBufferSize() != 0)														// Buffer destinazione Audit pieno e l'ultimo ID appena letto non ci sta
			{
				//--  Trasferimento intermedio Buffer Audit  --
			  	datalen = Get_dataBuffTransmitted();											// Lunghezza del buffer da trasferire su USB

				//-- Trasmissione blocco Audit ad AWS --
				Prepare_Data_Buffer(bufpnt, datalen, BlockNumber, CMD_AUD_REQ_WOUT_RESET);
				DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
				TmrStart(gCommVars.GtwyTime, TIME_TO_SEND_DATA_BLOCK);							// Attesa per l'invio del blocco Audit alla Gateway
				gCommVars.CMD_State = Cmd_IDLE;
				Gtwy_Poll();
				while(TmrTimeout(gCommVars.GtwyTime) == false){};								// Attendo invio buffer ad AWS
				BlockNumber++;
				memset(&buffer, 0, sizeof(buffer));
				//------------------------------------------------				
				
				bufpnt = (char*)&buffer[0];
				Set_Audit_Buf(bufpnt, (sizeof(buffer) -100));									// Init Buffer destinazione Audit and counters
				DDCMPMngrHookCopyLocalBuffer();													// Trasferisco ultimo ID appena letto nel buffer audit
			}
		} while(true);
	}
	else
	{
		// -- Prelievo Audit non disponibile --
		resp = true;																			// Exit error
		goto EndExportAudit;
	}

	//Set_DDCMP_EnableAuditClear();																// Registro data/ora prelievo attuale e marca per abilitare azzeramento Audit al successivo Main
	//buffer[0] = AUDIT_USB_READY;
	//WriteEEPI2C(AuditFileReady, (byte *)(&buffer), 1U);											// Scrittura marca Buffer Audit Ready per successivo azzeramento dati parziali UP
	Clear_fDDCMPTransferInProgress;																// Riabilita sistemi di pagamento

EndExportAudit:
	GreenLedCPU(ON);
	RedLedCPU(OFF);
	JSON_Format = false;
	return resp;
}

/*------------------------------------------------------------------------------------------*\
 Method: Export_Config_to_AWS

   IN:  - 
  OUT:  - sempre false = 0k
\*------------------------------------------------------------------------------------------*/
bool Export_Config_to_AWS(void)
{
#define	EE_PAGE_SIZE	128
    	
		uint16_t		datalen;
		char			*bufpnt;
		char	  		buffer[TX_AWS_DATA_BUFFER_SIZE];      									// Buffer for parse 1k
		uint8_t			*localpnt;
		uint8_t			localbuff[PageSize+2];
		uint16_t		TxConf_Start_Address = 0;
		uint32_t		j;
		uint8_t			temp;

		GreenLedCPU(OFF);
		RedLedCPU(ON);
		BlockNumber = 1;
		Rd_EEBuffStartAddr_And_BankSelect(BUF_TXCONFIG);										// In MemoryBytes[] indirizzo di inizio del Buffer TxConfig; predispone anche il Bank della EE
		TxConf_Start_Address = 0;				
		TxConf_Start_Address += MemoryBytes[1];													// LSB
		TxConf_Start_Address += (MemoryBytes[0]<<8);											// MSB
		JSON_Format = true;																		// Nel formato JSON memorizza come CRLF la stringa di 4 chr "\r\n" anziche' i due bytes 0x0d-0x0a
		bufpnt = &buffer[0];
		memset(&buffer, 0, sizeof(buffer));
		datalen = 0;
		//-- Loop lettura Configurazione da EEprom per trasferimento su AWS --
		do {
			localpnt = &localbuff[0];
			memset(&localbuff, 0, sizeof(localbuff));
			ReadEEPI2C(TxConf_Start_Address, localpnt, EE_PAGE_SIZE);							// Leggo EE_PAGE_SIZE bytes dalla EE
			TxConf_Start_Address += EE_PAGE_SIZE;
			for (j=0; j < EE_PAGE_SIZE; j++)
			{
				temp = *(localpnt+j);
				if ((TxConf_Start_Address - TxConfigBuff) > EE_TXCONFBUF_LEN)
				{
					temp = 0;																	// Mai trovato carattere di chiusura buffer TxConfigBuff: invio ultimo buffer con blocknum "LAST"
					if (datalen == 0)
					{
						*(bufpnt++) = ASCII_SPACE;												// Ultimo e unico chr da inviare
						datalen++;
					}
				}
				if ((temp == 0) || (datalen > (TX_AWS_DATA_BUFFER_SIZE-10)))
				{
					//-- Trasmissione blocco Audit ad AWS --
					if (temp == 0)
					{
						BlockNumber = LASTBLOCK;
					}
					Prepare_Data_Buffer(&buffer[0], datalen, BlockNumber, CMD_CONFIG_REQ);
					DeviceIoT_State = IoT_TX_BUFFER_TO_AWS;
					TmrStart(gCommVars.GtwyTime, TIME_TO_SEND_DATA_BLOCK);						// Attesa per l'invio del blocco Audit alla Gateway
					gCommVars.CMD_State = Cmd_IDLE;
					Gtwy_Poll();
					while(TmrTimeout(gCommVars.GtwyTime) == false){};							// Attendo invio buffer ad AWS
					BlockNumber++;
					bufpnt = &buffer[0];
					memset(&buffer, 0, sizeof(buffer));
					datalen = 0;
				}
				if (temp == 0) break;															// Invio Config terminato
				if (temp == ASCII_CR)
				{
					*(bufpnt++) = ASCII_BS;
					*(bufpnt++) = ASCII_r;
					datalen += 2;
				}
				else if (temp == ASCII_LF)
				{
					*(bufpnt++) = ASCII_BS;
					*(bufpnt++) = ASCII_n;
					datalen += 2;
				}
				else
				{
					*(bufpnt++) = temp;
					datalen++;
				}
			}
		} while(temp != 0);

	GreenLedCPU(ON);
	RedLedCPU(OFF);
	JSON_Format = false;
	return false;
}

/*----------------------------------------------------------------------------------------------*\
Method: RemoteErogazAnswer
	Prepara il buffer con messaggio di risposta alla richiesta di eogazione remota.

	IN:	  - 
	OUT:  - messaggio in RemoteErogazAnswerBuffer 
\*----------------------------------------------------------------------------------------------*/
void  RemoteErogazAnswer(uint8_t ErrorCode)
{
	char *buff = &RemoteErogazAnswerBuffer[0];
	char  		blockString[5], ErrorCodeString[3];
	uint32_t 	t_id_high;
	uint16_t 	t_id_low;
		
	get_time_stamp(&t_id_high, &t_id_low);
	get_time_stamp_string(TS_string);
	
	//-- Creazione JSON file da inviare ad AWS --
	blockString[0] = 'L';
	blockString[1] = 'A';
	blockString[2] = 'S';
	blockString[3] = 'T';
	blockString[4] = 0;
	
	//ErrorCodeString[0] =  hex_to_ascii(ErrorCode >> 4);
	//ErrorCodeString[1] =  hex_to_ascii(ErrorCode);
	//ErrorCodeString[2] = 0;
	
	ErrorCodeString[0] =  hex_to_ascii(ErrorCode);
	ErrorCodeString[1] = 0;

	memset(buff, 0, REMOTE_EROGAZ_ANSW_BUFSIZE);
	*(buff++) = ASCII_STX;
#if (CRYPTO_FILES == true)
	*(buff++) = BUFFER_TYPE_7;
#else
	*(buff++) = BUFFER_TYPE_2;
#endif
	snprintf(buff, sizeof(RemoteErogazAnswerBuffer), generic_json_frame,
					CodiceMacchina,												    				//  "\"pointofsaleid\":\"%d\"},"
					&CopyOfTransID[0],																//	"\"idrequest\":{\"type\":\"%s\","
					TS_string,																		//  "\"executiondate\":\"%s\","
					blockString,																	//  "\"blocknum\":\"%s\","
					ErrorCodeString,																//  "\"parameters\":\"%s\","
					CMD_EROGAZ_REQ																	//  "\"code\":\"%u\"
				);
	gCommVars.SendRemoteErogazAnswToAWS = true;
}



#endif	// IOT_PRESENCE
	
