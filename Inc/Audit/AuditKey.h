/*--------------------------------------------------------------------------------------*\
File:

  AuditKey.c

Description:
  Audit Key manager
  
History:
  Date     Aut  Note
  -------- ---  ----


\*--------------------------------------------------------------------------------------*/

#define AuditKeyPageLength              64
#define k_MaxKeyPageNumb                1024
#define k_MaxMifareCardPageNumb         1024
#define ClockKeyPage                    2048
#define AuditKeyFormatCode              0x55AA
#define kNumConfigKey					8

#define kAuditKeyError                  0x1
#define fAuditKeyError                  (pntAuditKey->Flags & kAuditKeyError)
#define Set_fAuditKeyError              (pntAuditKey->Flags |= kAuditKeyError)                  
#define Clear_fAuditKeyError            (pntAuditKey->Flags &= (0xFF-kAuditKeyError))  

#define kAuditKeyFull                   0x2
#define fAuditKeyFull                   (pntAuditKey->Flags & kAuditKeyFull)
#define Set_fAuditKeyFull               (pntAuditKey->Flags |= kAuditKeyFull)                  

#define kLastBlockKey                   0x80
#define fLastBlockKey                   (pntAuditKey->Flags & kLastBlockKey)
#define Set_fLastBlockKey               (pntAuditKey->Flags |= kLastBlockKey)                  
#define Clear_fLastBlockKey             (pntAuditKey->Flags &= (0xFF-kLastBlockKey))  

typedef struct {
        byte  TestEEPROMKey[8];                   /* Bytes per test eeprom chiave */
        uint  WritePageNumber;                    /* Numero Pagina inizio spazio disponibile */
        uint  ReadPageNumber;                     /* Numero Pagina inizio spazio occupato */
        uint  WriteBlocksNumber;                  /* Numero blocchi Audit scritti su chiave */
        uint  ReadBlocksNumber;                   /* Numero blocchi Audit letti da chiave */
        uint  PasswordSistema;                    /* Password Sistema */
        uint  OperatorCode;                       /* Codice Gestore */
        uint  FormatCode;                         /* Per test formattazione */	 
		uint  UserCode;							  		/* Codice utente per test Black List */
		uint16 AuditKeyOption1;					  /* Opzioni  */
		uint16 AuditKeyOptionNP;  				/* Opzioni 'NP' */	 // 27
		uint16 AuditKeyOptionNPM;				  /* Opzioni  */
//		uint16 AuditKeySize;				  	/* numero di pagine da 64 byte  */
		uint8 AuditKeyOptionD;					  /* Opzioni  */
		uint8 AuditKeyOptionM;					  /* Opzioni  */
		uint8 AuditKeyOptionY;					  /* Opzioni  */
		uint8 AuditKeyEEChip;					  	/* Opzioni  */
#if	kConfigurationKey || Oscar_Printer
		byte  unused;						/* per allineamento all'inetro */
		byte  ConfigurationKey;				/* se diverso da 0 indica che la chiave � di configurazione */
		uint  PageIndex[kNumConfigKey];		/* indice delle pagine contenenti configurazioni */
#endif	//kConfigurationKey
    } sAKPage0;

typedef struct {
        byte  Flags;                              /* Flags stato */
        uint  DataFrameUsed;                      /* Spazio occupato in DataBuffer */
        char  *pntDest;                           /* pointer write DataBuffer */
        sAKPage0 AKPage0;
        byte  DataBuffer[AuditKeyPageLength];     /* Buffer pagina per chiave */
        byte  Buffer[42];                         /* Buffer eccedenza */
        byte  FreeBuffer[42];                     /* Buffer preparazione dato e lettura da chiave*/
    } sAuditKey;

typedef struct {
        byte  Seconds;                            /* Secondi */
        byte  Minutes;                            /* Minuti */
        byte  Hours;                              /* Centenario + ore */
        byte  Day;                                /* Giorno della settimana */
        byte  Date;                               /* Giorno del mese */
        byte  Month;                              /* Mese */
        byte  Year;                               /* Anno */
    } sAKClock;


