/*--------------------------------------------------------------------------------------*\
File:
	Gestore_Exec_MSTSLV.h

Description:
    Dichiarazione strutture e routines Gestore Master/Slave Executive

History:
	Date	   Aut	Note
    Giu 2017	MR   
\*--------------------------------------------------------------------------------------*/

#ifndef __Gestore_Exec_MSTSLV_H_
#define __Gestore_Exec_MSTSLV_H_

/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

// --- Risposte dal VMC -----
#define 	RX_VMC_NO_VEND_REQUEST			0xFE
#define 	RX_VMC_VEND_RESPONSE            0x80
#define 	VMC_INIBIT_FLAG                 0x40
#define		VMC_FREE_VEND_REQUIRED_FLAG     0x80
#define 	kSlave_PNACK           			0xff

// --- Comandi del Master -----
#define 	EXEC_RR_NACK					0x3F												// Not Acknowledge dal Master Executive


#define 	MaxRRNoResp						5													// MAX numero di cmd da RR ai quali non si invia risposta


/*--------------------------------------------------------------------------------------*\
Strutture
\*--------------------------------------------------------------------------------------*/

// --- Fasi del Sistema --- 
typedef enum {
	//kCredZero,
	kCredBeforeVendReq,
	kWaitVend
} SystemPhase;

// VMC command codes
typedef enum {
	kExeRRCmdUnused0,
	kExecRRCmdStatus,
	kExecRRCmdCredit,
	kExecRRCmdVend,
	kExecRRCmdAudit,
	kExecRRCmdSendAuditAddr,
	kExecRRCmdSendAuditData,
	kExecRRCmdIdentify,
	kExecRRCmdAcceptData,
	kExecRRCmdDataSync,
	kExecRRCmdUnused10,
	kExecRRCmdUnused11,
	kExecRRCmdUnused12,
	kExecRRCmdUnused13,
	kExecRRCmdUnused14,
	kExecRRCmdNACK,
	kExecRRCmdUnknown	= 0xf0
} RRExecCmdCode;

typedef struct {
    CreditValue     creditVal;						// Credit value
    uint8_t			dpPosition;         			// Decimal point position to use to display credit value
    uint8_t			exactChange;    				// Exact change required (could become obsolete)
} InfoCredit;

typedef struct {
	uint8_t			scalingFact;
	uint8_t			decimalPoint;
	uint16_t		baseUnitValue;
	bool			exactChange;
} ExeCreditData;									// Credit data information


typedef struct {
	char			lastCharSent;					// Last char sent
	uint8_t			dataCount;						// Counter data bytes ricevuti
	uint8_t			statusCnt;						// Count status messages received
	uint8_t			masterUnavail;					// true if master unavailable
	uint8_t			cdChanged;						// true if cd changed
	uint8_t			commActivity;					// used to determine if master is available
	Tmr32Local		commTmr;						// Used to check comm line activity
	uint8_t			ExecSlvState;
	CreditValue		lastSelValue;
	bool			VendInProgress;
	bool			EsitoVend;
	RRExecCmdCode	CMD;							// Comando ricevuto
	SystemPhase		Phase;							// Fasi del sistema
	uint8_t			ByteDaGettRR;					// chr ricevuto dal Master Exec
	uint8_t			ByteDaVMC;						// chr ricevuto dal VMC 
	uint8_t			RRCmd_Cnt;						// Counter chr ricevuti dalla RR senza trasmettere risposta 
	uint8_t			VMC2_SelValue;					// Valore/Numero Selezione richiesta dal VMC2 
	bool			fRxChrDaGettRR;					// 1 = ricevuto chr dal Master Exec
	bool			fRxChrDaVMC;					// 1 = ricevuto chr dal VMC
	bool			fChrDaGettRR_SentToVMC;			// 1 = inviato al VMC chr ricevuto dal Master Exec
	bool			fVMC2_SelezReq;					// 1 = VMC2 ha richiesto selezione
	bool			fVMC2_FV_Req;					// 1 = VMC2 ha richiesto una Free Vend
	bool			fMasterSuspend;					// 1 = il Master Executive sospende ogni trasmissione al VMC2
	bool			fSlaveSuspend;					// 1 = lo Slave Executive non deve rispondere alla RR perche' VMC2 in vendita
	bool			fRRToVMC2;						// 1 = reindirizza comunicazioni RR al VMC2
	InfoCredit		ci;								// Current credit information
	ExeCreditData	cd;								// Last credit data received
	
#if	  kProtExecSlaveReplyDelay
	bool			sendIt;
	TmrLocal		delayReplyTmr;					// Used to delay replies
#endif
} MSVars;


/**************************************************************************************************************/
//extern typedef
/*
extern uint8_t         UnitScalingFactor;
extern uint8_t         DecimalPointPosition;
extern uint16_t        VMC_CreditoDaVisualizzare;

extern uint16_t  	VMC_ValoreSelezione;
extern uint8_t  	VMC_NumeroSelezione;

extern uint8_t		VMC_Command;
extern uint8_t		VMC_Response;

extern uint8_t          UnitScalingFactor;
extern uint8_t		TabellaSelezPrezzo[100];	
*/

/*--------------------------------------------------------------------------------------*\
Executive Master Slave routines
\*--------------------------------------------------------------------------------------*/

static bool  MSExecSlaveUpdateCreditInfo(void);
static void  TraceCMDRR(uint8_t data);
static void  SwitchToVMC2(void);
static void  SwitchToLocalVMC(void);

void 	 GestMS_Init(void);
void 	 Slave_Exec_Task(void);
void  	 GestMS_MasterRxDataDaVMC2(void);
void  	 GestMS_RR_CMD(uint8_t data);
void  	 GestMS_RxRR_Error(uint8_t data);
void  	 GestMS_RxVMC2_Error(uint8_t data);
void  	 GestMS_RxVMC2(uint8_t data);
bool  	 IsSlaveTxSuspendStatus(void);
bool  	 GestMS_VMC2_FV_Req(void);
bool  	 GestMS_VMC2_VendReq(uint8_t Selezione);
void  	 GestioneMS_Exec(void);
bool 	 GestMS_IsVMC2InVend(void);
uint8_t  RR_ReadyForVMC2Vend(void);
void  	 Set_VMC2_VendReq(uint8_t Selezione);
void  	 Set_VMC2_FV_Req(void);
void  	 VMC_Locale_Reply(uint8_t Reply);
void 	 GestoreMS_Task(void);




#endif		// __Gestore_Exec_MSTSLV_H_


