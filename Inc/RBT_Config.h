/****************************************************************************************
File:
    RBT_Config.h

Description:
    Include file con definizioni e strutture per la configurazione del VMC.

History:
    Date       Aut  Note
    Giu 2013	MR   

 *****************************************************************************************/

	
/*===============================================================
 *                     Response Error Values
 * =============================================================*/

#define RD_WR_CFG_OK		(0x00)            
#define CFG_NO_DELIMITER	(0x02)		// Manca il chr "*" tra l'ID e il valore  
#define WrCfg_Incomplete	(0x04)		// Configurazione completata solo parzialmente
#define ID_NOT_FOUND		(0x05)		// ID non trovato nella funzione "RW_ConfParam"
#define WrCfg_WriteFail		(0x06)		// Scrittura parametro in EEPRom fallita
#define WrRdCfg_ReadFail	(0x07)		// Lettura parametro dalla EEPRom fallita
#define BufferEE_Full		(0x08)		// Superato lo spazio disponibile in EEPROM


/*--------------------------------------------------------------------------------------*\
Types definition
\*--------------------------------------------------------------------------------------*/

#define	 ASCIISTARLEN			1		// Lunghezza chr asterisco
#define	 NEWLINELEN				2		// Lunghezza chr NewLine (0x0d 0x0a)

#define  MAX_LEN_STRING			255		// Numero massimo di caratteri per stringa di configurazione
#define  ID_LEN				  	4		// Numero di caratteri che costituiscono un ID di configurazione
#define  ID_PLUS_ASTERISC_LEN	5		// Numero di caratteri che costituiscono un ID di configurazione + l'asterisco finale
#define  EE_WRITE			  	0		// Scrittura dati dalla EEProm
#define  EE_READ			  	1		// Lettura dati dalla EEProm
#define  ID_LOOP_ENUM_START	  	100		// Numero di inizio degli ID Loop nell'enum PAR_ID


const char PROT_ID[] = {"PROT*%03u\r\n"};	// Tipo di prodotto
const char SWRV_ID[] = {"SWRV*%04s\r\n"};	// Revisione Prodotto
	
const unsigned char CFG_PARAM[99][ID_PLUS_ASTERISC_LEN] = {
        {"MODM*"},					// Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20)
        //{"SNXX*"},					// Serial Number del Produttore ID105 EVA-DTS - User Defined Field (Alfanumerico 1-12) 
		{"MS00*"},					// Messaggio programmabile Erogazione 1  da mettere sul display LCD
		{"MS01*"},					// Messaggio programmabile Erogazione 2  da mettere sul display LCD
		{"MS02*"},					// Messaggio programmabile Erogazione 3  da mettere sul display LCD
		{"MS03*"},					// Messaggio programmabile Erogazione 4  da mettere sul display LCD
		{"AUDP*"},
        {"CODM*"},
        {"CODE*"},
        {"CRMK*"},
        {"CRMM*"},
        {"DECP*"},
        {"FLAG*"},
        {"GEST*"},
        {"SING*"},
        {"PRHD*"},					// Price Holding
        {"SYSP*"},
        {"UNSF*"},
        {"VALU*"},
        {"DISC*"},
        {"VEGA*"},					// Opzione Vega Digisoft
        {"USER*"},					// Opzione per aggiornare il codice Utente durante l'inizializzazione delle carte preformattate
        {"UCOD*"},					// Codice Utente di partenza per l'inizializzazione delle carte preformattate
        {"INHP*"},					// Polarita' selettore Parallelo
        {"CHGM*"},					// Resto Massimo (Max Change)
        {"BNKE*"},					// Abilitazione Bill Validator: 0=solo con carta, 1=sempre
        {"EXTR*"},					// Preleva Audit Estesa
        {"INHX*"},					// Inhibit se Audit Estesa Full
        {"PERM*"},					// Cash Permanente (No timeout)
        {"MICT*"},					// Numero Minimo monete nei tubi (per determinare exact-change)
        {"SRNM*"},					// Serial Number uP
        {"PRDS*"},					// Price Display
        {"EXCH*"},					// Equazione Exact Change: 0=ExCh quando anche un solo tubo sotto il minimo, 1=ExCh quando il tubo di valore inferiore e' sotto il minimo
        {"RICU*"},					// Opzione Carta utente ricaricabile (=0) o NON ricaricabile (=1)
        {"ACCK*"},					// Credito Massimo Accettazione Carta
	    {"CHNG*"},					// Cambiamonete
		{"SSID*"},					// SSID Wi-Fi (Alfanumerico 1-32)
		{"WPSW*"},					// Password SSID Wi-Fi (Alfanumerico 1-32)
		{"TERR*"},					// Errore RTC in secondi
		{"TCAM*"},					// Tempo di misura dell'errore in minuti
		{"SEGN*"},					// Segno dell'errore del RTC: 1 = resta indietro - 0 = corre
		{"AUST*"},					// Ora Inizio Fascia per trasmissione Audit telemetria
		{"AUSP*"},					// Ora Fine Fascia per trasmissione Audit telemetria
		{"CIFR*"},					// Selezione Cifratura per Gateway AWS
		0x00
};

const unsigned char CFG_PAR_LOOP[16][ID_PLUS_ASTERISC_LEN] = {
        {"SA00*"},					// Byte A Opzioni Selezioni
        //{"SB00*"},					// Byte B Opzioni Selezioni
        {"SP00*"},					// Selezione/Prezzo in Price Holding 
        {"OP00*"},					// Modello Macchina ID102 EVA-DTS (Alfanumerico 1-20)
        //{"FA00*"},
		{"FB00*"},
        {"MI00*"},
        {"PR00*"},
        {"PA00*"},
        {"MN00*"},
		0x00
};

//**** ATTENZIONE: LA SEQUENZA DEGLI ENUM DEVE RISPETTARE QUELLA DELL'ARRAY CFG_PARAM E, IN FONDO, QUELLA DELL'ARRAY CFG_PAR_LOOP****
typedef enum {
		MODM = 0,
		//SNXX,
		MS00,
		MS01,
		MS02,
		MS03,						// 5
		AUDP,
		CODM,
		CODE,
		CRMK,						// 9
		CRMM,						// 10
		DECP,
		FLAG,
		GEST,
		SING,						// 20
		PRHD,
		SYSP,
		UNSF,
		VALU,
		DISC,
		VEGA,
		USER,
		UCOD,
		INHP,
		CHGM,
		BNKE,
		EXTR,
		INHX,
		PERM,
		MICT,
		SRNM,
		PRDS,
		EXCH,
		RICU,
		ACCK,
		CHNG,
		SSID,
		WPSW,
		TERR,
		TCAM,
		SEGN,
		AUST,
		AUSP,
		CIFR,
		SA00 = ID_LOOP_ENUM_START,		// 100
		//SB00,							// 101
		SP00,							// 102
		OP00,							// 103
		//FA00,							// 104
		FB00,							// 105
		MI00,							// 106
		PR00,							// 107
		PA00,							// 108
		MN00							// 109
} PAR_ID;


const uint32_t id_FWRev[] = {
	(uint32_t)"PROT",					// Tipo di prodotto
	(uint32_t)"SWRV",					// Revisione Firmware del prodotto
	(uint32_t)"BTRV"					// Revisione Firmware del Bootloader
};

#define	 idLoop_NumElem	   10																													// Numero degli ID dell'array idLoop

// La disposizione delle lunghezze degl iarray deve rispettare la sequenza in "PAR_ID"
//const  uint16_t idLoopLen[] = {SA_LEN, SB_LEN, SP_LEN, OP_LEN, FA_LEN, FB_LEN, MI_LEN, LenListaPrezzi, LenListaPrezzi, NumOfCoins};
const  uint16_t idLoopLen[] = {SA_LEN, SP_LEN, OP_LEN, 4, MI_LEN, LenListaPrezzi, LenListaPrezzi, NumOfCoins};

/*--------------------------------------------------------------------------------------*\
Function Prototypes 
\*--------------------------------------------------------------------------------------*/

uint8_t		CheckNewConfig(void);
uint8_t		SendConfToEEprom(uint8_t);
uint16_t  	GetStringFromEEprom(uint16_t *Addr_EEP);
uint8_t 	send_cfg(uint8_t* pbuf, uint8_t element, bool end);
uint8_t 	write_new_param(uint8_t *pnt, uint8_t len);
void  		GetRxConfigBuffAddress(void);

static uint8_t	RW_ConfParam(PAR_ID id, uint32_t * dato, uint8_t ReadWrite, uint16_t Id_Index);


























