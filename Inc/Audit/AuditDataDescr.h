/****************************************************************************************
File:
    AuditDataDescr.h

Description:
    Include file con dichiarazioni e defines per l'Audit

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __AuditDataDescr__
#define __AuditDataDescr__

#define AUDIT_STRING                1
#define AUDIT_DATA                  2
#define AUDIT_BEGIN_LOOP            3
#define AUDIT_LOOP_INDEX            4
#define AUDIT_FREE_FIELD            5
#define AUDIT_END_LOOP              6
#define AUDIT_EXEC_FUNCTION         7
#define AUDIT_END_STRING            8
#define AUDIT_RESET_DATA            9
#define AUDIT_COPY_DATA             10
#define AUDIT_SUM_DATA              11
#define AUDIT_SUB_DATA              12
#define AUDIT_RESULT_DATA           13
#define AUDIT_DATE                  14
#define AUDIT_TIME                  15
#define AUDIT_TEST_DATA            	16
#define AUDIT_TEST_EVENT            17
#define AUDIT_MESS_INDEX            18
#define AUDIT_BEGIN_LOOP_INDEX		19	
#define AUDIT_TEST_PRICE			20
#define	AUDIT_PRICEDISC_DATA		21


#define DATA_SI_ZERI                        0x80
#define DATA_NO_ZERI                        0

#define DATA_SI_INDEX                       0x40
#define DATA_NO_INDEX                       0

#define DATA_LONG                           0
#define DATA_INT                            0x20
#define DATA_BYTE                           0x10


// Defines di Mask per AUDIT_STRING
#define CONSTANT_STRING                     0
#define MESSAGE_STRING                      1

// Defines di Mask per AUDIT_END_STRING e AUDIT_END_LOOP
#define END_STRING                          2

// Defines di Mask per AUDIT_DATA e AUDIT_RESULT_DATA e AUDIT_LOOP_INDEX
//#define MSG_FORMAT                          1
//#define DECIMAL_POINT           			2

// Defines di Mask 
//#define PRINTER                             0x80

enum {
  kAuditFileRevisionSet = 1,
  kTestLA1NewMode,
  kTestLA1,
  kTestFidelity,
  kTubesCoinValGet,
  kTestEA2Cauzione,
  kTestBillReader,
  kTestCA1,
  kTestDA1
};

enum {
  kLA1_0 = 1,
  kLA1_1,
  kLA1_2,
  kLA1_3,
  kLA1_4
};

typedef struct {
  byte  Type;
  byte  Mask;
  const void *pnt;
  byte  Length;
} tAString;

typedef struct {
  byte  Type;
  byte  Mask;
  uint  Mess;
  byte  Length;
} tAMessage;

typedef struct {
  byte  Type;
  byte  Mask;
  ulong *pnt;
  byte  Length;
} tADataLong;

typedef struct {
  byte  Type;
  byte  Mask;
  uint  *pnt;
  byte  Length;
} tADataInt;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  *pnt;
  byte  Length;
} tADataByte;

typedef struct {
  byte  Type;
  byte  Mask;
  void  *pnt;
  byte  Length;
} tAData;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  Min;
  byte  Max;
  byte  DataOffset;
} tABeginLoop;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  Min;
  void  *Max;
  byte  DataOffset;
} tABeginLoopIx;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  LoopOffset;
  byte  Length;
  byte  Free;
} tALoopIndex;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  Free;
  byte  Free1;
  byte  Free2;
} tAFreeField;

#if kApplReduceAudit || kOnOffEvents || kApplExChEvents || kApplRTCEventSupport
typedef struct {
  byte  Type;
  byte  Mask;
  void  *pnt;
  byte  Addr;
} tATestData;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  *pnt;
  byte  Length;
} tAMessIndex;
#endif

typedef struct {
  byte  Type;
  byte  Mask;
  byte  function;
  byte  TableID_Offset;
  byte  List;
} tATestLA1;

typedef struct {
  byte  Type;
  byte  Mask;
const  void  *pnt;
  byte  TableID_Offset;
} tAEndLoop;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  Function;
  byte  Free;
  byte  Free1;
} tAExecFunction;

typedef struct {
  byte  Type;
  byte  Mask;
  byte  Free;
  byte  Free1;
  byte  Free2;
} tAEndString;

typedef struct {
  byte  Type;
  byte  Mask;
const  void  *pnt;
  byte  Length;
} tACopyData;

typedef struct {
  byte  Type;
  byte  Mask;
const  void  *pnt;
  byte  Length;
} tASumData;

typedef struct {
  byte  Type;
  byte  Mask;
  void  *pnt;
  byte  Length;
} tASubData;

typedef struct {
  byte  Type;
  byte  Mask;
  void  *pnt;
  byte  Length;
} tAResultData;

typedef struct {
const  void  *pnt;
} tAuditTable;

//  #endif

#endif


