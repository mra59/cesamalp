#ifndef __icpUSDProt__
#define __icpUSDProt__

#include "icpUSD.h"


//#pragma pack(1)


#define ICP_USD_CMD_RESET 				0
#define ICP_USD_CMD_SETUP 				1
#define ICP_USD_CMD_POLL 				2
#define ICP_USD_CMD_VEND 				3
#define ICP_USD_CMD_FUNDS 				4
#define ICP_USD_CMD_CONTROL 			5
#define ICP_USD_CMD_EXP 				7

#define ICP_USD_CMD_VENDAPPROVED 		0
#define ICP_USD_CMD_VENDDENIED 			1
#define ICP_USD_CMD_VENDSEL 			2
#define ICP_USD_CMD_VENDHOMESEL 		3
#define ICP_USD_CMD_VENDSELSTATUS		4
#define ICP_USD_CMD_FUNDSCREDIT 		0
#define ICP_USD_CMD_FUNDSSELPRICE 		1
#define ICP_USD_CMD_CONTROLDISABLE		0
#define ICP_USD_CMD_CONTROLENABLE 		1
#define ICP_USD_CMD_EXPREQUESTID 		0
#define ICP_USD_CMD_EXPENABLEOPT 		1
#define ICP_USD_CMD_EXPSENDBLOCKS 		2
#define ICP_USD_CMD_EXPSENDBLOCKN 		3
#define ICP_USD_CMD_EXPREQBLOCKN 		4
#define ICP_USD_CMD_EXPSENDBLOCK 		5
#define ICP_USD_CMD_EXPDIAG 			0xFF

#define ICP_USD_INACTIVE 				0
#define ICP_USD_DISABLED 				1
#define ICP_USD_ENABLED 				2
#define ICP_USD_VEND 					3

#if (_IcpMaster_ == true)
	#define ICP_USD_INACTIVE_TXRESET 			0
	#define ICP_USD_INACTIVE_WAITINIT 			1
	#define ICP_USD_INACTIVE_TXPOLL_1 			2
	#define ICP_USD_INACTIVE_TXPOLL_2			3

	#define ICP_USD_DISABLED_TXSETUPID 			0
	#define ICP_USD_DISABLED_WAITSETUPID 		1
	#define ICP_USD_DISABLED_TXEXPID 			2
	#define ICP_USD_DISABLED_WAITEXPID 			3
	#define ICP_USD_DISABLED_TXEXPFOPT 			4
	#define ICP_USD_DISABLED_WAITEXPFOPT 		5
	#define ICP_USD_DISABLED_TXPOLL 			6
	#define ICP_USD_DISABLED_TXCONTROLENABLE 	7
	#define ICP_USD_DISABLED_TXEXPDIAG 			8

	#define ICP_USD_ENABLED_TXPOLL 				0
	#define ICP_USD_ENABLED_TXCONTROLDISABLE 	1
	#define ICP_USD_ENABLED_TXFUNDSCREDIT 		2
	#define ICP_USD_ENABLED_TXFUNDSSELPRICE 	3
	#define ICP_USD_ENABLED_TXVENDSEL 			4
	#define ICP_USD_ENABLED_WAITVENDSEL 		5
	#define ICP_USD_ENABLED_TXVENDHOMESEL 		6
	#define ICP_USD_ENABLED_WAITVENDHOMESEL 	7
	#define ICP_USD_ENABLED_TXVENDSELSTATUS 	8
	#define ICP_USD_ENABLED_WAITVENDSELSTATUS 	9
	#define ICP_USD_ENABLED_TXVENDAPPROVED 		10
	#define ICP_USD_ENABLED_TXVENDDENIED 		11
	#define ICP_USD_ENABLED_TXEXPDIAG 			12

	#define ICP_USD_VEND_WAITVENDSUCCFAIL 		0
	#define ICP_USD_VEND_TXEXPDIAG 				1
#endif

#define ICP_USD_INACTIVE_WAITSETUP 		0
#define ICP_USD_DISABLED_WAITCMDS 		0
#define ICP_USD_ENABLED_WAITCMDS 		0
#define ICP_USD_VEND_RXVENDAPPROVED 	0
#define ICP_USD_VEND_RXVENDSEL 			1
#define ICP_USD_VEND_RXVENDHOMESEL 		2



#define ICP_USD_RESPTYPE_JUSTRESET	(ICP_USD_RESPTYPE + 1)
#define ICP_USD_RESPTYPE_VENDREQ 	(ICP_USD_RESPTYPE + 2)
#define ICP_USD_RESPTYPE_VENDSUCC 	(ICP_USD_RESPTYPE + 3)
#define ICP_USD_RESPTYPE_VENDFAILED (ICP_USD_RESPTYPE + 4)
#define ICP_USD_RESPTYPE_SETUP 		(ICP_USD_RESPTYPE + 5)
#define ICP_USD_RESPTYPE_PRICEREQ 	(ICP_USD_RESPTYPE + 6)
#define ICP_USD_RESPTYPE_ERRCODE 	(ICP_USD_RESPTYPE + 7)
#define ICP_USD_RESPTYPE_PERIPHID 	(ICP_USD_RESPTYPE + 8)
#define ICP_USD_RESPTYPE_SELSTATUS 	(ICP_USD_RESPTYPE + 9)
#define ICP_USD_RESPTYPE_BLOCKN 	(ICP_USD_RESPTYPE + 10)
#define ICP_USD_RESPTYPE_BLOCKDATA 	(ICP_USD_RESPTYPE + 11)
#define ICP_USD_RESPTYPE_EXPFF 		(ICP_USD_RESPTYPE + 12)



//-------------------------------------------------
// U S D  Info
//-------------------------------------------------

// Poll Resp Type = 0x01
typedef struct {
	uint8_t nUsdResp;
	uint8_t nSelRow;
	uint8_t nSelColumn;
	uint16_t nSelPrice;
} T_USD_VENDREQ;

// Poll Resp Type = 0x03
typedef struct {
	uint8_t nUsdResp;
	uint8_t nSelRow;
	uint8_t nSelColumn;
	uint16_t nErrCode;
} T_USD_VENDFAILED;

// Poll Resp Type = 0x04
typedef struct {
	uint8_t nUsdResp;
	uint8_t nFLevel;
	uint16_t nMaxPrice;
	uint8_t nSelRow;
	uint8_t nSelColumn;
	uint8_t nMaxTmResponse;
} T_USD_SETUP;

// Poll Resp Type = 0x05
typedef struct {
	uint8_t nUsdResp;
	uint8_t nSelRow;
	uint8_t nSelColumn;
} T_USD_PRICEREQ;

// Poll Resp Type = 0x06
typedef struct {
	uint8_t nUsdResp;
	uint16_t nErrCode;
} T_USD_ERRCODE;

// Poll Resp Type = 0x07
typedef struct {
	uint8_t nUsdResp;
	uint8_t aManufactCode[3];
	uint8_t aSerialNumber[12];
	uint8_t aModelNumber[12];
	uint16_t nSwVersion;
	uint32_t nFOptions;
} T_USD_PERIPHID;

// Poll Resp Type = 0x08
typedef struct {
	uint8_t nUsdResp;
	uint8_t nSelRow;
	uint8_t nSelColumn;
	uint16_t nSelStatus;
} T_USD_SELSTATUS;

// Poll Resp Type = 0x09
typedef struct {
	uint8_t nUsdResp;
	uint8_t nBlock;
	uint8_t aData[ICP_MAX_FRAMELEN -2];
} T_USD_EXP04;

// Poll Resp Type = 0x0A
typedef struct {
	uint8_t nUsdResp;
	uint8_t aData[ICP_MAX_FRAMELEN];
} T_USD_BLOCKDATA;

typedef struct {
	uint32_t nFOptions;
	T_USD_SETUP Setup;
	T_USD_SELSTATUS SelStatus;
#if (_IcpMaster_ == true)
	uint8_t nWaitInitCnt;
	uint8_t nCmdState;
	uint8_t nCmdType;
	uint8_t aCmdParameters[6];
#endif
	uint8_t nInfoMask;
	uint32_t	USD_CreditoInviato;					// 09.17 Variabile aggiunta per aggiornare il credito su USD in Master Mode
	uint8_t		waitingRealVendEnd;					// 09.17 In resto subito attendo la fine vendita reale
} T_ICP_USDINFO;



//-------------------------------------------------
// U S D  Command/Response Structures
//-------------------------------------------------

typedef struct {
	uint8_t nCmd;
} T_USDCMD_RESET;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_RESET;

typedef struct {
	uint8_t nCmd;
	T_VMC_SETUP Setup;
} T_USDCMD_SETUP;

// Poll Resp Type = 0x04
typedef union {
	uint8_t nodata;
	T_USD_SETUP Setup;
} T_USDRESP_SETUP;

typedef struct {
	uint8_t nCmd;
} T_USDCMD_POLL;

typedef union {
	uint8_t nodata;
	uint8_t aPollData[ICP_MAX_FRAMELEN];
} T_USDRESP_POLL;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_USDCMD_VEND00;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_VEND00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_USDCMD_VEND01;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_VEND01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nSelRow;
	uint8_t nSelColumn;
} T_USDCMD_VEND02;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_VEND02;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nSelRow;
	uint8_t nSelColumn;
} T_USDCMD_VEND03;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_VEND03;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nSelRow;
	uint8_t nSelColumn;
} T_USDCMD_VEND04;

// Poll Resp Type = 0x08
typedef union {
	uint8_t nodata;
	T_USD_SELSTATUS SelStatus;
} T_USDRESP_VEND04;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint16_t nFunds;
} T_USDCMD_FUNDS00;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_FUNDS00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nSelRow;
	uint8_t nSelColumn;
	uint16_t nSelPrice;
	uint16_t nSelID;
} T_USDCMD_FUNDS01;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_FUNDS01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_USDCMD_CONTROL00;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_CONTROL00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_USDCMD_CONTROL01;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_CONTROL01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
} T_USDCMD_EXP00;

// Poll Resp Type = 0x07
typedef union {
	uint8_t nodata;
	T_USD_PERIPHID PeriphID;
} T_USDRESP_EXP00;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint32_t nFOptions;
} T_USDCMD_EXP01;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_EXP01;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nBlock;
} T_USDCMD_EXP02;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_EXP02;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nBlock;
	uint8_t aData[ICP_MAX_FRAMELEN -3];
} T_USDCMD_EXP03;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_EXP03;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t nBlock;
} T_USDCMD_EXP04;

// Poll Resp Type = 0x09
typedef union {
	uint8_t nodata;
	T_USD_EXP04 Exp04; 
} T_USDRESP_EXP04;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t aData[ICP_MAX_FRAMELEN -2];
} T_USDCMD_EXP05;

typedef union {
	uint8_t nodata;
	uint8_t data;
} T_USDRESP_EXP05;

typedef struct {
	uint8_t nCmd;
	uint8_t nSubCmd;
	uint8_t aDiagData[ICP_MAX_FRAMELEN - 2];
} T_USDCMD_EXPFF;

typedef union {
	uint8_t nodata;
	struct {
		uint8_t nExpFF;
		uint8_t aDiagData[ICP_MAX_FRAMELEN];
	} expff;
} T_USDRESP_EXPFF;


void ICPProt_USDReset(void);
void ICPProt_USDSetup(void);
void ICPProt_USDPoll(void);
void ICPProt_USDVend00(void);
void ICPProt_USDVend01(void);
void ICPProt_USDVend02(void);
void ICPProt_USDVend03(void);
void ICPProt_USDVend04(void);
void ICPProt_USDFunds00(void);
void ICPProt_USDFunds01(void);
void ICPProt_USDControl00(void);
void ICPProt_USDControl01(void);
void ICPProt_USDExp00(void);
void ICPProt_USDExp01(void);
void ICPProt_USDExp02(void);
void ICPProt_USDExp03(void);
void ICPProt_USDExp04(void);
void ICPProt_USDExp05(void);
void ICPProt_USDExpFF(void);
#if _IcpUsdFTL_ > 0
void ICPProt_USDExpFA(void);
void ICPProt_USDExpFB(void);
void ICPProt_USDExpFC(void);
void ICPProt_USDExpFD(void);
void ICPProt_USDExpFE(void);
#endif

uint8_t ICPProt_USDResetResp(void);
uint8_t ICPProt_USDSetupResp(void);
uint8_t ICPProt_USDPollResp(void);
uint8_t ICPProt_USDVend00Resp(void);
uint8_t ICPProt_USDVend01Resp(void);
uint8_t ICPProt_USDVend02Resp(void);
uint8_t ICPProt_USDVend03Resp(void);
uint8_t ICPProt_USDVend04Resp(void);
uint8_t ICPProt_USDFunds00Resp(void);
uint8_t ICPProt_USDFunds01Resp(void);
uint8_t ICPProt_USDControl00Resp(void);
uint8_t ICPProt_USDControl01Resp(void);
uint8_t ICPProt_USDExp00Resp(void);
uint8_t ICPProt_USDExp01Resp(void);
uint8_t ICPProt_USDExp02Resp(void);
uint8_t ICPProt_USDExp03Resp(void);
uint8_t ICPProt_USDExp04Resp(void);
uint8_t ICPProt_USDExp05Resp(void);
uint8_t ICPProt_USDExpFFResp(void);
#if _IcpUsdFTL_ > 0
uint8_t ICPProt_USDExpFAResp(void);
uint8_t ICPProt_USDExpFBResp(void);
uint8_t ICPProt_USDExpFCResp(void);
uint8_t ICPProt_USDExpFDResp(void);
uint8_t ICPProt_USDExpFEResp(void);
#endif

bool USDChangeSlave(uint8_t nSlaveType);
void USDInit(uint8_t nSlaveType);
void USDInactive(void);
void USDDisabled(void);
void USDEnabled(void);
void USDVend(void);



#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
/*
typedef struct {
	uint8_t aPollData[16];
	uint8_t nRdData;
	uint8_t nWrData;
	uint8_t nData;
} T_ICP_USDPOLLDATA;

void ICPProt_USDResetPollData(void);
uint8_t ICPProt_USDGetPollData(uint8_t *ptData, bool fAllData);
*/
bool ICPProt_USDGetFTL(uint8_t slaveType);
bool ICPProt_USDGetMode1(uint8_t slaveType);
bool ICPProt_USDGetMode2(uint8_t slaveType);
bool ICPProt_USDIsFTLEnable(void);
#endif

extern T_ICP_USDINFO ICPUSD1Info, ICPUSD2Info, ICPUSD3Info;
#endif
