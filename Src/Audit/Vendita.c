/****************************************************************************************
File:
    Vendita.c

Description:
    Funzioni di generazione dell'Audit Vendita.	
    

History:
    Date       Aut  Note
    Feb 2013 	MR   

*****************************************************************************************/

#include "Orion.h"
#include "Vendita.h"
#include "AuditExt.h"
#include "Funzioni.h"
#include "IICEEP.h"
#include "I2C_EEPROM.h"
#include "Power.h"
#include "VMC_CPU_Prog.h"


#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
Global Declarations 
\*--------------------------------------------------------------------------------------*/

bool		fSelezioneUSD;

/*
static void  AuditVendSuccessConCash(uint8_t *status);
static void  AuditVendSuccessConKR(uint8_t *status);
static void  AuditSaleFree(uint8_t *status);
static void  AggiornamentoDatiVenditaConKR(void);
static void  AggiornamentoDatiVenditaConCash(void);
static void  AggiornamentoDati_VMCFreeVend(void);

	   void  UpDateDeconto(bool increm);
	   void  SetDeconto(void);
	   void  ClrDeconto(void);
	   void  CheckDeconto(void);
	   void  ResetDatiVendita(void);
*/

/*
 * Variabili esterne
 */
extern  uint8_t  CVHCoinsHandlerState;          // Coins Handler State 
extern  uint8_t  BVHBillsHandlerState;          // Bank Reader Handler State 



/*--------------------------------------------------------------------------------------*\
Method: AggiornaNumSel
	Aggiorna i campi battute LR per vendite a prezzo intero o scontate.
	*** Funzione chiamata sia per vendite con Cash che a chiave *******
	I contatori NumSelDiscount1,4 saranno sommati in PA2-03 dalla AuditStrutture, e 
	inviati singolarmente in LA1-1,4.
	La flag LA1_Digisoft serve a far conteggiare al sistema le vendite a chiave a prezzo pieno
	come vendite nelle liste LA1-1,4 come se fossero scontate.
	Fino ad oggi (30.07.15) la flag non servirebbe perche' nel PK3 le vendite a chiave arrivano
	qui sempre con NumLista da 1 a 4, quindi non c'e' il caso di vendita a chiave con prezzo
	intero e NumLista = 0,
	
Parameters:
	IN	- Sconto, NumLista e Tipo di Vendita
	OUT	- PSHProcessingData() set
\*--------------------------------------------------------------------------------------*/
void  AggiornaNumSel(uint sconto) {
	
	if ((TipoVendita == VEND_CON_KR || TipoVendita == VEND_CON_KR_PROVA) && (gOrionConfVars.LA1_Digisoft)) {
		// Vendita a Chiave con Audit Vega Digisoft
		switch (NumLista) {
	      case 0 :
	      case 1 :
	    	  AuditUintInc(IICEEPAuditAddr(NumSelDiscount1[NumeroSelezione - 1]));				// LA1-1
	    	  break;
	      case 2 :
	    	  AuditUintInc(IICEEPAuditAddr(NumSelDiscount2[NumeroSelezione - 1]));				// LA1-2
	    	  break;
	      case 3 :  
	          AuditUintInc(IICEEPAuditAddr(NumSelDiscount3[NumeroSelezione - 1]));				// LA1-3
	          break;
	      case 4 :
	          AuditUintInc(IICEEPAuditAddr(NumSelDiscount4[NumeroSelezione - 1]));				// LA1-4
	    }
	} else {
		// ---------------------------------------------------------------------------------------------
		// Se sconto e NumLista sono zero, e' vendita con cash: update quindi LA1-0.
		// NumLista e' sempre >0 per vendite a chiave per cui si aggiorna la lista di appartenenza
		// e, in effetti, la flag Dgisoft e' ininfluente.
		// ---------------------------------------------------------------------------------------------
		if (sconto == 0 && NumLista == 0) {														// Sconto e NumLista zero indicano vendita con cash: update LA1-0
			AuditUintInc(IICEEPAuditAddr(NumSelFullPrice[NumeroSelezione - 1]));				// Numero Battute LR senza sconto (PA203 e LA1-0)
		} else {
			switch (NumLista) {
		      case 1 :
		    	  AuditUintInc(IICEEPAuditAddr(NumSelDiscount1[NumeroSelezione - 1]));			// LA1-1
		    	  break;
		      case 2 :
		    	  AuditUintInc(IICEEPAuditAddr(NumSelDiscount2[NumeroSelezione - 1]));			// LA1-2
		    	  break;
		      case 3 :  
		          AuditUintInc(IICEEPAuditAddr(NumSelDiscount3[NumeroSelezione - 1]));			// LA1-3
		          break;
		      case 4 :
		          AuditUintInc(IICEEPAuditAddr(NumSelDiscount4[NumeroSelezione - 1]));			// LA1-4
		    }
		}
	}
}

/**********************************************************/
/*  Reset dati Vendita                                    */
/**********************************************************/

void  ResetDatiVendita(void) {

	PSHProcessingData();
	VendAmountKR 	= NULL_VALUE;            	// Valore vendita effettuata con Key Reader 
	VendAmountBR	= NULL_VALUE;               // Valore vendita effettuata con Cash da Bank Reader 
	VendAmountSEL 	= NULL_VALUE;               // Valore vendita effettuata con Cash da Selettore 
	VendAmountRNST 	= NULL_VALUE;               // Valore vendita effettuata con Reinstate 
	ValoreSelezione = NULL_VALUE;               // Valore selezione richiesta da VMC
	CreditUsedKR 	= NULL_VALUE;               // Valore Credit usato per vendita 
	FreeCreditUsedKR = NULL_VALUE;           	// Valore Free Credit usato per vendita 
	NewCreditKR 	= NULL_VALUE;               // Valore Credito aggiornato chiave 
	CodiceUtenteKR 	= NULL_VALUE;             	// Codice Utente 
	NewFreeCreditKR = NULL_VALUE;               // Valore Free Credit aggiornato chiave 
	NewFreeVendKR 	= NULL_VALUE;               // Numero delle Free Vend aggiornato chiave 
	FreeVendLevel 	= NULL_VALUE;
	StatusGestioneDatiVendita1 = 0;             // Stato gestione Dati Vendita 
	StatusGestioneDatiVendita2 = 0;             // Stato gestione Dati Vendita 
	StatusGestioneDatiVendita3 = 0;             // Stato gestione Dati Vendita 
	StatusVendita = VEND_NULL;                  // Stato Vendita in corso 
	TipoVendita = NO_VEND_IN_CORSO;             // Tipo Vendita in corso 
	NumLista = 0;
	PSHCheckPowerDown();
	fSelezioneUSD = false;
}


/*--------------------------------------------------------------------------------------*\
Method:	AuditSaleFree
	Aggiorna Audit Vendita gratuita. 
	
Parameters:
	IN	- StatusGestioneDatiVendita3
	OUT - PSHProcessingData() set
\*--------------------------------------------------------------------------------------*/
static void  AuditSaleFree(uint8_t *status) {

  do {
		switch(*status) {

			case 1 :
				// 30.07.15 L'EVA-DTS dice di non registrare le FreeVend nel PA2.
				// Registro solo l'evento EK_01 nel caso la selezione richiesta,
				// pagata con una FreeVend, non era nella lista prezzi
				if (NumeroSelezione == 0) { 
					AuditUintInc(IICEEPAuditAddr(LREvent1));                                    					// "EA2*EK_01" Prezzo Selezione Sconosciuto
				}
/* Tolta il 30.07.15 per essere compliance con EVA-DTS				
				if (NumeroSelezione == 0) { 
					AuditUintInc(IICEEPAuditAddr(LREvent1));                                    					// "EA2*EK_01" Prezzo Selezione Sconosciuto
				} else {
					AuditUintInc(IICEEPAuditAddr(NumSelDisc[NumeroSelezione - 1]));             					// PA203 Numero Battute LR
				}
*/
				*status = *status +1;
				PSHCheckPowerDown();

			case 2 :
				AuditUintInc(IICEEPAuditAddr(LRFreeNum));                                     						// VA304 
				*status = *status +1;
				PSHCheckPowerDown();

			case 3 :
				AuditUlongAdd(IICEEPAuditAddr(LRFreeVnd), ValoreSelezione);                   						// VA303 
				*status = *status +1;
				PSHCheckPowerDown();

			case 4 :
				AuditUlongInc(IICEEPAuditAddr(INFreeNum));                                							// VA302 
				// Quando selezione eseguita da questo DA (fSelezioneUSD=0)
				// incremento le battute costruttore.
				// Se fSelezioneUSD=1 la selezione e' stata fatta da un DA esterno
				if (fSelezioneUSD == false) {
					AuditUlongInc(BattuteCostrutt(VMCTotBattute));													// Totale Battute per Costruttore
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 5 :
				AuditUlongAdd(IICEEPAuditAddr(INFreeVnd), ValoreSelezione);                  						// VA301 
				*status = 0;

			default:
				PSHProcessingData();
				*status = 0;
				Clear_fVMCFreeVendRequired;
		}
	} while (*status != 0);
}


/*--------------------------------------------------------------------------------------*\
Method:	AuditVendSuccessConKR
	Aggiorna Audit Vendita OK con chiave. 
	
Parameters:
	IN	- StatusGestioneDatiVendita2
	OUT - PSHProcessingData() set
\*--------------------------------------------------------------------------------------*/
void  AuditVendSuccessConKR(uint8_t *status) {
	
	uint  sconto;

	if (FreeVendLevel != 0) {
		// *****   FREE   VEND    ******
		do {
			switch(*status) {
				case 1 :
					PSHProcessingData();
					StatusGestioneDatiVendita3 = 1;
					*status = *status +1;
					PSHCheckPowerDown();

				case 2 :
					AuditSaleFree(&StatusGestioneDatiVendita3);									// Aggiorna solo VA3 FreeVend (eventualmente anche EK_01 Selprice Sconosciuto)
					*status = *status +1;
					PSHCheckPowerDown();

				default:
					PSHProcessingData();
					*status = 0;
			}
		} while (*status != 0);
	} else {
		// *****   VENDITA    PAGATA   ******
		sconto = 0; 																			// ****** VENDITA   PAGATA   *******
		if (ValoreSelezione > VendAmountKR) sconto = (ValoreSelezione - VendAmountKR);			// Determino eventuale valore dello sconto
		do {
			switch(*status) {

				case 1 :
					if ((TipoVendita == VEND_CON_KR_PROVA) || (TipoVendita == VEND_DA_IN_GRATUITO)) {
						*status = *status +1;
					} else {
						*status = 6;
					}
					break;

				// *****  Audit Vend con chiave di Test  ******
				case 2 :																		// Lo standard richiede di aggiornare solo VA2
					AuditUintInc(IICEEPAuditAddr(LRTestNum));                                   // VA204 
					*status = *status +1;
					PSHCheckPowerDown();

				case 3 :
					AuditUlongAdd(IICEEPAuditAddr(LRTestVnd), ValoreSelezione);                 // VA203 
					*status = *status +1;
					PSHCheckPowerDown();

				case 4 :
					AuditUintInc(IICEEPAuditAddr(INTestNum));                                   // VA202 
					*status = *status +1;
					PSHCheckPowerDown();

				case 5 :
					AuditUlongAdd(IICEEPAuditAddr(INTestVnd), ValoreSelezione);					// VA201 
					AuditUlongInc(BattuteCostrutt(VMCTotBattute));								// Totale Battute per Costruttore
					*status = 0;																// Fine update Vendite di Test
					break;
				// ********    Fine update Vendite di Test  *******
				// ===============================================================================
					
				// *****  Audit Vend con chiave Utente  ******
				case 6 :
					if (sconto != 0) {
						*status = *status +1;
					} else {
						*status = 11;
					}
					break;

				case 7 :
					if (NumeroSelezione != 0) {
						AuditUintInc(IICEEPAuditAddr(NumSelDisc[NumeroSelezione - 1]));			// PA203 Numero Battute LR 
					}
					*status = *status +1;
					PSHCheckPowerDown();

				case 8 :
					AuditUintInc(IICEEPAuditAddr(NumCardDis));									// DA502 
					*status = *status +1;
					PSHCheckPowerDown();

				case 9 :
					AuditUlongAdd(IICEEPAuditAddr(LRCardDis), sconto);							// DA501 
					*status = *status +1;
					PSHCheckPowerDown();

				case 10 :
					AuditUlongAdd(IICEEPAuditAddr(INCardDis), sconto);							// DA503 
					*status = *status +1;
					PSHCheckPowerDown();
					break;

				case 11 :
					if (NumeroSelezione != 0) { 
						AggiornaNumSel(sconto);                         						// Aggiorna numero battute LR a prezzo pieno o scontate (PA203 e LA1)
					}
					*status = *status +1;
					PSHCheckPowerDown();

				case 12 :
					if (NumeroSelezione != 0) { 
						AuditUlongAdd(IICEEPAuditAddr(LRSelVnd[NumeroSelezione - 1]), VendAmountKR);  		// PA204 
					}
#if kApplVendCountersSupport
					if ( NumeroSelezione <= MRPPricesListLenght) {
						AuditUintInc(IICEEPAuditAddr(INCardNumSel[NumeroSelezione - 1]));
					}
#endif
					*status = *status +1;
					PSHCheckPowerDown();

					case 13 :
						AuditUintInc(IICEEPAuditAddr(LRCardNum));								// DA204 Number of Cashless 1 Sales LR
						*status = *status +1;
						PSHCheckPowerDown();

					case 14 :
						AuditUintInc(IICEEPAuditAddr(INCardNum));								// DA202 Number of Cashless 1 Sales IN
						// Quando selezione eseguita da questo DA (fSelezioneUSD=0)
						// incremento le battute costruttore.
						// Se fSelezioneUSD=1 la selezione e' stata fatta da un DA esterno
						if (fSelezioneUSD == false) {
							AuditUlongInc(BattuteCostrutt(VMCTotBattute));						// Incremento Battute Costruttore
						}
						*status = *status +1;
						PSHCheckPowerDown();

					case 15 :
						AuditUlongAdd(IICEEPAuditAddr(LRCardVnd), ValoreSelezione);				// DA203 Value of Cashless 1 Sales LR
						*status = *status +1;
						PSHCheckPowerDown();

					case 16 :
						AuditUlongAdd(IICEEPAuditAddr(INCardVnd), ValoreSelezione);				// DA201 Value of Cashless 1 Sales IN
						*status = *status +1;
						PSHCheckPowerDown();

					case 17 :
						AuditUlongAdd(IICEEPAuditAddr(LRCardDeb), VendAmountKR);				// DA302 Value debited from the Cashless 1 LR
						*status = *status +1;
						PSHCheckPowerDown();

					case 18 :
						AuditUlongAdd(IICEEPAuditAddr(INCardDeb), VendAmountKR);				// DA301 Value debited from the Cashless 1 IN
						*status = *status +1;
						PSHCheckPowerDown();

					case 19 :
						if (NumeroSelezione == 0) { 
							AuditUintInc(IICEEPAuditAddr(LREvent1));							// "EA2*EK_01" Selprice Sconosciuto
							*status = *status +1;
							PSHCheckPowerDown();
						} else {
							PSHProcessingData();
							*status = 0;
						}
						break;

					case 20 :
						AuditUintInc(IICEEPAuditAddr(INEvent1));								// "EA2*EK_01" Selprice Sconosciuto
						*status = *status +1;
						PSHCheckPowerDown();

					case 21 :
						AuditUlongAdd(IICEEPAuditAddr(LREv1Val), VendAmountKR);					// "EA2*EK_01" Selprice Sconosciuto

					default:
						PSHProcessingData();
						*status = 0;
			}
		} while (*status);
	}
}                                          
                                                                           

/*--------------------------------------------------------------------------------------*\
Method:	AuditVendSuccessConCash
	Aggiorna Audit Vendita OK con cash. 
	
Parameters:
	IN	- StatusGestioneDatiVendita2
	OUT - PSHProcessingData() set
\*--------------------------------------------------------------------------------------*/
void  AuditVendSuccessConCash(uint8_t *status) {

	uint8_t	ExactChange;
	uint  	sconto, maggiorazione;

	sconto = 0;
	maggiorazione = 0;
//	ExactChange= DeterminaExactChange(MinCoinsTube);											// Determina stato di Exact change
	ExactChange = Ex_Ch_InCashVend;																// 29.06.15 Memorizzata in backupram lo stato di Ex-ch in cash vend
	// Determino sconto o maggiorazione
	if (ValoreSelezione > (VendAmountSEL + VendAmountBR)) {
		sconto = (ValoreSelezione - (VendAmountSEL + VendAmountBR)); 
	} else {
		if (ValoreSelezione  < (VendAmountSEL + VendAmountBR)) {
		    maggiorazione = ((VendAmountSEL + VendAmountBR) - ValoreSelezione); 
		}
	}
	do {
		switch (*status) {

			case 1 :
				if (NumeroSelezione == 0) {
					AuditUintInc(IICEEPAuditAddr(LREvent1));									// "EA2*EK_01" Selprice Sconosciuto
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 2 :
				if (NumeroSelezione == 0) {
					AuditUlongInc(IICEEPAuditAddr(INEvent1));									// "EA2*EK_01" Selprice Sconosciuto 
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 3 :
				if (NumeroSelezione == 0) {
					AuditUlongAdd(IICEEPAuditAddr(LREv1Val), ValoreSelezione);					// "EA2*EK_01" Selprice sconosciuto
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 4 :
				if (NumeroSelezione != 0) {
					AggiornaNumSel(sconto);                         							// Aggiorna numero battute LR a prezzo pieno o scontate (PA2-03 e LA1)
#if kApplVendCountersSupport
				if (NumeroSelezione <= MRPPricesListLenght) {
					AuditUintInc(IICEEPAuditAddr(VendCounters[NumeroSelezione - 1]));               
				}
#endif
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 5 :
				if (NumeroSelezione != 0) {
					AuditUlongAdd(IICEEPAuditAddr(LRSelVnd[NumeroSelezione - 1]), (VendAmountSEL + VendAmountBR));		// PA204 
#if kApplVendCountersSupport
					if( NumeroSelezione <= MRPPricesListLenght) {
						AuditUintInc(IICEEPAuditAddr(INCashNumSel[NumeroSelezione - 1]));
					}
#endif
				}
				// Quando selezione eseguita da questo DA (fSelezioneUSD=0)
				// incremento le battute costruttore.
				// Se fSelezioneUSD=1 la selezione e' stata fatta da un DA esterno
				if (fSelezioneUSD == false) {
					AuditUlongInc(BattuteCostrutt(VMCTotBattute));								// Totale Battute per Costruttore
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 6 :
				if (ExactChange) {
					AuditUlongAdd(IICEEPAuditAddr(LRVndExc), ValoreSelezione);                  // CA901 
				}
				AuditUlongAdd(IICEEPAuditAddr(LRCashVnd), ValoreSelezione);                   	// CA203  Sempre aggiornato anche se in Exact-change
				*status = *status +1;
				PSHCheckPowerDown();

			case 7 :
				if (ExactChange) {
					AuditUlongAdd(IICEEPAuditAddr(INVndExc), ValoreSelezione);					// CA902 
				}
				AuditUlongAdd(IICEEPAuditAddr(INCashVnd), gOrionKeyVars.VendReq);				// CA201  Sempre aggiornato anche se in Exact-change
				*status = *status +1;
				PSHCheckPowerDown();

			case 8 :
				AuditUintInc(IICEEPAuditAddr(LRCashNum));                                       // CA204 
				*status = *status +1;
				PSHCheckPowerDown();

			case 9 :
				AuditUlongInc(IICEEPAuditAddr(INCashNum));                                  	// CA202 
				if (TipoVendita == VEND_DA_IN_EXEC) {											// In Execuitve termino qui: aggiornati numero tot battute, tot vendite,..
					*status = 0;																// ..battute costruttore e PA204.
					break;
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 10 :
				if (sconto != 0) {
					AuditUlongAdd(IICEEPAuditAddr(LRCashDis), sconto);							// CA701 
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 11 :
				if (sconto != 0) {
					AuditUlongAdd(IICEEPAuditAddr(INCashDis), sconto);							// CA702 
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 12 :
				if (maggiorazione != 0) {
					AuditUintInc(IICEEPAuditAddr(LREvent4));									// EA202_4 
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 13 :
				if (maggiorazione != 0) {
					AuditUlongInc(IICEEPAuditAddr(INEvent4));									// EA203_4 
				}
				*status = *status +1;
				PSHCheckPowerDown();

			case 14 :
				if (maggiorazione != 0) {
					AuditUlongAdd(IICEEPAuditAddr(LREv4Val), maggiorazione);					// EA204_4 
				}
#if !kApplTokenAudit
				*status = 0;
#else
				if (CGMngrHookTokenVend()) {													// Vendita con Token
					*status = *status +1;
					PSHCheckPowerDown();
				} else {
					*status = 0;                                 								// Termina se non � stato inserito un gettone
					break; 																		// Non aggiungere PSHCheckPowerDown perche' deve uscire con PSHProcessingData
				}

			case 15:
				AuditUintInc(IICEEPAuditAddr(LRNumSelecToken));   								// TA206 
				*status = *status +1;
				PSHCheckPowerDown();

			case 16:
				AuditUintInc(IICEEPAuditAddr(INNumSelecToken));   								// TA208 
				*status = *status +1;
				PSHCheckPowerDown();
    
			case 17:
				AuditUlongAdd(IICEEPAuditAddr(LRTokenVend), ValoreSelezione); 					// TA205 
				*status = *status +1;
				PSHCheckPowerDown();

			case 18:
				AuditUlongAdd(IICEEPAuditAddr(INTokenVend), ValoreSelezione); 					// TA207 
				CGMngrHookTokenVendSet(false);
				*status = 0;
				PSHCheckPowerDown();
#endif  //!kApplTokenAudit

			default:
				*status = 0;
		}
	} while(*status != 0);
}                                          
                                                                           

/*--------------------------------------------------------------------------------------*\
Method:	AggiornamentoDatiVenditaConKR
	Aggiorna Audit Vendita con chiave in base all'esito della vendita: 
	VEND_SUCCESS, VEND_FAILURE, VEND_CardWriteErr.
	
Parameters:
	IN	- StatusVendita, StatusGestioneDatiVendita1
	OUT - 
\*--------------------------------------------------------------------------------------*/
static void  AggiornamentoDatiVenditaConKR(void) {

	switch (StatusVendita) {

    	case VEND_SUCCESS :
    		switch (StatusGestioneDatiVendita1) {

    			case 1 :
    				StatusGestioneDatiVendita2 = 1;												// Non e' necessario controllare il PowerDown
    				StatusGestioneDatiVendita1 ++;

    			case 2 :
    				AuditVendSuccessConKR(&StatusGestioneDatiVendita2);
    				StatusGestioneDatiVendita1 ++;
    				PSHCheckPowerDown();

    			case 3 :
    				StatusGestioneDatiVendita2 = 1;												// Non e' necessario controllare il PowerDown
    				StatusGestioneDatiVendita1 ++;

    			case 4 :
    				AuditExtVendConKR(false);													// False per registrare vendita OK nel Record N. 2

    			default:
    				ResetDatiVendita();
    		}
    	break;

    	case VEND_FAILURE :
			AuditExtVendConKR(true);															// True per registrare FAIL Vend nel Record N. 10
			ResetDatiVendita();
			break;
			
    	case VEND_CardWriteErr :																// Errore di scrittura carta per una vend request
			AuditExtCardWriteErrVendReq();														// Audit Estesa Record N. 11

    	default:
    		ResetDatiVendita();
	}
}


/*--------------------------------------------------------------------------------------*\
Method:	AggiornamentoDatiVenditaConCash
	Aggiorna Audit Vendita con cash in base all'esito della vendita: 
	VEND_SUCCESS, VEND_FAILURE, VEND_CardWriteErr.
	
Parameters:
	IN	- StatusVendita, StatusGestioneDatiVendita1
	OUT - 
\*--------------------------------------------------------------------------------------*/
static void  AggiornamentoDatiVenditaConCash(void) {
	
	switch(StatusVendita) {

    	case VEND_SUCCESS :

    		switch(StatusGestioneDatiVendita1) {

    			case 1 :
    				StatusGestioneDatiVendita2 = 1;												// Non e' necessario controllare il PowerDown
    				StatusGestioneDatiVendita1++;

    			case 2 :
    				AuditVendSuccessConCash(&StatusGestioneDatiVendita2);
    				if (TipoVendita == VEND_DA_IN_EXEC) {										// Non registro Audit Estesa da vendite in Executive
            			ResetDatiVendita();
            			break;
    				}
    				StatusGestioneDatiVendita1++;
        		
    			case 3 :
        			AuditExtVendSuccessConCash();												// Audit Estesa Record N. 1

        		default:
        			ResetDatiVendita();
    		}
    		break;

    	case VEND_FAILURE :
       		AuditExtVendFailConCash();															// Audit Estesa Record N. 9

    	default:
    		ResetDatiVendita();
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	AggiornamentoDati_VMCFreeVend
	Aggiorna Audit Vendita gratuita richiesta dal VMC (fVMCFreeVendRequired)

Parameters:
	IN	- StatusVendita, StatusGestioneDatiVendita1
	OUT - 
\*--------------------------------------------------------------------------------------*/
static void  AggiornamentoDati_VMCFreeVend(void) {

	switch(StatusVendita) {

		case VEND_SUCCESS:
			
			switch(StatusGestioneDatiVendita1) {
				case 1:
					PSHProcessingData();
					StatusGestioneDatiVendita2 = 2;												// Salto update "EA2*001 Selez non valide" in AuditSaleFree
					StatusGestioneDatiVendita1 ++;
					PSHCheckPowerDown();

				case 2:
					AuditSaleFree(&StatusGestioneDatiVendita2);									// Aggiorna solo VA3 FreeVend
					StatusGestioneDatiVendita1 ++;

				default:
					ResetDatiVendita();
			}
		
		default:
			ResetDatiVendita();
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	AggiornamentoDati_DA_In_Gratuito
	Aggiorna solo Audit Vendite di Test, no Audit Estesa.
	Aggiorna anche battute costruttore.
	
Parameters:
	IN	- StatusVendita, StatusGestioneDatiVendita1
	OUT - 
\*--------------------------------------------------------------------------------------*/
static void  AggiornamentoDati_DA_In_Gratuito(void) {

	switch (StatusVendita) {

    	case VEND_SUCCESS :
    		switch (StatusGestioneDatiVendita1) {

    			case 1 :
    				StatusGestioneDatiVendita2 = 1;												// Non e' necessario controllare il PowerDown
    				StatusGestioneDatiVendita1 ++;

    			case 2 :
					AuditUlongInc(BattuteCostrutt(VMCTotBattute));								// Totale Battute per Costruttore
    				StatusGestioneDatiVendita1 ++;
    				PSHCheckPowerDown();

    			default:
    				ResetDatiVendita();
    		}
    	break;

    	case VEND_FAILURE :
			ResetDatiVendita();
			break;
			
    	default:
    		ResetDatiVendita();
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	AggiornamentoDati_Solo_Costruttore
	Aggiorna solo battute costruttore.
	Funzione chimamata quando il DA e' in Executive.
	
Parameters:
	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
static void  AggiornamentoDati_Solo_Costruttore(void) {

	if (StatusVendita == VEND_SUCCESS) {
		AuditUlongInc(BattuteCostrutt(VMCTotBattute));											// Totale Battute per Costruttore
		PSHCheckPowerDown();
	}
	ResetDatiVendita();
}

/*--------------------------------------------------------------------------------------*\
Method:	AggiornamentoDatiVendita
	Aggiorna Audit Vendita in base al tipo di vendita effettuata.

Parameters:
	IN	- TipoVendita, StatusGestioneDatiVendita1
	OUT - 
\*--------------------------------------------------------------------------------------*/
void AggiornamentoDatiVendita(void) {

	switch(TipoVendita) {

		case VEND_CON_KR :
		case VEND_CON_KR_PROVA :
			AggiornamentoDatiVenditaConKR();
			break;

		case VEND_CON_CASH :
		case VEND_DA_IN_EXEC:
			AggiornamentoDatiVenditaConCash();
			break;

		case VEND_SALE_FREE :
			AggiornamentoDati_VMCFreeVend();
			break;

		case VEND_DA_IN_GRATUITO :
			AggiornamentoDati_DA_In_Gratuito();
			break;

		case VEND_DA_AS_USD_SLAVE:
			AggiornamentoDati_Solo_Costruttore();
			break;

		default:
			TipoVendita = NO_VEND_IN_CORSO;
	}
	StatusGestioneDatiVendita1 = 0;
}

/*--------------------------------------------------------------------------------------*\
Method:	GestioneAuditVendita
	Controlla se e' stata effettuata una vendita per generare la relativa Audit.

Parameters:
	IN	- StatusVendita
	OUT - 
\*--------------------------------------------------------------------------------------*/
void  GestioneAuditVendita(void) {

	if (TipoVendita != NO_VEND_IN_CORSO) {
		if (StatusVendita == VEND_SUCCESS || StatusVendita == VEND_FAILURE || StatusVendita == VEND_CardWriteErr) {					// 08.04.15 L'ultimo test previsto per Aud ext
			StatusGestioneDatiVendita1 = 1;
			AggiornamentoDatiVendita();
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	SetDeconto
	Se funzione Deconto abilitata, predispone la flag Selezione in corso per decrementare
	il counter al termine della selezione OK.
	Chiamata dalla Gestione Selezioni prima della selezione.

	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
void  SetDeconto(void)
{
/*	
	if (gVMC_ConfVars.FunzDeconto == true) {
		DecontoState |= DecontoSelInProgress;
	}
*/
}

/*--------------------------------------------------------------------------------------*\
Method:	ClrDeconto
	Chiamata alla pressione del Tasto Reset su Touch o in programmazione LCD:
	si ricarica il contatore con il valore programmato. 
	Lo scopo e' quello di effettuare un intervento tecnico quando si scende sotto la
	riserva.

	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
void  ClrDeconto(void) {
	
	if (gVMC_ConfVars.FunzDeconto == true) {
		DecontoState = false;
		Clr_VMC_DecontoOver;
		ReadEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[ValMaxDeconto]), (uint8_t *)(&DecontoVal), 2U);
		WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[ActualDeconto]), (uint8_t *)(&DecontoVal), 2U);
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	CheckDeconto
	Usato per determinare la vita dei filtri.
	Controlla il valore dei counters e determina lo stato delle flag di inibizione 
	selezione.
	In presenza di Gateway invia le flag alla dashboard.

	IN	- 
	OUT - 
\*--------------------------------------------------------------------------------------*/
void  CheckDeconto(void)
{
	if (gVMC_ConfVars.FunzDeconto == true)
	{
		// --- Counter Filtro  1  ---
		if (gVMC_ConfVars.CntDecontoUP == false)
		{
			if (DecontoVal_1 == 0) Set_VMC_Deconto_1_Over;
		}
		else			
		{	
			if (DecontoVal_1 == 0) Set_VMC_Deconto_1_Over;
		}

	
		if (gVMC_ConfVars.DecontoOverInhSel == true)
		{
			Clr_VMC_DecontoOver;
			Clr_VMC_Deconto_1_Over;
			Clr_VMC_Deconto_2_Over;
			Clr_VMC_Deconto_3_Over;
			Clr_VMC_Deconto_4_Over;
			Filtro1_Riserva = false;
			Filtro2_Riserva = false;
			Filtro3_Riserva = false;
			Filtro4_Riserva = false;
		}	
	}
	else
	{
		// -- Deconto NON abilitato ---
		Clr_VMC_DecontoOver;
		Clr_VMC_Deconto_1_Over;
		Clr_VMC_Deconto_2_Over;
		Clr_VMC_Deconto_3_Over;
		Clr_VMC_Deconto_4_Over;
		Filtro1_Riserva = false;
		Filtro2_Riserva = false;
		Filtro3_Riserva = false;
		Filtro4_Riserva = false;
		DecontoState = false;
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	UpDateDeconto
	Utilizzo il Deconto come counters usura filtri.
	Se funzione Deconto abilitata, decrementa o incrementa il relativo counter e lo 
	compara con la riserva per determinarne lo stato.
	Al passaggio sotto la riserva, predispone l'invio remoto del warning.
	Al raggiungimento dello zero predispone l'invio remoto dello zero.

	IN	- Esito selezione: Vend_OK = true, Vend_FAIL = false
	OUT - 
\*--------------------------------------------------------------------------------------*/
void  UpDateDeconto(uint8_t NumSelez)
{
	if (gVMC_ConfVars.FunzDeconto == true)
	{
		//DecontoState &= ~DecontoSelInProgress;																// Non usato in Ecoline
		switch(NumSelez)
		{
			case Selez_01:
				if (gVMC_ConfVars.CntDecontoUP == false)
				{
					if (DecontoVal_1 > 0) DecontoVal_1--;
				}
				else
				{
					if (DecontoVal_1 < 0xffff) DecontoVal_1++;
				}
				WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[ActualDeconto_1]), (byte *)(&DecontoVal_1), 2U);
				break;
			
			case Selez_02:
				if (gVMC_ConfVars.CntDecontoUP == false)
				{
					if (DecontoVal_2 > 0) DecontoVal_2--;
				}
				else
				{
					if (DecontoVal_2 < 0xffff) DecontoVal_2++;
				}
				WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[ActualDeconto_2]), (byte *)(&DecontoVal_2), 2U);
				break;
			
			case Selez_03:
				if (gVMC_ConfVars.CntDecontoUP == false)
				{
					if (DecontoVal_3 > 0) DecontoVal_3--;
				}
				else
				{
					if (DecontoVal_3 < 0xffff) DecontoVal_3++;
				}
				WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[ActualDeconto_3]), (byte *)(&DecontoVal_3), 2U);
				break;

			case Selez_04:
				if (gVMC_ConfVars.CntDecontoUP == false)
				{
					if (DecontoVal_4 > 0) DecontoVal_4--;
				}
				else
				{
					if (DecontoVal_4 < 0xffff) DecontoVal_4++;
				}
				WriteEEPI2C(EEPVMCParam16Addr(EE_ParamMisc[ActualDeconto_4]), (byte *)(&DecontoVal_4), 2U);
				break;
		}
		CheckDeconto();
	}
}
	
	
