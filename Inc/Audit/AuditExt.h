/******************************/
/*                            */
/*    AuditExt.h              */
/*                            */
/******************************/


// Funzioni registrate nell'Audit Estesa
typedef enum {
	kReserved					= 0,			// 0
	kVendCash					= 1,			// 1	Vendita con Cash
	kVendCard					= 2,			// 2	Vendita con credito cash della carta
	kVendFree					= 3,			// 3	Vendita con Free Vend
	kRevalueCash				= 4,			// 4	Ricarica carta
	kFreeCredit					= 5,			// 5	Free Credit
	kRefundImmediate			= 6,			// 6	Refund immediato da Fail Vend (carta ancora inserita alla fail vend)
	kRefundDelayed				= 7,			// 7	Refund successivo al Fail Vend (carta non piu' nel lettore alla fail vend)
	kVendTest					= 8,			// 8	Test Vend
	kVendFailCash				= 9,			// 9	Fail Vend con Cash
	kVendFailCard				= 10,			// 10	Fail Vend con Card
	kCardWriteErrVendReq		= 11,			// 11	Card write Err at Vend Request
	kRevalueCartaCaricBlocchi	= 12,			// 12	Ricarica con Carta Caricamento a Blocchi
	kCardWriteErrRevalue		= 13,			// 13	Card write Err durante un revalue (sia cash che caricamento a blocchi)
	kKeyVendWriteError			= 14,			// 14	Errore scrittura Carta durante la Vend Request
	kKeyInserted				= 15			// 15	Inserimento Carta nel lettore
} AuditExtTypes;



void 	AuditExtVendSuccessConCash(void);
void 	AuditExtVendConKR(uint8_t esitovend);
void 	AuditExtRefundKR(byte TipoRefund);
void 	AuditExtRevalueKRCash(uint16_t CashRicaricato);
void 	AuditExtFreeCredit(void);
void 	AuditExtSendData(void);
void	AuditExtClearData(bool);
void 	AuditExtInitData(void);
void  	AuditExtVendFailConCash(void);
void  	AuditExtVendFailConKR(byte);
void  	AuditExtCardWriteErrVendReq(void);
void  	AuditExtRevalueCartaCaricamentoBlocchi(uint16_t CashRicaricato);

uint16_t 	AuditExtNumBlocks(void);
void 	ReadExtAudDataBlock(uint16_t, uint16_t, char*);

void  	SetExtAuditDataTimeCurrent(void*);
void  	SetExtAuditDataTimeNew(void*, uint8_t);
void	SetExtAuditDataTimeReceived(void*);

void 	TestAuditExtFullStatus(void);
bool  	Check_ExtAuditFull(void);
void  	SetBlockTypeZero(void);
void  	AuditExtCardWriteFail(uint8_t RevalType, uint16_t cashdaricaricare);

