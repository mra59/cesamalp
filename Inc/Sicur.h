/****************************************************************************************
File:
    Sicur.h

Description:
    Include file con definizioni e strutture per le funzioni di sicurezza

History:
    Date       Aut  Note
    Ott 2016	MR   

 *****************************************************************************************/

#ifndef __Sicur_h
#define __Sicur_h


bool  Test_uP(uint8_t *codici);

#if CALC_SICUR_CODES
	void cifra(void);
#endif

// ============================================
// =====  Define High Byte UID  uP        =====
// ============================================

#define	FIXED_CODE			0x59659294								// Nell'STM32 l'UID e' di 96 bit: aggiungo 32 bit per averne 128 come il K20

// ---------------------------------------------

#endif		// __Sicur_h
