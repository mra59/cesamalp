/* ***************************************************************************************
File:
    PN512_utility.c

Description:
    Funzioni dedicate alla gestione del PN512 e alla sua inizializzazione
    

History:
    Date       Aut  Note
    Gen 2013 	abe   

 *****************************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include "ORION.H"
#include <phcsBflStatus.h>
#include <phcsBflRefDefs.h>
#include <phcsBflHw1Reg.h>
#include "MifareReaderLib.h"
#include "PN512_utility.h"
#include <phcsBflRegCtl.h>

#if  Debug_Reg_18h
	#include "IICEEP.h"
#endif

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void  MIFARE_ChangeBaudRate(uint32_t BaudRate);
extern	void  MIFARE_ON(void);
extern	void  MIFARE_OFF(void);


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

uint8_t	lamp;


/*------------------------------------------------------------------------------------------
	PN512init reset HW PN512 con off/on alimentazione, controlla HW versione del PN 
	e cambia le velocit� delle seriali Kinetis e PN512
		parametri in ingresso: 
		speed (velocit� seriale)
		mask (out pin port da abilitare)
		
		parametri in uscita:
		status (da gestire eventuali errori)
	
		AZIONI:
		set seriale Kinetis a 9600 baud 
		Spegnimento alimentazione PN512 tramite segnale di controllo MIFreset (PTC18=0)
		Accensione alimentazione PN512 tramite segnale di controllo MIFreset (PTC18=1)
		Invia SW reset al PN512
		Inizializza variabili di comunicazione
		Inizializza MifareReader a bassa velocit�
		Inizializza seriali con speed
		Controllo HW version (confronto con 0x82)
		Abilita output driver on data out (come indicato da mask)
*/
	void        *comHandle = nil;
	uint32_t	settings = 0;
	
phcsBfl_Status_t PN512_mif_init(uint32_t speed, uint8_t mask)
{
	phcsBfl_Status_t status = PH_ERR_BFL_SUCCESS;
	
	MIFARE_ChangeBaudRate(MIFARE_BAUD_DEFAULT);							//set baud rate seriale Orion to default PN512 = 9600 baud
	if (gOrionKeyVars.PN512_RapidSearch == false)
	{
		MIFARE_OFF();													// Spengo PN512
		Touts.tmRF=Milli100;											// attendo 100 msec per lo spegnimento 
		while (Touts.tmRF>0){}
	}
	MIFARE_ON();														// Accendo PN512
	Touts.tmRF=Milli100;												// attendo 100 msec per accensione alimentazione 
	while (Touts.tmRF>0){}

	InitMifareAmbient();  												//inizializza variabili e parametri per la comunicazione con PN512
// 	eliminato SW reset 12-01-2013: esce sempre con errore		
//	status = MF_Sw_Reset();												// Sw Reset trasmesso con Baud Rate Low		 

	status = MifareReaderInit(comHandle, settings);			
	if (status) return status;

	status = MifareReaderUARTSpeed(speed);								// set baud rate High MPU e PN512
	if (status) return status;
	MIFARE_ChangeBaudRate(speed);	
	
	status = MifareCompareVersion();									// MR 30.01.2014 Tolto per testare anche l'antenna con PN512 Rev 1.0 ricevuta da Bucci
	if (status) return status;
	status = MifareReaderLedInit(mask);

	status = PN512_setr(PHCS_BFL_JREG_RXTRESHOLD, 0x85);				// MR 20.10.2014: dopo i test sulle antenne di produz Sett 2014, inseriamo fisso il valore 85h

#if  Debug_Reg_18h
	ReadEEPI2C(RFUAddr(TestByte), &status, 1U);
	if ((status & 0xF0) < 0x50 || status > 0xF5) {						// Il Nibble High contiene la soglia del ricevitore
		status &= 0x0F;
		status |= 0x80;
	}
	status = PN512_setr(PHCS_BFL_JREG_RXTRESHOLD, status);
#endif	
	return status;
}

/*------------------------------------------------------------------------------------------*\
Method: PN512_set_led
	Accende e spegne i led pilotati dal PN512.
	La "PHCS_BFL_JREG_TESTPINVALUE" esegue accensione o spegnimento led, per 1 o piu' 
	contemporaneamente.
		
	IN:	 - pin (maschera led da pilotare: 1=led da variare 0=non toccato)
		 - mask (maschera stato ON/OFF led: 1=ON 0=OFF, posizione relativa alla maschera led)
				NOTA le bit positon delle due maschere sono congruenti: bit1 led --> bit1 mask
	OUT: - status (da gestire eventuali errori)

\*------------------------------------------------------------------------------------------*/

phcsBfl_Status_t PN512_set_led (uint8_t pin, uint8_t mask) {
	
	uint8_t 			dato, temp;
	phcsBfl_Status_t 	status;
	
	dato = 0;
	status = PH_ERR_BFL_SUCCESS;
	
	status = PN512_getr(PHCS_BFL_JREG_TESTPINVALUE, &dato);										// Leggo attuale stato dei Led sul PN512
	// *****  Pin Out_D1 del PN512: LED  BIANCO  *****	
	temp = (pin & Out_D1);
	if (temp != 0) {
		temp = (temp & mask);
		if (temp == 0){
			dato = (dato & ~Out_D1);
		} else {
			dato = (dato | Out_D1);
		}
	}
	// *****  Pin Out_D2 del PN512: LED  VERDE  *****	
	temp = (pin & Out_D2);
	if (temp != 0) {
		temp = (temp & mask);
		if (temp == 0) {
			dato = (dato & ~Out_D2);
		} else {
		dato = (dato | Out_D2);
		}
	}
	// *****  Pin Out_D3 del PN512: LED  ROSSO  *****	
	temp = (pin & Out_D3);
	if (temp != 0) {
		temp = (temp & mask);
		if (temp == 0) {
			dato = (dato & ~Out_D3);
		} else {
			dato = (dato | Out_D3);
		}
	}
	status = PN512_setr(PHCS_BFL_JREG_TESTPINVALUE, dato);
	return status;			
}


/*---------------------------------------------------------------------------------------------*\
Method: Accendi_LedGiallo
	Accende il led giallo sull'antenna.
	
	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  Accendi_LedGiallo(void) {
	
	PN512_set_led (led_verde+led_rosso, led_verde|led_rosso);									// Led verde ON - Led Rosso ON
	gOrionKeyVars.CardReadErr = true;															// CardReadErr impedisce al main di riaccendere il verde
}

/*--------------------------------------------------------------------------------------*\
Method:	lamp_led_rosso
	Funzione per far lampeggiare il led rosso dell'Antenna con frequenza fissa di 25
	volte l'esecuzione della gestione Mifare nel Main (che avviene ogni 100 msec).
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void lamp_led_rosso(void)
{
#define timelamp 10

	if (gOrionKeyVars.RedLedCardReader) {
		if (lamp==0) {
			lamp = timelamp;
			PN512_set_led ((led_verde | led_rosso), (!led_verde | led_rosso));					// Accendo led rosso
		} else {
			lamp--;
			if (lamp < (timelamp/2)) {
				PN512_set_led ((led_verde | led_rosso), ! (led_verde | led_rosso));				// Spengo led rosso
			}
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method:	lamp_led_verde
	Funzione per far lampeggiare il led verde dell'Antenna con frequenza fissa di 25
	volte l'esecuzione della gestione Mifare nel Main (che avviene ogni 100 msec).
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/

void lamp_led_verde(void)
{
#define tlamp 25
	
	if (gOrionKeyVars.CardReadErr || gOrionKeyVars.RedLedCardReader) return;					// CardError o RedLEd: sono accesi il led giallo o il rosso, esco senza fare nulla

	if (gOrionKeyVars.Inserted == true) {
		if (lamp == (tlamp+2)) {
	  		PN512_set_led ((led_verde | led_rosso), (led_verde |(!led_rosso)));					// Led Verde ON
		}
  		lamp=tlamp+2;
  		return;
	}
	if (gVars.ReaderInhibit == true) {															// Reader inibito: accendo led giallo lampeggiante
		if (lamp == 0) {
			lamp = tlamp;
			PN512_set_led (led_giallo, led_giallo);
		} else {
			lamp--;
			if (lamp == (tlamp/2)) {
				PN512_set_led (led_giallo, !(led_giallo));
			}
		}
	} else {																					// Reader pronto: accendo led verde lampeggiante
		if (lamp == 0) {
			lamp = tlamp;
			PN512_set_led ((led_verde | led_rosso), (led_verde |!led_rosso));					// Led Verde ON
		} else {
			lamp--;
			if (lamp == (tlamp-2)) 
			PN512_set_led ((led_verde | led_rosso), (!led_verde | !led_rosso));					// Led Verde OFF
		}
	 }  
}

/*--------------------------------------------------------------------------------------*\
Method:	Azzera_Lamp
	Azzera timer Led Antenna per partire con una nuova indicazione 
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void Azzera_Lamp (void)
{
	lamp = 0;
}




