/****************************************************************************************
File:
    VMC_CPU_DispStone.c

Description:
    Funzioni per display Touch Stone.
    

History:
    Date       Aut  Note
    Lug 2021	MR   

*****************************************************************************************/

#include <string.h>

#include "ORION.H"
#include "VMC_CPU_DispStone.h"
#include "VMC_CPU_DispStone_XYCoord.h"
#include "IICEEP.h"
#include "Dummy.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	bool  		IsMonitorActive(void);
extern	uint8_t		RS485_Comm_SendBlock(uint8_t *DataBuff, uint8_t BuffLenght);


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

static 	char			prebuffer[100];															// Buffer dove si prepara la stringa prima di trasfeirla nel buffer da inviare al touch
static	uint8_t			prebuffLen;																// Contatore chr nel prebuffer
static	uint8_t			NumMess, UartErr;
static 	char			NewTouchBuff[100];														// Buffer per nuovo Touch
static	uint8_t			NewTouchLen;															// Contatore chr nel NewTouchBuff

T_Display	TouchDisp;																			// Struttura per la gestione del display Touch

// ---- Stringhe  Fisse  ----
const uint8_t EndFrame[] = {0xCC, 0x33, 0xC3, 0x3C};


/*------------------------------------------------------------------------------------------*\
 Method: Touch_Gest_Visualizzazioni
 	 Visualizza le varie pagine in funzione dello stato del VMC.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  Touch_Gest_Visualizzazioni(void)
{

	//CreditValue Valselez;

	if (gVMCState.InProgrammaz == false)
	{																							// Pagine da gestire solo fuori dalla programmazione
		// -------   Inizializzazione   Macchina  ------------
		if ((TouchDisp.Phase == Touch_OutInit))
		{
			Touch_VisFirmware();																// Revisione firmware 
			//Touch_ReadVersion();																// Revisione firmware 
			TouchDisp.Phase = TouchNormalOper;
		}
	}	
	/*
	if (gVMCState.InProgrammaz == false)
	{																							// Pagine da gestire solo fuori dalla programmazione
		// -------   Inizializzazione   Macchina  ------------
		if ((TouchDisp.Phase == Touch_OutInit) && (IsMonitorActive() == false))
		{
			if (IsLCD_NoInitMsg() == false)
			{																					// Solo quando scade il timeout display LCD esce da Init
				if (IO_1_FwRev != 0) Touch_VisFirmware_IO();									// Ricevuta Revisione firmware dalla scheda I/O
				return;
			}
			else
			{
				Vis_Pag_Home();																	// Visualizza la pagina Home
				TouchDisp.Phase = TouchNormalOper;
			}
			return;													
		}
		// -------   Normali  Funzioni   Macchina  ------------
		if (TouchDisp.Phase == TouchPrelevaBev_Msg)
		{																						// Controllo timeout pagina preleva bevanda
			if (TmrTimeout(TouchDisp.TimeTouchDispMsg))
			{
				TouchDisp.Phase = Touch_VMC_WaitNewVisual;										// Selezione terminata, disattivo animazione
				TouchDisp.ForzaNewVisCred = true;
			}
			return;
		}
		else if ((gVMCState.InSelezione == false) && (TouchDisp.Phase == TouchSelInCorso_Msg))
		{
				StopAnimazione();																// Selezione terminata, disattivo animazione
				if (TouchDisp.SetPagePrelevaBevanda == true)
				{
					TouchDisp.SetPagePrelevaBevanda = false;
					TouchSet_StartMsgPrelevaBevanda();											// Visualizza la pagina di prelievo bevanda
					TmrStart(TouchDisp.TimeTouchDispMsg, FIVESEC);								// Tempo di visualizzazione su Touch
					TouchDisp.Phase = TouchPrelevaBev_Msg;
					return;
				}
		} 
		if ((Is_VMC_Fail() == true) || fVMC_NoAnswer)
		{																						// C'e' almeno un guasto o I/O non risponde
			Touch_Disp_VMC_Fail();
		}
		else
		{
			// Visualizzazione guasti NON bloccanti
			if (fVMC_NoLink)
			{																				// No Link con Master Executive o Secchio Pieno
				Touch_Disp_VMC_Fail();
				return;
			}
			// ======   VMC OK  =========
			if (TouchDisp.Phase != TouchNormalOper)
			{
				TouchDisp.Phase = TouchNormalOper;
				Vis_Pag_Home();																// Visualizza la pagina Home
				return;
			}
#if ReturnHome
			// ======  Automatic Return Home =========
			if (TouchDisp.ActualPageOnDisplay != T_CHIUDI_SPORTELLINO)												// Pagina chiudere sportello su Touch: non cambiare qui
			{
				if ((TouchDisp.ActualPageOnDisplay != T_Home_Page) && (VMC_CreditoDaVisualizzare == 0))
				{
					if (TmrTimeout(TouchDisp.ToutReturnHome))
					{
						Vis_Pag_Home();																				// Visualizza la pagina Home
					}
				}
			}
#endif

#if AnimazDopoHome
			// ======  Animazione dopo Home =========
			if (gVMCState.InProgrammaz == false)
			{
				if ((TouchDisp.ActualPageOnDisplay == T_Home_Page) 	&&
					(TouchDisp.ToutAnimazDopoHome.tmrEnd > 0) 		&&
					(VMC_CreditoDaVisualizzare == 0))
				{
					if (TmrTimeout(TouchDisp.ToutAnimazDopoHome))
					{
						StartStopAnimazione(STARTANIMAZ, AfterHome);												// Start Animazione dopo Home
						TouchDisp.ToutAnimazDopoHome.tmrEnd = 0;
					}
				}
			}
#endif
			// ========================================================
			// ========  Pagine  Utente  Macchina  Standard  ==========
			// ========================================================
			// --- Si visualizza Credito su pag menu 1, 2 e 3 solo se > 0, altrimenti Home
			if ((VMC_CreditoDaVisualizzare != TouchDisp.CreditoDisplay) || (TouchDisp.ForzaNewVisCred == true) || ExChStatusChanged())
			{ 
				if ((VMC_CreditoDaVisualizzare > 0) && (TouchDisp.ActualPageOnDisplay == T_Home_Page))
				{
					if (gNovaCFMacc.DA_Gelati == true)
					{
						if (TouchDisp.AnimazInCorso == true)
						{
							StopAnimazione();
						}
					}
					TouchSetPage(T_Menu_Bevande_1);																	// Credito > 0, passo dalla Home alla Menu_Bevande_1
					if (gNovaCFMacc.DA_Gelati == true)																// 24.07.2020 Dang chiede di visualizzare gia' nella pagina T_Menu_Bevande_1 i prezzi delle due selezioni gelato
					{
						Valselez = MRPPrices.Prices99.MRPPricesList99[ICE_CREAM_1-1];
						Touch_VisVal_uint16(Valselez, &XY_PRICE_GUSTO1_PAG2[0], DecimalPointPosition);				// Visualizza Prezzo 1
						Valselez = MRPPrices.Prices99.MRPPricesList99[ICE_CREAM_2-1];
						Touch_VisVal_uint16(Valselez, &XY_PRICE_GUSTO2_PAG2[0], DecimalPointPosition);				// Visualizza Prezzo 2
					}
				}
				else if (VMC_CreditoDaVisualizzare == 0)
				{
					// --- Credito = 0 ---
					if (TouchDisp.ActualPageOnDisplay <= T_Menu_Bevande_3)
					{																								// Se sono nelle pagine Menu_Bevande_1, 2 o 3 torno a Home
						Vis_Pag_Home();																				// Visualizza la pagina Home
						TouchDisp.ForzaNewVisCred = false;
						TouchDisp.CreditoDisplay = VMC_CreditoDaVisualizzare;
						return;
					}
				}
				// --- Aggiorna la pagina con Credito > 0, Ex-ch e Selezioni Indisponibili ----
				Touch_VisVal_uint16(VMC_CreditoDaVisualizzare, &CoordinateCredit[0], DecimalPointPosition);			// Visualizza il credito
				Touch_Visual_ExCh();
				Check_No_Disponibile();																				// Check eventuale indicazione di selezioni non disponibili
			}
		}
	}

*/
}


/*------------------------------------------------------------------------------------------*\
 Method: DispTouch_TxString
	Invio buffer al display Touch Stone solo se monitor non attivo.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void DispTouch_TxString(uint8_t BuffLenght)
{
		uint8_t		j = 0;
		bool		msg_err = false;
const 	uint8_t 	*pnt = NULL;


	if (IsMonitorActive() == false)
	{
		// =============================================
		// ========    New   Touch   Stone      ========
		// =============================================
		memset (&NewTouchBuff, 0x0, sizeof(NewTouchBuff));
		NewTouchBuff[POS_R3] = 0xA5;																	// Indirizzo selezione Touch
		NewTouchBuff[POS_RA] = 0x5A;
		
		switch (TouchDisp.T_DataBuff[1])
		{
			case SwitchPicture:
				NewTouchBuff[POS_BYTE_CNT] 			= PIC_Swicth_CMD_Len;								// Lunghezza messaggio dopo Header
				NewTouchBuff[POS_CMD] 				= WRITE_REGS;										// Comando da trasmettere
				NewTouchBuff[POS_REG_ADDR] 			= PIC_ID;											// Numero Registro da programmare
				NewTouchBuff[POS_FIRST_CHR_REG] 	= NumPag_MSB;										// MSB address PIC_ID
				NewTouchBuff[POS_FIRST_CHR_REG+1] 	= TouchDisp.T_DataBuff[3];							// LSB address PIC_ID Pagina da visualizzare
				NewTouchLen 						= NewTouchBuff[POS_BYTE_CNT] + HEADER_BYTES_NUM;
				break;
		
			case Text_12x24:
				// Chr di comando visualizza text
				switch (NumMess)
				{
	/*
					case kCoordinateRigaSopra:
						Create_Wr_VAR_Buff(Text_RigaSopra_Addr_MSB, Text_RigaSopra_Addr_LSB, BuffLenght, Text_RigaSopra_Size);
						break;

					case kCoordinateLowRigaSotto:
						Create_Wr_VAR_Buff(Text_RigaSotto_Addr_MSB, Text_RigaSotto_Addr_LSB, BuffLenght, Text_RigaSotto_Size);
						break;
							
					case kCoordinateCredit:
						Create_Wr_VAR_Buff(Text_Credito_Addr_MSB, Text_Credito_Addr_LSB, BuffLenght, Text_Credito_Size);
						break;

					case kXY_ExCh:
						Create_Wr_VAR_Buff(Text_ExCh_Addr_MSB, Text_ExCh_Addr_LSB, BuffLenght, Text_ExCh_Size);
						break;
	*/
					default:
						msg_err = true;
						break;
				}
				break;

			case ColorCommand:
				msg_err = true;
				break;
		
			default:
				msg_err = true;
				break;
		}
		if (msg_err == false)
		{
			NewTouchLen = NewTouchBuff[POS_BYTE_CNT] + HEADER_BYTES_NUM;								// Calcolo lunghezza totale del buffer da inviare al touch
			do
			{
				UartErr = RS485_Comm_SendBlock((uint8_t*)&NewTouchBuff[0], NewTouchLen);
				WDT_Clear(gVars.devWDT);
			} while (UartErr != Resp_OK);
			while (gVars.RS485FlagTx == false) {}													// Attendo invio stringa al Touch
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: TextCopy
	Copia OldBuffLen caratteri dal buffer "TouchDisp.T_DataBuff" al nuovo buffer NewTouchBuff.
	Riempie di spazi i residui fino a TextLen.

   IN:  - TouchDisp.T_DataBuff, OldBuffLen, TextLen
  OUT:  - NewTouchBuff
\*------------------------------------------------------------------------------------------*/
static void TextCopy(uint8_t OldBuffLen, uint8_t TextLen)
{
	uint8_t	src, dest, j;
	
	if (OldBuffLen >= (OldHeader_NumByte + OldTrailer_NumByte))
	{
		OldBuffLen -= (OldHeader_NumByte + OldTrailer_NumByte);										// Tolgo i chr di header e trailer stringa vecchio touch
		dest = POS_FIRST_CHR_STRING;
		memset (&NewTouchBuff[dest], ASCII_SPACE, TextLen);
		for (j=0, src=Pos_OldFirstChr, dest=POS_FIRST_CHR_STRING; j < OldBuffLen; j++, src++, dest++) {
			NewTouchBuff[dest] = TouchDisp.T_DataBuff[src];
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: Create_Wr_VAR_Buff
	Crea il buffer da inviare al nuovo touch con comando di modifica della variabile testo.
	Copia OldBuffLen caratteri dal buffer "TouchDisp.T_DataBuff" al nuovo buffer NewTouchBuff.
	Riempie di spazi i residui fino a StringSize.

   IN:  - VAR_Addr_MSB, VAR_Addr_LSB, TouchDisp.T_DataBuff, OldBuffLen, TextLen
  OUT:  - NewTouchBuff
\*------------------------------------------------------------------------------------------*/
static void  Create_Wr_VAR_Buff(uint8_t VAR_Addr_MSB, 													// Address MSB Variabile da aggiornare
						 uint8_t VAR_Addr_LSB, 													// Address LSB Variabile da aggiornare
						 uint8_t OldTouchBuffLen, 												// Lunghezza del buffer da copiare nel nuovo buffer
						 uint8_t StringSize) 													// Lunghezza della stringa da creare
{	
	memset (&NewTouchBuff, 0x0, sizeof(NewTouchBuff));
	NewTouchBuff[POS_R3] 			= 0xA5;																// Indirizzo selezione Touch
	NewTouchBuff[POS_RA] 			= 0x5A;
	NewTouchBuff[POS_CMD] 			= WRITE_VAR;												// Comando da trasmettere
	NewTouchBuff[POS_VAR_ADDR_MSB] 	= VAR_Addr_MSB;												// MSB Variabile da programmare
	NewTouchBuff[POS_VAR_ADDR_LSB] 	= VAR_Addr_LSB;												// LSB Variabile da programmare
	TextCopy(OldTouchBuffLen, StringSize);														// Copia il testo da "TouchDisp.T_DataBuff" al "NewTouchBuff" 
	NewTouchBuff[POS_BYTE_CNT] 	= HEADER_BYTES_NUM + StringSize;								// Lunghezza messaggio dopo Header
}

/*------------------------------------------------------------------------------------------*\
 Method: ReadActualTouchPage
	Invia al New Touch la richiesta di avere la pagina corrente sul display.
	Funzione usata nel Reset per determinare il tipo di touch. 

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void ReadActualTouchPage(void)
{
	memset (&NewTouchBuff, 0x0, sizeof(NewTouchBuff));
	NewTouchBuff[POS_R3] 			= 0xA5;														// Indirizzo selezione Touch
	NewTouchBuff[POS_RA] 			= 0x5A;
	NewTouchBuff[POS_BYTE_CNT] 	= 3;														// Lunghezza messaggio dopo Header
	NewTouchBuff[POS_CMD] 			= READ_REGS;													// Comando da trasmettere
	NewTouchBuff[POS_REG_ADDR] 		= PIC_ID;													// Numero Registro da programmare
	NewTouchBuff[POS_FIRST_CHR_REG] 	= 2;														// Numero di bytes da ricevere
	NewTouchLen = NewTouchBuff[POS_BYTE_CNT] + HEADER_BYTES_NUM;									// Calcolo lunghezza totale del buffer da inviare al touch
	do
	{
		UartErr = RS485_Comm_SendBlock(&NewTouchBuff[0], NewTouchLen);
		WDT_Clear(gVars.devWDT);
	} while (UartErr != Resp_OK);
	while (gVars.RS485FlagTx == false) {}														// Attendo invio stringa al Touch
}

/*------------------------------------------------------------------------------------------*\
 Method: Touch_ReadVersion
	Legge Versione Touch

	Register Address: 0x00

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void  Touch_ReadVersion(void)
{
	memset (&NewTouchBuff, 0x0, sizeof(NewTouchBuff));
	NewTouchBuff[POS_R3] 			= 0xA5;														// Frame Header
	NewTouchBuff[POS_RA] 			= 0x5A;
	NewTouchBuff[POS_BYTE_CNT] 		= 3;														// Lunghezza messaggio dopo Header
	NewTouchBuff[POS_CMD] 			= READ_REGS;												// Comando da trasmettere
	NewTouchBuff[POS_REG_ADDR] 		= REG_VERSION;												// Numero Registro
	NewTouchBuff[POS_FIRST_CHR_REG] = 1;														// Numero di bytes da ricevere
	NewTouchLen = NewTouchBuff[POS_BYTE_CNT] + HEADER_BYTES_NUM;								// Calcolo lunghezza totale del buffer da inviare al touch
	do
	{
		UartErr = RS485_Comm_SendBlock(&NewTouchBuff[0], NewTouchLen);
		WDT_Clear(gVars.devWDT);
	} while (UartErr != Resp_OK);
	while (gVars.RS485FlagTx == false) {}														// Attendo invio stringa al Touch
}

/*------------------------------------------------------------------------------------------*\
 Method: Touch_VisFirmware
	Visualizza il firmware all'accensione

	AA 6F Coordinate kProductSWVersionStr CC 33 C3 3C

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static void  Touch_VisFirmware(void)
{
	uint8_t	TBuffLen=0, TestLen, numchar;
	char	FWRevBuffer[5];
	char	buffer[10];

//xxx	
	
	memset (&NewTouchBuff, 0x0, sizeof(NewTouchBuff));
	NewTouchBuff[POS_R3] 			= 0xA5;														// Frame Header
	NewTouchBuff[POS_RA] 			= 0x5A;
	NewTouchBuff[POS_CMD] 			= WRITE_VAR;												// Comando da trasmettere
	NewTouchBuff[POS_VAR_ADDR_MSB] 	= 0x02;														// MSB Variabile da programmare
	NewTouchBuff[POS_VAR_ADDR_LSB] 	= 0x00;														// LSB Variabile da programmare

	memset(&TouchDisp.T_DataBuff, ASCII_SPACE, sizeof(TouchDisp.T_DataBuff));
	TouchDisp.T_DataBuff[TBuffLen++] = 'C';
	TouchDisp.T_DataBuff[TBuffLen++] = 'P';
	TouchDisp.T_DataBuff[TBuffLen++] = 'U';
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	TouchDisp.T_DataBuff[TBuffLen++] = 'R';
	TouchDisp.T_DataBuff[TBuffLen++] = 'E';
	TouchDisp.T_DataBuff[TBuffLen++] = 'V';
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	strncpy(&FWRevBuffer[0], kProductSWVersionStr, 4U);										// Visualizzo Revisione Firmware
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[0];
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[1];
	TouchDisp.T_DataBuff[TBuffLen++] = '.';
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[2];
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[3];
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	TouchDisp.T_DataBuff[TBuffLen++] = 'S';
	TouchDisp.T_DataBuff[TBuffLen++] = 'T';
	TouchDisp.T_DataBuff[TBuffLen++] = 'R';
	TouchDisp.T_DataBuff[TBuffLen++] = 'I';
	TouchDisp.T_DataBuff[TBuffLen++] = 'N';
	TouchDisp.T_DataBuff[TBuffLen++] = 'G';
	TouchDisp.T_DataBuff[TBuffLen++] = 'A';
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	TouchDisp.T_DataBuff[TBuffLen++] = 'd';
	TouchDisp.T_DataBuff[TBuffLen++] = 'i';
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	TouchDisp.T_DataBuff[TBuffLen++] = 'P';
	TouchDisp.T_DataBuff[TBuffLen++] = 'R';
	TouchDisp.T_DataBuff[TBuffLen++] = 'O';
	TouchDisp.T_DataBuff[TBuffLen++] = 'V';
	TouchDisp.T_DataBuff[TBuffLen++] = 'A';
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	TouchDisp.T_DataBuff[TBuffLen++] = 'd';
	TouchDisp.T_DataBuff[TBuffLen++] = 'i';
	TouchDisp.T_DataBuff[TBuffLen++] = 'L';
	TouchDisp.T_DataBuff[TBuffLen++] = 'E';
	TouchDisp.T_DataBuff[TBuffLen++] = 'N';
	TouchDisp.T_DataBuff[TBuffLen++] = 'G';
	TouchDisp.T_DataBuff[TBuffLen++] = 'H';
	TouchDisp.T_DataBuff[TBuffLen++] = 0;
	numchar = strlen((char*)&TouchDisp.T_DataBuff[0]);
	strncpy(&NewTouchBuff[POS_FIRST_CHR_STRING], (const char*)&TouchDisp.T_DataBuff[0], numchar);
	NewTouchBuff[POS_BYTE_CNT] 	= HEADERLEN + COMMANDLEN + numchar;							// Lunghezza messaggio dopo Header
	numchar = NewTouchBuff[POS_BYTE_CNT] + HEADERLEN + COMMANDLEN;
	do
	{
		UartErr = RS485_Comm_SendBlock(&NewTouchBuff[0], numchar);
		WDT_Clear(gVars.devWDT);
	} while (UartErr != Resp_OK);
	while (gVars.RS485FlagTx == false) {}														// Attendo invio stringa al Touch
	
/*	
	
	//TouchSetColor();																		// Set colori stringhe da visualizzare
	memset(&TouchDisp.T_DataBuff, ASCII_SPACE, sizeof(TouchDisp.T_DataBuff));
	TouchDisp.T_DataBuff[TBuffLen++] = TouchTxStart;										// Chr di start trasmissione
	TouchDisp.T_DataBuff[TBuffLen++] = Text_12x24;											// Chr di comando visualizza text
	if (Touch_VisFirmware == false)
	{
		TBuffLen = MemoCoordinate(&CoordinateModello[0], TBuffLen);							// 4 chr delle coordinate
		strncpy((char*)&TouchDisp.T_DataBuff[TBuffLen], (char const*)&MachineModelNumber, Len_ID102);			// Visualizzo Modello Macchina (ID102)
		TBuffLen += Len_ID102;
		TBuffLen = MemoEndFrame(TBuffLen);													// Aggiungo i 4 chr di fine stringa al buffer
		DispTouch_TxString(TBuffLen);														// Attendo invio stringa al Touch
	}
	memset(&TouchDisp.T_DataBuff, ASCII_SPACE, sizeof(TouchDisp.T_DataBuff));
	TouchDisp.T_DataBuff[TBuffLen++] = TouchTxStart;										// Chr di start trasmissione
	TouchDisp.T_DataBuff[TBuffLen++] = Text_12x24;											// Chr di comando visualizza text
	TBuffLen = MemoCoordinate(&CoordinateFW[0], TBuffLen);									// 4 chr delle coordinate
	TouchDisp.T_DataBuff[TBuffLen++] = 'C';
	TouchDisp.T_DataBuff[TBuffLen++] = 'P';
	TouchDisp.T_DataBuff[TBuffLen++] = 'U';
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	TouchDisp.T_DataBuff[TBuffLen++] = 'R';
	TouchDisp.T_DataBuff[TBuffLen++] = 'E';
	TouchDisp.T_DataBuff[TBuffLen++] = 'V';
	TouchDisp.T_DataBuff[TBuffLen++] = ' ';
	strncpy(&FWRevBuffer[0], kProductSWVersionStr, 4U);										// Visualizzo Revisione Firmware
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[0];
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[1];
	TouchDisp.T_DataBuff[TBuffLen++] = '.';
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[2];
	TouchDisp.T_DataBuff[TBuffLen++] = FWRevBuffer[3];
	TBuffLen = MemoEndFrame(TBuffLen);														// Aggiungo i 4 chr di fine stringa al buffer
	//DispTouch_TxString(TBuffLen);															// Attendo invio stringa al Touch
	
	//Create_Wr_VAR_Buff(0x02, 0x00, BuffLenght, Text_RigaSopra_Size);
	//TextCopy(OldTouchBuffLen, StringSize);													// Copia il testo da "TouchDisp.T_DataBuff" al "NewTouchBuff" 
*/

}


/*------------------------------------------------------------------------------------------*\
 Method: Touch_Visual_Init
	Inizializza il Touch prima dell'uso.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void  Touch_Visual_Init(void)
{
	//Touch_VisFirmware();
	TouchDisp.Phase = Touch_OutInit;
}

/*------------------------------------------------------------------------------------------*\
 Method: MemoCoordinate
	Inserisce le coordinate nel buffer per il Touch.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static uint8_t  MemoCoordinate(const uint8_t *Coordinate, uint8_t index)
{
	const uint8_t *pnt;
	
	pnt = Coordinate;
	TouchDisp.T_DataBuff[index++] = *(pnt++);													// Aggiungo i 4 bytes delle coordinate
	TouchDisp.T_DataBuff[index++] = *(pnt++);													// Aggiungo i 4 bytes delle coordinate
	TouchDisp.T_DataBuff[index++] = *(pnt++);													// Aggiungo i 4 bytes delle coordinate
	TouchDisp.T_DataBuff[index++] = *(pnt++);													// Aggiungo i 4 bytes delle coordinate
	NumMess = *(pnt++);																			// Numero messaggio (serve per il nuovo touch)
	return index;
}

/*------------------------------------------------------------------------------------------*\
 Method: MemoEndFrame
	Memorizzo i 4 bytes di fine stringa nel punto del buffer da inviare al Touch indicato
	dal parametro index.

   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
static uint8_t  MemoEndFrame(uint8_t index)
{
	TouchDisp.T_DataBuff[index++] = EndFrame[0];												// Aggiungo i 4 chr di fine stringa al buffer
	TouchDisp.T_DataBuff[index++] = EndFrame[1];
	TouchDisp.T_DataBuff[index++] = EndFrame[2];
	TouchDisp.T_DataBuff[index++] = EndFrame[3];
	return index;
}




