/* ***************************************************************************************
File:
    Events.c

Description:
    File con le funzioni chiamate dai diversi Interrupts delle periferiche.
    
History:
    Date       Aut  Note
    Apr 2019 	MR   

 *****************************************************************************************/

#include "main.h"
#include "ORION.H"
#include "Events.h"
#include "Gestore_Exec_MSTSLV.h"
#include "VMC_CPU_HW.h"
#include "Monitor.h"
#include "VMC_CPU_Motori.h"
#include "IoT_Comm.h"



/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	volatile uint32_t scan_timeout;
extern	volatile uint32_t now_time;

extern	void	DDCMPTxDoneInd(void);
extern	void	Sel24_read_coin(void);
extern	void 	DDCMPRxIndData(uint8_t data);
extern	void 	ICPDataInd(uint8_t nData);
extern	void 	ICPTxComplete(void);
extern	void	SaveBackupData(void);
extern	void  	All_595Outputs_OFF(void);
extern	bool	SuspendMotCoclea;
extern	void  	Buz_stop(void);
extern	void  	scan_buffer(uint8_t chr);
extern	void  	EXPANSION_1_Poll(void);
extern	void  	Task_Expansion_1(void);
extern	void  	RS485_RxError(void);

extern	uint32_t	adc_DMA_buffer[4];
extern	uint8_t		PN512_RxData;

extern	UART_HandleTypeDef	hlpuart1;
extern	UART_HandleTypeDef	huart2;
extern	UART_HandleTypeDef	huart3;
extern	UART_HandleTypeDef	huart4;
extern	UART_HandleTypeDef	huart5;
extern 	ADC_HandleTypeDef	hadc1;
#if IRDA_PRESENTE
	extern	HAL_StatusTypeDef HAL_IRDA_Receive_IT(IRDA_HandleTypeDef *hirda, uint8_t *pData, uint16_t Size);
#endif

/*--------------------------------------------------------------------------------------*\
Global variables and types definition	
\*--------------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------------------*\
Method: HAL_UART_RxCpltCallback
	Rx Transfer completed callback (fine ricezione da una Uart)
	hlpuart1 per Protocolli Slave:
	- Se Executive Slave l'errore e' ricevuto dalla RR: chiamo la GestMS_RR_CMD.
	- In MDB non rispondo, sara' il Master a prendere provvedimenti.

	huart2 per Protocolli Master:
	- In Executive chiamo la GestMS_RxVMC2_Error
	- In MDB ci si affida ai controlli che dovrebbero determinare la correttezza dei dati ricevuti.


	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *uart)
{
	// -----  Ricezione Slave Executive/MDB USD -----
	if (uart == &hlpuart1)
	{
	  	if (gVMC_ConfVars.ProtMDBSLV == true)
		{
			Monitor_LogMDBRxData(hlpuart1_BuffRx[1]);
			MDB_RxModeBitVal = hlpuart1_BuffRx[0];
			ICPDataInd(hlpuart1_BuffRx[1]);														// Slave MDB
		}
		else if (gVMC_ConfVars.ExecutiveSLV == true)
		{
			GestMS_RR_CMD(hlpuart1_BuffRx[0]);													// Gestore MST/SLV Executive per destinare chr ricevuto
		}
		HAL_UART_Receive_IT(&hlpuart1, &hlpuart1_BuffRx[0], 1U);								// Start reception of one character
	}
	
	// -----  Ricezione MIFARE  -----
	if (uart == &huart2)
	{
		PN512_RxData = huart2_BuffRx[0];														// Chr ricevuto dal PN512
		gVars.MifareRDRF = true;
	}

	// -----  Ricezione RS485  -----
	if (uart == &huart3)
	{
		//--  Touch  Stone ---
		scan_buffer(huart3_BuffRx[0]);															// Se arrivo qui c'e' almeno un chr ricevuto 
		HAL_UART_Receive_IT(&huart3, &huart3_BuffRx[0], 1U);									// Start reception of one character
	}

	// -----  Ricezione Master MDB/Executive  -----
	if (uart == &huart5)
	{
		if (OpMode == MDB)
		{
			Monitor_LogMDBRxData(huart5_BuffRx[0]);
			MDB_RxModeBitVal = huart5_BuffRx[1];
			ICPDataInd(huart5_BuffRx[0]);														// Master MDB
		}
		else
		{
			GestMS_RxVMC2(huart5_BuffRx[0]);													// Rx da VMC2
		}
		HAL_UART_Receive_IT(&huart5, &huart5_BuffRx[0], 1U);									// Start reception of one character
	}

#if IOT_PRESENCE
	// -----  Ricezione IoT  -----
	if (uart == &huart4)
	{
		Gtwy_RxData(huart4_BuffRx[0]);
		HAL_UART_Receive_IT(&huart4, &huart4_BuffRx[0], 1U);									// Start reception of one character
	}
#endif

}

/*---------------------------------------------------------------------------------------------*\
Method: HAL_IRDA_RxCpltCallback
	Rx Transfer completed callback (fine ricezione dalla Uart IrDA)

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
#if IRDA_PRESENTE
void HAL_IRDA_RxCpltCallback(IRDA_HandleTypeDef *hirda1)
{
	DDCMPRxIndData(hirda1_BuffRx[0]);															// Rx DDCMP su IrDA
	HAL_IRDA_Receive_IT(hirda1, &hirda1_BuffRx[0], 1U);											// Start reception of one character
}
#endif
/*---------------------------------------------------------------------------------------------*\
Method: HAL_IRDA_TxCpltCallback
	Tx Transfer completed callback (fine trasmmissione Uart IrDA)

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
#if IRDA_PRESENTE
void HAL_IRDA_TxCpltCallback(IRDA_HandleTypeDef *hirda1)
{
	DDCMPTxDoneInd();
}
#endif

/*---------------------------------------------------------------------------------------------*\
Method: HAL_UART_TxCpltCallback
	Tx Transfer completed callback (fine trasmissione di una Uart)
	hlpuart1 per Protocolli Slave:
	- In Executive non utilizzo l'IRQ di fine trasmissione: esco senza fare nulla
	- In MDB chiamo la ICPTxComplete.

	huart5 per Protocolli Master:
	- In Executive chiamo la GestMS_RxVMC2_Error
	- In MDB ci si affida ai controlli che dovrebbero determinare la correttezza dei dati ricevuti.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *uart)
{
	// -----  Fine Trasmissione Executive/MDB  Slave -----
	if (uart == &hlpuart1)
	{
		if ((gVMC_ConfVars.ProtMDB) && (gVMC_ConfVars.ProtMDBSLV))
		{
			ICPTxComplete();
		}
	}
	
	// -----  Fine Trasmissione Master MDB/Executive  -----
	if (uart == &huart5)
	{
		if (gVMC_ConfVars.ProtMDB)
		{
			ICPTxComplete();
		}
	}
	
	// -----  Fine Trasmissione RS485  -----
	if (uart == &huart3)
	{
		//RS485_Comm_ReceiveBlock(&gCommVars.RS485_RxBuff[0], 1U);							// Abilita ricezione 1 chr
	}

	// -----  Fine Trasmissione MIFARE  -----
	if (uart == &huart2)
	{
		gVars.MifareTxCplt = true;
	}
}

/*---------------------------------------------------------------------------------------------*\
Method: HAL_UART_ErrorCallback
	hlpuart1 per Protocolli Slave:
	- Se Executive Slave l'errore e' ricevuto dalla RR: chiamo la GestMS_RxRR_Error.
	- In MDB non rispondo, sara' il Master a prendere provvedimenti.

	huart5 per Protocolli Master:
	- In Executive chiamo la GestMS_RxVMC2_Error
	- In MDB ci si affida ai controlli che dovrebbero determinare la correttezza dei dati ricevuti.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *uart)
{
	// -----  Executive/MDB  Slave -----
	if (uart == &hlpuart1)
	{
	  	if (OpMode != MDB)
		{
			GestMS_RxRR_Error(hlpuart1_BuffRx[0]);												// Gestore MST/SLV Executive per destinare errore ricevuto
		}
	  	HAL_UART_Receive_IT(&hlpuart1, &hlpuart1_BuffRx[0], 1);									// Start reception of one character
	}
	// -----  MDB/Executive  Master -----
	if (uart == &huart5)
	{
	 	if (OpMode != MDB)
	  	{
			GestMS_RxVMC2_Error(huart5_BuffRx[0]);
	  	}
	  	HAL_UART_Receive_IT(&huart5, &huart5_BuffRx[0], 1);										// Start reception of one character
	}
	// -----  RS485_Comm  -----
	if (uart == &huart3)
	{
		HAL_UART_Receive_IT(&huart3, &huart3_BuffRx[0], 1);										// Start reception of one character
	}

#if IOT_PRESENCE
	// -----  IoT  Comm  -----
	if (uart == &huart4)
	{
		HAL_UART_Receive_IT(&huart4, &huart4_BuffRx[0], 1U);									// Start reception of one character
	}
#endif
}

/*
** ===================================================================
**     Event       :  PIT0_OnCounterRestart (module Events)
**
**     Component   :  PIT0 [TimerUnit_LDD]
**     Description :
**         Called if counter overflow/underflow or counter is
**         reinitialized by modulo or compare register matching.
**         OnCounterRestart event and Timer unit must be enabled. See
**         <SetEventMask> and <GetEventMask> methods. This event is
**         available only if a <Interrupt> is enabled.
**     Parameters  :
**         NAME            - DESCRIPTION
**       * UserDataPtr     - Pointer to the user or
**                           RTOS specific data. The pointer passed as
**                           the parameter of Init method.
**     Returns     : Nothing
** ===================================================================
*/
/*---------------------------------------------------------------------------------------------*\
Method: IRQ_Tick_Timer
	IRQ del Tick Timer da 1 msec
	Nel Fw Ecoline dura circa 70 usec (13.03.2021)

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void IRQ_Tick_Timer(void)
{
  	if (IRQ_TickTimerFunctEnabled == false) return;

	// =======================================================
	// ========    Test   Buzzer        ======================
	// =======================================================
	
	if (Touts.Buzz_time1)
	{
		Touts.Buzz_time1--;
		if ((Touts.Buzz_time1 == 0) && (Touts.Buzz_time2 == 0))
		{
		  	Buz_stop();
		}
	} 
	else 
	{
		if (Touts.Buzz_time2)
		{
			Touts.Buzz_time1 = Touts.Buzz_time2;												// Inizia il tempo del secondo tono
			Buz.freq1 = Buz.freq2;
			Touts.Buzz_time2 = 0;
			BuzFreqChange(Buz.freq2);
		}
	}

	// =======================================================
	// ========    IRQ  ogni  1  msec   ======================
	// =======================================================

#if DEB_TIME_PIT0
	HAL_GPIO_WritePin(GPIO_1_GPIO_Port, GPIO_1_Pin, GPIO_PIN_SET);			// Debug  PE9 HIGH  GPIO-1
#endif	
	
	gVars.gKernelFreeCounter++;																	// Incremento il General Purpose Counter usato in Monitor
	if (gVars.gKernelFreeCounter == 0)
	{
		WatchDogReset();																		// Reset uP tramite WatchDog
		while(true){};
	}											
	
	if(Touts.tmUartMif_RX) 	Touts.tmUartMif_RX--;												// Timer sicurezza ricezione da PN512
 	if (Touts.tmUartMif_TX) Touts.tmUartMif_TX--;												// Timer sicurezza trasmissione a PN512
	if (Touts.tmUSB_Key) 	Touts.tmUSB_Key--;													// Timer rilevamento chiave USB
	if (Touts.tmRF) 		Touts.tmRF--;														// Timer riattivazione antenna MIFARE
	if (Touts.tmMon) 		Touts.tmMon--;														// Timer Monitor

#if (IOT_PRESENCE == true)
	if (Touts.GtwyToutAnsw)
	{
		Touts.GtwyToutAnsw--;																	// Timer Com  Gateway
	}
	if (ts_milliseconds > 0) ts_milliseconds--;
	if (ts_milliseconds == 0) ts_milliseconds = 1000;

#endif

	if (IsMonitorReceivingFiles() == true)  return;
	
	if (PowSup.PWD_Flag == false)																// Non sono in Power-Down
	{
	  	if (scan_timeout) scan_timeout--;
		now_time++;																				// Timers Display Touch
		if (Touts.ToutVend) Touts.ToutVend--;
		RTx_IO();																				// Ricetrasmissione con sezione I/O della CPU
		if (sel_warmup) sel_warmup--;
		Sel24_read_coin();																		// Leggo selettore parallelo
		AnalogsIntegration();
		if (Touts.ToutScanTemper > 0) Touts.ToutScanTemper--;
	}
#if DEB_TIME_PIT0
	HAL_GPIO_WritePin(GPIO_1_GPIO_Port, GPIO_1_Pin, GPIO_PIN_RESET);			// Debug  PE9 LOW GPIO-1
#endif	
	
}

// ========================================================================
// ========    A D C   C o n v e r t e r          =========================
// ========================================================================
/*
void  HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
	uint32_t  j;
	
	if (hadc->Instance == ADC1)
	{
		for (j=0; j < 9; j++)
		{
	 		adc_read_values[j] = adc_DMA_buffer[j];
		}
	}
}
*/
/*---------------------------------------------------------------------------------------------*\
Method: HAL_ADCEx_LevelOutOfWindow2Callback
	Arrivo qui quando la misura di corrente delle uscite ha superato la soglia massima impostata
	come corrente di Corto Circuito.

	IN:	  - 
	OUT:  - flag ICC_Alarm true
\*----------------------------------------------------------------------------------------------*/
void HAL_ADCEx_LevelOutOfWindow2Callback(ADC_HandleTypeDef *hadc)
{
	// -- Disabilito immediatamente tutte le uscite --
	All_595Outputs_OFF();																		// Disabilito uscite
	ICC_Alarm = true;
	Set_VMC_ICC;																				// Set Fermo Macchina per Corrente di Corto Circuito Uscite 
}

#if (DEB_NO_VOLTAGE_TEST == false)
/*---------------------------------------------------------------------------------------------*\
Method: HAL_ADC_LevelOutOfWindowCallback
	Arrivo qui quando la tensione di alimentazione e' sotto la soglia LowLevelADC predefinita.
	Se sono all'accensione (PowSup.PUP_WaitViHigh = true) esco senza fare nulla.
	La flag PowSup.PUP_WaitViHigh = false indica una discesa della tensione di alimentazione
	e devo procvedere col power down.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef *hadc)
{
	//HAL_StatusTypeDef	res;

  	if (hadc->Instance == ADC1)
	{
		if (PowSup.PUP_WaitViHigh == true) return;												// Sono in attesa della stabilizzazione della tensione di alimentazione

		// ========================================================================
		// ========    Diattivazione    Consumi    Vari      ======================
		// ========================================================================
		// **** Vi < Vi Max Funzionamento = PowerDown ****
		
#if DBG_PWD_TIME
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_RESET);									// PE13 LOW
#endif
		
		All_595Outputs_OFF();																	// Disabilito uscite
		MIFARE_OFF();																			// Spengo PN512
		GreenLedCPU(OFF);																		// Led Verde CPU OFF
		//EnableRxIrDA();
		if (PowSup.PUP_Restore | PowSup.EEInUse)												// Se Restore o EE in uso esco senza salvare la RAM in EEprom
		{
			//res = HAL_ADC_Stop_DMA(&hadc1);
			HAL_ADC_Stop_DMA(&hadc1);
		 	HAL_ADC_MspDeInit(&hadc1);															// Disattivo ADC per non rientrare in IRQ e salvare Backup RAM nel MAIN
		}
		else
		{
			SaveBackupData();																	// Salvo area RAM in EEPROM e attendo la' Spegnimento o WD Reset
		}
	}
}
#endif	// End DEB_NO_VOLTAGE_TEST

/*---------------------------------------------------------------------------------------------*\
Method: HAL_GPIO_EXTI_Callback
	Irq generato dal fronte di salita di PD7.

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	AttivaMonitor();																			// Abilita il programma Monitor se non gia' attivo
}

/* END Events */

