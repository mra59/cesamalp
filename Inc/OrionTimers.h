/****************************************************************************************
File:
    OrionTimers.h

Description:
    Include file con definizioni e strutture per la gestione dei timers.

History:
    Date       Aut  Note
    Feb 2013	MR   

 *****************************************************************************************/

#ifndef __OrionTimers_H
#define __OrionTimers_H

void KernelTmr32Start(Tmr32Local* t, uint32_t timeout);
bool KernelTmr32Timeout(Tmr32Local* t);
void KernelTmr32Add(Tmr32Local* t, uint32_t timeout);

#endif		// __OrionTimers_H


