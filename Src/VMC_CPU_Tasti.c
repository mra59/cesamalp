/* ***************************************************************************************
File:
    VMC_CPU_Tasti.c

Description:
    File con le funzioni per il rilevamento tasti premuti da display touch Stone e da
    tastiera a membrana.

History:
    Date       Aut  Note
    Apr 2016 	Abe/MR  

 *****************************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include "ORION.H"
#include "VMC_CPU_Tasti.h"
#include "VMC_CPU_HW.h"
#include "OrionTimers.h"
#include "IICEEP.h"
#include "VMC_CPU_LCD.h"
#include "VMC_CPU_Prog.h"


#include "Dummy.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/
extern	void		AttivaMonitor(void);
extern	bool		Is_DA_Vuoto(void);
extern	uint16_t  	Dec16Touint16(uint16_t);
extern	void   		Prog_Main(void);
extern	void 		Overpay(uint8_t);
extern	void  		GestTastiSelez(uint16_t tasto);
extern	void  		ResetGuasti(void);
extern	void 		VisMessage(uint8_t NumMess_1, uint8_t NumRigaMsg_1, uint8_t NumMess_2, uint8_t NumRigaMsg_2, bool FillSpace_All_LCD);
extern	void 		VisualNum_2cifre(uint8_t Numero, uint8_t NumRiga);
extern	void 		HAL_NVIC_SystemReset(void);
extern	bool  		Is_VMC_Fail(void);
extern	uint8_t		SendConfToEEprom(uint8_t);
extern	void  		Motori_OFF(void);
extern	void  		MIFARE_COMM_DeInit(void);
extern	void		HAL_Delay(uint32_t Delay);
extern	uint8_t 	ReadErogazStatus(uint8_t NumErogaz);

//MR19 extern	void  		SetToutReturnHome(void);


/*--------------------------------------------------------------------------------------*\
Private constants and types definition	
\*--------------------------------------------------------------------------------------*/

volatile	SCAN_STAT 		scan_state;
static 		uint8_t 		msg_len;
static 		uint8_t 		msg_cmd;
static		uint8_t			Var_Addr, Picture_ID;

volatile 	uint8_t			msg_data[MAX_numchar];
volatile 	uint8_t			Rx_data[16];														// Dati ricevuti

static		uint8_t			Cnt_Tasto;
static 		uint16_t 		val_tasto, SommaTasti;
static 		uint32_t 		previous_time = 0;
volatile 	uint32_t 		now_time;
volatile 	uint32_t 		scan_timeout;

volatile 	TASTI_TOUCH 	touch_button[MAX_TASTI];

static		TempiTouch		T_times;
static		bool			TastoDaTouch, fError, fTouchPmt;

volatile 	uint32_t 		t_w_pointer = 0;													// write buffer pointer
			uint32_t 		t_r_pointer = 0;													// read buffer pointer


/*---------------------------------------------------------------------------------------------*\
Method: scan_buffer
	UART IRQ callback, only when valid char is received.
	Il tTouch Stone inizia la trasmissione con il chr 0xAA.
	Il Monitor e' attivato se il primo chr e' "M". 
 
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void scan_buffer(uint8_t chr) {
  
	uint32_t	Temporary_Time = now_time;				// Introdotto per eliminare il Warning[Pa082]: undefined behavior: the order of volatile accesses is undefined in this statement alla Riga 161
	
	if (gOrionConfVars.NewTouchStone == false)
	{

		// ---------   TOUCH 7" Mod. STI07  ---------
		// =============================================
		// ========    Vecchio  Touch  Stone    ========
		// =============================================
		switch(scan_state)
		{
			//check start character
			 case SCAN_IDLE:
				 if (chr == 0xAA) {
					 scan_state = SCAN_START_RX;
					 scan_timeout = MAX_WAIT_TOUCH;
				 } else {
					 if (chr == 'M'){
						 AttivaMonitor();														// Attivo Monitor
					 }
				 }
				 break;
			 
			 //check if data is touch button pressed information	
			 case SCAN_START_RX:
				 if (chr == 0x78){
					 scan_state = SCAN_WAIT_MSB_TOUCH;
				 }else{
					 scan_state = SCAN_IDLE;
				 }
				 break;
			 
				 //wait for touch area identification code
			 case SCAN_WAIT_MSB_TOUCH:
				 val_tasto = (uint16_t)(chr<<8);
				 scan_state = SCAN_WAIT_LSB_TOUCH;
				 break;
			 case SCAN_WAIT_LSB_TOUCH:
				 val_tasto |= (uint16_t)(chr);
				 scan_state = SCAN_END_1;
				 break;
			 
				 //check valid string terminator (4 char)
			 case SCAN_END_1:
				 if (chr == 0xCC){
					 scan_state = SCAN_END_2;
				 }else{
					 scan_state = SCAN_IDLE;
				 }
				 break;
			 case SCAN_END_2:
				 if (chr == 0x33){
					 scan_state = SCAN_END_3;
				 }else{
					 scan_state = SCAN_IDLE;
				 }
				 break;
			 case SCAN_END_3:
				 if (chr == 0xC3){
					 scan_state = SCAN_END_4;
				 }else{
					 scan_state = SCAN_IDLE;
				 }
				 break;
			 case SCAN_END_4:
				 if (chr == 0x3C){
					 //touch area valid, process button code
					 scan_state = SCAN_VALID;
				 }else{
					 scan_state = SCAN_IDLE;
				 }
				 break;
			 default:
				 scan_state = SCAN_IDLE;
				 break;
		 }
		 if (scan_state == SCAN_VALID)
		 {
			 scan_state = SCAN_IDLE;
			 touch_button[t_w_pointer].valore_tasto = val_tasto;
			 //MR19 touch_button[t_w_pointer].time_tasto = now_time-previous_time;			//Warning[Pa082]: undefined behavior: the order of volatile accesses is undefined in this statement
			 //MR19 previous_time = now_time;
			 touch_button[t_w_pointer].time_tasto = Temporary_Time-previous_time;
			 previous_time = Temporary_Time;
			 t_w_pointer = (t_w_pointer+1)%MAX_TASTI;
		 }
	}
	else
	{
		 // =============================================
		 // ========    New    Touch    Stone    ========
		 // =============================================
		 // ---------   New TOUCH Stone Protocol   ---------
		 // Il messaggio del nuovo touch e' cosi' costituito:
		 // 0xA5 - 0x5A - Len - CMD - Data
		 switch(scan_state){
	 
		 //check First start character
		 case NEWSCAN_Idle:
			 if (chr == 0xA5){
				 scan_state = NEWSCAN_Second_SyncChr;
				 scan_timeout = MAX_WAIT_TOUCH;
			 } else {
				 if (chr == 'M'){
					 AttivaMonitor();																// Attivo Monitor
				 }
			 }
			 break;
			 
		 //check Second start character
		 case NEWSCAN_Second_SyncChr:
			 if (chr == 0x5A){
				 scan_state = NEWSCAN_MsgCounter;
			 }else{
				 scan_state = NEWSCAN_Idle;
			 }
			 break;
	 
		 //check for message len counter
		 case NEWSCAN_MsgCounter:
			 if (chr == 0) {
				 scan_state = NEWSCAN_Idle;
			 } else {
				 msg_len = chr;
				 t_w_pointer = 0;
				 scan_state = NEWSCAN_MsgCommand;
			 }
			 break;
			 
		 //check for message Command
		 case NEWSCAN_MsgCommand:
			 msg_cmd = chr;
			 msg_len--;
			 scan_state = NEWSCAN_Data;
			 break;
				 
		 //wait for touch address variabile
		 case NEWSCAN_Data:
			 msg_data[t_w_pointer++] = chr;
			 if (--msg_len == 0) {
				 scan_state = NEWSCAN_Valid;
			 }
			 break;
			 
		 default:
			 scan_state = NEWSCAN_Idle;
			 break;
		 }
		 if (scan_state == NEWSCAN_Valid)
		 {
			 t_w_pointer = 0;
			 scan_state = NEWSCAN_Idle;
			 fError = false;
			 switch(msg_cmd)																		// Elaborazione comando ricevuto dal Touch
			 {
			 	case Read_REG:																		// 0x81
					 // --- Lettura  Registro ---
					 switch (msg_data[0])															// Elaborazione registro ricevuto
					 {
						case PIC_ID:																// Numero della pagina sul touch
							Picture_ID = (msg_data[PIC_ID_MSB] << 8);
							Picture_ID |= (msg_data[PIC_ID_LSB]);
							SetActualTouchPage(Picture_ID);
							fError = true;															// Non e' un tasto premuto
							break;
						 
					 	default:
							fError = true;
							break;
					 }
					 break;
					 
				 case Read_VAR:																		// 0x83
					 // --- Lettura  Variabile ---
					 Var_Addr = (msg_data[0] << 8);													// Indirizzo variabile ricevuta
					 Var_Addr |= (msg_data[1]);
					 if (Var_Addr == Button_Addr)
					 {
						 val_tasto = (msg_data[Butt_MSB] << 8);
						 val_tasto |= msg_data[Butt_LSB];
					 }
					 break;
				 
				 default:
					 fError = true;
					 break;
			 }
			 if (fError == false)
			 {
				 fTouchPmt = true;
				 touch_button[0].valore_tasto = val_tasto;
			 }
		 }
	 }
}

/*---------------------------------------------------------------------------------------------*\
Method: Gestione_Touch_Tastiera
	Elaborazione tasti dal Touch, dalla tastiera a matrice, dal pulsante di programmazione
	e dai pulsanti di erogazione.
 
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
void  Gestione_Touch_Tastiera(void)
{
	bool		EnaElabTasto;
	uint16_t 	tasto;

	// =============================================================
	// ========  Timeout Uscita Automatica Programmazione ==========
	// =============================================================
#if OutProg		
		if (IsToutProgramElapsed() == true) {
			Restart_Da_Prog();																	// Attesa WDT da timeout per uscire dalla programmazione
		}
#endif		

	// ================================================
	// ========  Pulsante programmazione CPU ==========
	// ================================================
	if (TastoProgrammazione == 0)																// Tasto PROG premuto
	{
		if (gVMCState.TastoProgElaborato == false)
		{
			gVMCState.TastoProgElaborato = true;												// Tasto PROG elaborato
			if (gVMCState.InProgrammaz == false)
			{
				gVMCState.InProgrammaz = true;													// Inizio programmazione
				Motori_OFF();																	// Disattivo eventuali uscite (usando anche la flag InProgrammaz)
				MIFARE_OFF();																	// Tolgo tensione +5V Mifare altrimenti il uP riparte in Boot dopo il reset in uscita programmazione
				ResetToutProgram();																// Carico Timer uscita automatica programmazione
				gVMCState.ParamModified = false;
				Buz_start(Milli100, HZ_2000, 0);												// Segnalo prog premuto
				if (gOrionCashVars.creditCash > 0)												// Eventuale credito cash in Overpay
				{
				  	Overpay(OVERPAY_CASH);
					gOrionCashVars.creditCash = 0;
					VMC_CreditoDaVisualizzare = 0;
				}
				Imene = false;																	// Reinvio Init tramite Main
				//MR19 TouchSetPage(T_PagProgrammazione);												// Visualizza la pagina di programmazione
				//MR19 ClrAccessLevel();																// Predispongo livello accesso zero
				Set_fVMC_Inibito;																// VMC inibito
				gVMCState.TastieraDisable = false;												// Riabilita tastiera
			}
			else
			{
				// Premuto per uscire dalla programmazione
				Buz_start(Milli100, HZ_2000, 0);												// Segnalo tasto premuto
				Restart_Da_Prog();																// Attende Reset da WDT
			}
		}
	}
	else
	{
		gVMCState.TastoProgElaborato = false;													// Tasto PROG rilasciato
	}
	
	// ======================================================
	// ========         Pulsanti Erogazione        ==========
	// ======================================================
	
	//--  Elaborazione  Pulsante  1 ----
	if (fTastoPmt1 == true)
	{																							// Premuto pulsante erogazione 1
		//if ((ReadErogazStatus(0) & INPROGR_STATUS) != false) goto FineElabTasto;				// Durante l'erogazione non elaboro qui il pulsante premuto
		if (fTastoElab1 == false)																// Pulsante da elaborare
		{
			if (gVMCState.InProgrammaz == false)												// Testo guasti ed elaboro sempre tastiera fuori dalla programmazione
			{
				fTastoElab1 = true;
				EnaElabTasto = (gVMCState.TastieraDisable | (Is_VMC_Fail()));
				EnaElabTasto |= Is_DA_Vuoto();													// Non elaboro tasti anche quando il DA e' vuoto
				EnaElabTasto |= fVMC_NoLink;													// 06.2020 Non elaboro tasti anche quando il DA e' in NoLink
				EnaElabTasto |= gOrionConfVars.NO_Pulsanti;										// Cesam
				if (EnaElabTasto)																// Non elaboro tasto premuto con tast disable, guasto bloccante
				{
					goto FineElabTasto;															
				}
				tasto = Selez_01;
				goto Elabora_Tasto;																// Elabora pulsante premuto
			}
			/*MR20
			if (IsLCD_SelNonDisponibileMsg() || IsLCD_PrelevaProdottoMsg())
			{																					// Durante msg "Sel Non disp." o "Preleva prod." non prendo altri tasti
				fTastoElab = true;
				goto FineElabTasto;						
			}
			*/
		}
	}
	
	//--  Elaborazione  Pulsante  2 ----
	if (fTastoPmt2 == true)
	{																							// Premuto pulsante erogazione 2
		//if ((ReadErogazStatus(1) & INPROGR_STATUS) != false) goto FineElabTasto;				// Durante l'erogazione non elaboro qui il pulsante premuto
		if (fTastoElab2 == false)																// Pulsante da elaborare
		{
			if (gVMCState.InProgrammaz == false)												// Testo guasti ed elaboro sempre tastiera fuori dalla programmazione
			{
				EnaElabTasto = (gVMCState.TastieraDisable | (Is_VMC_Fail()));
				EnaElabTasto |= Is_DA_Vuoto();													// Non elaboro tasti anche quando il DA e' vuoto
				EnaElabTasto |= fVMC_NoLink;													// 06.2020 Non elaboro tasti anche quando il DA e' in NoLink
				EnaElabTasto |= gOrionConfVars.NO_Pulsanti;										// Cesam
				if (EnaElabTasto)																// Non elaboro tasto premuto con tast disable, guasto bloccante
				{
					fTastoElab2 = true;
					goto FineElabTasto;															
				}
				fTastoElab2 = true;
				tasto = Selez_02;
				goto Elabora_Tasto;																// Elabora pulsante premuto
			}
			/*MR20
			if (IsLCD_SelNonDisponibileMsg() || IsLCD_PrelevaProdottoMsg())
			{																					// Durante msg "Sel Non disp." o "Preleva prod." non prendo altri tasti
				fTastoElab = true;
				goto FineElabTasto;						
			}
			*/
		}
	}
	
	//--  Elaborazione  Pulsante  3 ----
	if (fTastoPmt3 == true)
	{																							// Premuto pulsante erogazione 3
		//if ((ReadErogazStatus(2) & INPROGR_STATUS) != false) goto FineElabTasto;				// Durante l'erogazione non elaboro qui il pulsante premuto
		if (fTastoElab3 == false)																// Pulsante da elaborare
		{
			if (gVMCState.InProgrammaz == false)												// Testo guasti ed elaboro sempre tastiera fuori dalla programmazione
			{
				EnaElabTasto = (gVMCState.TastieraDisable | (Is_VMC_Fail()));
				EnaElabTasto |= Is_DA_Vuoto();													// Non elaboro tasti anche quando il DA e' vuoto
				EnaElabTasto |= fVMC_NoLink;													// 06.2020 Non elaboro tasti anche quando il DA e' in NoLink
				EnaElabTasto |= gOrionConfVars.NO_Pulsanti;										// Cesam
				if (EnaElabTasto)																// Non elaboro tasto premuto con tast disable, guasto bloccante
				{
					fTastoElab3 = true;
					goto FineElabTasto;															
				}
				fTastoElab3 = true;
				tasto = Selez_03;
				goto Elabora_Tasto;																// Elabora pulsante premuto
			}
			/*MR20
			if (IsLCD_SelNonDisponibileMsg() || IsLCD_PrelevaProdottoMsg())
			{																					// Durante msg "Sel Non disp." o "Preleva prod." non prendo altri tasti
				fTastoElab = true;
				goto FineElabTasto;						
			}
			*/
		}
	}
	
	//--  Elaborazione  Pulsante  4 ----
	if (fTastoPmt4 == true)
	{																							// Premuto pulsante erogazione 4
		//if ((ReadErogazStatus(3) & INPROGR_STATUS) != false) goto FineElabTasto;				// Durante l'erogazione non elaboro qui il pulsante premuto
		if (fTastoElab4 == false)																// Pulsante da elaborare
		{
			if (gVMCState.InProgrammaz == false)												// Testo guasti ed elaboro sempre tastiera fuori dalla programmazione
			{
				EnaElabTasto = (gVMCState.TastieraDisable | (Is_VMC_Fail()));
				EnaElabTasto |= Is_DA_Vuoto();													// Non elaboro tasti anche quando il DA e' vuoto
				EnaElabTasto |= fVMC_NoLink;													// 06.2020 Non elaboro tasti anche quando il DA e' in NoLink
				EnaElabTasto |= gOrionConfVars.NO_Pulsanti;										// Cesam
				if (EnaElabTasto)																// Non elaboro tasto premuto con tast disable, guasto bloccante
				{
					fTastoElab4 = true;
					goto FineElabTasto;															
				}
				fTastoElab4 = true;
				tasto = Selez_04;
				goto Elabora_Tasto;																// Elabora pulsante premuto
			}
			/*MR20
			if (IsLCD_SelNonDisponibileMsg() || IsLCD_PrelevaProdottoMsg())
			{																					// Durante msg "Sel Non disp." o "Preleva prod." non prendo altri tasti
				fTastoElab = true;
				goto FineElabTasto;						
			}
			*/
		}
	}

	if (gVMCState.InProgrammaz == false) goto FineElabTasto;
	

	// ======================================================
	// ========  Controllo Tastiera e Touch Screen ==========
	// ======================================================
	
	if (fTastoPmt == true) {																	// Premuto tasto su tastiera a matrice
		// --- Premuto tasto su Tastiera a Matrice  ---
		if (fTastoElab == false)																// Tasto da elaborare
		{
			if (gVMCState.InProgrammaz == false)												// Testo guasti ed elaboro sempre tastiera fuori dalla programmazione
			{
				EnaElabTasto = (gVMCState.TastieraDisable | (Is_VMC_Fail()));
				EnaElabTasto |= Is_DA_Vuoto();													// Non elaboro tasti anche quando il DA e' vuoto
				EnaElabTasto |= fVMC_NoLink;													// 06.2020 Non elaboro tasti anche quando il DA e' in NoLink
				if (EnaElabTasto)																// Non elaboro tasto premuto con tast disable, guasto bloccante
				{
					fTastoElab = true;
					goto FineElabTasto;															
				}
			}
			if (IsLCD_SelNonDisponibileMsg() || IsLCD_PrelevaProdottoMsg())
			{																					// Durante msg "Sel Non disp." o "Preleva prod." non prendo altri tasti
				fTastoElab = true;
				goto FineElabTasto;						
			}
			/*MR19 Non sono usati i tasti + e -
			  if ((Tasto == Tasto_Piu) || (Tasto == Tasto_Meno))
			{																					// Tasti + e - 
				SommaTasti = 0;
				Cnt_Tasto = 0;
				tasto = Tasto;
			}
			*/
			if (Tasto & FOUR_TASTI)
			{																					// Tasto da tastiera a 4 tasti
				SommaTasti = 0;
				Cnt_Tasto = 0;
				tasto = (Tasto & ~(FOUR_TASTI));
			}
			else
			{
				if (gVMCState.InProgrammaz == false)											// Se true, premuto un tasto per iniziare la programmazione: vado a Elabora_Tasto
				{
					Tmr32Start(T_times.DelayTastoTastiera, TOUTPRIMOTASTO);						// Ricarico Timeout tasti premuti per azzerare se abbandonata la sequenza
					tasto = 0;
					if (Cnt_Tasto == 0)
					{
						SommaTasti = Tasto << 4;												// Primo tasto dei due necessari alla selezione
						Cnt_Tasto++;
						VisMessage(mSelezioneNumero, Row_3, 0, 0, true);
						VisualNum_2cifre(Tasto, Row_3);											// Visualizza prima cifra premuta
					} 
					else
					{																			// Premuto Secondo tasto numero selezione
						SommaTasti |= Tasto;
						tasto = Dec16Touint16(SommaTasti);
						Cnt_Tasto = 0;
						SommaTasti = 0;
						T_times.DelayTastoTastiera.tmrEnd = 0;
						VisMessage(mSelezioneNumero, Row_3, 0, 0, true);
						VisualNum_2cifre(tasto, Row_3);											// Visualizza numero selezione
					}
				}
			}
			goto Elabora_Tasto;																	// Elabora tasto premuto
		} else {
			Tmr32Start(T_times.DelayTastoTastiera, TOUTPRIMOTASTO);								// Continuo a ricaricare Timeout tasti premuti fino al suo rilascio
			goto FineElabTasto;
		}
	}
	else
	{
		if (gOrionConfVars.NewTouchStone == false)
		{
			if (t_w_pointer != t_r_pointer)
			{
				// --- Premuto pulsante su Old Touch ---
				tasto = touch_button[t_r_pointer].valore_tasto;
				t_r_pointer = (t_r_pointer+1)%MAX_TASTI;
				TastoDaTouch = true;															// Tasto da elaborare
				ResetToutProgram();																// Carico Timer uscita automatica programmazione
				//MR19 SetToutReturnHome();														// Carico Timer ritorno automatico alla pagina Home
			} 
			else
			{
				goto FineElabTasto;																// Nessun tasto premuto ne' su Touch ne' su tastiera
			}
		}
		else
		{
			if (fTouchPmt == TRUE) 
			{
				// --- Premuto pulsante su New Touch ---
				tasto = touch_button[0].valore_tasto;
				fTouchPmt = FALSE;
				TastoDaTouch = TRUE;															// Tasto da elaborare
				ResetToutProgram();																// Carico Timer uscita automatica programmazione
				//MR19 SetToutReturnHome();														// Carico Timer ritorno automatico alla pagina Home
			}
			else
			{
				goto FineElabTasto;																// Nessun tasto premuto ne' su Touch ne' su tastiera
			}
		}
	}
	
// =============================================================================
// =======   T A S T O    P R E M U T O  ( da tastiera o da  Touch)   ==========
// =============================================================================
Elabora_Tasto:	
		// -----  Elaborazione tasto premuto ----
		if (TastoDaTouch == true) {																// Premuto tasto su Touch
			TastoDaTouch = false;
			if (gVMCState.InProgrammaz == true) {
				gVMCState.ParamModified = true;													// Modificato almeno un parametro
				gVMCState.TastieraDisable = true;												// Programmazione con Touch: disabilito tastiera
			}
			if (((tasto >= FIRSTSELEZ) && (tasto <= LASTSELEZ))) {								// Pulsanti Touch corrispondenti a richiesta selezione
				GestTastiSelez(tasto);															// Eseguo selezione come da tastiera a membrana
			} else {
				//MR19 Touch_Standard(tasto);															// Elaboro tutti gli altri pulsanti in "Touch_Standard()"
			}
		} else if (gVMCState.InProgrammaz == true)
		{																						// Premuto tasto su tastiera: in programmazione vado a Prog_Main
				Buz_start(Milli100, HZ_2000, 0);												// Segnalo tasto premuto
				gVMCState.ParamModified = true;													// Modificato almeno un parametro (?? qui non ho ancora modificato niente)
				tasto = Tasto_NessunTasto;
				Tasto = 0;
				fTastoElab = true;																// Tasto Elaborato
				Prog_Main();																	// Entro in programmazione da tastiera a membrana e LCD ed esco con RESET
		}
		GestTastiSelez(tasto);
		fTastoElab = true;																		// Tasto Elaborato

// ==========================================================================================
// ==========================================================================================
FineElabTasto:		
	
	// --- Old Touch Stone ---
	if ((scan_timeout == 0) && (scan_state != SCAN_IDLE )) {
		scan_state = SCAN_IDLE;
	}
#if (CESAM == false)	
	// Reset counter tasti premuti se non premuto il secondo entro il timeout dal primo
	if ((T_times.DelayTastoTastiera.tmrEnd != 0) 
			&& (TmrTimeout(T_times.DelayTastoTastiera))) {										// Nessun tasto e' stato piu' premuto entro il timeout dal precedente
		if (fTastoPmt == false) {																// Se non e' stato rilasciato il primo tasto non azzero il counter
			Cnt_Tasto = 0;
			SommaTasti = 0;
			T_times.DelayTastoTastiera.tmrEnd = 0;
			ClrVisualAll();																		// Ripristina st-by display
			//MR19 Bicchiere_OnTouch();																// Update preselez. Bicc se erogatore bicchieri installato e pagine utente su touch
		}
	}
#endif
}

/*------------------------------------------------------------------------------------------*\
 Method: Restart_Da_Prog
 	 Funzione di uscita dalla pogrammazione: resetta guasti, predispone nuovo file Config
 	 in EEprom se almeno un tasto di programmazione e' stato premuto e attende reset da 
 	 watch-dog Timer.
 	 
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void Restart_Da_Prog(void)
{
	MIFARE_COMM_DeInit();																		// Per togliere tensione al pin +5 MIF collegato al Boot del uP
  	gVMCState.InProgrammaz = false;
	LCD_Disp_WaitRestart();
	HAL_Delay(TwoSec);																			// Attendo riduzione tensione pin +5V MIfare connesso al pin Boot del uP
#if CESAM
	//MR19 TouchSetPage(T_PAG_WAIT_RESTART);															// Visualizza pagina attesa restart
#else
	Vis_Pag_Home();																				// Visualizza la pagina Home
#endif
	
	ResetGuasti();
	if (gVMCState.ParamModified == true) {
#if SaltaNewTxConf == 0	
		SendConfToEEprom(false);																// Dopo modifiche configurazione memo tutta la config in EEPROM
#endif		
	}
	//HAL_NVIC_SystemReset();		//MR2021 provare il reset da WDT
	WatchDogReset();
}

/*------------------------------------------------------------------------------------------*\
 Method: CheckPulsSelezGratuita
 	 Se premuto, il DA esegue una selezione come selezione di test.
 	 
   IN:  - 
  OUT:  - 
\*------------------------------------------------------------------------------------------*/
void CheckPulsSelezGratuita(void) {
/*	
	if ((Micro1_4 & MicroFree) == 0) {															// Tasto Selezione di Prova premuto
		if (SystemStBy() == true) {																// Sistema in st-by, senza credito
			if (gVMCState.TastoSelezProva == false) {
				gVMCState.TastoSelezProva = true;												// Tasto Selezione di Prova elaborato
				Buz_start(Milli100, HZ_2000, 0);												// Segnalo Selezione di Prova premuto
				gVMC_ConfVars.Gratuito 		= true;
				gVMC_ConfVars.PresMifare 	= false;
				gVMC_ConfVars.ExecutiveSLV 	= false;
			}
		}
	} else {
		gVMCState.TastoSelezProva = false;														// Tasto selezione di prova rilasciato
	}
*/
}

/*------------------------------------------------------------------------------------------*\
 Method: Preselez_Zucchero
 	 Incrementa/decrementa la quantita' di zucchero da erogare e carica il timeout di questa
 	 preselezione.
 	 
   IN:  - 
  OUT:  - true se regolazione eseguita, false se non c'e' erogatore zucchero. 
\*------------------------------------------------------------------------------------------*/
bool Preselez_Zucchero(bool increm)
{
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: Preselez_No_Bicchiere
 	 Set/reset flag "gVMCState.NoBicch" e carico timeout preselezione che altri non e' che
 	 il timeout tasti premuta su tastiera a membrana.
 	 
   IN:  - 
  OUT:  - true se impostazione eseguita, false se non c'e' erogatore bicchieri. 
\*------------------------------------------------------------------------------------------*/
bool Preselez_NoBicc(bool NoBicc)
{
	return false;
}

/*------------------------------------------------------------------------------------------*\
 Method: ResetToutProgram
 	 Ricarica timeout uscita automatica dalla programmazione.
 	 
   IN:  - 
  OUT:  -  
\*------------------------------------------------------------------------------------------*/
void  ResetToutProgram(void) {

	if (gVMCState.InProgrammaz == true) {
		Tmr32Start(gVMCState.ProgramTimeOut, TwoMin);
#if TimeBrevi
		Tmr32Start(gVMCState.ProgramTimeOut, TenSec);
#endif
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: IsToutProgramElapsed
	Test del timeout di uscita automatica dalla programmazione.
 	 
   IN:  - 
  OUT:  -  true se time over, altriemnti false
\*------------------------------------------------------------------------------------------*/
bool  IsToutProgramElapsed(void) {
	
	if (gVMCState.InProgrammaz == true) {
		if (TmrTimeout(gVMCState.ProgramTimeOut)) return true;
	}
	return false;
}

#if DEB_VISTIME
/*------------------------------------------------------------------------------------------*\
 Method: Is_First_Puls_Selez_Premuto
	Rende se e' stato premuto un ulsante di richiesta selezione per non cancellare
	il messaggio su LCD 
 	 
   IN:  - 
  OUT:  -  true se pulsante premuito, altriemnti false
\*------------------------------------------------------------------------------------------*/
bool  Is_First_Puls_Selez_Premuto(void)
{
  return (T_times.DelayTastoTastiera.tmrEnd > 0 ? true : false);
}
#endif













