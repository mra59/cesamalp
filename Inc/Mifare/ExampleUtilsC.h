/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file ExampleUtilsC.h
 *
 *  Source: ExampleUtils.h
 * $Author: mha $
 * $Revision: 1.22 $
 * $Date: Fri Jun 30 11:02:45 2006 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */
//#pragma pack(1)

#ifndef __EXAMPLE_UTILS_C_H__
#define __EXAMPLE_UTILS_C_H__

#ifndef WIN32
	#define HANDLE void*
	#define DWORD  uint32_t
	//#define FALSE (1==0)
	//#define TRUE  (1==1)
	#define EV_RING 0x100
#else
	#include <windows.h>
#endif

/*
 * Checks if the platform is appropriate.
 * If the conditions are not met, the application returns a value unequal to PH_ERR_BFL_SUCCESS
 */
//MR19 phcsBfl_Status_t CheckPlatform();		Non usata

/*
 * Checks if the version register of the PN51x fits to the specified register value
 */
phcsBfl_Status_t CheckSiliconVersion(	phcsBflRegCtl_t* rc_reg_ctl,
								    uint8_t expectedVersion );

/*
 * Changes the serial speed of the PN51x. 
 * Note that the speed of the host interface (UART on PC) has to be also set to the
 * appropriate one.
 */
phcsBfl_Status_t ChangePN51xBaudRate(phcsBflRegCtl_t* rc_reg_ctl,
								    uint32_t baudrate );

/*
 * This example shows how to set the PN51x into soft power down mode.
 * Note: This implementation just works for Windows.
 */
phcsBfl_Status_t PowerDownExample(phcsBflRegCtl_t* rc_reg_ctl);

#define PN51X_TMR_DEFAULT	0x00

#define PN51X_STEPS_10US	0x01
#define PN51X_STEPS_100US	0x02
#define PN51X_STEPS_500US	0x03

#define PN51X_TMR_MAX		0x10
#define PN51X_TMR_OFF		0x20

#define PN51X_START_NOW		0x80

/*
 * This function sets the PN51x timer 
 */
phcsBfl_Status_t SetTimeOut(	phcsBflRegCtl_t *rc_reg_ctl, 
						    phcsBflOpCtl_t *rc_op_ctl, 
						    uint32_t aMicroSeconds, 
						    uint8_t aFlags);

/*
 * This function can be used for debugging purposes 
 */
phcsBfl_Status_t DebugViaSigOut(	phcsBflRegCtl_t *rc_reg_ctl, 
							    uint8_t aSigOutSel);

phcsBfl_Status_t WaitEventCB(phcsBflAux_WaitEventCbParam_t* wait_event_cb_param);

#ifdef __DEBUG__
phcsBfl_Status_t DumpRegisters(phcsBflRegCtl_t *rc_reg_ctl);
#endif

#define SET_COMM_IEN		0x01
#define SET_DIV_IEN			0x02
#define FLUSH_DIV_IRQ		0x04
#define FLUSH_COMM_IRQ		0x08

phcsBfl_Status_t ResetInterrupts(phcsBflRegCtl_t *rc_reg_ctl, 
                               uint8_t aCommIen, uint8_t aDivIen, uint8_t aSettings);

/*
 * This example shows how to wake up the PN51x via RS232
 */
phcsBfl_Status_t WakeUpRs232Example(	phcsBflRegCtl_t* rc_reg_ctl,
								    phcsBflBal_t *bal);

/*
 * Waits for an interrupt on the IRQ line (on the RI pin)
 */
phcsBfl_Status_t WaitForPN51xInterruptRI(void *aPort, int32_t aTimeOut);

/*
 * Wrapper for the WaitForPN51xInterrupt() function
 */
typedef struct
{
	HANDLE			mComHandle;
	uint8_t	mDivEvents;
	uint8_t	mComEvents;

} sCbEvent;

phcsBfl_Status_t WaitForPN51xInterruptCb(phcsBflAux_WaitEventCbParam_t* aParms);

/*
 * Collection of non-operating mode relevant examples
 */
phcsBfl_Status_t ExampleUtils(	void *comHandle,
						        phcsBflRegCtl_t* rc_reg_ctl,
						        phcsBflOpCtl_t* rc_op_ctl,
						        phcsBflBal_t *bal,
						        uint8_t aSettings);

#endif /* __EXAMPLE_UTILS_C_H__ */

