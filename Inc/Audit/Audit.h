/****************************************************************************************
File:
    Audit.h

Description:
    Include file

History:
    Date       Aut  Note
    Set 2012	MR   

 *****************************************************************************************/

#ifndef __Audit__
#define __Audit__

#include "AuditDataDescr.h"

/*--------------------------------------------------------------------------------------*\
Types definition
\*--------------------------------------------------------------------------------------*/

typedef void  (*AuditDataHandlerFn)(char *pnt, uint size);
typedef bool  (*AuditEndTestFn)(void* pntAudit);

typedef struct {
	bool		Audit_for_IoT;					// Audit destinata alla Gateway, inserire AUDIT_CRLF_JSON perche' sia JSON file compatibile 
	bool		loopBodyResponse;
	bit8		txBuffer:1;
	bit8    	txAuditExt:1;                   // tx Audit Ext
	bit8    	configReceived:1;               // We have received configuration
	bit8    	endTx:1;                        // end trasmission
	bit8    	auditDataRead:1;                // Audit data has been read
	bit8    	auditExtDataRead:1;             // Audit Ext data has been read
	uint8_t   	numSwitchComm;
	uint16_t	numBlocks;					    // Audit Ext Num Blocks
	uint16_t	block;						    // Audit Ext Block
	uint16_t	dataBuffTransmitted;	        // 
	uint16_t	dataBuffUsed;					// data Buffer Used
	uint16_t	dataBuffFree;				    // data Buffer Free
	char*   	applDataBuff;					// data Buffer pointer
	uint8_t   	listNumber;					    // Number of list
    char    	*localBufferPnt;                // Local Buffer pointer
	uint8_t   	localBufferSize;				// Local Buffer size
    char    	localBuffer[80];                // Buffers
    char    	buffers[kDDCMPDTS2BuffSize];    // Buffers
    uint8_t		EndTxAudit;						// 
 } sAudit;

typedef union {
		sAudit audit;
		//MR19 sAuditKey auditKey;
} uAuditData;

extern uAuditData uData;

static struct {
  	uint8_t  			flags;
  	uint8_t  			loopMin;
  	uint8_t  			loopMax;
  	uint16_t			dataOffset;
  	uint32_t			lTotale;
  	const void  		*pntAudit;
  	AuditDataHandlerFn 	pntFunction;
  	AuditEndTestFn		pntTest;
  	char 				*workBuffer;
	tAuditTable  		*pntTable;
	uint8_t    			testEnd;
	uint8_t    			endString;
} sAuditCommVars;



/*--------------------------------------------------------------------------------------*\
Prog module interface
\*--------------------------------------------------------------------------------------*/
void AuditInitProfiler(AuditDataHandlerFn handler, AuditEndTestFn auditEnd, char*);
  /* Initialization. Must be called once before using this module */

//void AuditInit(tAuditTable* startAudit, AuditDataHandlerFn handler, AuditEndTestFn auditEnd, char* workArea, bool test);
  /* Initialization. Must be called once before using this module */

void AuditMain(void);
  /* Start the programming module. Return only when Audit table has terminates */

void AuditLoopInit(void);
bool AuditLoopBody(void);                   

void AuditPrint(void);

void AuditFileRevisionStringSet(void);

/*--------------------------------------------------------------------------------------*\
External routines needed
  These routines are product-specific, so must be provided in some other module
  They are the interface between this module and the rest of ths system
\*--------------------------------------------------------------------------------------*/
void AuditGetMsg(char* dst, int msgIx);
  /* Chiamata per leggere il testo del messaggio di configurazione 'msgIx' in dst. */

void AuditDataRead(void* dst, void* src, uint size, uint8_t fileId);
  /* Legge il dato di programmazione all'indirizzo src nel file fileId ad offset dst */

void AuditExec(char*);

void AuditAddressSet(char*);

void AuditAddressLoop(void);

uint8_t AuditStringRead(void*, void*, uint, uint8_t);

void AuditDataRead(void*, void*, uint, uint8_t);

#define AuditItemFile(pnt)  ((pnt)->Mask)
  // The N upper bits in execute mask are used to contain the id of the file
  // containing the data

void AuditInit(tAuditTable* startAudit, AuditDataHandlerFn handler, AuditEndTestFn auditEnd, char* workArea, bool test);

/*--------------------------------------------------------------------------------------*\
Global variables
\*--------------------------------------------------------------------------------------*/
              
#endif /* CCompiler */

//#define kAuditMask  0x0f

#define fResetData                      (sAuditCommVars.flags & 1)
#define Set_fResetData                  (sAuditCommVars.flags |= 1)

#define fCopyData                       (sAuditCommVars.flags & 2)
#define Set_fCopyData                   (sAuditCommVars.flags |= 2)

#define fSumData                        (sAuditCommVars.flags & 4)
#define Set_fSumData                    (sAuditCommVars.flags |= 4)

#define fSubData                        (sAuditCommVars.flags & 8)
#define Set_fSubData                    (sAuditCommVars.flags |= 8)

#define fResultData                     (sAuditCommVars.flags & 0x10)
#define Set_fResultData                 (sAuditCommVars.flags |= 0x10)

#define fDate                           (sAuditCommVars.flags & 0x20)
#define Set_fDate                       (sAuditCommVars.flags |= 0x20)

#define fTime                           (sAuditCommVars.flags & 0x40)
#define Set_fTime                       (sAuditCommVars.flags |= 0x40)

//#endif

#define kTestEndString					true
#define kNoTestEndString				false

#define	TxLastAuditBlk					1
#define	AckedLastAuditBlk				2


