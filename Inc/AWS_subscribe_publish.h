/****************************************************************************************
File:
    AWS_subscribe_publis.h

Description:
    Include file con definizioni e strutture per le funzioni di elaborazione messaggi 
	scambiati con AWS.

History:
    Date       Aut  Note
    Apr 2021	MR   

 *****************************************************************************************/

#ifndef __AWS_subscribe_publis_H
#define  __AWS_subscribe_publis_H


/*--------------------------------------------------------------------------------------*\
Private Declarations 
\*--------------------------------------------------------------------------------------*/
	
// ------  Private Defines -----

#define	CMD_AUD_REQ_WOUT_RESET						 1
#define	CMD_AUD_REQ_W_RESET							 2
#define	CMD_NEW_CONFIG								 3
#define	CMD_STATUS_REQUEST							 4
#define	CMD_STOP_ALARM								 5
#define	CMD_DIS_GETT								 6
#define	CMD_ENA_GETT								 7
#define	CMD_CONFIG_REQ								 8
#define	CMD_EROGAZ_REQ								 9
#define	CMD_CPU_RESET								90
#define	CMD_ACTIVE_OUT								99


#define	MACHINE_CODE_MAX_LEN						10											// Massimo numero di chr del codice macchina
#define	MAX_NUM_CHR									20											// Numero massimo di chr nei quali cercare quanto richiesto nella funzione "Find_ParamValue"
#define	MAX_NUM_BLOCKS_CHR							6											// Numero massimo di chr nei quali cercare quanto richiesto nella funzione "Find_ParamValue"
#define	BLOCKS_NUM_SIZE							 	16
#define	RESULT_BUF_LEN							 	33											// Size buffer elaborazione numeri ricevuti
#define	CMD_9_MAX_PARAM_LEN							6											// Numero massimo di cifre del parametro comando 9


typedef enum
{
	ASCII_NUMERIC = 0,							// Ricerca un valore ASCII numerico 
	ASCII_HEX,									// Ricerca un valore ASCII HEX
	ASCII_LITERAL_HEX,							// Ricerca un valore lettera ASCII nei limiti HEX (A-F)(sia maiuscolo che minuscolo)
	ASCII_ALFANUMERIC,
	ASCII_LITERAL_A_z,							// Ricerca un valore lettera ASCII (sia maiuscolo che minuscolo)
}SEARCH_TYPE;



typedef enum
{
	DECIPHER_OK 				= 0,
	ERR_NO_CRYTPVERS_STRING,
	ERR_CRYPT_VERSION,
	ERR_NO_TRAILER_STRING,
	ERR_NO_PAYLOAD_STRING,
	ERR_WRONG_LENGHT,
	ERR_DECIPHER,
	ERR_NO_CHECKSUM_STRING,
	ERR_WRONG_CRC,
	ERR_NO_CHECKSUM_VALUE,
	ERR_NO_CRYPTO_VALUES,
	NO_CIPHERED_MSG				= 0x5969
}DecipherErr;

typedef enum
{
	PK3_AUDREQ_WOUT_RES 	= 1,
	PK3_AUDREQ_W_RES		= 2,
	PK3_PRICE 				= 5,
	PK3_INHIBIT				= 6,
	PK3_ENABLE				= 7,
	PK3_BOOKED				= 12,
	PK3_BOOKED_DELETE		= 13,
	PK3_START_MACHINE_CYCLE	= 14,
	PK3_LAUNDRO_NEW_PRICE	= 15,
	PK3_LAUNDRO_INH			= 16,
	PK3_LAUNDRO_ENA			= 17,
	PK3_CMD_RESET_PK3		= 90,
	PK3_CMD_REJECTED		= 254
}GW_COMMAND;



IOT_COMM_STATE	MQTTcallbackHandler(char *params);
char* 			Find_ParamValue(char* paramPnt, SEARCH_TYPE ValueType, uint32_t MaxNumChar);
static bool  	GetServicePrice(char *payloadbuff, uint16_t *PriceVal);
static void  	GetTransactionID(char *params);
static uint8_t  EstraeParametroNumerico(char* sourcebuffer, char* stringToFind, uint8_t* resvalue, uint8_t SearchCharNum);
void  			Init_EEAddr_Config_Block_Save(uint8_t NumBlock);


#endif		//  __AWS_subscribe_publis_H


