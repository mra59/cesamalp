/****************************************************************************************
File:
    Dummy.h

Description:
    Include file con definizioni e strutture per compilare senza errori.

History:
    Date       Aut  Note
    Mag 2019	MR   

 *****************************************************************************************/

#ifndef __Dummy__H
#define __Dummy__H

void  		Touch_Msg_PrelevaBevanda(void);
void  		SetTouchNewViscredit(void);
void 		Touch_Msg_VendFail(void);
void  		WDT_Clear(LDD_TDeviceData *);
void		Touch_Gest_Visualizzazioni(void);
bool 		CGMngrHookCoinTokenMix(void);
void 		CGMngrHookTokenVendSet(bool status);
void 		SetActualTouchPage(uint16_t pagina);

#endif		// __Dummy__H


