/*--------------------------------------------------------------------------------------*\
File:
	ExecMaster.h

Description:
    Include file con stati e comandi/risposte del protocollo Executive Master.

History:
	Date	   Aut	Note
    Set 2012	MR   
\*--------------------------------------------------------------------------------------*/

#ifndef __ExecMaster_H_
#define __ExecMaster_H_


/*--------------------------------------------------------------------------------------*\
Private Routines
\*--------------------------------------------------------------------------------------*/

static void Check_VMC_Answer(void);
static void DelayBeforeTxCMD(void);
static void Send_VMC_Command(uint8_t comando);

void  SetMasterTxStatusOnly(void);
void  ClrMasterTxStatusOnly(void);
void  RestartExecMaster(void);
void  ClrMasterSuspend(void);
void  ExecMasterRxDataVMC2(uint8_t responseData);
void  ExecMasterRxErr(void);
void  GestioneVMCStatusState(void);
void  GestioneVMCCreditState(void);
void  VMC_Credito(uint16_t valore);
void  Send_VMC_Credito(uint16_t valore);
void  ExecSendMessage(uint8_t messageLenght, uint8_t Delay_To_Tx, uint32_t ToutWaitResponse);
void  ExecMasterInit(void);


void  GestioneExecutive(void);
void  EXEC_Master_Task(void);


/*--------------------------------------------------------------------------------------*\
Defines Varie
\*--------------------------------------------------------------------------------------*/


#ifndef kTxCreditAgainWhenInh
	#define kTxCreditAgainWhenInh		true						// Alcuni DA perdono la visualizzata del credito quando sono inibiti (Tecnomet 540 MONZA SUPER PLUS)
#endif



#define RX_VMC_NO_VEND_REQUEST			0xFE
#define RX_VMC_VEND_RESPONSE            0x80

#define VMC_INIBIT_FLAG                 0x40
#define VMC_FV_REQ_FLAG     			0x80

#define TX_COMANDO_NO_RESPONSE			0x00
#define TX_COMANDO_OK                   0x01
#define TX_COMANDO_FAIL_PNACK           0xFF
#define NO_LINK_SERIALE                 0xFD


//  Stati Gestione Protocollo EXECUTIVE

#define VMC_STATUS						0x01
#define VMC_CREDIT						0x02
#define WAIT_VEND_AUTOR					0x03
#define WAIT_VEND_NEW_CREDIT			0x04
#define WAIT_VMC_VEND_RESPONSE			0x05
#define WAIT_VISUAL_NEW_CREDIT			0x06
#define WAIT_Card_Extraction			0x07
#define MasterSuspended					0x08
#define RR_Busy_Vend_Nova				0x09


//  Definizioni Comandi Protocollo Executive

#define COMMAND_LEN						1               	// Numero caratteri invio comando a VMC
#define CREDIT_LEN						10          		// Numero caratteri invio visualizzata credito a VMC
#define EXEC_VMC_STATUS					0x31    
#define EXEC_VMC_CREDIT					0x32    
#define EXEC_VMC_VEND					0x33    
#define EXEC_VMC_ACCEPT_DATA			0x38    
#define EXEC_VMC_SYNC					0x39    
#define EXEC_VMC_ADDRESS				0x20
#define EXEC_Master_NACK				0x3F				// Not Acknowledge dal Master Executive
#define PNACK	                 		0xFF               	// Not Acknowledge dalla Periferica

//-----------------------------------------------------------------------------------------------------

#define kRetryCnt                		3					// Numero di ritrasmissioni 
#define kNumToutWaitVMCAnswer			2					// Numero di volte che puo' scadere il  timeout attesa risposta dal VMC
#define kInterByteTimeCommand   		20
#define kInterByteTimeData       		2

#define RetryCounterTOExpired   		0xFE                // Retry Counter TimeOut Expired condition
#define RetryCounterNAckExpired 		0xFF                // Retry Counter Not Acknowledge Expired condition

#define kExecToutNoLink					FIVESEC       		// Timeout attesa risposta che determina il NoLink
#define kExecToutCMD					TenSec       		// Timeout attesa risposta DA a qualsiasi trasmissione del Master ad esclusione del comando VEND
#define kExecToutVend					TwoMin  			// Timeout attesa risposta DA al comando VEND    
#define kExecInterCommand				Milli50        		// Attesa tra un comando e il successivo
#define kExecInterByteDisplayData		Milli2        		// Attesa tra 2 bytes di dato della stringa del credito  
#define kExecToutNovaInVend				OneSec  			// Time frequenza invio STATUS al VMC2 quando VMC Master in Vend    

typedef struct {
  uint8_t  		fByteToSend;
  uint8_t  		fWaitResponse;
  uint8_t			reSendCredit;
  uint8_t 		toInterByteTime;
  uint8_t 		messageLenght;
  uint8_t 		retryCnt;												
  uint8_t 		ToutRetryCnt;
  uint8_t 		byteToSend;
  uint8_t 		VariazExCh;									// Flag Exact-Change variato
  uint8_t* 		txPointer;
  uint32_t  		TimeAttesaRisposta;							// Variabile dove memorizzare il Tout attesa risposta: 10 sec a tutti i CMD e 120 sec al solo comando VEND
  Tmr32Local	tmrWaitResponse;							// Timer attesa risposta dal VMC
  Tmr32Local 	tmrInterByteTime;							// Timer attesa prima di inviare nuovo CMD
  Tmr32Local 	TimeoutSendPriceToVMC;
  Tmr32Local 	TimeoutNoLink;								// Timer per determinare lo stato di NoLink
} ExecMasterVar;                                   



#endif	// __ExecMaster_H_
