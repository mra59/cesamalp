/*--------------------------------------------------------------------------------------*\
File:
	icp.h

Description:
	

History:
	Date	   Aut	Note
	---------- ---	----

\*--------------------------------------------------------------------------------------*/

#ifndef __icp__
#define __icp__

#include "ORION.H"
#include "DevSerial.h"

/*--------------------------------------------------------------------------------------*\
Customization constants
\*--------------------------------------------------------------------------------------*/

#ifndef kICPSlaveCGSupport
#define kICPSlaveCGSupport	true
	// Include support for ICP ChangeGiver slave protocol
#endif

#ifndef kICPSlaveBVSupport
#define kICPSlaveBVSupport	true
	// Include support for ICP BillValidator slave protocol
#endif

#ifndef kICPSlaveUSDSupport
#define kICPSlaveUSDSupport	false
	// Include support for ICP UniversalSatelliteDevice slave protocol
#endif

#ifndef kICPMasterSlaveSupport
#define kICPMasterSlaveSupport  (kICPMasterSupport&(kICPSlaveCGCode|kICPSlaveBVCode|kICPSlaveCPCCode))
//#define kICPMasterSlaveSupport  (kICPMasterSupport&(kICPSlaveCGCode|kICPSlaveBVCode|kICPSlaveCPCCode|kICPSlaveUSDCode))
#endif


#ifndef kIcpRs232Support
#define kIcpRs232Support false
#endif

#ifndef kICPRs232_TM_INTERBYTE
#define kICPRs232_TM_INTERBYTE 60
#endif

#ifndef kICPRs232_TM_RESPONSE
#define kICPRs232_TM_RESPONSE 120
#endif

/*--------------------------------------------------------------------------------------*\
ICP Types & Constants
\*--------------------------------------------------------------------------------------*/
/*MR19
typedef enum {
	ICP_CHG_LINK = 0x01,
	ICP_BILL_LINK = 0x02,
	ICP_CPC_LINK = 0x04,
	ICP_USD1_LINK = 0x08,
	ICP_USD2_LINK = 0x10,
	ICP_USD3_LINK = 0x20
} ET_SLAVELINK;
*/

#define ICP_CHG_ADDR 0x08
#define ICP_CPC_ADDR 0x10
#define ICP_AUD_ADDR 0x18
#define ICP_BILL_ADDR 0x30
#define ICP_USD1_ADDR 0x40
#define ICP_USD2_ADDR 0x48
#define ICP_USD3_ADDR 0x50


/*--------------------------------------------------------------------------------------*\
ICP interface routines
\*--------------------------------------------------------------------------------------*/

void ICPTask(void);


void ICPProt_Init(void);
	// Initialize ICP protocol

//void ICPProt_Close(void);
//	// Close ICP protocol

bool ICPHook_GetSlaveMode(void);
	// Return false if ICP protocol Master

// date&time synchronization Vmc Rhea
void ICPHook_DateTimeSyncSet(void);
bool ICPHook_DateTimeSyncGet(void);
void ICPHook_DateTimeSync(void);
void ICPHook_DateTimeSet(uint8_t *dt);

// Routines chiamate in irq

void ICPTxEmptyInd(void);
void ICPTxComplete(void);
	// Called when it is possible to transmit a new character

void ICPDataInd(uint8_t nData);
	// Called when a data byte has been received

void ICPErrInd(uint8_t nData, uint8_t rxErr);
	// Called when a receiver error has occoured


/*--------------------------------------------------------------------------------------*\
External routines needed
	These routines are product-specific, so must be provided in some other module
	They are the interface between this module and the rest of the system
\*--------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------*\
User hooks
\*--------------------------------------------------------------------------------------*/

#if(kIcpRs232Support == true)
	bool ICPHook_GetRS232Mode(void);
		// Called to check rs232 mode...
#endif

void ICPHook_SetLink(bool fLink);
	// Called to indicate that ICP link has been established or broken.

//MR19 void ICPHook_SetSlaveLink(ET_SLAVELINK nSlaveLinkMask);
	// Signal slave link states


void ICPHook_SetManufactCode(uint8_t *ptData, uint8_t nDataLen);
void ICPHook_SetSerialNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPHook_SetModelNumber(uint8_t *ptData, uint8_t nDataLen);
void ICPHook_SetSWVersion(uint16_t nSwVersion);
	// Called to save Vmc identification informations

void ICPHook_GetManufactCode(uint8_t *ptData, uint8_t dataLen);
void ICPHook_GetSerialNumber(uint8_t *ptData, uint8_t dtaLen);
void ICPHook_GetModelNumber(uint8_t *ptData, uint8_t dataLen);
uint16_t ICPHook_GetSWVersion(void);
	// Paremetri di identificazione


#endif

