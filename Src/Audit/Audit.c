/****************************************************************************************
File:
    Audit.c

Description:
    
    

History:
    Date       Aut  Note
    Set 2012 	MR   

*****************************************************************************************/
#include <stdio.h>        
#include <string.h>
#include <stdlib.h>

#include "ORION.H"
//#include "AuditDataDescr.h"
#include "AuditStrutture.h"
#include "Audit.h"
#include "IICEEP.h"
#include "icpBILLProt.h"
#include "icpCHGProt.h"
#include "icpCPCProt.h"
#include "icpChg.h"
#include "ICPCHGHook.h"
#include "Funzioni.h"

#include "Dummy.h"

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#pragma pack(4)							//MR19 todo: da verificare - inserito per non avere warnings "use of address of unaligned structure member"
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


/* Global variables */

  uint32_t  SaveAuditPntTable;

/* Internal variables */

/* Private functions */
static void 		AuditString(void);
static void 		AuditData(void);
static void 		AuditBeginLoop(void);
static void 		AuditBeginLoopIx(void);
static void 		AuditLoopIndex(void);
static void 		AuditFreeField(void);
static void 		AuditEndLoop(void);
static void 		AuditExecFunction(void);
static void 		AuditEndString(void);
//static uint16_t 		ProgHookGetDPP(void);
static void 		AuditVerifyPrice(void);
static void			SkipStructs(uint8_t);
static void 		PriceDiscData(void);

extern const tAEndLoop	tPA1_99_05;
extern const tAEndLoop  tLA1_99_0_06;
extern const tAEndLoop  tLA1_99_1_08;

#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
static void 			AuditTestEvent (void);
static void 			AuditMessage (void);
static void  			FillTempBuffer(char *, uint8_t);
extern const tAEndLoop  tEA1_04;
//extern const char 		DESCR_EA1[];
#endif

extern const char 		AUDIT_CRLF[];
extern const char 		AUDIT_JSON_CRLF[];

extern bool 			ca_15_mdb;

extern  const tATestLA1     tLA1_2_0;
extern  const tAEndLoop     tLA1_1_08;
extern  const tAEndLoop     tLA1_2_08;
extern  const tAEndLoop     tLA1_3_08;
extern  const tAEndLoop     tLA1_4_08;

//extern  const tAEndLoop     tLA1_C_08;
//extern  const tAEndLoop     tLA1_P_08;
extern  const tAEndString   tCA15_03;

extern const tAEndLoop    	tPA1_05;
extern const tAEndLoop    	tLA1_0_06;



#if  TestAudStruct
  extern bool StructToBreak(const void*);
#endif



/*--------------------------------------------------------------------------------------*\
Method: AuditInit
  Initialize programming module
Parameters:
  startAudit -> ptr to Audit structure
  AuditFunction  ->  ptr to a function.
\*--------------------------------------------------------------------------------------*/
void AuditInit(tAuditTable* startAudit, AuditDataHandlerFn auditFunction, AuditEndTestFn auditTest, char* workArea, bool test)
{
  sAuditCommVars.pntTable = startAudit;                    										// Primo indirizzo della tabella di costanti da scandire
  sAuditCommVars.testEnd = test;
  sAuditCommVars.pntAudit = sAuditCommVars.pntTable->pnt;                 						// prima struttura AUDIT da gestire
  sAuditCommVars.pntFunction = auditFunction;              										// funzione da chiamare per gestire i dati Audit
  sAuditCommVars.pntTest = auditTest;                      										// funzione da chiamare per test Audit 
  sAuditCommVars.workBuffer= workArea;
  //MR19 RigheAudit = 0;
}


/*--------------------------------------------------------------------------------------*\
Method: AuditMain
  Audit module loop
\*--------------------------------------------------------------------------------------*/
void AuditMain(void) {
  AuditLoopInit();
  while(AuditLoopBody());                  /* loop gestione */
}

/*--------------------------------------------------------------------------------------*\
Method: AuditLoopInit
  Initialize programming loop
\*--------------------------------------------------------------------------------------*/
void AuditLoopInit(void)
{
	sAuditCommVars.dataOffset 	= 0;
  	sAuditCommVars.loopMin 		= 0;
  	sAuditCommVars.loopMax 		= 0;
}

/*--------------------------------------------------------------------------------------*\
Method: AuditLoopBody
  Handle programming loop
Return:
  false if end of programming
\*--------------------------------------------------------------------------------------*/
bool AuditLoopBody(void) {
	
	if(sAuditCommVars.pntAudit == NULL)
	{
		return false;									// The end
	}
  
//  if( (*sAuditCommVars.pntTest)(&sAuditCommVars.pntAudit) == false ) return false;

  WDT_Clear(gVars.devWDT);  
  sAuditCommVars.flags = 0;
  sAuditCommVars.endString = false;
  sAuditCommVars.testEnd = true;

#if  TestAudStruct
  // In "StructToBreak" mettere l'indirizzo della tabella
  // dove si vuole fermare l'esecuzione 
  if (StructToBreak(sAuditCommVars.pntAudit)) {
	  sAuditCommVars.testEnd = true;					// Istruzione inserita solo per poter mettere un bkpnt
  }
#endif
  
  switch(*(uint8_t*)sAuditCommVars.pntAudit) {
    case AUDIT_STRING :            AuditString();            			break;
    case AUDIT_DATA :              AuditData();              			break;
    case AUDIT_BEGIN_LOOP :        AuditBeginLoop();        			break;
    case AUDIT_BEGIN_LOOP_INDEX :  AuditBeginLoopIx();        			break;
    case AUDIT_LOOP_INDEX :        AuditLoopIndex();         			break;
    case AUDIT_FREE_FIELD :        AuditFreeField();         			break;
    case AUDIT_END_LOOP :          AuditEndLoop();           			break;
    case AUDIT_EXEC_FUNCTION :     AuditExecFunction();      			break;
    case AUDIT_END_STRING :        AuditEndString();         			break;
    case AUDIT_RESET_DATA :        Set_fResetData;     AuditData();    	break;
    case AUDIT_COPY_DATA :         Set_fCopyData;      AuditData();    	break;
    case AUDIT_SUM_DATA :          Set_fSumData;       AuditData();    	break;
    case AUDIT_SUB_DATA :          Set_fSubData;       AuditData();    	break;
    case AUDIT_RESULT_DATA :       Set_fResultData;    AuditData();    	break;
    case AUDIT_DATE :              Set_fDate;          AuditData();    	break;
    case AUDIT_TIME :              Set_fTime;          AuditData();    	break;
    case AUDIT_PRICEDISC_DATA: 	   PriceDiscData();	   AuditData();		break;
    case AUDIT_TEST_PRICE :        AuditVerifyPrice();         			break;
    
    
#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
    case AUDIT_TEST_EVENT :        AuditTestEvent();         			break;
    case AUDIT_MESS_INDEX :        AuditMessage();           			break;
#endif
  }      

  if(sAuditCommVars.testEnd == true){
    sAuditCommVars.pntTable++;                    
    sAuditCommVars.pntAudit = sAuditCommVars.pntTable->pnt;                 // successiva struttura AUDIT da gestire 

  }
  return true;
}

/*--------------------------------------------------------------------------------------*\
Method: AuditAddressSet
Parameters:
\*--------------------------------------------------------------------------------------*/
void AuditAddressSet(char* addr)
{
	//MR19 (char*)(sAuditCommVars.pntAudit) = addr;			// Non utilizzata
}

/*--------------------------------------------------------------------------------------*\
Method: AuditAddressLoop
Parameters:
\*--------------------------------------------------------------------------------------*/
void AuditAddressLoop(void)
{
	//MR19  (uint8_t*)(sAuditCommVars.pntAudit) -= 5;		// Non utilizzata
}

/*--------------------------------------------------------------------------------------*\
Method: AuditString 
  Gestione  Stringhe
\*--------------------------------------------------------------------------------------*/
static void AuditString(void) {
	
	tAString 	*pnt;
	uint8_t		dataLen;
	
	pnt = (tAString *)sAuditCommVars.pntAudit;
	//if (!AuditStringRead(sAuditCommVars.workBuffer, (void *)pnt->pnt, pnt->Length, AuditItemFile(pnt))) return;
	dataLen = AuditStringRead(sAuditCommVars.workBuffer, (void *)pnt->pnt, pnt->Length, AuditItemFile(pnt));
	if (dataLen == 0) return;
	//(*sAuditCommVars.pntFunction)(sAuditCommVars.workBuffer, (uint16_t)pnt->Length);
	(*sAuditCommVars.pntFunction)(sAuditCommVars.workBuffer, dataLen);
}

/*--------------------------------------------------------------------------------------*\
Method: AuditTestPrice
  ritorna il valore del parametro MRPPrices.Prices99.MRPPricesList99
\*--------------------------------------------------------------------------------------*/
static uint16_t AuditTestPrice(void) {

	//MR19 tATestData *pnt;			// Variabile set ma non usata
	uint16_t Valore;
	char *pntDato;
	char *pntValore;
	char *pntUpperLimit;																		// Address max tabella prezzi la cui lunghezza non e' piu' fissa ma determinata da "LenListaPrezzi"

	pntUpperLimit = (char *)&MRPPrices.Prices99.MRPPricesList99[LenListaPrezzi];
	//MR19 pnt = (tATestData *)sAuditCommVars.pntAudit;  												// indirizzo struttura
	pntDato = (char *)&MRPPrices.Prices99.MRPPricesList99[0];
	pntValore = (char *)&Valore; 
	pntDato += (sAuditCommVars.loopMin *(sAuditCommVars.dataOffset * 2));
	if (pntDato >= pntUpperLimit) {																// Per sapere se si punta a un elemento insesistente della tabella
		return PrezzoNonDisponibile;
	}
	ReadDato(pntValore, pntDato, 2);      														// lettura in lValore del dato
	return (Valore);
}

/*---------------------------------------------------------------------------------------------*\
Method: AuditVerifyPrice
	Verifica il Prezzo: se � valido crea il relativo ID altrimenti salta all'ID del prezzo
	successivo.
	
	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
static void AuditVerifyPrice(void) {
	
	tATestData *pnt;
	
	pnt = (tATestData *)sAuditCommVars.pntAudit;							// Indirizzo struttura in elaborazione
	if ((AuditTestPrice() == PrezzoNonDisponibile)) {
		switch (pnt->Addr) {
			case 1:
				sAuditCommVars.pntAudit = &tPA1_05;    	 break;
			case 2:
				sAuditCommVars.pntAudit = &tLA1_0_06;    break;
			case 3:
				sAuditCommVars.pntAudit = &tLA1_1_08;    break;
			case 4:
				sAuditCommVars.pntAudit = &tLA1_2_08;    break;
			case 5:
				sAuditCommVars.pntAudit = &tLA1_3_08;    break;
			case 6:
				sAuditCommVars.pntAudit = &tLA1_4_08;    break;
		}
	  	sAuditCommVars.pntTable = (tAuditTable*)SaveAuditPntTable;
		sAuditCommVars.testEnd = false;										// Non incrementare "sAuditCommVars.pntAudit" nella "AuditLoopBody"
	}
}

/*--------------------------------------------------------------------------------------*\
Method: AuditData 
  Gestione  Dati
\*--------------------------------------------------------------------------------------*/
static void AuditData(void) {
	
	tAData 		*pnt;
	uint8_t  	length;
	uint8_t  	sizedata;
	char 		*pntDato;
	char 		*pntValore;
	uint32_t 		lValore;
	uint8_t 	lung;
	char 		buffer[10];
	//uint8_t decimals;

	pnt = (tAData *)sAuditCommVars.pntAudit;
	length = pnt->Length & 0x0f;

	if (fResetData) {
		sAuditCommVars.lTotale = 0;
		return;
	}
	if (!(fResultData)) {
		lValore = 0;
		sizedata = sizeof(uint32_t);
		if ((pnt->Length & DATA_INT) != 0) {
			sizedata = sizeof(uint16_t);
		} else {
			if ((pnt->Length & DATA_BYTE) != 0) {
				sizedata = sizeof(uint8_t);
			}
		}
		pntValore = (char *)((char*)&lValore); 
		pntDato = pnt->pnt;
		if ((pnt->Length & DATA_SI_INDEX) != 0) {
			pntDato += (sAuditCommVars.loopMin *(sAuditCommVars.dataOffset * sizedata));
		}
		AuditDataRead(pntValore, pntDato, sizedata, AuditItemFile(pnt));
	}

	if (fCopyData) {
		sAuditCommVars.lTotale = lValore;
	} else { 
		if (fSumData) {
			sAuditCommVars.lTotale += lValore;
		} else {
			if (fSubData) {
				if (lValore <= sAuditCommVars.lTotale) {
					sAuditCommVars.lTotale -= lValore;
				} else {
					sAuditCommVars.lTotale = 0;
				}
			} else {
				if (fResultData) {
					lValore = sAuditCommVars.lTotale;
				}
					if ((pnt->Length & DATA_SI_ZERI) == 0) {
						sprintf(sAuditCommVars.workBuffer+1,"%-lu",lValore);
						lung = strlen(sAuditCommVars.workBuffer+1);
						if (lung <= length) {
							pntDato = &sAuditCommVars.workBuffer[0];
						} else {
							pntDato = &sAuditCommVars.workBuffer[lung-length];
						}
					} else {
						sprintf(sAuditCommVars.workBuffer,"%012lu",lValore);
						pntDato = &sAuditCommVars.workBuffer[12-length-1];
					}
					if (fDate) {
						memcpy(buffer,(pntDato+1),6);
						*(pntDato+1) = buffer[4];
						*(pntDato+2) = buffer[5];
						*(pntDato+3) = '-';
						*(pntDato+4) = buffer[2];
						*(pntDato+5) = buffer[3];
						*(pntDato+6) = '-';
						*(pntDato+7) = buffer[0];
						*(pntDato+8) = buffer[1];
						*(pntDato+9) = 0;
					}
					if (fTime) {
					memcpy(buffer,(pntDato+1),4);
		 			*(pntDato+1) = buffer[0];
		 			*(pntDato+2) = buffer[1];
		 			*(pntDato+3) = ':';
		 			*(pntDato+4) = buffer[2];
		 			*(pntDato+5) = buffer[3];
		 			*(pntDato+6) = 0;
					}
					if (fDate || fTime) {
						pntDato++;
					} else {
						if (pnt->Mask & 0x40) {
							memcpy(&sAuditCommVars.workBuffer[0],pntDato+1,lung);
							pntDato = &sAuditCommVars.workBuffer[0];
							pntDato[lung] = nil;
						} else {
							*(pntDato) = '*';
						}
					}
					length = strlen( pntDato );
//				}
				(*sAuditCommVars.pntFunction)(pntDato, (uint16_t)length);
			}
		}
	}
}

/*------------------------------------------------------------------------------------------*\
 Method: PriceDiscData
   Utilizzata per le liste LA: set flag SubData se le liste sono Sconti, altrimenti set flag
   CopyData se le liste sono prezzi.

   IN:  -  opzione Prezzi/Sconti gOrionConfVars.ListaPrezzo
  OUT:  -  SubData se liste Sconti o CopyData se Liste Prezzi
\*------------------------------------------------------------------------------------------*/
static void PriceDiscData(void) {
	
	if (gOrionConfVars.ListaPrezzo) {
		// Le liste sono PREZZI
		Set_fCopyData;
	} else {
		// Le liste sono SCONTI
		Set_fSubData;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: AuditBeginLoop 
  Gestione  Inizio Loop
\*--------------------------------------------------------------------------------------*/
static void AuditBeginLoop(void)
{
  tABeginLoop *pnt;
    
  pnt = (tABeginLoop *)sAuditCommVars.pntAudit;

  sAuditCommVars.loopMin = pnt->Min;
  sAuditCommVars.loopMax = pnt->Max;
  sAuditCommVars.dataOffset = pnt->DataOffset;

  SaveAuditPntTable = (uint32_t) sAuditCommVars.pntTable;
  SaveAuditPntTable +=4;				  // I loop successivi al primo passaggio devono essere eseguiti dalla seconda struttura del loop
  	  	  	  	  	  	  	  	  	  	  // Qui e' corretto aggiungere +4 perche' fa riferimento alla struttura "tAuditTable" che e' composta
  	  	  	  	  	  	  	  	  	  	  // da indirizzi a 4 bytes
}

/*-----------------------------------------------------------------------------------------*\
Method: AuditBeginLoopIndex 
	Gestione  Inizio Loop: la differenza con la struttura "tABeginLoop" consiste nel fatto
	che il parametro da mettere in loopMax deve essere letto tramite "pnt" che punta ad
	un indirizzo di RAM.
\*-----------------------------------------------------------------------------------------*/
static void AuditBeginLoopIx(void) {
  tABeginLoopIx *pnt;
    
  pnt = (tABeginLoopIx *)sAuditCommVars.pntAudit;

  sAuditCommVars.loopMin = pnt->Min;
  sAuditCommVars.loopMax = *((uint32_t*)(pnt->Max));
  if (sAuditCommVars.loopMax == NULL_VALUE) sAuditCommVars.loopMax++;
  sAuditCommVars.dataOffset = pnt->DataOffset;

  SaveAuditPntTable = (uint32_t) sAuditCommVars.pntTable;
  SaveAuditPntTable +=4;				  // I loop successivi al primo passaggio devono essere eseguiti dalla seconda struttura del loop
  	  	  	  	  	  	  	  	  	  	  // Qui e' corretto aggiungere +4 perche' fa riferimento alla struttura "tAuditTable" che e' composta
  	  	  	  	  	  	  	  	  	  	  // da indirizzi a 4 bytes
}

/*--------------------------------------------------------------------------------------*\
Method: AuditLoopIndex 
  Gestione  Indice Loop
\*--------------------------------------------------------------------------------------*/
static void AuditLoopIndex(void) {
	
  tALoopIndex	*pnt;
  uint8_t  		length;
  char 			*pntDato;
  uint32_t 		lValore;
  uint8_t 		lung;

  pnt = (tALoopIndex *)sAuditCommVars.pntAudit;
  lValore = sAuditCommVars.loopMin + pnt->LoopOffset;
  length = pnt->Length & 0x0f;
/*																									// 06.05.15 Tolti per pulizia files
  if((pnt->Mask & PRINTER) != 0) {
	  memset(sAuditCommVars.workBuffer,LocalGetAttr(kLocCifraIntera),length);
	  sAuditCommVars.workBuffer[length] = 0;
  }
  if((pnt->Mask & MSG_FORMAT) != 0 || (pnt->Mask & PRINTER) != 0) {
	  StrInsertVal(sAuditCommVars.workBuffer, lValore, 0);											// 06.05.15 Tolti per pulizia files
	  pntDato = sAuditCommVars.workBuffer;
	  length = strlen( pntDato );
  } else {
*/
	  if( (pnt->Length & DATA_SI_ZERI) == 0 ){
		  sprintf(sAuditCommVars.workBuffer+1,"%-lu",lValore);
		  lung = strlen(sAuditCommVars.workBuffer+1);
		  if( lung <= length ){
			  pntDato = &sAuditCommVars.workBuffer[0];
		  } else {
			  pntDato = &sAuditCommVars.workBuffer[lung-length];
		  }
	  } else {
		  sprintf(sAuditCommVars.workBuffer,"%012lu",lValore);
		  pntDato = &sAuditCommVars.workBuffer[12-length-1];
	  }
	  *(pntDato) = '*';
	  length = strlen( pntDato );
//  }

  (*sAuditCommVars.pntFunction)(pntDato, (uint16_t)length);
}


/*--------------------------------------------------------------------------------------*\
Method: AuditFreeField 
  Gestione  Campo Vuoto
\*--------------------------------------------------------------------------------------*/
static void AuditFreeField(void)
{
	//MR19 tAFreeField *pnt;			// Set but never used

	//MR19 pnt = (tAFreeField *)sAuditCommVars.pntAudit;
  	sAuditCommVars.workBuffer[0] = '*';
  	(*sAuditCommVars.pntFunction)(sAuditCommVars.workBuffer, 1);
}


/*--------------------------------------------------------------------------------------*\
Method: AuditEndLoop 
  Gestione  Fine Loop
\*--------------------------------------------------------------------------------------*/
static void AuditEndLoop(void) {
	
  tAEndLoop *pnt;
  uint8_t  i, NextID_AfterEventLoop;
    
  pnt = (tAEndLoop *)sAuditCommVars.pntAudit;
  
  if( sAuditCommVars.loopMin == sAuditCommVars.loopMax-1 ){
    sAuditCommVars.loopMin = 0;
    sAuditCommVars.loopMax = 0;
    if((pnt->Mask & END_STRING) != 0) sAuditCommVars.endString = true;
    
    NextID_AfterEventLoop = pnt->TableID_Offset;								// Loop aggiunto per riportare sAuditCommVars.pntTable a puntare alla tabella
    if (NextID_AfterEventLoop) {												// successiva al loop appena terminato. Avendo aggiunto un test per saltare elementi del loop,
    	sAuditCommVars.pntTable = (tAuditTable*)SaveAuditPntTable;				// il pointer potrebbe trovarsi a puntare l'ultima tabella del loop o la prima, a seconda se
        for (i=0; i<NextID_AfterEventLoop; i++) {								// l'ultimo elemento era stato elaborato oppure no, come ad esempio eventi vuoti o non validi.
            sAuditCommVars.pntTable++;
        }
        sAuditCommVars.pntAudit = sAuditCommVars.pntTable->pnt;                 // Prossima struttura AUDIT da gestire 
	  	sAuditCommVars.testEnd = false;											// Non incrementare "sAuditCommVars.pntAudit" nella "AuditLoopBody"
    }
  } else {
	  	  sAuditCommVars.loopMin++;
	  	  sAuditCommVars.pntAudit = pnt->pnt;
	  	  sAuditCommVars.testEnd = false;										// Non incrementare "sAuditCommVars.pntAudit" nella "AuditLoopBody"
	  	  sAuditCommVars.pntTable = (tAuditTable*)SaveAuditPntTable;			// pntTable punta alla seconda struttura del loop (la prima e' quella che punta a AUDIT_BEGIN_LOOP)
  }
}

/*--------------------------------------------------------------------------------------*\
Method: AuditExecFunction 
  Gestione  Esecuzione Funzione
\*--------------------------------------------------------------------------------------*/
static void AuditExecFunction(void)
{
  tAExecFunction *pnt;

  pnt = (tAExecFunction *)sAuditCommVars.pntAudit;

  AuditExec((char*)pnt);
}


/*--------------------------------------------------------------------------------------*\
Method: AuditEndString 
  Gestione  Fine Stringa
\*--------------------------------------------------------------------------------------*/
static void AuditEndString(void) {
	
	tAEndString *pnt;
    
	pnt = (tAEndString *)sAuditCommVars.pntAudit;
	if (JSON_Format == true)
	{
		memcpy(sAuditCommVars.workBuffer, &AUDIT_JSON_CRLF[0], strlen(&AUDIT_JSON_CRLF[0]));
		//MR19 RigheAudit++;
		(*sAuditCommVars.pntFunction)(sAuditCommVars.workBuffer, strlen(&AUDIT_JSON_CRLF[0]));
	}
	else
	{
		memcpy(sAuditCommVars.workBuffer, &AUDIT_CRLF[0], strlen(&AUDIT_CRLF[0]));
		//MR19 RigheAudit++;
		(*sAuditCommVars.pntFunction)(sAuditCommVars.workBuffer, strlen(&AUDIT_CRLF[0]));
	}

	
	if((pnt->Mask & END_STRING) != 0) {
		sAuditCommVars.endString = true;
	}
}


#if kOnOffEvents || kApplExChEvents	|| kApplRTCEventSupport
/*--------------------------------------------------------------------------------------*\
Method: AuditTestEvent
	Test del dato: se � uguale a zero salta alla struttura indicata, altrimenti 
	continua con la struttura successiva.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
static void AuditTestEvent (void) {

	uint8_t TipoEvento;
	uint8_t *pntDato;
	uint8_t *pntValore;

	pntDato = IICEEPAuditOffset(TypeEventList);
	pntDato += (sAuditCommVars.loopMin *(sAuditCommVars.dataOffset * 1));
	pntValore = (uint8_t *)&TipoEvento; 
	ReadDato ((char *)pntValore, (char *)pntDato, sizeof(TipoEvento));				// lettura del dato
	if (TipoEvento == kNoEvent || TipoEvento > EventNum) {							// Non c'e' evento o evento sconosciuto
		sAuditCommVars.pntAudit = &tEA1_04;											// Punto direttamente alla funzione "AUDIT_END_LOOP"
		sAuditCommVars.testEnd = false;												// Non incrementare "sAuditCommVars.pntAudit" nella "AuditLoopBody"
	}
}
/*--------------------------------------------------------------------------------------*\
Method: AuditMessage
	"TypeEventList" indica qual'e' l'ID dell'evento, contenuto nell'Array "DESCR_EA1[]".
	Nell'array i vari ID sono separati dal chr '?'.
	
	IN:	  - pointer alla struttura tAMessIndex e TypeEventList
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
static void AuditMessage (void) {

	tAMessIndex *pnt;
	uint8_t TipoEvento;
	uint8_t *pntDato;
	uint8_t *pntValore;
#define  TempBuffLen  4																// Lunghezza del buffer temporaneo se non si dovesse trovare l'ID dell'evento
	
	uint8_t	i, j, count;
	char	TempBuff[20];															// Buffer temporaneo di estrazione dell'ID

	pntDato = IICEEPAuditOffset(TypeEventList);
	pntDato += (sAuditCommVars.loopMin *(sAuditCommVars.dataOffset * 1));
	pntValore = (uint8_t *)&TipoEvento; 

	ReadDato ((char *)pntValore, (char *)pntDato, sizeof(TipoEvento));				// Lettura tipo di evento

	pnt = (tAMessIndex *)sAuditCommVars.pntAudit;
	pntValore = pnt->pnt;															// Pointer all'array DESCR_EA1[]
	
	// Cerco le stringa tra due '?' che corrisponda all'evento che devo
	// inviare all'audit.
	count = 0;
	j = 0;
	FillTempBuffer(&TempBuff[0], sizeof(TempBuff));
	
	for (i=0; i < (uint8_t)pnt->Length; i++) {
		if (*(pntValore+i) != '?') {
			TempBuff[j++] = *(pntValore+i);											// Memo carattere nel buffer temporaneo
			if (j > sizeof(TempBuff)) {
				FillTempBuffer(&TempBuff[0], sizeof(TempBuff));						// Buffer temporaneo riempito e non trovato ancora il chr '?'
				j = 0;																
				break;
			}
		} else {
			// Trovato chr '?'
			count ++;
			if (count == TipoEvento) {
				// Ho l'ID dell'evento nel buffer temporaneo: esco
				break;
			} else {
				// L'evento che ho nel buffer temporaneo non e' quello
				// puntato da TipoEvento: cerco il prossimo ID
				FillTempBuffer(&TempBuff[0], sizeof(TempBuff));
				j = 0;
			}
		}
	}
	if (j == 0) j = TempBuffLen;													// Se non ho trovato l'ID dell'evento passo all'audit il buffer di default
	pntValore = (uint8_t *)&TempBuff;													// Pointer al buffer temporaneo
	memcpy(sAuditCommVars.workBuffer, pntValore, j);
	(*sAuditCommVars.pntFunction)(sAuditCommVars.workBuffer, j);
}

/*--------------------------------------------------------------------------------------*\
Method: FillTempBuffer
	Riempie il buffer temporaneo di caratteri '-' e imposta lunghezza del buffer.
	
	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
static void  FillTempBuffer(char * pntBuff, uint8_t bufflen) {
	
	uint8_t  s;
	for (s=0; s<bufflen-1; s++) {
		* (pntBuff+s) = '-';
	}
	* (pntBuff+s) = 0x0;
}

#endif

/**********************************************************/
/*  Lettura messaggio                                     */
/**********************************************************/
uint8_t  AuditStringRead(void* dst, void* src, uint16_t size, uint8_t fileId) {

	uint16_t 		*pnt;
	uint8_t		j, k;
	bool		chrSignif = false;
	uint8_t		*DestBuffPnt;
	uint8_t		*SrcBuffPnt;
	//MR19 tAString 	*pntsrc;			// Declared but never used
    
	//MR19 pntsrc = (tAString *)sAuditCommVars.pntAudit;
	DestBuffPnt = dst;
	SrcBuffPnt = src;
	pnt = src;
	if( (fileId & 1) != 0 ) {
		ReadMess((char*)dst, (int) src);
	} else {
		if (IsStruct_ID106(sAuditCommVars.pntAudit) == true) {									// New ID106: stringa ASCII alla quale vanno tolti gli zeri non significativi
			for (j=0, k=0; j<EE_CodeMacchinaLen; j++) {
				if (chrSignif == FALSE) {
					if (*(SrcBuffPnt+j) != '0') {
						*(DestBuffPnt+k) = *(SrcBuffPnt+j);
						k++;
						chrSignif = true;
					}
				} else {
					*(DestBuffPnt+k) = *(SrcBuffPnt+j);
					k++;
				}
			}
			if (chrSignif == FALSE) {															// Codice Macchina = 0
				*((char*) dst)= *((char*) src);
				size = 1;
			} else {
				size = k;
			}
		} else {
			//if (*pnt == 0x00) return false;
			if (*pnt == 0x00) return 0;
			memcpy((char*) dst, (char*) src, size);
		}
	}
	return size;
}

/**********************************************************/
/*  Lettura dato                                          */
/**********************************************************/
void AuditDataRead(void* dst, void* src, uint16_t size, uint8_t fileId) {
	ReadDato((char *)dst, (char*)src, size);
}

/*--------------------------------------------------------------------------------------*\
Method: AuditExec
	Esegue la funzione presente nella struttura "tATestLA1".

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/

void AuditExec(char* pnt) {
	
	  tAExecFunction 	*pntExec;
	  tATestLA1 		*pntTestLA1;
	  uint8_t* 			pntList;
	  uint8_t			i;
	  uint16_t 			valore;
	  uint32_t 			AudData;

	  pntExec = (tAExecFunction *)pnt;
	  pntTestLA1 = (tATestLA1 *)pnt;
	  switch(pntExec->Function) {

#if !kAuditLA1OldModeSupport

/*--------------------------------------------------------------------------------------*/
	  	  // Salta l'intera Lista LA1-x se gli array di contatori delle selezioni 
	  	  // scontate contengono zero. 
		  
	  case kTestLA1:
		  switch(pntTestLA1->List) {
  		  	  case kLA1_0:
  		  		  pntList = IICEEPAuditOffset(NumSelFullPrice[0]);
  		  		  break;
    		
	  	  	  case kLA1_1:
	  			  pntList = IICEEPAuditOffset(NumSelDiscount1[0]);
	  	 		  break;
				
	  	  	  case kLA1_2:
	  	 		  pntList = IICEEPAuditOffset(NumSelDiscount2[0]);
	  	 		  break;
        		
	  	  	  case kLA1_3:
	  	  		  pntList = IICEEPAuditOffset(NumSelDiscount3[0]);
	  	  		  break;
        		
	  	  	  case kLA1_4:
	  	  		  pntList = IICEEPAuditOffset(NumSelDiscount4[0]);
	  	  		  break;
		  }
		  for(i=0; i<MRPPricesListLenght99; i++,pntList+=sizeof(valore)) {
			  ReadDato((char*)&valore, (char *)pntList, sizeof(valore));      
			  if( valore != 0 ) break;
		  }
		  if( i == MRPPricesListLenght99 ) {
			  SkipStructs(pntTestLA1->TableID_Offset);
		  }
		  break;
		  
#endif
	  		  

#if (_IcpMaster_ == true)
/*--------------------------------------------------------------------------------------*/
	  		// Salta l'intera struttura se non c'e' alcun Bill Reader MDB connesso.
	  		  
	  		case kTestBillReader:
	  			for (i=0; i<sizeof(ICPBILLInfo.BillInfo.aManufactCode); i++) {
	  				if (ICPBILLInfo.BillInfo.aManufactCode[i] != NULL_VALUE) return;											// C'e' almeno un carattere diverso da NULL
	  			}
	  			for (i=0; i<sizeof(ICPBILLInfo.BillInfo.aSerialNumber); i++) {
	  				if (ICPBILLInfo.BillInfo.aSerialNumber[i] != NULL_VALUE) return;											// C'e' almeno un carattere diverso da NULL
	  			}
	  			SkipStructs(pntTestLA1->TableID_Offset);
	  			break;
	  			
/*--------------------------------------------------------------------------------------*/
	  	  	// Salta l'intera struttura "CA1" (dati da Change Giver MDB) se non 
	  	  	// c'e' alcun changer MDB connesso.
	  	  	  		  
	  	  	case kTestCA1:
	  	  		for (i=0; i<sizeof(ICPCHGInfo.ChgInfo.aManufactCode); i++) {
	  	  			if (ICPCHGInfo.ChgInfo.aManufactCode[i] != NULL_VALUE) return;												// C'e' almeno un carattere diverso da NULL
	  	  	  	}
	  	  	  	for (i=0; i<sizeof(ICPCHGInfo.ChgInfo.aSerialNumber); i++) {
	  	  	  		if (ICPCHGInfo.ChgInfo.aSerialNumber[i] != NULL_VALUE) return;												// C'e' almeno un carattere diverso da NULL
	  	  	  	}
	  			SkipStructs(pntTestLA1->TableID_Offset);
	  	  	  	break;
	  	  	  			
/*--------------------------------------------------------------------------------------*/
	  	  	// Salta l'intera struttura "DA1" (dati da Cashless MDB) se non 
	  	  	// c'e' alcun casheless MDB connesso.
	  	  	  		  
	  	  	case kTestDA1:
	  	  		for (i=0; i<sizeof(ICPCPCInfo.CPCInfo.aManufactCode); i++) {
	  	  			if (ICPCPCInfo.CPCInfo.aManufactCode[i] != NULL_VALUE) return;												// C'e' almeno un carattere diverso da NULL
	  	  	  	}
	  	  	  	for (i=0; i<sizeof(ICPCPCInfo.CPCInfo.aSerialNumber); i++) {
	  	  	  		if (ICPCPCInfo.CPCInfo.aSerialNumber[i] != NULL_VALUE) return;												// C'e' almeno un carattere diverso da NULL
	  	  	  	}
	  			SkipStructs(pntTestLA1->TableID_Offset);
	  	  	  	break;
	  	  		  	  	  			
/*--------------------------------------------------------------------------------------*/
	  		case kTubesCoinValGet:
	  			if (ICPProt_CHGGetReady()) {																					// Se la RR non c'e' non si crea il CA15
	  				sAuditCommVars.lTotale = 0;
	  				for (i=0; i< ICP_MAX_COINTYPES; i++) {
	  					if ((AuditOptions & CA15_FROM_CHG) == false) {															// Default:
	  						sAuditCommVars.lTotale += (ICPProt_CHGGetCoinValue(i) * GetCoinsInTube(i) * ICPProt_CHGGet_USF());	// CA15 come da Tube Status ricevuto dal CHG
	  					} else {    				
	  						AudData = AuditReadData(IICEEPAuditAddr(TubCoinStatus[i]), 1);										// CA15 calcolato dall'audit locale 
	  						sAuditCommVars.lTotale += CHGHook_GetCoinTubeAmount((uint8_t)AudData, i);
	  					}
	  				}
	  			} else {
	  				//AuditAddressSet((char*)(&tCA15_03));
		  			SkipStructs(pntTestLA1->TableID_Offset);
	  			}
#endif    	
    	
    break;
	  }
}

/*--------------------------------------------------------------------------------------*\
Method: SkipStructs
	Salta tante strutture quante il parametro passato ("TableID_Offset").

Parameters:
	IN	-
	OUT	- 
\*--------------------------------------------------------------------------------------*/
static void SkipStructs(uint8_t numstructskip) {

	  uint8_t  i;

	  if (numstructskip) {
		  for (i=0; i<numstructskip; i++) {
			  sAuditCommVars.pntTable++;
		  }
		  sAuditCommVars.pntAudit = sAuditCommVars.pntTable->pnt;               // Successiva struttura AUDIT da gestire
	  	  sAuditCommVars.testEnd = false;										// Non incrementare "sAuditCommVars.pntAudit" nella "AuditLoopBody"
	  }
}

/*--------------------------------------------------------------------------------------*\
Method: ProgHookGetDPP
  Return decimal point position
\*--------------------------------------------------------------------------------------*/
/* 06.05.15 Tolti per pulizia files
static uint16_t ProgHookGetDPP(void)
{
  return(DecimalPointPosition);
}
*/


















