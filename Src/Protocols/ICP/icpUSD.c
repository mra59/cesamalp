/****************************************************************************************
File:
    ICPUsd.c

Description:
    Protocollo ICP per Macchine Slave

History:
    Date       Aut  Note
    Set 2012 	MR   

*****************************************************************************************/

#include <string.h>
#include <intrinsics.h>

#include "ORION.H"
#include "icpProt.h"
#include "icpVMCProt.h"
#include "icpUSDProt.h"
#include "ICPHook.h"
#include "ICPUSDHook.h"


/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern void ICPUSDHook_GetVendFailed(uint8_t usdType, T_USD_VENDFAILED *ptResp);
	// Called to get Vend Failed Info... Error Code of current "selRow/selColumn" selection
extern void ICPUSDHook_GetErrCode(uint8_t usdType, T_USD_ERRCODE *ptResp);
	// Called to get the USD failure code
extern void ICPUSDHook_GetSelStatus(uint8_t usdType, T_USD_SELSTATUS *ptResp);
	// Called to get the Status of current "selRow/selColumn" selection



//--------------------------------------------------------------------------------------
#if (_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))


#if (_IcpSlave_ & ICP_USD1)
T_ICP_USDINFO ICPUSD1Info;
#endif
#if (_IcpSlave_ & ICP_USD2)
T_ICP_USDINFO ICPUSD2Info;
#endif
#if (_IcpSlave_ & ICP_USD3)
T_ICP_USDINFO ICPUSD3Info;
#endif
T_VMC_SETUP ICPUSDVmcSetup;
T_ICP_USDINFO *usdInfo;


bool USDChangeSlave(uint8_t nSlaveType) {
	switch(nSlaveType) {
	#if (_IcpSlave_ & ICP_USD1)
	case ICP_USD1_ADDR:
		usdInfo = &ICPUSD1Info;
		break;
	#endif
	#if (_IcpSlave_ & ICP_USD2)
	case ICP_USD2_ADDR:
		usdInfo = &ICPUSD2Info;
		break;
	#endif
	#if (_IcpSlave_ & ICP_USD3)
	case ICP_USD3_ADDR:
		usdInfo = &ICPUSD3Info;
		break;
	#endif
	default:
		usdInfo = nil;
	}
	return usdInfo ? true : false;
}

/*--------------------------------------------------------------------------------------*\
Method: USDInit

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void USDInit(uint8_t nSlaveType) {
	
	if (!USDChangeSlave(nSlaveType)) return;
	ICPProt_InitSlave(nSlaveType);
	memset(usdInfo, 0, sizeof(T_ICP_USDINFO));
	#if (_IcpMaster_ == true)
		usdInfo->nWaitInitCnt = ICP_TM_USDWAITINIT / ICP_TM_POLLPERIPH;
		ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
		ICPInfo.nSlaveLinkMask &= ~icpSlvLinkMask;
	#endif
	usdInfo->USD_CreditoInviato = 0xffff;
	ICPProt_USDSetEvtMask(nSlaveType, ICP_USDINFO_JUSTRESET);
}




//-------------------------------------------------
//
//					U S D	Slave Code
//
//-------------------------------------------------
#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))

void USDSlaveResp(void) {
	//MR19 uint8_t nRespType;			//Never referenced
	uint8_t nInfoMask = 0;

	if(ICPDrvGetComState() == COM_RXFRAME) {

		#if (_IcpSmsAlarms_ == true)
			if(!ICPUSDHook_IsEnabled(icpSlvAddr)) {
				switch(ICPCommand) {
				case ICP_USD_CMD_RESET:
					ICPProt_USDResetResp();
					break;
				case ICP_USD_CMD_CONTROLDISABLE:
					ICPProt_USDControl00Resp();
					break;
				case ICP_USD_CMD_CONTROLENABLE:
					ICPProt_USDControl01Resp();
					break;
				default:
					break;
				}
				return;
			}
		#endif

		if(!ICPUSDHook_IsEnabled(icpSlvAddr)) return;

		/*nPeriphID = ICPInfo.nCurrPeriph;
		nCurrState = ICPInfo.aPeriphInfo[nPeriphID].nState;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		switch(ICPCommand) {
		//----------------------//
		//	RESET command
		//----------------------//
		case ICP_USD_CMD_RESET:
			ICPProt_USDResetResp();
			//
			// Jump to INACTIVE State!
			//
			ICPProt_ChangeState(icpSlv, ICP_USD_INACTIVE, ICP_USD_INACTIVE_WAITSETUP);
			break;

		//----------------------//
		//	SETUP command
		//----------------------//
		case ICP_USD_CMD_SETUP:
			ICPProt_USDSetupResp();
			ICPProt_ChangeState(icpSlv, ICP_USD_DISABLED, ICP_USD_DISABLED_WAITCMDS);
			break;

		//----------------------//
		//	POLL command
		//----------------------//
		case ICP_USD_CMD_POLL:
			ICPProt_USDPollResp();
			break;

		//----------------------//
		//	VEND commands
		//----------------------//
		case ICP_USD_CMD_VEND:
			switch(ICPSubCommand) {
			case ICP_USD_CMD_VENDAPPROVED:
				ICPProt_USDVend00Resp();
				if(icpSlvState == ICP_USD_ENABLED)
					//
					// Jump to VEND State!
					//
					ICPProt_ChangeState(icpSlv, ICP_USD_VEND, ICP_USD_VEND_RXVENDAPPROVED);
				else nInfoMask |= ICP_USDINFO_VENDFAILED;
				break;
			case ICP_USD_CMD_VENDDENIED:
				ICPProt_USDVend01Resp();
				break;
			case ICP_USD_CMD_VENDSEL:
				ICPProt_USDVend02Resp();
				if(icpSlvState == ICP_USD_ENABLED) ICPProt_ChangeState(icpSlv, ICP_USD_VEND, ICP_USD_VEND_RXVENDSEL);
				else nInfoMask |= ICP_USDINFO_VENDFAILED;
				break;
			case ICP_USD_CMD_VENDHOMESEL:
				ICPProt_USDVend03Resp();
				if(icpSlvState == ICP_USD_ENABLED) ICPProt_ChangeState(icpSlv, ICP_USD_VEND, ICP_USD_VEND_RXVENDHOMESEL);
				else nInfoMask |= ICP_USDINFO_VENDFAILED;
				break;
			case ICP_USD_CMD_VENDSELSTATUS:
				ICPProt_USDVend04Resp();
				break;
			default:
				//	Invalid sub-commands: no response!
				break;
			}
			break;

		//----------------------//
		//	FUNDS command
		//----------------------//
		case ICP_USD_CMD_FUNDS:
			switch(ICPSubCommand) {
			case ICP_USD_CMD_FUNDSCREDIT:
				ICPProt_USDFunds00Resp();
				break;
			case ICP_USD_CMD_FUNDSSELPRICE:
				ICPProt_USDFunds01Resp();
				break;
			default:
				//	Invalid sub-commands: no response!
				break;
			}
			break;

		//----------------------//
		//	CONTROL commands
		//----------------------//
		case ICP_USD_CMD_CONTROL:
			switch(ICPSubCommand) {
			case ICP_USD_CMD_CONTROLDISABLE:
				ICPProt_USDControl00Resp();
				ICPProt_ChangeState(icpSlv, ICP_USD_DISABLED, ICP_USD_DISABLED_WAITCMDS);
				break;
			case ICP_USD_CMD_CONTROLENABLE:
				ICPProt_USDControl01Resp();
				ICPProt_ChangeState(icpSlv, ICP_USD_ENABLED, ICP_USD_ENABLED_WAITCMDS);
				break;
			default:
				//	Invalid sub-commands: no response!
				break;
			}
			break;

		//----------------------//
		//	EXPANSION commands
		//----------------------//
		case ICP_USD_CMD_EXP:
			switch(ICPSubCommand) {
			case ICP_USD_CMD_EXPREQUESTID:
				ICPProt_USDExp00Resp();
				break;
			case ICP_USD_CMD_EXPENABLEOPT:
				ICPProt_USDExp01Resp();
				break;
			case ICP_USD_CMD_EXPSENDBLOCKS:
				ICPProt_USDExp02Resp();
				break;
			case ICP_USD_CMD_EXPSENDBLOCKN:
				ICPProt_USDExp03Resp();
				break;
			case ICP_USD_CMD_EXPREQBLOCKN:
				ICPProt_USDExp04Resp();
				break;
			case ICP_USD_CMD_EXPSENDBLOCK:
				ICPProt_USDExp05Resp();
				break;
			case ICP_USD_CMD_EXPDIAG:
				ICPProt_USDExpFFResp();
				break;
			default:
				//	Invalid sub-commands: no response!
				break;
			}
			break;

		//
		//	Invalid commands: no response!
		//
		default:
			break;
		}

		if(nInfoMask) {
			usdInfo->nInfoMask |= nInfoMask;
		}

	
		//27/03/2007: BUG: if command not handled no following commands recognized!
		if(ICPDrvGetComState() == COM_RXFRAME) {
			// if rxframe not handled, no tx data, then discard it
			ICPDrvSetComState(COM_IDLE);
		}
		//27/03/2007

	}
}


void GetUSDSlaveStatus(void) {
	uint8_t nInfoMask = 0, evtMask;

	//MR19 uint8_t nLevel;					//Never referenced
	//MR19 CPCCreditType_L3 nAmount;		//Never referenced

	/*nPeriphID = ICPInfo.nCurrPeriph;
	nCurrState = ICPInfo.aPeriphInfo[nPeriphID].nState;
	nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/
	ICPInfo.nFrameLen = 0;
	evtMask = usdInfo->nInfoMask;

	if(evtMask & ICP_USDINFO_JUSTRESET) {
		//
		// Send 00 = JUST RESET
		//
		nInfoMask |= ICP_USDINFO_JUSTRESET;
		ICPInfo.buff[0] = ICP_USD_JUSTRESET;
		ICPInfo.nFrameLen = 1;
	}

	if(!ICPInfo.nFrameLen && (evtMask & ICP_USDINFO_VENDREQ)) {
		if(icpSlvState == ICP_USD_ENABLED) {
			//
			// Send 01.selRow.selColumn.selPrice =  VEND REQUEST
			//
			CreditCpcValue nSelPrice;
			T_USD_VENDREQ *ptResp = (T_USD_VENDREQ *)&ICPInfo.buff[0];
			ptResp->nUsdResp = ICP_USD_VENDREQ;
			ICPUSDHook_GetVendRequest(icpSlvAddr, &ptResp->nSelRow, &ptResp->nSelColumn, &nSelPrice);
			nSelPrice /= ICPUSDVmcSetup.nUSF;
			ptResp->nSelPrice = nSelPrice;
			//MR20 Cvt16ToBigEndian(&ptResp->nSelPrice)
			ptResp->nSelPrice = __REV16(ptResp->nSelPrice);
			ICPInfo.nFrameLen = sizeof(T_USD_VENDREQ);
		}
		nInfoMask |= ICP_USDINFO_VENDREQ;
	}

	if(!ICPInfo.nFrameLen && (evtMask & ICP_USDINFO_VENDFAILED)) {
		if(icpSlvState == ICP_USD_VEND) {
			//
			// Send 03.selRow.selColumn.errCode =  VEND FAILED
			//
			T_USD_VENDFAILED *ptResp = (T_USD_VENDFAILED *)&ICPInfo.buff[0];
			ptResp->nUsdResp = ICP_USD_VENDFAILED;
			//MR20 ICPUSDHook_GetVendFailed(icpSlvAddr, &ptResp->nSelRow, &ptResp->nSelColumn, &ptResp->nErrCode);
			ICPUSDHook_GetVendFailed(icpSlvAddr, ptResp);
			//MR20 Cvt16ToBigEndian(&ptResp->nErrCode)
			ptResp->nErrCode = __REV16(ptResp->nErrCode);
			ICPInfo.nFrameLen = sizeof(T_USD_VENDFAILED);
		}
		nInfoMask |= ICP_USDINFO_VENDFAILED;
		ICPProt_ChangeState(icpSlv, ICP_USD_ENABLED, ICP_USD_ENABLED_WAITCMDS);
	}

	if(!ICPInfo.nFrameLen && (evtMask & ICP_USDINFO_VENDSUCC)) {
		if(icpSlvState == ICP_USD_VEND) {
			//
			// Send 02 =  VEND SUCCESS
			//
			ICPInfo.buff[0] = ICP_USD_VENDSUCC;
			ICPInfo.nFrameLen = 1;
		}
		nInfoMask |= ICP_USDINFO_VENDSUCC;
		ICPProt_ChangeState(icpSlv, ICP_USD_ENABLED, ICP_USD_ENABLED_WAITCMDS);
	}

	if(!ICPInfo.nFrameLen && (evtMask & ICP_USDINFO_PRICEREQ)) {
		//
		// Send 05.selRow.selColumn =  SEL. PRICE REQUEST
		//
		T_USD_PRICEREQ *ptResp = (T_USD_PRICEREQ *)&ICPInfo.buff[0];
		ptResp->nUsdResp = ICP_USD_PRICEREQ;
		ICPUSDHook_GetPriceRequest(icpSlvAddr, &ptResp->nSelRow, &ptResp->nSelColumn);
		ICPInfo.nFrameLen = sizeof(T_USD_PRICEREQ);
		nInfoMask |= ICP_USDINFO_PRICEREQ;
	}

	if(!ICPInfo.nFrameLen && (evtMask & ICP_USDINFO_ERRCODE)) {
		//
		// Send 06.errCode =  ERR. CODE
		//
		T_USD_ERRCODE *ptResp = (T_USD_ERRCODE *)&ICPInfo.buff[0];
		ptResp->nUsdResp = ICP_USD_ERRCODE;
		ICPUSDHook_GetErrCode(icpSlvAddr, ptResp);
		//MR20 Cvt16ToBigEndian(&ptResp->nErrCode)
		ptResp->nErrCode = __REV16(ptResp->nErrCode);
		ICPInfo.nFrameLen = sizeof(T_USD_ERRCODE);
		nInfoMask |= ICP_USDINFO_ERRCODE;
	}

	if(!ICPInfo.nFrameLen && (evtMask & ICP_USDINFO_SELSTATUS)) {
		//
		// Send 08.selRow.selColumn.selStatus =  SEL. STATUS
		//
		T_USD_SELSTATUS *ptResp = (T_USD_SELSTATUS *)&ICPInfo.buff[0];
		ptResp->nUsdResp = ICP_USD_SELSTATUS;
		ICPUSDHook_GetSelStatus(icpSlvAddr, ptResp);
		//MR20 Cvt16ToBigEndian(&ptResp->nSelStatus)
		ptResp->nSelStatus = __REV16(ptResp->nSelStatus);
		ICPInfo.nFrameLen = sizeof(T_USD_SELSTATUS);
		nInfoMask |= ICP_USDINFO_SELSTATUS;
	}

	if(nInfoMask) {
		usdInfo->nInfoMask &= ~nInfoMask;
	}

	if(ICPInfo.nFrameLen) ICPDrvSendMsg(ICPInfo.nFrameLen);
	else ICPDrvSendRW(ICP_RW_ACK);
}
#endif

// -----------------------------------------------------------
void ICPProt_USDSetEvtMask(uint8_t currUSD, uint8_t evtMask) {
	
	switch(currUSD) {
	
		case ICP_USD1_ADDR:
			ICPUSD1Info.nInfoMask |= evtMask;
			break;
		case ICP_USD2_ADDR:
			ICPUSD2Info.nInfoMask |= evtMask;
			break;
		case ICP_USD3_ADDR:
			ICPUSD3Info.nInfoMask |= evtMask;
			break;
	}
}




//-------------------------------------------------
//
//					I N A C T I V E
//
//-------------------------------------------------
void USDInactive(void) {
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	USDChangeSlave(icpSlvAddr);

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint8_t nComState;
		bool fJustReset = false;

		/*nPeriphID = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[nPeriphID].nState != ICP_USD_INACTIVE)
			return;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_USD_INACTIVE_TXRESET:
					if(icpSlvCheckNoRespState <= 1) {
						ICPProt_USDReset();
					}
					break;
				case ICP_USD_INACTIVE_WAITINIT:
					if(!ICPProt_WaitInit(icpSlvAddr))
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_INACTIVE_TXPOLL_1);
					break;
				case ICP_USD_INACTIVE_TXPOLL_1:
				case ICP_USD_INACTIVE_TXPOLL_2:
					ICPProt_USDPoll();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_USD_INACTIVE_TXRESET:
				nRespType = ICPProt_USDResetResp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_INACTIVE_WAITINIT);
				}
				break;
			case ICP_USD_INACTIVE_TXPOLL_1:
				nRespType = ICPProt_USDPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_USD_DISABLED, ICP_USD_DISABLED_TXSETUPID);
					}
					break;
				case ICP_RESPTYPE_JUSTRESET:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_USD_DISABLED, ICP_USD_DISABLED_TXSETUPID);
					} else {
						ICPInfo.nSlaveJustResetMask |= icpSlvLinkMask;
					}
					break;
				default:
					break;
				}
				///ICPProt_ChangeState(icpSlv, -1, ICP_USD_INACTIVE_TXPOLL_2);
				break;
			case ICP_USD_INACTIVE_TXPOLL_2:
				nRespType = ICPProt_USDPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) fJustReset = true;
					break;
				case ICP_RESPTYPE_JUSTRESET:
					fJustReset = true;
					break;
				default:
					break;
				}
				if(fJustReset) {
					ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
					ICPProt_ChangeState(icpSlv, ICP_USD_DISABLED, ICP_USD_DISABLED_TXSETUPID);
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	USDSlaveResp();
#endif
}


//-------------------------------------------------
//
//					D I S A B L E D
//
//-------------------------------------------------
void USDDisabled(void) {
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	USDChangeSlave(icpSlvAddr);

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*nPeriphID = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[nPeriphID].nState != ICP_USD_DISABLED)
			return;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_USD_DISABLED_TXSETUPID:
					ICPProt_SetReady(icpSlvAddr, false);
					ICPProt_USDSetup();
					break;
				case ICP_USD_DISABLED_TXEXPID:
					ICPProt_USDExp00();
					break;
				case ICP_USD_DISABLED_TXEXPFOPT:
					ICPProt_USDExp01();
					break;
				case ICP_USD_DISABLED_TXCONTROLENABLE:
					ICPProt_USDControl01();
					break;
				case ICP_USD_DISABLED_WAITSETUPID:
				case ICP_USD_DISABLED_WAITEXPID:
				case ICP_USD_DISABLED_TXPOLL:
					ICPProt_USDPoll();
					break;
//03/12/2009 agg. ExpDIAG
				case ICP_USD_DISABLED_TXEXPDIAG:
					ICPProt_USDExpFF();
					break;
//03/12/2009 agg. ExpDIAG
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_USD_DISABLED_TXSETUPID:
				nRespType = ICPProt_USDSetupResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_WAITSETUPID);
					break;
				case ICP_USD_RESPTYPE_SETUP:
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXEXPID);
					break;
				default:
					break;
				}
				break;
			case ICP_USD_DISABLED_TXEXPID:
				nRespType = ICPProt_USDExp00Resp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_WAITEXPID);
					break;
				case ICP_USD_RESPTYPE_PERIPHID:
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXEXPFOPT);
					break;
				default:
					break;
				}
				break;
			case ICP_USD_DISABLED_TXEXPFOPT:
				nRespType = ICPProt_USDExp01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXCONTROLENABLE);
				}
				break;
//03/12/2009 agg. ExpDIAG
			case ICP_USD_DISABLED_TXEXPDIAG:
				nRespType = ICPProt_USDExpFFResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_USD_RESPTYPE_EXPFF:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_DIAGDONE);
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
//03/12/2009 agg. ExpDIAG
			case ICP_USD_DISABLED_TXCONTROLENABLE:
				nRespType = ICPProt_USDControl01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_ENABLED);
					ICPProt_ChangeState(icpSlv, ICP_USD_ENABLED, ICP_USD_ENABLED_TXPOLL);
					ICPProt_SetReady(icpSlvAddr, true);
				}
				break;
			case ICP_USD_DISABLED_WAITSETUPID:
			case ICP_USD_DISABLED_WAITEXPID:
			case ICP_USD_DISABLED_TXPOLL:
				nRespType = ICPProt_USDPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_USD_INACTIVE, ICP_USD_INACTIVE_TXRESET);
					break;
				case ICP_USD_RESPTYPE_SETUP:
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXEXPID);
					break;
				case ICP_USD_RESPTYPE_PERIPHID:
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXEXPFOPT);
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_USD_RESET:
						ICPProt_ChangeState(icpSlv, ICP_USD_INACTIVE, ICP_USD_INACTIVE_TXRESET);
						break;
					//case ICP_VMCCMD_USD_ENABLE:
					//	ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXCONTROLENABLE);
					//	break;
//03/12/2009 agg. ExpDIAG
					case ICP_VMCCMD_USD_DIAG:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_DISABLED_TXEXPDIAG);
						break;
//03/12/2009 agg. ExpDIAG
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	USDSlaveResp();
#endif
}


//-------------------------------------------------
//
//					E N A B L E D
//
//-------------------------------------------------
void USDEnabled(void) {
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	USDChangeSlave(icpSlvAddr);

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*nPeriphID = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[nPeriphID].nState != ICP_USD_ENABLED)
			return;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_USD_ENABLED_TXPOLL:
					ICPProt_USDPoll();
					break;
				case ICP_USD_ENABLED_TXCONTROLDISABLE:
					ICPProt_USDControl00();
					break;
				case ICP_USD_ENABLED_TXFUNDSCREDIT:
					ICPProt_USDFunds00();
					break;
				case ICP_USD_ENABLED_TXFUNDSSELPRICE:
					ICPProt_USDFunds01();
					break;
				case ICP_USD_ENABLED_TXVENDAPPROVED:
					ICPProt_USDVend00();
					break;
				case ICP_USD_ENABLED_TXVENDDENIED:
					ICPProt_USDVend01();
					break;
				case ICP_USD_ENABLED_TXVENDSEL:
					ICPProt_USDVend02();
					break;
				case ICP_USD_ENABLED_TXVENDHOMESEL:
					ICPProt_USDVend03();
					break;
				case ICP_USD_ENABLED_TXVENDSELSTATUS:
					ICPProt_USDVend04();
					break;
//03/12/2009 agg. ExpDIAG
				case ICP_USD_ENABLED_TXEXPDIAG:
					ICPProt_USDExpFF();
					break;
//03/12/2009 agg. ExpDIAG
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_USD_ENABLED_TXPOLL:
				nRespType = ICPProt_USDPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_USD_INACTIVE, ICP_USD_INACTIVE_TXRESET);
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_USD_RESET:
						ICPProt_ChangeState(icpSlv, ICP_USD_INACTIVE, ICP_USD_INACTIVE_TXRESET);
						break;
					case ICP_VMCCMD_USD_DISABLE:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXCONTROLDISABLE);
						break;
					case ICP_VMCCMD_USD_FUNDSCREDIT:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXFUNDSCREDIT);
						break;
					case ICP_VMCCMD_USD_FUNDSSELPRICE:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXFUNDSSELPRICE);
						break;
					case ICP_VMCCMD_USD_VENDSEL:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXVENDSEL);
						break;
					case ICP_VMCCMD_USD_VENDHOMESEL:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXVENDHOMESEL);
						break;
					case ICP_VMCCMD_USD_VENDSELSTATUS:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXVENDSELSTATUS);
						break;
					case ICP_VMCCMD_USD_VENDAPPROVED:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXVENDAPPROVED);
						break;
					case ICP_VMCCMD_USD_VENDDENIED:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXVENDDENIED);
						break;
//03/12/2009 agg. ExpDIAG
					case ICP_VMCCMD_USD_DIAG:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXEXPDIAG);
						break;
//03/12/2009 agg. ExpDIAG
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				default:
					break;
				}
				break;
			case ICP_USD_ENABLED_TXCONTROLDISABLE:
				nRespType = ICPProt_USDControl00Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_DISABLED);
					ICPProt_ChangeState(icpSlv, ICP_USD_DISABLED, ICP_USD_DISABLED_TXPOLL);
				}
				break;
			case ICP_USD_ENABLED_TXFUNDSCREDIT:
				nRespType = ICPProt_USDFunds00Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_FUNDSCREDIT);
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXPOLL);
				}
				break;
			case ICP_USD_ENABLED_TXFUNDSSELPRICE:
				nRespType = ICPProt_USDFunds01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_FUNDSSELPRICE);
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXPOLL);
				}
				break;
			case ICP_USD_ENABLED_TXVENDSEL:
				nRespType = ICPProt_USDVend02Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_VENDSEL);
					ICPProt_ChangeState(icpSlv, ICP_USD_VEND, ICP_USD_VEND_WAITVENDSUCCFAIL);
				}
				break;
			case ICP_USD_ENABLED_TXVENDHOMESEL:
				nRespType = ICPProt_USDVend03Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_VENDHOMESEL);
					ICPProt_ChangeState(icpSlv, ICP_USD_VEND, ICP_USD_VEND_WAITVENDSUCCFAIL);
				}
				break;
			case ICP_USD_ENABLED_TXVENDSELSTATUS:
				nRespType = ICPProt_USDVend04Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_VENDSELSTATUS);
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXPOLL);
				}
				break;
			case ICP_USD_ENABLED_TXVENDAPPROVED:
				nRespType = ICPProt_USDVend00Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_VENDAPPROVED);
					ICPProt_ChangeState(icpSlv, ICP_USD_VEND, ICP_USD_VEND_WAITVENDSUCCFAIL);
				}
				break;
			case ICP_USD_ENABLED_TXVENDDENIED:
				nRespType = ICPProt_USDVend01Resp();
				if(nRespType == ICP_RESPTYPE_RW_ACK) {
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_VENDDENIED);
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXPOLL);
				}
				break;
//03/12/2009 agg. ExpDIAG
			case ICP_USD_ENABLED_TXEXPDIAG:
				nRespType = ICPProt_USDExpFFResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_USD_RESPTYPE_EXPFF:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_DIAGDONE);
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
//03/12/2009 agg. ExpDIAG
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	USDSlaveResp();
#endif
}



//-------------------------------------------------
//
//					V E N D
//
//-------------------------------------------------
void USDVend(void) {
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	USDChangeSlave(icpSlvAddr);

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t nRespType;
		uint8_t nComState;

		/*nPeriphID = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[nPeriphID].nState != ICP_USD_VEND)
			return;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_USD_VEND_WAITVENDSUCCFAIL:
					ICPProt_USDPoll();
					break;
//03/12/2009 agg. ExpDIAG
				case ICP_USD_VEND_TXEXPDIAG:
					ICPProt_USDExpFF();
					break;
//03/12/2009 agg. ExpDIAG
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_USD_VEND_WAITVENDSUCCFAIL:
				nRespType = ICPProt_USDPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_USD_INACTIVE, ICP_USD_INACTIVE_TXRESET);
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_USD_RESET:
						ICPProt_ChangeState(icpSlv, ICP_USD_INACTIVE, ICP_USD_INACTIVE_TXRESET);
						break;
//03/12/2009 agg. ExpDIAG
					case ICP_VMCCMD_USD_DIAG:
						ICPProt_ChangeState(icpSlv, -1, ICP_USD_VEND_TXEXPDIAG);
						break;
//03/12/2009 agg. ExpDIAG
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				case ICP_USD_RESPTYPE_VENDSUCC:
				case ICP_USD_RESPTYPE_VENDFAILED:
					ICPProt_ChangeState(icpSlv, ICP_USD_ENABLED, ICP_USD_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
//03/12/2009 agg. ExpDIAG
			case ICP_USD_VEND_TXEXPDIAG:
				nRespType = ICPProt_USDExpFFResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_USD_RESPTYPE_EXPFF:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_USD_DIAGDONE);
					ICPProt_ChangeState(icpSlv, -1, ICP_USD_VEND_WAITVENDSUCCFAIL);
					break;
				default:
					break;
				}
				break;
//03/12/2009 agg. ExpDIAG
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	USDSlaveResp();
#endif
}


//===============================================
//		RESET Command (40H / 48H / 50H)
//===============================================
void ICPProt_USDReset(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_RESET *ptCmd = (T_USDCMD_RESET *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_RESET) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	USDInit(icpSlvAddr);

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_RESET;
	nFrameLen = sizeof(ptCmd->nCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDResetResp(void) {
	
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_RESET *ptResp = (T_USDRESP_RESET *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			// 09.17 Ignoro risposte al RESET di USD2 e USD3
			if (icpSlvAddr == ICP_USD1_ADDR) {													// 09.17 Accetto solo UD1
				nRespType = ICP_RESPTYPE_RW_ACK;
			}
		}
		ICPDrvSetComState(COM_IDLE);
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	
	USDInit(icpSlvAddr);
	ICPDrvSendRW(ICP_RW_ACK);
	ICPUSDHook_Init(icpSlvAddr);																// 09.07 Reinizializzazioen DA 
	//ICPProt_SetInhibit(icpSlvAddr, true);														// 09.17 Tolto perche' usato solo per SMS Alarms
#endif
	return nRespType;
}


//===============================================
//		SETUP Command (41H / 49H / 51H)
//===============================================
void ICPProt_USDSetup(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_SETUP *ptCmd = (T_USDCMD_SETUP *)&ICPInfo.buff[0];
	T_VMC_SETUP *ptVmcSetup = (T_VMC_SETUP *)&ICPUSDVmcSetup;
	uint8_t nFrameLen;

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_SETUP;
	ICPProt_VMCUsdGetSetupInfo(&ptCmd->Setup);
	ICPUSDVmcSetup = ptCmd->Setup;
	//MR20 Cvt16ToBigEndian(&ptVmcSetup->nUSF);									// 09.17 Riporto in Low Endian l'USF che sara' usato per calcolare i valori in valuta da/verso l'USD
	ptVmcSetup->nUSF = __REV16(ptVmcSetup->nUSF);
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDSetupResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		CreditCpcValue nMaxPrice;
		T_USDRESP_SETUP *ptResp = (T_USDRESP_SETUP *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			//MR20 Cvt16ToBigEndian(&ptResp->Setup.nMaxPrice);
			ptResp->Setup.nMaxPrice = __REV16(ptResp->Setup.nMaxPrice);
			usdInfo->Setup = ptResp->Setup;
			ICPProt_SetLevel(icpSlvAddr, ptResp->Setup.nFLevel);
			nMaxPrice = ptResp->Setup.nMaxPrice * ICPUSDVmcSetup.nUSF;
			ICPUSDHook_SetMaxPrice(icpSlvAddr, nMaxPrice);
			ICPUSDHook_SetRowColumn(icpSlvAddr, ptResp->Setup.nSelRow, ptResp->Setup.nSelColumn);
			nRespType = ICP_USD_RESPTYPE_SETUP;
			ICPDrvSendRW(ICP_RW_ACK);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	T_USDRESP_SETUP *ptResp = (T_USDRESP_SETUP *)&ICPInfo.buff[0];
	T_USDCMD_SETUP *ptCmd = (T_USDCMD_SETUP *)&ICPInfo.buff[0];
	T_VMC_SETUP *ptVmcSetup = (T_VMC_SETUP *)&ICPUSDVmcSetup;

	// Per evitare malfunzionamenti con VMC Mdb che non attendono
	// il ns. JustReset prima di far partire la sequenza di inizializzazione!
	usdInfo->nInfoMask &= ~ICP_USDINFO_JUSTRESET;
	ICPUSDVmcSetup = ptCmd->Setup;
	//MR20 Cvt16ToBigEndian(&ptVmcSetup->nUSF);														// 09.17 Converto ICPUSDVmcSetup.nUSF perche' usato per i calcoli successivi
	ptVmcSetup->nUSF = __REV16(ptVmcSetup->nUSF);
	ICPProt_SetLevel(icpSlvAddr, ptCmd->Setup.nFLevel);
	ICPUSDHook_VMCSetupInfo(&ptCmd->Setup);														// Parametri del VMC (Level, USF, DPP e timeout)

	ptResp->Setup.nUsdResp = ICP_USD_SETUP;
	ptResp->Setup.nFLevel = ICPUSDHook_GetFeatureLevel(icpSlvAddr);
	ptResp->Setup.nMaxPrice = ICPUSDHook_GetMaxPrice(icpSlvAddr);
	//MR20 Cvt16ToBigEndian(&ptResp->Setup.nMaxPrice)
	ptResp->Setup.nMaxPrice = __REV16(ptResp->Setup.nMaxPrice);
	ICPUSDHook_GetRowColumn(icpSlvAddr, &ptResp->Setup.nSelRow, &ptResp->Setup.nSelColumn);
	ptResp->Setup.nMaxTmResponse = 0xFF;

	ICPInfo.nFrameLen = sizeof(ptResp->Setup);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif

	return nRespType;
}


//===============================================
//		POLL Command (42H / 4AH / 52H)
//===============================================
void ICPProt_USDPoll(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_POLL *ptCmd = (T_USDCMD_POLL *)&ICPInfo.buff[0];
	uint8_t nFrameLen;

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_POLL;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDPollResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
	CreditCpcValue nPrice;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_POLL *pt = (T_USDRESP_POLL *)&ICPInfo.buff[0];
		int8 i, n;
		//MR19 uint8_t	nRespLen;				//Never referenced
		if((ICPInfo.fSlaveRW) && (pt->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {

			n = ICPInfo.iRX - 1;
			for(i = 0; i < n; ) {

				nRespType = pt->aPollData[i];
				switch(nRespType) {
					// Poll Resp Type = 0x00
					case ICP_USD_JUSTRESET: {
						nRespType = ICP_RESPTYPE_JUSTRESET;
						i = n;
						}
						break;
					// Poll Resp Type = 0x01
					case ICP_USD_VENDREQ: {
						T_USD_VENDREQ *ptResp = (T_USD_VENDREQ *)&pt->aPollData[i];
						//MR20 Cvt16ToBigEndian(&ptResp->nSelPrice);
						ptResp->nSelPrice = __REV16(ptResp->nSelPrice);
						nPrice = ptResp->nSelPrice * ICPUSDVmcSetup.nUSF;
						ICPUSDHook_VendReq(icpSlvAddr, ptResp->nSelRow, ptResp->nSelColumn, nPrice);
						i += sizeof(T_USD_VENDREQ);
						nRespType = ICP_USD_RESPTYPE_VENDREQ;
						}
						break;
					// Poll Resp Type = 0x02
					case ICP_USD_VENDSUCC: {
						ICPUSDHook_VendSucc(icpSlvAddr);
						i += 1;
						nRespType = ICP_USD_RESPTYPE_VENDSUCC;
						}
						break;
					// Poll Resp Type = 0x03
					case ICP_USD_VENDFAILED: {
						T_USD_VENDFAILED *ptResp = (T_USD_VENDFAILED *)&pt->aPollData[i];
						ICPUSDHook_VendFailed(icpSlvAddr, ptResp->nSelRow, ptResp->nSelColumn, ptResp->nErrCode);
						i += sizeof(T_USD_VENDFAILED);
						nRespType = ICP_USD_RESPTYPE_VENDFAILED;
						}
						break;
					// Poll Resp Type = 0x04
					case ICP_USD_SETUP: {
						CreditCpcValue nMaxPrice;
						T_USDRESP_SETUP *ptResp = (T_USDRESP_SETUP *)&pt->aPollData[i];
						//MR20 Cvt16ToBigEndian(&ptResp->Setup.nMaxPrice);
						ptResp->Setup.nMaxPrice = __REV16(ptResp->Setup.nMaxPrice);
						usdInfo->Setup = ptResp->Setup;
						ICPProt_SetLevel(icpSlvAddr, ptResp->Setup.nFLevel);
						nMaxPrice = ptResp->Setup.nMaxPrice * ICPUSDVmcSetup.nUSF;
						ICPUSDHook_SetMaxPrice(icpSlvAddr, nMaxPrice);
						ICPUSDHook_SetRowColumn(icpSlvAddr, ptResp->Setup.nSelRow, ptResp->Setup.nSelColumn);
						i += sizeof(ptResp->Setup);
						nRespType = ICP_USD_RESPTYPE_SETUP;
						}
						break;
					// Poll Resp Type = 0x05
					case ICP_USD_PRICEREQ: {
						T_USD_PRICEREQ *ptResp = (T_USD_PRICEREQ *)&pt->aPollData[i];
						ICPUSDHook_PriceReq(icpSlvAddr, ptResp->nSelRow, ptResp->nSelColumn);
						// get SelPrice && send it later!!!
						i += sizeof(T_USD_PRICEREQ);
						nRespType = ICP_USD_RESPTYPE_PRICEREQ;
						}
						break;
					// Poll Resp Type = 0x06
					case ICP_USD_ERRCODE: {
						T_USD_ERRCODE *ptResp = (T_USD_ERRCODE *)&pt->aPollData[i];
						ICPUSDHook_ErrCode(icpSlvAddr, ptResp->nErrCode);
						i += sizeof(T_USD_ERRCODE);
						nRespType = ICP_USD_RESPTYPE_ERRCODE;
						}
						break;
					// Poll Resp Type = 0x07
					case ICP_USD_PERIPHID: {
						T_USDRESP_EXP00 *ptResp = (T_USDRESP_EXP00 *)&pt->aPollData[i];

						ICPUSDHook_SetManufactCode(icpSlvAddr, &ptResp->PeriphID.aManufactCode[0], sizeof(ptResp->PeriphID.aManufactCode));
						ICPUSDHook_SetSerialNumber(icpSlvAddr, &ptResp->PeriphID.aSerialNumber[0], sizeof(ptResp->PeriphID.aSerialNumber));
						ICPUSDHook_SetModelNumber(icpSlvAddr, &ptResp->PeriphID.aModelNumber[0], sizeof(ptResp->PeriphID.aModelNumber));
						//MR20 Cvt16ToBigEndian(&ptResp->PeriphID.nSwVersion)
						ptResp->PeriphID.nSwVersion = __REV16(ptResp->PeriphID.nSwVersion);
						ICPUSDHook_SetSWVersion(icpSlvAddr, ptResp->PeriphID.nSwVersion);
						//MR20 Cvt32ToBigEndian(&ptResp->PeriphID.nFOptions)
						ptResp->PeriphID.nFOptions = __REV(ptResp->PeriphID.nFOptions);
						usdInfo->nFOptions = ptResp->PeriphID.nFOptions;
						i += sizeof(ptResp->PeriphID);
						nRespType = ICP_USD_RESPTYPE_PERIPHID;
						}
						break;
					// Poll Resp Type = 0x08
					case ICP_USD_SELSTATUS: {
						T_USDRESP_VEND04 *ptResp = (T_USDRESP_VEND04 *)&pt->aPollData[i];
						ICPUSDHook_SelStatus(icpSlvAddr, ptResp->SelStatus.nSelRow, ptResp->SelStatus.nSelColumn, ptResp->SelStatus.nSelStatus);
						i += sizeof(ptResp->SelStatus);
						nRespType = ICP_USD_RESPTYPE_SELSTATUS;
						}
						break;
					// Poll Resp Type = 0x09
					case ICP_USD_BLOCKN: {
						T_USDRESP_EXP04 *ptResp = (T_USDRESP_EXP04 *)&pt->aPollData[i];
						switch(ptResp->Exp04.nUsdResp) {
						case 0:
							ICPUSDHook_DataBlockReq(icpSlvAddr, ptResp->Exp04.nBlock);
							i += sizeof(ptResp->Exp04);
							break;
						case 1:
							ICPUSDHook_SendDataReq(icpSlvAddr, ptResp->Exp04.nBlock);
							i += sizeof(ptResp->Exp04);
							break;
						case 2:
						default:
							ICPUSDHook_DataBlock(icpSlvAddr, ptResp->Exp04.nBlock, ptResp->Exp04.aData, n - i - 2);
							i = n;
							break;
						}
						nRespType = ICP_USD_RESPTYPE_BLOCKN;
						}
						break;
					// Poll Resp Type = 0x0A
					case ICP_USD_BLOCKDATA: {
						T_USD_BLOCKDATA *ptResp = (T_USD_BLOCKDATA *)&pt->aPollData[i];
						ICPUSDHook_Data(icpSlvAddr, ptResp->aData, n - i - 2);
						i = n;
						nRespType = ICP_USD_RESPTYPE_BLOCKDATA;
						}
						break;
					// Poll Resp Type = 0xFF
					case ICP_USD_DIAG: {
						T_USDRESP_EXPFF *ptResp = (T_USDRESP_EXPFF *)&pt->aPollData[i];
						ICPUSDHook_SetDiagData(icpSlvAddr, ptResp->expff.aDiagData, n - i - 1);
						nRespType = ICP_USD_RESPTYPE_EXPFF;
						i = n;
						}
						break;
					default:
						break;
				}
			}
			ICPDrvSendRW(ICP_RW_ACK);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	GetUSDSlaveStatus();
#endif

	return nRespType;
}


//==========================================================
//		VEND APPROVED Command (43H 00H / 4BH 00H / 53H 00H)
//==========================================================
void ICPProt_USDVend00(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_VEND00 *ptCmd = (T_USDCMD_VEND00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_VENDAPPROVED) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_VEND;
	ptCmd->nSubCmd = ICP_USD_CMD_VENDAPPROVED;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDVend00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_VEND00 *ptResp = (T_USDRESP_VEND00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	ICPDrvSendRW(ICP_RW_ACK);
	ICPUSDHook_SlaveVendApproved(icpSlvAddr);
#endif

	return nRespType;
}


//==========================================================
//		VEND DENIED Command (43H 01H / 4BH 01H / 53H 01H)
//==========================================================
void ICPProt_USDVend01(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_VEND01 *ptCmd = (T_USDCMD_VEND01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_VENDDENIED) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_VEND;
	ptCmd->nSubCmd = ICP_USD_CMD_VENDDENIED;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDVend01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_VEND01 *ptResp = (T_USDRESP_VEND01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	ICPDrvSendRW(ICP_RW_ACK);
	ICPUSDHook_SlaveVendDenied(icpSlvAddr);
#endif

	return nRespType;
}


//==========================================================
//		VEND SEL. Command (43H 02H / 4BH 02H / 53H 02H)
//==========================================================
void ICPProt_USDVend02(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_VEND02 *ptCmd = (T_USDCMD_VEND02 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nSelRow = 0, nSelColumn = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_VENDSEL) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_VEND;
	ptCmd->nSubCmd = ICP_USD_CMD_VENDSEL;
	if(ptCmdPar) {
		nSelRow = *((uint8_t *)(ptCmdPar));
		nSelColumn = *((uint8_t *)(ptCmdPar + sizeof(uint8_t)));
	}
	ptCmd->nSelColumn = nSelColumn;
	ptCmd->nSelRow = nSelRow;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDVend02Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_VEND02 *ptResp = (T_USDRESP_VEND02 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	T_USDCMD_VEND02 *ptCmd;

	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_USDCMD_VEND02 *)&ICPInfo.buff[0];
	ICPUSDHook_VendSel(icpSlvAddr, ptCmd->nSelRow, ptCmd->nSelColumn);
	}
#endif

	return nRespType;
}


//==========================================================
//		VEND HOME SEL. Command (43H 03H / 4BH 03H / 53H 03H)
//==========================================================
void ICPProt_USDVend03(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_VEND03 *ptCmd = (T_USDCMD_VEND03 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nSelRow = 0, nSelColumn = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_VENDHOMESEL) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_VEND;
	ptCmd->nSubCmd = ICP_USD_CMD_VENDHOMESEL;
	if(ptCmdPar) {
		nSelRow = *((uint8_t *)(ptCmdPar));
		nSelColumn = *((uint8_t *)(ptCmdPar + sizeof(uint8_t)));
	}
	ptCmd->nSelColumn = nSelColumn;
	ptCmd->nSelRow = nSelRow;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDVend03Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_VEND03 *ptResp = (T_USDRESP_VEND03 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	T_USDCMD_VEND03 *ptCmd;

	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_USDCMD_VEND03 *)&ICPInfo.buff[0];
	ICPUSDHook_VendHomeSel(icpSlvAddr, ptCmd->nSelRow, ptCmd->nSelColumn);
	}
#endif

	return nRespType;
}


//==========================================================
//		VEND SEL. STATUS Command (43H 04H / 4BH 04H / 53H 04H)
//==========================================================
void ICPProt_USDVend04(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_VEND04 *ptCmd = (T_USDCMD_VEND04 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nSelRow = 0, nSelColumn = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_VENDSELSTATUS) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_VEND;
	ptCmd->nSubCmd = ICP_USD_CMD_VENDSELSTATUS;
	if(ptCmdPar) {
		nSelRow = *((uint8_t *)(ptCmdPar));
		nSelColumn = *((uint8_t *)(ptCmdPar + sizeof(uint8_t)));
	}
	ptCmd->nSelColumn = nSelColumn;
	ptCmd->nSelRow = nSelRow;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDVend04Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_VEND04 *ptResp = (T_USDRESP_VEND04 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	T_USDCMD_VEND04 *ptCmd = (T_USDCMD_VEND04 *)&ICPInfo.buff[0];
	T_USDRESP_VEND04 *ptResp = (T_USDRESP_VEND04 *)&ICPInfo.buff[0];

	ICPDrvSendRW(ICP_RW_ACK);

	ptResp->SelStatus.nUsdResp = ICP_USD_SELSTATUS;
	ptResp->SelStatus.nSelRow = ptCmd->nSelRow;
	ptResp->SelStatus.nSelColumn = ptCmd->nSelColumn;
	ptResp->SelStatus.nSelStatus = ICPUSDHook_VendSelStatus(icpSlvAddr, ptCmd->nSelRow, ptCmd->nSelColumn);
	//MR20 Cvt16ToBigEndian(&ptResp->SelStatus.nSelStatus)
	ptResp->SelStatus.nSelStatus = __REV16(ptResp->SelStatus.nSelStatus);

	ICPInfo.nFrameLen = sizeof(ptResp->SelStatus);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif

	return nRespType;
}


//==========================================================
//		FUNDS CREDIT Command (44H 00H / 4CH 00H / 54H 00H)
//==========================================================
void ICPProt_USDFunds00(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_FUNDS00 *ptCmd = (T_USDCMD_FUNDS00 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	CreditCpcValue nFunds = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_FUNDSCREDIT) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_FUNDS;
	ptCmd->nSubCmd = ICP_USD_CMD_FUNDSCREDIT;
	if(ptCmdPar) {
		nFunds = *((CreditCpcValue *)(ptCmdPar));
		nFunds /= ICPUSDVmcSetup.nUSF;
		Cvt16ToBigEndian(&nFunds);							// 09.17 Aggiunta conversione
		//nFunds = __REV16(nFunds);
	}
	ptCmd->nFunds = nFunds;

	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDFunds00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_FUNDS00 *ptResp = (T_USDRESP_FUNDS00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	T_USDCMD_FUNDS00 *ptCmd;
	CreditCpcValue nFunds;

	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_USDCMD_FUNDS00 *)&ICPInfo.buff[0];
	//MR20 Cvt16ToBigEndian(&ptCmd->nFunds)
	ptCmd->nFunds = __REV16(ptCmd->nFunds);
	nFunds = ptCmd->nFunds * ICPUSDVmcSetup.nUSF;
	ICPUSDHook_FundsCredit(icpSlvAddr, nFunds);
	}
#endif

	return nRespType;
}


//==========================================================
//		FUNDS SEL.PRICE Command (44H 01H / 4CH 01H / 54H 01H)
//==========================================================
void ICPProt_USDFunds01(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_FUNDS01 *ptCmd = (T_USDCMD_FUNDS01 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nSelRow = 0, nSelColumn = 0;
	CreditCpcValue nSelPrice = 0;
	uint16_t nSelID = 0xFFFF;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_FUNDSSELPRICE) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_FUNDS;
	ptCmd->nSubCmd = ICP_USD_CMD_FUNDSSELPRICE;
	if(ptCmdPar) {
		nSelRow = *((uint8_t *)(ptCmdPar));
		nSelColumn = *((uint8_t *)(ptCmdPar + sizeof(uint8_t)));
		nSelPrice = *((CreditCpcValue *)(ptCmdPar + sizeof(uint8_t) + sizeof(uint8_t)));
		nSelID = *((uint16_t *)(ptCmdPar + sizeof(uint8_t) + sizeof(uint8_t) + sizeof(CreditCpcValue)));
	}
	ptCmd->nSelRow = nSelRow;
	ptCmd->nSelColumn = nSelColumn;
	ptCmd->nSelPrice = nSelPrice;
	ptCmd->nSelID = nSelID;

	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDFunds01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_FUNDS01 *ptResp = (T_USDRESP_FUNDS01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	T_USDCMD_FUNDS01 *ptCmd;

	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_USDCMD_FUNDS01 *)&ICPInfo.buff[0];
	//MR20 Cvt16ToBigEndian(&ptCmd->nSelPrice)
	ptCmd->nSelPrice = __REV16(ptCmd->nSelPrice);
	//MR20 Cvt16ToBigEndian(&ptCmd->nSelID)
	ptCmd->nSelID = __REV16(ptCmd->nSelID);
	ICPUSDHook_FundsSelPrice(icpSlvAddr, ptCmd->nSelRow, ptCmd->nSelColumn, ptCmd->nSelPrice, ptCmd->nSelID);
	}
#endif

	return nRespType;
}


//==========================================================
//		CONTROL ENABLE Command (45H 00H / 4DH 00H / 55H 00H)
//==========================================================
void ICPProt_USDControl00(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_CONTROL00 *ptCmd = (T_USDCMD_CONTROL00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_DISABLE) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_CONTROL;
	ptCmd->nSubCmd = ICP_USD_CMD_CONTROLDISABLE;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDControl00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_CONTROL00 *ptResp = (T_USDRESP_CONTROL00 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	ICPDrvSendRW(ICP_RW_ACK);
	ICPUSDHook_ControlEnable(icpSlvAddr, false);												// Disable USD
	ICPProt_SetInhibit(icpSlvAddr, true);
#endif

	return nRespType;
}


//==========================================================
//		CONTROL DISABLE Command (45H 01H / 4DH 01H / 55H 01H)
//==========================================================
void ICPProt_USDControl01(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_CONTROL01 *ptCmd = (T_USDCMD_CONTROL01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_ENABLE) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_CONTROL;
	ptCmd->nSubCmd = ICP_USD_CMD_CONTROLENABLE;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDControl01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_CONTROL01 *ptResp = (T_USDRESP_CONTROL01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	ICPDrvSendRW(ICP_RW_ACK);
	ICPUSDHook_ControlEnable(icpSlvAddr, true);													// Enable USD
	ICPProt_SetInhibit(icpSlvAddr, false);
#endif

	return nRespType;
}


//==========================================================
//		EXP. DIAG. Command (47H FFH / 4FH FFH / 57H FFH)
//==========================================================
void ICPProt_USDExpFF(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_EXPFF *ptCmd = (T_USDCMD_EXPFF *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	uint8_t **pt, *ptData, nDataLen, i;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_EXP;
	ptCmd->nSubCmd = ICP_USD_CMD_EXPDIAG;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_DIAG) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	
	if(ptCmdPar) {
		nDataLen = *((uint8_t *)ptCmdPar);
		if(nDataLen > (ICP_MAX_FRAMELEN - 2))
			nDataLen = ICP_MAX_FRAMELEN - 2;
		pt = (uint8_t **)(ptCmdPar + sizeof(uint8_t));
		ptData = (uint8_t *)(*pt);
		for(i = 0; i < nDataLen; i++) {
			ptCmd->aDiagData[i] = ptData[i];
		}
		nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd) + nDataLen;
	}

	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDExpFFResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_EXPFF *ptResp = (T_USDRESP_EXPFF *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {

			ICPUSDHook_SetDiagData(icpSlvAddr, ptResp->expff.aDiagData, (uint8_t)(ICPInfo.iRX - sizeof(ptResp->expff.nExpFF) - 1));
			nRespType = ICP_USD_RESPTYPE_EXPFF;
			ICPDrvSendRW(ICP_RW_ACK);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
//03/12/2009 agg. ExpDIAG
	{
		T_USDCMD_EXPFF *ptCmd = (T_USDCMD_EXPFF *)&ICPInfo.buff[0];
		T_USDRESP_EXPFF *ptResp = (T_USDRESP_EXPFF *)&ICPInfo.buff[0];
		uint8_t len = (uint8_t)(ICPInfo.iRX - 2 - 1);
		bool fData = false;

		memcpy(ptResp->expff.aDiagData, ptCmd->aDiagData, len);
		fData = ICPUSDHook_SetDiagData(icpSlvAddr, ptResp->expff.aDiagData, len);
		if(!fData) {
			ICPDrvSendRW(ICP_RW_ACK);
		} else {
			ptResp->expff.nExpFF = ICP_USD_DIAG;
			ICPInfo.nFrameLen = len;
			ICPDrvSendMsg(ICPInfo.nFrameLen);
		}
	}
//03/12/2009 agg. ExpDIAG
#endif

	return nRespType;
}


//==========================================================
//		EXP. ID. Command (47H 00H / 4FH 00H / 57H 00H)
//==========================================================
void ICPProt_USDExp00(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_EXP00 *ptCmd = (T_USDCMD_EXP00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_EXP;
	ptCmd->nSubCmd = ICP_USD_CMD_EXPREQUESTID;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDExp00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_EXP00 *ptResp = (T_USDRESP_EXP00 *)&ICPInfo.buff[0];

		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPUSDHook_SetManufactCode(icpSlvAddr, &ptResp->PeriphID.aManufactCode[0], sizeof(ptResp->PeriphID.aManufactCode));
			ICPUSDHook_SetSerialNumber(icpSlvAddr, &ptResp->PeriphID.aSerialNumber[0], sizeof(ptResp->PeriphID.aSerialNumber));
			ICPUSDHook_SetModelNumber(icpSlvAddr, &ptResp->PeriphID.aModelNumber[0], sizeof(ptResp->PeriphID.aModelNumber));
			//MR20 Cvt16ToBigEndian(&ptResp->PeriphID.nSwVersion)
			ptResp->PeriphID.nSwVersion = __REV16(ptResp->PeriphID.nSwVersion);
			ICPUSDHook_SetSWVersion(icpSlvAddr, ptResp->PeriphID.nSwVersion);
			//MR20 Cvt32ToBigEndian(&ptResp->PeriphID.nFOptions)
			ptResp->PeriphID.nFOptions = __REV(ptResp->PeriphID.nFOptions);
			usdInfo->nFOptions = ptResp->PeriphID.nFOptions;
			nRespType = ICP_USD_RESPTYPE_PERIPHID;
			ICPDrvSendRW(ICP_RW_ACK);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	uint32_t nFOptions;
	T_USDRESP_EXP00 *ptResp = (T_USDRESP_EXP00 *)&ICPInfo.buff[0];
	ptResp->PeriphID.nUsdResp = ICP_USD_PERIPHID;
	ICPUSDHook_GetManufactCode(ptResp->PeriphID.aManufactCode, sizeof(ptResp->PeriphID.aManufactCode));
	ICPUSDHook_GetSerialNumber(ptResp->PeriphID.aSerialNumber, sizeof(ptResp->PeriphID.aSerialNumber));
	ICPUSDHook_GetModelNumber(ptResp->PeriphID.aModelNumber, sizeof(ptResp->PeriphID.aModelNumber));
	ptResp->PeriphID.nSwVersion = ICPUSDHook_GetSWVersion();
	//MR20 Cvt16ToBigEndian(&ptResp->PeriphID.nSwVersion)
	ptResp->PeriphID.nSwVersion = __REV16(ptResp->PeriphID.nSwVersion);

	nFOptions = 0;
	if(ICPProt_USDGetMode1(icpSlvAddr)) nFOptions |= ICP_USDOPT_USD1;
	if(ICPProt_USDGetMode2(icpSlvAddr)) nFOptions |= ICP_USDOPT_USD2;
	if(ICPProt_USDGetFTL(icpSlvAddr)) nFOptions |= ICP_USDOPT_FTL;
	Cvt32ToBigEndian(&nFOptions);
	ptResp->PeriphID.nFOptions = nFOptions;
	usdInfo->nFOptions = ptResp->PeriphID.nFOptions;

	ICPInfo.nFrameLen = sizeof(ptResp->PeriphID);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif

	return nRespType;
}


//==========================================================
//		EXP. OPT. Command (47H 01H / 4FH 01H / 57H 01H)
//==========================================================
void ICPProt_USDExp01(void) {
#if (_IcpMaster_ == true)

	T_USDCMD_EXP01 *ptCmd = (T_USDCMD_EXP01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint32_t nFOptions;

	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_EXP;
	ptCmd->nSubCmd = ICP_USD_CMD_EXPENABLEOPT;
	nFOptions = ICP_USDOPT_NONE;
	nFOptions = usdInfo->nFOptions;
	if((nFOptions & ICP_USDOPT_FTL) && ICPProt_USDGetFTL(icpSlvAddr))
		nFOptions |= ICP_USDOPT_FTL;
	if((nFOptions & ICP_USDOPT_USD1) && ICPProt_USDGetMode1(icpSlvAddr))
		nFOptions |= ICP_USDOPT_USD1;
	if((nFOptions & ICP_USDOPT_USD2) && ICPProt_USDGetMode2(icpSlvAddr))
		nFOptions |= ICP_USDOPT_USD2;
	Cvt32ToBigEndian(&nFOptions);
	ptCmd->nFOptions = nFOptions;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_USDExp01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_EXP01 *ptResp = (T_USDRESP_EXP01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	{
	T_USDCMD_EXP01 *ptCmd;

	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_USDCMD_EXP01 *)&ICPInfo.buff[0];
	//MR20 Cvt32ToBigEndian(&ptCmd->nFOptions)
	ptCmd->nFOptions = __REV(ptCmd->nFOptions);
	usdInfo->nFOptions = ptCmd->nFOptions;
	}
#endif

	return nRespType;
}


//==========================================================
//		EXP. SEND BLOCKS Command (47H 02H / 4FH 02H / 57H 02H)
//==========================================================
void ICPProt_USDExp02(void) {
#if (_IcpMaster_ == true)
/*
	T_USDCMD_EXP02 *ptCmd = (T_USDCMD_EXP02 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nBlock = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_SENDBLOCKS) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_EXP;
	ptCmd->nSubCmd = ICP_USD_CMD_EXPSENDBLOCKS;
	if(ptCmdPar) {
		nBlock = *((uint8_t *)(ptCmdPar));
	}
	ptCmd->nBlock = nBlock;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
*/
#endif
}

uint8_t ICPProt_USDExp02Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_EXP02 *ptResp = (T_USDRESP_EXP02 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//da fare...
#endif

	return nRespType;
}


//==========================================================
//		EXP. SEND BLOCKN Command (47H 03H / 4FH 03H / 57H 03H)
//==========================================================
void ICPProt_USDExp03(void) {
#if (_IcpMaster_ == true)
/*
	T_USDCMD_EXP03 *ptCmd = (T_USDCMD_EXP03 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nBlockNum = 0, nBytes = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_SENDBLOCKN) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_EXP;
	ptCmd->nSubCmd = ICP_USD_CMD_EXPSENDBLOCKN;
	if(ptCmdPar) {
		nBlockNum = *((uint8_t *)(ptCmdPar));
		nBytes = *((uint8_t *)(ptCmdPar) + sizeof(uint8_t));
		memcpy(ptCmd->aData, ptCmdPar+2, nBytes);
	}
	ptCmd->nBlockNum = nBlockNum;
	nFrameLen = nBytes + 3;
	ICPDrvSendMsg(nFrameLen);
*/
#endif
}

uint8_t ICPProt_USDExp03Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_EXP03 *ptResp = (T_USDRESP_EXP03 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//da fare...
#endif

	return nRespType;
}


//==========================================================
//		EXP. REQ. BLOCKN Command (47H 04H / 4FH 04H / 57H 04H)
//==========================================================
void ICPProt_USDExp04(void) {
#if (_IcpMaster_ == true)
/*
	T_USDCMD_EXP04 *ptCmd = (T_USDCMD_EXP04 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nBlock = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_REQBLOCKN) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_EXP;
	ptCmd->nSubCmd = ICP_USD_CMD_EXPREQBLOCKN;
	if(ptCmdPar) {
		nBlock = *((uint8_t *)(ptCmdPar));
	}
	ptCmd->nBlock = nBlock;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
*/
#endif
}

uint8_t ICPProt_USDExp04Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_EXP04 *ptResp = (T_USDRESP_EXP04 *)&ICPInfo.buff[0];
		//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;

		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {

			switch(ptResp->Exp04.nUsdResp) {
			case 0:
				ICPUSDHook_DataBlockReq(icpSlvAddr, ptResp->Exp04.nBlock);
				break;
			case 1:
				ICPUSDHook_SendDataReq(icpSlvAddr, ptResp->Exp04.nBlock);
				break;
			case 2:
			default:
				ICPUSDHook_DataBlock(icpSlvAddr, ptResp->Exp04.nBlock, ptResp->Exp04.aData, (uint8_t)(ICPInfo.iRX - 2 - 1));
				break;
			}
			nRespType = ICP_USD_RESPTYPE_BLOCKN;
			ICPDrvSendRW(ICP_RW_ACK);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//da fare...
#endif

	return nRespType;
}


//==========================================================
//		EXP. SEND BLOCK Command (47H 05H / 4FH 05H / 57H 05H)
//==========================================================
void ICPProt_USDExp05(void) {
#if (_IcpMaster_ == true)
/*
	T_USDCMD_EXP05 *ptCmd = (T_USDCMD_EXP05 *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar = nil;
	uint8_t nFrameLen;
	//uint8_t currUSD = ICPInfo.nCurrPeriphAddr;
	uint8_t nBytes = 0;

	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_USD_SENDBLOCK) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_USD_CMD_EXP;
	ptCmd->nSubCmd = ICP_USD_CMD_EXPSENDBLOCK;
	if(ptCmdPar) {
		nBytes = *((uint8_t *)(ptCmdPar));
		memcpy(ptCmd->aData, ptCmdPar+1, nBytes);
	}
	nFrameLen = nBytes + 2;
	ICPDrvSendMsg(nFrameLen);
*/
#endif
}

uint8_t ICPProt_USDExp05Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_USDRESP_EXP05 *ptResp = (T_USDRESP_EXP05 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//da fare...
#endif

	return nRespType;
}



#if _IcpUsdFTL_ > 0

void ICPProt_USDExpFA(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_USDExpFAResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//...
#endif
	return nRespType;
}


void ICPProt_USDExpFB(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_USDExpFBResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//...
#endif
	return nRespType;
}


void ICPProt_USDExpFC(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_USDExpFCResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//...
#endif
	return nRespType;
}


void ICPProt_USDExpFD(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_USDExpFDResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//...
#endif
	return nRespType;
}


void ICPProt_USDExpFE(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_USDExpFEResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
	//...
#endif
	return nRespType;
}
#endif	// _IcpUsdFTL_ > 0





//-------------------------------------------------
// Routines to give USD info
//-------------------------------------------------
bool ICPProt_USDGetFTL(uint8_t slaveType) { 
	return _IcpUsdFTL_; 
}

bool ICPProt_USDGetMode1(uint8_t slaveType)
{
	switch(slaveType)
	{
	case ICP_USD1_ADDR:
		return _IcpUsd1Mode1_;
	case ICP_USD2_ADDR:
		return _IcpUsd2Mode1_;
	case ICP_USD3_ADDR:
		return _IcpUsd3Mode1_;
	default:
		return _IcpUsd1Mode1_;
	}
}

bool ICPProt_USDGetMode2(uint8_t slaveType)
{
	switch(slaveType)
	{
	case ICP_USD1_ADDR:
		return _IcpUsd1Mode2_;
	case ICP_USD2_ADDR:
		return _IcpUsd2Mode2_;
	case ICP_USD3_ADDR:
		return _IcpUsd3Mode2_;
	default:
		return _IcpUsd1Mode2_;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: IsUSD1Connected
	Funzione chiamata dalla USD_Task quando sono in Master Mode.

	IN:	  - 
	OUT:  - TRUE se periferica USD connessa
\*--------------------------------------------------------------------------------------*/
bool IsUSD1Connected(void) {

	return (ICPInfo.nSlaveLinkMask & ICP_USD1_LINK ? TRUE : FALSE);
}

/*--------------------------------------------------------------------------------------*\
Method: Get_USD1_CreditoAttuale
	Chiamata in Master Mode dalla USD_Task per avere il credito visualizzato sulla 
	periferica USD

	IN:	  - 
	OUT:  - credito visualizzato sulla periferica USD
\*--------------------------------------------------------------------------------------*/
uint32_t Get_USD1_CreditoAttuale(void) {
	
	return ICPUSD1Info.USD_CreditoInviato;
}

/*--------------------------------------------------------------------------------------*\
Method: Set_USD1_NewCredit
	Chiamata in Master Mode dalla USD_Task per memorizzare il credito inviato alla 
	periferica USD.

	IN:	  - 
	OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  Set_USD1_NewCredit(uint32_t NuovoCredito) {
	
	ICPUSD1Info.USD_CreditoInviato = NuovoCredito;
}



/*
#if (_IcpSlaveCode_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
T_ICP_USDPOLLDATA ICPUSDPollInfo;

void ICPProt_USDResetPollData(void) {
	ICPUSDPollInfo.nData = 0;
	ICPUSDPollInfo.nWrData = 0;
	ICPUSDPollInfo.nRdData = 0;
	ICPProt_USDSetStatus(ICP_USD_JUSTRESET);
}

uint8_t ICPProt_USDGetPollData(uint8_t *ptData, bool fAllData) {
	uint8_t nData = ICPUSDPollInfo.nData;
	uint8_t nRdData = ICPUSDPollInfo.nRdData;

	if(ptData && nData) {
		memcpy(ptData, &ICPUSDPollInfo.aPollData[nRdData], nData);
		ICPUSDPollInfo.nRdData = 0;
		ICPUSDPollInfo.nData = 0;
	}

	return nData;
}


bool ICPProt_USDSetStatus(uint16_t nStatusType) {
	if(ICPUSDPollInfo.nData < sizeof(ICPUSDPollInfo.aPollData)) {
		uint16_t *ptStatus;
		switch(nStatusType) {
		case ICP_USD_HEALTHSAFETYVIOLATION:
		case ICP_USD_CHUTESENSORFAILURE:
		case ICP_USD_KEYPADSWITCHFAILURE:
			ICPUSDPollInfo.aPollData[ICPUSDPollInfo.nWrData] = ICP_USD_ERRCODE;
			ptStatus = (uint16_t*)&ICPUSDPollInfo.aPollData[ICPUSDPollInfo.nWrData + 1];
			*ptStatus = nStatusType;
			Cvt16ToBigEndian(ptStatus)
			ICPUSDPollInfo.nWrData = (ICPUSDPollInfo.nWrData + 3) & 0x0F;
			ICPUSDPollInfo.nData += 3;
			return true;
		case ICP_USD_JUSTRESET:
			ICPUSDPollInfo.aPollData[ICPUSDPollInfo.nWrData] = ICP_USD_JUSTRESET;
			ICPUSDPollInfo.nWrData = (ICPUSDPollInfo.nWrData + 1) & 0x0F;
			ICPUSDPollInfo.nData += 1;
			return true;
		default:
			// invalid status type!
			break;
		}
	}
	return false;
}
#endif
*/
#endif //(_IcpSlave_ & (ICP_USD1 | ICP_USD2 | ICP_USD3))
