/**
  ******************************************************************************
  * @file    file_operations.h
  * @author  Abe - (C) COPYRIGHT AbTech september 2019
  * @brief   Baltom USB file functions headers 
  ******************************************************************************
 */

#ifndef __FILE_OPERATION_H__
#define __FILE_OPERATION_H__


// =================================================================================================
extern FATFS USBH_fatfs;

#define _INIT_                  0
#define RTC_INIT                0
#define FIXED_NAME              0
#define	NO_CLEAR_LOG			0

//#define EE_24LC1025_ADD 		0x50

#define BUFFER_LENGTH		(1024)						// 1K bytes
#define TX_AWS_BUFFER_SIZE	(1024)						// 1K bytes

#define LED_ROSSO			0
#define LED_VERDE			1

#define	AUDIT_LIST			1

typedef enum {
	USB_ERR_OK        	= 0,	// 0
	ERR_NO_CONFIG_FILE,			// 1
	ERR_RW_FILE,				// 2
	ERR_NO_DIR,					// 3
	ERR_PSW,					// 4
	ERR_FULL_DIR,				// 5
	ERR_AUDIT_DENIED,			// 6
	ERR_NO_DATA_LOG,			// 7
	ERR_EE_WRITE,				// 8
	USB_AWS_ERR_OK        		// 9
}USB_Key_Err;



/* Private function prototypes -----------------------------------------------*/

bool MSC_File_Operations(void);
uint32_t EE_cfg_read(uint8_t*);
static uint16_t EE_log_read(uint8_t* buf);

static uint8_t EE_Block(uint8_t *arr, uint32_t size);
static bool inc_nome(char[], uint8_t *);
static void copyname (char *src, char * dst, uint8_t size);
static void SetLed(uint32_t output, bool state);
static FRESULT set_timestamp (char *obj);
static USB_Key_Err Export_conf(void);
static USB_Key_Err Export_Log(void);
static USB_Key_Err Export_Audit(void);
static USB_Key_Err Import_Conf(void);
static void Select_Message(USB_Key_Err MsgNum, uint8_t RowNum);

bool write_app_flag(uint16_t add, uint8_t dato);

typedef enum{
	FILENAME_COMPARE_OK = 0,
	FILENAME_FAIL = -1
}CompFileName;



#endif  //__FILE_OPERATION_H__