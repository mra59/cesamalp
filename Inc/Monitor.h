/** ###################################################################
**     Filename    : Monitor.h
** ###################################################################*/


/* ===================================================*/

typedef enum {
	NoMonitor,
	MonitorActive,
	MonCompleto,
	MonUtente,
	MonCliente
} MonitorMenu;


typedef enum {
	WaitPC_Call,				//	00
	InviaMsg,					//	01
	SendMenuComandi,			//	02
	SendSceltaComando,			//	03
	WaitInvioSceltaComando,		//	04
	SceltaComando,				//	05
	Ask_Address,				//	06
	Read_date,					//	07
	Write_date,					//	08
	Read_time,					//	09
	Write_time,					//	0A
	Debug_Mifare,				//	0B
	WaitAddress,				//	0C
	readeeprom,					//	0D
	AskNumByteToRead,			//	0E
	WaitNumByteToRead,			//	0F
	AskParametroWrite,			//	10
	WaitParameterWrite,			//	11
	writeeeprom,				//	12
	Quit_Monitor,				//	13
	Fill_EEProm,				//	14
	AzzeraAudit,				//	15
	ReadCardCodes,				//	16
	ResetAreaInterscambioUSB,	//	17
	WaitMonitorEndSession,		//	18
	ShowCardError,				//	19
	InsertPassword,				//	1A
	WaitPassword,				//	1B
	WaitConfirmEraseEEprom,		//	1C
	WaitConfirmEraseAudit,		//	1D
	SendComandoListaComandi,	//	1E
	DefaultConfig,				//	1F
	WaitConfirmDefaultConfig,	//	20
	ClrAreaUSB,					//	21
	DebugVendReqData,			//	22
	ShowVendReqData,			//	23
	DebugExecutiveProtocol,		//	24
	ShowExecProtComm,			//  25
	PauseShowExecProtComm,		//  26
	DebugMDBProtocol,			//	27
	ShowMDBProtComm,			//  28
	PauseShowMDBProtComm,		//  29
	TxPauseMDBMessage,			//	30
	TxPauseExecMessage,			//	31
	WaitFillParameter,			//	32
	AskConfirmFill_EEProm,		//	33
	AzzeraBackupEEPROM,			//	34
	WaitConfirmEraseBackupEE,	//	35
	SendConfigToPC,				//	36
	WaitConfirmSendConfig,		//	37
	Toggle_DDCMP,				//  38
	WaitDDCMP_LogParameter,		//  39
	Memo_DDCMP_Log_State,		//  40
	SendLogBufferToPC,			//	41
	WaitConfirmSendLogBuffer,	//	42
	PN512_registers,			//	43
	WaitSceltaRead_Write,		//	44
	Ask_PN512Address,			//	45
	WaitPN512Address,			//	46
	AskNewPN512RegVal,			//	47
	ReadPN512Reg,				//	48
	WritePN512Reg,				//	49
	WaitParamPN512,				//	50
	AskSceltaRead_Write,		//	51
	GetConfFromPC,				//	52
	GettingConfBuffer,			//	53
	Fill_StartAddress,			//	54
	Fill_EndAddress,			//	55
	Ask_Fill_EndAddress,		//	56
	Fill_EE_Value,				//	57
	Esegui_Fill_EEprom,			//	58
	Read_gVars_Struct,			//	59
	DebugExecMasterProtocol,	//	60
	ShowExecMSTProtComm,		//	61
	PauseShowExecMSTProtComm,	//	62
	TxPauseExecMSTMessage		//	63
	
} MFProgrammer;



typedef struct {
	MFProgrammer	state;
	bool			RxCR;								// Ricevuto Carriage Return
	bool			PC_Call;							// Richiesta attivazione Monitor da PC
	bool			Call_DDCMP_PC;						// Richiesta di comunicazione DDCMP da PC
	bool			Set_DDCMP_PC;						// Attivate linee connessione hardware verso PC
	bool			ProtMonitor;						// Protocollo "System Debug"
	bool			RxCMD;								// Ricevuto comando completo
	bool			DontTxChrBack;						// Non ritrasmettere il carattere ricevuto 
	bool			TxMsgInCorso;						// Non modificare Buffer perche' Tx in corso 
	bool			EnableLogRxMDB;						// Abilita la registrazione dei dati ricevuti (serve dopo la pausa per sincronizzare i dati col master)
	bool			EnableLogRxExec;					// Abilita la registrazione dei dati ricevuti (serve dopo la pausa per sincronizzare i dati col master)
	bool			LogMDBPowerUp;						// Log MDB Protocol gia' attivo al power up
	uint8_t			RxCount;							// Pointer al buffer di ricezione
	uint8_t			TxCount;							// Pointer al buffer di trasmissione
	uint8_t			LogProtocol;						// Attivata registrazione protocollo
	MonitorMenu		MonFeatures;						// Tipo di funzioni disponibili in base alla password digitata
	uint8_t			GenericCntIN;						// Pointer caratteri inseriti in CommBuff   			         
	uint8_t			GenericCntOUT;						// Pointer caratteri estratti da CommBuff   			         
	uint16_t		VendDeniedType;						// Contiene il motivo del Vend Denied
	char			CommBuff[128];						// Buffer di comunicazione
} Mon;

extern Mon SysMonitor;

/* =====================================*/


#define	read_ee			1
#define	read_ee_ASCII	21
#define	write_ee		2
#define	read_date		3
#define	readPN512		1
#define	writePN512		2

#define	TypeExec		1
#define	TypeMDB			2
#define	TypeMasterExec	3

#define	PswMia			25965							// Psw accesso completo
#define	maxnumcmd		 9
#define	RespPos			32								// Inizio buffer di risposta al PC
#define	RWBuff		   256								// Inizio buffer bytes di memoria da scrivere o da leggere
#define	MaxAddrLen		 6								// Massimo numero di byte per rappresentare l'indirizzo di memoria
#define	NumBlocksPerRaw	 7								// Numero di blocchi da 4 Hex (8 caratteri) per ogni riga di terminale
//#define	PageSize		128
#define	PswMHD			20131							// Psw accesso MHD
#define	ResetCmd		99								// Comando di Reset
#define	RecProtocolMDB	9294							// Registrazione protocollo MDB
#define	RecProtocolExec	9601							// Registrazione protocollo Executive
#define	RecProtExecMST	2233							// Registrazione protocollo Executive Master

extern	LDD_RTC_TTime DataOra;
extern	const char chr_STAR;

/* ===================================================*/

void  			SystemMonitor(void);
void  			MonitorRxData(uint8_t);
static	void	Read_EEPROM(uint8_t);
static	void	PrintError(const char *);
void  			TxCardErr(uint16_t);
static 	bool 	ElaboraAddress(void);
static 	bool 	ElaboraNumeroComando(void);
static 	bool 	GetNumBytes(uint8_t);
static 	bool 	GetNumero(uint8_t);
static 	bool 	GetValToWrite(void);
static 	bool 	FindEscape(void);
static 	bool 	TestMenuAvailable(uint16_t);
static  void 	SCI_RxOFF(void);
bool  			TestMonitor(void);
void  			ClrFlags_PC(void);
void  			Monitor_LogMDBRxData(uint8_t RxData);
void  			Monitor_LogMDBTxData(uint8_t dato, uint8_t ModeBitStatus);

void  			Monitor_LogExecTxToRR(uint8_t dato);
void  			Monitor_LogExecRxRR(uint8_t RxData);
void  			Monitor_LogExecTxToVMC2(uint8_t dato);
void  			Monitor_LogExecRxVMC2(uint8_t RxData);

void  			Monitor_SendProtChar_To_PC(void);
void  			Send_DDCMP_Echo_Al_PC(void);
void  			Monitor_ExecVendReq(uint8_t);
void  			Monitor_VendReqMDB(CreditCpcValue, uint16_t);
void  			Monitor_DeniedReason(uint16_t);
void  			Monitor_SendDeniedReason_To_PC(void);
void  			Monitor_DeniedPerNoKey(void);
void  			Monitor_DeniedPerNoIDLE(void);
void  			Monitor_DeniedPerNoSelVal(void);
uint32_t		AddLogDataToEEPromBufferLog(uint8_t *, uint32_t, bool);
void  			InitLogBufferPointers(bool MemClr);
void  			Monitor_DeniedPerSystOFF_DDCMP_AudExtFull(void);

void  			GetStartLog(void);
void  			TranferLogToEE(bool, bool);
void  			SendMessageToPC(const char *);
uint32_t		ReadLogFileContentLen(void);
void  			AttivaMonitor(void);
bool  			IsMonitorActive(void);
bool  			IsMonitorReceivingFiles(void);
static	bool	TestTxMsgInCorso(void);
static  void 	SendBackNULL(void);
void 			SendBuffer(uint8_t* tx_buf, uint16_t len);
void  			Monitor_EchoDDCMP_Slave(uint8_t dato);
void  			Monitor_EchoDDCMP_Master(uint8_t RxData);
void  			Rd_EEBuffStartAddr_And_BankSelect(uint8_t TipoBuff);

#if USB_CDC_PC
	void  		Monitor_CDC_Receive(uint8_t* Buf, uint32_t *Len);
#endif






