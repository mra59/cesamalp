/****************************************************************************************
File:
    ICPBill.c

Description:
    Protocollo ICP per Bill Validator

History:
    Date       Aut  Note
    Set 2012 	MR   

*****************************************************************************************/

#include <string.h>
#include <intrinsics.h>

#include "ORION.H"
#include "icpProt.h"
#include "icpVMCProt.h"
#include "icpBILLProt.h"


#if (_IcpSlave_ & ICP_BILLVALIDATOR)

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
T_ICP_BILLPOLLDATA ICPBILLPollInfo;
#endif


T_ICP_BILLINFO ICPBILLInfo;


/*---------------------------------------------------------------------------------------------*\
Method: swap

	IN:	  - 
	OUT:  - 
\*----------------------------------------------------------------------------------------------*/
uint32_t swap(uint32_t in)
{
  in = ((in & 0xff000000) >> 24) | ((in & 0x00FF0000) >> 8) | ((in & 0x0000FF00) << 8) | ((in & 0xFF) << 24);
  in = (in >> 16) | (in << 16);
  return in;
}

void BILLInit(uint8_t nSlaveType) {

	ICPProt_InitSlave(nSlaveType);
	memset(&ICPBILLInfo, 0, sizeof(T_ICP_BILLINFO));
	sCMDICPCash.acceptAmountBillChgd = false;													// 26.06.15 Aggiunta perche' non c'era il suo clr
	DisattivaEnDisBill();																		// 26.06.15 Per riabilitare il BV dopo un reset caldo
	#if (_IcpMaster_ == true)
	ICPBILLInfo.nWaitInitCnt = ICP_TM_BILLWAITINIT / ICP_TM_POLLPERIPH;
	ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
	ICPInfo.nSlaveLinkMask &= ~icpSlvLinkMask;
	#endif
	#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	ICPProt_BILLResetPollData();
#if kReq456Support
//***********************
//  MSTNOACK_SLVNORETRY (COINCO_2P)
//***********************
	ICPSetFrameSent(false);
	ICPSetFrameAcked(false);
	ICPSetNewState(-1);
	ICPSetNewSubState(0);
//***********************
#endif
	#endif
}




//-------------------------------------------------
//
//					B V	Slave Code
//
//-------------------------------------------------
#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)

void BILLSlaveResp(void) {

#if kReq456Support
//***********************
//	MSTNOACK_SLVNORETRY (COINCO_2P / OSCARMDBONLY)
//***********************
	//
	// Check if Retry have to do...
	//
	if(ICPGetFrameSent) {
		if(ICPGetFrameAcked) {
			//
			// Last frame sent is ok, reset last event && change to new state
			//
			if(ICPGetNewState != -1)
				ICPProt_ChangeState(icpSlv, ICPGetNewState, ICPGetNewSubState);
			ICPSetFrameSent(false);
			ICPSetFrameAcked(false);
			ICPSetNewState(-1);
			ICPSetNewSubState(0);
		}
	}
//***********************
#endif

	if(ICPDrvGetComState() == COM_RXFRAME) {

		// Per compatibilit� modifica su CHG (vedi modifica x Harting sigarette)
		/*if(!ICPBILLHook_IsInitCompleted())
			return;
		*/

		#if (_IcpSmsAlarms_ == true)
		//if(!ICPBILLHook_IsEnabled()) {
			switch(ICPCommand) {
			case ICP_BILL_CMD_RESET:
				ICPProt_BILLResetResp();
				break;
			case ICP_BILL_CMD_BILLTYPE:
				ICPProt_BILLBillTypeResp();
				break;
			default:
				break;
			}
			return;
		//}
		#endif

		switch(ICPCommand) {
		case ICP_BILL_CMD_RESET:
			ICPProt_BILLResetResp();						// go to ICP_BILL_INACTIVE State
			break;
		case ICP_BILL_CMD_STATUS:
			ICPProt_BILLStatusResp();
			break;
		case ICP_BILL_CMD_SECURITY:
			ICPProt_BILLSecurityResp();
			break;
		case ICP_BILL_CMD_POLL:
			ICPProt_BILLPollResp();
			break;
		case ICP_BILL_CMD_BILLTYPE:
			ICPProt_BILLBillTypeResp();
			break;
		case ICP_BILL_CMD_ESCROW:
			ICPProt_BILLEscrowResp();
			break;
		case ICP_BILL_CMD_STACKER:
			ICPProt_BILLStackerResp();
			break;
		case ICP_BILL_CMD_EXP:
			switch(ICPSubCommand) {
			case ICP_BILL_CMD_EXPREQUESTID1:
				ICPProt_BILLExp00Resp();
				break;
			case ICP_BILL_CMD_EXPENABLEOPT:
				ICPProt_BILLExp01Resp();
				break;
			case ICP_BILL_CMD_EXPREQUESTID2:
				ICPProt_BILLExp02Resp();
				break;

			case ICP_BILL_CMD_EXPDIAG:
			default:
				break;
			}
			break;

		default:
			break;
		}
		//BUG: if command not handled no following commands recognized!
		if(ICPDrvGetComState() == COM_RXFRAME) {
			// if rxframe not handled, no tx data, then discard it
			ICPDrvSetComState(COM_IDLE);
		}

	}
}
#endif

//-------------------------------------------------
//
//					I N A C T I V E
//
//-------------------------------------------------
void BILLInactive(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint8_t nComState;
		bool fJustReset = false;

		/*nPeriphID = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[nPeriphID].nState != ICP_BILL_INACTIVE)
			return;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_BILL_INACTIVE_TXRESET:
					if(icpSlvCheckNoRespState <= 1) {
						ICPProt_BILLReset();
					}
					break;
				case ICP_BILL_INACTIVE_WAITINIT:
					if(!ICPProt_WaitInit(icpSlvAddr))
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_INACTIVE_TXPOLL_1);
					break;
				case ICP_BILL_INACTIVE_TXPOLL_1:
				case ICP_BILL_INACTIVE_TXPOLL_2:
					ICPProt_BILLPoll();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_BILL_INACTIVE_TXRESET:
				nRespType = ICPProt_BILLResetResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_INACTIVE_WAITINIT);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_INACTIVE_TXPOLL_1:
				nRespType = ICPProt_BILLPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_BILL_RESPTYPE_DISABLED:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_BILL_DISABLED, ICP_BILL_DISABLED_TXSTATUS);
					}
					break;
				case ICP_RESPTYPE_JUSTRESET:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask) {
						ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
						ICPProt_ChangeState(icpSlv, ICP_BILL_DISABLED, ICP_BILL_DISABLED_TXSTATUS);
					} else {
						ICPInfo.nSlaveJustResetMask |= icpSlvLinkMask;
					}
					break;
				default:
					break;
				}
				////ICPProt_ChangeState(icpSlv, -1, ICP_BILL_INACTIVE_TXPOLL_2);
				break;
			case ICP_BILL_INACTIVE_TXPOLL_2:
				nRespType = ICPProt_BILLPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_BILL_RESPTYPE_DISABLED:
					if(ICPInfo.nSlaveJustResetMask & icpSlvLinkMask)
						fJustReset = true;
					break;
				case ICP_RESPTYPE_JUSTRESET:
					fJustReset = true;
					break;
				default:
					break;
				}
				if(fJustReset) {
					ICPInfo.nSlaveJustResetMask &= ~icpSlvLinkMask;
					ICPProt_ChangeState(icpSlv, ICP_BILL_DISABLED, ICP_BILL_DISABLED_TXSTATUS);
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	BILLSlaveResp();
#endif
}


//-------------------------------------------------
//
//					D I S A B L E D
//
//-------------------------------------------------
void BILLDisabled(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint16_t nComState;					//16/11/00 OSz uint8_t --> uint16_t (-15)

		/*nPeriphID = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[nPeriphID].nState != ICP_BILL_DISABLED)
			return;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_BILL_DISABLED_TXSTATUS:
					ICPProt_SetReady(icpSlvAddr, false);
					ICPProt_BILLStatus();
					break;
				case ICP_BILL_DISABLED_TXEXP00:
					ICPProt_BILLExp00();
					break;
				case ICP_BILL_DISABLED_TXEXP01:
					ICPProt_BILLExp01();
					break;
				case ICP_BILL_DISABLED_TXEXP02:
					ICPProt_BILLExp02();
					break;
				case ICP_BILL_DISABLED_TXSTACKER:
					ICPProt_BILLStacker();
					break;
				case ICP_BILL_DISABLED_TXBILLTYPE:
				case ICP_BILL_DISABLED_TXBILLTYPE1:
					ICPProt_BILLBillType();
					break;
				case ICP_BILL_DISABLED_TXPOLL:
					ICPProt_BILLPoll();
					break;
				case ICP_BILL_DISABLED_TXSECURITY:
					ICPProt_BILLSecurity();
					break;
				/*
				case ICP_BILL_DISABLED_TXESCROW:
					ICPProt_BILLEscrow();
					break;
				*/
				case ICP_BILL_DISABLED_TXEXPDIAG:
					ICPProt_BILLExpFF();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_BILL_DISABLED_TXSTATUS:
				nRespType = ICPProt_BILLStatusResp();
				switch(nRespType) {
				case ICP_BILL_RESPTYPE_STATUS:
					switch(ICPProt_GetLevel()) {
					case 1:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXEXP00);
						break;
					case 2:
					default:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXEXP02);
						break;
					}
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXEXP00:
				nRespType = ICPProt_BILLExp00Resp();
				switch(nRespType) {
				case ICP_BILL_RESPTYPE_EXP00:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXSTACKER);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXEXP01:
				nRespType = ICPProt_BILLExp01Resp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXSTACKER);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXEXP02:
				nRespType = ICPProt_BILLExp02Resp();
				switch(nRespType) {
				case ICP_BILL_RESPTYPE_EXP02:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXEXP01);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXSTACKER:
				nRespType = ICPProt_BILLStackerResp();
				switch(nRespType) {
				case ICP_BILL_RESPTYPE_STACKER:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXBILLTYPE);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXBILLTYPE:
				nRespType = ICPProt_BILLBillTypeResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(!ICPBILLInfo.BillTypeState.nBillEnable)
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXPOLL);
					else
						ICPProt_ChangeState(icpSlv, ICP_BILL_ENABLED, ICP_BILL_ENABLED_TXPOLL);
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_BILL_BILLTYPEDONE);
					ICPProt_SetReady(icpSlvAddr, true);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXBILLTYPE1:
				nRespType = ICPProt_BILLBillTypeResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(!ICPBILLInfo.BillTypeState.nBillEnable)
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXPOLL);
					else
						ICPProt_ChangeState(icpSlv, ICP_BILL_ENABLED, ICP_BILL_ENABLED_TXPOLL);
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_BILL_BILLTYPEDONE);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXSECURITY:
				nRespType = ICPProt_BILLSecurityResp();
				switch(nRespType) {
					case ICP_RESPTYPE_RW_ACK:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXPOLL);
						break;
					default:
						break;
				}
				break;
			/*
			case ICP_BILL_DISABLED_TXESCROW:
				nRespType = ICPProt_BILLEscrowResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			*/
			case ICP_BILL_DISABLED_TXPOLL:
				nRespType = ICPProt_BILLPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXSTATUS);
					break;
				case ICP_BILL_RESPTYPE_INVALIDESCROW:
					break;
				case ICP_BILL_RESPTYPE_BUSY:
					break;
				case ICP_BILL_RESPTYPE_INHIBIT:
					break;
				case ICP_BILL_RESPTYPE_BILLSTACKED:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXSTACKER);
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_BILL_RESET:
						ICPProt_ChangeState(icpSlv, ICP_BILL_INACTIVE, ICP_BILL_INACTIVE_TXRESET);
						break;
					case ICP_VMCCMD_BILL_SECURITY:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXSECURITY);
						break;
					case ICP_VMCCMD_BILL_STACKER:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXSTACKER);
						break;
					case ICP_VMCCMD_BILL_ENABLE:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXBILLTYPE1);
						break;
					case ICP_VMCCMD_BILL_DIAG:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXEXPDIAG);
						break;
					case ICP_VMCCMD_BILL_ESCROW:
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_DISABLED_TXEXPDIAG:
				nRespType = ICPProt_BILLExpFFResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_BILL_RESPTYPE_EXPFF:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_BILL_DIAGDONE);
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_DISABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	BILLSlaveResp();
#endif
}


//-------------------------------------------------
//
//					E N A B L E D
//
//-------------------------------------------------
void BILLEnabled(void) {
#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		uint8_t  nRespType;
		uint8_t nComState;

		/*nPeriphID = ICPInfo.nCurrPeriph;
		if(ICPInfo.aPeriphInfo[nPeriphID].nState != ICP_BILL_ENABLED)
			return;
		nCurrSubState = ICPInfo.aPeriphInfo[nPeriphID].nSubState;*/

		nComState = ICPDrvGetComState();
		switch(nComState) {
		case COM_IDLE:
		case COM_MSTIDLE:
			if(ICPInfo.fTmPoll) {
				switch(icpSlvSubState) {
				case ICP_BILL_ENABLED_TXPOLL:
				case ICP_BILL_ENABLED_WAITENDESCROW:
					ICPProt_BILLPoll();
					break;
				case ICP_BILL_ENABLED_TXBILLTYPE:
				case ICP_BILL_ENABLED_TXBILLTYPE1:
					ICPProt_BILLBillType();
					break;
				case ICP_BILL_ENABLED_TXSTACKER:
					ICPProt_BILLStacker();
					break;
				case ICP_BILL_ENABLED_TXESCROW:
					ICPProt_BILLEscrow();
					break;
				case ICP_BILL_ENABLED_TXSECURITY:
					ICPProt_BILLSecurity();
					break;
				case ICP_BILL_ENABLED_TXEXPDIAG:
					ICPProt_BILLExpFF();
					break;
				default:
					break;
				}
			}
			break;
		case COM_RXFRAME:
			ICPProt_SetLink(icpSlvAddr, true);
			switch(icpSlvSubState) {
			case ICP_BILL_ENABLED_TXPOLL:
				nRespType = ICPProt_BILLPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_BILL_DISABLED, ICP_BILL_DISABLED_TXSTATUS);
					break;
				case ICP_BILL_RESPTYPE_INVALIDESCROW:
					break;
				case ICP_BILL_RESPTYPE_BUSY:
					break;
				case ICP_BILL_RESPTYPE_INHIBIT:
					break;
				case ICP_BILL_RESPTYPE_BILLSTACKED:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXSTACKER);
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_BILL_RESET:
						ICPProt_ChangeState(icpSlv, ICP_BILL_INACTIVE, ICP_BILL_INACTIVE_TXRESET);
						break;
					case ICP_VMCCMD_BILL_SECURITY:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXSECURITY);
						break;
					case ICP_VMCCMD_BILL_STACKER:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXSTACKER);
						break;
					case ICP_VMCCMD_BILL_ENABLE:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXBILLTYPE);
						break;
					case ICP_VMCCMD_BILL_INHIBIT:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXBILLTYPE1);
						break;
					case ICP_VMCCMD_BILL_ESCROW:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXESCROW);
						break;
					case ICP_VMCCMD_BILL_DIAG:
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXEXPDIAG);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_ENABLED_TXBILLTYPE:
				nRespType = ICPProt_BILLBillTypeResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					if(ICPBILLInfo.BillTypeState.nBillEnable)
						ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXPOLL);
					else
						ICPProt_ChangeState(icpSlv, ICP_BILL_DISABLED, ICP_BILL_DISABLED_TXPOLL);
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_BILL_BILLTYPEDONE);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_ENABLED_TXBILLTYPE1:
				nRespType = ICPProt_BILLBillTypeResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXPOLL);
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_BILL_BILLTYPEDONE);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_ENABLED_TXSTACKER:
				nRespType = ICPProt_BILLStackerResp();
				switch(nRespType) {
				case ICP_BILL_RESPTYPE_STACKER:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_ENABLED_TXESCROW:
				nRespType = ICPProt_BILLEscrowResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_WAITENDESCROW);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_ENABLED_WAITENDESCROW:
				nRespType = ICPProt_BILLPollResp();
				switch(nRespType) {
				case ICP_RESPTYPE_JUSTRESET:
					ICPProt_ChangeState(icpSlv, ICP_BILL_DISABLED, ICP_BILL_DISABLED_TXSTATUS);
					break;
				case ICP_BILL_RESPTYPE_INVALIDESCROW:
					break;
				case ICP_BILL_RESPTYPE_BUSY:
					break;
				case ICP_BILL_RESPTYPE_INHIBIT:
					break;
				case ICP_BILL_RESPTYPE_BILLSTACKED:
				case ICP_BILL_RESPTYPE_BILLRETURNED:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_BILL_ESCROWDONE);
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXSTACKER);
					break;
				case ICP_RESPTYPE_RW_ACK:
					switch(ICPProt_VMCGetCmd(icpSlvAddr)) {
					case ICP_VMCCMD_NONE:
						break;
					case ICP_VMCCMD_BILL_RESET:
						ICPProt_ChangeState(icpSlv, ICP_BILL_INACTIVE, ICP_BILL_INACTIVE_TXRESET);
						break;
					default:
						ICPProt_VMCCmdRejected(icpSlvAddr);
						break;
					}
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_ENABLED_TXSECURITY:
				nRespType = ICPProt_BILLSecurityResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			case ICP_BILL_ENABLED_TXEXPDIAG:
				nRespType = ICPProt_BILLExpFFResp();
				switch(nRespType) {
				case ICP_RESPTYPE_RW_ACK:
				case ICP_BILL_RESPTYPE_EXPFF:
					ICPProt_VMCCmdDone(icpSlvAddr, ICP_VMCCMDSTATE_BILL_DIAGDONE);
					ICPProt_ChangeState(icpSlv, -1, ICP_BILL_ENABLED_TXPOLL);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		return;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	BILLSlaveResp();
#endif
}


//===============================================
//		RESET Command (30H)
//===============================================
void ICPProt_BILLReset(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_RESET *ptCmd = (T_BILLCMD_RESET *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	uint8_t nCmd, nCmdState;

	if((nCmd = ICPProt_VMCGetCmd(icpSlvAddr)) == ICP_VMCCMD_BILL_RESET) {
		ICPProt_VMCGetPar(icpSlvAddr);
		nCmdState = ICPBILLInfo.nCmdState;
	}
	BILLInit(icpSlvAddr);    //ABELE gi� fatto in ICPprot_Init
	if(nCmd == ICP_VMCCMD_BILL_RESET) {
		ICPBILLInfo.nCmdType = nCmd;
		ICPBILLInfo.nCmdState = nCmdState;
	}
	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_RESET;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLResetResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_RESET *ptResp = (T_BILLRESP_RESET *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
		}
		ICPDrvSetComState(COM_IDLE);
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	BILLInit(icpSlvAddr);
	//ICPProt_BILLResetPollData();
	//if(ICPBILLHook_IsEnabled()) {
		ICPDrvSendRW(ICP_RW_ACK);
	//}
	ICPBILLHook_Init();
	ICPProt_SetInhibit(icpSlvAddr, true);
#endif
	return nRespType;
}

//===============================================
//		STATUS Command (31H)
//===============================================
void ICPProt_BILLStatus(void)
{
#if (_IcpMaster_ == true)
	T_BILLCMD_STATUS *ptCmd = (T_BILLCMD_STATUS *)&ICPInfo.buff[0];
	uint8_t nFrameLen;

	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_STATUS;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLStatusResp(void)
{
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_STATUS *ptResp = (T_BILLRESP_STATUS *)&ICPInfo.buff[0];
		int8 nRespLen, n;

		if((!ICPInfo.fSlaveRW) && (ICPInfo.iRX > 1)) {

			ICPDrvSendRW(ICP_RW_ACK);
			nRespLen = sizeof(ptResp->Status);
			for(n = ICPInfo.iRX - 1; n < nRespLen; n++) {
				ICPInfo.buff[n] = 0;
			}
			ICPBILLInfo.Status = ptResp->Status;
			ICPBILLInfo.Status.nCCode = swap(ICPBILLInfo.Status.nCCode);

			ICPBILLInfo.Status.nCCode						= __REV16(ICPBILLInfo.Status.nCCode);
			ICPBILLInfo.Status.nUSF							= __REV16(ICPBILLInfo.Status.nUSF);
			ICPBILLInfo.Status.StackerState.nBillsInStacker	= __REV16(ICPBILLInfo.Status.StackerState.nBillsInStacker);
			ICPBILLInfo.Status.SecurityLevel.nSecurityLevel	= __REV16(ICPBILLInfo.Status.SecurityLevel.nSecurityLevel);
			ICPProt_SetLevel(icpSlvAddr, ICPBILLInfo.Status.nFLevel);
			ICPBILLHook_SetDPP(ptResp->Status.nDPP);
			nRespType = ICP_BILL_RESPTYPE_STATUS;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	uint8_t nBillType;
	T_BILLRESP_STATUS *ptResp;

	ptResp = (T_BILLRESP_STATUS *)&ICPInfo.buff[0];
	ptResp->Status.nFLevel = ICPBILLHook_GetFeatureLevel();
	ptResp->Status.nCCode = ICPBILLHook_GetCountryCode();
	Cvt16ToBigEndian(&ptResp->Status.nCCode)
	ptResp->Status.nUSF = ICPBILLHook_GetScalingFactor();
	Cvt16ToBigEndian(&ptResp->Status.nUSF)
	ptResp->Status.nDPP = ICPBILLHook_GetDecimalPoint();
	ptResp->Status.StackerState.nBillsInStacker = ICPBILLHook_GetStackerSize();
	Cvt16ToBigEndian(&ptResp->Status.StackerState.nBillsInStacker)
	ptResp->Status.SecurityLevel.nSecurityLevel = ICPBILLHook_GetSecurityLevel();
	Cvt16ToBigEndian(&ptResp->Status.SecurityLevel.nSecurityLevel)
	ptResp->Status.fEscrow = ICPBILLHook_GetEscrowCapacity() ? 0xFF : 0;
	for(nBillType = 0; nBillType < ICP_MAX_BILLTYPES; nBillType++) {
		ptResp->Status.aBillTypeVal[nBillType] = ICPBILLHook_GetBillTypeVal(nBillType) / ptResp->Status.nUSF;
	}
	ICPBILLInfo.Status = ptResp->Status;
	ICPInfo.nFrameLen = sizeof(ptResp->Status);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	ICPProt_BILLClearPollData();													// svuota eventuali "JustReset" in coda causato da comandi di Reset multipli
	}
#endif
	return nRespType;
}


//===============================================
//		SECURITY Command (32H)
//===============================================
void ICPProt_BILLSecurity(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_SECURITY *ptCmd = (T_BILLCMD_SECURITY *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar;
	uint8_t nFrameLen;

	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_SECURITY;
	ptCmdPar = NULL;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_BILL_SECURITY) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	if(ptCmdPar) {
		ptCmd->SecurityLevel.nSecurityLevel = *((uint16_t *)ptCmdPar);
		//MR20 Cvt16ToBigEndian(&ptCmd->SecurityLevel.nSecurityLevel);
		ptCmd->SecurityLevel.nSecurityLevel = __REV16(ptCmd->SecurityLevel.nSecurityLevel);
	} else ptCmd->SecurityLevel.nSecurityLevel = 0;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLSecurityResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_SECURITY *ptResp = (T_BILLRESP_SECURITY *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	T_BILLCMD_SECURITY *ptCmd;
	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_BILLCMD_SECURITY *)&ICPInfo.buff[0];
	Cvt16ToBigEndian(&ptCmd->SecurityLevel.nSecurityLevel)
	ICPBILLInfo.Status.SecurityLevel = ptCmd->SecurityLevel;
	ICPBILLHook_SetSecurityLevel(ptCmd->SecurityLevel.nSecurityLevel);
	}
#endif
	return nRespType;
}


//===============================================
//		POLL Command (33H)
//===============================================
void ICPProt_BILLPoll(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_POLL *ptCmd = (T_BILLCMD_POLL *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_POLL;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLPollResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_POLL *ptResp = (T_BILLRESP_POLL *)&ICPInfo.buff[0];
		uint8_t nBillType, nDestType, i, n;
		
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPDrvSendRW(ICP_RW_ACK);
			n = ICPInfo.iRX - 1;
			for(i = 0; i < n; i++) {
				nRespType = ptResp->aPollData[i];
				switch(nRespType & 0x80) {
				case 0x80:
					nDestType = (nRespType >> 4) & 0x07;
					switch(nDestType) {
					case ICP_BILL_ESCROWPOS:
					case ICP_BILL_STACKED:
						nBillType = nRespType & 0x0F;
						ICPBILLHook_SetBillsDeposited((ET_BILLPOLLDEST)nDestType, nBillType, (CreditCoinValue)(ICPBILLInfo.Status.aBillTypeVal[nBillType] * ICPBILLInfo.Status.nUSF));
						nRespType = ICP_BILL_RESPTYPE_BILLSTACKED;
						break;
					case ICP_BILL_RETURNED:
						nRespType = ICP_BILL_RESPTYPE_BILLRETURNED;
						break;
					default:
						nRespType = ICP_RESPTYPE_NONE;
						break;
					}
					i = n;
					break;
				default:
					switch(nRespType) {
					case ICP_BILL_INVALIDESCROWREQ:
						nRespType = ICP_BILL_RESPTYPE_INVALIDESCROW;
						break;
					case ICP_BILL_BUSY:
						nRespType = ICP_BILL_RESPTYPE_BUSY;
						break;
					case ICP_BILL_ROMCHKERROR:
					case ICP_BILL_VALIDATORJAM:
						nRespType = ICP_BILL_RESPTYPE_INHIBIT;
						break;
					case ICP_BILL_UNITDISABLED:
						//nRespType = ICP_BILL_RESPTYPE_DISABLED;
						nRespType = ICP_RESPTYPE_RW_ACK;
						break;
					case ICP_BILL_JUSTRESET:
						nRespType = ICP_RESPTYPE_JUSTRESET;
						i = n;
						break;
					default:
						break;
					}
					break;
				}
			}
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)

#if kReq456Support
//***********************
//	MSTNOACK_SLVNORETRY (COINCO_2P)
//***********************
	//
	// Check if Retry have to do...
	//
	if(ICPGetFrameSent) {
		if(ICPGetFrameAcked == false) {
			//
			// Retry to send last frame if ACK not received from Master
			//
			ICPInfo.nFrameLen = ICPLoadSlaveFrameSent();
			ICPDrvSendMsg(ICPInfo.nFrameLen);
			return;
		} else {
			//
			// Last frame sent is ok, reset last event && change to new state
			//
			if(ICPGetNewState != -1)
				ICPProt_ChangeState(icpSlv, ICPGetNewState, ICPGetNewSubState);
		}
	}
	ICPSetFrameSent(false);
	ICPSetFrameAcked(false);
	ICPSetNewState(-1);
	ICPSetNewSubState(0);
//***********************
#endif

	ICPInfo.nFrameLen = ICPProt_BILLGetPollData(&ICPInfo.buff[0], TRUE);
	if(ICPInfo.nFrameLen) {
		ICPDrvSendMsg(ICPInfo.nFrameLen);
#if kReq456Support
		ICPSaveSlaveFrameSent(ICPInfo.nFrameLen);
		ICPSetFrameSent(true);
#endif
	}
	else ICPDrvSendRW(ICP_RW_ACK);
#endif
	return nRespType;
}


//===============================================
//		BILLTYPE Command (34H)
//===============================================
void ICPProt_BILLBillType(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_BILLTYPE *ptCmd = (T_BILLCMD_BILLTYPE *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar, nCmdType;
	uint8_t nFrameLen;
	uint16_t nBillEnable;

	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_BILLTYPE;
	ptCmdPar = NULL;
	nCmdType = ICPProt_VMCGetCmd(icpSlvAddr);
	if((nCmdType == ICP_VMCCMD_BILL_ENABLE) || (nCmdType == ICP_VMCCMD_BILL_INHIBIT)){
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	nBillEnable = ICPBILLInfo.BillTypeState.nBillEnable;
	if(ptCmdPar) {
		if(nBillEnable) {
			CreditValue nCashLimitAmount = *((CreditValue *)ptCmdPar);
			if(!nCashLimitAmount) nBillEnable = 0;
			else {
				int16 nBillType;					//16/11/00 OSz int8 --> int16 (-3)
				uint16_t nMask;
				uint16_t nBillInhibit = 0;

				nCashLimitAmount = nCashLimitAmount / ICPBILLInfo.Status.nUSF;
				for(nBillType = ICP_MAX_COINTYPES - 1; nBillType >= 0; nBillType--) {
					if(!ICPBILLInfo.Status.aBillTypeVal[nBillType]) continue;
					if(ICPBILLInfo.Status.aBillTypeVal[nBillType] > nCashLimitAmount) {
						nMask = 1 << nBillType;
						nBillInhibit |= nMask;
					} else break;
				}
				if(nBillInhibit) nBillEnable &= ~nBillInhibit;
			}
		}
	}
	ptCmd->BillTypeState.nBillEnable = nBillEnable;
	ptCmd->BillTypeState.nEscrowEnable = 0;
	//MR20 Cvt16ToBigEndian(&ptCmd->BillTypeState.nBillEnable);
	//MR20 Cvt16ToBigEndian(&ptCmd->BillTypeState.nEscrowEnable);
	ptCmd->BillTypeState.nBillEnable	= __REV16(ptCmd->BillTypeState.nBillEnable);
	ptCmd->BillTypeState.nEscrowEnable	= __REV16(ptCmd->BillTypeState.nEscrowEnable);
	ICPBILLInfo.BillTypeState.nEscrowEnable = ptCmd->BillTypeState.nBillEnable;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLBillTypeResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_BILLTYPE *ptResp = (T_BILLRESP_BILLTYPE *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	T_BILLCMD_BILLTYPE *ptCmd;
	bool fInh;
	//if(ICPBILLHook_IsEnabled()) {
		ICPDrvSendRW(ICP_RW_ACK);
	//}
	ptCmd = (T_BILLCMD_BILLTYPE *)&ICPInfo.buff[0];
	Cvt16ToBigEndian(&ptCmd->BillTypeState.nBillEnable)
	Cvt16ToBigEndian(&ptCmd->BillTypeState.nEscrowEnable)
	ICPBILLInfo.BillTypeState = ptCmd->BillTypeState;
	if(ICPBILLInfo.BillTypeState.nEscrowEnable && !ICPBILLInfo.Status.fEscrow)
		// Escrow can't be enabled because escrow feature is not available!!!
		ICPBILLInfo.BillTypeState.nEscrowEnable = 0;
	ICPProt_BILLSetBillTypeState();
	fInh = ptCmd->BillTypeState.nBillEnable ? false : true;
	ICPProt_SetInhibit(icpSlvAddr, fInh);
	}
#endif

	return nRespType;
}


//===============================================
//		ESCROW Command (35H)
//===============================================
void ICPProt_BILLEscrow(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_ESCROW *ptCmd = (T_BILLCMD_ESCROW *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar;
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_ESCROW;
	ptCmdPar = NULL;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_BILL_ESCROW) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	if(ptCmdPar) {
		ptCmd->fStackBill = *((uint8_t *)ptCmdPar);
	} else ptCmd->fStackBill = 0;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLEscrowResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_ESCROW *ptResp = (T_BILLRESP_ESCROW *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	T_BILLCMD_ESCROW *ptCmd;
	bool fStackBill;
	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_BILLCMD_ESCROW *)&ICPInfo.buff[0];
	if(!ICPBILLInfo.Status.fEscrow) {
		// Invalid escrow command because escrow feature is not available!!!
		ICPProt_BILLSetStatus(ICP_BILL_INVALIDESCROWREQ);
	} else {
		fStackBill = (ptCmd->fStackBill & 0x01) ? true : false;
		ICPBILLHook_EscrowBill(fStackBill);
	}
	}
#endif
	return nRespType;
}


//===============================================
//		STACKER Command (36H)
//===============================================
void ICPProt_BILLStacker(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_STACKER *ptCmd = (T_BILLCMD_STACKER *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_BILL_STACKER) {
		ICPProt_VMCGetPar(icpSlvAddr);
	}
	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_STACKER;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

//--------------------------------------------------------
uint8_t ICPProt_BILLStackerResp(void)
{
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode)
	{
		T_BILLRESP_STACKER *ptResp = (T_BILLRESP_STACKER *)&ICPInfo.buff[0];
		if((!ICPInfo.fSlaveRW) && (ICPInfo.iRX > 1))
		{
			ICPDrvSendRW(ICP_RW_ACK);
			//MR20 Cvt16ToBigEndian(&ptResp->StackerState)
			ptResp->StackerState.nBillsInStacker = __REV16(ptResp->StackerState.nBillsInStacker);	// Modificato pointer altrimenti "Error[Pe167]: argument of type "T_BILL_STACKER" is incompatible with parameter of type "unsigned int"
			ICPBILLInfo.StackerState = ptResp->StackerState;
			if(ptResp->StackerState.nBillsInStacker & 0x8000)
			{
				ICPBILLInfo.nInfoMask |= ICP_BILLINFO_STACKERFULL;
			}
			else
			{
				ICPBILLInfo.nInfoMask &= ~ICP_BILLINFO_STACKERFULL;
			}
			nRespType = ICP_BILL_RESPTYPE_STACKER;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	T_BILLRESP_STACKER *ptResp;
	ptResp = (T_BILLRESP_STACKER *)&ICPInfo.buff[0];
	ptResp->StackerState.nBillsInStacker = ICPBILLHook_GetStackerSize();
	if(ICPBILLHook_IsStackerFull()) ptResp->StackerState.nBillsInStacker |= 0x8000;
	else ptResp->StackerState.nBillsInStacker &= 0x7FFF;
	Cvt16ToBigEndian(&ptResp->StackerState.nBillsInStacker)
	ICPInfo.nFrameLen = sizeof(ptResp->StackerState);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif
	return nRespType;
}


//===============================================
//		EXP. ID. Command (37H 00H)
//===============================================
void ICPProt_BILLExp00(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_EXP00 *ptCmd = (T_BILLCMD_EXP00 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_EXP;
	ptCmd->nSubCmd = ICP_BILL_CMD_EXPREQUESTID1;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLExp00Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_EXP00 *ptResp = (T_BILLRESP_EXP00 *)&ICPInfo.buff[0];
		if((!ICPInfo.fSlaveRW) && (ICPInfo.iRX > 1)) {
			ICPDrvSendRW(ICP_RW_ACK);
			ICPBILLHook_SetManufactCode(&ptResp->PeriphID1.aManufactCode[0], sizeof(ptResp->PeriphID1.aManufactCode));
			ICPBILLHook_SetSerialNumber(&ptResp->PeriphID1.aSerialNumber[0], sizeof(ptResp->PeriphID1.aSerialNumber));
			ICPBILLHook_SetModelNumber(&ptResp->PeriphID1.aModelNumber[0], sizeof(ptResp->PeriphID1.aModelNumber));
			//MR20 Cvt16ToBigEndian(&ptResp->PeriphID1.nSwVersion)
			ptResp->PeriphID1.nSwVersion = __REV16(ptResp->PeriphID1.nSwVersion);
			ICPBILLHook_SetSWVersion(ptResp->PeriphID1.nSwVersion);
			ICPBILLInfo.nFOptions = ICP_BILLOPT_NONE;
			nRespType = ICP_BILL_RESPTYPE_EXP00;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	uint32_t nFOptions;
	T_BILLRESP_EXP00 *ptResp;
	ptResp = (T_BILLRESP_EXP00 *)&ICPInfo.buff[0];
	ICPHook_GetManufactCode(&ptResp->PeriphID1.aManufactCode[0], sizeof(ptResp->PeriphID1.aManufactCode));
	ICPHook_GetSerialNumber(&ptResp->PeriphID1.aSerialNumber[0], sizeof(ptResp->PeriphID1.aSerialNumber));
	ICPHook_GetModelNumber(&ptResp->PeriphID1.aModelNumber[0], sizeof(ptResp->PeriphID1.aModelNumber));
	ptResp->PeriphID1.nSwVersion = ICPHook_GetSWVersion();
	Cvt16ToBigEndian(&ptResp->PeriphID1.nSwVersion)
	ICPInfo.nFrameLen = sizeof(ptResp->PeriphID1);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif
	return nRespType;
}


//===============================================
//		EXP. OPT. Command (37H 01H)
//===============================================
void ICPProt_BILLExp01(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_EXP01 *ptCmd = (T_BILLCMD_EXP01 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_EXP;
	ptCmd->nSubCmd = ICP_BILL_CMD_EXPENABLEOPT;
	ptCmd->nFOptions = ICP_BILLOPT_NONE;
	if((ICPBILLInfo.nFOptions & ICP_BILLOPT2_FTL) && ICPProt_BILLGetFTL())
		ptCmd->nFOptions |= ICP_BILLOPT2_FTL;
	//MR20 Cvt32ToBigEndian(&ptCmd->nFOptions);
	ptCmd->nFOptions = __REV(ptCmd->nFOptions);
	ICPBILLInfo.nFOptions = ptCmd->nFOptions;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLExp01Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_EXP01 *ptResp = (T_BILLRESP_EXP01 *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	T_BILLCMD_EXP01 *ptCmd;
	ICPDrvSendRW(ICP_RW_ACK);
	ptCmd = (T_BILLCMD_EXP01 *)&ICPInfo.buff[0];
	Cvt32ToBigEndian(&ptCmd->nFOptions)
	#if _IcpBillLevel_ > 1
		ICPBILLInfo.nFOptions = ICPBILLHook_GetFeatures();
	#endif
	ICPBILLInfo.nFOptions &= ptCmd->nFOptions;
	}
#endif
	return nRespType;
}


//===============================================
//		EXP. ID. + OPT. Command (37H 02H)
//===============================================
void ICPProt_BILLExp02(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_EXP02 *ptCmd = (T_BILLCMD_EXP02 *)&ICPInfo.buff[0];
	uint8_t nFrameLen;
	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_EXP;
	ptCmd->nSubCmd = ICP_BILL_CMD_EXPREQUESTID2;
	nFrameLen = sizeof(*ptCmd);
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLExp02Resp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_EXP02 *ptResp = (T_BILLRESP_EXP02 *)&ICPInfo.buff[0];
		if((!ICPInfo.fSlaveRW) && (ICPInfo.iRX > 1)) {
			ICPDrvSendRW(ICP_RW_ACK);
			ICPBILLHook_SetManufactCode(&ptResp->PeriphID2.PeriphID1.aManufactCode[0], sizeof(ptResp->PeriphID2.PeriphID1.aManufactCode));
			ICPBILLHook_SetSerialNumber(&ptResp->PeriphID2.PeriphID1.aSerialNumber[0], sizeof(ptResp->PeriphID2.PeriphID1.aSerialNumber));
			ICPBILLHook_SetModelNumber(&ptResp->PeriphID2.PeriphID1.aModelNumber[0], sizeof(ptResp->PeriphID2.PeriphID1.aModelNumber));
			//MR20 Cvt16ToBigEndian(&ptResp->PeriphID2.PeriphID1.nSwVersion)
			ptResp->PeriphID2.PeriphID1.nSwVersion = __REV16(ptResp->PeriphID2.PeriphID1.nSwVersion);
			ICPBILLHook_SetSWVersion(ptResp->PeriphID2.PeriphID1.nSwVersion);
			//MR20 Cvt32ToBigEndian(&ptResp->PeriphID2.nFOptions);
			ptResp->PeriphID2.nFOptions = __REV(ptResp->PeriphID2.nFOptions);
			ICPBILLInfo.nFOptions = ptResp->PeriphID2.nFOptions;
			nRespType = ICP_BILL_RESPTYPE_EXP02;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	{
	uint32_t nFOptions;
	T_BILLRESP_EXP02 *ptResp;
	ptResp = (T_BILLRESP_EXP02 *)&ICPInfo.buff[0];
	ICPHook_GetManufactCode(&ptResp->PeriphID2.PeriphID1.aManufactCode[0], sizeof(ptResp->PeriphID2.PeriphID1.aManufactCode));
	ICPHook_GetSerialNumber(&ptResp->PeriphID2.PeriphID1.aSerialNumber[0], sizeof(ptResp->PeriphID2.PeriphID1.aSerialNumber));
	ICPHook_GetModelNumber(&ptResp->PeriphID2.PeriphID1.aModelNumber[0], sizeof(ptResp->PeriphID2.PeriphID1.aModelNumber));
	ptResp->PeriphID2.PeriphID1.nSwVersion = ICPHook_GetSWVersion();
	Cvt16ToBigEndian(&ptResp->PeriphID2.PeriphID1.nSwVersion)
	nFOptions = ICP_BILLOPT_NONE;
	#if _IcpBillLevel_ > 1
		nFOptions = ICPBILLHook_GetFeatures();
	#endif
	ptResp->PeriphID2.nFOptions = nFOptions;
	Cvt32ToBigEndian(&ptResp->PeriphID2.nFOptions)
	ICPBILLInfo.nFOptions = ptResp->PeriphID2.nFOptions;
	ICPInfo.nFrameLen = sizeof(ptResp->PeriphID2);
	ICPDrvSendMsg(ICPInfo.nFrameLen);
	}
#endif
	return nRespType;
}


//===============================================
//		EXP. DIAG. Command (37H FFH)
//===============================================
void ICPProt_BILLExpFF(void) {
#if (_IcpMaster_ == true)
	T_BILLCMD_EXPFF *ptCmd = (T_BILLCMD_EXPFF *)&ICPInfo.buff[0];
	uint8_t *ptCmdPar;
	uint8_t nFrameLen;
	uint8_t **pt, *ptData, nDataLen, i;

	ptCmd->nCmd = icpSlvAddr + ICP_BILL_CMD_EXP;
	ptCmd->nSubCmd = ICP_BILL_CMD_EXPDIAG;
	nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd);
	ptCmdPar = NULL;
	if(ICPProt_VMCGetCmd(icpSlvAddr) == ICP_VMCCMD_BILL_DIAG) {
		ptCmdPar = ICPProt_VMCGetPar(icpSlvAddr);
	}
	if(ptCmdPar) {
		nDataLen = *((uint8_t *)ptCmdPar);
		if(nDataLen > (ICP_MAX_FRAMELEN - 2))
			nDataLen = ICP_MAX_FRAMELEN - 2;
		pt = (uint8_t **)(ptCmdPar + sizeof(uint8_t));
		ptData = (uint8_t *)(*pt);
		for(i = 0; i < nDataLen; i++) {
			ptCmd->aDiagData[i] = ptData[i];
		}
		nFrameLen = sizeof(ptCmd->nCmd) + sizeof(ptCmd->nSubCmd) + nDataLen;
	}
	ICPDrvSendMsg(nFrameLen);
#endif
}

uint8_t ICPProt_BILLExpFFResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;

#if (_IcpMaster_ == true)
	if(icpMasterMode) {
		T_BILLRESP_EXPFF *ptResp = (T_BILLRESP_EXPFF *)&ICPInfo.buff[0];
		if((ICPInfo.fSlaveRW) && (ptResp->nodata == ICP_RW_ACK)) {
			nRespType = ICP_RESPTYPE_RW_ACK;
			ICPDrvSetComState(COM_IDLE);
		} else {
			ICPDrvSendRW(ICP_RW_ACK);
			ICPBILLHook_SetDiagData(ptResp->aDiagData, (uint8_t)(ICPInfo.iRX - 1));
			nRespType = ICP_BILL_RESPTYPE_EXPFF;
		}
		return nRespType;
	}
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	ICPDrvSendRW(ICP_RW_NAK);
#endif
	return nRespType;
}



#if _IcpBillFTL_ > 0
void ICPProt_BILLExpFA(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_BILLExpFAResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	//...
#endif
	return nRespType;
}

void ICPProt_BILLExpFB(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_BILLExpFBResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	//...
#endif
	return nRespType;
}

void ICPProt_BILLExpFC(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_BILLExpFCResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	//...
#endif
	return nRespType;
}

void ICPProt_BILLExpFD(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_BILLExpFDResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	//...
#endif
	return nRespType;
}

void ICPProt_BILLExpFE(void) {
#if (_IcpMaster_ == true)
	//...
#endif
}

uint8_t ICPProt_BILLExpFEResp(void) {
	uint8_t nRespType = ICP_RESPTYPE_NONE;
#if (_IcpMaster_ == true)
	//...
#endif

#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
	//...
#endif
	return nRespType;
}
#endif	// _IcpBillFTL_ > 0




//-------------------------------------------------
// Routines to give BILL info
//-------------------------------------------------
#if (_IcpMaster_ == true)
bool	ICPProt_BILLGetFTL(void) {
	return _IcpBillFTL_;
}

uint16_t	ICPProt_BILLGetBillValue(uint8_t ix) {
	return ICPBILLInfo.Status.aBillTypeVal[ix];
}

void	Set_Bill_Info_Data(uint8_t ix, uint8_t l, uint8_t *point) {
	uint8_t i;

	for (i=0;i<l;i++){
		ICPBILLInfo.BillInfo.aManufactCode[i+ix] = *point;
		point++;
	}
}

//MR19 Aggiunta Giu 2019
void Set_Bill_SwRev_Data(uint16_t Bill_Rev)
{
  ICPBILLInfo.BillInfo.nSwVersion = Bill_Rev;					//Bill Validator Sw Rervision
}

#endif


#if (_IcpSlaveCode_ & ICP_BILLVALIDATOR)
void ICPProt_BILLClearPollData(void) {
	ICPBILLPollInfo.nData = 0;
	ICPBILLPollInfo.nWrData = 0;
	ICPBILLPollInfo.nRdData = 0;
}

void ICPProt_BILLResetPollData(void) {
	ICPProt_BILLClearPollData();
	ICPProt_BILLSetStatus(ICP_BILL_JUSTRESET);
}

uint8_t ICPProt_BILLGetPollData(uint8_t *ptData, bool fAllData) {
	uint8_t nDataLen = 0;
	uint8_t nData = ICPBILLPollInfo.nData;
	uint8_t nRdData = ICPBILLPollInfo.nRdData;
	uint8_t *ptDest = ptData;
	uint8_t *ptSrc;

	if(ptData && nData) {
		ptSrc = &ICPBILLPollInfo.aPollData[0];
		if(!fAllData) nData = 1;
		ICPBILLPollInfo.nData -= nData;
		do {
			*ptDest++ = ptSrc[nRdData];
			nRdData = (nRdData + 1) & 0x0F;
			nData -= 1;
		} while(nData);
		ICPBILLPollInfo.nRdData = nRdData;
		nDataLen = ptDest - ptData;
	}
	return nDataLen;
}


void ICPProt_BILLSetStatus(ET_BILLPOLLSTATUS nStatusType) {

	if(ICPBILLPollInfo.nData < sizeof(ICPBILLPollInfo.aPollData)) {
		switch(nStatusType) {
		case ICP_BILL_DEFECTIVEMOTOR:
		case ICP_BILL_SENSORERROR:
		case ICP_BILL_BUSY:
		case ICP_BILL_ROMCHKERROR:
		case ICP_BILL_VALIDATORJAM:
		case ICP_BILL_JUSTRESET:
		case ICP_BILL_BILLREMOVED:
		case ICP_BILL_CASHBOXOPEN:
		case ICP_BILL_UNITDISABLED:
		case ICP_BILL_INVALIDESCROWREQ:
		case ICP_BILL_BILLREJECTED:
		case ICP_BILL_NINPUTBILL:
			break;
		default:
			// invalid status type!
			return;
		}
		ICPBILLPollInfo.aPollData[ICPBILLPollInfo.nWrData] = nStatusType;
		ICPBILLPollInfo.nWrData = (ICPBILLPollInfo.nWrData + 1) & 0x0F;
		ICPBILLPollInfo.nData += 1;
	}
}

void ICPProt_BILLSetBillsDeposited(uint8_t nBillType, ET_BILLPOLLDEST DestType) {
	uint8_t nBillsDeposited = 0x80;
	uint8_t nDestType;

	if(ICPBILLPollInfo.nData < sizeof(ICPBILLPollInfo.aPollData)) {
		nDestType = DestType << 4;
		nBillsDeposited |= nDestType;
		nBillType &= 0x0F;
		nBillsDeposited |= nBillType;
		ICPBILLPollInfo.aPollData[ICPBILLPollInfo.nWrData] = nBillsDeposited;
		ICPBILLPollInfo.nWrData = (ICPBILLPollInfo.nWrData + 1) & 0x0F;
		ICPBILLPollInfo.nData += 1;
	}
}

void ICPProt_BILLSetBillTypeState(void) {
	uint8_t nBillType;
	uint16_t nMask;

	for(nBillType = 0; nBillType < ICP_MAX_BILLTYPES; nBillType++) {
		nMask = 1 << nBillType;
		if(ICPBILLInfo.BillTypeState.nBillEnable & nMask) {
			ICPBILLHook_SetBillEnable(nBillType, TRUE);
		} else ICPBILLHook_SetBillEnable(nBillType, FALSE);
		if(ICPBILLInfo.BillTypeState.nEscrowEnable & nMask) {
			ICPBILLHook_SetEscrowEnable(nBillType, TRUE);
		} else ICPBILLHook_SetEscrowEnable(nBillType, FALSE);
	}
}

bool ICPProt_BILLIsFTLEnable(void) {
	return (ICPBILLInfo.nFOptions & ICP_BILLOPT2_FTL) ? TRUE : FALSE;
}

#endif

/*
//-------------------------------------------------
// Routines to give CHG info
//-------------------------------------------------

void	Set_Bill_Info_Data(uint8_t ix, uint8_t l, uint8_t *point) {
	uint8_t i;

	for (i=0;i<l;i++){
		ICPBILLInfo.BillInfo.aManufactCode[i+ix] = *point;
		point++;
	}
}
*/

#endif //(_IcpSlave_ & ICP_BILLVALIDATOR)
