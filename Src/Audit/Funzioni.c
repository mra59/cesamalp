/*--------------------------------------------------------------------------------------*\
File:
    Funzioni.c

Description:
    File con funzioni generali.
    
History:
    Date       Aut  Note
    Set 2012 	MR   
\*--------------------------------------------------------------------------------------*/

#include <string.h>

#include "ORION.H"
#include "Funzioni.h"
#include "Totalizzazioni.h"
#include "AuditDataDescr.h"
#include "Audit.h"
#include "IICEEP.h"
#include "AuditExt.h"
#include "I2C_EEPROM.h"
#include "Power.h"
#include "VMC_CPU_Prog.h"

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern	LDD_RTC_TTime DataOra;

extern	const char	Lang0_ProgMsgLCD[10][40];
extern	const char	Lang1_ProgMsgLCD[10][40];
extern	bool		ConnessioneDDCMP_IrDA(void);
extern	uint32_t	ReadLogFileContentLen(void);
extern	void		LeggiOrologio(void);
extern	void  		Luci(bool stato);
extern	void  		Rd_EEBuffStartAddr_And_BankSelect(uint8_t);


/*--------------------------------------------------------------------------------------*\
Global variables
\*--------------------------------------------------------------------------------------*/
const char	gSWRevCode[]= kProductSWVersionStr;
uint16_t	MinPrice, MaxPrice;
uint8_t		MemoryBytes[4];


//MR19 static tFasce	FasceOFFCaldaia[MaxFasceOFFCaldaia];
static tFasce	FasceONLuci[MaxFasceONLuci];


/*--------------------------------------------------------------------------------------*\
Method: SystemStBy
  Ritorna vero se nel sistema non c'e' credito e non ci sono chiavi Utente/Servizio
  inserite.

Parameters:
	OUT: - True se sistema in St-By
\*--------------------------------------------------------------------------------------*/
bool SystemStBy(void) {

	if (!gOrionKeyVars.Inserted && !gOrionKeyVars.RFFieldFree && TotaleCreditoAttuale() == 0) {
		return true;
	} else {
		return false;
	}
}
 
/*--------------------------------------------------------------------------------------*\
Method: IntListGetMin
	Determina il valore piu' basso di un array di prezzi. 

Parameters:
	IN : - pointer a un array di prezzi
	OUT: - valore piu' basso dell'array prezzi
\*--------------------------------------------------------------------------------------*/
uint16_t IntListGetMin(uint16_t *ptr, uint16_t num) {
	
	uint16_t i, min;

	min = MAX_SYSTEM_CREDIT;
	for (i=0; i<num; i++,ptr++ )
	{
		if ((*ptr > 0) && (*ptr < min)) {
			min = *ptr;
		}
	}
	return (min);
}

/*--------------------------------------------------------------------------------------*\
Method: IntListGetMax
	Determina il valore piu' alto di un array di prezzi. 

Parameters:
	IN : - pointer a un array di prezzi
	OUT: - valore piu' alto (e diverso da prezzo non disponibile) dell'array prezzi
\*--------------------------------------------------------------------------------------*/
uint16_t IntListGetMax(uint16_t *ptr, uint16_t num) {
	
	uint8_t i;
	uint16_t max;

	max = 0;
	for (i=0; i<num; i++,ptr++ ) {
		if (*ptr > max && *ptr != PrezzoNonDisponibile) {
			max = *ptr;
		}
	}
	return (max);
}

/*------------------------------------------------------------------------------------------*\
 Method: SetMinMaxPrice
   Calcola MinPrice e MaxPrice

   IN:  - MRPPrices.Prices99.MRPPricesList99
  OUT:  - MinPrice e MaxPrice
\*------------------------------------------------------------------------------------------*/
void  SetMinMaxPrice()
{
    MinPrice = IntListGetMin(MRPPrices.Prices99.MRPPricesList99, MRPPricesListLenght99);
    MaxPrice = IntListGetMax(MRPPrices.Prices99.MRPPricesList99, MRPPricesListLenght99);
    if( MinPrice > MaxPrice ) {
    	MinPrice = 0;
    }
}

/*--------------------------------------------------------------------------------------------*\
Method: ResetDaWDOG_Events
	Memorizza l'evento di Reset da WDOG Timer. 

Parameters:
	IN: - 
\*--------------------------------------------------------------------------------------------*/
void  ResetDaWDOG_Events(void)
{
	PSHProcessingData();
	CntEventi_WD_Reset = 0;
	SetDataTime(&OffSavedData, &OffSavedTime);													// Leggo data/ora attuali
	InsertEvent (kWDogResetEvent, OffSavedData, OffSavedTime, CheckPWD);						// "EK_05  ": Reset da Watch-Dog
}

/*--------------------------------------------------------------------------------------*\
Method: GestioneFasceSconto
	Determina se il sistema e' in una qualsiasi fascia sconti.

   IN:  - 
  OUT:  - gOrionKeyVars.InFasciaSconti according
\*--------------------------------------------------------------------------------------*/
void  GestioneFasceSconto(void)
{
	gOrionKeyVars.InFasciaSconti = false;
	uint8_t fascestatus;
	
	fascestatus = TestFasce(FasceSconto, MaxFasceSconto);
	if (fascestatus == DentroFasce) {
		gOrionKeyVars.InFasciaSconti = true;
	} else {
		gOrionKeyVars.InFasciaSconti = false;
	}
}

/*--------------------------------------------------------------------------------------*\
Method: TestFasce
	Determina se il sistema e' nella fascia passata come parametro.

   IN:  - 
  OUT:  - DentroFasce o FuoriFasce
\*--------------------------------------------------------------------------------------*/
uint8_t TestFasce(tFasce *fasce, uint8_t maxfasce)
{
/*
static struct {
	uint32_t  data;
	uint16_t  time;
	uint8_t   cnt;
} sTestFasceVars;
*/
	uint8_t		i;
	uint16_t	Time;
	tFasce 		*pntfascia;

	pntfascia = fasce;
	for (i=0; i<maxfasce; i++,fasce++) {
		if ((fasce->Inizio != fasce->Fine) && (fasce->Inizio < fasce->Fine)) {								// Errore: fine fascia minore di inizio fascia
			break;
		}
	}
	if (i == maxfasce) 
	{
		return(NoFasce);																					// Non e' programmata alcuna fascia
	} 
	else 
	{
/*	  
		if (fasce == &FasceSconto[0] || fasce == &FasceFreeCredit[0]) sTestFasceVars.cnt = 0;
		if (sTestFasceVars.cnt == 0)
		{
			if (SetDataTime(&sTestFasceVars.data, &sTestFasceVars.time) != true)
			{
				return (NoFasce);
			}
		}
		sTestFasceVars.cnt++;
		for (i=0; i<maxfasce; i++,pntfascia++)
		{
			if ((sTestFasceVars.time >= pntfascia->Inizio ) && (sTestFasceVars.time < pntfascia->Fine))
			{
				return (DentroFasce);
			}
		}
		return (FuoriFasce);
*/
		Time = (uint16_t)((DataOra.Hour * 100) + DataOra.Minute);
		for (i=0; i<maxfasce; i++,pntfascia++)
		{
			if ( (Time >= pntfascia->Inizio ) && (Time < pntfascia->Fine) )
			{
				return (DentroFasce);
			}
		}
		return (FuoriFasce);
	}
}

/*--------------------------------------------------------------------------------------*\
Method: TestFasceGiornaliere
	Chiamata ogni minuto dal Main.
	Determina se sono gia' stata caricate in RAM dalla EE le fasce odierne.
	Verifica se ci si trova in una delle fasce impostate e determina le azioni da
	intraprendere.
	L'uscita Luci e' attiva dentro la fascia.
	Le fasce sono uguali per tutti i giorni della settimana.
	Il clock del uP numera i giorni da 0 (Dom) a 6 (Sab) mentre nella programmazione
	delle fasce numero i giorni da 1 (Lun) a 7 (Dom).
	Sono quindi tutti uguali tranne la domenica.

   IN:  - 
  OUT:  - 
\*--------------------------------------------------------------------------------------*/
void  TestFasceGiornaliere(void) {
  
	uint8_t		j, DayOfTheWeek;
	uint16_t	AddrParam;
	uint8_t		LocalBuff[16];
	
	if (gVMCState.InSelezione == true) return;
		
	if (DataOra.DayOfWeek == 0) {
		DayOfTheWeek = 6;																	// Domenica
	} else {
		DayOfTheWeek = (uint8_t)(DataOra.DayOfWeek - 1);									// Da LUN (0) a SAB (5)
	}
/*MR19	
	// Address fascia ON-OFF caldaia
	j = InizioFasce + ((Fasce_ON_OFF * FasciaSettimana_Len) + (DayOfTheWeek * (sizeof(SizeEE_FA_Array) * 4)));		// Pointer alla prima fascia ON-OFF del giorno da elaborare
	AddrParam = EEFasceOrarieAddr(EE_ParamFasc[j]);
	ReadEEPI2C(AddrParam, (uint8_t *)(&LocalBuff), 16);
	j = 0;
	FasceOFFCaldaia[0].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceOFFCaldaia[0].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceOFFCaldaia[1].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceOFFCaldaia[1].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceOFFCaldaia[2].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceOFFCaldaia[2].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceOFFCaldaia[3].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceOFFCaldaia[3].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
*/		
	// Address fascia LUCI
	j = InizioFasce + ((Fasce_Luci * FasciaSettimana_Len) + (DayOfTheWeek * (sizeof(SizeEE_FA_Array) * 4)));		// Pointer alla prima fascia LUCI del giorno da elaborare
	AddrParam = EEFasceOrarieAddr(EE_ParamFasc[j]);
	ReadEEPI2C(AddrParam, (uint8_t *)(&LocalBuff), 16);
	j = 0;
	FasceONLuci[0].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceONLuci[0].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceONLuci[1].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceONLuci[1].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceONLuci[2].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceONLuci[2].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceONLuci[3].Inizio = (LocalBuff[j] | (LocalBuff[j+1] << 8));
	j += 2;
	FasceONLuci[3].Fine   = (LocalBuff[j] | (LocalBuff[j+1] << 8));
		
	if (gVMCState.EnaFasceLuci == true) {														// Fasce orarie Luci abilitate
		if (TestFasce(FasceONLuci, MaxFasceONLuci) == DentroFasce) {
			Luci(ON);
		} else {
			Luci(OFF);
		}
	}
/*MR19
	if (gVMCState.EnaOFFCaldaia == true) {														// OFF caldaia da fasce orarie abilitato
		if (TestFasce(FasceOFFCaldaia, MaxFasceOFFCaldaia) == DentroFasce) {					// Sono in fascia OFF Caldaia
			if (SetPoint != Caldaia.Temper_OFF) {
				SetPoint = Caldaia.Temper_OFF;
			}
		}
	}
*/

}


/**********************************************************/
/*  Read Messaggio - low level                            */
/**********************************************************/
void ReadMessage(char *pnt, uint8_t lang, uint8_t mess, uint8_t msgLen) {
	
	uint16_t	j;
	uint32_t	msgOffs;
	//int (*pntsourcemsg)[40] = 0;
	const char* pntsourcemsg = 0;
	char *p;
	
	mess--;
	msgOffs = mess * MessLength;
	pntsourcemsg = &Lang0_ProgMsgLCD[0][0];
	if (lang == 1) {
		pntsourcemsg = &Lang1_ProgMsgLCD[0][0];
	}
	p = (char*)pntsourcemsg;
	p += msgOffs;
	
#if MsgInFlash
	for (j=0; j < msgLen; j++) {
		*(pnt+j) = p[j];
	}
#else
    IICEEPLangMess(msgOffs, lang, mess);
    IICEEPR((uint8_t *)pnt, (uint32_t)msgOffs, (uint32_t)msgLen);
#endif
}


/**********************************************************/
/*  Read Messaggio                                        */
/**********************************************************/
void ReadMess(char *pnt, uint16_t mess) {
}



/**************************************************/
/*  Audit Read Data					              */
/**************************************************/

uint32_t AuditReadData(uint32_t pnt, uint8_t size)
{
	
	  uint32_t iVal;
	  IICEEPR((uint8_t *)(&iVal), pnt, size );
	  return iVal;
}

/**************************************************/
/*  Incremento totale uint8_t Audit                 */
/**************************************************/

void AuditByteInc(uint32_t pnt, uint8_t value)
{
	  uint8_t  iVal;

	  IICEEPR((uint8_t *)(&iVal), pnt, sizeof(iVal) );
	  PSHProcessingData();
	  iVal += value;
	  IICEEPW(pnt, (uint8_t *)(&iVal), sizeof(iVal) );
}

/**************************************************/
/*  Decremento totale uint8_t Audit                 */
/**************************************************/

void AuditByteDec(uint32_t pnt, uint8_t value)
{
	  uint8_t  iVal;

	  IICEEPR((uint8_t *)(&iVal), pnt, sizeof(iVal) );
	  PSHProcessingData();
	  // testa se minuendo < sottraendo: in tale caso, minuendo = sottraendo
	  if (iVal < value)value = iVal;
	  iVal -= value;
	  IICEEPW(pnt, (uint8_t *)(&iVal), sizeof(iVal) );
}

/**************************************************/
/*  Incremento totale uint16_t Audit                  */
/**************************************************/
void  AuditUintInc(uint32_t pnt)
{
	AuditUintAdd(pnt, 1);
}

/**************************************************/
/*  Somma totale uint16_t Audit                      */
/**************************************************/
void AuditUintAdd(uint32_t pnt, uint8_t value)
{
	uint16_t  iVal;

  	IICEEPR((uint8_t *)(&iVal), pnt, sizeof(iVal) );
  	PSHProcessingData();
  	iVal += value;
  	IICEEPW(pnt, (uint8_t *)(&iVal), sizeof(iVal) );
}

/**************************************************/
/*  Incremento totale ulong Audit                 */
/**************************************************/
void  AuditUlongInc(uint32_t pnt) {
	
	uint32_t lVal;

	IICEEPR((uint8_t *)(&lVal), pnt, sizeof(lVal) );
	PSHProcessingData();
	lVal += 1;
	IICEEPW(pnt, (uint8_t *)(&lVal), sizeof(lVal) );
}

/**************************************************/
/*  Somma valore uint16_t a totale ulong Audit        */
/**************************************************/
void  AuditUlongAdd(uint32_t pnt, uint16_t valore) {
	
	uint32_t lVal;

	IICEEPR((uint8_t *)(&lVal), pnt, sizeof(lVal) );
	lVal += valore;
	if( lVal > 99999999 )
		lVal -= 100000000;
	PSHProcessingData();
	IICEEPW(pnt, (uint8_t *)(&lVal), sizeof(lVal) );
}

/**************************************************/
/*  Somma valore uint32_t a totale ulong Audit      */
/**************************************************/
void  AuditVeryUlongAdd(uint32_t pnt, uint32_t valore) {
	
	uint32_t lVal;

	IICEEPR((uint8_t *)(&lVal), pnt, sizeof(lVal) );
	lVal += valore;
	if( lVal > 99999999 )
		lVal -= 100000000;
	PSHProcessingData();
	IICEEPW(pnt, (uint8_t *)(&lVal), sizeof(lVal) );
}

/**************************************************/
/*  Sottrai valore uint16_t a totale ulong Audit      */
/* Needed only if changegiver with BDV protocol   */
/**************************************************/
void  AuditUlongSub(IICEEPOffsetType pnt, uint16_t valore) {

	uint32_t lVal;

	IICEEPR((uint8_t *)(&lVal), pnt, sizeof(lVal) );
	lVal-= valore;
	if(lVal>99999999)
		lVal +=100000000;
	PSHProcessingData();
	IICEEPW(pnt, (uint8_t *)(&lVal), sizeof(lVal) );
}

/*--------------------------------------------------------------------------------------*\
Method: SetDatePrice
	Data/ora ultima modifica ai prezzi
	
\*--------------------------------------------------------------------------------------*/
void SetDatePrice(void)
{
	SetDataTime(&SaveData, &SaveTime);
	IICEEPW(IICEEPAuditAddr(DatePrice), (uint8_t *)(&SaveData), sizeof(SaveData) );
	IICEEPW(IICEEPAuditAddr(TimePrice), (uint8_t *)(&SaveTime), sizeof(SaveTime) );
}

/*--------------------------------------------------------------------------------------*\
Method: SetDateInit
	Data/ora ultima inizializzazione memoria e/o azzeramento Audit IN
	
\*--------------------------------------------------------------------------------------*/
void SetDateInit(void){
	SetDataTime(&SaveData, &SaveTime);
	IICEEPW(IICEEPAuditAddr(DateInit), (uint8_t *)(&SaveData), sizeof(SaveData) );
	IICEEPW(IICEEPAuditAddr(TimeInit), (uint8_t *)(&SaveTime), sizeof(SaveTime) );
}
/*--------------------------------------------------------------------------------------*\
Method: SetDataTime
  Legge l'orologio interno alla MPU e memorizza Data e Ora compattati agli indirizzi 
  passati come parametri.

Parameters:
	OUT: - Data (uint32_t ddmmyy) e Ora (uint16_t hhmm) alle locazioni passate come parametri.
\*--------------------------------------------------------------------------------------*/
bool SetDataTime(uint32_t *data, uint16_t *time) {
	
	LeggiOrologio();
	*data = DataOra.Year%100;
	*data *=  10000;
	*data += ((DataOra.Month * 100)  + DataOra.Day);
	*time = ((DataOra.Hour * 100) + DataOra.Minute);
	return true;
}

/*--------------------------------------------------------------------------------------*\
Method: AzzeraTuttaEEPROM
	Azzera tutte le EEPROMS

	IN:		- 
	OUT:	- 
\*--------------------------------------------------------------------------------------*/
void   AzzeraTuttaEEPROM(void) {
	FillEEPROMS(0x00);
}	
	
/*--------------------------------------------------------------------------------------*\
Method: FillEEPROMS
	Riempie tutte le EEPROMs col valore passato come parametro.

	IN:		- valore di FILL
	OUT:	- 
\*--------------------------------------------------------------------------------------*/
void   FillEEPROMS(uint8_t FillVal)
{
	uint8_t		ZeroVal = 0;
	uint32_t	SizePagina;
	
	SizePagina = 128;																						// Default page size
	if (Tipo_EE == Atmel)
	{
		SizePagina = 256;
	}

	FillEEPI2C((uint16_t)BackupDataStart, FillVal, (GP_CODES-BackupDataStart));								// 0x0000 - 0x2A00 (escludo l'area con codici VARI)
	AuditExtClearData(true);																				// Azzero contatore blocchi di audit estesa in EE e counter prelievi
	if (FillVal == 0x00)
	{
		// *******   Azzeramento   Memoria    ********
		FillEEPI2C((uint16_t)EE_FREE1, FillVal, (EEprom_Bank_Size-EE_FREE1));								// 0x2B00 - 0xffff: da EE_FREE1 fino alla fine della EEProm Bank0
		FillEEPI2C((uint16_t)IICEEPConfigAddr(EEPrezzoPR[0]), 0xFF, (200));									// Lista Prezzi PR = 65535
		WriteEEPI2C(AuditFileReady, &ZeroVal, 2U);															// Azzera marche Audit disponibile e Audit Prelevata
		WriteEEPI2C(LogFileContentLen, &ZeroVal, 7U);														// Azzero counter dati di Log
		
		// --- Azzero Bank 1 EEprom 1 ----
		BankSelect(0x01);
		FillEEPI2C((uint16_t)EEprom_Bank1, ZeroVal, EEprom_Bank_Size);										// Azzero tutto il Bank 1
		if (gVars.ee2)
		{
			// --- Azzero Bank 1 EEprom 2 ----
			BankSelect(0x02);
			FillEEPI2C((uint16_t)0x0, ZeroVal, EEprom_Bank_Size);											// Azzero EEprom N. 2 Bank 1
			// --- Azzero Bank 2 EEprom 2 ----
			BankSelect(0x03);
			FillEEPI2C((uint16_t)EEprom_Bank1, ZeroVal, EEprom_Bank_Size);									// Azzero EEprom N. 2 Bank 2
		}
	}
	else 
	{
		// *******   FILL  Memoria con FillVal   ********
		FillEEPI2C((uint16_t)EE_FREE1, FillVal, ((EEprom_Bank_Size+1) - EE_FREE1));							// 0x2B00 - 0xffff: da EE_FREE1 fino alla fine della EEProm Bank0
		FillEEPI2C((uint16_t)IICEEPConfigAddr(EEPrezzoPR[0]), FillVal, (200));								// Lista Prezzi con lo stesso valore di FILL
		FillEEPI2C((uint16_t)TxConfigBuff, ZeroVal, SizePagina);											// Azzero una pagina di TxConfigBuff (0xD000) per avere l'EOF 
		BankSelect(0x01);
		FillEEPI2C((uint16_t)EEprom_Bank1, FillVal, (0x10000));												// 0x10000 - 0x1FFFF
		if (gVars.ee2)
		{
			BankSelect(0x02);
			FillEEPI2C((uint16_t)0x0, FillVal, (0x10000));													// EEprom N. 2:  0x0 - 0xFFFF
			BankSelect(0x03);
			FillEEPI2C((uint16_t)EEprom_Bank1, FillVal, (0x10000));											// EEprom N. 2:  0x10000 - 0x1FFFF
		}
	}
	BankSelect(0x00);
	SetDateInit();
}

/*--------------------------------------------------------------------------------------*\
Method: FillEE_LogBuffer
	Riempie tutto il Log Buffer in EEprom del chr passato come parametro.

	IN:		- valore di FILL
	OUT:	- 
\*--------------------------------------------------------------------------------------*/
void   FillEE_LogBuffer(uint8_t FillVal)
{
	uint32_t	LogBuffSize, LogBuffStartAddr;

	ReadEEPI2C(LogFileSize, (uint8_t *)(&MemoryBytes[0]), 3U);									// In LogBuffSize dimensioni del Log Buffer in EEProm
	LogBuffSize = MemoryBytes[0];																// LSB
	LogBuffSize += (MemoryBytes[1]<<8);															// Medium
	LogBuffSize += (MemoryBytes[2]<<16);														// MSB

	// Determino address inizio Log Buffer in EEProm
	Rd_EEBuffStartAddr_And_BankSelect(BUF_LOGFILE);												// In  MemoryBytes[] StartAddress del buffer e bank della EE gia' selezionato
	LogBuffStartAddr = 0;
	LogBuffStartAddr += MemoryBytes[1];															// LSB
	LogBuffStartAddr += (MemoryBytes[0]<<8);													// MSB
	FillEEPI2C((uint16_t)LogBuffStartAddr, FillVal, LogBuffSize);								// Riempio il Log Buffer EEProm del valore FillVal
	BankSelect(0x00);
}

/*--------------------------------------------------------------------------------------*\
Method: Check_Clear_Audit_LR
	Controlla se l'Audit e' stata prelevata per azzerare gli ID LR.
	Dopo un prelievo DDCMP la marca "AuditClearEnabled" contiene 0x66 mentre dopo un
	prelievo con chiave USB contiene 0xA5 cosi' come la marca "AuditFileReady".
	Se la marca "AuditFileReady" e' 0xA5 mentre la marca "AuditClearEnabled" e' zero,
	significa che il sistema ha predisposto l'Audit per il prelievo ma poi non e' stato
	effettuato da alcuna chiave.
	La funzione controlla anche se e' stata prelevata l'Audit estesa per azzerarne il
	counter. 
	Se la chiave inserita non era una chiave di prelievo Audit, la flag "AuditFileReady"
	e' set ma non "ENA_AUDIT_LR_CLEAR": per evitare quindi di prelevare poi un'Audit
	vecchia se viene inserita una chiave Audit a sistema spento, azzero entrambe le flag.
	Se e' abilitata l'Audit Estesa si fa lo stesso controllo su "LogFileContentLen", il
	counter del buffer che la contiene: se l'Audit estesa e' stata prelevata il counter
	e' zero, in caso contrario significa che non e' stata prelevata e si azzera il
	counter per evitare che si prelevi un'Audit Estesa vecchia se la chiave sara' inserita
	a sistema spento.
	Solo successivamente a queste operazioni, si controlla se registrare in EE eventuali
	eventi di Power Outage e WDReset (le variabili in RAM sono gia' state predisposte nel
	reset).
	In registrazione LogExec non azzero LogFileContentLen (counter chr in log EE) se il 
	bootloader non ha potuto registrarlo sulla chiave USB, per non perdere le informazioni
	contenute. 

	IN:		- 
	OUT:	- 
\*--------------------------------------------------------------------------------------*/
void  Check_Clear_Audit_LR(void)  {

	if (FileAudReady == AUDIT_USB_READY) {														// E' stata inserita una chiave USB: preparata Audit Standard
		if (gOrionConfVars.AuditExt) {															// Audit Estesa abilitata: preparata anche Audit Estesa
			if (ReadLogFileContentLen() == 0) {													// Se LogBuffCnt = 0 significa che Audit Estesa esportata su chiave USB:
				AuditExtClearData(false);														// azzero contatore blocchi e incremento numero prelievi
			} else {
				if ((SystOpt3 & LogProtExec) == 0) {											// In LogProtExec non azzero LogFileContentLen perche' counter dati log Exec
					memset(&MemoryBytes[0], 0, 4U);												// L'audit estesa NON e' stata prelevata pertanto azzero counter per evitare
					WriteEEPI2C(LogFileContentLen, (uint8_t *)(&MemoryBytes[0]), 4U);			// un prelievo vecchio se verra' inserita una chiave a sistema spento.
				}
			}
		}
		if (EnableAuditClr == ENA_AUDIT_LR_CLEAR) {												// File Audit esportato su chiave USB: azzero Audit Standard LR
			ClearAudit();
		}
		ClrMarcheAudit();																		// Azzera marche Audit pronta per esport. e Audit Prelevata
	} else {
		if (EnableAuditClr == DDCMP_ENA_AUDIT_CLEAR && !ConnessioneDDCMP_IrDA()) {				// Audit prelevata con DDCMP
			if (gOrionConfVars.AuditExt) {														// Abilitato prelievo anche dell'Audit Estesa
				AuditExtClearData(false);														// Azzero contatore blocchi e incremento numero prelievi
			}
			ClearAudit();																		// Azzera Audit Standard LR
		}
	}

#if kOnOffEvents || kApplExChEvents || kApplRTCEventSupport	
	if (CntEventi_PowerOutage) {
		PowerOutageAudit();																		// Power ON: inserisco Evento e Audit Power Outages
	}
	if (CntEventi_WD_Reset) {
    	ResetDaWDOG_Events();																	// Reset da WDOG: inserisco Evento
	}
#endif
}

/*---------------------------------------------------------------------------------------------*\
Method: Set_DDCMP_EnableAuditClear
	Abilita il main ad azzerare l'Audit LR dopo un prelievo tramite DDCMP.
	Data/ora del prelievo attuale diverranno data/ora prelievo precedente al successivo
	azzeramento Audit LR: per questo devono essere memorizzati in EEPROM perche' il sistema 
	potrebbe azzerare l'audit anche dopo diversi spegnimenti.
	Per velocizzare la scrittura in EE utilizzo il buffer locale LocalDateBuff.

	IN:		- 
	OUT:	- 
\*----------------------------------------------------------------------------------------------*/
void  Set_DDCMP_EnableAuditClear(void) {

	uint8_t	LocalDateBuff[6];
	
	LocalDateBuff[0] = SaveData;
	LocalDateBuff[1] = SaveData>>8;
	LocalDateBuff[2] = SaveData>>16;
	LocalDateBuff[3] = SaveData>>24;
	LocalDateBuff[4] = SaveTime;
	LocalDateBuff[5] = SaveTime>>8;
	
	EnableAuditClr = DDCMP_ENA_AUDIT_CLEAR;
	PSHProcessingData();
	WriteEEPI2C(AuditClearEnabled, (uint8_t *)(&EnableAuditClr), 1U);							// Scrittura marca Audit Prelevata
	IICEEPW(IICEEPAuditAddr(DateAuditRead), &LocalDateBuff[0], 6U );							// Salvo Data/Ora prelievo Audit perche' il sistema potrebbe spegnersi
	PSHCheckPowerDown();
}

/*--------------------------------------------------------------------------------------*\
Method: ClearAudit
	Azzera LR ID, incrementa contatore prelievi e salva data/ora prelievo precedente.
	Azzera inoltre la marca di "Audit disponibile al prelievo USB".
	
\*--------------------------------------------------------------------------------------*/
void  ClearAudit(void)
{
	uint16_t	MarcaAudit;																		// uint16_t perche' usata per azzerare le 2 marche Audit Ready e Audit Clr Enabled  

	MarcaAudit = 0;
	PSHProcessingData();
    AuditUlongInc(IICEEPAuditAddr(NumReads));
    IICEEPW(IICEEPAuditAddr(DateLast), (uint8_t *)(&SaveData), sizeof(SaveData));
    IICEEPW(IICEEPAuditAddr(TimeLast), (uint8_t *)(&SaveTime), sizeof(SaveTime));
	FillEEPI2C((uint16_t)AuditLR_Begin, 0, AuditLR_Len);
	WriteEEPI2C(AuditFileReady, (uint8_t *)(&MarcaAudit), 2U);									// Clr marca "AuditFileReady" per USB e marca "AuditClearEnabled" Audit prelevata
	PSHCheckPowerDown();
	FileAudReady = 0;
	EnableAuditClr = 0;
}

/*--------------------------------------------------------------------------------------*\
Method: ClrMarcheAudit
	Azzera marche File Audit pronto e abilitazione Audit Clr. 

\*--------------------------------------------------------------------------------------*/
void  ClrMarcheAudit(void) {
	
	FillEEPI2C(AuditFileReady, 0x00, 0x02);
	FileAudReady = 0;
	EnableAuditClr = 0;
}

/*--------------------------------------------------------------------------------------*\
Method: AuditLRClear
	Azzera solamente i dati LR in eeprom (usata per azzerare la memoria NON da prelievo
	audit ma da Monitor o da Carta MHD Azzeramento Audit).
	
\*--------------------------------------------------------------------------------------*/
void AuditLRClear(void) {
	
	PSHProcessingData();
	FillEEPI2C((uint16_t)AuditLR_Begin, 0x00, AuditLR_Len);
	if (gOrionConfVars.AuditExt) {																// Abilitato prelievo anche dell'Audit Estesa
		AuditExtClearData(true);																// Azzero contatore blocchi di audit estesa in EE e counter prelievi
	}
	PSHCheckPowerDown();
}

/*--------------------------------------------------------------------------------------*\
Method: AuditINClear
	Azzero l'Audit IN senza azzerare le prime 6 locazioni che contengono Date/Time Price.
	Si aggiorna inoltre Date/Time Init: la Date/Time Init.
	 
\*--------------------------------------------------------------------------------------*/
void AuditINClear(void) {
	uint32_t	size;

	size = sizeof(IICEEPAuditAddr(DatePrice)) + sizeof(IICEEPAuditAddr(TimePrice));				// Len memoria occupata da DatePrice e TimePrice
	FillEEPI2C((uint16_t)(IICEEPAuditAddr(DateInit)), 0, (AuditIN_Len - size));
	SetDateInit();
}

/*--------------------------------------------------------------------------------------*\
Method: MemoDateTimeActualAuditRead
	Salva Data e Ora prelievo attuale in EEPROM: utilizzata quando si preleva l'Audit
	tramite chiave USB perche' il vero prelievo e successivo azzeramento sara' effettuato
	dopo un reset (a differenza del FINISH che arriva senza spegnimenti intermedi) e
	senza questa memorizzazione data e ora attuali, che diventeranno data/ora prelievo
	precedente, sarebbero persi. 
	
\*--------------------------------------------------------------------------------------*/
void  MemoDateTimeActualAuditRead(void)
{
	PSHProcessingData();
	IICEEPW(IICEEPAuditAddr(DateAuditRead), (uint8_t *)(&SaveData), sizeof(SaveData) );			// Salvo Data/Ora prelievo Audit perche' il sistema potrebbe spegnersi
	IICEEPW(IICEEPAuditAddr(TimeAuditRead), (uint8_t *)(&SaveTime), sizeof(SaveTime) );			// prima dell'azzeramento dati, dove viene utilizzata data/ora prelievo
	PSHCheckPowerDown();
}

/*--------------------------------------------------------------------------------------*\
Method: CT5AuditAdjust
  Adjust audit after it has been cleared
\*--------------------------------------------------------------------------------------*/
void CT5AuditAdjust(void) {
  #if kApplChangeGiverSupport
  uint8_t tubeIx;

  // Adjust audit IN to keep it aligned with number of coins in tubes
  CGMngrTubeAssociationUpdate();
  for(tubeIx= 0; tubeIx<kCGNumTubes; ++tubeIx) {
    CGCoinCh coinCh= CGMngrTubeChGet(tubeIx);
    uint8_t coinCount= CGMngrRealCoinsInTubeGet(tubeIx);
    CreditCoinValue coinVal= CGMngrTubeCoinVal(tubeIx);
    if(coinCount==0) continue;
    PSHProcessingData();
    CT5AuditCoinManInInd(coinCh, coinVal, coinCount);
    PSHCheckPowerDown();
  }
  CT5AuditFlush();
  #endif  //kApplChangeGiverSupport
}

/************************************************************/
/*  Read dato da EEPROM seriale o da RAM  					*/
/************************************************************/
void  ReadDato(char *pntDestinazione, char *pntSorgente, uint16_t nBytes)
{
	if ((uint32_t)pntSorgente <= MCU_STM32_RAMBegin)
	{
		IICEEPR((uint8_t *)pntDestinazione, (IICEEPOffsetType)pntSorgente, nBytes);
	}
	else
	{
		memcpy(pntDestinazione, pntSorgente, nBytes);
	}
}

/**********************************************************/
/*  IICEEP Fill                                           */
/**********************************************************/
uint8_t IICEEPF(uint32_t pntDestinazione, uint8_t Value, IICEEPSizeType nBytes) {

	if(!nBytes) return 0;
    pntDestinazione -= IICEEPBaseAddress;
//	return FillEEPI2C((uint16_t)pntDestinazione, Value, nBytes);
	return true;
}
/*--------------------------------------------------------------------------------------*\
Method: IICEEPR Read   
\*--------------------------------------------------------------------------------------*/

uint8_t  IICEEPR(uint8_t *pntDestinazione, uint32_t pntSorgente, uint32_t nBytes) {

	uint8_t  ReadResult;
	
	if(!nBytes) return 0;

	pntSorgente -= IICEEPBaseAddress;
	if (pntSorgente >= EEprom_Bank1) {
		BankSelect(0x01);
	}
	ReadResult = ReadEEPI2C((uint16_t)pntSorgente, pntDestinazione, nBytes);
	BankSelect(0x00);
	return ReadResult;
}  
  
/*---------------------------------------------------------------------------------------------------*\
Method: IICEEPW
	Scrive la EEPROM seriale.

	IN:		- Address EEPROM dove scrivere i dati, address dati sorgente, numero di bytes da scrivere
	OUT:	- True e dato in memoria, altrimenti false
\*----------------------------------------------------------------------------------------------------*/
uint8_t  IICEEPW(uint32_t pntDestinazione, uint8_t *pntSorgente, uint32_t nBytes)
{
	uint8_t  Result;
	
	if(!nBytes) return 0;
	pntDestinazione -= IICEEPBaseAddress;
	if (pntDestinazione >= EEprom_Bank1)
	{
		BankSelect(0x01);
	}
	Result = WriteEEPI2C((uint16_t)pntDestinazione, pntSorgente, nBytes);
	BankSelect(0x00);
	return Result;
}  

/*--------------------------------------------------------------------------------------*\
Method: SetConfigReceiveStart
\*--------------------------------------------------------------------------------------*/
void SetConfigReceiveStart(void) {
/*
	uint8_t dato;

  dato = (bConfigReceiveStart | kConfigReceiveStart);       
  WriteDato((char *)(&bConfigReceiveStart), (char *)(&dato), sizeof(bConfigReceiveStart));
*/  
}

/*--------------------------------------------------------------------------------------*\
Method: ResetConfigReceiveStart
\*--------------------------------------------------------------------------------------*/
void ResetConfigReceiveStart(void) {
/*	
  uint8_t dato;

  dato = (bConfigReceiveStart & (0xff - kConfigReceiveStart)); 
  WriteDato((char *)(&bConfigReceiveStart), (char *)(&dato), sizeof(bConfigReceiveStart));
 */
}

//#endif

#if kOnOffEvents
/*--------------------------------------------------------------------------------------------*\
Method: PowerOutageAudit
	Memorizza gli eventi di OFF e ON solo se Reset da Power Down.
	Data e ora OFF sono in OffSavedData e OffSavedTime salvati nella backupRAM al power down.
	Data e ora attuali di accensione sono letti dal RTC tramite la "SetDataTime".

Parameters:
	IN: - OffSavedData e OffSavedTime con data e ora spegnimento
\*--------------------------------------------------------------------------------------------*/
void  PowerOutageAudit(void) {

	// Aggiorno ID EA7 counters Power Outages
	AuditUlongInc(IICEEPAuditAddr(LROutage));													// EA-701 
	AuditUlongInc(IICEEPAuditAddr(INOutage));													// EA-702 
	CntEventi_PowerOutage = 0;
	
	InsertEvent (kPowerdownEvent, OffSavedData, OffSavedTime, NocheckPWD);						// "OCF_OFF": Power Outage
	SetDataTime(&OffSavedData, &OffSavedTime);													// Leggo data/ora attuali
	InsertEvent (kStartupEvent, OffSavedData, OffSavedTime, CheckPWD);							// "OCF_ON ": Power Up
	PSHCheckPowerDown();
}
#endif


#if kOnOffEvents || kApplExChEvents || kApplRTCEventSupport 

/*--------------------------------------------------------------------------------------*\
Method: InsertEvent
	Memorizza nel buffer circolare Tipo evento e data/ora attuali.
	Incrementa anche il contatore di eventi memorizzati.

Parameters:
	IN: - Tipo evento, data e ora
\*--------------------------------------------------------------------------------------*/

void  InsertEvent (uint8_t tipo, uint32_t date, uint16_t time, uint8_t TestPWDN_Flag) {

  uint8_t indice;

  IICEEPR ((uint8_t *)(&indice), IICEEPAuditAddr(IndexDataEvent), sizeof(indice));

  PSHProcessingData();
  IICEEPW (IICEEPAuditAddr(DateEventList[indice]), (uint8_t *)(&date), sizeof(date));
  IICEEPW (IICEEPAuditAddr(TimeEventList[indice]), (uint8_t *)(&time), sizeof(time));
  IICEEPW (IICEEPAuditAddr(TypeEventList[indice]), (uint8_t *)(&tipo), sizeof(tipo));

  indice++;
  if (indice >= EVENT_NUMBER) indice = 0;
  IICEEPW (IICEEPAuditAddr(IndexDataEvent), (uint8_t *)(&indice), sizeof(indice));
  if (TestPWDN_Flag) {
	  PSHCheckPowerDown();
  }
}

/*--------------------------------------------------------------------------------------------*\
Method: ClearBackupRAM_Events
	Memorizza l'evento di Clear BackupRAM.

Parameters:
	IN: - 
\*--------------------------------------------------------------------------------------------*/
void  ClearBackupRAM_Events(void) {

	SetDataTime(&OffSavedData, &OffSavedTime);													// Leggo data/ora attuali
	InsertEvent (kClrBackupRAMEvent, OffSavedData, OffSavedTime, CheckPWD);
}

/*--------------------------------------------------------------------------------------------*\
Method: SwRevUpdate_Events
	Memorizza l'evento di aggiornamento revisione software.

Parameters:
	IN: - 
\*--------------------------------------------------------------------------------------------*/
void  SwRevUpdate_Events(void) {

	SetDataTime(&OffSavedData, &OffSavedTime);													// Leggo data/ora attuali
	InsertEvent (kSwRevUpdateEvent, OffSavedData, OffSavedTime, CheckPWD);
}

  	

#endif
