/****************************************************************************************
File:
    VMC_CPU_DispStone_XYCoord.h

Description:
    Definizioni delle costanti per le coordinate dei vari messaggi per il Display Touch Stone.

History:
    Date       Aut  Note
    Lug 2021	MR   

 *****************************************************************************************/


#ifndef __VMC_CPU_DispStone_XYCoord_H
#define __VMC_CPU_DispStone_XYCoord_H

 
// Touch 7" DA Standard

// --- Indice attribuito ad ogni coordinata per poterla trasformare nell'address Variabile nel Nuovo Touch Stone -- 
#define kXY_Riga1_OpzVarie			0x01
#define kXY_Riga2_OpzVarie			0x02
#define kXY_Audit_Riga1				0x03
#define kXY_Audit_Riga2				0x04
#define kXY_Audit_Riga3				0x05
#define kXY_Audit_Riga4				0x06
#define kXY_Audit_Riga5				0x07
#define kXY_Audit_Riga6				0x08
#define kCoordinateRigaSopra		0x09
#define kCoordinateLowRigaSotto		0x0a
#define kCoordinateCredit			0x0b
#define kCoordinateFW				0x0c
#define kCoordinateFW_IO			0x0d
#define kCoordinatePrezzoSel1		0x0e
#define kCoordinatePrezzoSel2		0x0f
#define kCoordinateZucchero			0x10
#define kXY_Bicchiere				0x11
#define kCoordinateRegolazPrezzi	0x12
#define kCoordinateGuasto			0x13
#define kXY_PrezzoPagPrezSelez		0x14
#define kXY_ScontoPagPrezSelez		0x15
#define kXY_NumeroSelezPriceHold	0x16
#define kXY_Pin1					0x17
#define kXY_Gestore					0x18
#define kXY_ExCh					0x19
#define kXY_Introd_PSW				0x1a
#define kXY_Modifica_PSW			0x1b
#define	kXY_EnaDis_Deconto			0x1c
#define	kXY_Set_Deconto				0x1d
#define	kXY_Set_Riserva_Deconto		0x1e
#define kXY_DecontoLow				0x1f
#define kXY_DecontoAttuale			0x20
#define kCoordinateModello			0x21
#define kCoordinateSelInProgramm	0x22
#define kXY_Fascia_1_Start			0x23
#define kXY_Fascia_1_Stop			0x24
#define kXY_Fascia_2_Start			0x25
#define kXY_Fascia_2_Stop			0x26
#define kXY_Fascia_3_Start			0x27
#define kXY_Fascia_3_Stop			0x28
#define kXY_Fascia_4_Start			0x29
#define kXY_Fascia_4_Stop			0x2a
#define	kXY_CCH2O_CapsGr_1			0x2b
#define	kXY_CCH2O_CapsGr_2			0x2c
#define	kXY_CCH2O_CapsGr_3			0x2d
#define	kXY_CCH2O_CapsGr_4			0x2e
#define	kXY_Riga1_CapsGr_1 			0x2f
#define	kXY_Riga2_CapsGr_1 			0x30
#define	kXY_Riga1_CapsGr_2 			0x31
#define	kXY_Riga2_CapsGr_2 			0x32
#define	kXY_Riga1_CapsGr_3 			0x33
#define	kXY_Riga2_CapsGr_3 			0x34
#define	kXY_Riga1_CapsGr_4 			0x35
#define	kXY_Riga2_CapsGr_4 			0x36
#define kXY_NumCoinBillMDB 			0x37
#define	kXY_XY_EnaInhCoinMDB		0x38
#define	kXY_XY_NumPezziMDB 			0x39
#define kXY_ActualDecalcif 			0x3f
#define	kXY_Audit_Riga7				0x40
#define	kXY_ENADIS_DECONTO_ICE		0x41
#define	kXY_DECONTO_ICE_ATTUALE		0x42


#define kXY_NoDispoSel_Posiz_1_di_6		0
#define kXY_NoDispoSel_Posiz_2_di_6		0
#define kXY_NoDispoSel_Posiz_3_di_6		0
#define kXY_NoDispoSel_Posiz_4_di_6		0
#define kXY_NoDispoSel_Posiz_5_di_6		0
#define kXY_NoDispoSel_Posiz_6_di_6		0


const uint8_t XY_Riga1_OpzVarie[] 		= {0x00, 0x0a, 0x01, 0x90, 0x01};			// 10,400		Coordinate Riga 1
const uint8_t XY_Riga2_OpzVarie[] 		= {0x00, 0x0a, 0x01, 0xb3, 0x02};			// 10,435		Coordinate Riga 2

#if (AuditSoloValori == 0)
const uint8_t XY_Audit_Riga1[] 			= {0x00, 0x32, 0x00, 0x28, 0x03};			// 50,40		Coordinate Audit Riga 1
const uint8_t XY_Audit_Riga2[] 			= {0x00, 0x32, 0x00, 0x46, 0x04};			// 50,70		Coordinate Audit Riga 2
const uint8_t XY_Audit_Riga3[] 			= {0x00, 0x32, 0x00, 0x64, 0x05};			// 50,100		Coordinate Audit Riga 3
const uint8_t XY_Audit_Riga4[] 			= {0x00, 0x32, 0x00, 0x82, 0x06};			// 50,130		Coordinate Audit Riga 4
const uint8_t XY_Audit_Riga5[] 			= {0x00, 0x32, 0x00, 0xa0, 0x07};			// 50,160		Coordinate Audit Riga 5
const uint8_t XY_Audit_Riga6[] 			= {0x00, 0x32, 0x00, 0xbe, 0x08};			// 50,190		Coordinate Audit Riga 6
#else
const uint8_t XY_Audit_Riga1[] 			= {0x01, 0xea, 0x00, 0x28, 0x03};			// 490,40		Coordinate Audit Riga 1
const uint8_t XY_Audit_Riga2[] 			= {0x01, 0xea, 0x00, 0x46, 0x04};			// 490,70		Coordinate Audit Riga 2
const uint8_t XY_Audit_Riga3[] 			= {0x01, 0xea, 0x00, 0x64, 0x05};			// 490,100		Coordinate Audit Riga 3
const uint8_t XY_Audit_Riga4[] 			= {0x01, 0xea, 0x00, 0x82, 0x06};			// 490,130		Coordinate Audit Riga 4
const uint8_t XY_Audit_Riga5[] 			= {0x01, 0xea, 0x00, 0xa0, 0x07};			// 490,160		Coordinate Audit Riga 5
const uint8_t XY_Audit_Riga6[] 			= {0x01, 0xea, 0x00, 0xbe, 0x08};			// 490,190		Coordinate Audit Riga 6
#endif

const uint8_t Coordinate[] 				= {0x00, 0x1C, 0x01, 0xA6, 0x09};			// 28,422		Coordinate Riga sopra
const uint8_t CoordinateLow[] 			= {0x00, 0x1C, 0x01, 0xC2, 0x0a};			// 28,450		Coordinate Riga sotto
const uint8_t CoordinateCredit[] 			= {0x01, 0xCC, 0x01, 0x97, 0x0b};			// 460,407		Coordinate Credito
const uint8_t CoordinateModello[] 		= {0x00, 0x3c, 0x00, 0x23, 0x21};			// 60,35		Coordinate Modello Macchina
const uint8_t CoordinateFW[] 				= {0x00, 0x3c, 0x00, 0x41, 0x0c};			// 60,65		Coordinate Firmware
const uint8_t CoordinateFW_IO[]			= {0x00, 0x3c, 0x00, 0x5f, 0x0d};			// 60,95		Coordinate Firmware scheda I/O
const uint8_t CoordinatePrezzoSel1[] 		= {0x01, 0x42, 0x00, 0xbe, 0x0e};			// 322,190		Coordinate Prezzo selezione 1
const uint8_t CoordinatePrezzoSel2[] 		= {0x01, 0x98, 0x00, 0xbe, 0x0f};			// 408,190		Coordinate Prezzo selezione 2
const uint8_t CoordinateZucchero[] 		= {0x00, 0xa8, 0x01, 0x4b, 0x10};			// 168,331		Coordinate regolazione zucchero
const uint8_t XY_Bicchiere[] 				= {0x02, 0x76, 0x01, 0x4b, 0x11};			// 630,331		Coordinate regolazione zucchero
const uint8_t CoordinateRegolazPrezzi[] 	= {0x02, 0x8a, 0x00, 0x34, 0x12};			// 650,052		Coordinate Regolazione Prezzo selezioni
const uint8_t CoordinateGuasto[] 			= {0x02, 0xbc, 0x01, 0xA8, 0x13};			// 700,424		Coordinate visualizzazione codice di guasto e temp caldaia Low
const uint8_t XY_PrezzoPagPrezSelez[] 	= {0x02, 0x35, 0x00, 0x64, 0x14};			// 565,100		Coordinate Prezzo Selezioni
const uint8_t XY_ScontoPagPrezSelez[] 	= {0x02, 0x9e, 0x00, 0x64, 0x15};			// 670,100		Coordinate Sconto Selezioni
const uint8_t XY_NumeroSelezPriceHold[] 	= {0x02, 0x40, 0x01, 0x4a, 0x16};			// 576,330		Coordinate Numero Selezione
const uint8_t XY_Pin1[] 					= {0x00, 0xb4, 0x00, 0x6a, 0x17};			// 180,106		Coordinate Pin 1   Pagina Opzioni Mifare
const uint8_t XY_Gestore[] 				= {0x01, 0xb0, 0x00, 0x6a, 0x18};			// 432,106		Coordinate Gestore Pagina Opzioni Mifare
const uint8_t XY_ExCh[]	 				= {0x00, 0xb8, 0x01, 0x97, 0x19};			// 152,407		Coordinate msg Ex-Ch
const uint8_t XY_Introd_PSW[] 			= {0x00, 0xc8, 0x00, 0x88, 0x1a};			// 200,136		Coordinate Introdurre password
const uint8_t XY_Modifica_PSW[] 			= {0x02, 0x8a, 0x00, 0x88, 0x1b};			// 650,136		Coordinate Modifica password
const uint8_t XY_EnaDis_Deconto[] 		= {0x00, 0x0a, 0x01, 0x90, 0x1c};			// 10,400		Coordinate Abilitazione/Disabilitazione Deconto
const uint8_t XY_Set_Deconto[] 			= {0x00, 0xc8, 0x00, 0x88, 0x1d};			// 200,136		Coordinate Introduzione Deconto
const uint8_t XY_Set_Riserva_Deconto[] 	= {0x02, 0x8a, 0x00, 0x88, 0x1e};			// 650,136		Coordinate Introduzione Riserva Deconto
const uint8_t XY_DecontoLow[] 			= {0x02, 0x30, 0x01, 0xc2, 0x1f};			// 560,450		Coordinate visualizzazione "Service" per Deconto Low
const uint8_t XY_DecontoAttuale[] 		= {0x00, 0x0a, 0x01, 0xae, 0x20};			// 10,430		Coordinate visualizzazione Deconto attuale
const uint8_t XY_SelInProgrammaz[] 		= {0x02, 0xe4, 0x00, 0x26, 0x22};			// 740,038		Coordinate visualizzazione Numero selezione in programmazione

const uint8_t XY_Fascia_1_Start[] 		= {0x00, 0x32, 0x00, 0xc0, 0x23};			// 050,192		Coordinate visualizzazione Fascia Oraria 1 Ore:Min START
const uint8_t XY_Fascia_1_Stop[] 			= {0x00, 0x9f, 0x00, 0xc0, 0x24};			// 159,192		Coordinate visualizzazione Fascia Oraria 1 Ore:Min STOP
const uint8_t XY_Fascia_2_Start[] 		= {0x01, 0x35, 0x00, 0xc0, 0x25};			// 309,192		Coordinate visualizzazione Fascia Oraria 2 Ore:Min START
const uint8_t XY_Fascia_2_Stop[] 			= {0x01, 0x9f, 0x00, 0xc0, 0x26};			// 415,192		Coordinate visualizzazione Fascia Oraria 2 Ore:Min STOP
const uint8_t XY_Fascia_3_Start[] 		= {0x00, 0x32, 0x01, 0x72, 0x27};			// 050,370		Coordinate visualizzazione Fascia Oraria 3 Ore:Min START
const uint8_t XY_Fascia_3_Stop[] 			= {0x00, 0x9f, 0x01, 0x72, 0x28};			// 159,370		Coordinate visualizzazione Fascia Oraria 3 Ore:Min STOP
const uint8_t XY_Fascia_4_Start[] 		= {0x01, 0x35, 0x01, 0x72, 0x29};			// 309,370		Coordinate visualizzazione Fascia Oraria 4 Ore:Min START
const uint8_t XY_Fascia_4_Stop[] 			= {0x01, 0x9f, 0x01, 0x72, 0x2a};			// 415,370		Coordinate visualizzazione Fascia Oraria 4 Ore:Min STOP

const uint8_t XY_CCH2O_CapsGr_1[] 		= {0x00, 0xec, 0x00, 0x34, 0x2b};			// 236,052		Coordinate CC Acqua Colonne Gruppo 1
const uint8_t XY_CCH2O_CapsGr_2[] 		= {0x02, 0x1a, 0x00, 0x34, 0x2c};			// 538,052		Coordinate CC Acqua Colonne Gruppo 2
const uint8_t XY_CCH2O_CapsGr_3[] 		= {0x00, 0xec, 0x00, 0xff, 0x2d};			// 236,255		Coordinate CC Acqua Colonne Gruppo 3
const uint8_t XY_CCH2O_CapsGr_4[] 		= {0x02, 0x1a, 0x00, 0xff, 0x2e};			// 538,255		Coordinate CC Acqua Colonne Gruppo 4

const uint8_t XY_Riga1_CapsGr_1[] 		= {0x00, 0x0a, 0x01, 0xa6, 0x2f};			// 10,422		Coordinate Riga 1 Colonne Gruppo 1
const uint8_t XY_Riga2_CapsGr_1[] 		= {0x00, 0x0a, 0x01, 0xbf, 0x30};			// 10,447		Coordinate Riga 2 Colonne Gruppo 1
const uint8_t XY_Riga1_CapsGr_2[] 		= {0x00, 0xd2, 0x01, 0xa6, 0x31};			// 210,422		Coordinate Riga 1 Colonne Gruppo 2
const uint8_t XY_Riga2_CapsGr_2[] 		= {0x00, 0xd2, 0x01, 0xbf, 0x32};			// 210,447		Coordinate Riga 2 Colonne Gruppo 2
const uint8_t XY_Riga1_CapsGr_3[] 		= {0x01, 0x90, 0x01, 0xa6, 0x33};			// 400,422		Coordinate Riga 1 Colonne Gruppo 2
const uint8_t XY_Riga2_CapsGr_3[] 		= {0x01, 0x90, 0x01, 0xbf, 0x34};			// 400,447		Coordinate Riga 2 Colonne Gruppo 2
const uint8_t XY_Riga1_CapsGr_4[] 		= {0x02, 0x58, 0x01, 0xa6, 0x35};			// 600,422		Coordinate Riga 1 Colonne Gruppo 2
const uint8_t XY_Riga2_CapsGr_4[] 		= {0x02, 0x58, 0x01, 0xbf, 0x36};			// 600,447		Coordinate Riga 2 Colonne Gruppo 2

const uint8_t XY_NumCoinBillMDB[] 		= {0x00, 0xdc, 0x00, 0x6a, 0x37};			// 220,106		Coordinate Numero moneta/Banconota MDB
const uint8_t XY_EnaInhCoinMDB[] 			= {0x01, 0xb0, 0x00, 0x6a, 0x38};			// 432,106		Coordinate Flag Enable/Inhibit moneta in RR MDB
const uint8_t XY_NumPezziMDB[] 			= {0x02, 0x08, 0x01, 0x40, 0x39};			// 500,320		Coordinate Numero di pezzi nei tubi della moneta in RR MDB

const uint8_t XY_DataOra_Day[] 			= {0x00, 0x46, 0x00, 0xdc, 0x3a};			//  70,220		Coordinate Orologio: Giorno
const uint8_t XY_DataOra_Month[] 			= {0x00, 0xda, 0x00, 0xdc, 0x3b};			// 218,220		Coordinate Orologio: Mese
const uint8_t XY_DataOra_Year[] 			= {0x01, 0x68, 0x00, 0xdc, 0x3c};			// 360,220		Coordinate Orologio: Anno
const uint8_t XY_DataOra_Hour[] 			= {0x02, 0x1c, 0x00, 0xdc, 0x3d};			// 540,220		Coordinate Orologio: Ore
const uint8_t XY_DataOra_Min[] 			= {0x02, 0xb0, 0x00, 0xdc, 0x3e};			// 688,220		Coordinate Orologio: Minuti

const uint8_t XY_ActualDecalcif[] 		= {0x00, 0xc8, 0x01, 0xb3, 0x3f};			// 200,435		Coordinate Contatore Attuale decalcificatore


#if (AuditSoloValori == 0)
const uint8_t XY_Audit_Riga7[] 			= {0x00, 0x32, 0x00, 0xdc, 0x08};			// 50,220		Coordinate Audit Riga 7
#else
const uint8_t XY_Audit_Riga7[] 			= {0x01, 0xea, 0x00, 0xdc, 0x40};			// 490,220		Coordinate Audit Riga 7
#endif

const uint8_t XY_ENADIS_DECONTO_ICE[] 	= {0x01, 0x72, 0x00, 0x96, 0x41};			// 370,150		Coordinate Abilitazione/Disabilitazione Deconto
const uint8_t XY_DECONTO_ICE_ATTUALE[] 	= {0x01, 0x72, 0x00, 0xb4, 0x42};			// 370,180		Coordinate visualizzazione Deconto attuale
const uint8_t XY_FAILBICC[] 				= {0x02, 0x30, 0x01, 0x9a, 0x43};			// 560,410		Coordinate visualizzazione "Warning" per Fail prelievo bicchiere in DA gelati
const uint8_t XY_PRICE_GUSTO1_PAG2[] 		= {0x00, 0xDC, 0x01, 0x18, 0x44};			// 220,280		Coordinate Prezzo selezione 1 nella prima pagina utente con i due gusti visualizzati
const uint8_t XY_PRICE_GUSTO2_PAG2[] 		= {0x02, 0x1C, 0x01, 0x18, 0x45};			// 540,280		Coordinate Prezzo selezione 2 nella prima pagina utente con i due gusti visualizzati

const uint8_t XY_OzonoTimeOFF_Fascia_1[] 	= {0x00, 0x32, 0x00, 0x60, 0x46};			// 050,096		Coordinate visualizzazione Time OFF Ozono Fascia 1
const uint8_t XY_OzonoTimeON_Fascia_1[] 	= {0x00, 0x9f, 0x00, 0x60, 0x47};			// 159,096		Coordinate visualizzazione Time ON  Ozono Fascia 1
const uint8_t XY_OzonoTimeOFF_Fascia_2[] 	= {0x01, 0x35, 0x00, 0x60, 0x48};			// 309,096		Coordinate visualizzazione Time OFF Ozono Fascia 2
const uint8_t XY_OzonoTimeON_Fascia_2[] 	= {0x01, 0x9f, 0x00, 0x60, 0x49};			// 415,096		Coordinate visualizzazione Time ON  Ozono Fascia 2
const uint8_t XY_OzonoTimeOFF_Fascia_3[] 	= {0x00, 0x32, 0x01, 0x40, 0x4a};			// 050,320		Coordinate visualizzazione Time OFF Ozono Fascia 3
const uint8_t XY_OzonoTimeON_Fascia_3[] 	= {0x00, 0x9f, 0x01, 0x40, 0x4b};			// 159,320		Coordinate visualizzazione Time ON  Ozono Fascia 3
const uint8_t XY_OzonoTimeOFF_Fascia_4[] 	= {0x01, 0x35, 0x01, 0x40, 0x4c};			// 309,320		Coordinate visualizzazione Time OFF Ozono Fascia 4
const uint8_t XY_OzonoTimeON_Fascia_4[] 	= {0x01, 0x9f, 0x01, 0x40, 0x4d};			// 415,320		Coordinate visualizzazione Time ON  Ozono Fascia 4

const uint8_t XY_FasciaOzono_3_Start[] 	= {0x00, 0x32, 0x01, 0x9f, 0x4e};			// 050,415		Coordinate visualizzazione Fascia Oraria Ozono 3 Ore:Min START
const uint8_t XY_FasciaOzono_3_Stop[] 	= {0x00, 0x9f, 0x01, 0x9f, 0x4f};			// 159,415		Coordinate visualizzazione Fascia Oraria Ozono 3 Ore:Min STOP
const uint8_t XY_FasciaOzono_4_Start[] 	= {0x01, 0x35, 0x01, 0x9f, 0x50};			// 309,415		Coordinate visualizzazione Fascia Oraria Ozono 4 Ore:Min START
const uint8_t XY_FasciaOzono_4_Stop[] 	= {0x01, 0x9f, 0x01, 0x9f, 0x51};			// 415,415		Coordinate visualizzazione Fascia Oraria Ozono 4 Ore:Min STOP

const uint8_t CoordinateDebugTemper[] 	= {0x02, 0x8a, 0x01, 0xA8, 0x52};			// 650,424		Coordinate visualizzazione temperature in debug
const uint8_t XY_AUDIT_SINGLE_SEL[] 		= {0x01, 0x30, 0x01, 0xa4, 0x53};			// 304,420		Coordinate Audit Singola Selezione
const uint8_t XY_Audit_Riga8[] 			= {0x01, 0xea, 0x00, 0xfa, 0x54};			// 490,250		Coordinate Audit Riga 8


const uint8_t XY_BLIND_MSG_ERR_0[] 		= {0x03, 0x84, 0x02, 0x44, 0x00};			// 900,580		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_ERR_1[] 		= {0x03, 0x84, 0x02, 0x58, 0x00};			// 900,600		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_ERR_2[] 		= {0x03, 0x84, 0x02, 0x6c, 0x00};			// 900,620		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_ERR_3[] 		= {0x03, 0x84, 0x02, 0x80, 0x00};			// 900,640		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_ERR_4[] 		= {0x03, 0x84, 0x02, 0x94, 0x00};			// 900,660		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_ERR_5[] 		= {0x03, 0x84, 0x02, 0xa8, 0x00};			// 900,680		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_ERR_6[] 		= {0x03, 0x84, 0x02, 0xbc, 0x00};			// 900,700		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_ERR_7[] 		= {0x03, 0x84, 0x02, 0xd0, 0x00};			// 900,720		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_NOSEL[] 		= {0x03, 0x84, 0x02, 0xe4, 0x00};			// 900,740		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)
const uint8_t XY_BLIND_MSG_CAPS[] 		= {0x03, 0x84, 0x02, 0xf8, 0x00};			// 900,760		Coordinate stringa fuori campo visibile (usata dal tablet al posto dello Stone)


const uint8_t XY_COUNTER_PIENO[] 			= {0x00, 0xc1, 0x01, 0x0e, 0x00};			// 193,270
const uint8_t XY_COUNTER_RISERVA[] 		= {0x01, 0xdb, 0x01, 0x0e, 0x00};			// 475,270
const uint8_t XY_CAPS_INTESTAZIONE[] 		= {0x00, 0x28, 0x01, 0x63, 0x00};			// 40,355
const uint8_t XY_CAPS_CNT_FULL[] 			= {0x00, 0x28, 0x01, 0x7c, 0x00};			// 40,380
const uint8_t XY_CAPS_CNT_RISERVA[] 		= {0x00, 0x28, 0x01, 0x95, 0x00};			// 40,405
const uint8_t XY_CAPS_CNT_ACTUAL[] 		= {0x00, 0x28, 0x01, 0xae, 0x00};			// 40,430



const uint8_t XY_NoDispoSel_Posiz_1_di_6[] = {0x00, 0x5f, 0x00, 0x28, 0x00};			//        		Coordinate Pulsante 1 di 6 nei menu utente
const uint8_t XY_NoDispoSel_Posiz_2_di_6[] = {0x01, 0x6d, 0x00, 0x28, 0x00};			//        		Coordinate Pulsante 2 di 6 nei menu utente
const uint8_t XY_NoDispoSel_Posiz_3_di_6[] = {0x02, 0x71, 0x00, 0x28, 0x00};			//        		Coordinate Pulsante 3 di 6 nei menu utente
const uint8_t XY_NoDispoSel_Posiz_4_di_6[] = {0x00, 0x5f, 0x00, 0xeb, 0x00};			//        		Coordinate Pulsante 4 di 6 nei menu utente
const uint8_t XY_NoDispoSel_Posiz_5_di_6[] = {0x01, 0x6d, 0x00, 0xeb, 0x00};			//        		Coordinate Pulsante 5 di 6 nei menu utente
const uint8_t XY_NoDispoSel_Posiz_6_di_6[] = {0x02, 0x71, 0x00, 0xeb, 0x00};			//        		Coordinate Pulsante 6 di 6 nei menu utente

const uint8_t XY_NoDispoSel_Gusto_1[] 	 = {0x00, 0xd0, 0x00, 0xa8, 0x00};			//        		Coordinate Pulsante Gusto 1 nei menu utente
const uint8_t XY_NoDispoSel_Gusto_2[] 	 = {0x02, 0x20, 0x00, 0xa8, 0x00};			//        		Coordinate Pulsante Gusto 2 nei menu utente



const uint8_t XY_NoDispoSel_Posiz_Sx[] 	 = {0x00, 0x82, 0x00, 0x5f, 0x00};			// 130,95 		Coordinate Pulsante di Sx nel sotto menu selezione
const uint8_t XY_NoDispoSel_Posiz_Dx[] 	 = {0x02, 0x6c, 0x00, 0x5f, 0x00};			// 620,95		Coordinate Pulsante di Dx nel sotto menu selezione

const uint8_t XY_NoDispo_Gusto1_Sx[] 		 = {0x00, 0x90, 0x00, 0x90, 0x00};			// 144,144 		Coordinate Pulsante Gusto 1 nel sotto menu selezione
const uint8_t XY_NoDispo_Gusto2_Dx[] 		 = {0x00, 0x90, 0x00, 0x90, 0x00};			// 144,144		Coordinate Pulsante Gusto 2 nel sotto menu selezione

const uint8_t XY_INSTANT_NODISP[] 		 = {0x00, 0x96, 0x00, 0xa5, 0x00};			// 150,165		Coordinate Selezione Instant
const uint8_t XY_TIME_EV_SANIF_RIGA_H[] 	 = {0x01, 0xcc, 0x00, 0x40, 0x00};			// 460,064		Coordinate Tempo Attivazione EV Sanificazione Riga High
const uint8_t XY_TIME_EV_SANIF_RIGA_L[]  	 = {0x01, 0xcc, 0x00, 0xc8, 0x00};			// 460,200		Coordinate Tempo Attivazione EV Sanificazione Riga Low


#endif		// VMC_CPU_DispStone_XYCoord_H



