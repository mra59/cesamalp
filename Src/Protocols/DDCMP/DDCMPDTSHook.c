/* ***************************************************************************************
File:
    DDCMPDTSHook.c

Description:
    Funzioni protocollo DDCMP

History:
    Date       Author
    Set 2012 	MR   

 *****************************************************************************************/

#include <string.h>


#include "ORION.H"
#include "DevSerial.h"
#include "DDCMPProtocol.h"
#include "DDCMP.h"
#include "DDCMPDTS.h"
#include "DDCMPDTSHook.h"



#include "AuditDataDescr.h"
#include "Audit.h"
#include "AuditExt.h"

#if kDEXUCSProtocolSupport
#include "DEXUCS.h"
#endif

#define kGPRSLst12  (1&kGPRSSupport)

/*--------------------------------------------------------------------------------------*\
External References 
\*--------------------------------------------------------------------------------------*/

extern  const		tAString tDXS_00;
extern 	bool 		TestAudit(void**);
extern 	void 		MGSMComDDCMPConnEstablished(void);
extern	uint16_t	TotaleCreditoAttuale(void);
extern	bool 		SetDataTime(uint32_t *data, uint *time);



/*--------------------------------------------------------------------------------------*\
Global variables
\*--------------------------------------------------------------------------------------*/

uAuditData uData;   



#if kDEXUCSProtocolSupport
static bool sDEXUCSLastBlock;

#pragma codeseg(DEXUCSSegCode)

#endif

/*--------------------------------------------------------------------------------------*\
Method: DataTranferTx
  Send a data message

Parameters:
  dataLen     ->  length of data
  selFlag     ->  select flag
\*--------------------------------------------------------------------------------------*/
void DataTranferTx(uint16_t dataLen, bool selFlag) {
#if kDEXUCSProtocolSupport
  if(fDataTrasferDEXUCS) {
    DEXUCSDataTx(dataLen, selFlag);
    sDEXUCSLastBlock= false;
  }
  else
#endif
    DDCMPDTSDataTx(dataLen, selFlag);
}

/*--------------------------------------------------------------------------------------*\
Method: DataTranferEnd
    Terminate current data transfer
\*--------------------------------------------------------------------------------------*/
void DataTranferEnd(void) {
#if kDEXUCSProtocolSupport
  if(fDataTrasferDEXUCS) {
    DEXUCSCmdEnd();
    sDEXUCSLastBlock= true;
  }
  else
#endif
    DDCMPDTSCmdEnd();
}


#if kDDCMPDTSMasterSupport && kDDCMPDTS2ConnReqSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTSHookMasterInfoGet
Parameters:
\*--------------------------------------------------------------------------------------*/
void DDCMPDTS2HookMasterInfoGet(DDCMPDTSMasterInfo* mi)
{
  mi->securityCode= DDCMPDTS2HookPwdGet();
}
#endif


/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookConnReqInd
Parameters:
\*--------------------------------------------------------------------------------------*/
/*MR19
bool DDCMPDTS2HookConnReqInd(const DDCMPDTSMasterInfo* mi, DDCMPDTSSlaveInfo* si)
{
  uint16_t crcVal;
  uint8_t data[6];
  RTCDateTime dateTime;

  data[0] = mi->day;
  data[1] = mi->month;
  data[2] = mi->year;
  data[3] = mi->hours;
  data[4] = mi->minutes;
  data[5] = mi->seconds;
  crcVal = 0;
  si->serialNumber[0] = crcVal>>8;
  si->serialNumber[1] = crcVal & 0x00ff;

    return (mi->securityCode==DDCMPDTS2HookPwdGet() || mi->securityCode==kDDCMPDTSUniversalPwd);

}
*/


#if kDEXUCSProtocolSupport && kDEXUCSDTSConnReqSupport
/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookConnReqInd
Parameters:
\*--------------------------------------------------------------------------------------*/
bool DEXUCSDTSHookConnReqInd(const DEXUCSDTSMasterInfo* mi, DEXUCSDTSSlaveInfo* si) {
  uint32toa0((char*)si, CodiceMacchina, 8);
  return true;
//Non prevista password x protocollo DEX-UCS
//return (mi->securityCode==DDCMPDTS2HookPwdGet() || mi->securityCode==kDDCMPDTSUniversalPwd);
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2Initialization
  Init DDCMP
Parameters:
\*--------------------------------------------------------------------------------------*/
void DDCMPDTS2Initialization(void) {
/*MR19
#if kDEXUCSProtocolSupport
  if(fDataTrasferDEXUCS) {
    DEXUCSOpen(kDEXUCSModeSlave, uData.audit.buffers);
    return;
  }
#endif

  	  DDCMPDTS2Init();
  	  DDCMPDTS2Open(kDDCMPDTSModeSlave, uData.audit.buffers);
*/
}

bool DDCMPDTS2HookSlaveRelease(void) {
  return false;
}

/*--------------------------------------------------------------------------------------*\
Method:	AuditTransferAllowed
	Controllo se ci sono le condizioni per prelevare l'Audit.

	IN:	  - 
	OUT:  - true se audit permesso, altrimenti false
\*--------------------------------------------------------------------------------------*/
bool  AuditTransferAllowed(void)
{
	if ((gOrionKeyVars.Inserted || (gOrionKeyVars.State > kOrionCPCStates_Enabled) || (TotaleCreditoAttuale() != 0)))
	{
		return false;
	}
	else
	{
		return true;
	}
}


#if kDDCMPDTS2MasterSupport
/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookConnEstablishedMasterInd
  Connection established in master mode
Parameters:
\*--------------------------------------------------------------------------------------*/
  #if kDDCMPDTS2ConnReqSupport
void DDCMPDTS2HookConnEstablishedMasterInd(const DDCMPDTSSlaveInfo* slaveInfo)
  #else
void DDCMPDTS2HookConnEstablishedMasterInd(void)
  #endif
{

	if(!DDCMPMngrHookConnEstablishedInd()) {
		// Sorry, I can't transfer audit now
		DDCMPDTS2CmdEnd();
		return;
	}
	uData.audit.listNumber 	= 1;
	gAuditListNumber 		= 1;
	DDCMPDTS2CmdWriteData(1, 0x0000);
    // Write audit list
}
#endif


#if kDEXUCSProtocolSupport
/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookConnEstablishedInd
    Connection established in master/slave mode (Notify our user)
\*--------------------------------------------------------------------------------------*/
void DEXUCSDTSHookConnEstablishedInd(void) {
  DDCMPMngrHookConnEstablishedInd();
  uData.audit.listNumber = 1;
//  DDCMPDTS2CmdWriteData(1, 0x0000);
    // Write audit list
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookConnIsEstablished
  Check if connection established in slave/master mode
Returns:
  true if connection is established
\*--------------------------------------------------------------------------------------*/
bool DDCMPDTS2HookConnIsEstablished(void) {
  return fDDCMPTransferInProgress;
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookConnRejectedInd
  Connection was rejected by slave device
Parameters:
\*--------------------------------------------------------------------------------------*/
void DDCMPDTS2HookConnRejectedInd(void) {
	Clear_fDDCMPTransferInProgress;
}



#if kDEXUCSProtocolSupport
/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookConnFinishedInd
  Connection completed succesfully
Parameters:
\*--------------------------------------------------------------------------------------*/
void DEXUCSDTSHookConnFinishedInd(void) {
  DDCMPDTS2HookConnFinishedInd();
}
#endif


#if kDEXUCSProtocolSupport
/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookConnFailedInd
  Connection aborted/failed
Parameters:
\*--------------------------------------------------------------------------------------*/
void DEXUCSDTSHookConnFailedInd(void) {
  DDCMPDTS2HookConnFailedInd();
}
#endif


/*--------------------------------------------------------------------------------------*\
Method: ReadTipoLista
  Check if we are reading audit list with DDCMP
Returns:
  true if we are reading audit list
\*--------------------------------------------------------------------------------------*/
bool ReadTipoLista(void)
{
  return ((uData.audit.listNumber == 1) || (uData.audit.listNumber == 2) || (uData.audit.listNumber == 3));
}


#if kDEXUCSProtocolSupport
/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookReadDataReqInd
  Handle reception of a ReadData request
  Returns true if read command accepted
Parameters:
\*--------------------------------------------------------------------------------------*/
bool DEXUCSDTSHookReadDataReqInd(uint8_t listNumber) {

  if(listNumber==kDEXUCSDTSListAuditCollection) {
    // DEXUCS is implemented only for audit data list
    if(!AuditTransferAllowed()) return false;
    Set_fDDCMPTransferInProgress;     // Inhibit payment system while we are sending audit

    uData.audit.listNumber = 1;
    DDCMPMngrTxBegin();               // read audit list
    return true;
  }

  return false;
}
#endif

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookReadDataRspInd
  Handle reception of a ReadData response
Parameters:
\*--------------------------------------------------------------------------------------*/
void DDCMPDTS2HookReadDataRspInd(bool accepted, uint16_t length) {
}


/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookWriteDataRspInd
  Handle reception of a WriteData response
Parameters:
\*--------------------------------------------------------------------------------------*/
/*MR19
void DDCMPDTS2HookWriteDataRspInd(bool accepted)
{
  if(accepted) DDCMPMngrTxBegin();
  else DDCMPDTS2CmdEnd();
}
*/

/*--------------------------------------------------------------------------------------*\
Method: DDCMPDTS2HookDeleteDataRspInd
  Handle reception of a DeleteData response
Parameters:
\*--------------------------------------------------------------------------------------*/
void DDCMPDTS2HookDeleteDataRspInd(bool accepted) {
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPMngrTxBegin

	*** ATTENZIONE ***: utilizzata anche durante trasferimento Audit nella chiave USB.

Parameters:
\*--------------------------------------------------------------------------------------*/
void DDCMPMngrTxBegin(void) {
	
	uData.audit.localBufferSize = 0;
	uData.audit.dataBuffTransmitted = 0;
	uData.audit.loopBodyResponse = true;
	uData.audit.txBuffer = false;
	uData.audit.endTx = false;
	uData.audit.EndTxAudit = NULL;
	uData.audit.txAuditExt = false;

	if (ReadTipoLista()) {
		SetDataTime(&SaveData, &SaveTime);																		// Legge orologio per determinare data/ora prelievo attuale
		AuditInitProfiler(&DDCMPMngrHookBufferAudit, (AuditEndTestFn)&TestAudit, &uData.audit.localBuffer[0]);
		AuditLoopInit();
		uData.audit.numBlocks = 0;
		if (gOrionConfVars.AuditExt || uData.audit.listNumber==3)
		{
			uData.audit.numBlocks = AuditExtNumBlocks();														// Calcola quanti blocchi di Audit Estesa devono essere trasmessi
		}
		uData.audit.block = 0;
	}
	else 
	{
//    ConfTxInit((void *)tConfigParam, &DDCMPMngrHookBufferAudit, (uint8_t*)&uData.audit.localBuffer[0]);
	}

}


    /**********************************************************/
    /*                                                        */
    /**********************************************************/
void  DDCMPMngrHookBufferAudit(char *pnt, uint Size) {
	
	uData.audit.localBufferPnt = pnt;
	uData.audit.localBufferSize = Size;

	if (uData.audit.localBufferSize > 0 && uData.audit.dataBuffFree > uData.audit.localBufferSize) {
	    DDCMPMngrHookCopyLocalBuffer();
	} else {
		// Se localBufferSize>0 ma dataBuffFree=localBufferSize ed e' l'ultimo blocco di ID da trasmettere,
		// chiamo comunque la DDCMPMngrHookCopyLocalBuffer
		if (uData.audit.localBufferSize > 0 && uData.audit.dataBuffFree == uData.audit.localBufferSize && uData.audit.loopBodyResponse == false) {
		    DDCMPMngrHookCopyLocalBuffer();
		}
	}
}

/*--------------------------------------------------------------------------------------*\
Method: DDCMPMngrHookCopyLocalBuffer
	Trasferisce  il buffer locale nel buffer destinazione

Parameters:
	IN	- pointer buffer locale e suo size e pointer buffer destinazione
	OUT	- 
\*--------------------------------------------------------------------------------------*/
void  DDCMPMngrHookCopyLocalBuffer()
{
	memcpy(uData.audit.applDataBuff, uData.audit.localBufferPnt, uData.audit.localBufferSize);
	uData.audit.applDataBuff+= uData.audit.localBufferSize;
	uData.audit.dataBuffFree-= uData.audit.localBufferSize;
	uData.audit.dataBuffUsed+= uData.audit.localBufferSize; 
	uData.audit.dataBuffTransmitted+= uData.audit.localBufferSize; 
	uData.audit.localBufferSize = 0;
}

/**********************************************************************/


#define kMaxBaudRateDDCMPExtIrDA  kDevSerBitRate2400

#define GSMAppOpen    MGSMOpen
#define GSMAppClose   MGSMClose
#define GSMAppTxBegin MGSMTxBegin
#define GSMAppTxEnd   MGSMTxEnd
#define GSMAppRxBegin MGSMRxBegin
#define GSMAppRxEnd   MGSMRxEnd
#define GSMAppCharTx  MGSMCharTx

/*--------------------------------------------------------------------------------------*\
Method: DDCMP2HookCommOpen
Parameters:
\*--------------------------------------------------------------------------------------*/
void DDCMP2HookCommOpen(void) {
/*MR19
	DDCMPSCIOpen(serOpt, hnd);
	IrDAOpen(serOpt->bitRate);
*/
} 


void DDCMP2HookCommClose(void) {
/*MR19
    DDCMPSCIClose();
*/
}

void DDCMP2HookTxBegin(void) {
/*MR19
    DDCMPSCIRxEnd();
    DDCMPSCITxBegin();
*/
} 

void DDCMP2HookTxEnd(void) {
/*MR19
    DDCMPSCITxEnd();
*/
}
 
void DDCMP2HookRxBegin(void) {
/*MR19
    DDCMPSCIRxBegin();
*/
}

void DDCMP2HookRxEnd(void) {
/*MR19
    DDCMPSCIRxEnd();
*/
}

void DDCMP2HookCharTx(char ch) {
/*MR19
    DDCMPSCICharTx(ch);
*/
}



    /**********************************************************/
    /*                                                        */
    /**********************************************************/
/*MR19	
uint8_t DDCMP2HookBaudRateMaxGet(void) {

	bool externalIrDABaudRate= fExternalIrDABaudRate;

    if (sCT5DDCMPUseExternalIrDA) {
    	return externalIrDABaudRate? kMaxBaudRateDDCMP : kMaxBaudRateDDCMPExtIrDA;
    } else {
        return kMaxBaudRateDDCMP;
    }
}
*/

#if kDEXUCSProtocolSupport

/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookConnStartInd
    Connection starting in master/slave mode
\*--------------------------------------------------------------------------------------*/
void DEXUCSDTSHookConnStartInd(void) {
  uData.audit.listNumber= kDDCMPDTSListNull;
}

/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookDataTxInd
    Handle transmission of audit data
Parameters:
    applDataBuff    ->  ptr to the buffer where to put application data to send
    dataBuffSize    ->  size of the buffer
Return:
    True            -> Audit tx finished
    False           -> still audit data tx
\*--------------------------------------------------------------------------------------*/
bool DEXUCSDTSHookDataTxInd(uint8_t* applDataBuff, uint16_t dataBuffSize) {
  sDEXUCSLastBlock= false;
  DDCMPDTS2HookDataTxInd(1, applDataBuff, dataBuffSize);
  return sDEXUCSLastBlock;
}

#if kDEXUCSIrDASupport
/*--------------------------------------------------------------------------------------*\
Method: DEXUCSHookCommOpen
Parameters:
\*--------------------------------------------------------------------------------------*/
void DEXUCSHookCommOpen(void) {

	DEXUCSSCIOpen();
    //MR19 IrDAOpen(serOpt->bitRate);
} 


void DEXUCSHookCommClose(void) {
  DEXUCSSCIClose();
}

void DEXUCSHookTxBegin(void) {
  DEXUCSSCIRxEnd();          //Disable RX for IRDA 31.1.2005
  DEXUCSSCITxBegin();
} 

void DEXUCSHookTxEnd(void) {
  DEXUCSSCITxEnd();
}
 
void DEXUCSHookRxBegin(void) {
  DEXUCSSCIRxBegin();
}

void DEXUCSHookRxEnd(void) {
  DEXUCSSCIRxEnd();
}

void DEXUCSHookCharTx(char ch) {
  DEXUCSSCICharTx(ch);
}

/*--------------------------------------------------------------------------------------*\
Method: DEXUCSDTSHookWaitStart
	Called when DEXUCS wait start command (ENQ)
\*--------------------------------------------------------------------------------------*/
void DEXUCSDTSHookWaitStart(void) {
  DDCMPHookWaitStart();
}

#endif  // kDEXUCSIrDASupport
#endif  // kDEXUCSProtocolSupport






